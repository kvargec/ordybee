<?php

namespace frontend\controllers;

use Yii;
use common\models\Dijete;
use common\models\Search\DijeteSearch;
use common\models\Zahtjev;
use common\models\Rodbina;
use common\models\VrstaPrograma;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DijeteController implements the CRUD actions for Dijete model.
 */
class DijeteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dijete models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DijeteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dijete model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Dijete model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dijete();
        //provjeri postoje li podvrste vrsta programa, 
        $podvrsta = false;
        $podvrstePrograma = VrstaPrograma::find()->where(['>', 'parent', 0])->one();
        if ($podvrstePrograma) {
            $podvrsta = true;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $post = Yii::$app->request->post('Dijete');
            if (empty($post['vrsta_programa'])) {
                $model->vrsta_programa = $post['vrstadepdrop'];
            } else {
                $model->vrsta_programa = $post['vrsta_programa'];
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'podvrsta' => $podvrsta
        ]);
    }

    /**
     * Updates an existing Dijete model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $id = (int)$id;
        $model = $this->findModel($id);
        $model->teskoce = \json_decode($model->teskoce);
        $model->dijagnostika = \json_decode($model->dijagnostika);
        //provjeri postoje li podvrste vrsta programa, 
        $podvrsta = false;
        $podvrstePrograma = VrstaPrograma::find()->where(['>', 'parent', 0])->one();
        if ($podvrstePrograma) {
            $podvrsta = true;
        }

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post('Dijete');
            $model->prezime = trim($post['prezime']);
            $model->ime = trim($post['ime']);
            $model->spol = trim($post['spol']);
            $model->dat_rod = trim($post['dat_rod']);
            $model->oib = substr(trim($post['oib']), 0, 13);
            $model->adresa = trim($post['adresa']);
            $model->mjesto = trim($post['mjesto']);
            $model->prebivaliste = trim($post['prebivaliste']);
            $model->sestra = trim($post['sestra']);
            $model->cekanje = trim($post['cekanje']);
            $model->god_cekanja = trim($post['god_cekanja']);
            $model->razvoj = $post['razvoj'];
            $model->teskoce = json_encode($post['teskoce']);
            $model->dijagnostika = json_encode($post['dijagnostika']);
            $model->druge_potrebe = $post['druge_potrebe'];
            $model->posebne = $post['posebne'];
            if (empty($post['vrsta_programa'])) {
                $model->vrsta_programa = $post['vrstadepdrop'];
            } else {
                $model->vrsta_programa = $post['vrsta_programa'];
            }
            $model->update_at = date("Y-m-d H:i:s", time());

            $zahtjev = Zahtjev::find()->where(['dijete' => $model->id])->one();


            if ($model->save(false)) {
                return $this->redirect(['zahtjev/korak7', 'zahtjevId' => $zahtjev->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'podvrsta' => $podvrsta
        ]);
    }

    /**
     * Updates an existing Dijete sigling model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdatesibling($id)
    {
        $id = (int)$id;
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post('Dijete');
            $model->ime = $post['ime'];
            $model->prezime = $post['prezime'];
            $model->spol = trim($post['spol']);
            $model->dat_rod = trim($post['dat_rod']);
            $model->oib = substr(trim($post['oib']), 0, 13);
            $model->adresa = $post['adresa'];
            $model->save();

            $glavniSibling = Rodbina::find()->where(['sibling' => $model->id])->one();
            $zahtjev = Zahtjev::find()->where(['dijete' => $glavniSibling->dijete])->one();

            if ($model->save(false)) {
                return $this->redirect(['zahtjev/korak7', 'zahtjevId' => $zahtjev->id]);
            }
        }

        return $this->render('updatesibling', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Dijete model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Dijete model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dijete the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dijete::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
