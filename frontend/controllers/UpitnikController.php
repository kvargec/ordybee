<?php

namespace frontend\controllers;

use common\models\Odgovori;
use common\models\Pitanje;
use common\models\User;
use Yii;
use common\models\Upitnik;
use common\models\Search\UpitnikSearch;
use yii\base\BaseObject;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UpitnikController implements the CRUD actions for Upitnik model.
 */
class UpitnikController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Upitnik models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UpitnikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Upitnik model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $svaPitanja=Pitanje::find()->where(['upitnik'=>$id])->orderBy('id ASC')->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'svaPitanja'=>$svaPitanja
        ]);
    }
    public function actionSpremi(){
        $post=Yii::$app->request->post();
        $userID=Yii::$app->user->id;
        $odgovori=array();
        $pitanja=array();
        if($post){
            foreach ($post as $item=>$value){
                $idPit=explode("_",$item);

                if($idPit[1]!='csrf-frontend'){
                    $forTest=Odgovori::find()->where(['pitanje'=>$idPit[1],'user'=>$userID])->one();
                    if(isset($forTest)){
                        $temp=$forTest;
                    }else{
                        $temp=new Odgovori();
                    }
                    $temp->user=$userID;
                    $temp->pitanje=$idPit[1];
                    $temp->value=json_encode(['odgovori'=>$value]);
                    $temp->save();
                    $pitanja[]=$idPit[1];
                    $odgovori[$idPit[1]]=$value;
                }

            }
        }
        $pitanjaObjekti=Pitanje::find()->where(['in','id',$pitanja])->all();
        return $this->render('odgovori', [
            'pitanja' => $pitanjaObjekti,
            'odgovori'=>$odgovori 
        ]);
    }
    /**
     * Creates a new Upitnik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Upitnik();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Upitnik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Upitnik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Upitnik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Upitnik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Upitnik::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
