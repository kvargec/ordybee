<?php

namespace frontend\controllers;

use Yii;
use common\models\DijeteSkupina;
use common\models\Mailbox;
use common\models\MsgFolders;
use common\models\Roditelj;
use common\models\Skupina;
use common\models\Mstatus;
use common\models\UserRoditelj;
use common\models\Zaposlenik;
use common\models\Zaposlenje;
use common\models\User;
use common\models\Poruke;
use common\models\Search\PorukeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PorukeController implements the CRUD actions for Poruke model.
 */
class PorukeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Poruke models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PorukeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    /**
     * Displays a single Poruke model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $folders = MsgFolders::find()->where(['in', 'user', [0, Yii::$app->user->id]])->orderBy('redoslijed')->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'folders' => $folders
        ]);
        /*
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        */
    }

    /**
     * Creates a new Poruke model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*
    public function actionCreate($folder='inbox')
    {
        $model = new Poruke();
        $msgFolder=MsgFolders::find()->where(['folder_name'=>$folder])->one();
        $folders=MsgFolders::find()->where(['in','user',[0,Yii::$app->user->id]])->orderBy('redoslijed')->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'folders'=>$folders,
            'msgFolder' => $msgFolder
        ]);
    }*/
    public function actionCreate()
    {
        $model = new Poruke();
        $userID = Yii::$app->user->id;
        $folders = MsgFolders::find()->where(['in', 'user', [0, Yii::$app->user->id]])->orderBy('redoslijed')->all();
        $reply = false;
        $reply_all = false;

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Poruke');
            $model->recipients = [$post['grupacija'] => $post['recipients']];
            if (!empty($post['cc_recipients'])) {
                $model->cc_recipients = [$post['ccgrupacija'] => $post['cc_recipients']];
            }
            $model->save();
            //za senderov mailbox
            $temp = new Mailbox();
            $temp->message = $model->id;
            $temp->user = $userID;
            $temp->mstatus = 2;
            $folderSent = MsgFolders::find()->where(['folder_name' => 'sent'])->one();
            $temp->folder = $folderSent->id;
            $temp->save();
            switch ($post['grupacija']) {
                case 'roditelji':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['recipients']])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'skupine':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['recipients']])->select('dijete')->all();
                        $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                        $roditeljiSvi = array();
                        foreach ($roditelji as $rod) {
                            $roditeljiSvi[] = $rod->id;
                        }
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    }
                    break;
                case 'zaposlenici':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        var_dump($post['recipients']);
                        $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['recipients']])->asArray()->all();
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'pojedini':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['recipients']])->andWhere(['!=', 'status', 3])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'admin':
                    if ($post['recipients'][0] == 0) {
                        $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                        $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = User::find()->where(['in', 'id', $post['recipients']])->all();

                        foreach ($kojiUseri as $item) {
                            //print("<pre>".print_r($item,true)."</pre>");die();
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
            }
            if (isset($model->cc_recipients)) {
                switch ($post['ccgrupacija']) {
                    case 'roditelji':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['cc_recipients']])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['cc_recipients']])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            var_dump($post['cc_recipients']);
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['cc_recipients']])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['cc_recipients']])->andWhere(['!=', 'status', 3])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'admin':
                        if ($post['cc_recipients'][0] == 0) {
                            $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                            $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = User::find()->where(['in', 'id', $post['cc_recipients']])->all();

                            foreach ($kojiUseri as $item) {
                                //print("<pre>".print_r($item,true)."</pre>");die();
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            return $this->redirect(['mailbox/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'folders' => $folders,
            'reply' => $reply,
            'reply_all' => $reply_all,
            'replyId' => null
        ]);
    }

    public function actionResend($id)
    {
        $model_old = $this->findModel($id);
        $model = new Poruke();
        $userID = Yii::$app->user->id;

        if (isset($model_old)) {
            if (is_array($model_old->recipients)) {
                $grupacija = key($model_old->recipients);
            } else {
                $grupacija = null;
            }
            if (is_array($model_old->cc_recipients)) {
                $ccgrupacija = key($model_old->cc_recipients);
            } else {
                $ccgrupacija = null;
            }
            $model->subject = $model_old->subject;
            $model->message = $model_old->message;
            $model->sender = $model_old->sender;
            $model->recipients = $model_old->recipients;
            $model->cc_recipients = $model_old->cc_recipients;
            $model->status = $model_old->status;
            $model->attachments = $model_old->attachments;
            $model->priority = $model_old->priority;
            $model->vrsta_poruke = $model_old->vrsta_poruke;
            $model->save();
            //za senderov mailbox
            $temp = new Mailbox();
            $temp->message = $model->id;
            $temp->user = $userID;
            $temp->mstatus = 2;
            $folderSent = MsgFolders::find()->where(['folder_name' => 'sent'])->one();
            $temp->folder = $folderSent->id;
            $temp->save();

            switch ($grupacija) {
                case 'roditelji':
                    if ($model->recipients[$grupacija] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $rodje = $model->recipients[$grupacija];
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $rodje])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'skupine':
                    if ($model->recipients[$grupacija] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $skupinaS = $model->recipients[$grupacija];
                        $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $skupinaS])->select('dijete')->all();
                        $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                        $roditeljiSvi = array();
                        foreach ($roditelji as $rod) {
                            $roditeljiSvi[] = $rod->id;
                        }
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    }
                    break;
                case 'zaposlenici':
                    if ($model->recipients[$grupacija] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $zapl = $model->recipients[$grupacija];
                        $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $zapl])->asArray()->all();
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'pojedini':
                    if ($model->recipients[$grupacija] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $zapl = $model->recipients[$grupacija];
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', $zapl])->andWhere(['!=', 'status', 3])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'admin':
                    if ($model->recipients[$grupacija] == 0) {
                        $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                        $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $usr = $model->recipients[$grupacija];
                        $kojiUseri = User::find()->where(['in', 'id', $usr])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
            }
            if (isset($ccgrupacija)) {
                switch ($ccgrupacija) {
                    case 'roditelji':
                        if ($model->cc_recipients[$ccgrupacija] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $rodje = $model->cc_recipients[$ccgrupacija];
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $rodje])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($model->cc_recipients[$ccgrupacija] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $skupinaS = $model->cc_recipients[$ccgrupacija];
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $skupinaS])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($model->cc_recipients[$ccgrupacija] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $zapl = $model->cc_recipients[$ccgrupacija];
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $zapl])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($model->cc_recipients[$ccgrupacija] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $zapl = $model->cc_recipients[$ccgrupacija];
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $zapl])->andWhere(['!=', 'status', 3])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'admin':
                        if ($model->cc_recipients[$ccgrupacija] == 0) {
                            $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                            $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $usr = $model->cc_recipients[$ccgrupacija];
                            $kojiUseri = User::find()->where(['in', 'id', $usr])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            return $this->redirect(['mailbox/index']);
        }
    }

    public function actionSaveTemplate()
    {
        $post = Yii::$app->request->post('Poruke');
        $msgid = !empty($post['id']) ? $post['id'] : null;
        $userID = Yii::$app->user->id;
        $status = Mstatus::find()->where(['status' => 'template'])->one();
        $folder = MsgFolders::find()->where(['folder_name' => 'templates'])->one();
        $mailbox_msg = null;
        if (isset($msgid)) {
            $mailbox_msg = Mailbox::find()->where(['message' => $msgid, 'folder' => $folder->id, 'mstatus' => $status->id])->one();
        }
        if (isset($mailbox_msg)) {
            $model = $this->findModel($post['id']);
            if ($model->load(Yii::$app->request->post())) {
                $model->recipients = null;
                $model->cc_recipients = null;
                $model->save();
            }
        } else {
            $model = new Poruke();
            if ($model->load(Yii::$app->request->post())) {
                $model->recipients = null;
                $model->cc_recipients = null;
                $model->save();
                //za senderov mailbox
                $temp = new Mailbox();
                $temp->message = $model->id;
                $temp->user = $userID;
                $temp->mstatus = $status->id;
                $temp->folder = $folder->id;
                $temp->save();
            }
        }
        return $this->redirect(['mailbox/index']);
    }

    public function actionSaveDraft()
    {
        $post = Yii::$app->request->post('Poruke');
        $userID = Yii::$app->user->id;
        $status = Mstatus::find()->where(['status' => 'saved'])->one();
        $folder = MsgFolders::find()->where(['folder_name' => 'drafts'])->one();
        if (isset($post)) {
            if (!empty($post['id'])) {
                $mailbox_msg = Mailbox::find()->where(['message' => $post['id'], 'folder' => $folder->id, 'mstatus' => $status->id])->one();
                if (isset($mailbox_msg)) {
                    $model = $this->findModel($post['id']);
                    if ($model->load(Yii::$app->request->post())) {
                        $model->recipients = [$post['grupacija'] => $post['recipients']];
                        if (!empty($post['cc_recipients'])) {
                            $model->cc_recipients = [$post['ccgrupacija'] => $post['cc_recipients']];
                        }
                        $model->save();
                    }
                }
            } else {
                $model = new Poruke();
                if ($model->load(Yii::$app->request->post())) {
                    $model->recipients = [$post['grupacija'] => $post['recipients']];
                    if (!empty($post['cc_recipients'])) {
                        $model->cc_recipients = [$post['ccgrupacija'] => $post['cc_recipients']];
                    }
                    $model->save();
                    //za senderov mailbox
                    $temp = new Mailbox();
                    $temp->message = $model->id;
                    $temp->user = $userID;
                    $temp->mstatus = $status->id;
                    $temp->folder = $folder->id;
                    $temp->save();
                }
            }
        }
        return $this->redirect(['mailbox/index']);
    }

    public function actionOpenTemplate($id)
    {
        $model = $this->findModel($id);
        $userID = Yii::$app->user->id;
        $folders = MsgFolders::find()->where(['in', 'user', [0, Yii::$app->user->id]])->orderBy('redoslijed')->all();
        $reply = false;
        $reply_all = false;
        $attachments_old = $model->attachments;

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Poruke');
            $model_new = new Poruke();
            $model_new->recipients = [$post['grupacija'] => $post['recipients']];
            if (!empty($post['cc_recipients'])) {
                $model_new->cc_recipients = [$post['ccgrupacija'] => $post['cc_recipients']];
            }
            $model_new->priority = $post['priority'];
            $model_new->vrsta_poruke = $post['vrsta_poruke'];
            $model_new->status = $post['status'];
            $model_new->subject = $post['subject'];
            $model_new->message = $post['message'];
            $model_new->sender = $post['sender'];
            $model_new->save();
            if (empty($model_new->attachments['attachments'])) {
                $model_new->attachments = $attachments_old;
                $model_new->save();
            }
            //za senderov mailbox
            $temp = new Mailbox();
            $temp->message = $model_new->id;
            $temp->user = $userID;
            $temp->mstatus = 2;
            $folderSent = MsgFolders::find()->where(['folder_name' => 'sent'])->one();
            $temp->folder = $folderSent->id;
            $temp->save();
            switch ($post['grupacija']) {
                case 'roditelji':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['recipients']])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'skupine':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['recipients']])->select('dijete')->all();
                        $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                        $roditeljiSvi = array();
                        foreach ($roditelji as $rod) {
                            $roditeljiSvi[] = $rod->id;
                        }
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    }
                    break;
                case 'zaposlenici':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        var_dump($post['recipients']);
                        $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['recipients']])->asArray()->all();
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'pojedini':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['recipients']])->andWhere(['!=', 'status', 3])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'admin':
                    if ($post['recipients'][0] == 0) {
                        $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                        $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = User::find()->where(['in', 'id', $post['recipients']])->all();

                        foreach ($kojiUseri as $item) {
                            //print("<pre>".print_r($item,true)."</pre>");die();
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
            }
            if (isset($model->cc_recipients)) {
                switch ($post['ccgrupacija']) {
                    case 'roditelji':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['cc_recipients']])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['cc_recipients']])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            var_dump($post['cc_recipients']);
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['cc_recipients']])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['cc_recipients']])->andWhere(['!=', 'status', 3])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'admin':
                        if ($post['cc_recipients'][0] == 0) {
                            $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                            $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = User::find()->where(['in', 'id', $post['cc_recipients']])->all();

                            foreach ($kojiUseri as $item) {
                                //print("<pre>".print_r($item,true)."</pre>");die();
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            return $this->redirect(['mailbox/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'folders' => $folders,
            'reply' => $reply,
            'reply_all' => $reply_all,
            'replyId' => null
        ]);
    }

    public function actionOpenDraft($id)
    {
        $model = $this->findModel($id);
        $userID = Yii::$app->user->id;
        $folders = MsgFolders::find()->where(['in', 'user', [0, Yii::$app->user->id]])->orderBy('redoslijed')->all();
        $reply = false;
        $reply_all = false;
        $attachments_old = $model->attachments;

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Poruke');
            $model_new = new Poruke();
            $model_new->recipients = [$post['grupacija'] => $post['recipients']];
            if (!empty($post['cc_recipients'])) {
                $model_new->cc_recipients = [$post['ccgrupacija'] => $post['cc_recipients']];
            }
            $model_new->priority = $post['priority'];
            $model_new->vrsta_poruke = $post['vrsta_poruke'];
            $model_new->status = $post['status'];
            $model_new->subject = $post['subject'];
            $model_new->message = $post['message'];
            $model_new->sender = $post['sender'];
            $model_new->save();
            if (empty($model_new->attachments['attachments'])) {
                $model_new->attachments = $attachments_old;
                $model_new->save();
            }
            //za senderov mailbox
            $temp = new Mailbox();
            $temp->message = $model_new->id;
            $temp->user = $userID;
            $temp->mstatus = 2;
            $folderSent = MsgFolders::find()->where(['folder_name' => 'sent'])->one();
            $temp->folder = $folderSent->id;
            $temp->save();
            switch ($post['grupacija']) {
                case 'roditelji':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['recipients']])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'skupine':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['recipients']])->select('dijete')->all();
                        $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                        $roditeljiSvi = array();
                        foreach ($roditelji as $rod) {
                            $roditeljiSvi[] = $rod->id;
                        }
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    }
                    break;
                case 'zaposlenici':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        var_dump($post['recipients']);
                        $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['recipients']])->asArray()->all();
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'pojedini':
                    if ($post['recipients'][0] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['recipients']])->andWhere(['!=', 'status', 3])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'admin':
                    if ($post['recipients'][0] == 0) {
                        $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                        $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = User::find()->where(['in', 'id', $post['recipients']])->all();

                        foreach ($kojiUseri as $item) {
                            //print("<pre>".print_r($item,true)."</pre>");die();
                            $temp = new Mailbox();
                            $temp->message = $model_new->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
            }
            if (isset($model->cc_recipients)) {
                switch ($post['ccgrupacija']) {
                    case 'roditelji':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['cc_recipients']])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['cc_recipients']])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            var_dump($post['cc_recipients']);
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['cc_recipients']])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['cc_recipients']])->andWhere(['!=', 'status', 3])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'admin':
                        if ($post['cc_recipients'][0] == 0) {
                            $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                            $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = User::find()->where(['in', 'id', $post['cc_recipients']])->all();

                            foreach ($kojiUseri as $item) {
                                //print("<pre>".print_r($item,true)."</pre>");die();
                                $temp = new Mailbox();
                                $temp->message = $model_new->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                }
            }
            $status = Mstatus::find()->where(['status' => 'saved'])->one();
            $folder = MsgFolders::find()->where(['folder_name' => 'drafts'])->one();
            $draft = Mailbox::find()->where(['message' => $model->id, 'folder' => $folder->id, 'mstatus' => $status->id])->one();
            $draft->delete();

            return $this->redirect(['mailbox/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'folders' => $folders,
            'reply' => $reply,
            'reply_all' => $reply_all,
            'replyId' => null
        ]);
    }

    public function actionReply($id)
    {
        $model = new Poruke();
        $userID = Yii::$app->user->id;
        $folders = MsgFolders::find()->where(['in', 'user', [0, Yii::$app->user->id]])->orderBy('redoslijed')->all();
        $reply = true;
        $reply_all = false;

        $received_msg = Poruke::find()->where(['id' => $id])->one();
        $sender_id = $received_msg->sender; //User->id
        $sender_group = null;
        $reply_id = null;
        $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => $sender_id])->one();

        if (isset($zaposlenik)) {
            $sender_group = 'pojedini';
            $reply_id = $zaposlenik->id;
        } else if (!isset($zaposlenik)) {
            $user_roditelj = \common\models\UserRoditelj::find()->where(['user' => $sender_id])->one();
            $roditelj = null;
            if (isset($user_roditelj)) {
                $roditelj = \common\models\Roditelj::find()->where(['id' => $user_roditelj->roditelj])->one();
            }
            if (isset($roditelj)) {
                $sender_group = 'roditelji';
                $reply_id = $roditelj->id;
            } else if (!isset($roditelj)) {
                $user = \common\models\User::find()->where(['id' => $sender_id])->one();
                if (isset($user)) {
                    $sender_group = 'admin';
                    $reply_id = $user->id;
                }
            }
        }
        $model->recipients = [$sender_group => [$reply_id]];
        $model->subject = 'Re: ' . $received_msg->subject;
        $model->message = '<br/><br/>----------<br/>' . $received_msg->message;

        if ($model->load(Yii::$app->request->post())) {
            $model->recipients = [$sender_group => [$reply_id]];
            $model->save();
            //za senderov mailbox
            $temp = new Mailbox();
            $temp->message = $model->id;
            $temp->user = $userID;
            $temp->mstatus = 2;
            $folderSent = MsgFolders::find()->where(['folder_name' => 'sent'])->one();
            $temp->folder = $folderSent->id;
            $temp->save();
            switch ($sender_group) {
                case 'roditelji':
                    if ($reply_id != 0) {
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $reply_id])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'pojedini':
                    if ($reply_id != 0) {
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', $reply_id])->andWhere(['!=', 'status', 3])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'admin':
                    if ($reply_id != 0) {
                        $kojiUseri = User::find()->where(['in', 'id', $reply_id])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
            }
            return $this->redirect(['mailbox/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'folders' => $folders,
            'reply' => $reply,
            'reply_all' => $reply_all,
            'replyId' => $id,
        ]);
    }

    public function actionReplyAll($id)
    {
        $model = new Poruke();
        $userID = Yii::$app->user->id;
        $folders = MsgFolders::find()->where(['in', 'user', [0, Yii::$app->user->id]])->orderBy('redoslijed')->all();
        $reply = false;
        $reply_all = true;

        //find user role and group
        $user_group = null;
        $user_role = Zaposlenik::find()->where(['user' => $userID])->one();
        if (isset($user_role)) {
            $user_group = 'pojedini';
        } else {
            $sender_roditelj_user = \common\models\UserRoditelj::find()->where(['user' => $userID])->one();
            if (isset($sender_roditelj_user)) {
                $user_role = \common\models\Roditelj::find()->where(['id' => $sender_roditelj_user->roditelj])->one();
                $user_group = 'roditelji';
            } else {
                $user_role  = \common\models\User::find()->where(['id' => $userID])->one();
                if (isset($user_role)) {
                    $user_group = 'admin';
                }
            }
        }

        //find sender and sender role
        $received_msg = Poruke::find()->where(['id' => $id])->one();
        $sender_id = $received_msg->sender; //User->id
        $sender_group = null;
        $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => $sender_id])->one();
        $reply_id = [];
        if (isset($zaposlenik)) {
            $sender_group = 'pojedini';
            $reply_id['sender'] = $zaposlenik->id;
        } else if (!isset($zaposlenik)) {
            $user_roditelj = \common\models\UserRoditelj::find()->where(['user' => $sender_id])->one();
            $roditelj = null;
            if (isset($user_roditelj)) {
                $roditelj = \common\models\Roditelj::find()->where(['id' => $user_roditelj->roditelj])->one();
            }
            if (isset($roditelj)) {
                $sender_group = 'roditelji';
                $reply_id['sender'] = $roditelj->id;
            } else if (!isset($roditelj)) {
                $user = \common\models\User::find()->where(['id' => $sender_id])->one();
                if (isset($user)) {
                    $sender_group = 'admin';
                    $reply_id['sender'] = $user->id;
                }
            }
        }
        $model->subject = 'Re: ' . $received_msg->subject;
        $model->message = '<br/><br/>----------<br/>' . $received_msg->message;

        //find all recipients (recipients + cc_recipients)
        $receiver_grupacije = array_keys($received_msg->recipients);
        $ccgrupacija = null;
        $ccrecipients = null;

        //sender
        if ($sender_group != $user_group) {
            $model->recipients = [$sender_group => [$reply_id['sender']]];
        } else {
            if ($reply_id['sender'] != $user_role->id) {
                $model->recipients = [$sender_group => [$reply_id['sender']]];
            } else {
                $model->recipients = [];
            }
        }

        //check for same recipients, so it doesn't send double messages
        foreach ($receiver_grupacije as $receiver_grupacija) {
            $receiver_svi = $received_msg->recipients[$receiver_grupacija];
            if ($receiver_grupacija == $user_group) {
                if (in_array($user_role->id, $receiver_svi)) {
                    $key = array_search($user_role->id, $receiver_svi);
                    unset($receiver_svi[$key]);
                }
            }
            switch ($receiver_grupacija) {
                case 'roditelji':
                case 'skupine':
                case 'zaposlenici':
                case 'pojedini':
                case 'admin':
                    foreach ($receiver_svi as $receiver) {
                        $reply_id['others'] = $receiver;
                    }
                    break;
            }
            if (isset($reply_id['others'])) {
                $model->recipients = array_merge($model->recipients, [$receiver_grupacija => [$reply_id['others']]]);
            }
        }

        //check cc_recipients
        if (isset($received_msg->cc_recipients)) {
            $ccgrupacija = array_keys($received_msg->cc_recipients)[0];
            $ccrecipients = $received_msg->cc_recipients[$ccgrupacija];
            if ($ccgrupacija != $user_group) {
                $model->recipients = array_merge($model->recipients, [$ccgrupacija => $ccrecipients]);
            } else {
                if (in_array($user_role->id, $ccrecipients)) {
                    $key = array_search($user_role->id, $ccrecipients);
                    unset($ccrecipients[$key]);
                }
                $model->recipients = array_merge($model->recipients, [$ccgrupacija => $ccrecipients]);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            //za senderov mailbox
            $temp = new Mailbox();
            $temp->message = $model->id;
            $temp->user = $userID;
            $temp->mstatus = 2;
            $folderSent = MsgFolders::find()->where(['folder_name' => 'sent'])->one();
            $temp->folder = $folderSent->id;
            $temp->save();
            foreach ($model->recipients as $grupacija => $id) {
                switch ($grupacija) {
                    case 'roditelji':
                        if ($id[0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $id])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($id[0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $id])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($id[0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            var_dump($reply_id['others']);
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $id])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($id[0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $id])->andWhere(['!=', 'status', 3])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'admin':
                        if ($id[0] == 0) {
                            $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                            $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = User::find()->where(['in', 'id', $id])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                }
            }

            return $this->redirect(['mailbox/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'folders' => $folders,
            'reply' => $reply,
            'reply_all' => $reply_all,
            'replyId' => $id,
        ]);
    }

    public function actionRestore($id)
    {
        $userId = Yii::$app->user->id;
        //$model = Poruke::find()->where(['id' => $id])->one();
        $message = Mailbox::find()->where(['message' => $id])->andWhere(['user' => $userId])->andWhere(['!=', 'mstatus', 4])->one();

        if (isset($message)) {
            //if ($userId == $model->sender) {
            //    $message->updateAttributes(['folder' => 4]);
            //} else {
            $message->updateAttributes(['folder' => 1]);
            //}
        }
        return $this->redirect(['mailbox/index', 'folder' => 'inbox']);
    }

    /**
     * Updates an existing Poruke model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Poruke model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Poruke model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Poruke the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poruke::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionGrupacije()
    { {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $pedGodina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
            $pedGodina = $pedGodina->id;
            $out = array();
            if (isset($_POST['depdrop_parents'])) {
                $ids = $_POST['depdrop_parents'];
                $grupacija = $ids; // empty($ids[0]) ? null : $ids[0]; ukoliko je multiple

                switch ($grupacija[0]) {
                    case 'roditelji':
                        $roditelji = Roditelj::find()->all();
                        //vidjeti da je pedagoska godina
                        $out[] = ['id' => 0, 'name' => Yii::t('app', 'Svi roditelji')];
                        foreach ($roditelji as $roditelj) {
                            $out[] = ['id' => $roditelj->id, 'name' => $roditelj->prezime . ' ' . $roditelj->ime];
                        }
                        break;
                    case 'skupine':
                        $skupine = Skupina::find()->where(['ped_godina' => $pedGodina])->all();
                        //vidjeti da je pedagoska godina
                        $out[] = ['id' => 0, 'name' => Yii::t('app', 'Sve skupine')];
                        foreach ($skupine as $skupina) {
                            $out[] = ['id' => $skupina->id, 'name' => $skupina->naziv];
                        }
                        break;
                    case 'zaposlenici':
                        $radno_mjesto = \common\models\RadnoMjesto::find()->all();
                        //vidjeti da je pedagoska godina
                        $out[] = ['id' => 0, 'name' => Yii::t('app', 'Sva radna mjesta')];
                        foreach ($radno_mjesto as $rmjesto) {
                            $out[] = ['id' => $rmjesto->id, 'name' => $rmjesto->naziv];
                        }
                        break;
                    case 'pojedini':
                        //$aktivan=StatusOpce::find()->where(['naziv'=>'Aktivno'])->one();// zakomentirano jer nije unesen status u bazama vrtića
                        $zaposlenici = \common\models\Zaposlenik::find()->where(['!=', 'status', 3])->all(); //->where(['status'=>$aktivan->id])->all();
                        //vidjeti da je pedagoska godina
                        $out[] = ['id' => 0, 'name' => Yii::t('app', 'Svi zaposlenici')];
                        foreach ($zaposlenici as $zaposlenik) {
                            $out[] = ['id' => $zaposlenik->id, 'name' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
                        }
                        break;
                    case 'admin':
                        $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                        //print("<pre>".print_r($admini,true)."</pre>");die();
                        $out[] = ['id' => 0, 'name' => Yii::t('app', 'Svi administratori')];
                        foreach ($admin_ids as $admin_id) {
                            $admin_user = User::find()->where(['id' => $admin_id])->one();
                            $out[] = ['id' => $admin_user->id, 'name' => $admin_user->username];
                        }
                        break;
                }
                return ['output' => $out, 'selected' => ''];
            }
            return ['output' => [], 'selected' => ''];
        }
    }

    public function actionTrash($id, $user, $folder)
    {
        $models = Mailbox::find()->where(['user' => $user])->andWhere(['message' => $id])->andWhere(['folder' => $folder])->all();
        foreach ($models as $model) {
            if ($folder == 1) {
                $model->updateAttributes(['folder' => 4]);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Poruka izbrisana.'));
            } else {
                $model->updateAttributes(['mstatus' => 4]);
                Yii::$app->session->setFlash('success', Yii::t('app', 'Poruka izbrisana.'));
            }
        }
        $fol = MsgFolders::find()->where(['id'=>$folder])->one();

        return $this->redirect(['mailbox/index', 'folder' => $fol->folder_name]);
    }

    public function actionDeleteAllTrash($id)
    {
        $model = Mailbox::find()->where(['user' => $id])->andWhere(['folder' => 5])->all();
        foreach ($model as $item) {
            $item->updateAttributes(['mstatus' => 4]);
        }
        Yii::$app->session->setFlash('success', Yii::t('app', 'Sve poruke izbrisane.'));
        return $this->redirect(['mailbox/index', 'folder' => 'trash']);
    }


    public function actionSaveForUpload($id = null, $reply, $reply_all)
    {
        $post = Yii::$app->request->post('Poruke');
        $userID = Yii::$app->user->id;

        if (!$reply && !$reply_all) {
            if (isset($post)) {
                if (!empty($post['id'])) {
                    $mailbox_msg = Mailbox::find()->where(['message' => $post['id']])->one();
                    if (isset($mailbox_msg)) {
                        $model = $this->findModel($post['id']);
                        if ($model->load(Yii::$app->request->post())) {
                            $model->recipients = [$post['grupacija'] => $post['recipients']];
                            if (!empty($post['cc_recipients'])) {
                                $model->cc_recipients = [$post['ccgrupacija'] => $post['cc_recipients']];
                            }
                            $model->save();
                        }
                        return $this->render('uploadfiles', ['model' => $model]);
                    }
                } else {
                    $model = new Poruke();
                    if ($model->load(Yii::$app->request->post())) {
                        $model->recipients = [$post['grupacija'] => $post['recipients']];
                        if (!empty($post['cc_recipients'])) {
                            $model->cc_recipients = [$post['ccgrupacija'] => $post['cc_recipients']];
                        }
                        $model->save();
                    }
                    return $this->render('uploadfiles', ['model' => $model]);
                }
            }
        } else {
            if ($reply) {
                if (isset($post)) {
                    $model = new Poruke();
                    $received_msg = Poruke::find()->where(['id' => $id])->one();
                    $sender_id = $received_msg->sender; //User->id
                    $sender_group = null;
                    $reply_id = null;
                    $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => $sender_id])->one();

                    if (isset($zaposlenik)) {
                        $sender_group = 'pojedini';
                        $reply_id = $zaposlenik->id;
                    } else if (!isset($zaposlenik)) {
                        $user_roditelj = \common\models\UserRoditelj::find()->where(['user' => $sender_id])->one();
                        $roditelj = null;
                        if (isset($user_roditelj)) {
                            $roditelj = \common\models\Roditelj::find()->where(['id' => $user_roditelj->roditelj])->one();
                        }
                        if (isset($roditelj)) {
                            $sender_group = 'roditelji';
                            $reply_id = $roditelj->id;
                        } else if (!isset($roditelj)) {
                            $user = \common\models\User::find()->where(['id' => $sender_id])->one();
                            if (isset($user)) {
                                $sender_group = 'admin';
                                $reply_id = $user->id;
                            }
                        }
                    }
                    if ($model->load(Yii::$app->request->post())) {
                        $model->recipients = [$sender_group => [$reply_id]];
                        $model->save();
                    }
                    return $this->render('uploadfiles', ['model' => $model]);
                }
            } else if ($reply_all) {
                if (isset($post)) {
                    $model = new Poruke();
                    //find user role and group
                    $user_group = null;
                    $user_role = Zaposlenik::find()->where(['user' => $userID])->one();
                    if (isset($user_role)) {
                        $user_group = 'pojedini';
                    } else {
                        $sender_roditelj_user = \common\models\UserRoditelj::find()->where(['user' => $userID])->one();
                        if (isset($sender_roditelj_user)) {
                            $user_role = \common\models\Roditelj::find()->where(['id' => $sender_roditelj_user->roditelj])->one();
                            $user_group = 'roditelji';
                        } else {
                            $user_role  = \common\models\User::find()->where(['id' => $userID])->one();
                            if (isset($user_role)) {
                                $user_group = 'admin';
                            }
                        }
                    }
                    //find sender and sender role
                    $received_msg = Poruke::find()->where(['id' => $id])->one();
                    $sender_id = $received_msg->sender; //User->id
                    $sender_group = null;
                    $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => $sender_id])->one();
                    $reply_id = [];
                    if (isset($zaposlenik)) {
                        $sender_group = 'pojedini';
                        $reply_id['sender'] = $zaposlenik->id;
                    } else if (!isset($zaposlenik)) {
                        $user_roditelj = \common\models\UserRoditelj::find()->where(['user' => $sender_id])->one();
                        $roditelj = null;
                        if (isset($user_roditelj)) {
                            $roditelj = \common\models\Roditelj::find()->where(['id' => $user_roditelj->roditelj])->one();
                        }
                        if (isset($roditelj)) {
                            $sender_group = 'roditelji';
                            $reply_id['sender'] = $roditelj->id;
                        } else if (!isset($roditelj)) {
                            $user = \common\models\User::find()->where(['id' => $sender_id])->one();
                            if (isset($user)) {
                                $sender_group = 'admin';
                                $reply_id['sender'] = $user->id;
                            }
                        }
                    }
                    //find all recipients (recipients + cc_recipients)
                    $receiver_grupacije = array_keys($received_msg->recipients);
                    $ccgrupacija = null;
                    $ccrecipients = null;

                    if ($model->load(Yii::$app->request->post())) {
                        //sender
                        if ($sender_group != $user_group) {
                            $model->recipients = [$sender_group => [$reply_id['sender']]];
                        } else {
                            if ($reply_id['sender'] != $user_role->id) {
                                $model->recipients = [$sender_group => [$reply_id['sender']]];
                            } else {
                                $model->recipients = [];
                            }
                        }

                        //check for same recipients, so it doesn't send double messages
                        foreach ($receiver_grupacije as $receiver_grupacija) {
                            $receiver_svi = $received_msg->recipients[$receiver_grupacija];
                            if ($receiver_grupacija == $user_group) {
                                if (in_array($user_role->id, $receiver_svi)) {
                                    $key = array_search($user_role->id, $receiver_svi);
                                    unset($receiver_svi[$key]);
                                }
                            }
                            switch ($receiver_grupacija) {
                                case 'roditelji':
                                case 'skupine':
                                case 'zaposlenici':
                                case 'pojedini':
                                case 'admin':
                                    foreach ($receiver_svi as $receiver) {
                                        $reply_id['others'] = $receiver;
                                    }
                                    break;
                            }
                            if (isset($reply_id['others'])) {
                                $model->recipients = array_merge($model->recipients, [$receiver_grupacija => [$reply_id['others']]]);
                            }
                        }

                        //check cc_recipients
                        if (isset($received_msg->cc_recipients)) {
                            $ccgrupacija = array_keys($received_msg->cc_recipients)[0];
                            $ccrecipients = $received_msg->cc_recipients[$ccgrupacija];
                            if ($ccgrupacija != $user_group) {
                                $model->recipients = array_merge($model->recipients, [$ccgrupacija => $ccrecipients]);
                            } else {
                                if (in_array($user_role->id, $ccrecipients)) {
                                    $key = array_search($user_role->id, $ccrecipients);
                                    unset($ccrecipients[$key]);
                                }
                                $model->recipients = array_merge($model->recipients, [$ccgrupacija => $ccrecipients]);
                            }
                        }
                        $model->save();
                    }
                    return $this->render('uploadfiles', ['model' => $model]);
                }
            }
        }
    }

    public function actionSendMessageWithAttachments($id)
    {
        $model = $this->findModel($id);
        //$reply = false;
        $recipients = $model->recipients;
        $ccrecipients = $model->cc_recipients;
        $userID = Yii::$app->user->id;

        //za senderov mailbox
        $temp = new Mailbox();
        $temp->message = $model->id;
        $temp->user = $userID;
        $temp->mstatus = 2;
        $folderSent = MsgFolders::find()->where(['folder_name' => 'sent'])->one();
        $temp->folder = $folderSent->id;
        $temp->save();

        if (count($recipients) == 1) {
            switch (key($recipients)) {
                case 'roditelji':
                    if ($recipients['roditelji'][0] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $recipients['roditelji']])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'skupine':
                    if ($recipients['skupine'][0] == 0) {
                        $kojiUseri = UserRoditelj::find()->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $recipients['skupine']])->select('dijete')->all();
                        $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                        $roditeljiSvi = array();
                        foreach ($roditelji as $rod) {
                            $roditeljiSvi[] = $rod->id;
                        }
                        $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    }
                    break;
                case 'zaposlenici':
                    if ($recipients['zaposlenici'][0] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $recipients['zaposlenici']])->asArray()->all();
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'pojedini':
                    if ($recipients['pojedini'][0] == 0) {
                        $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = Zaposlenik::find()->where(['in', 'id', $recipients['pojedini']])->andWhere(['!=', 'status', 3])->all();

                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->user;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
                case 'admin':
                    if ($recipients['admin'][0] == 0) {
                        $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                        $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                        foreach ($kojiUseri as $item) {
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            $temp->save();
                        }
                    } else {
                        $kojiUseri = User::find()->where(['in', 'id', $recipients['admin']])->all();

                        foreach ($kojiUseri as $item) {
                            //print("<pre>".print_r($item,true)."</pre>");die();
                            $temp = new Mailbox();
                            $temp->message = $model->id;
                            $temp->user = $item->id;
                            $temp->mstatus = 2;
                            $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                            $temp->folder = $folderInbox->id;
                            if ($temp->save()) {
                            } else {
                                var_dump($temp->errors);
                                die();
                            };
                        }
                    }
                    break;
            }
        } else {
            foreach ($recipients as $key => $value) {
                switch ($key) {
                    case 'roditelji':
                        if ($value[0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $value])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($value[0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $value])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($value[0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $value])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($value[0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $value])->andWhere(['!=', 'status', 3])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'admin':
                        if ($value[0] == 0) {
                            $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                            $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = User::find()->where(['in', 'id', $value])->all();

                            foreach ($kojiUseri as $item) {
                                //print("<pre>".print_r($item,true)."</pre>");die();
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                }
            }
        }
        if (isset($ccrecipients)) {
            if (count($ccrecipients) == 1) {
                switch (key($ccrecipients)) {
                    case 'roditelji':
                        if ($ccrecipients['roditelji'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $ccrecipients['roditelji']])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($ccrecipients['skupine'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $ccrecipients['skupine']])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($ccrecipients['zaposlenici'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $ccrecipients['zaposlenici']])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($ccrecipients['pojedini'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $ccrecipients['pojedini']])->andWhere(['!=', 'status', 3])->all();

                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->user;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'admin':
                        if ($ccrecipients['admin'][0] == 0) {
                            $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                            $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                $temp->save();
                            }
                        } else {
                            $kojiUseri = User::find()->where(['in', 'id', $ccrecipients['admin']])->all();

                            foreach ($kojiUseri as $item) {
                                //print("<pre>".print_r($item,true)."</pre>");die();
                                $temp = new Mailbox();
                                $temp->message = $model->id;
                                $temp->user = $item->id;
                                $temp->mstatus = 2;
                                $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                $temp->folder = $folderInbox->id;
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                }
            } else {
                foreach ($ccrecipients as $key => $value) {
                    switch ($key) {
                        case 'roditelji':
                            if ($value[0] == 0) {
                                $kojiUseri = UserRoditelj::find()->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->user;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    $temp->save();
                                }
                            } else {
                                $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $value])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->user;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            }
                            break;
                        case 'skupine':
                            if ($value[0] == 0) {
                                $kojiUseri = UserRoditelj::find()->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->user;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    $temp->save();
                                }
                            } else {
                                $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $value])->select('dijete')->all();
                                $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                                $roditeljiSvi = array();
                                foreach ($roditelji as $rod) {
                                    $roditeljiSvi[] = $rod->id;
                                }
                                $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();

                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->user;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    $temp->save();
                                }
                            }
                            break;
                        case 'zaposlenici':
                            if ($value[0] == 0) {
                                $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->user;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    $temp->save();
                                }
                            } else {
                                $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $value])->asArray()->all();
                                $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->user;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            }
                            break;
                        case 'pojedini':
                            if ($value[0] == 0) {
                                $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->user;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    $temp->save();
                                }
                            } else {
                                $kojiUseri = Zaposlenik::find()->where(['in', 'id', $value])->andWhere(['!=', 'status', 3])->all();

                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->user;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            }
                            break;
                        case 'admin':
                            if ($value[0] == 0) {
                                $admin_ids = Yii::$app->authManager->getUserIdsByRole('superadmin');
                                $kojiUseri = User::find()->where(['id' => $admin_ids])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->id;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    $temp->save();
                                }
                            } else {
                                $kojiUseri = User::find()->where(['in', 'id', $value])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new Mailbox();
                                    $temp->message = $model->id;
                                    $temp->user = $item->id;
                                    $temp->mstatus = 2;
                                    $folderInbox = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
                                    $temp->folder = $folderInbox->id;
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            }
                            break;
                    }
                }
            }
        }

        $status = Mstatus::find()->where(['status' => 'saved'])->one();
        $folder = MsgFolders::find()->where(['folder_name' => 'drafts'])->one();
        $draft = Mailbox::find()->where(['message' => $model->id, 'folder' => $folder->id, 'mstatus' => $status->id])->one();
        if (isset($draft)) {
            $draft->delete();
        }

        return $this->redirect(['mailbox/index']);
    }
    public function actionUploadFile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
        } else if (isset($_POST) && isset($_POST['model_id'])) {
            $id = $_POST['model_id'];
            $model = $this->findModel($id);

            if (!file_exists(Yii::getAlias('@backend') . '/web/poruke/' . $id)) {
                mkdir(Yii::getAlias('@backend') . '/web/poruke/' . $id, 0777, true);
            }
            $dir = Yii::getAlias('@backend') . '/web/poruke/' . $id;
            $realAttachments = [];
            if (!empty($model->attachments['attachments'])) {
                foreach ($model->attachments['attachments'] as $item) {
                    $realAttachments[] = $item;
                }
            }
            $model->attachments = UploadedFile::getInstances($model, 'attachments');

            foreach ($model->attachments as $file) {
                $fileName = preg_replace("/\s+/", "_", $file->name);
                if (!in_array($fileName, $realAttachments)) {
                    $realAttachments[] = $fileName;
                }
                $file->saveAs($dir . '/' . $fileName);
            }
            $model->attachments = ['attachments' => $realAttachments];

            if ($model->save()) {
                $preview = $model->getFilePaths($id);
                $config = $model->getFilePathsConfig();
                $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                return json_encode($out);
            } else if (Yii::$app->user->isGuest) {
                $model->delete();
                return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
            }
        } else {
            return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
        }
    }
    public function actionDeleteFile()
    {
        if (isset($_POST)) {
            $id = $_POST['model_id'];
            $key = $_POST['key'];
            $model = $this->findModel($id);
            $allFiles = $model->attachments['attachments'];
            $dir = Yii::getAlias('@backend') . '/web/poruke/' . $id;

            $file = array_search($key, $model->attachments['attachments']);
            if (isset($file)) {
                unset($allFiles[$file]);
                $attachments = array_values($allFiles);
                $model->attachments = ['attachments' => $attachments];
                unlink($dir . '/' . $key);
                $model->save();
            }

            $preview = $model->getFilePaths($id);
            $config = $model->getFilePathsConfig();
            $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];

            return json_encode($out);
        }
    }
}
