<?php

namespace frontend\controllers;

use Yii;
use common\models\Media;
use common\models\Postavke;
use common\models\Search\MediaSearch;
use common\models\MediaRecipients;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * MediaController implements the CRUD actions for Media model.
 */
class MediaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Media models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MediaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Media model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Media model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Media();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Media model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Media model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Media model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Media the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Media::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionDownloadMedia($id)
    {
        $model = $this->findModel($id);
        $file = \Yii::getAlias('@webroot/archive' . microtime() . '.zip');
        $zip = new \ZipArchive();
        $zip->open($file, \ZipArchive::CREATE);

        $weburl = Postavke::find()->where(['postavka' => 'backendWeb'])->one();
        if ($weburl->vrijednost !== null) {
            $admin = $weburl->vrijednost;
        } else {
            $admin = '/admin';
        }

        if (file_exists(Yii::getAlias('@backend/web/media/') . $id)) {
            foreach ($model->attachments as $key => $val) {
                foreach ($val as $k => $v) {
                    if (file_exists(Yii::getAlias('@backend/web/media/')  . $id . '/' . $v)) {
                        $zip->addFile(Yii::getAlias('@backend/web/media/')  . $id . '/' . $v, $v);
                    }
                }
            }
        } else {
            foreach ($model->attachments as $key => $val) {
                foreach ($val as $k => $v) {
                    if (file_exists(Yii::getAlias('@backend/web/media/')  . $v)) {
                        $zip->addFile(Yii::getAlias('@backend/web/media/')  . $v, $v);
                    }
                }
            }
        }

        $zip->close();
        if (file_exists($file)) {
            \Yii::$app->response->sendFile($file, 'media-' . $model->naziv . '.zip');
            ignore_user_abort(true); //delete the temporary file
            if (connection_aborted()) unlink($file);
            register_shutdown_function('unlink', $file);
        }
    }
}
