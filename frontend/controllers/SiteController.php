<?php

namespace frontend\controllers;

use common\models\MsgFolders;
use common\models\Notifikacije;
use common\models\Poruke;
use common\models\Roditelj;
use common\models\Dijete;
use common\models\DijeteSkupina;
use common\models\Skupina;
use common\models\Search\MailboxSearch;
use common\models\Upitnik;
use common\models\User;
use common\models\UserRoditelj;
use common\models\Zahtjev;
use common\models\Prisutnost;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use common\helpers\Utils;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        //see captcha and error added here, this fixes the issue
                        'actions' => ['contact', 'about', 'terms', 'forgot', 'reset-password', 'captcha', 'error'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'actions' => ['logout', 'index', 'katalozi'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $poruke = [];
        if (!Yii::$app->user->isGuest) {
            //$user = User::findOne(Yii::$app->user->id);
            /*$dijete=Zahtjev::find()->where(['user'=>$user->id])->all();
            $djeca=[];
            foreach ($dijete as $dij){
                $djeca[]=$dij->id;
            }
            $roditelji=Roditelj::find()->where(['in','dijete',$djeca])->all();

            $porukeSve=Poruke::find()->all();
            $rodi= [];
            foreach($roditelji as $rod){
                $rodi[]=$rod->id;
            }
            foreach ($porukeSve as $poruka){
                //$temp=json_decode($poruka->recipients);
                if(is_array($poruka->recipients)){
                    $temp=$poruka->recipients;
                if(array_key_exists('roditelji',$poruka->recipients)){
                    foreach ($temp['roditelji'] as $key=>$value){
                        if(in_array($value,$rodi)){
                            echo $value;
                            $poruke[]=$poruka;
                        }
                    }
                }
                }
            }*/
            $user_roditelj = UserRoditelj::find()->where(['user' => Yii::$app->user->id])->one();
            $dijete = null;
            $prisutnostFull = null;
            $poc = null;
            $kraj = null;
            if (isset($user_roditelj)) {
                $roditelj = Roditelj::findOne($user_roditelj->roditelj);
                $dijete = Dijete::findOne($roditelj->dijete);
                $dijeteskupina = DijeteSkupina::find()->orderBy(['id' => SORT_DESC])->where(['dijete' => $dijete->id])->all();

                $skupina = Skupina::find()->where(['id' => $dijeteskupina[0]->skupina])->one();
                $mj = idate('m', strtotime(date('m') . " -1 month"));
                $mjeseci = [0 => 9, 1 => 10, 2 => 11, 3 => 12, 4 => 1, 5 => 2, 6 => 3, 7 => 4, 8 => 5, 9 => 6, 10 => 7, 11 => 8];
                $pedMjesec = array_search($mj, $mjeseci, true);
                $mjesec = Utils::getMjesecPed($pedMjesec);
                $numberDays = cal_days_in_month(CAL_GREGORIAN, $mjesec['mjesec'], date('Y'));
                $poc = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-01';
                $kraj = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $numberDays;
                $zaNaziv = $dijete->ime . ' ' . $dijete->prezime;
                $header = [''];
                $retci = [];
                $ukupnoDani = [];
                for ($i = 1; $i < $numberDays + 1; $i++) {
                    $dan = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $i;
                    if (date('N', strtotime($dan)) >= 6) {
                        $temp = '<span style="color:#eb4b50;font-size:12px">' . $i . '</span>';
                    } else {
                        $temp = '<span style="font-size:10px">' . $i . '</span>';
                    }
                    $ukupnoDani[$i] = 0;
                    $header = array_merge($header, [$temp]);
                }
                $zaNaziv = $dijete->ime;
                $temp = [];
                $temp[0] = '<span style="font-weight: bold;text-align:left" class="left">' . $zaNaziv . '</span>';
                $prisutnost = Prisutnost::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet_id' => $dijete->id, 'entitet' => 'dijete'])->orderBy('datum ASC')->all();
                foreach ($prisutnost as $prist) {
                    $dan = date("j", strtotime($prist->datum));
                    if ($prist->status == 1) {
                        $temp[$dan] = '<i class="glyphicon glyphicon-ok" style="font-size:10px"></i>';
                        if (isset($ukupnoDani[$dan])) {
                            $ukupnoDani[$dan]++;
                        } else {
                            $ukupnoDani[$dan] = 1;
                        }
                    } else {
                        $temp[$dan] = '<i class="glyphicon glyphicon-minus" style="font-size:10px"></i>';
                    }
                }
                $retci[] = $temp;
                $prisutnostFull = array_merge([$header], $retci);
            }





            // ovo nije dobro a upitnike, trebalo bi pokazivati upitnik korisniku ako je njegovo dijete u skupini na koju se odnosi upitnik, ako se odnosi na njegovo baš dijete i ako je za sve roditelje
            $upitniciSvi = Upitnik::find()->where(['status' => 2])->all();
            $upitnici = [];

            foreach ($upitniciSvi as $u) {
                $zaKoga = $u->upitnik_for;

                foreach ($zaKoga as $zk) {
                    if ($zk == 'roditelji') {
                        $upitnici[$u->id] = $u->naziv;
                    }
                    if (isset($skupina) && $zk == $skupina->id) {
                        $upitnici[$u->id] = $u->naziv;
                    }
                }
            }

            //zamijeniti!!!!!!!!!!!!!!!!!

            $searchModel = new MailboxSearch();
            $msgFolder = MsgFolders::find()->where(['folder_name' => 'inbox'])->one();
            $poruke = $searchModel->search(Yii::$app->request->queryParams, $msgFolder->id);
        }
        return $this->render('index', ['poruke' => $poruke, 'upitnici' => $upitnici, 'prisutnost' => $prisutnostFull, 'poc' => $poc, 'kraj' => $kraj]);
    }
    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub
        if (!Yii::$app->user->isGuest) {
            $notifikacije = Notifikacije::find()->where(['user' => Yii::$app->user->id, 'read_at' => null])->all();
            Yii::$app->view->params['notifikacije'] = $notifikacije;
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
}
