<?php

namespace frontend\components\validators;

use yii\validators\Validator;

class OibValidator extends Validator
{

    public function init()
    {
        parent::init();
        $this->message = 'Neispravan OIB.';
    }

    public function validateAttribute($model, $attribute)
    {
        $oib = $model->oib;
        if(!ctype_digit($oib)) {
            $model->addError($attribute, $this->message);
        }
        
        $r = 10;
        for($i = 0; $i < 10; $i++) {
            $x = ($r + (int)$oib[$i]) % 10;
            if($x == 0) {
                $x = 10;
            }
            $r = (2*$x)%11;
        }
        $x = (11 - $r) % 10;
        $kontrolnaZnamenka = (int)$oib[10];
        if(!($x == $kontrolnaZnamenka)) {
            $model->addError($attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return <<<JS
            var oib = value;
            if(oib.length != 11) {
                messages.push('OIB mora sadržavati 11 znakova.');
            }
            else if(isNaN(parseInt(oib))) {
                messages.push($message);
            } else {
                var r = 10;
                var i;
                var x;
                var kontrolnaZnamenka;
                for (i = 0; i < oib.length-1; i++) {
                    x = (r + Number(oib[i])) % 10;
                    if(x == 0) {
                        x = 10;
                    }
                    r = (2*x)%11;
                }
                x = (11 - r) % 10;
                kontrolnaZnamenka = Number(oib[10]);
                if(!(x == kontrolnaZnamenka)) {
                    messages.push($message);
                }
            }
JS;
    }
}