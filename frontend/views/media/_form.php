<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Media */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="media-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opis')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'media_type')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\MediaType::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'size')->textInput() ?>

    <?= $form->field($model, 'filename')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\User::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'recipients')->textInput() ?>

    <?= $form->field($model, 'kategorija')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\KategorijaMedia::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
