<?php
/* @var $this yii\web\View */

use kriss\calendarSchedule\CalendarScheduleWidget;
use yii\web\JsExpression;
?>
<div class="site-about">
    
        <h1> <?= Yii::t('app', 'Kalendar') ?></h1>
    

        <?php
        echo \backend\widgets\calendar\CalendarWidget::widget(['model'=>$events]);
        ?>
    </div>
    </div>