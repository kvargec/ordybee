<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VrstaPrograma */

$this->title = Yii::t('app', 'Create Vrsta Programa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vrsta Programas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrsta-programa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
