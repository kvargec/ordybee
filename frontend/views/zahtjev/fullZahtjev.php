<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
$zahtjevId=$zahtjev->id;

$teskoceData = ['a' => Yii::t('app','oštećenja vida'),
    'b' => Yii::t('app','oštećenja sluha'),
    'c' => Yii::t('app','poremećaji govorno –glasovne komunikacije i specifične teškoće u učenju'),
    'd' => Yii::t('app','tjelesni invaliditet'),
    'e' => Yii::t('app','intelektualne teškoće (sindromi…)'),
    'f' => Yii::t('app','poremećaji u ponašanju uvjetovani organskim faktorima, ADHD'),
    'g' => Yii::t('app','poremećaj socijalne komunikacije; poremećaj iz autističnog spektra; autizam'),
    'h' => Yii::t('app','postojanje više vrsta i stupnjeva teškoća u psihofizičkom razvoju')];

$dijagnostikaData = [
    'a' => Yii::t('app','vještačenjem stručnog povjerenstva socijalne skrbi'),
    'b' => Yii::t('app','nalazom i mišljenjem specijalizirane ustanove'),
    'c' => Yii::t('app','nalazom i mišljenjem ostalih stručnjaka')
];
?>
<div class="shop-create site-about">

    <h1><?= Yii::t('app','Podaci u zahtjevu za upis')?></h1>

    <div class="col s12 m12 l4">

        <blockquote>
            <?php $vrtic=\common\models\Postavke::find()->where(['postavka'=>'nazivVrtica'])->one()?>
            <p><?= Yii::t('app','Unijeli ste sve potrebne podatke za predaju zahtjeva za upis djeteta u ').$vrtic->vrijednost ?><br><?= Yii::t('app','Nakon što pregledate sve podatke, pošaljite prijavu za upisom na poveznicu Pošalji zahtjev')?>  </p>
        </blockquote>


    </div>

    <div class="col s12 m12 l8">
        <h4><?= Yii::t('app','Pregled unešenih podataka')?></h4>
        <div class="panel panel-success">
            <div class="panel-heading">
                <?php echo $zahtjev->dijete0->ime.', '.$zahtjev->dijete0->prezime.' (Željeni datum upisa:'.$zahtjev->zeljeni_datum.') '; ?>
            </div>
            <div class="panel-body">

                <?= DetailView::widget([
                    'model' => $dijete,
                    'attributes' => [
                        'ime',
                        'prezime',
                        'drzavljanstvo',
                        'spol',
                        'dat_rod',
                        'oib',
                        [
                            'label'=>'Mjesto stanovanja/boravišta',
                            'format'=>'html',
                            'value'=>function($model){
                                $temp='';
                                $temp.=$model->adresa.'<br/>';
                                if(!empty($model->cetvrt)){
                                    return $temp.$model->mjesto.', '.$model->cetvrt;
                                }else{
                                    return $temp.$model->mjesto;
                                }
                            }
                        ],
                        [
                            'label'=>'Prebivalište',
                            'format'=>'html',
                            'value'=>function($model){
                                $temp='';
                                $temp.=$model->adresa.'<br/>';

                                return $temp.$model->mjesto;

                            },
                            'visible'=>$dijete->prebivaliste_jednako_boraviste!='D'
                        ],

                        'sestra',
                        'cekanje',
                        'god_cekanja',
                        'razvoj',
                        'vrstaPrograma.vrsta',
                        [
                            'attribute' => 'teskoce',
                            'format' => 'html',
                            'value' => function ($data ) use ($teskoceData){

                                if (!empty($data->teskoce) and $data->teskoce!=''){
                                    $return = '';
                                    if(is_array(json_decode($data->teskoce))){
                                        foreach (json_decode($data->teskoce) as $item) {
                                            $return.= $teskoceData[$item].'<br>';
                                        }
                                    }


                                }
                                return $return;
                            }
                        ],
                        [
                            'attribute' => 'dijagnostika',
                            'format' => 'html',
                            'value' => function ($data ) use ($dijagnostikaData){
                                if (!empty($data->dijagnostika)){
                                    $return = '';
                                    if(is_array(json_decode($data->dijagnostika))) {
                                        foreach (json_decode($data->dijagnostika) as $item) {
                                            $return .= $dijagnostikaData[$item] . '<br>';
                                        }
                                    }
                                }
                                return $return;
                            }
                        ],
                        'druge_potrebe',
                        'posebne',
                    ],
                ]) ?>
            </div>

        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <?= Yii::t('app','Roditelji')?>

            </div>
            <div class="panel-body">
                <?= yii\grid\GridView::widget([
                    'dataProvider' => $roditelji,
                    'columns' => [
                        [
                            'header'=>'Ime i prezime <br /> OIB',
                            'format'=>'html',
                            'value'=>function($data){
                                return $data->ime.', '.$data->prezime.'<br />'.$data->oib;
                            }
                        ],
                        [
                            'header'=>'Datum rođ.  <br />državljanstvo',
                            'format'=>'html',
                            'value'=>function($data){
                                return $data->dat_rod.'<br />'.$data->drzavljanstvo;
                            }
                        ],

                        [
                            'header'=>'Mjesto stanovanja<br />&nbsp;',
                            'format'=>'html',
                            'value'=>function($model){
                                $temp='';
                                $temp.=$model->adresa.'<br/>';
                                if(!empty($model->cetvrt)){
                                    return $temp.$model->mjesto.', '.$model->cetvrt;
                                }else{
                                    return $temp.$model->mjesto;
                                }
                            }
                        ],
                        [
                            'header'=>'Prebivalište<br />&nbsp;',
                            'format'=>'html',
                            'value'=>function($model){
                                $temp='';
                                $temp.=$model->adresa.'<br/>';

                                return $temp.$model->prebivaliste;

                            },
                            'visible'=>$dijete->prebivaliste_jednako_boraviste!='D'
                        ],
                        [
                            'header'=>'Kontakt<br />&nbsp;',
                            'format'=>'html',
                            'value'=>function($data){
                                return $data->mobitel.'<br />'.$data->email;
                            }
                        ],
                        [
                            'header'=>'Poslodavac<br />&nbsp;',
                            'format'=>'html',
                            'value'=>function($data){
                                $temp=$data->poslodavaca.'<br />'.$data->adresa_poslodavca;

                                return $temp;
                            }
                        ],
                        [
                            'header'=>'Zaposlenje<br />&nbsp;',
                            'format'=>'html',
                            'value'=>function($data){

                                $temp=$data->zanimanje.'<br />'.$data->radno.'';
                                return $temp;
                            }
                        ]


                    ],
                    'showHeader' => true,
                    'summary'=>'',
                ])
                ?>

            </div>

        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <?= Yii::t('app','Ostala djeca u obitelji')?>

            </div>
            <div class="panel-body">
                <?= yii\grid\GridView::widget([
                    'dataProvider' => $siblings,
                    'columns' => [
                        'ime',
                        'prezime',
                        'spol',
                        'dat_rod',
                        'oib',
                        'prebivaliste',
                    ],
                    'showHeader' => true,
                    'summary'=>'',
                ])
                ?>
            </div>

        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <?= Yii::t('app','Dodani dokumenti')?>
            </div>
            <div class="panel-body">
                <?= yii\grid\GridView::widget([
                    'dataProvider' => $documents,
                    'columns' => [
                        'naziv',
                        'filename',
                    ],
                    'showHeader' => true,
                    'summary'=>'',
                ])
                ?>
            </div>
            </div>
        </div>

        <!-- <p>OSTALA DOKUMENTACIJA :</p> -->
        <hr>
        <strong><?= Yii::t('app','Roditelj na razgovor treba ponijeti:')?></strong><br><br>
        <ul>
            <li><?= Yii::t('app','Potvrdu o obavljenom sistematskom pregledu')?></li>
            <li><?= Yii::t('app','Presliku iskaznice imunizacije te original iskaznicu na uvid')?></li>
            <li><?= Yii::t('app','Kopiju zdravstvene iskaznice djeteta')?></li>
            <li><?= Yii::t('app','Kopiju medicinske/logopedske dokumentacije (ukoliko postoji)')?></li>
            <li><?= Yii::t('app','Kopiju rješenje Centra za socijalnu skrb te nalaze i mišljenja tijela vještačenja (ukoliko
                postoji)')?></li>
        </ul>
        <hr />

        <strong><?php
            $izjava=\common\models\Postavke::find()->where(['postavka'=>'izjava'])->one();
            if(!empty($izjava)) {echo $izjava->vrijednost; }

            ?><br>
            <?= Yii::t('app','PREDAJOM ZAHTJEVA DIJETE NIJE UPISANO U DJEČJI VRTIĆ – DIJETE JE UPISANO U VRTIĆ U TRENUTKU KAD
                    RODITELJ POTPIŠE UGOVOR S DJEČJIM VRTIĆEM')?><br>
            <?= Yii::t('app','DJEČJI VRTIĆ ZADRŽAVA PRAVO RASPOREDA DJETETA U SKUPINE I OBJEKTE')?><br></strong><br>


        <div class="shop-form">
            <div class="col s12 m12 l12">

                <div class="col s12 m12 l6">
                </div>
                <div class="col s12 m12 l6">
                </div>

            </div>
        </div>
    </div>
</div>
