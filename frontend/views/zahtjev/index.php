<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZahtjevSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zahtjevi za upis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zahtjev-index site-about">

    <h1><?= Html::encode($this->title) ?> <?= Html::a(Yii::t('app', 'Izradi novi zahtjev'), ['create'], ['class' => 'btn btn-primary float-r']) ?></h1>

   

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            // 'urbroj',
            [
                'value' => 'urbroj',
                'label' => Yii::t('app', 'Šifra djeteta'),
                'attribute' => 'urbroj',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Šifra djeteta'),
                    'class' => 'form-control'
                ]
            ],
            // 'dijete0.ime',
            [
                'value' => 'dijete0.ime',
                'label' => Yii::t('app', 'Ime'),
                'attribute' => 'dijete.ime',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Ime'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value' => 'dijete0.prezime',
                'label' => Yii::t('app', 'Prezme'),
                'attribute' => 'dijete.prezime',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Prezime'),
                    'class' => 'form-control'
                ]
            ],
            // 'dijete0.prezime',
            [
                'label'=>Yii::t('app','Izrađen'),
                'value'=>function($data){
                    return yii::$app->formatter->asDate($data->create_at,'php:d.m.Y. H:i');
                }

            ],
            'status0.status',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {download}  {print}',
                // 'visible' => Yii::$app->user->can('admin'),
                'buttons' => [

                    'update' => function ($url, $data) {

                        $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/korak7', 'id' => $data->id]);
                        return Html::a('<span class="material-icons">edit</span>', $url2, [
                            'title' => Yii::t('app', 'Uredi zahtjev'),
                            'class' => 'text-right block'
                        ]);
                    },

                    'download' => function ($url, $data) {

                        $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/mojapotvrda', 'id' => $data->id]);
                        return Html::a('<span class="material-icons">download</span>', $url2, [
                            'title' => Yii::t('app', 'Skini potvrdu'),
                            'class' => 'text-left block',
                            'target' => '_blank',
                        ]);
                    },
                    'print' => function ($url, $data) {

                        $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/pdfzahtjev', 'id' => $data->id]);
                        return Html::a('<span class="material-icons">print</span>', $url2, [
                            'title' => Yii::t('app', 'Skini potvrdu'),
                            'class' => 'text-left block',
                            'target' => '_blank',
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>


</div>
