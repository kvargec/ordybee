<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Zahtjev */

$this->title = Yii::t('app', 'Predaja zahtjev');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zahtjevi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-create site-about">
    <?= $this->render('_form', [
            'model'=>$model,
        'modelDijete' => $modelDijete,
    ]) ?>

</div>
