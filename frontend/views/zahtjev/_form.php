<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>

<h1><?= Yii::t('app', 'Predaja novog zahtjeva') ?></h1>
<p><?= Yii::t('app', 'Predaja zahtjeva obavlja se u nekoliko koraka tijekom kojih možete ispraviti pogrešno unešene podatke.') ?></p>
<div class="col s12 m12 l4">
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
            </div>

        </div>
    </div>
    <blockquote>
        <p><?= Yii::t('app', 'Prvi korak u predaji zahtjeva je&nbsp;<strong>popunjavanje osnovnih podataka o Vašem djetetu.') ?></strong></p>
    </blockquote>

</div>
<div class="col s12 m12 l8">
    <h2 class=""><?= Yii::t('app', 'Upišite osnovne podatke o djetetu') ?></h2>
    <div class="row">
        <div class="col s12 m12">

        </div>
    </div>

    <div class="shop-form">

        <?php $form = ActiveForm::begin(); ?>

        <?php // $form->field($model, 'id')->textInput() 
        ?>
        <div class="input-field">
            <?= $form->field($modelDijete, 'ime')->textInput()->label(Yii::t('app', "Ime djeteta")) ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($modelDijete, 'prezime')->textInput()->label(Yii::t('app', "Prezime djeteta")) ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($modelDijete, 'spol')->radioList([
                'M' => Yii::t('app', 'muški'),
                'Ž' => Yii::t('app', 'ženski')
            ])->label(Yii::t('app', 'Spol')); ?>
        </div>
        <br />
        <div class="input-field">
            <?= DatePicker::widget([
                'model' => $modelDijete,
                'attribute' => 'dat_rod',
                'convertFormat' => true,
                'options' => ['placeholder' => Yii::t('app', 'Datum rođenja djeteta')],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'autoclose' => true
                ]
            ]);
            ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($modelDijete, 'oib')->textInput()->label(Yii::t('app', "OIB djeteta")) ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($modelDijete, 'mjesto_rod')->textInput()->label(Yii::t('app', "Mjesto rođenja")) ?>
        </div>
        <br />
        <?= $form->field($modelDijete, 'drzavljanstvo')->textInput()->label(Yii::t('app', "Državljanstvo")) ?>

        <div class="input-field">

            <?= $form->field($modelDijete, 'adresa')->textInput()->label(Yii::t('app', "Adresa boravišta")) ?>

        </div>
        <br />
        <div class="input-field">
            <?= $form->field($modelDijete, 'mjesto')->textInput()->label(Yii::t('app', "Grad / općina")) ?>
        </div>

        <?php
        $cetvrti = \common\models\Postavke::find()->where(['postavka' => 'cetvrti'])->one();
        if (!empty($cetvrti) and $cetvrti->vrijednost !== 'false') {
            $popis = $cetvrti->dodatno[0];

            echo '<div class="input-field">';
            echo $form->field($modelDijete, 'cetvrt')->dropDownList($popis)->label(Yii::t('app', "Četvrt"));
            echo '</div>';
        }

        if ($modelDijete->isNewRecord) {
            $modelDijete->prebivaliste_jednako_boraviste = 'D';
        }
        ?>
        <br>
        <div class="input-field">
            <label> Podaci o adresi stanovanja/boravišta jednaki su podacima adrese prebivališta?</label>
            <?= $form->field($modelDijete, 'prebivaliste_jednako_boraviste')->radioList([
                'D' => Yii::t('app', 'Da'), 'N' => Yii::t('app', 'Ne')
            ], ['id' => 'prebivaliste-select'])->label(false) ?>
        </div>
        <div id="prebivaliste" <?= $modelDijete->prebivaliste_jednako_boraviste == 'N' ? '' : 'style="display:none;"' ?>>

            <div class="input-field">
                <?= $form->field($modelDijete, 'adresa_prebivalista')->textInput()->label(Yii::t('app', "Adresa prebivališta")) ?>
            </div>

            <div class="input-field">
                <?= $form->field($modelDijete, 'prebivaliste')->textInput()->label(Yii::t('app', "Grad / općina")) ?>
            </div>
        </div>
        <br />
        <?php
        if ($modelDijete->isNewRecord) {
            $modelDijete->sestra = 'N';
            $modelDijete->cekanje = 'N';
        }

        ?>
        <div class="input-field">
            <?= $form->field($modelDijete, 'sestra')->radioList([
                'D' => Yii::t('app', 'Da'),
                'N' => Yii::t('app', 'Ne')
            ])->label(Yii::t('app', 'Ima li brata/sestru već upisane u dječji vrtić')); ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($modelDijete, 'cekanje')->radioList([
                'D' => Yii::t('app', 'Da'),
                'N' => Yii::t('app', 'Ne')
            ])->label(Yii::t('app', 'Je li dijete bilo na listi čekanja u Dječjem vrtiću ')); ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($modelDijete, 'god_cekanja')->textInput()->label(Yii::t('app', 'Ako da, koje godine')); ?>
        </div>
        <br />
        <div class="form-group">
            <?= Html::submitButton($modelDijete->isNewRecord ? Yii::t('app', 'Sljedeći korak') : Yii::t('app', 'Sljedeći korak'), ['class' => $modelDijete->isNewRecord ? 'btn btn-success right w-50' : 'btn btn-success right']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>