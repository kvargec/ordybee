<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Shop */

$this->title = Yii::t('app', 'Predaja novog zahtjeva');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zahtjevi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">


    <?= $this->render('_form', [
        'model' => $model,
        'modelDijete' => $modelDijete,
    ]) ?>

</div>