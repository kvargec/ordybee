<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>
<table class="tablePDF obican">
    <tr>
        <td style = "width: 300px;">
   

        <?php
            $podaciVrtic=\common\models\Postavke::find()->where(['postavka'=>'vrticPodaci'])->one();
            $nazivVrtica=\common\models\Postavke::find()->where(['postavka'=>'nazivVrtica'])->one();

            ?>
            <?= Yii::t('app','OIB: ').$podaciVrtic->dodatno[0]['OIB']?><br/>
            <?= $nazivVrtica->vrijednost?><br/>
            <?= $podaciVrtic->dodatno[0]['Adresa']?><br/>
            <?= $podaciVrtic->dodatno[0]['ZIP'].' '.$podaciVrtic->dodatno[0]['Mjesto']?><br/>
        </td>
        <td></td><td></td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; padding-top: 50px;">
        <center>
<?= Yii::t('app','ZAHTJEV ZA UPIS DJETETA U ').$nazivVrtica->vrijednost?><br>
            <?php if(empty($zahtjev->lokacija)){

            }else{
             $objekt=\common\models\Objekt::find()->where(['id'=>$zahtjev->lokacija])->one();
             echo 'Za lokaciju: '.$objekt->naziv.' '.$objekt->adresa.', '.$objekt->mjesto0->naziv;
            } ?>
        <?php echo $zahtjev->dijete0->ime." ".$zahtjev->dijete0->prezime; ?>
        </center>
        </td>
    </tr>
    <tr>
        <td style = "width: 300px; padding-top: 50px;">
        <strong><?= Yii::t('app','Klasa:')?></strong> 15-19-1<br>
        <strong><?= Yii::t('app','Datum:')?></strong> 25.04.2019.<br>
        <strong><?= Yii::t('app','Šifra djeteta:')?></strong> <?= $zahtjev->urbroj; ?><br>
        </td>
        <td></td><td></td>
    </tr>
</table>
<div class="shop-create">
    <div class="col s12 m12 l8">
        <div class="row">
            <div class="col s12 m12">
                <div class="panel panel-success">
                    <div class="panel-heading">
<?= Yii::t('app','PODACI O DJETETU')?>
                    </div>
                    <div class="panel-body">
                        <?= DetailView::widget([
                            'model' => $dijete,
                            'attributes' => [
                                'ime',
                                'prezime',
                                'spol',
                                'dat_rod',
                                'oib',
                                'adresa',
                                'mjesto',
                                'mjesto_rod',
                                'prebivaliste',
                                'sestra',
                                'cekanje',
                                'god_cekanja',
                                'razvoj',
                                'vrsta_programa',
                                'teskoce',
                                'dijagnostika',
                                'druge_potrebe',
                                'posebne',
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">
<?= Yii::t('app','Roditelji')?>
                    </div>
                    <div class="panel-body">
                    <?= DetailView::widget([
                            'model' => $roditelj1,
                            'attributes' => [
                                'ime',
                                'prezime',
                                'dat_rod',
                                'oib',
                                'adresa',
                                'mjesto',
                                'prebivaliste',
                                'mobitel',
                                'email',
                                'poslodavaca',
                                'radno',
                            ],
                        ]) ?>
                    </div>
                    <div class="panel-body">
                    <?= DetailView::widget([
                            'model' => $roditelj2,
                            'attributes' => [
                                'ime',
                                'prezime',
                                'dat_rod',
                                'oib',
                                'adresa',
                                'mjesto',
                                'prebivaliste',
                                'mobitel',
                                'email',
                                'poslodavaca',
                                'radno',
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">
<?= Yii::t('app','Sestre i braća')?>
                    </div>
                    <div class="panel-body">
                    <?= yii\grid\GridView::widget([
                        'dataProvider' => $siblings,
                        'columns' => [
                            'ime',
                            'prezime',
                            'spol',
                            'dat_rod',
                            'oib',
                            'prebivaliste',
                        ],
                        'showHeader' => false,
                    ])
                    ?>
                    </div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">
<?= Yii::t('app','Dodani dokumenti')?>
                    </div>
                    <div class="panel-body">
                    <?= yii\grid\GridView::widget([
                        'dataProvider' => $documents,
                        'columns' => [
                            'naziv',
                            'filename',
                        ],
                        'showHeader' => true,
                        'summary'=>'',
                    ])
                    ?>
                    </div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">
<?= Yii::t('app','Dodani dokumenti')?>
                    </div>
                    <div class="panel-body">
                    <?php
                    foreach ($dokumenti as $dokument) {
                    ?>
                    <div><?= $dokument['naziv']; ?></div>
                    <div><img src="<?=Yii::getAlias('@webroot')?>/upload/<?= $dokument['filename']; ?>" style="width: 180mm; margin: 0;"></div>
                    <?php
                    }
                    ?>
                    </div>
                </div>

                <p><?= Yii::t('app','OSTALA DOKUMENTACIJA :')?></p>
                <strong><?= Yii::t('app','Roditelj na intervju donosi:')?></strong><br>
                <ul>
                    <li><?= Yii::t('app','Potvrdu o obavljenom sistematskom pregledu')?></li>
                    <li><?= Yii::t('app','Presliku iskaznice imunizacije te original iskaznicu na uvid')?></li>
                    <li><?= Yii::t('app','Kopiju zdravstvene iskaznice djeteta')?></li>
                    <li><?= Yii::t('app','Kopiju medicinske/logopedske dokumentacije (ukoliko postoji)')?></li>
                    <li><?= Yii::t('app','Kopiju rješenje Centra za socijalnu skrb te nalaze i mišljenja tijela vještačenja (ukoliko
                postoji)')?></li>
                </ul>
                <hr />

                <strong><?= Yii::t('app','IZJAVE ZAPRIMLJENE NAKON ROKA ZA PREDAJU I NEPOTPUNE PRIJAVE NEĆE SE RAZMATRATI NITI
                    BODOVATI')?><br>
<?= Yii::t('app','PREDAJOM ZAHTJEVA DIJETE NIJE UPISANO U DJEČJI VRTIĆ – DIJETE JE UPISANO U VRTIĆ U TRENUTKU KAD
                    RODITELJ POTPIŠE UGOVOR S DJEČJIM VRTIĆEM')?><br>
<?= Yii::t('app','DJEČJI VRTIĆ ZADRŽAVA PRAVO RASPOREDA DJETETA U SKUPINE I OBJEKTE')?><br></strong>

            </div>
        </div>

        <div class="shop-form">
            <div class="col s12 m12 l12">
                <hr/>
                <div class="col s12 m12 l6">
                </div>
                <div class="col s12 m12 l6">
                </div>

            </div>
        </div>
    </div>
</div>