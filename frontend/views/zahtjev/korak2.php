<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use app\models\SubjectLocation;
use app\models\SubjectDescription;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use dektrium\user\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\widgets\DepDrop;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Predaja novog zahtjeva 2.korak');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zahtjevi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$teskoceData = [
    'a' => Yii::t('app', 'oštećenja vida'),
    'b' => Yii::t('app', 'oštećenja sluha'),
    'c' => Yii::t('app', 'poremećaji govorno –glasovne komunikacije i specifične teškoće u učenju'),
    'd' => Yii::t('app', 'tjelesni invaliditet'),
    'e' => Yii::t('app', 'intelektualne teškoće (sindromi…)'),
    'f' => Yii::t('app', 'poremećaji u ponašanju uvjetovani organskim faktorima, ADHD'),
    'g' => Yii::t('app', 'poremećaj socijalne komunikacije; poremećaj iz autističnog spektra; autizam'),
    'h' => Yii::t('app', 'motorički poremećaji'),
    'i' => Yii::t('app', 'specifične teškoće u učenju'),
    'j' => Yii::t('app', 'kronične bolesti'),
    'k' => Yii::t('app', 'rizična ponašanja'),
    'l' => Yii::t('app', 'posebne zdravstvene potrebe'),
    'm' => Yii::t('app', 'postojanje više vrsta i stupnjeva teškoća u psihofizičkom razvoju')
];

$dijagnostikaData = [
    'a' => Yii::t('app', 'vještačenjem stručnog povjerenstva socijalne skrbi'),
    'b' => Yii::t('app', 'nalazom i mišljenjem specijalizirane ustanove'),
    'c' => Yii::t('app', 'nalazom i mišljenjem ostalih stručnjaka')
];

/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="site-about">
    <h1><?= Yii::t('app', 'Razvojni status djeteta') ?></h1>
    <div class="col s12 m12 l4">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/update', 'id' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">1</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-2" type="button" class="btn btn-success btn-circle">2</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                </div>

            </div>
        </div>
        <blockquote>
            <p><?= Yii::t('app', 'U ovom koraku unosite podatke o razvojnom statusu Vašeg djeteta.') ?> <br><?= Yii::t('app', 'Ukoliko dijete ima urednu razvojnu liniju, potrebno je to odabrati i kliknuti dalje.') ?> <br><?= Yii::t('app', 'U suprotnom unesite sve potrebne podatke.') ?> </p>
        </blockquote>
    </div>
    <div class="col s12 m12 l8">
        <div class="col l4">
            <h1><?= Yii::t('app', 'Razvojni status djeteta') ?> </h1><br>
        </div>
        <div class="row">
            <div class="col s12 m12">

            </div>
        </div>

        <div class="shop-form ">

            <?php $form = ActiveForm::begin(); ?>

            <?php // $form->field($model, 'id')->textInput() 
            ?>
            <div class="input-field">
                <?php
                if ($bubamara) {
                } else {
                    echo $form->field($modelZ, 'zeljeni_datum')->widget(DatePicker::class, [
                        'convertFormat' => true,
                        'options' => ['placeholder' => Yii::t('app', 'Datum željenog polaska')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ]);
                }
                ?>

                <?= $form->field($modelDijete, 'razvoj')->radioList([
                    'uredan' => Yii::t('app', 'uredna razvojna linija'),
                    'teskoce' => Yii::t('app', 'dijete s teškoćama u razvoju')
                ])->label(Yii::t('app', 'Razvojni status djeteta')); ?>
            </div>
            <div class="input-field">
                <?php
                if (!$podvrsta) {
                    echo $form->field($modelDijete, 'vrsta_programa')->radioList(ArrayHelper::map(\common\models\VrstaPrograma::find()->where(['or', ['parent' => null], ['parent' => 0]])->all(), 'id', 'vrsta'))->label(Yii::t('app', 'ZAHTJEV PODNOSIM ZA UPIS DJETETA U SLJEDEĆI PROGRAM'));
                } else {
                    echo $form->field($modelDijete, 'vrstadepdrop')->radioList(ArrayHelper::map(\common\models\VrstaPrograma::find()->where(['or', ['parent' => null], ['parent' => 0]])->all(), 'id', 'vrsta'))->label(Yii::t('app', 'ZAHTJEV PODNOSIM ZA UPIS DJETETA U SLJEDEĆI PROGRAM'));

                    echo $form->field($modelDijete, 'vrsta_programa')->radioList([])->label(Yii::t('app', 'POTPROGRAM'));

                    //echo $form->field($modelDijete, 'vrstadepdrop')->widget(Select2::classname(), [
                    //    'data' => ArrayHelper::map(\common\models\VrstaPrograma::find()->where(['or', ['parent' => null], ['parent' => 0]])->all(), 'id', 'vrsta'),
                    //    'options' => ['placeholder' => Yii::t('app', 'Odaberi vrstu krovnog programa...'), 'id' => 'vrstadepdrop'],
                    //])->label(Yii::t('app', 'Krovni program'));
                    //
                    //echo $form->field($modelDijete, 'vrsta_programa')->widget(DepDrop::classname(), [
                    //    'type' => DepDrop::TYPE_SELECT2,
                    //    'options' => ['id' => 'vrsta_programa'],
                    //    'select2Options' => [
                    //        'pluginOptions' => [
                    //            'placeholder' => Yii::t('app', 'Odaberite...'),
                    //            'allowClear' => true,
                    //            'multiple' => false
                    //        ]
                    //    ],
                    //    'pluginOptions' => [
                    //        'depends' => ['vrstadepdrop'],
                    //        'placeholder' => Yii::t('app', 'Odaberite...'),
                    //        'initialize' => true,
                    //        'loadingText' => '',
                    //        'url' => Url::to(['/zahtjev/vrsta-potprograma'])
                    //    ]
                    //])->label(Yii::t('app', "Potprogram"));
                }
                ?>
            </div>
            <hr />
            <div class="input-field">
                <label class="control-label"><?= Yii::t('app', 'Teškoće u razvoju') ?></label>
                <?= Select2::widget([
                    'model' => $modelDijete,
                    'attribute' => 'teskoce[]',
                    'data' => $teskoceData,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Odaberite iz liste...'),
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'tokenSeparators' => [',', ' '],
                    ],
                ]);
                ?>
            </div>
            <br />
            <div class="input-field">
                <label class="control-label"><?= Yii::t('app', 'Dijagnostički postupak za utvrđivanje teškoća') ?></label>
                <?= Select2::widget([
                    'model' => $modelDijete,
                    'attribute' => 'dijagnostika[]',
                    'data' => $dijagnostikaData,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Odaberite iz liste...'),
                        'multiple' => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'tokenSeparators' => [',', ' '],
                    ],
                ]);
                ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelDijete, 'druge_potrebe')->textInput()->label(Yii::t('app', "druge specifične razvojne posebne potrebe djeteta (poremećaji s prkošenjem i suprotstavljanjem, poremećaj ophođenja, anksiozni poremećaj, depresivna stanja, tikovi, noćne more ili strahovi, fobije, neadekvatna privrženost, proživljeno traumatsko iskustvo, povučenost, sramežljivost, ispadi bijesa, agresija i drugo)")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelDijete, 'posebne')->textInput()->label(Yii::t('app', "posebne zdravstvene potrebe djeteta (alergije, posebna prehrana, kronična oboljenja, epy, astma, febrilne konvulzije i drugo)")) ?>
            </div>
            <br />

            <br />

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Dalje'), ['class' => $modelDijete->isNewRecord ? 'btn btn-success right' : 'btn btn-success right']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
$url = \yii\helpers\Url::to(['/zahtjev/vrsta-potprograma']);
$js = <<<JS

$(document).ready(function() {
    $("input[name='Dijete[vrstadepdrop]']").on("click", function(e) {
        //e.preventDefault();
        var data = $("input[name='Dijete[vrstadepdrop]']:checked").val();
        console.log(data);
        $.ajax({
            url: '$url',
            type: 'post',
            dataType: 'json',
            data: {'depdrop_parents':data}
        }).done(function(data, textStatus, jqXHR) {
            console.log(data);
            if (data.output.length>0) {
                $('#dijete-vrsta_programa').html(function() {
                    var html = '';
                    for(var i=0;i<data.output.length;i++) {
                        if (data.output[i]['id'] == 0) {
                            html += '<label><input type="radio" style="margin-right:5px" checked name="Dijete[vrsta_programa]" value="'+ data.output[i]['id'] +'"/>'+data.output[i]['name']+'</label>';
                        } else {
                            html += '<label><input type="radio" style="margin-right:5px" name="Dijete[vrsta_programa]" value="'+ data.output[i]['id'] +'"/>'+data.output[i]['name']+'</label>';
                        }     
                    }
                    return html;
                })
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            $.notify( errorThrown, 'error' );
        });
    });
});
JS;
$this->registerJs($js);
?>