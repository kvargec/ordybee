<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */

if (is_array($dokumenti->dodatno[0])) {
    $popisImena = $dokumenti->dodatno[0];
    // $popisImena=array_reverse($popisImena);
} else {
    $popisImena = ['Nema obaveznih dokumenata'];
}
/*$popisImena=[
    'Preslika rodnog lista djeteta ili izvatka iz matice rođenih'=>Yii::t('app','Preslika rodnog lista djeteta ili izvatka iz matice rođenih'),
    'Preslike osobne iskaznice'=>Yii::t('app','Preslike osobne iskaznice (bilo koja strana)'),
    'Potvrda o prebivalištu djeteta (ne starija od šest mjeseci)'=>Yii::t('app','Potvrda o prebivalištu djeteta (ne starija od šest mjeseci)'),
    'Potvrda Hrvatskog zavoda za mirovinsko osiguranje o radno-pravnom statusu (ne starija od 30 dana)'=>Yii::t('app','Potvrda Hrvatskog zavoda za mirovinsko osiguranje o radno-pravnom statusu (ne starija od 30 dana)'),
    'Preslika Rješenja da je roditelj djeteta žrtva ili invalid Domovinskog rata'=>Yii::t('app','Preslika Rješenja da je roditelj djeteta žrtva ili invalid Domovinskog rata'),
    'Preslike izvatka iz matične knjige rođenih ili rodnog lista ostale malodobne djece'=>Yii::t('app','Preslike izvatka iz matične knjige rođenih ili rodnog lista ostale malodobne djece'),
    'Preslika medicinske dokumentacije djeteta koja potvrđuje posebne potrebe u rastu i razvoju djeteta'=>Yii::t('app','Preslika medicinske dokumentacije djeteta koja potvrđuje posebne potrebe u rastu i razvoju djeteta'),
    'Preslika Rješenja o skrbništvu za roditelje koji žive sami'=>Yii::t('app','Preslika Rješenja o skrbništvu za roditelje koji žive sami'),
    'Preslika Rješenja o skrbništvu za uzdržavanu djecu'=>Yii::t('app','Preslika Rješenja o skrbništvu za uzdržavanu djecu'),
    'Preslika Rješenja da je roditelj djeteta korisnik doplataka za dijete'=>Yii::t('app','Preslika Rješenja da je roditelj djeteta korisnik doplataka za dijete'),
    'Ostalo'=>Yii::t('app','Ostalo')

];*/
?>
<div class="shop-create site-about">

    <h1><?= Yii::t('app', 'Obavezni dokumenti') ?></h1>
    <div class="col s12 m12 l4">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/update', 'id' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">1</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/korak2', 'zahtjevId' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">2</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/korak3', 'zahtjevId' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">3</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/korak4', 'zahtjevId' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">4</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-success btn-circle">5</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                </div>

            </div>
        </div>
        <blockquote>
            <p><?= Yii::t('app', 'Peti korak u predaji zahtjeva je&nbsp;<strong>učitavanje (upload) potrebnih dokumenata.') ?><br>
                <?= Yii::t('app', 'Odaberite vrstu dokumenta iz padajućeg izbornika, možete odabrate i više istih (osobna iskaznica i za majku i za oca, više priložene medicinske dokumentacije)') ?> </strong></p>
            <p><?= Yii::t('app', 'Ukoliko nemate scanner, slikajte ih pažljivo mobilnim telefonom.') ?></p>
        </blockquote>


    </div>
    <div class="col s12 m12 l8">
        <h2 class=""><?= Yii::t('app', 'Predani dokumenti') ?></h2>

        <div class="col l8">
            <?= GridView::widget([
                'dataProvider' => $popisDatoteka,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'document0.naziv',
                    [
                        'label' => Yii::t('app', 'Datoteka'),
                        'format' => 'raw',
                        'value' => function ($data) use ($zahtjevId) {

                            return Html::a('<i class="glyphicon glyphicon-file"></i>' . Yii::t('app', 'Preuzmi'), Yii::$app->params['frontendUrl'] . "/upload/zahtjevi/$zahtjevId/" . $data->document0->filename);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}  ',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'delete' => function ($url, $data) {


                                $url2 = \Yii::$app->urlManager->createUrl(['document/deletez', 'id' => $data->document]);
                                $gumb = Html::beginForm($url2, 'post')
                                    . Html::submitButton(
                                        '<span class="material-icons">delete</span>',
                                        ['class' => 'text-left  hgreen']
                                    )
                                    . Html::endForm();
                                return $gumb;
                            },

                        ],
                    ],

                ],
            ]); ?>

        </div>
        <hr />
        <div class="panel">
            <div class="panel-heading">
                <?= Yii::t('app', 'Odabir datoteke') ?>

            </div>
            <div class="panel-body">
                <div class="shop-form">
                    <p style="font-style:italic;">Upute:<br>Odaberite vrstu dokumenta, odaberite datoteku i pritisnite žuto dugme "Dodaj datoteku".<br> Radnju ponovite za svaki dokument koji želite unijeti. Kada ste unijeli sve dokumente prijeđite na sljedeći korak.</p>
                    <?php
                    $inicijalni = \common\models\Postavke::find()->where(['postavka' => 'inicijalniUpitnik'])->one();
                    if (!empty($inicijalni) and $inicijalni->vrijednost) {


                        echo '<a href="';
                        echo $inicijalni->vrijednost;
                        echo '" target="_blank">Inicijalni upitnik možete preuzeti ovdje</a><br><br>';
                    }
                    ?>
                    <?php $form = ActiveForm::begin(['action' => '', 'options' => ['enctype' => 'multipart/form-data', 'class' => 'form']]); ?>
                    <div class="form-group">
                        <?= $form->field($modelSD, 'naziv')->dropDownList($popisImena)->label(Yii::t('app', 'Vrsta dokumenta')) ?>

                    </div>

                    <div class="form-group">
                        <!-- <label class="control-label"><?= Yii::t('app', 'Odaberite datoteku') ?></label><br> -->
                        <div class="relative-file">
                            <!-- <label class="file-input-label btn btn-primary"><?= Yii::t('app', 'Odaberite datoteku') ?></label> -->
                            <?php echo $form->field($modelSD, 'filename')->fileInput(['class' => 'form-control', 'id' => 'upload', 'multiple' => false])->label(false); ?>
                            <?= Html::submitButton(Yii::t('app', 'Dodaj datoteku'), ['class' => 'btn btn-warning mb10', 'id' => 'dodaj_datoteku_btn']) ?>
                        </div>
                    </div>




                    <?php ActiveForm::end(); ?>
                </div>

            </div>
        </div>
    </div>
    <div class="col l4">
        <div class="form-group">
            <?= Html::a(Yii::t('app', 'Sljedeći korak'), \yii\helpers\Url::to(['zahtjev/korak7', 'id' => $zahtjevId]), ['class' => 'btn btn-success right', 'id' => 'upload-files-submit']) ?>
        </div>
    </div>
</div>

<?php
$js = <<<JS
var fileNumber = parseInt("$brojDatoteka");
var documentNumber = parseInt("$brojDokumenata");
console.log(fileNumber);
console.log(documentNumber);
$("#upload-files-submit").on("click", function(e) {
    if (fileNumber < documentNumber) {
        e.preventDefault();
        //alert("Molimo dodajte sve obavezne datoteke");
        $("#document-naziv").notify("Molimo dodajte sve obavezne datoteke", "error");  
    } 
});
$("#dodaj_datoteku_btn").on("click", function(e) {
    if ($("#document-naziv").val() === null) {
        e.preventDefault();
        //alert("Molimo odaberite vrstu dokumenta.");
        $("#document-naziv").notify("Molimo odaberite vrstu dokumenta", "error");
    } 
});
JS;
$this->registerJs($js);
?>