<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use dektrium\user\models\User;
use yii\helpers\ArrayHelper;

$this->title = 'POTVRDA O PREDANOM ZAHTJEVU I PRILOZIMA ZA DIJETE:';
?>

<html>
    <head>
    <style>
        body { color: #000000; }
        td { vertical-align: top;}
        .tablePDF { width:100%; background-color: #ffffff; font-size:11px; border-spacing: 0px;}
        .tablePDF td { text-align:left; background-color: #ffffff; padding:2px 2px 2px 2px; margin: 0;}
        .tablePDf tr.crta td { border-bottom: 1px solid #003399;}
        .tablePDF th { background-color: #ffffff; padding:2px 2px 2px 2px; border:1px solid #003399;}
        .tablePDF td.sredina { text-align:center;}
        .tablePDF tr.desno { text-align:right;}
        .desno { text-align:right;}
        .kv-page-summary-container td{ text-align:right; font-weight:bold;}
        body{}
        img{ padding:5px; border:1px solid #000;}
        .obican{ font-family:Helvetica; font-size:16px;}
        .veliki{ font-family:Helvetica; font-size:24px; text-transform: uppercase;}
        </style>
    </head>
<body>
<table class="tablePDF obican">
    <tr>
        <td style = "width: 300px;">
        
            <?php
            $podaciVrtic=\common\models\Postavke::find()->where(['postavka'=>'vrticPodaci'])->one();
            $nazivVrtica=\common\models\Postavke::find()->where(['postavka'=>'nazivVrtica'])->one();
            ?>
<?= Yii::t('app','OIB: ').$podaciVrtic->dodatno['OIB']?><br/>
<?= $nazivVrtica->vrijednost?><br/>
<?= $podaciVrtic->dodatno['Adresa']?><br/>
<?= $podaciVrtic->dodatno['ZIP'].' '.$podaciVrtic->dodatno['Mjesto']?>lalla<br/>
        </td>
        <td></td><td></td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; padding-top: 50px;">

<?= Yii::t('app','POTVRDA O PREDANOM ZAHTJEVU I PRILOZIMA ZA DIJETE:')?><br>
        <?php echo $dijete->ime." ".$dijete->prezime; ?>

        </td>
    </tr>
    <tr>
        <td style = "width: 300px; padding-top: 50px;">
        <strong><?= Yii::t('app','Klasa:')?></strong> <br>
        <strong><?= Yii::t('app','URBR:')?></strong> <?=$model->urbroj;?><br>
        <strong><?= Yii::t('app','Datum:')?></strong> <?php echo date('d.m.Y.',time())?><br>
        <strong><?= Yii::t('app','Šifra djeteta:')?></strong> <?=$model->urbroj;?><br>
        </td>
        <td></td><td></td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; padding-top: 50px;">
        <center>
<?= Yii::t('app','Predani prilozi')?>
        </center>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; border: 1px solid black; padding: 30px;">
        <?php 
        foreach ($dokumenti as $dokument) {
        ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo $dokument->naziv; ?><br><br>
        <?php
        } ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; padding-top: 30px;">
<?= Yii::t('app','Napomena:')?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; padding-top: 200px;">
<?= Yii::t('app','Ova potvrda izdana je elektroničkim putem i valjana je bez žiga i potpisa')?>
        </td>
    </tr>
</table>
</body>
</html>
