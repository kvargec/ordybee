<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
/* @var $modelRoditelji \common\models\Roditelj[] */

$jsScript = <<<JS

$('input[name="Zahtjev[posjeduje_pravo]"]').click(function(){
            if($(this).attr("value")=="0"){
                $("#posjeduje_pravo").hide('slow');
                console.log('hide');
            }
            if($(this).attr("value")=="1"){
                $("#posjeduje_pravo").show('slow');
                console.log('show');
            }        
        });
$('input[name="Zahtjev[Roditelj[1][prebivaliste_jednako_boraviste]]"]').click(function(){
            if($(this).attr("value")=="N"){
                $("#posjeduje_pravo").hide('slow');
                console.log('hide');
            }
            if($(this).attr("value")=="D"){
                $("#posjeduje_pravo").show('slow');
                console.log('show');
            }        
        });
$('input[name="Roditelj[0][prebivaliste_jednako_boraviste]"]').click(function(){
            if($(this).attr("value")=="N"){
                $("#posjeduje_pravo").hide('slow');
                console.log('hide');
            }
            if($(this).attr("value")=="D"){
                $("#posjeduje_pravo").show('slow');
                console.log('show');
            }        
        });

JS;

$this->registerJs($jsScript, View::POS_READY);
?>
<div class="shop-create site-about">

    <h1><?= Yii::t('app', 'Podaci o roditeljima/skrbnicima') ?></h1>
    <div class="col s12 m12 l4">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/update', 'id' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">1</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/korak2', 'zahtjevId' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">2</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-success btn-circle">3</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                </div>

            </div>
        </div>
        <blockquote>
            <p><?= Yii::t('app', 'Treći korak u predaji zahtjeva je&nbsp;<strong>unos podataka o roditeljima / skrbnicima') ?> </strong></p>
        </blockquote>


    </div>
    <div class="col s12 m12 l8">
        <h2 class=""><?= Yii::t('app', 'Podaci o roditeljima / skrbnicima') ?></h2>
        <hr />
        <br />

        <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
        <?php


        ?>
        <div class="input-field">
            <?= $form->field($modelZahtjev, 'vrsta_obitelji')->radioList(ArrayHelper::map(\common\models\VrstaObitelji::find()->all(), 'id', 'naziv'), ['separator' => '<br>']) ?>
        </div>
        <label class="pravo">
            Majka/otac ima rješenje o korištenju rodiljnog ili roditeljskog dopusta ili drugog prava iz Zakona o rodiljnim i
            roditeljskim potporama<br> (NN 85/08, 110/08 - ispravak, 34/11, 54/13, 152/14, 59/17 i 37/20)</label>
        <div class="input-field">
            <?php
            if ($modelZahtjev->isNewRecord or $modelZahtjev->posjeduje_pravo === null) {

                $modelZahtjev->posjeduje_pravo = 0;
            }
            if ($modelZahtjev->posjeduje_pravo === true) {
                $modelZahtjev->posjeduje_pravo = 1;
            } else {
                $modelZahtjev->posjeduje_pravo = 0;
            }
            ?>
            <?= $form->field($modelZahtjev, 'posjeduje_pravo')->radioList([1 => Yii::t('app', 'Da'), 0 => Yii::t('app', 'Ne'),], ['separator' => '<br>'])->label(false) ?>
        </div>

        <div id="posjeduje_pravo" <?php echo $modelZahtjev->posjeduje_pravo == true ? '' : 'style="display:none"' ?>>
            <div class="input-field pravo">
                <?= $form->field($modelZahtjev, 'pravo_pocetak')->widget(DatePicker::class, [
                    'convertFormat' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Datum početka korištenja prava')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ])

                ?>
            </div>
            <div class="input-field pravo">
                <?= $form->field($modelZahtjev, 'pravo_kraj')->widget(DatePicker::class, [
                    'convertFormat' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Datum kraja korištenja prava')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]) ?>
            </div>
            <div class="input-field">
                <?= $form->field($modelZahtjev, 'pravo_naziv')->textInput() ?>
            </div>
        </div>
        <br />
        <h3><?= Yii::t('app', 'Podaci o majci djeteta') ?></h3>
        <hr />
        <br />

        <div class="shop-form">

            <?php // $form->field($model, 'id')->textInput() 
            ?>

            <?= $form->field($modelRoditelj[0], '[0]id')->hiddenInput()->label(false) ?>
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]ime')->textInput()->label(Yii::t('app', "Ime majke")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]prezime')->textInput()->label(Yii::t('app', "Prezime majke")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= DatePicker::widget([
                    'model' => $modelRoditelj[0],
                    'attribute' => '[0]dat_rod',
                    'convertFormat' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Datum rođenja majke')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]oib')->textInput()->label(Yii::t('app', "OIB majke")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]drzavljanstvo')->textInput()->label(Yii::t('app', "Državljanstvo")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]adresa')->textInput()->label(Yii::t('app', "Adresa boravišta")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]mjesto')->textInput()->label(Yii::t('app', "Grad / općina")) ?>
            </div>
            <br />
            <div class="input-field">
                <?php
                if ($modelRoditelj[0]->isNewRecord) {
                    $modelRoditelj[0]->prebivaliste_jednako_boraviste = 'D';
                }
                ?>
                <?= $form->field($modelRoditelj[0], '[0]prebivaliste_jednako_boraviste')->radioList(['D' => Yii::t('app', 'Da'), 'N' => Yii::t('app', 'Ne')], ['id' => 'prebivaliste-select-majka'])->label("Podaci o adresi stanovanja/boravišta jednaki su podacima adrese prebivališta?") ?>
            </div>

            <div id="prebivaliste-majka" <?= $modelRoditelj[0]->prebivaliste_jednako_boraviste == 'N' ? '' : 'style="display:none;"' ?>>
                <!-- Podaci o adresi stanovanja/boravišta jednaki su podacima adrese prebivališta? -->
                <div class="input-field">
                    <?= $form->field($modelRoditelj[0], '[0]adresa_prebivalista')->textInput()->label(Yii::t('app', "Adresa prebivališta")) ?>
                </div>

                <div class="input-field">
                    <?= $form->field($modelRoditelj[0], '[0]prebivaliste')->textInput()->label(Yii::t('app', "Grad / općina")) ?>
                </div>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]poslodavaca')->textInput()->label(Yii::t('app', 'Naziv poslodavca') . ' (unesite - ukoliko ne želite podijeliti)'); ?>
            </div>
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]adresa_poslodavca')->textInput()->label(Yii::t('app', 'Adresa poslodavca') . ' (unesite - ukoliko ne želite podijeliti)'); ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]mobitel')->textInput()->label(Yii::t('app', "Broj mobitela")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]email')->textInput()->label(Yii::t('app', "E-mail")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]zanimanje')->textInput()->label(Yii::t('app', "Zanimanje i stručna sprema")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[0], '[0]radno')->textInput()->label(Yii::t('app', "Radno vrijeme")) ?>
            </div>
            <br />
            <h3><?= Yii::t('app', 'Podaci o ocu djeteta') ?></h3>
            <hr />
            <br />

            <?= $form->field($modelRoditelj[1], '[1]id')->hiddenInput()->label(false) ?>
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]ime')->textInput()->label(Yii::t('app', "Ime oca")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]prezime')->textInput()->label(Yii::t('app', "Prezime oca")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= DatePicker::widget([
                    'model' => $modelRoditelj[1],
                    'attribute' => '[1]dat_rod',
                    'convertFormat' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Datum rođenja oca')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]oib')->textInput()->label(Yii::t('app', "OIB oca")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]drzavljanstvo')->textInput()->label(Yii::t('app', "Državljanstvo")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]adresa')->textInput()->label(Yii::t('app', "Adresa boravišta")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]mjesto')->textInput()->label(Yii::t('app', "Grad / općina")) ?>
            </div>
            <br />
            <?php
            if ($modelRoditelj[1]->isNewRecord) {
                $modelRoditelj[1]->prebivaliste_jednako_boraviste = 'D';
            }
            ?>
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]prebivaliste_jednako_boraviste')->radioList(['D' => Yii::t('app', 'Da'), 'N' => Yii::t('app', 'Ne')], ['id' => 'prebivaliste-select-otac'])->label("Podaci o adresi stanovanja/boravišta jednaki su podacima adrese prebivališta?") ?>
            </div>

            <div id="prebivaliste-otac" <?= $modelRoditelj[1]->prebivaliste_jednako_boraviste == 'N' ? '' : 'style="display:none;"' ?>>
                <!-- Podaci o adresi stanovanja/boravišta jednaki su podacima adrese prebivališta? -->
                <div class="input-field">
                    <?= $form->field($modelRoditelj[1], '[1]adresa_prebivalista')->textInput()->label(Yii::t('app', "Adresa prebivališta")) ?>
                </div>

                <div class="input-field">
                    <?= $form->field($modelRoditelj[1], '[1]prebivaliste')->textInput()->label(Yii::t('app', "Grad / općina")) ?>
                </div>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]poslodavaca')->textInput()->label(Yii::t('app', 'Naziv poslodavca') . ' (unesite - ukoliko ne želite podijeliti)'); ?>
            </div>
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]adresa_poslodavca')->textInput()->label(Yii::t('app', 'Adresa poslodavca') . ' (unesite - ukoliko ne želite podijeliti)'); ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]mobitel')->textInput()->label(Yii::t('app', "Broj mobitela")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]email')->textInput()->label(Yii::t('app', "E-mail")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]zanimanje')->textInput()->label(Yii::t('app', "Zanimanje i stručna sprema")) ?>
            </div>
            <br />
            <div class="input-field">
                <?= $form->field($modelRoditelj[1], '[1]radno')->textInput()->label(Yii::t('app', "Radno vrijeme")) ?>
            </div>
            <br />
            <br />
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Dalje'), ['class' => $modelRoditelj[0]->isNewRecord ? 'btn btn-success right' : 'btn btn-primary right']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>


    </div>


</div>
</div>
</div>