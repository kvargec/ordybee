<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="shop-create site-about">

    <h1><?= Yii::t('app', 'Podaci o malodobnoj djeci') ?></h1>

    <div class="col s12 m12 l4">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/update', 'id' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">1</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/korak2', 'zahtjevId' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">2</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo \yii\helpers\Url::to(['zahtjev/korak3', 'zahtjevId' => $zahtjevId]); ?>" type="button" class="btn btn-warning btn-circle">3</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-success btn-circle">4</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                </div>

            </div>
        </div>
        <blockquote>
            <p><?= Yii::t('app', 'Četvrti korak u predaji zahtjeva je&nbsp;<strong>unošenje podataka o ostaloj malodobnoj djeci u
                    obitelji') ?> </strong></p>
        </blockquote>


    </div>
    <div class="col s12 m12 l8">
        <h2 class=""><?= Yii::t('app', 'Podaci o ostaloj malodobnoj djeci u obitelji') ?></h2>
        <p>Nije obavezan korak. <br>Kako bi preskočili ovaj korak na dnu stranice kliknite na dugme "Dalje".</p>
        <div class="row">
            <div class="col s12 m12">

            </div>
        </div>

        <div class="shop-form">

            <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
            <?php
            foreach ($modelDijete as $index => $dete) {


            ?>
                <?= $form->field($dete, "[$index]id")->hiddenInput()->label(false) ?>
                <div class="input-field">
                    <?= $form->field($dete, "[$index]ime")->textInput()->label(Yii::t('app', "Ime djeteta")) ?>
                </div>
                <br />
                <div class="input-field">
                    <?= $form->field($dete, "[$index]prezime")->textInput()->label(Yii::t('app', "Prezime djeteta")) ?>
                </div>
                <br />
                <div class="input-field">
                    <?= $form->field($dete, "[$index]spol")->radioList([
                        'M' => Yii::t('app', 'muški'),
                        'Ž' => Yii::t('app', 'ženski')
                    ])->label(Yii::t('app', 'Spol')); ?>
                </div>
                <br />
                <div class="input-field">
                    <?= DatePicker::widget([
                        'model' => $dete,
                        'attribute' => "[$index]dat_rod",
                        'convertFormat' => true,
                        'options' => ['placeholder' => Yii::t('app', 'Datum rođenja djeteta')],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'autoclose' => true
                        ]
                    ]);
                    ?>
                </div>
                <br />
                <div class="input-field">
                    <?= $form->field($dete, "[$index]oib")->textInput()->label(Yii::t('app', "OIB djeteta")) ?>
                </div>
                <br />
                <div class="input-field">
                    <?= $form->field($dete, "[$index]mjesto_rod")->textInput()->label(Yii::t('app', "Mjesto rođenja")) ?>
                </div>
                <br />
                <?= $form->field($dete, "[$index]drzavljanstvo")->textInput()->label(Yii::t('app', "Državljanstvo")) ?>

                <div class="input-field">

                    <?= $form->field($dete, "[$index]adresa")->textInput()->label(Yii::t('app', "Adresa boravišta")) ?>

                </div>
                <br />
                <div class="input-field">
                    <?= $form->field($dete, "[$index]mjesto")->textInput()->label(Yii::t('app', "Grad / općina")) ?>
                </div>

                <br />
                <?php
                $cetvrti = \common\models\Postavke::find()->where(['postavka' => 'cetvrti'])->one();
                if (!empty($cetvrti) and $cetvrti->vrijednost) {
                    $popis = $cetvrti->dodatno[0];

                    echo '<div class="input-field">';
                    echo $form->field($dete, "[$index]cetvrt")->dropDownList($popis)->label(Yii::t('app', "Četvrt"));
                    echo '</div>';
                }


                ?>
                <br>
                <div class="input-field">
                    <label> Podaci o adresi stanovanja/boravišta jednaki su podacima adrese prebivališta?</label>
                    <?= $form->field($dete, "[$index]prebivaliste_jednako_boraviste")->radioList([
                        'D' => Yii::t('app', 'Da'), 'N' => Yii::t('app', 'Ne')
                    ], ['id' => '[$index]prebivaliste-select'])->label(false) ?>
                </div>
                <div id="prebivaliste" <?= $dete->prebivaliste_jednako_boraviste == 'N' ? '' : 'style="display:none;"' ?>>

                    <div class="input-field">
                        <?= $form->field($dete, "[$index]adresa_prebivalista")->textInput()->label(Yii::t('app', "Adresa prebivališta")) ?>
                    </div>

                    <div class="input-field">
                        <?= $form->field($dete, "[$index]prebivaliste")->textInput()->label(Yii::t('app', "Grad / općina")) ?>
                    </div>
                </div>
                <hr />
            <?php
            }
            ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Dalje'), ['class' =>  'btn btn-success right']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>