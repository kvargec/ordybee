<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="shop-create site-about">

    <h1><?= Yii::t('app','Dodatni dokumenti')?></h1>
    <div class="col s12 m12 l4">
      <div class="stepwizard">
          <div class="stepwizard-row setup-panel">
              <div class="stepwizard-step">
                  <a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled">1</a>
              </div>
              <div class="stepwizard-step">
                  <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
              </div>
              <div class="stepwizard-step">
                  <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
              </div>
              <div class="stepwizard-step">
                  <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
              </div>
              <div class="stepwizard-step">
                  <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
              </div>
              <div class="stepwizard-step">
                  <a href="#step-3" type="button" class="btn btn-success btn-circle" >6</a>
              </div>
              <div class="stepwizard-step">
                  <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
              </div>
          </div>
      </div>
        <blockquote>
            <p><?= Yii::t('app','Šesti korak u predaji zahtjeva je')?> <strong><?= Yii::t('app','učitati (upload) ostalih dodatnih dokumenata.')?></strong><br><?= Yii::t('app','Ukoliko nema
                    takvih dokumenata, kliknite Dalje.')?> </p>
        </blockquote>


    </div>
    <div class="col s12 m12 l8">
        <h2 class=""><?= Yii::t('app','Dodatni dokumenti')?></h2>
        <p>  <?= Yii::t('app','Preslike ostalih dokumenata kojima se dokazuje neka od prednosti pri upisu')?></p>
        <div class="row">
            <div class="col s12 m12">

            </div>
        </div>

        <div class="shop-form">

            <?php $form = ActiveForm::begin(['action' => '', 'options' => ['enctype' => 'multipart/form-data']]); ?>
            <?= $form->field($modelSD, '[1]naziv')->hiddenInput(['value' => 'Preslika Rješenja da je roditelj djeteta žrtva ili invalid Domovinskog rata '])->label(false) ?>
            <br/>
            <div class="col l8">
                <?=Yii::t('app','Preslika Rješenja da je roditelj djeteta žrtva ili invalid Domovinskog rata')?><br/><br/>
                <?php echo $form->field($modelSD, '[1]filename')->fileInput()->label(false); ?>
            </div>

            <?= $form->field($modelSD, '[2]naziv')->hiddenInput(['value' => 'Preslike izvatka iz matične knjige rođenih ili rodnog lista ostale malodobne djece '])->label(false) ?>
            <br/>
            <div class="col l8">
                <?= Yii::t('app','Preslike izvatka iz matične knjige rođenih ili rodnog lista ostale malodobne djece')?><br/><br/>
                <?php echo $form->field($modelSD, '[2]filename')->fileInput()->label(false); ?>
            </div>

            <?= $form->field($modelSD, '[3]naziv')->hiddenInput(['value' => 'Preslika medicinske dokumentacije djeteta koja potvrđuje posebne potrebe u rastu i razvoju
            djeteta '])->label(false) ?>
            <br/>
            <div class="col l8">
                <?= Yii::t('app','Preslika medicinske dokumentacije djeteta koja potvrđuje posebne potrebe u rastu i razvoju djeteta,')?><br/><br/>
                <?php echo $form->field($modelSD, '[3]filename')->fileInput()->label(false); ?>
            </div>

            <?= $form->field($modelSD, '[4]naziv')->hiddenInput(['value' => 'Preslika Rješenja o skrbništvu za roditelje koji žive sami '])->label(false) ?>
            <br/>
            <div class="col l8">
                <?= Yii::t('app','Preslika Rješenja o skrbništvu za roditelje koji žive sami')?><br/><br/>
                <?php echo $form->field($modelSD, '[4]filename')->fileInput()->label(false); ?>
            </div>

            <?= $form->field($modelSD, '[5]naziv')->hiddenInput(['value' => 'Preslika Rješenja o skrbništvu za uzdržavanu djecu '])->label(false) ?>
            <br/>
            <div class="col l8">
                <?= Yii::t('app','Preslika Rješenja o skrbništvu za uzdržavanu djecu')?><br/><br/>
                <?php echo $form->field($modelSD, '[5]filename')->fileInput()->label(false); ?>
            </div>

            <?= $form->field($modelSD, '[6]naziv')->hiddenInput(['value' => 'Preslika Rješenja da je roditelj djeteta korisnik doplataka za dijete '])->label(false) ?>
            <br/>
            <div class="col l8">
                <?= Yii::t('app','Preslika Rješenja da je roditelj djeteta korisnik doplataka za dijete')?><br/><br/>
                <?php echo $form->field($modelSD, '[6]filename')->fileInput()->label(false); ?>
            </div>

            <?= $form->field($modelSD, '[7]naziv')->hiddenInput(['value' => 'Ostalo '])->label(false) ?>
            <br/>
            <div class="col l8">
                <?= Yii::t('app','Ostalo')?><br/><br/>
                <?php echo $form->field($modelSD, '[7]filename')->fileInput()->label(false); ?>
            </div>
            <br/>
            <div class="col l4">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Dalje'), ['class' => 'btn btn-success right']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
