<?php

/**
 * @var $model \common\models\Mailbox
 */

use \common\models\Postavke;
?>

<div class="msg-content">
    <div class="light"><?= Yii::t('app', 'Sadržaj poruke:') ?></div>
    <br>
    <?= $model->message0->message ?>
    <br>
    <?php
    if (!empty($model->message0->attachments)) {
        echo '<div class="light">' . Yii::t('app', 'Prilozi') . ':</div><br>';
        foreach ($model->message0->attachments as $attachments) {
            $links = '';
            if (!empty($attachments)) {
                $weburl = Postavke::find()->where(['postavka' => 'backendWeb'])->one();
                foreach ($attachments as $attachment) {
                    $array = explode(',', $attachment);
                    $fileName = $array[array_key_last($array)];
                    $link = 'poruke/' . $model->message0->id . '/' . $attachment;
                    // dodan backendweb postavka za frontend datoteke u prilogu
                    if (isset($weburl) && isset($weburl->vrijednost)) {
                        $links .= "<a href='" . trim($weburl->vrijednost, '"') . "/$link' target='_blank'>" . $fileName . "</a><br>";
                    } else {
                        $links .= "<a href='admin/$link' target='_blank'>" . $fileName . "</a><br>";
                    }
                }
            }
        }
        echo $links;
    }
    ?>
</div>