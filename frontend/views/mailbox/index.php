<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\Poruke;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\MailboxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $folders \common\models\MsgFolders[] */
/* @var $msgFolder \common\models\MsgFolders */

$this->title = Yii::t('app', 'Poruke');
$this->params['breadcrumbs'][] = $this->title;
$user_id = Yii::$app->user->id;

?>
<div class="notifikacije-index site-about">

    <h1><?= Html::encode($this->title) ?></h1>



    <div class="row">
        <div class="col-md-4 col-lg-2">
            <div class="text-center">
                <?= Html::a('<span class="material-icons btn btn-default btn-round">add</span>', ['poruke/create'], ['class' => 'new-message']) ?></div>


            <div id="accordion" role="tablist">
                <div class="card-collapse mt-0">
                    <div class="card-header" role="tab" id="headingOne">
                        <h5 class="mb-0 mt-0">
                            <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                <?php echo Yii::t('app', 'Folders'); ?>
                                <i class="material-icons">keyboard_arrow_down</i>
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">
                            <ul>
                                <?php
                                foreach ($folders as $folder) {

                                ?>
                                    <li>
                                        <p class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/mailbox/index', 'folder' => $folder->folder_name]) ?>" id="<?= $folder->folder_name . '-' . $folder->id ?>" class="<?= $msgFolder->folder_name == $folder->folder_name ? 'active' : '' ?>">
                                                <span class="material-icons"><?= $folder->icon ?></span><?= ' ' . Yii::t('app', $folder->folder_name) ?> <?php // $folder->folder_name == $msgFolder->folder_name ? $count > 0 ? '(' . $count . ')' : '':'' 
                                                                                                                                                            ?>
                                            </a></p>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>


            </div>

        </div>
        <div class="col-md-8 col-lg-10">


            <style>
                @media screen and (max-width: 480px) {
                    .kv-table-wrap tr>td:first-child {
                        font-size: 1em;
                    }

                    span.clear {
                        clear: both;
                        display: block;
                    }
                }
            </style>


            <?php // echo $this->render('_search', ['model' => $searchModel]);
            ?>
            <div class="">
                <?= \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout' => "{items}{pager}\n",

                    'columns' => [
                        //'created_at',
                        [
                            'label' => Yii::t('app', 'Naslov poruke'),
                            'format' => 'html',
                            'value' => function ($data) {
                                $minute_diff = time() / 60 - strtotime($data->message0->created_at) / 60;
                                if ($minute_diff <= 60) {
                                    return '<span class="pull-left">' . $data->message0->subject . '</span><span class="pull-right">' . Yii::t('app', 'Prije {n, plural, one{# minute} few{# minute} other{# minuta}}', ['n' => ceil($minute_diff)]) . '</span><span class="clear"></span>
                                        <span class="clear"></span>';
                                } else {
                                    return '<span class="pull-left">' . $data->message0->subject . '</span><span class="pull-right">' . Yii::$app->formatter->asDate($data->created_at, 'php:d.m.Y. H:i') . '</span><span class="clear"></span>
                                        <span class="clear"></span>';
                                }
                            }
                        ],
                        [
                            'label' => Yii::t('app', 'Pošiljatelj'),
                            'value' => function ($data) {
                                /*
                                 * Dodati koji je user
                                 * */
                                $message = Poruke::find()->where(['id' => $data->message])->one();
                                $sender = $message->sender;
                                //$tkoJeSlao = $data->user0->username;
                                $user = User::find()->where(['id' => $sender])->one();
                                $tkoJeSlao = $user->username;
                                //print("<pre>".print_r($tkoJeSlao,true)."</pre>");die();
                                $tkoJeZap = \common\models\Zaposlenik::find()->where(['user' => $sender])->one();
                                if (empty($tkoJeZap)) {
                                    $roditelj = \common\models\UserRoditelj::find()->where(['user' => $sender])->one();

                                    if (!empty($roditelj)) {
                                        $temp = \common\models\Roditelj::find()->where(['id' => $roditelj->roditelj])->one();
                                        $tkoJeSlao = $temp->prezime . ' ' . $temp->ime;
                                    }
                                } else {
                                    $tkoJeSlao = $tkoJeZap->prezime . ' ' . $tkoJeZap->ime;
                                }
                                return $tkoJeSlao;
                            }
                        ],
                        [
                            'label' => Yii::t('app', 'Primatelji'),
                            'contentOptions' => ['style' => 'max-width: 100px;word-break:normal;white-space:normal'],
                            'value' =>
                            function ($data) {
                                if ($data->message0->recipients !== null) {
                                    $cc = $data->message0->recipients;
                                    $data = '';
                                    foreach ($cc as $key => $value) {
                                        $data .= $this->context->ccrec($key, $value);
                                    }

                                    return $data;
                                }
                            }
                        ],
                        [
                            'label' => Yii::t('app', 'Cc'),
                            'contentOptions' => ['style' => 'max-width: 100px;word-break:normal;white-space:normal'],
                            'value' =>
                            function ($data) {
                                if ($data->message0->cc_recipients !== null) {
                                    $cc = $data->message0->cc_recipients;
                                    $data = '';
                                    foreach ($cc as $key => $value) {
                                        $data .= $this->context->ccrec($key, $value);
                                    }
                                    return $data;
                                }
                            }
                        ],

                        [
                            'class' => 'kartik\grid\ExpandRowColumn',
                            'width' => '50px',
                            'value' => function ($model, $key, $index, $column) {
                                return \kartik\grid\GridView::ROW_COLLAPSED;
                            },
                            // uncomment below and comment detail if you need to render via ajax
                            //     'detailUrl' => Url::to(['/poruke/book-details']),
                            'detail' => function ($model, $key, $index, $column) {

                                return Yii::$app->controller->renderPartial('_expand-row-details', ['model' => $model]);
                            },
                            'headerOptions' => ['class' => 'kartik-sheet-style'],
                            'header' => false,
                            'expandOneOnly' => true,
                            'enableRowClick' => true,
                            'expandIcon' => '<span class="material-icons">visibility</span>',
                            'collapseIcon' => '<span class="material-icons">visibility_off</span>',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{reply}{reply_all}{resend}{restore}{delete}{open_template}{open_draft}',
                            // 'visible' => Yii::$app->user->can('admin'),
                            'buttons' => [
                                'reply' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/reply', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue reply_btn">reply</span>', $url2, [
                                        'title' => Yii::t('app', 'Odgovori')
                                    ]);
                                },
                                'reply_all' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/reply-all', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue reply_btn">reply_all</span>', $url2, [
                                        'title' => Yii::t('app', 'Odgovori svima')
                                    ]);
                                },
                                'resend' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/resend', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue send_again_btn">repeat</span>', $url2, [
                                        'title' => Yii::t('app', 'Ponovno pošalji')
                                    ]);
                                },
                                'open_template' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/open-template', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue open_template_btn">open_in_browser</span>', $url2, [
                                        'title' => Yii::t('app', 'Otvori Predložak')
                                    ]);
                                },
                                'open_draft' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/open-draft', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue open_draft_btn">open_in_browser</span>', $url2, [
                                        'title' => Yii::t('app', 'Otvori Skicu')
                                    ]);
                                },
                                'restore' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/restore', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue restore_btn">settings_backup_restore</span>', $url2, [
                                        'title' => Yii::t('app', 'Vrati iz smeća')
                                    ]);
                                },
                                'delete' => function ($url, $data) {
                                    $user_id = Yii::$app->user->id;
                                    $url2 = \yii\helpers\Url::to(['poruke/trash', 'id' => $data->message0->id, 'user' => $user_id, 'folder'=>$data->folder0->id]);
                                    return Html::a('<span class="material-icons red">delete</span>', $url2, [
                                        'title' => Yii::t('app', 'Obriši'), 'data-method' => 'POST'
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['poruke/delete-all-trash', 'id' => $user_id], [
                    'class' => 'btn btn-danger btn-sm pull-right',
                    'id' => 'trash_all',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete all messages?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <div class="mob">
                <?= \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout' => "{items}{pager}\n",

                    'columns' => [
                        //'created_at',
                        [
                            'label' => Yii::t('app', 'Naslov poruke'),
                            'format' => 'html',
                            'value' => function ($data) {
                                $minute_diff = time() / 60 - strtotime($data->message0->created_at) / 60;
                                $message = Poruke::find()->where(['id' => $data->message])->one();
                                $sender = $message->sender;
                                //$tkoJeSlao = $data->user0->username;
                                $user = User::find()->where(['id' => $sender])->one();
                                $tkoJeSlao = $user->username;
                                $tkoJeZap = \common\models\Zaposlenik::find()->where(['user' => $sender])->one();
                                if (empty($tkoJeZap)) {
                                    $roditelj = \common\models\UserRoditelj::find()->where(['user' => $sender])->one();

                                    if (!empty($roditelj)) {
                                        $temp = \common\models\Roditelj::find()->where(['id' => $roditelj->roditelj])->one();
                                        $tkoJeSlao = $temp->prezime . ' ' . $temp->ime;
                                    }
                                } else {
                                    $tkoJeSlao = $tkoJeZap->prezime . ' ' . $tkoJeZap->ime;
                                }
                                if ($minute_diff <= 60) {

                                    return '<span class="pull-left">' . $tkoJeSlao . '</span><span class="pull-right">' . Yii::t('app', 'Prije {n, plural, one{# minute} few{# minute} other{# minuta}}', ['n' => ceil($minute_diff)]) . '</span>';
                                } else {
                                    return '<span class="pull-left">' . $tkoJeSlao . '</span><span class="pull-right">' . Yii::$app->formatter->asDate($data->created_at, 'php:d.m.Y. H:i') . '</span>';
                                }
                            }
                        ],
                        [
                            'label' => Yii::t('app', 'Pošiljatelj'),
                            'format' => 'html',
                            'value' => function ($data) {
                                /*
                                 * Dodati koji je user
                                 * */
                                $subject = $data->message0->subject;

                                return '<span class="pull-left">' . $subject . '</span><span class="pull-right"><span class="material-icons yellow">visibility</span></span>';
                            }
                        ],
                        [
                            'label' => Yii::t('app', 'Primatelji'),
                            'contentOptions' => ['style' => 'max-width: 100px;word-break:normal;white-space:normal'],
                            'value' =>
                            function ($data) {
                                if ($data->message0->recipients !== null) {
                                    $cc = $data->message0->recipients;
                                    $data = '';
                                    foreach ($cc as $key => $value) {
                                        $data .= $this->context->ccrec($key, $value);
                                    }

                                    return $data;
                                }
                            }
                        ],
                        [
                            'label' => Yii::t('app', 'Cc'),
                            'contentOptions' => ['style' => 'max-width: 100px;word-break:normal;white-space:normal'],
                            'value' =>
                            function ($data) {
                                if ($data->message0->cc_recipients !== null) {
                                    $cc = $data->message0->cc_recipients;
                                    $data = '';
                                    foreach ($cc as $key => $value) {
                                        $data .= $this->context->ccrec($key, $value);
                                    }
                                    return $data;
                                }
                            }
                        ],

                        [
                            'class' => 'kartik\grid\ExpandRowColumn',
                            'width' => '50px',
                            'value' => function ($model, $key, $index, $column) {
                                return \kartik\grid\GridView::ROW_COLLAPSED;
                            },
                            // uncomment below and comment detail if you need to render via ajax
                            //     'detailUrl' => Url::to(['/poruke/book-details']),
                            'detail' => function ($model, $key, $index, $column) {

                                return '<div class="msg-content">Sadržaj poruke:<br><br></div>' . Yii::$app->controller->renderPartial('_expand-row-details', ['model' => $model]);
                            },
                            'headerOptions' => ['class' => 'kartik-sheet-style'],
                            'header' => false,
                            'expandOneOnly' => true,
                            'enableRowClick' => true,
                            'expandIcon' => '<span class="material-icons">visibility</span>',
                            'collapseIcon' => '<span class="material-icons">visibility_off</span>',
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{reply}{reply_all}{resend}{restore}{delete}{open_template}{open_draft}',
                            // 'visible' => Yii::$app->user->can('admin'),
                            'buttons' => [
                                'reply' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/reply', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue reply_btn">reply</span>', $url2, [
                                        'title' => Yii::t('app', 'Odgovori'),
                                    ]);
                                },
                                'reply_all' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/reply-all', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue reply_btn">reply_all</span>', $url2, [
                                        'title' => Yii::t('app', 'Odgovori svima')
                                    ]);
                                },
                                'resend' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/resend', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue send_again_btn">repeat</span>', $url2, [
                                        'title' => Yii::t('app', 'Ponovno pošalji')
                                    ]);
                                },
                                'open_template' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/open-template', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue open_template_btn">open_in_browser</span>', $url2, [
                                        'title' => Yii::t('app', 'Otvori Predložak')
                                    ]);
                                },
                                'open_draft' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/open-draft', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue open_draft_btn">open_in_browser</span>', $url2, [
                                        'title' => Yii::t('app', 'Otvori Skicu')
                                    ]);
                                },
                                'restore' => function ($url, $data) {
                                    $url2 = \Yii::$app->urlManager->createUrl(['poruke/restore', 'id' => $data->message0->id]);
                                    return Html::a('<span class="material-icons hblue restore_btn">settings_backup_restore</span>', $url2, [
                                        'title' => Yii::t('app', 'Vrati iz smeća')
                                    ]);
                                },
                                'delete' => function ($url, $data) {
                                    $user_id = Yii::$app->user->id;
                                    $url2 = \yii\helpers\Url::to(['poruke/trash', 'id' => $data->message0->id, 'user' => $user_id, 'folder'=>$data->folder0->id]);
                                    return Html::a('<span class="material-icons red">delete</span>', $url2, [
                                        'title' => Yii::t('app', 'Obriši'), 'data-method' => 'POST'
                                    ]);
                                },

                            ],
                        ],
                    ],
                ]); ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['poruke/delete-all-trash', 'id' => $user_id], [
                    'class' => 'btn btn-danger btn-sm',
                    'id' => 'trash_all',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    function show_trash_all() {
        const div_trash = document.querySelector('#trash-5');
        if (div_trash.classList.contains('active')) {
            document.getElementById('trash_all').style.display = "inline-block";
        } else {
            document.getElementById('trash_all').style.display = "none";
        }
    }

    function show_reply() {
        const div_received = document.querySelector('#inbox-1');
        var button_list = document.getElementsByClassName('reply_btn');

        if (div_received.classList.contains('active')) {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "inline-block";
                }
            }
        } else {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "none";
                }
            }
        }
    }

    function show_resend() {
        const div_sent = document.querySelector('#sent-4');
        var button_list = document.getElementsByClassName('send_again_btn');

        if (div_sent.classList.contains('active')) {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "inline-block";
                }
            }
        } else {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "none";
                }
            }
        }
    }

    function show_restore() {
        const div_sent = document.querySelector('#trash-5');
        var button_list = document.getElementsByClassName('restore_btn');

        if (div_sent.classList.contains('active')) {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "inline-block";
                }
            }
        } else {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "none";
                }
            }
        }
    }

    function show_open_template() {
        const div_sent = document.querySelector('#templates-3');
        var button_list = document.getElementsByClassName('open_template_btn');

        if (div_sent.classList.contains('active')) {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "inline-block";
                }
            }
        } else {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "none";
                }
            }
        }
    }

    function show_open_draft() {
        const div_sent = document.querySelector('#drafts-2');
        var button_list = document.getElementsByClassName('open_draft_btn');

        if (div_sent.classList.contains('active')) {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "inline-block";
                }
            }
        } else {
            if (button_list) {
                for (var i = 0; i < button_list.length; i++) {
                    button_list[i].style.display = "none";
                }
            }
        }
    }

    show_trash_all();
    show_reply();
    show_resend();
    show_restore();
    show_open_draft();
    show_open_template();
</script>