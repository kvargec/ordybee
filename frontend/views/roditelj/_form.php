<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Roditelj */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="roditelj-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prezime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'spol')->dropDownList(['Ž'=>Yii::t('app','Ženski'),'M'=>Yii::t('app','Muški')]) ?>

    <div class="input-field">
        <?= DatePicker::widget([
            'model' => $model,
            'attribute' => 'dat_rod',
            'convertFormat' => true,
            'options' => ['placeholder' => Yii::t('app','Datum rođenja majke')],
            'pluginOptions' => [
                'autoClose'=>true
            ]
        ]);
        ?>
    </div>

    <?= $form->field($model, 'oib')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adresa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mjesto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prebivaliste')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'radno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zanimanje')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'poslodavaca')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobitel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dijete')->hiddenInput(['value' => $model->dijete])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
