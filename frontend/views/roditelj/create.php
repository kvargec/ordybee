<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Roditelj */

$this->title = Yii::t('app', 'Create Roditelj');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roditeljs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="roditelj-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
