<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Cjenik */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cjenik-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'program')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\VrstaPrograma::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'cijena')->textInput() ?>

    <?= $form->field($model, 'povlastena')->textInput() ?>

    <?= $form->field($model, 'ped_godina')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\PedagoskaGodina::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
