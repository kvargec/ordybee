<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cjenik */

$this->title = Yii::t('app', 'Create Cjenik');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cjeniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cjenik-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
