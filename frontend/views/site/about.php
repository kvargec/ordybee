<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

// $this->title = 'About';
// $this->params['breadcrumbs'][] = $this->title;
// use Yii;
// use common\models\Postavke;


?>
<div class="site-about">
    <blockquote style="margin-top:40px">
        <p>IZJAVA O PRIVATNOSTI</p>
    </blockquote>
    <blockquote>
        <p>Što sadrži ova Izjava o privatnosti</p>
    </blockquote>
    <p>
        Ova Izjava opisuje kako <?php

                                $vrtic = \common\models\Postavke::find()->where(['postavka' => 'nazivVrtica'])->one();
                                echo $vrtic->vrijednost;
                                ?> prikuplja, koristi, čuva i prenosi
        osobne podatke o djeci korisnicima usluga, njihovim roditeljima ili skrbnicima
        odnosno njihovim punomoćnicima.<br><br>
        Nadalje, ova Izjava opisuje kao <?php echo $vrtic->vrijednost; ?> kao poslodavac prikuplja, koristi,
        čuva i prenosi osobne podatke o radnicima i svih drugim osobama čijim se radom
        koristi u bilo kojem statusu.<br><br>
        Također, ova Izjava opisuje kako <?php echo $vrtic->vrijednost; ?> (dalje: mi) prikuplja, koristi, čuva
        i prenosi osobne podatke o svim drugim osobama čije proizvode kupuje ili čije usluge
        koristi, s kojima je u bilo kakvom ugovornom ili poslovnom odnosu odnosno
        poslovnom kontaktu.<br><br>
        Ova Izjava se odnosi i na osobne podatke posjetitelja naše web stranice ili pošiljatelja
        ili primatelja poruke naše elektroničke pošte.<br><br>
        Naš je cilj zaštiti vašu privatnost u skladu sa zahtjevima Opće uredbe o zaštiti
        podataka. Ako koristite naše usluge, stupate u poslovni odnos s nama ili u poslovnu
        komunikaciju, pri tome trebate bit svjesni da obrađujemo vaše osobne podatke koji
        su nam potrebni u obavljanju naše djelatnosti.<br><br></p>
    <blockquote>
        <p>Vaša obaviještenost</p>
    </blockquote>
    <p>
        Ako ste jedna od osoba čije osobne podatke obrađujemo vaše je pravo da budete
        obaviješteni o tome, a naša je obveza da vas obavijestimo o svrsi i načinu obrade
        vaših osobnih podataka i načinu kako štitimo vašu privatnost provodeći određene
        politike zaštite vaših osobnih podataka.<br><br>
        Molimo vas da prilikom stupanja u bilo kakav kontakt ili odnos s nama: bilo da
        nam šaljete poruku elektroničke pošte, posjećujete našu web stranicu, koristite
        našu uslugu, stupate u bilo kakav poslovni ili pravni odnos s nama ili stupate u
        bilo koji oblik poslovne komunikacije, pažljivo pročitate ovu Izjavu, kako biste
        bili svjesni naše obrade vaših osobnih podataka, učinaka takve obrade te vaših
        prava prema nama u odnosu na vaše osobne podatke.<br><br>
        Stupanjem u bilo koju vrstu od navedenih kontakata, poslovnih ili pravnih
        odnosa potvrđujete da ste upoznati s ovom Izjavom.<br><br>
    <p>
    <p>
        <!-- Voditelj obrade<br>
        <?php echo $vrtic->vrijednost; ?>,<br>
        <?php if (isset($jsonPodaci[0]['Adresa'])) {
            echo $jsonPodaci[0]['Adresa'];
        } ?><br>
        <?php if (isset($jsonPodaci[0]['Mjesto'])) {
            echo $jsonPodaci[0]['Mjesto'];
        } ?> -->
        <!-- tel: 01/2791-349<br> -->
        <!-- e-mail: info@dvvrbovec.hr<br> -->
    </p>
    <!-- Službenik za zaštitu podataka
Nenad Šimunec, odvjetnik
Zagreb, Marjanovićev prilaz 13
e-mail: nenad.simunec@optinet.hr -->
    <blockquote>
        <p>Koje osobne podatke prikupljamo i obrađujemo</p>
    </blockquote>
    <p>
        Ovisno o vašem statusu u odnosu na nas, o vama prikupljamo različite vrste osobnih
        podataka.<br><br>
        Najčešće, ali ne isključive, vrste osobnih podataka koje obrađujemo su:<br>
        - ime i prezime,<br>
        - adresa,<br>
        - OIB,<br>
        - datum rođenja,<br>
        - brojevi telefona,<br>
        - adresa elektroničke pošte.<br><br>
    </p>
    <blockquote>
        <p>Djeca</p>
    </blockquote>
    <p>
        S obzirom da smo odgojno - obrazovna ustanova koja obavlja djelatnost
        predškolskog odgoja i obrazovanja najvažnija i najveća grupa fizičkih osoba čije
        osobne podatke obrađujemo su djeca - korisnici naših usluga.<br><br>
        O djeci obrađujemo više vrsta osobnih podataka, kao što su npr.: ime i prezime,
        datum rođenja, adresa, podaci o roditeljima odnosno sve one vrste osobnih podataka
        koje su propisane Zakonom o predškolskom odgoju i obrazovanju, Pravilnikom o
        obrascima i sadržaju pedagoške dokumentacije i evidencije o djeci u dječjem vrtiću,
        Pravilnikom o obrascima zdravstvene dokumentacije djece predškolske dobi i
        evidencije u dječjem vrtiću, Programom zdravstvene zaštite djece, higijene i pravilne
        prehrane djece u dječjim vrtićima, Pravilnikom o upisu i ostvarivanju prava i obveza
        korisnika usluga vrtića te drugim propisima.<br><br>
        Među podacima koji se obrađuju o djeci nalaze se i osobni podaci koji spadaju u
        posebne kategorije osobnih podataka, kao što su podaci o zdravstvenom stanju
        djece, a koji su nam potrebni da bismo mogli izvršavati svoje zakonom propisane
        obveze koje se odnose na očuvanja zdravlja djece.<br><br>
    </p>
    <blockquote>
        <p>Roditelji, skrbnici odnosno njihovim punomoćnici, članovi kućanstva</p>
    </blockquote>
    <p>
        O roditeljima, skrbnicima odnosno njihovim punomoćnicima obrađuju se one vrste
        osobnih podataka koje su propisane Zakonom o predškolskom odgoju i obrazovanju,
        Pravilnikom o obrascima i sadržaju pedagoške dokumentacije i evidencije o djeci u
        dječjem vrtiću, Pravilnikom o obrascima zdravstvene dokumentacije djece
        predškolske dobi i evidencije u dječjem vrtiću, Programom zdravstvene zaštite djece,
        higijene i pravilne prehrane djece u dječjim vrtićima, Pravilnikom o upisu i
        ostvarivanju prava i obveza korisnika usluga vrtića te drugim
        propisima.<br><br>
        Ako roditelji ili skrbnici ovlaste drugu osobu na obavljanje pojedinih radnji s djetetom
        (npr. dovođenje ili odvođenje iz Ustanove), o tim osobama se obrađuju osobni podaci
        koji su potrebni za nesumnjivu identifikaciju te osobe, uključujući i vizualnu
        identifikaciju te izdavanje punomoći.<br><br>
    </p>
    <blockquote>
        <p>Zastupnici i predstavnici pravnih osoba</p>
    </blockquote>
    <p>
        Obrađuju se one vrste osobnih podataka koje su sadržane u pismima, dopisima ili
        drugim poslovnim dokumentima koje primamo.<br><br></p>

    <blockquote>
        <p>Videosnimke</p>
    </blockquote>
    <p>
        Radi osiguranja sigurnosti objekata, djece, radnika i posjetitelja, objekti i radne
        prostorije nadziru se videonadzorom.<br><br>
    </p>
    <blockquote>
        <p>U koje svrhe prikupljamo osobne podatke i po kojoj pravnoj osnovi</p>
    </blockquote>
    <p>
        Najveći broj osobnih podataka obrađujemo o djeci korisnicima vrtića, njihovim
        roditeljima, skrbnicima njihovim punomoćnicima i članovima kućanstva. Ove podatke
        obrađujemo da bismo mogli izvršavati našu funkciju predškolskog odgoja i
        obrazovanja, odrediti i naplaćivati naknadu za korištenje naše usluge u skladu s
        propisima. Ove podatke uglavnom obrađujemo na osnovi Zakona o predškolskom
        odgoju i obrazovanju i svih podzakonskih općih akata u vezi s navedenim zakonom
        te općih akata osnivača.<br><br>
        Za određene podatke koji nisu propisani navedenim propisima tražit ćemo
        odgovarajuću privolu roditelja odnosno skrbnika.<br><br>
        Osobne podatke o radnicima i drugim osobama čijim se radom koristimo obrađujemo
        da bismo sklopili ugovor o radu i izvršavali sve naše propisane obveze u vezi s tim
        ugovorom te omogućili ostvarivanje prava tih osoba. Ove podatke obrađujemo na
        osnovi Zakona o radu i drugih propisa kojima se uređuju prava i obveze u vezi s
        radnim odnosom.<br><br>
        Ako su nam potrebni drugi dodatni osobni podaci o tome ćemo tražiti privolu tih
        osoba.<br><br>
        Videonadzor vršimo jer imamo legitimni interes osigurati naše objekte, djecu, radnike
        i sve osobe koje u njima borave, od rizika provala, krađi i drugih sličnih radnji.<br><br>
    </p>
    <blockquote>
        <p>Kako prikupljamo osobne podatke</p>
    </blockquote>
    <p>
        Osobne podatke prikupljamo najčešće da ih tražimo od samih ispitanika. Za neke
        podatke nužno moramo tražiti da nam predate određene dokumente i isprave i to u
        slučajevima u kojima nam je to određeno propisom.<br><br>
        Određene osobne podatke prikupit ćemo izravno iz usmenih izjava ispitanika.
        Osobni podaci u korištenju naše web stranice ili elektroničke pošte automatizirano se
        bilježe.<br><br>
        Videonadzorom bilježimo sve aktivnosti koje se provode u perimetru snimanja.<br><br>
    </p>
    <blockquote>
        <p>Kome prenosimo vaše osobne podatke</p>
    </blockquote>
    <p>
        Vaše osobne podatke prenosimo trećima- institucijama u izvršenju naših propisanih
        obveza. Primatelji vaših osobnih podataka su: tijela državne uprave, tijela osnivača,
        druge državne organizacije i institucije. Vaše osobne podatke
        možemo prenijeti i drugim pravnim osobama kada je to potrebno radi izvršenja naših
        propisanih obveza, radi ostvarivanja vaših prava ili osnovanih interesa.
        Određene podatke, osobito o djeci, prenosimo primateljima u svrhu izvršenja naše
        funkcije, brige o djeci i njihovu zdravlju.<br><br>
        Ako imate nepodmirenih novčanih obveza prema nama, zbog čega smo prisiljeni
        provesti postupak prisilne naplate, vaše podate ćemo proslijediti odvjetnicima.<br><br>
    </p>
    <blockquote>
        <p>Ako nam odbijete dati vaše osobne podatke</p>
    </blockquote>
    <p>
        Ako nam odbijete dati osobne podatke o djeci ili u vezi s djecom, čija nam je obrada
        određena propisom nećemo vam moći pružiti uslugu predškolskog odgoja i
        obrazovanja ili nećemo moći izvršiti pojedine naše obveze kako su propisane. Radi
        toga vaša djeca ili vi kao roditelji ili skrbnici možete biti uskraćeni u određenim
        pravima koja biste inače mogli ostvariti.<br><br>
        Ako nam kao ostali ispitanici odbijete dati podatke koji su određeni propisom, mi
        možda nećemo biti u mogućnosti izvršiti našu obvezu zbog čega nećete moći
        ostvariti neko svoje pravo.<br><br>
        Ako nam odbijete dati osobne podatke koji su nužni radi sklapanja i izvršenja
        ugovora nećemo moći s vama sklopiti ugovor.<br><br>
        Ako nam odbijete dati osobne podatke koje obrađujemo na osnovi legitimnog
        interesa to može imati za posljedicu da nećete moći komunicirati s nama ili nećete
        moći ostvariti neki svoj interes.<br><br>
    </p>
    <blockquote>
        <p>Zaštita osobnih podataka djece</p>
    </blockquote>
    <p>
        S obzirom da smo svjesni rizika izloženosti osobnih podataka o djeci kao osobito
        ranjivoj grupi ispitanika, nastojimo, gdje god je to moguće, izbjeći objavu imena i
        prezimena djeteta ili drugih osobnih podataka (na oglasnoj ploči, na internetu i dr.) na
        osnovi kojih se može izravno identificirati određeno dijete. Pri tome koristimo zaštitne
        mjere anonimizacije ili pseudonimizacije.<br><br>
        U provedbi odgojno-obrazovnih aktivnosti u skladu s Godišnjim planom i programom
        odgojno-obrazovnog rada vrtića, vršimo pojedine radnje obrade
        osobnih podataka pri čemu snimamo ili fotografiramo djecu radi bilježenja takvih
        aktivnosti. Snimke odnosno fotografije koristimo za stručni rad s djecom, stručno
        usavršavanje, informiranja roditelja i promicanje rada ustanove. Za takvu obradu
        tražimo privolu roditelja.<br><br>
        Ako namjeravamo takve snimke ili fotografije javno objavljivati, za to tražimo
        posebnu privolu roditelja. Također tražimo privola roditelja za javnu objavu bilo kojih
        drugih osobnih podataka o djetetu.<br><br>
        Privola roditelja daje se prilikom upisa djeteta u Dječji vrtić ili kasnije. Roditelj može u
        svakom trenutku povući danu privolu.<br><br>
    </p>
    <blockquote>
        <p>Rok čuvanja osobnih podataka</p>
    </blockquote>
    <p>
        Osobne podatke čuvamo u rokovima koji su nam određeni propisom.<br><br>
        Ako razdoblje čuvanja nije određeno propisom niti u našom Politikom zaštite
        podataka, odredit ćemo ga sami u skladu s načelom ograničenja obrade.<br><br>
        Osobni podaci brišu se:<br>
        - po isteku roka čuvanja;<br>
        - pa usvojenom zahtjevu za brisanje kojeg je podnio ispitanik.<br><br>
    </p>
    <blockquote>
        <p>Zaštita vaših osobnih podataka</p>
    </blockquote>
    <p>
        Poduzimamo odgovarajuće tehničke i organizacijske mjere s ciljem zaštite
        prikupljenih osobnih podataka i sprječavanja slučajnog ili nezakonitog uništenja,
        gubitka, izmjene, neovlaštenog otkrivanja ili pristupa osobnim podacima.
        Osobitu pažnju posvećujemo zaštiti osobnih podataka o djeci.<br><br>
        U zaštiti vaših osobnih podataka postupamo u skladu s našom Politikom zaštite
        osobnih podataka.<br><br>
    </p>
    <blockquote>
        <p>Privola za obradu osobnih podataka</p>
    </blockquote>
    <p>
        Ako se obrada određene vrste osobnog podatka temelji na privoli ili je za objavu ili
        prijenos osobnog podatka potrebna privola, istu ćemo od vas pribaviti u pisanom
        obliku.<br><br>
        Prilikom davanja privole informirat ćemo vas o svrsi davanja privole i posljedici ako
        odbijete dati privolu. Vaša privola mora biti dobrovoljna i nedvosmislena.<br><br>
        Pisana privola čuva se onoliko vremena koliko se čuvaju i osobni podaci na koju se
        odnosi.<br><br>
        Ako ste dali privolu za određenu obradu osobnih podataka imate pravo u svako doba
        povući privolu. Povlačenje privole ne utječe na zakonitost obrade prije njezina
        povlačenja. Prilikom davanja privole o tome ćemo vas informirati.
        Radi urednog i dokumentiranog provođenja postupka zahtijevamo da izjavu o
        povlačenu privole predate u pisanom obliku na propisanom obrascu Povlačenje
        privole. Obrazac je dostupan u tajništvu i na našoj web stranici.<br><br>
    </p>
    <blockquote>
        <p>Ostvarivanje vaših prava</p>
    </blockquote>
    <p>
        Vi imate pravo obratiti nam se sa zahtjevom radi ostvarivanja nekog do prava koje
        vam pripadaju kao našem ispitaniku:<br>
        - pravo na pristup podacima,<br>
        - pravo na ispravak,<br>
        - na zaborav (brisanje),<br>
        - pravo na ograničenje obrade,<br>
        - pravo na prenosivost (ako je primjenjivo),<br>
        - pravo na prigovor (ako je primjenjivo).<br><br>
        Radi urednog i dokumentiranog provođenja postupka zahtijevamo da zahtjev za
        ostvarivanje prava predate u pisanom obliku na propisanom obrascu Zahtjev
        ispitanika. Obrazac je dostupan u tajništvu i na našoj web stranici.<br><br>
        Zahtjev se predaje neposredno u tajništvu ili poštom. Zahtjev se može dostaviti
        porukom elektroničke pošte, a smatra se urednim ako je dostavljen s adrese
        podnositelja zahtjeva. Osoba koja podnosi zahtjev mora se identificirati. Ako je
        zahtjev anoniman, a Ustanova ne može na lak i dostupan način izvršiti utvrđivanje
        identiteta, po zahtjevu se neće postupiti.<br><br>
        U roku od mjesec dana od primitka vašeg zahtjeva obavijestit ćemo vas o našoj
        odluci i poduzetim radnjama<br><br>
        Podnošenje prigovora Agenciji za zaštitu osobnih podataka<br><br>
        Ako u roku od mjesec dana ne primite naš odgovor na vaš zahtjev u vezi s obradom
        osobnih podataka imate pravo podnijeti prigovor Agenciji za zaštitu osobnih
        podataka. prigovor imate podnijeti i ako smatrate da smo našom odlukom ili
        postupanjem povrijedili vaša prava.<br><br>
    <p>
    <blockquote>
        <p>Promjene Izjave o privatnosti</p>
    </blockquote>
    <p>
        Ovisno o potrebama moguće je da ćemo promijeniti ovu Izjavu o privatnosti kako
        bismo naše postupanje unaprijedili i postigli veću zaštitu vašeg prava na privatnost ili
        ako će to zahtijevati promjene u propisima.<br><br>
        Svaku izmjenu ove Izjave odgovarajuće ćemo objaviti. Molimo vas da povremeno
        provjerite da li smo promijenili ovu našu Izjavu o privatnosti.<br><br>
    </p>
    <blockquote>
        <p>Objava</p>
    </blockquote>
    <p>
        Ova Izjava o privatnosti objavljuje se na web stranici te na oglasnoj ploči našeg
        <?php echo $vrtic->vrijednost; ?>.<br>
        Važeće od: 27. rujna 2018.<br>
        Posljednje ažuriranje: 08. travnja 2020.<br>
    </p>
</div>