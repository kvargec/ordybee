<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;

use common\models\Postavke;
?>
<div class="site-index">
    <div class="body-content">
        <div class="container text-center ">
            <div class="row ">

                <div class="col-lg-6 col-lg-offset-3">
                    <div class="card mb25">
                        <div class="card-header " data-background-color="blue">

                            <div>
                                <h4 class="title"><?= Yii::t('app', 'Dobro došli, {user}!', ['user' => Yii::$app->user->identity->username]) ?> </h4>
                            </div>
                        </div>
                        <?php $eupisi = Postavke::find()->where(['postavka' => 'eupisi'])->one();               ?>
                        <div class="card-content" <?php if ($eupisi->vrijednost == 'true') {
                                                        echo 'hidden';
                                                    } ?>>
                            <p><?= Yii::t('app', 'Predaja zahtjeva za upis') ?></p>

                            <?php
                            //            if (mktime(0, 0, 0, 4, 8, 2021) < time() and mktime(0, 0, 0, 4, 26, 2021)) {
                            echo \yii\helpers\Html::beginForm(['/zahtjev/create'], 'GET', ['id' => 'zahtjev-lokacija']);
                            echo \kartik\select2\Select2::widget([
                                'name' => 'lokacija',
                                'data' => ['' => Yii::t('app', 'Sve lokacije')] + \yii\helpers\ArrayHelper::map(\common\models\Objekt::find()->all(), 'id', function (\common\models\Objekt $data) {
                                    return $data->naziv . ', ' . $data->adresa . ', ' . $data->mjesto0->naziv;
                                })
                            ]);
                            echo \yii\helpers\Html::endForm();
                            ?>

                            <br>
                            <button class="btn btn-primary w5" onclick="$('#zahtjev-lokacija').submit()"><?= Yii::t('app', 'Izradi novi zahtjev') ?></button>

                            <a class="btn btn-secondary w5" href="<?php echo \yii\helpers\Url::to(['zahtjev/index']) ?>"><?= Yii::t('app', 'Pregled zahtjeva') ?></a>

                            <?php
                            //          }
                            ?>

                        </div>
                    </div>
                </div>
            </div>


            <div class="row ">

                <div class="col-lg-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="blue">
                            <span class="material-icons">email</span>
                        </div>
                        <div class="card-content">
                            <a href="#">
                                <h3 class="category"><a href="<?php echo \yii\helpers\Url::to(['mailbox/index']) ?>"><?= Yii::t('app', 'Poruke') ?></a></h3>
                            </a>
                        </div>
                        <div class="card-footer">
                            <div class="stats text-left">

                                <?php if (!empty($poruke)) {
                                    echo '<ul>';
                                    $i = 0;

                                    foreach ($poruke->getModels() as $poruka) {
                                        if ($i < 3) {
                                            echo '<li>';
                                            echo \yii\helpers\Html::a($poruka->message0->subject, \yii\helpers\Url::to(['mailbox/index']));
                                            echo '</li>';
                                            $i++;
                                        }
                                    }
                                    echo '</ul>';
                                } else {
                                    echo '<p> nema poruka </p>';
                                } ?>


                            </div>
                        </div>

                    </div>
                    <div class="card">
                        <div class="card-header " data-background-color="orange">

                            <div>
                                <h4 class="title"><?= Yii::t('app', 'Obavijesti') ?></h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <!--    <ul>
                            <li>Pridržavanje mjera</li>
                            <li>Prijedlog aktivnosti</li>
                            <li>Dani vrtića</li>
                        </ul>-->

                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header " data-background-color="blue">

                            <div>
                                <h4 class="title"><?= Yii::t('app', 'Upitnici') ?></h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <ul class="text-left">
                                <?php if (!empty($upitnici)) {
                                    foreach ($upitnici as $key => $value) { ?>

                                        <li><a href="<?php echo \yii\helpers\Url::to(['upitnik/view']) ?>&id=<?php echo $key; ?>"><?php echo $value; ?></a>
                                            <br>
                                            <!-- <div class="flex"><?php if (isset($upitnik->date_end)) { ?><span class="material-icons date-small">event</span><span class=" date-small"><?php echo  Yii::$app->formatter->asDate($upitnik->date_end, 'd. M. Y.'); ?></span></div><?php } ?> -->
                                        </li>

                                <?php }
                                } else {
                                    echo '<h4> nema upitnika </h4>';
                                } ?>
                            </ul>


                        </div>
                    </div>



                </div>

                <div class="col-lg-6">

                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <span class="material-icons">child_care</span>
                        </div>
                        <div class="card-content">
                            <h3 class="category"><?= Yii::t('app', 'Moje notifikacije') ?></h3>

                        </div>
                        <div class="card-footer">
                            <div class="stats text-left">
                                <?php if (!empty($djeca)) {
                                    foreach ($djeca as $dijete) {
                                ?>



                                        <p><?php $dijete->ime ?></p>
                                        <!--   <ul>
                                <li>prva vijest</li>
                                <li>druga vijest</li>
                                <li>treća vijest</li>
                            </ul> -->

                                <?php }
                                } else {
                                    echo ' <p>' . Yii::t('app', 'Nema notifikacija') . '</p>';
                                } ?>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header " data-background-color="blue">

                            <div>
                                <h4 class="title"><?= Yii::t('app', 'Slike i video') ?></h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <a href="<?php echo \yii\helpers\Url::to(['media/index']) ?>">Pogledajte slike i video materijale svoje djece</a>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header " data-background-color="orange">

                            <div>
                                <h4 class="title"><?= Yii::t('app', 'Zapisi') ?></h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <!--        <ul>
                            <li><a href="#">prvi zapis</a></li>
                            <p>kratki opis zapisa ako je potreban</p>
                            <li><a href="#">drugi zapis</a></li>
                            <p>kratki opis zapisa ako je potreban</p>
                            <li><a href="#">treći zapis</a></li>
                            <p>kratki opis zapisa ako je potreban</p>
                        </ul>-->

                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header " data-background-color="blue">

                            <div>
                                <h4 class="title"><?= Yii::t('app', 'Materijali') ?></h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <!-- <ul>
                            <li><a href="#"><img class="small-img" src="<?php echo Yii::getAlias('@web') . '/img/pdf.png'; ?>">prva vijest</a></li>
                            <li><a href="#">druga vijest</a></li>
                            <li><a href="#">treća vijest</a></li>
                        </ul> -->

                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card-chart blue" data-background-color="blue">

                            <div class="legend">
                                <?php if (!empty($djeca)) {
                                    foreach ($djeca as $dijete) {
                                ?>
                                        <div class="square"><span><?php if (isset($dijete->ime)) {
                                                                        echo $dijete->ime;
                                                                    } else {
                                                                        echo Yii::t('app', 'Nema djece');
                                                                    } ?></span></div>

                                <?php }
                                } else {
                                    echo ' <div class=""><span>' . Yii::t('app', 'Prisutnost djece') . ': ' .  Yii::$app->formatter->asDate($poc, 'php:j.n.Y.') . ' - ' . Yii::$app->formatter->asDate($kraj, 'php:j.n.Y.') . '</span></div>';
                                } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-bordered w-100">

                                <!-- <tr>
                                    <td style="font-size:12px">Dijete</td>
                                    <?php
                                    $i = 1;
                                    while ($i <= 31) {
                                        echo '<td style="font-size:12px">' . $i . '</td>';
                                        $i++;
                                    }
                                    ?>
                                </tr>
                                <tr>
                                    <td>Harland</td>
                                    <?php
                                    $i = 1;
                                    while ($i <= 25) {
                                        echo '<td><i class="glyphicon glyphicon-ok" style="font-size:10px"></i></td>';
                                        $i++;
                                    }

                                    while ($i > 25 && $i <= 31) {
                                        echo '<td><i class="glyphicon glyphicon-minus" style="font-size:10px;color:#d80e15"></i></td>';
                                        $i++;
                                    }
                                    ?>
                                </tr> -->
                                <tr>
                                    <?php
                                    // print_r($prisutnost);
                                    if (isset($prisutnost)) {
                                        foreach ($prisutnost[0] as $polje) { ?>
                                            <th>
                                                <?php echo $polje ?>
                                            </th>
                                    <?php }
                                    } ?>
                                </tr>

                                <?php
                                if (isset($prisutnost)) {
                                    for ($i = 1; $i < count($prisutnost); $i++) {
                                ?>
                                        <tr>
                                            <?php
                                            for ($j = 0; $j < count($prisutnost[0]); $j++) {
                                                $textCenter = $j > 0 ? ' style="text-align:center"' : '';
                                            ?>
                                                <td <?php echo $textCenter ?>>
                                                    <?php echo isset($prisutnost[$i][$j]) ? $prisutnost[$i][$j] : '-'; ?>
                                                </td>
                                            <?php }
                                            ?>
                                        </tr>
                                <?php }
                                }
                                ?>
                                <tr>
                            </table>
                        </div>
                        <!-- <div class="card-footer no-border">
                            <p></p>
                            <h4 class="title"><?= Yii::t('app', 'Prisutnost djece za') ?></h4>
                            <p></p>
                        </div> -->
                    </div>
                </div>



            </div>


        </div>
    </div>
</div>