<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Kontakt';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact site-about">
    

    <div class="row">
        <div class="site-about col-lg-6 col-lg-push-3">
        <h1><?= Html::encode($this->title) ?></h1>

<!-- <p>
    <?= Yii::t('app', 'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.')?>
</p> -->
            <?php $form = ActiveForm::begin(['id' => 'contact-form']);  ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label('Ime i prezime')  ?>

                <?= $form->field($model, 'email')->label('Email adresa')  ?>

                <?= $form->field($model, 'subject')->label('Naslov poruke') ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6])->label('Vaša poruka')  ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ])->label('Verifikacijski kod')  ?>

                <div class="form-group">
                    <?= Html::submitButton('Pošalji', ['class' => 'btn btn-primary w-50', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
