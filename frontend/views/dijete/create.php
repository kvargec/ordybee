<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dijete */

$this->title = Yii::t('app', 'Create Dijete');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dijetes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dijete-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'podvrsta' => $podvrsta
    ]) ?>

</div>