<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Dijete */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Ažuriraj dijete: {name}', [
    'name' => $model->ime.', '.$model->prezime,
]);
?>
<div class="roditelj-update">

<h1><?= Html::encode($this->title) ?></h1>

<div class="dijete-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="input-field">
            <?= $form->field($model, 'ime')->textInput()->label(Yii::t('app',"Ime djeteta")) ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($model, 'prezime')->textInput()->label(Yii::t('app',"Prezime djeteta")) ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($model, 'spol')->radioList([
                    'M' => Yii::t('app','muški'),
                    'Ž' => Yii::t('app','ženski')
                ])->label(Yii::t('app','Spol')); ?>
        </div>
        <br />
        <div class="input-field">
            <?= DatePicker::widget([
                'model' => $model,
                'attribute' => 'dat_rod',
                'convertFormat' => true,
                'options' => ['placeholder' => Yii::t('app','Datum rođenja djeteta')],
                'pluginOptions' => [
                    'todayHighlight' => true
                ]
            ]);
            ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($model, 'oib')->textInput()->label(Yii::t('app',"OIB djeteta")) ?>
        </div>
        <br />
        <div class="input-field">
            <?= $form->field($model, 'adresa')->textInput()->label(Yii::t('app',"Adresa stanovanja")) ?>
        </div>
        <br />

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Spremi izmjene'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>