<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\Dijete */
/* @var $form yii\widgets\ActiveForm */

$teskoceData = [
    'a' => Yii::t('app', 'oštećenja vida'),
    'b' => Yii::t('app', 'oštećenja sluha'),
    'c' => Yii::t('app', 'poremećaji govorno –glasovne komunikacije i specifične teškoće u učenju'),
    'd' => Yii::t('app', 'tjelesni invaliditet'),
    'e' => Yii::t('app', 'intelektualne teškoće (sindromi…)'),
    'f' => Yii::t('app', 'poremećaji u ponašanju uvjetovani organskim faktorima, ADHD'),
    'g' => Yii::t('app', 'poremećaj socijalne komunikacije; poremećaj iz autističnog spektra; autizam'),
    'h' => Yii::t('app', 'postojanje više vrsta i stupnjeva teškoća u psihofizičkom razvoju')
];

$dijagnostikaData = [
    'a' => Yii::t('app', 'vještačenjem stručnog povjerenstva socijalne skrbi'),
    'b' => Yii::t('app', 'nalazom i mišljenjem specijalizirane ustanove'),
    'c' => Yii::t('app', 'nalazom i mišljenjem ostalih stručnjaka')
];
?>

<div class="dijete-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="input-field">
        <?= $form->field($model, 'ime')->textInput()->label(Yii::t('app', "Ime djeteta")) ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'prezime')->textInput()->label(Yii::t('app', "Prezime djeteta")) ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'spol')->radioList([
            'M' => Yii::t('app', 'muški'),
            'Ž' => Yii::t('app', 'ženski')
        ])->label(Yii::t('app', 'Spol')); ?>
    </div>
    <br />
    <div class="input-field">
        <?= DatePicker::widget([
            'model' => $model,
            'attribute' => 'dat_rod',
            'convertFormat' => true,
            'options' => ['placeholder' => Yii::t('app', 'Datum rođenja djeteta')],
            'pluginOptions' => [
                'todayHighlight' => true
            ]
        ]);
        ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'oib')->textInput()->label(Yii::t('app', "OIB djeteta")) ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'adresa')->textInput()->label(Yii::t('app', "Adresa stanovanja")) ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'mjesto')->textInput()->label(Yii::t('app', "Grad / općina")) ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'prebivaliste')->textInput()->label(Yii::t('app', "Prijavljeno prebivalište")) ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'sestra')->radioList([
            'D' => Yii::t('app', 'Da'),
            'N' => Yii::t('app', 'Ne')
        ])->label(Yii::t('app', 'Ima li brata/sestru već upisane u dječji vrtić')); ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'cekanje')->radioList([
            'D' => Yii::t('app', 'Da'),
            'N' => Yii::t('app', 'Ne')
        ])->label(Yii::t('app', 'Je li dijete bilo na listi čekanja u dječjem vrtiću ')); ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'god_cekanja')->textInput()->label(Yii::t('app', 'Ako da, koje godine')); ?>
    </div>

    <div class="input-field">
        <?= $form->field($model, 'razvoj')->radioList([
            'uredan' => Yii::t('app', 'uredna razvojna linija'),
            'teskoce' => Yii::t('app', 'dijete s teškoćama u razvoju')
        ])->label(Yii::t('app', 'Razvojni status djeteta')); ?>
    </div>
    <div class="input-field">
        <?php
        if (!$podvrsta) {
            echo $form->field($modelDijete, 'vrsta_programa')->radioList(ArrayHelper::map(\common\models\VrstaPrograma::find()->where(['or', ['parent' => null], ['parent' => 0]])->all(), 'id', 'vrsta'))->label(Yii::t('app', 'ZAHTJEV PODNOSIM ZA UPIS DJETETA U SLJEDEĆI PROGRAM'));
        } else {
            echo $form->field($modelDijete, 'vrstadepdrop')->radioList(ArrayHelper::map(\common\models\VrstaPrograma::find()->where(['or', ['parent' => null], ['parent' => 0]])->all(), 'id', 'vrsta'))->label(Yii::t('app', 'ZAHTJEV PODNOSIM ZA UPIS DJETETA U SLJEDEĆI PROGRAM'));

            echo $form->field($modelDijete, 'vrsta_programa')->radioList([])->label(Yii::t('app', 'POTPROGRAM'));

            //echo $form->field($modelDijete, 'vrstadepdrop')->widget(Select2::classname(), [
            //    'data' => ArrayHelper::map(\common\models\VrstaPrograma::find()->where(['or', ['parent' => null], ['parent' => 0]])->all(), 'id', 'vrsta'),
            //    'options' => ['placeholder' => Yii::t('app', 'Odaberi vrstu krovnog programa...'), 'id' => 'vrstadepdrop'],
            //])->label(Yii::t('app', 'Krovni program'));
            //
            //echo $form->field($modelDijete, 'vrsta_programa')->widget(DepDrop::classname(), [
            //    'type' => DepDrop::TYPE_SELECT2,
            //    'options' => ['id' => 'vrsta_programa'],
            //    'select2Options' => [
            //        'pluginOptions' => [
            //            'placeholder' => Yii::t('app', 'Odaberite...'),
            //            'allowClear' => true,
            //            'multiple' => false
            //        ]
            //    ],
            //    'pluginOptions' => [
            //        'depends' => ['vrstadepdrop'],
            //        'placeholder' => Yii::t('app', 'Odaberite...'),
            //        'initialize' => true,
            //        'loadingText' => '',
            //        'url' => Url::to(['/zahtjev/vrsta-potprograma'])
            //    ]
            //])->label(Yii::t('app', "Potprogram"));
        }
        ?>
    </div>
    <hr />
    <div class="input-field">
        <label class="control-label"><?= Yii::t('app', 'Teškoće u razvoju') ?></label>
        <?= Select2::widget([
            'model' => $model,
            'attribute' => 'teskoce[]',
            'data' => $teskoceData,
            'options' => [
                'placeholder' => Yii::t('app', 'Odaberite iz liste...'),
                'multiple' => true
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'tags' => true,
                'tokenSeparators' => [',', ' '],
            ],
        ]);
        ?>
    </div>
    <br />
    <div class="input-field">
        <div class="input-field">
            <label class="control-label"><?= Yii::t('app', 'Dijagnostički postupak za utvrđivanje teškoća') ?></label>
            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'dijagnostika[]',
                'data' => $dijagnostikaData,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite iz liste...'),
                    'multiple' => true
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                ],
            ]);
            ?>
        </div>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'druge_potrebe')->textInput()->label(Yii::t('app', "druge specifične razvojne posebne potrebe djeteta (poremećaji s prkošenjem i suprotstavljanjem, poremećaj ophođenja, anksiozni poremećaj, depresivna stanja, tikovi, noćne more ili strahovi, fobije, neadekvatna privrženost, proživljeno traumatsko iskustvo, povučenost, sramežljivost, ispadi bijesa, agresija i drugo)")) ?>
    </div>
    <br />
    <div class="input-field">
        <?= $form->field($model, 'posebne')->textInput()->label(Yii::t('app', "posebne zdravstvene potrebe djeteta (alergije, posebna prehrana, kronična oboljenja, epy, astma, febrilne konvulzije i drugo)")) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Spremi izmjene'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$url = \yii\helpers\Url::to(['/zahtjev/vrsta-potprograma']);
$js = <<<JS

$(document).ready(function() {
    $("input[name='Dijete[vrstadepdrop]']").on("click", function(e) {
        //e.preventDefault();
        var data = $("input[name='Dijete[vrstadepdrop]']:checked").val();
        console.log(data);
        $.ajax({
            url: '$url',
            type: 'post',
            dataType: 'json',
            data: {'depdrop_parents':data}
        }).done(function(data, textStatus, jqXHR) {
            console.log(data);
            if (data.output.length>0) {
                $('#dijete-vrsta_programa').html(function() {
                    var html = '';
                    for(var i=0;i<data.output.length;i++) {
                        if (data.output[i]['id'] == 0) {
                            html += '<label><input type="radio" style="margin-right:5px" checked name="Dijete[vrsta_programa]" value="'+ data.output[i]['id'] +'"/>'+data.output[i]['name']+'</label>';
                        } else {
                            html += '<label><input type="radio" style="margin-right:5px" name="Dijete[vrsta_programa]" value="'+ data.output[i]['id'] +'"/>'+data.output[i]['name']+'</label>';
                        }     
                    }
                    return html;
                })
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            $("input[name='Dijete[vrstadepdrop]']").notify( errorThrown, 'error' );
        });
    });
});
JS;
$this->registerJs($js);
?>