<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DijeteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dijetes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dijete-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Dijete'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ime',
            'prezime',
            'spol',
            'dat_rod',
            //'oib',
            //'adresa',
            //'mjesto',
            //'prebivaliste',
            //'sestra',
            //'cekanje',
            //'god_cekanja',
            //'razvoj',
            //'vrsta_programa',
            //'teskoce',
            //'dijagnostika',
            //'druge_potrebe',
            //'posebne',
            //'create_at',
            //'update_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
