<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dijete */

$this->title = Yii::t('app', 'Ažuriraj dijete: {name}', [
    'name' => $model->ime . " " . $model->prezime,
]);
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dijetes'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dijete-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'podvrsta' => $podvrsta
    ]) ?>

</div>