<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Upitnik */

$this->title = Yii::t('app', 'Create Upitnik');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upitniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="upitnik-create site-about">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
