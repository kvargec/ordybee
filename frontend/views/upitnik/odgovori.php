<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Upitnik */

$this->title = 'Odgovori';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upitniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="upitnik-view site-about">
    <ul>
        <?php
        foreach ($pitanja as $pit) {
            echo '<li><h5>' . $pit->pitanje . '</h5>';
            echo '<p>';
            switch ($pit->vrsta_pitanja) {
                case 1:
                    $tt = json_decode($pit->odgovori, true);
                    echo $tt[$odgovori[$pit->id]];
                    break;
                case 2:
                    foreach ($odgovori[$pit->id] as $odg) {
                        $tt = json_decode($pit->odgovori);
                        foreach ($tt as $index => $odgovor) {
                            if ($odg === $index) {
                                echo $odgovor . '<br>';
                            }
                        }
                    };
                    break;
                case 3:
                case 4:
                    echo $odgovori[$pit->id];
                    break;
                case 5:
                    echo $odgovori[$pit->id];
                    break;
            }
            echo '</p></li>';
        }

        ?>
    </ul>
</div>