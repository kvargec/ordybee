<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\UserRoditelj;

/* @var $this yii\web\View */
/* @var $model common\models\Upitnik */

$this->title = $model->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upitniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="upitnik-view site-about">

    <span class="h1 pt-3"><?= Html::encode($this->title) ?> </span>


    <?php
    $user_roditelj = UserRoditelj::find()->where(['user' => Yii::$app->user->id])->one();
    if (!isset($user_roditelj)) { ?>
        <?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => '']); ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //  'id',
                'naziv',
                'opis:ntext',
                [
                    'label' => Yii::t('app', 'Ped.godina'),
                    'value' => function ($data) {
                        $temp1 = explode("-", $data->pedGodina->od);
                        $temp2 = explode("-", $data->pedGodina->do);
                        return $temp1[0] . './.' . $temp2[0];
                    }
                ],
                //'date_publish',
                [
                    'label' => Yii::t('app', 'Datum kraja'),
                    'value' => function ($data) {
                        $temp1 = $data->date_end;
                        return  Yii::$app->formatter->asDate($temp1, 'd. M. Y.');;
                    }
                ],
                //'created_at',
                //'creator',
                //'status',
                //'upitnik_for',
                'is_required:boolean',
            ],
        ]) ?>

        <h1><?= Html::encode($this->title) ?></h1>
    <?php } ?>

    <?php
    //print("<pre>".print_r($odgovori,true)."</pre>");die();

    echo Html::beginForm(['spremi'], 'post');
    foreach ($svaPitanja as $pitanje) { ?>
        <?php
        echo '<div class="form-group">';
        $odgovori = json_decode($pitanje->odgovori);
        echo '<label class="control-label h4">' . $pitanje->pitanje . '</label>';
        switch ($pitanje->vrsta_pitanja) {
            case 1:

                echo '<div role="radiogroup">';
                foreach ($odgovori as $index => $odgovor) {
                    echo '<label>';
                    echo '<input type="radio"  name="pitanje_' . $pitanje->id . '" value="' . $index . '"> ' . $odgovor;
                    echo '</label>';
                }
                echo '</div>';
                break;
            case 2:

                echo '<div class="form-group">';
                $i = 1;

                foreach ($odgovori as $index => $odgovor) {
                    echo '<label class="control-label mr-1">';
                    echo '<input type="checkbox" name="pitanje_' . $pitanje->id . '[]" value="' . $index . '" > ' . $odgovor;
                    echo '</label>';
                    //if($i%3==0){
                    echo '<br>';
                    // }
                    $i++;
                }

                echo '</div>';
                break;
            case 3:

                echo '<input type="text" class="form-control" name="pitanje_' . $pitanje->id . '" > ';
                break;
            case 4:

                echo '<textarea name="pitanje_' . $pitanje->id . '" class="form-control"></textarea>';
                break;
            case 5:
                echo '<input type="text" class="form-control dates" name="pitanje_' . $pitanje->id . '" > ';
                break;
        }
        echo '</div> <br />';
        ?>
    <?php }
    echo '<button type="submit" class="btn btn-success right">Spremi</button>';
    echo Html::endForm();
    ?>

</div>
</div>