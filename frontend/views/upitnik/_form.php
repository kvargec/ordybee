<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Upitnik */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="upitnik-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opis')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ped_godina')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\PedagoskaGodina::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'date_publish')->textInput() ?>

    <?= $form->field($model, 'date_end')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'creator')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\User::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\StatusOpce::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'upitnik_for')->textInput() ?>

    <?= $form->field($model, 'is_required')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
