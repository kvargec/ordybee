<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\UpitnikSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Upitniks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="upitnik-index site-about">

    <h1><?= Html::encode($this->title) ?> <?= Html::a(Yii::t('app', 'Create Upitnik'), ['create'], ['class' => 'btn btn-success float-r']) ?></h1>

 

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'naziv',
            'opis:ntext',
            [
                'label' => Yii::t('app','Ped.godina'),
                'value' => function ($data) {
                    $temp1 = explode("-", $data->pedGodina->od);
                    $temp2 = explode("-", $data->pedGodina->do);
                    return $temp1[0] . './.' . $temp2[0];
                }
            ],
            //'date_publish',
            [
                'label' => Yii::t('app','Datum kraja'),
                'value' => function ($data) {
                    $temp1 = $data->date_end;
                    return  Yii::$app->formatter->asDate($temp1, 'd. M. Y.'); 
                }
            ],
            //'created_at',
            //'creator',
            //'status',
            //'upitnik_for',
            'is_required:boolean',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update}',
            // 'visible' => Yii::$app->user->can('admin'),
            'buttons' => [

                'view' => function ($url, $data) {

                    $url2 = \Yii::$app->urlManager->createUrl(['upitnik/view', 'id' => $data->id]);
                    return Html::a('<span class="material-icons">visibility</span>', $url2, [
                        'title' => Yii::t('app', 'Pogledaj upitnik'),
                        'class' => 'text-right block'
                    ]);
                },

                'update' => function ($url, $data) {

                    $url2 = \Yii::$app->urlManager->createUrl(['upitnik/update', 'id' => $data->id]);
                    return Html::a('<span class="material-icons">edit</span>', $url2, [
                        'title' => Yii::t('app', 'Uredi upitnik'),
                        'class' => 'text-left block',
                        'target' => '_blank',
                    ]);
                },
             

            ],
        ],
        ],
    ]); ?>


</div>
