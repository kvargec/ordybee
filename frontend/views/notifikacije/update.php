<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Notifikacije */

$this->title = Yii::t('app', 'Update Notifikacije: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notifikacijes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update ');
?>
<div class="notifikacije-update site-about">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
