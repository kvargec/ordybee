<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Notifikacije */

$this->title = Yii::t('app', 'Create Notifikacije');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notifikacijes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifikacije-create site-about">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
