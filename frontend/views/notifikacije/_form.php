<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Notifikacije */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notifikacije-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user')->textInput() ?>

    <?= $form->field($model, 'akcija')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'akcija_id')->textInput() ?>

    <?= $form->field($model, 'radnja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'read_at')->textInput() ?>

    <?= $form->field($model, 'sadrzaj')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
