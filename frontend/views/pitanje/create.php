<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pitanje */

$this->title = Yii::t('app', 'Create Pitanje');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pitanjes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pitanje-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
