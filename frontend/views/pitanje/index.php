<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\PitanjeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pitanjes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pitanje-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pitanje'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'pitanje',
            'opis:ntext',
            'upitnik',
            'is_required:boolean',
            //'vrsta_pitanja',
            //'odgovori',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
