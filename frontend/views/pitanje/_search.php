<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\PitanjeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pitanje-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'pitanje') ?>

    <?= $form->field($model, 'opis') ?>

    <?= $form->field($model, 'upitnik') ?>

    <?= $form->field($model, 'is_required')->checkbox() ?>

    <?php // echo $form->field($model, 'vrsta_pitanja') ?>

    <?php // echo $form->field($model, 'odgovori') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
