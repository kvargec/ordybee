<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Postavke;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" href="<?php echo Yii::getAlias('@web') . '/'; ?>img/favicon.png" type="image/png" sizes="16x16">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
    <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap back">

        <?php

        NavBar::begin([
            'brandLabel' => Html::img('@web/light.svg', ['alt' => Yii::$app->name]),
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-primary navbar-fixed-top',
            ],
        ]);
        $menuItems = [
            ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
            ['label' => Yii::t('app', 'O usluzi'), 'url' => ['/site/about']],
            ['label' => Yii::t('app', 'Kontakt'), 'url' => ['/site/contact']],

        ];
        if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => Yii::t('app', 'Registracija'), 'url' => ['/site/signup']];
            $menuItems[] = ['label' => Yii::t('app', 'Prijava'), 'url' => ['/site/login']];
        } else {
            $notifikacije = isset($this->params['notifikacije']) ? $this->params['notifikacije'] : array();
            $eupisi = Postavke::find()->where(['postavka' => 'eupisi'])->one();
            if ($eupisi->vrijednost == 'false') {
                $menuItems[] = ['label' => Yii::t('app', 'Moji zahtjevi'), 'url' => ['/zahtjev/index']];
            };
            // $menuItems[] = ['label' => Yii::t('app', 'Moji zahtjevi'), 'url' => ['/zahtjev/index']];
            //   $menuItems[]=['label' => Yii::t('app','Izrada zahtjeva'), 'url' => ['/zahtjev/create']];
            $notifikacijeLista = [];
            foreach ($notifikacije as $notifikacija) {
                $notifikacijeLista[] = ['label' => $notifikacija->sadrzaj, 'url' => ['/notifikacije/view', 'id' => $notifikacija->id]];
            }
            $menuItems[] = ['label' => '<i class="glyphicon glyphicon-calendar"></i>', 'url' => ['/calendar/index']];
            $menuItems[] = ['label' => '<i class="glyphicon glyphicon-bell"></i> <span class="count-notifications">' . count($notifikacije) . '</span>', 'items' => $notifikacijeLista];
            $menuItems[] = '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    '<i class="glyphicon glyphicon-log-out"></i>' . Yii::t('app', ' {user}', ['user' => Yii::$app->user->identity->username]),
                    ['class' => 'btn btn-primary logout']
                )
                . Html::endForm()
                . '</li>';
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
            'encodeLabels' => false,
        ]);
        NavBar::end();
        ?>

        <div class="container main-panel">

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>

            <?= $content ?>
        </div>
    </div>
    <svg version="1.1" id="footer-divider" class="secondary" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1440 126" xml:space="preserve" preserveAspectRatio="none slice">
        <path class="st0" d="M685.6,38.8C418.7-11.1,170.2,9.9,0,30v96h1440V30C1252.7,52.2,1010,99.4,685.6,38.8z"></path>
    </svg>
    <footer class="footer">
        <div class="container">
            <div class="row ">
                <div class="col-lg-4 ">
                    <p><a class="success" href="<?php echo \yii\helpers\Url::to(['site/about']) ?>"><?= Yii::t('app', 'Izjava o privatnosti') ?></a></p>
                </div>

            </div>
            <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

            <p class="pull-right">by <a href="https://dikobraz.hr/" target="_blank">Dikobraz</a></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script>
        $(function() {
            $('input.dates').daterangepicker({
                opens: 'center',
                locale: {
                    format: 'DD.MM.YYYY.',
                    applyLabel: '  <span  class="material-icons">check</span>  ',
                    cancelLabel: '<span  class="material-icons danger">close</span>',
                },
                cancelButtonClasses: 'cancelBtn btn btn-sm btn-danger'
            }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });
    </script>
    <script>
        jQuery(document).ready(function() {
            $('input[name="Dijete[prebivaliste_jednako_boraviste]"]').click(function() {
                if ($(this).attr("value") == "D") {
                    $("#prebivaliste").hide('slow');
                    console.log('hide');
                }
                if ($(this).attr("value") == "N") {
                    $("#prebivaliste").show('slow');
                    console.log('show');
                }
            });
            $('input[name="Roditelj[0][prebivaliste_jednako_boraviste]"]').click(function() {
                if ($(this).attr("value") == "D") {
                    $("#prebivaliste-majka").hide('slow');
                    console.log('hide');
                }
                if ($(this).attr("value") == "N") {
                    $("#prebivaliste-majka").show('slow');
                    console.log('show');
                }
            });
            $('input[name="Roditelj[1][prebivaliste_jednako_boraviste]"]').click(function() {
                if ($(this).attr("value") == "D") {
                    $("#prebivaliste-otac").hide('slow');
                    console.log('hide');
                }
                if ($(this).attr("value") == "N") {
                    $("#prebivaliste-otac").show('slow');
                    console.log('show');
                }
            });
        });
    </script>
    <script>
        $(".alert").animate({
            opacity: 1
        }, 3000).fadeOut();
    </script>
    <script>
        $("select option[value*='0']").prop('disabled', true);
        $("select option[value*='dodatno0']").prop('disabled', true);
    </script>
</body>

</html>
<?php $this->endPage() ?>