<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Poruke */

$this->title = Yii::t('app', 'Create Poruke');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Porukes'), 'url' => ['/mailbox']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poruke-create">
    <div class="notifikacije-index site-about">
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="row">
            <div class="col-md-4 col-lg-2">
                <div class="text-center">
                    <?= Html::a('<span class="material-icons btn btn-default btn-round">add</span>', ['poruke/create'], ['class' => 'new-message']) ?></div>

                <div id="accordion" role="tablist">
                    <div class="card-collapse mt-0">
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="mb-0 mt-0">
                                <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                    <?php echo Yii::t('app', 'Folders'); ?>
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </a>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                            <div class="card-body">
                                <ul>
                                    <?php
                                    foreach ($folders as $folder) {

                                    ?>
                                        <li>
                                            <p class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/mailbox/index', 'folder' => $folder->folder_name]) ?>" class="">
                                                    <span class="material-icons"><?= $folder->icon ?></span><?= ' ' . Yii::t('app', $folder->folder_name) ?> <?php // $folder->folder_name == $msgFolder->folder_name ? $count > 0 ? '(' . $count . ')' : '':'' 
                                                                                                                                                                ?>
                                                </a></p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
            <div class="col-md-8 col-lg-10">
                <style>
                    .field-grupacija,
                    .field-poruke-sender {
                        margin-top: 0
                    }

                    .form-group {
                        margin-top: 15px;
                    }
                </style>
                <?= $this->render('_form', [
                    'model' => $model,
                    'reply' => $reply,
                    'reply_all' => $reply_all,
                    'replyId' => $replyId
                ]) ?>

            </div>
        </div>
    </div>
</div>