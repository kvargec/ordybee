<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\PorukeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Porukes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poruke-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Poruke'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'subject',
            'message:ntext',
            'sender',
            //'recipients',
            //'created_at',
            //'updated_at',
            //'send_at',
            //'status',
            //'attachments',
            //'priority',
            //'vrsta_poruke',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
