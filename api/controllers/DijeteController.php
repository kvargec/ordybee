<?php

namespace api\controllers;

use common\models\Dijete;
use common\models\Prisutnost;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use \Da\QrCode\Action\QrCodeAction;

use yii\rest\ActiveController;

class DijeteController extends ActiveController
{

    public $modelClass = 'common\models\Dijete';

    public function behaviors()
    {
        return array_merge([
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ],
            'authenticator' => [
                'class' => HttpBearerAuth::class,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'zabiljeziprisutnost' => ['post', 'get']
                ]
            ]
        ], parent::behaviors());
    }
    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        $actions['index']['zabiljeziPrisutnost'] = [$this, 'zabiljeziPrisutnost'];

        return $actions
            //'qr' => [
            //    'class' => QrCodeAction::className(),
            //    'text' => 'Dijete',
            //    'param' => 'id',
            //    'method' => 'post',
            //    'component' => 'qr' // if configured in our app as `qr` 
            //],
            //'zabiljeziprisutnost' => [
            //    'class' => QrCodeAction::className(),
            //    'text' => 'Dijete prisutnost update',
            //    'param' => 'id',
            //    'method' => 'get',
            //    'component' => 'qr'
            //]
        ;
    }

    public function prepareDataProvider()
    {
        $query = Dijete::find();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 0]
        ]);

        return $dataProvider;
    }
    public function actionPopis(){

    }
    public function actionZabiljeziPrisutnost($id){

        //$dijete = $_REQUEST['id'];
        $dijete = $id;

        $prisutnost = new Prisutnost();
        $prisutnost->entitet = 'dijete';
        $prisutnost->datum = date('Y-m-d', time());
        $prisutnost->entitet_id = $dijete;
        $prisutnost->status = 1;
        $prisutnost->save();

        //if ($prisutnost->save()) {
        //
        //    $this->setHeader(200);
        //    echo json_encode(array('status'=>1),JSON_PRETTY_PRINT);
        //  
        //    } 
        //else {
        //    $this->setHeader(400);
        //    echo json_encode(array('status'=>0,'error_code'=>400),JSON_PRETTY_PRINT);
        //}
    }
}