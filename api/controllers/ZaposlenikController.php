<?php

namespace api\controllers;

use common\models\Prisutnost;
use common\models\Zaposlenik;
use yii\helpers\Json;

use yii\rest\ActiveController;

class ZaposlenikController extends ActiveController
{

    public $modelClass = 'common\models\Zaposlenik';

    public function behaviors()
    {
        return array_merge([
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ],
            'authenticator' => [
                'class' => HttpBearerAuth::class,
            ]
        ], parent::behaviors());
    }
    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;

    }

    public function prepareDataProvider()
    {
        $query = Zaposlenik::find();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 0]
        ]);

        return $dataProvider;
    }
    public function actionPocetak($vrijeme, $zaposlenik){
        $prisutnost = new Prisutnost();
        $prisutnost->entitet = 'zaposlenik';
        $prisutnost->pocetak = strtotime($vrijeme);
        $prisutnost->datum = date('Y-m-d', time());
        $prisutnost->entitet_id = $zaposlenik;
        $prisutnost->status = 1;
        $prisutnost->save();
        //success

    }
    public function actionKraj($vrijeme, $zaposlenik){
        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik, 'datum' => date('Y-m-d', time())]);
        if (!empty($prisutnost)) {
            $prisutnost->kraj = strtotime($vrijeme);
            $prisutnost->trajanje = ($prisutnost->kraj - $prisutnost->pocetak) / (60 * 60);
            $prisutnost->save();
            //success
        }
        //error
    }


}