<?php

namespace api\controllers;

use common\models\Jelovnik;
use common\models\Obrok;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\Json;
use Mpdf;
use yii\rest\ActiveController;

class JelovnikController extends ActiveController
{

    public $modelClass = 'common\models\Jelovnik';

    public function behaviors()
    {
        return array_merge([
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ],
            'authenticator' => [
                'class' => HttpBearerAuth::class,
            ]
        ], parent::behaviors());
    }
    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;

    }

    public function prepareDataProvider()
    {
        $query = Jelovnik::find();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 0]
        ]);

        return $dataProvider;
    }
    public function actionHtml(){
        $datumi = $this->getDatum(0);

        $dorucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $rucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $uzina = new DynamicModel(['pon','uto','sri','cet','pet']);
        $model= new DynamicModel(['dorucak','rucak','uzina']);
        $model->rucak = $rucak;

        $model->uzina=$uzina;
        $model->dorucak=$dorucak;
        foreach ($model as $obrok=>$item){
            $tmpObrok = Obrok::find()->where(['obrok'=>$obrok])->one();
            foreach ($item as $dan=>$jelo){
                $jeloZapis = Jelovnik::find()->where(['datum'=>$datumi[$dan],'obrok'=>$tmpObrok->id])->orderBy('redoslijed ASC')->all();

                if(!empty($jeloZapis)) {
                    $tmp = '';
                    foreach ($jeloZapis as $item) {
                        $tmp .= strtolower($item->jelo0->naziv) . ', ';
                    }
                    $model->$obrok->$dan = ucfirst(substr($tmp,0,-2));
                    $out[$dan][$obrok]= ucfirst(substr($tmp,0,-2));
                }
            }
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $out;
     /*   $htmlContent =  $this->renderPartial('jelovnikpdf', ['model' =>$model,'datum' =>$datumi,'dodatak'=>null]);
        return $this::printPDF($htmlContent,
            "jelovnik.pdf",
            "jelovnik".date('j.n.Y',strtotime( $datumi['pon']))."-".date('j.n.Y',strtotime( $datumi['pet'])).".pdf");*/

    }
    private function getDatum($tjedan){
        $datum['pon'] = date('Y-m-d', strtotime('monday this week'));
        $datum['uto'] = date('Y-m-d', strtotime('tuesday this week'));
        $datum['sri'] = date('Y-m-d', strtotime('wednesday this week'));
        $datum['cet'] = date('Y-m-d', strtotime('thursday this week'));
        $datum['pet'] = date('Y-m-d', strtotime('friday this week'));
        if ($tjedan == '0'){
            return $datum;
        }else{
            $datum['pon'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['pon'])));
            $datum['uto'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['uto'])));
            $datum['sri'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['sri'])));
            $datum['cet'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['cet'])));
            $datum['pet'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['pet'])));
        }
        return $datum;

    }
    private function printPDF($sadrzaj,$pdfPath,$nazivDokumenta,$orient='P'){

        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format'=>'A4', 12, '', 'margin_left'=>15, 'margin_right'=>15, 'margin_top'=>16, 'margin_bottom'=>16, 'mgh'=>1, 'mgf'=>1, 'orientation'=>$orient,'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($tempDir);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath,\Mpdf\Output\Destination::DOWNLOAD );
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }


}