<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'api\controllers',
    'modules' => [],
    'components' => [
        'request' => [
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => 'zaposlenik',
                    'extraPatterns' => [
                        'GET /zaposlenik/popis' => 'zaposlenik/popis', // 'xxxxx' refers to 'actionXxxxx'

                    ],],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'dijete','pluralize'=>false],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'jelovnik','pluralize'=>false],
                //['class' => 'yii\rest\UrlRule', 'controller' => 'prisutnost','pluralize'=>false],

           ],
        ],
        'qr' => [
            'class' => '\Da\QrCode\Component\QrCodeComponent',
            'label' => '',
            'size' => 210
            // ... you can configure more properties of the component here
        ]
    ],
    'params' => $params,
];
