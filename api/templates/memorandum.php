<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>
<html>
<head>
    <style>
        body { color: #000000; }
        td { vertical-align: top;}
        .tablePDF { width:100%; background-color: #ffffff; font-size:11px; border-spacing: 0px;}
        .tablePDF td { text-align:left; background-color: #ffffff; padding:2px 2px 2px 2px; margin: 0;}
        .tablePDf tr.crta td { border-bottom: 1px solid #003399;}
        .tablePDF th { background-color: #ffffff; padding:2px 2px 2px 2px; border:1px solid #003399;}
        .tablePDF td.sredina { text-align:center;}
        .tablePDF tr.desno { text-align:right;}
        .desno { text-align:right;}
        .kv-page-summary-container td{ text-align:right; font-weight:bold;}
        body{}
        img{ padding:5px; border:1px solid #000;}
        .obican{ font-family:Helvetica; font-size:16px;}
        .veliki{ font-family:Helvetica; font-size:24px; text-transform: uppercase;}
    </style>
</head>
<body>
<?php if ($naslov !== 'Rješenje'):?>
<table class="tablePDF obican">
    <tr>
        <td style = "width: 300px;">
            OIB: 44444146578<br/>
            Dječji vrtić Vrbovec<br/>
            7. svibnja 12a<br/>
            10340 Vrbovec<br/>
        </td>
        <td style="text-align: right">
            Vrbovec,<br />
            <?php echo date('d.m.Y.',time())?>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="width: 100%; padding-top: 50px;">
                <h1><?php echo $naslov?></h1>
        </td>
    </tr>

</table>
<?php endif; ?>
<div class="shop-create">
        <div class="row">
            <div class="col s12 m12">
                <?php echo $content ?>
            </div>
        </div>
</div>
</body>
</html>