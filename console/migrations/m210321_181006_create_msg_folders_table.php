<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%msg_folders}}`.
 */
class m210321_181006_create_msg_folders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%msg_folders}}', [
            'id' => $this->primaryKey(),
            'folder_name' => $this->string(255),
            'user' => $this->integer(),
            'icon' => $this->string(50),
            'redoslijed' => $this->tinyInteger(),
        ]);
        $this->batchInsert('msg_folders', ['folder_name','user','icon','redoslijed'],[
            ['inbox',0,'inbox',1],
            ['drafts',0,'drafts',2],
            ['templates',0,'drafts',3],
            ['sent',0,'outbox',4],
            ['trash',0,'delete',5]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%msg_folders}}');
    }
}
