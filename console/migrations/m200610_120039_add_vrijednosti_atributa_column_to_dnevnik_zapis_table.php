<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%dnevnik_zapis}}`.
 */
class m200610_120039_add_vrijednosti_atributa_column_to_dnevnik_zapis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%dnevnik_zapis}}', 'vrijednosti_atributa', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%dnevnik_zapis}}', 'vrijednosti_atributa');
    }
}
