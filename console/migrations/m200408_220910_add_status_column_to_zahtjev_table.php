n<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zahtjev}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%status}}`
 */
class m200408_220910_add_status_column_to_zahtjev_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zahtjev}}', 'status', $this->integer());

        // creates index for column `status`
        $this->createIndex(
            '{{%idx-zahtjev-status}}',
            '{{%zahtjev}}',
            'status'
        );

        // add foreign key for table `{{%status}}`
        $this->addForeignKey(
            '{{%fk-zahtjev-status}}',
            '{{%zahtjev}}',
            'status',
            '{{%status}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%status}}`
        $this->dropForeignKey(
            '{{%fk-zahtjev-status}}',
            '{{%zahtjev}}'
        );

        // drops index for column `status`
        $this->dropIndex(
            '{{%idx-zahtjev-status}}',
            '{{%zahtjev}}'
        );

        $this->dropColumn('{{%zahtjev}}', 'status');
    }
}
