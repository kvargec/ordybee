<?php

use yii\db\Migration;

/**
 * Class m220124_110250_insert_new_mstatus
 */
class m220124_110250_insert_new_mstatus extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('mstatus', [
            'status' => 'template',
        ]);
        $this->insert('mstatus', [
            'status' => 'saved',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('mstatus', ['status' => 'template']);
        $this->delete('mstatus', ['status' => 'saved']);
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220124_110250_insert_new_mstatus cannot be reverted.\n";

        return false;
    }
    */
}
