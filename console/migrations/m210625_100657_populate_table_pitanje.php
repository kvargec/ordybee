<?php

use yii\db\Migration;

/**
 * Class m210625_100657_populate_table_pitanje
 */
class m210625_100657_populate_table_pitanje extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //$data = ['Inicijalni razgovor', 2, 1, 2, 'roditelji', false];
        $this->insert('upitnik',['naziv' => 'Inicijalni razgovor', 'ped_godina' => 2, 'creator' => 1, 'status' => 2, 'upitnik_for' => ['roditelji'], 'is_required' => false]);

        $upitnikID = Yii::$app->db->getMasterPdo()->lastInsertId();

        $data2 = [
            ['Kako biste vi opisali svoje dijete?', null, $upitnikID, false, 4, null],
            ['Tko sve živi u domaćinstvu s djetetom:', null, $upitnikID, false, 3, null],
            ['Ukoliko dijete ne živi s oba roditelja molimo navedite razlog:', null, $upitnikID, false, 4, null],
            ['Preboljene i češće bolesti djeteta', null, $upitnikID, false, 3, null],
            ['Alergije na ljek', null, $upitnikID, false, 3, null],
            ['Alergije na hranu', null, $upitnikID, false, 3, null],
            ['Ostale alergije', null, $upitnikID, false, 3, null],
            ['Kako se alergija manifestira', null, $upitnikID, false, 4, null],
            ['Hospitalizacije', null, $upitnikID, false, 1, '{"0": "da", "1": "ne"}'],
            ['Radi čega? U kojoj dobi? Koliko dugo? Gdje? Reakcija?', null, $upitnikID, false, 4, null],
            ['Prati li se dijete kod nekog specijaliste (npr. neuropedijatra, fizijatra,...) ili je uključeno u neke tretmane (npr. rehabilitatora, logopeda i sl.)', null, $upitnikID, false, 1, '{"0": "da", "1": "ne"}'],
            ['Ako da, kod koga:', null, $upitnikID, false, 3, null],
            ['Pelene treba:', null, $upitnikID, false, 2, '{"0": "stalno", "1": "za dnevno spavanje", "2": "za noćno spavanje", "3": "za noćno spavanje", "4": "ne koristi ih"}'],
            ['Samostalnost u obavljanju nužde:', null, $upitnikID, false, 2, '{"0": "potpuno samostalno", "1": "treba ga podsjetiti", "2": "traži pomoć pri obavljanju nužde", "3": "ima pelenu, prijavljuje kad obavi nuždu", "4": "ima pelenu, ne smeta mu kad je puna"}'],
            ['Odabir hrane:', null, $upitnikID, false, 2, '{"0": "jede što i ostali ukućani ", "1": "ponekad mu se sprema posebna hrana "}'],
            ['Ukoliko se priprema posebna hrana, razlog:', null, $upitnikID, false, 3, null],
            ['Samostalnost u hranjenju:', null, $upitnikID, false, 2, '{"0": "jede samo ", "1": "traži pomoć odraslog", "2": "u početku jede samo, do kraja obroka uz pomoć odraslog"}'],
            ['Još neka važna napomena vezana uz prehranu vašeg djeteta (slab apetit, prejedanje, odbijanje neke vrste hrane, alergije na hranu, miksanje hrane i sl.)', null, $upitnikID, false, 4, null],
            ['Ritam spavanja kod kuće - noćni san od do:', null, $upitnikID, false, 3, null],
            ['Dnevni odmor od do:', null, $upitnikID, false, 3, null],
            ['Želite li da vaše dijete spava u vrtiću?', null, $upitnikID, false, 1, '{"0": "da", "1": "ne"}'],
            ['Kako se dijete uspavljuje (zaspi samo, uz roditelja, uz dudu, bocu, tješilicu...):', null, $upitnikID, false, 3, null],
            ['Samostalnost u odijevanju – oblačenje:', null, $upitnikID, false, 1, '{"0": "da", "2": "djelomično", "2": "ne"}'],
            ['Samostalnost u odijevanju – Svlačenje:', null, $upitnikID, false, 1, '{"0": "da", "2": "djelomično", "2": "ne"}'],
            ['Samostalnost u odijevanju – Obuvanje:', null, $upitnikID, false, 1, '{"0": "da", "2": "djelomično", "2": "ne"}'],
            ['Samostalnost u odijevanju – Izuvanje:', null, $upitnikID, false, 2, '{"0": "da", "2": "djelomično", "2": "ne"}'],
            ['Kakva je motorika vašeg djeteta (molimo označite kvadratiće ispred svega što se odnosi na vaše dijete):', null, $upitnikID, false, 2, '{"0": "dijete je spretno u kretanju;", "1": "nespretno je (češće pada, spotiče se, sudara s predmetima);", "2": "sklono je povredama;", "3": "pojačano je motorno aktivno (živahno);", "4": "hoda na prstima;", "5": "ne voli se kretati;"}'],
            ['Pokazuje li dijete preosjetljivost na podražaje iz okoline (molimo označite kvadratiće ispred svega što se odnosi na vaše dijete):', null, $upitnikID, false, 2, '{"0": "zvuk;", "1": "dodir;", "2": "miris;", "3": "svjetlosne promjene;", "4": "okus;", "5": "vrtnju, ljuljanje;", "6": "drugo"}'],
            ['Ukoliko je dijete osjetljivo na drugi podražaj, koji?', null, $upitnikID, false, 3, null],
            ['Tko sada čuva dijete?', null, $upitnikID, false, 3, null],
            ['Kako reagira na odvajanje od roditelja?', null, $upitnikID, false, 4, null],
            ['Kako prihvaća pravila? Kako reagira na zabrane?', null, $upitnikID, false, 4, null],
            ['Koje igre/igračke/aktivnosti su vašem djetetu najdraže? Koliko se dugo zadrži u nekoj igri?', null, $upitnikID, false, 4, null],
            ['Voli li se više igrati samo, s drugom djecom ili s odraslima?', null, $upitnikID, false, 4, null],
            ['Jesu li kod djeteta prisutna neka od sljedećih ponašanja (molimo označite kvadratiće ispred svega što se odnosi na vaše dijete):', null, $upitnikID, false, 2, '{"0": "škripanje zubima", "1": "guranje ruke u usta", "2": "masturbacija ", "3": "grickanje noktiju", "4": "kopanje nosa", "5": "ritmično ljuljanje", "6": "čupanje kose", "7": "sisanje prsta", "8": "tikovi"}'],
            ['Jesu li kod djeteta prisutna neka od sljedećih ponašanja (molimo označite kvadratiće ispred svega što se odnosi na vaše dijete):', null, $upitnikID, false, 2, '{"0": "pretjerana tvrdoglavost", "1": "pretjerana plačljivost", "2": "strah od odvajanja", "3": "potištenost", "4": "uvredljivost", "5": "anksioznost", "6": "povučenost", "7": "ljubomora", "8": "agresivnost", "9": "sramežljivost", "10": "strah"}'],
            ['Ukoliko je kod djeteta prisutan strah, od čega:', null, $upitnikID, false, 3, null],
            ['Ukoliko vaše dijete još ne govori, molimo označite kvadratiće ispred svega što se odnosi na vaše dijete:', null, $upitnikID, false, 2, '{"0": "odaziva se na ime;", "1": "donosi vam i pokazuje njemu zanimljive stvari;", "2": "gleda vas u oči;", "3": "pogledava u vas i provjerava kako vi reagirate;", "4": "smije se kad se vi smijete;", "5": "imitira vaše pokrete, grimase, pljeskanje, maše pa-pa;", "6": "koristi gestu pokazivanja kažiprstom"}'],
            ['Ukoliko vaše dijete komunicira riječima ili rečenicama, molimo označite kvadratiće ispred onoga što se odnosi na vaše dijete:', null, $upitnikID, false, 2, '{"0": "bez teškoća u razvoju govora (komunicira rečenicama, izgovara sve glasove);", "1": "izražava se riječima ili dvočlanim rečenicama (npr. beba nana);", "2": "nepravilan izgovor;", "3": " promjene u ritmu i tempu govora ( ponavljanja, ubrzavanja, zastajkivanja u  govoru);"}'],
            ['Ukoliko dijete neke glasove ne izgovara pravilno, koje:', null, $upitnikID, false, 3, null],
            ['Ostale napomene roditelja:', null, $upitnikID, false, 4, null],
        ];
        
        $this->batchInsert('pitanje',['pitanje', 'opis', 'upitnik', 'is_required', 'vrsta_pitanja', 'odgovori'],$data2);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210625_100657_populate_table_pitanje cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210625_100657_populate_table_pitanje cannot be reverted.\n";

        return false;
    }
    */
}
