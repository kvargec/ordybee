<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prijevoz}}`.
 */
class m221215_143316_create_prijevoz_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%prijevoz}}', [
            'id' => $this->primaryKey(),
            'vrsta' => $this->string()->notNull(),
            'cijena' => $this->decimal(2),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%prijevoz}}');
    }
}
