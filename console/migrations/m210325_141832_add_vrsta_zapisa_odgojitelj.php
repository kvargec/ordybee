<?php

use yii\db\Migration;

/**
 * Class m210325_141832_add_vrsta_zapisa_odgojitelj
 */
class m210325_141832_add_vrsta_zapisa_odgojitelj extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('vrsta_zapis',['naziv', 'grupa'], [
            ['zapis', 'odgajatelj'],
            ['izvjestaj o razvoju djeteta','odgajatelj'],
            ['izvjestaj o aktivnostima','odgajatelj'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210325_141832_add_vrsta_zapisa_odgojitelj cannot be reverted.\n";

        return false;
    }
    */
}
