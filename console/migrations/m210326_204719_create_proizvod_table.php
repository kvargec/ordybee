<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%proizvod}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%grupa_proizvod}}`
 * - `{{%mjed}}`
 */
class m210326_204719_create_proizvod_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%proizvod}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(250),
            'sifra' => $this->string(150),
            'grupa' => $this->integer(),
            'opis' => $this->text(),
            'mjed' => $this->integer(),
        ]);

        // creates index for column `grupa`
        $this->createIndex(
            '{{%idx-proizvod-grupa}}',
            '{{%proizvod}}',
            'grupa'
        );

        // add foreign key for table `{{%grupa_proizvod}}`
        $this->addForeignKey(
            '{{%fk-proizvod-grupa}}',
            '{{%proizvod}}',
            'grupa',
            '{{%grupa_proizvod}}',
            'id',
            'CASCADE'
        );

        // creates index for column `mjed`
        $this->createIndex(
            '{{%idx-proizvod-mjed}}',
            '{{%proizvod}}',
            'mjed'
        );

        // add foreign key for table `{{%mjed}}`
        $this->addForeignKey(
            '{{%fk-proizvod-mjed}}',
            '{{%proizvod}}',
            'mjed',
            '{{%mjed}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%grupa_proizvod}}`
        $this->dropForeignKey(
            '{{%fk-proizvod-grupa}}',
            '{{%proizvod}}'
        );

        // drops index for column `grupa`
        $this->dropIndex(
            '{{%idx-proizvod-grupa}}',
            '{{%proizvod}}'
        );

        // drops foreign key for table `{{%mjed}}`
        $this->dropForeignKey(
            '{{%fk-proizvod-mjed}}',
            '{{%proizvod}}'
        );

        // drops index for column `mjed`
        $this->dropIndex(
            '{{%idx-proizvod-mjed}}',
            '{{%proizvod}}'
        );

        $this->dropTable('{{%proizvod}}');
    }
}
