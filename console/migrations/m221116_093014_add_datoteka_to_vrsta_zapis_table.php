<?php

use yii\db\Migration;
use common\models\VrstaZapis;

/**
 * Class m221116_093014_add_datoteka_to_vrsta_zapis_table
 */
class m221116_093014_add_datoteka_to_vrsta_zapis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $data = VrstaZapis::find()->where(['naziv' => 'datoteka'])->asArray()->all();

        if (count($data)==0) {
            $this->insert('vrsta_zapis', ['naziv' => 'datoteka', 'grupa' => 'zdravstvo', 'title'=>'Unesi datoteku', 'redoslijed' => 100]);
            $this->insert('vrsta_zapis', ['naziv' => 'datoteka', 'grupa' => 'odgajatelj', 'title'=>'Unesi datoteku', 'redoslijed' => 100]);
        }
        if (count($data)==1) {
            if ($data[0]['grupa'] == 'zdravstvo') {
                $this->insert('vrsta_zapis', ['naziv' => 'datoteka', 'grupa' => 'odgajatelj', 'title'=>'Unesi datoteku', 'redoslijed' => 100]);
            } 
            if ($data[0]['grupa'] == 'odgajatelj') {
                $this->insert('vrsta_zapis', ['naziv' => 'datoteka', 'grupa' => 'zdravstvo', 'title'=>'Unesi datoteku', 'redoslijed' => 100]);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m221116_093014_add_datoteka_to_vrsta_zapis_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221116_093014_add_datoteka_to_vrsta_zapis_table cannot be reverted.\n";

        return false;
    }
    */
}
