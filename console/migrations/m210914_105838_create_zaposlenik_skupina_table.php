<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zaposlenik_skupina}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%zaposlenik}}`
 * - `{{%skupina}}`
 * - `{{%pedagoska_godina}}`
 */
class m210914_105838_create_zaposlenik_skupina_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zaposlenik_skupina}}', [
            'id' => $this->primaryKey(),
            'zaposlenik' => $this->integer(),
            'skupina' => $this->integer(),
            'ped_godina' => $this->integer(),
            'aktivno' => $this->boolean(),
        ]);

        // creates index for column `zaposlenik`
        $this->createIndex(
            '{{%idx-zaposlenik_skupina-zaposlenik}}',
            '{{%zaposlenik_skupina}}',
            'zaposlenik'
        );

        // add foreign key for table `{{%zaposlenik}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik_skupina-zaposlenik}}',
            '{{%zaposlenik_skupina}}',
            'zaposlenik',
            '{{%zaposlenik}}',
            'id',
            'CASCADE'
        );

        // creates index for column `skupina`
        $this->createIndex(
            '{{%idx-zaposlenik_skupina-skupina}}',
            '{{%zaposlenik_skupina}}',
            'skupina'
        );

        // add foreign key for table `{{%skupina}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik_skupina-skupina}}',
            '{{%zaposlenik_skupina}}',
            'skupina',
            '{{%skupina}}',
            'id',
            'CASCADE'
        );

        // creates index for column `ped_godina`
        $this->createIndex(
            '{{%idx-zaposlenik_skupina-ped_godina}}',
            '{{%zaposlenik_skupina}}',
            'ped_godina'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik_skupina-ped_godina}}',
            '{{%zaposlenik_skupina}}',
            'ped_godina',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%zaposlenik}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik_skupina-zaposlenik}}',
            '{{%zaposlenik_skupina}}'
        );

        // drops index for column `zaposlenik`
        $this->dropIndex(
            '{{%idx-zaposlenik_skupina-zaposlenik}}',
            '{{%zaposlenik_skupina}}'
        );

        // drops foreign key for table `{{%skupina}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik_skupina-skupina}}',
            '{{%zaposlenik_skupina}}'
        );

        // drops index for column `skupina`
        $this->dropIndex(
            '{{%idx-zaposlenik_skupina-skupina}}',
            '{{%zaposlenik_skupina}}'
        );

        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik_skupina-ped_godina}}',
            '{{%zaposlenik_skupina}}'
        );

        // drops index for column `ped_godina`
        $this->dropIndex(
            '{{%idx-zaposlenik_skupina-ped_godina}}',
            '{{%zaposlenik_skupina}}'
        );

        $this->dropTable('{{%zaposlenik_skupina}}');
    }
}
