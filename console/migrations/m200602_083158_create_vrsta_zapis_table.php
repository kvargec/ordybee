<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrsta_zapis}}`.
 */
class m200602_083158_create_vrsta_zapis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrsta_zapis}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(150),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrsta_zapis}}');
    }
}
