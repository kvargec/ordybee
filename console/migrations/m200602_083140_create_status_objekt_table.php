<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%status_objekt}}`.
 */
class m200602_083140_create_status_objekt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%status_objekt}}', [
            'id' => $this->primaryKey(),
            'status'=>$this->string(10),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%status_objekt}}');
    }
}
