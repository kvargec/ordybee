<?php

use yii\db\Migration;

/**
 * Class m210930_094231_insert_into_vrsta_zapis_table
 */
class m210930_094231_insert_into_vrsta_zapis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('vrsta_zapis',['naziv'=>'grupa zapis','grupa'=>'odgajatelj', 'title' => 'Zapis o radu grupe']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('vrsta_zapis',['naziv'=>'grupa zapis']);

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210930_094231_insert_into_vrsta_zapis_table cannot be reverted.\n";

        return false;
    }
    */
}
