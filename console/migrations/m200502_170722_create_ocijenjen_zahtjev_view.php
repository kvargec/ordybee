<?php

use yii\db\Migration;

/**
 * Class m200502_170722_create_ocijenjen_zahtjev_view
 */
class m200502_170722_create_ocijenjen_zahtjev_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
        CREATE VIEW ocijenjen_zahtjev AS
        SELECT zahtjev.*,
            sum(kriterij.bodovi) as bodovi,
            sum(kriterij.izravan_upis::int)>0 as izravan_upis
        FROM zahtjev, zahtjev_kriterij, kriterij
        WHERE zahtjev.id = zahtjev_kriterij.zahtjev_id   and
            zahtjev_kriterij.kriterij_id = kriterij.id and
            zahtjev_kriterij.zadovoljen = true
        GROUP BY zahtjev.id;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('DROP VIEW ocijenjen_zahtjev;');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200502_170722_create_ocijenjen_zahtjev_view cannot be reverted.\n";

        return false;
    }
    */
}
