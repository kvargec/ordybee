<?php

use yii\db\Migration;

/**
 * Class m201008_064752_dodaj_zaposlenike
 */
class m201008_064752_dodaj_zaposlenike extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sprema=\common\models\Sprema::find()->orderBy('id ASC')->one();
        $id=$sprema->id;
        $str = file_get_contents(__DIR__ . '/data/zaposlenici.txt');
        $lines = explode(PHP_EOL, $str);
        $db = Yii::$app->db;
        $dataSprema=[

        ];
        foreach ($lines as $line) {
            if (!$line) {
                continue;
            }

            $line = str_replace('\N', 'null', $line);

             $nekaj= explode(",", $line);
             $naziv=explode(" ",$nekaj[1]);
             $dataSprema[]=[$naziv[1],$naziv[0],$id];


        }
        $this->batchInsert('zaposlenik',['ime','prezime','sprema'],$dataSprema);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201008_064752_dodaj_zaposlenike cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201008_064752_dodaj_zaposlenike cannot be reverted.\n";

        return false;
    }
    */
}
