<?php

use yii\db\Migration;

/**
 * Class m211004_052236_ver101
 */
class m211004_052236_ver101 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->update('postavke',['postavka'=>'verzijaOrdyBee','vrijednost'=>"1.0.1","dodatno"=>""],['postavka'=>'verzijaOrdyBee']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211004_052236_ver101 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211004_052236_ver101 cannot be reverted.\n";

        return false;
    }
    */
}
