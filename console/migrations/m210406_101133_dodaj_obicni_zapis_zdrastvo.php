<?php

use yii\db\Migration;

/**
 * Class m210406_101133_dodaj_obicni_zapis_zdrastvo
 */
class m210406_101133_dodaj_obicni_zapis_zdrastvo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('vrsta_zapis',['id','naziv', 'grupa'], [
            [1,'bilješka', 'zdravstvo'],

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210406_101133_dodaj_obicni_zapis_zdrastvo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210406_101133_dodaj_obicni_zapis_zdrastvo cannot be reverted.\n";

        return false;
    }
    */
}
