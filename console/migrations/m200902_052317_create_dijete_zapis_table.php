<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dijete_zapis}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%dijete}}`
 * - `{{%dnevnik_zapis}}`
 */
class m200902_052317_create_dijete_zapis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dijete_zapis}}', [
            'id' => $this->primaryKey(),
            'dijete' => $this->integer()->notNull(),
            'zapis' => $this->integer(),
            'dodatno' => $this->json(),
        ]);

        // creates index for column `dijete`
        $this->createIndex(
            '{{%idx-dijete_zapis-dijete}}',
            '{{%dijete_zapis}}',
            'dijete'
        );

        // add foreign key for table `{{%dijete}}`
        $this->addForeignKey(
            '{{%fk-dijete_zapis-dijete}}',
            '{{%dijete_zapis}}',
            'dijete',
            '{{%dijete}}',
            'id',
            'CASCADE'
        );

        // creates index for column `zapis`
        $this->createIndex(
            '{{%idx-dijete_zapis-zapis}}',
            '{{%dijete_zapis}}',
            'zapis'
        );

        // add foreign key for table `{{%dnevnik_zapis}}`
        $this->addForeignKey(
            '{{%fk-dijete_zapis-zapis}}',
            '{{%dijete_zapis}}',
            'zapis',
            '{{%dnevnik_zapis}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%dijete}}`
        $this->dropForeignKey(
            '{{%fk-dijete_zapis-dijete}}',
            '{{%dijete_zapis}}'
        );

        // drops index for column `dijete`
        $this->dropIndex(
            '{{%idx-dijete_zapis-dijete}}',
            '{{%dijete_zapis}}'
        );

        // drops foreign key for table `{{%dnevnik_zapis}}`
        $this->dropForeignKey(
            '{{%fk-dijete_zapis-zapis}}',
            '{{%dijete_zapis}}'
        );

        // drops index for column `zapis`
        $this->dropIndex(
            '{{%idx-dijete_zapis-zapis}}',
            '{{%dijete_zapis}}'
        );

        $this->dropTable('{{%dijete_zapis}}');
    }
}
