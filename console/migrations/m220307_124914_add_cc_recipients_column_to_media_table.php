<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%media}}`.
 */
class m220307_124914_add_cc_recipients_column_to_media_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%media}}', 'cc_recipients', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%media}}', 'cc_recipients');
    }
}
