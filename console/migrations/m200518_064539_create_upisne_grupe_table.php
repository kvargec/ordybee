<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%upisne_grupe}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%pedagoska_godina}}`
 */
class m200518_064539_create_upisne_grupe_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%upisne_grupe}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(150),
            'date_poc' => $this->date(),
            'date_kraj' => $this->date(),
            'broj' => $this->integer(),
            'ped_godina' => $this->integer(),
        ]);

        // creates index for column `ped_godina`
        $this->createIndex(
            '{{%idx-upisne_grupe-ped_godina}}',
            '{{%upisne_grupe}}',
            'ped_godina'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-upisne_grupe-ped_godina}}',
            '{{%upisne_grupe}}',
            'ped_godina',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-upisne_grupe-ped_godina}}',
            '{{%upisne_grupe}}'
        );

        // drops index for column `ped_godina`
        $this->dropIndex(
            '{{%idx-upisne_grupe-ped_godina}}',
            '{{%upisne_grupe}}'
        );

        $this->dropTable('{{%upisne_grupe}}');
    }
}
