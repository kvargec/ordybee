<?php

use yii\db\Migration;

/**
 * Class m210322_173717_napravi_mailboxove
 */
class m210322_173717_napravi_mailboxove extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210322_173717_napravi_mailboxove cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210322_173717_napravi_mailboxove cannot be reverted.\n";

        return false;
    }
    */
}
