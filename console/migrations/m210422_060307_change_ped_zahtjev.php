<?php

use yii\db\Migration;

/**
 * Class m210422_060307_change_ped_zahtjev
 */
class m210422_060307_change_ped_zahtjev extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210422_060307_change_ped_zahtjev cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210422_060307_change_ped_zahtjev cannot be reverted.\n";

        return false;
    }
    */
}
