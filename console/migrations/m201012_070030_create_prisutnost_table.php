<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prisutnost}}`.
 */
class m201012_070030_create_prisutnost_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%prisutnost}}', [
            'id' => $this->primaryKey(),
            'datum' => $this->date(),
            'trajanje' => $this->float(),
            'entitet' => $this->string(100),
            'entitet_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%prisutnost}}');
    }
}
