<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%radno_mjesto}}`.
 */
class m210920_084803_add_slug_column_to_radno_mjesto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%radno_mjesto}}', 'slug', $this->string()); 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%radno_mjesto}}', 'slug');
    }
}
