<?php

use yii\db\Migration;

/**
 * Class m200603_150345_add_data_to_dokument_template_table
 */
class m200603_150345_add_data_to_dokument_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $str = file_get_contents(__DIR__ . '/data/dokument_predlosci.txt');
        $lines = explode(PHP_EOL, $str);
        $db = Yii::$app->db;
        foreach ($lines as $line) {
            if (!$line) {
                continue;
            }

            [$id, $naziv, $template, $slug, $sadrzaj, $ped_godina] = explode("\t", $line);
            $sadrzaj = str_replace('\r\n', '\n', $sadrzaj);
            $sql = "INSERT INTO public.dokument_template(id, naziv, template, slug, sadrzaj, ped_godina)
                    VALUES ($id, '$naziv', '$template', '$slug', E'$sadrzaj', $ped_godina);";
            $command = $db->createCommand($sql);
            $command->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $db = Yii::$app->db;
        $sql = 'DELETE FROM public.dokument_template;';
        $command = $db->createCommand($sql);
        $command->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200603_150345_add_data_to_dokument_template_table cannot be reverted.\n";

        return false;
    }
    */
}
