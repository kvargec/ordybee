<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cjenik_dijete}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%dijete}}`
 */
class m211124_084811_create_cjenik_dijete_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cjenik_dijete}}', [
            'id' => $this->primaryKey(),
            'dijete' => $this->integer()->notNull(),
            'cijena' => $this->float(),
            'dat_poc' => $this->date(),
            'dat_kraj' => $this->date(),
        ]);

        // creates index for column `dijete`
        $this->createIndex(
            '{{%idx-cjenik_dijete-dijete}}',
            '{{%cjenik_dijete}}',
            'dijete'
        );

        // add foreign key for table `{{%dijete}}`
        $this->addForeignKey(
            '{{%fk-cjenik_dijete-dijete}}',
            '{{%cjenik_dijete}}',
            'dijete',
            '{{%dijete}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%dijete}}`
        $this->dropForeignKey(
            '{{%fk-cjenik_dijete-dijete}}',
            '{{%cjenik_dijete}}'
        );

        // drops index for column `dijete`
        $this->dropIndex(
            '{{%idx-cjenik_dijete-dijete}}',
            '{{%cjenik_dijete}}'
        );

        $this->dropTable('{{%cjenik_dijete}}');
    }
}
