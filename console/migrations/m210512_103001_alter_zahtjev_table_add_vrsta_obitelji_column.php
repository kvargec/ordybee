<?php

use yii\db\Migration;

/**
 * Class m210512_103001_alter_zahtjev_table_add_vrsta_obitelji_column
 */
class m210512_103001_alter_zahtjev_table_add_vrsta_obitelji_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('zahtjev', 'vrsta_obitelji', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('zahtjev', 'vrsta_obitelji');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210512_103001_alter_zahtjev_table_add_vrsta_obitelji_column cannot be reverted.\n";

        return false;
    }
    */
}
