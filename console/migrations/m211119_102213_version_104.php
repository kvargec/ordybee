<?php

use yii\db\Migration;

/**
 * Class m211119_102213_version_104
 */
class m211119_102213_version_104 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('postavke',['postavka'=>'verzijaOrdyBee','vrijednost'=>"1.0.4","dodatno"=>""],['postavka'=>'verzijaOrdyBee']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211119_102213_version_104 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211119_102213_version_104 cannot be reverted.\n";

        return false;
    }
    */
}
