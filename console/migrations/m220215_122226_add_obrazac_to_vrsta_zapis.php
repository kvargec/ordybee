<?php

use yii\db\Migration;

/**
 * Class m220215_122226_add_obrazac_to_vrsta_zapis
 */
class m220215_122226_add_obrazac_to_vrsta_zapis extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('vrsta_zapis', ['title' => 'Obrazac za individualno praćenje djeteta', 'redoslijed' => 26, 'naziv' => 'pracenje djeteta', 'grupa' => 'odgajatelj']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('vrsta_zapis', ['naziv' => 'pracenje djeteta']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220215_122226_add_obrazac_to_vrsta_zapis cannot be reverted.\n";

        return false;
    }
    */
}
