<?php

use yii\db\Migration;

/**
 * Class m210305_085506_alter_aktivnsot_table
 */
class m210305_085506_alter_aktivnsot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('aktivnosti', 'datum_pocetka');
        $this->dropColumn('aktivnosti', 'datum_kraja');
        $this->addColumn('aktivnosti', 'datum_pocetka', $this->dateTime());
        $this->addColumn('aktivnosti', 'datum_kraja', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210305_085506_alter_aktivnsot_table cannot be reverted.\n";

        return false;
    }
    */
}
