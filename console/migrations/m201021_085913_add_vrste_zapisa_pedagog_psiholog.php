<?php

use yii\db\Migration;

/**
 * Class m201021_085913_add_vrste_zapisa_pedagog_psiholog
 */
class m201021_085913_add_vrste_zapisa_pedagog_psiholog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->batchInsert('vrsta_zapis',['naziv', 'grupa'], [
                ['observacija', 'pedagog'],
                ['nalaz','pedagog'],
                ['savjetovanje','pedagog'],
                ['observacija','psiholog'],
                ['nalaz','psiholog'],
                ['savjetovanje','psiholog'],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('vrsta_zapis','grupa = \'pedagog\' or grupa = \'psiholog\'');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201021_085913_add_vrste_zapisa_pedagog_psiholog cannot be reverted.\n";

        return false;
    }
    */
}
