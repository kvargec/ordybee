<?php

use yii\db\Migration;

/**
 * Class m211215_085844_version_105
 */
class m211215_085844_version_105 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('postavke',['postavka'=>'verzijaOrdyBee','vrijednost'=>"1.0.5","dodatno"=>""],['postavka'=>'verzijaOrdyBee']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211215_085844_version_105 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211215_085844_version_105 cannot be reverted.\n";

        return false;
    }
    */
}
