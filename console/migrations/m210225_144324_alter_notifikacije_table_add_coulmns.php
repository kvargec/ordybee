<?php

use yii\db\Migration;

/**
 * Class m210225_144324_alter_notifikacije_table_add_coulmns
 */
class m210225_144324_alter_notifikacije_table_add_coulmns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifikacije', 'sender_id' , $this->integer());
        $this->addColumn('notifikacije', 'subject', $this->string());
        $this->addColumn('notifikacije', 'is_deleted', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifikacije', 'sender_id');
        $this->dropColumn('notifikacije','subject');
        $this->dropColumn('notifikacije', 'is_deleted');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210225_144324_alter_notifikacije_table_add_coulmns cannot be reverted.\n";

        return false;
    }
    */
}
