<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dodatak_sadrzaj}}`.
 */
class m201109_095643_create_dodatak_sadrzaj_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dodatak_sadrzaj}}', [
            'id' => $this->primaryKey(),
            'dodatak' => $this->string(),
            'datum_pocetak' => $this->dateTime(),
            'datum_kraj' => $this->dateTime(),
            'tip' => $this->integer()
        ]);
        $this->createIndex(
            '{{%idx-dodatak_sadrzaj-tip_dodatak}}',
            '{{%dodatak_sadrzaj}}',
            'tip'
        );

        // add foreign key for table `{{%obrok}}`
        $this->addForeignKey(
            '{{%fk-dodatak_sadrzaj-tip_dodatak}}',
            '{{%dodatak_sadrzaj}}',
            'tip',
            '{{%tip_dodatak}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-dodatak_sadrzaj-tip_dodatak}}','{{%dodatak_sadrzaj}}');
        $this->dropIndex('{{%idx-dodatak_sadrzaj-tip_dodatak}}', '{{%dodatak_sadrzaj}}');
        $this->dropTable('{{%dodatak_sadrzaj}}');
    }
}
