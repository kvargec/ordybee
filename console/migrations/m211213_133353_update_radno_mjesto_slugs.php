<?php

use yii\db\Migration;

/**
 * Class m211213_133353_update_radno_mjesto_slugs
 */
class m211213_133353_update_radno_mjesto_slugs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //$this->update('radno_mjesto',['slug'=>'pedagog'],['or', ['naziv'=>'Pedagog','naziv'=>'pedagog','naziv'=>'pedagoginja','naziv'=>'Pedagoginja – stručni suradnik' ]]);
        $this->update('radno_mjesto',['slug'=>'pedagog'],['naziv'=>'Pedagog']);
        $this->update('radno_mjesto',['slug'=>'pedagog'],['naziv'=>'pedagog']);
        $this->update('radno_mjesto',['slug'=>'pedagog'],['naziv'=>'pedagoginja']);
        $this->update('radno_mjesto',['slug'=>'pedagog'],['naziv'=>'Pedagoginja – stručni suradnik']);
        //$this->update('radno_mjesto',['slug'=>'psiholog'],['or', ['naziv'=>'Psiholog','naziv'=>'psiholog','naziv'=>'psihologica','naziv'=>'Stručni suradnik psiholog','naziv'=>'Psiholog - stručni suradnik']]);
        $this->update('radno_mjesto',['slug'=>'psiholog'],['naziv'=>'Psiholog']);
        $this->update('radno_mjesto',['slug'=>'psiholog'],['naziv'=>'psiholog']);
        $this->update('radno_mjesto',['slug'=>'psiholog'],['naziv'=>'psihologica']);
        $this->update('radno_mjesto',['slug'=>'psiholog'],['naziv'=>'Stručni suradnik psiholog']);
        $this->update('radno_mjesto',['slug'=>'psiholog'],['naziv'=>'Psiholog - stručni suradnik']);
        //$this->update('radno_mjesto',['slug'=>'logoped'],['or', ['naziv'=>'Logoped','naziv'=>'logoped','naziv'=>'logopedkinja','naziv'=>'Logoped - stručni suradnik']]);
        $this->update('radno_mjesto',['slug'=>'logoped'],['naziv'=>'Logoped']);
        $this->update('radno_mjesto',['slug'=>'logoped'],['naziv'=>'logoped']);
        $this->update('radno_mjesto',['slug'=>'logoped'],['naziv'=>'logopedkinja']);
        $this->update('radno_mjesto',['slug'=>'logoped'],['naziv'=>'Logoped - stručni suradnik']);
        //$this->update('radno_mjesto',['slug'=>'rehabilitator'],['or',['naziv'=>'Rehabilitator','naziv'=>'rehabilitator','naziv'=>'Rehabilitator - stručni suradnik']]);
        $this->update('radno_mjesto',['slug'=>'rehabilitator'],['naziv'=>'Rehabilitator']);
        $this->update('radno_mjesto',['slug'=>'rehabilitator'],['naziv'=>'rehabilitator']);
        $this->update('radno_mjesto',['slug'=>'rehabilitator'],['naziv'=>'Rehabilitator - stručni suradnik']);
        //$this->update('radno_mjesto',['slug'=>'zdravstveni_voditelj'],['or',['naziv'=>'Zdravstvena voditeljica','naziv'=>'Zdravstveni voditelj','naziv'=>'zdravstvena voditeljica','naziv'=>'zdravstvena djelatnica','naziv'=>'Zdravstvena voditeljica – stručni suradnik']]);
        $this->update('radno_mjesto',['slug'=>'zdravstveni_voditelj'],['naziv'=>'Zdravstvena voditeljica']);
        $this->update('radno_mjesto',['slug'=>'zdravstveni_voditelj'],['naziv'=>'Zdravstveni voditelj']);
        $this->update('radno_mjesto',['slug'=>'zdravstveni_voditelj'],['naziv'=>'zdravstvena voditeljica']);
        $this->update('radno_mjesto',['slug'=>'zdravstveni_voditelj'],['naziv'=>'zdravstvena djelatnica']);
        $this->update('radno_mjesto',['slug'=>'zdravstveni_voditelj'],['naziv'=>'Zdravstvena voditeljica – stručni suradnik']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211213_133353_update_radno_mjesto_slugs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211213_133353_update_radno_mjesto_slugs cannot be reverted.\n";

        return false;
    }
    */
}
