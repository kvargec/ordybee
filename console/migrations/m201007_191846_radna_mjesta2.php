<?php

use yii\db\Migration;

/**
 * Class m201007_191846_radna_mjesta2
 */
class m201007_191846_radna_mjesta2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sprema=\common\models\Sprema::find()->orderBy('id ASC')->one();
        $id=$sprema->id;
        $data=[
            ['spremačica/pomoćna kuharica',$id,1,'001'],
            ['odgojiteljice',$id,1,'002'],
            ['spremačica',$id,1,'003'],
            ['glavna kuharica',$id,1,'004'],
            ['zdravstvena voditeljica',$id,1,'005'],
            ['kuharica',$id,1,'006'],
            ['ravnateljica',$id,1,'007'],
            ['tajnica',$id,1,'008'],
            ['pomoćna kuharica',$id,1,'009'],
            ['domar',$id,1,'010'],
            ['zamjenica ravnatelja',$id,1,'011'],
            ['voditeljica računovodstva',$id,1,'012'],
            ['pomoćni djelatnik za njegu, skrb i pratnju',$id,1,'013'],
            ['logoped',$id,1,'014'],
            ['psiholog',$id,1,'015'],
            ['pedagog',$id,1,'016'],
            ['rehabilitator',$id,1,'017'],
            ['ekonom',$id,1,'018']
        ];
        $this->batchInsert('radno_mjesto',['naziv','sprema','koeficijent','sifra'],$data);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201007_191846_radna_mjesta2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201007_191846_radna_mjesta2 cannot be reverted.\n";

        return false;
    }
    */
}
