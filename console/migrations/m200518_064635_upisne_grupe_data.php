<?php

use yii\db\Migration;

/**
 * Class m200518_064635_upisne_grupe_data
 */
class m200518_064635_upisne_grupe_data extends Migration
{
    /**
     * {@inheritdoc}
     *
     *
    1.4.2019.-31.3.2020. - mlađa jaslička skupina
    1.4.2018.-31.3.2019. - srenja jaslička skupina
    1.4.2017.-31.3.2018. - starija jaslička skupina
    1.4.2016.-31.3.2017. - mlađa vrtićka skupina
    1.4. 2015.-31.3.2016. - srednja vrtićka skupina
    1.4.2014.- 31.3.2015. - starija vrtićka skupina.
     */
    public function safeUp()
    {

        $data2=[
            ['mlađa jaslička skupina','2019-04-01','2020-03-31',17,1],
            ['srednja jaslička skupina','2018-04-01','2019-03-31',8,1],
            ['starija jaslička skupina','2017-04-01','2018-03-31',1,1],
            ['mlađa vrtićka skupina','2016-04-01','2017-03-31',5,1],
            ['srednja vrtićka skupina 5+','2014-04-01','2016-03-31',6,1],
            //['starija vrtićka skupina',''],
        ];
        $this->batchInsert('upisne_grupe',['naziv','date_poc','date_kraj','broj','ped_godina'],$data2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200518_064635_upisne_grupe_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200518_064635_upisne_grupe_data cannot be reverted.\n";

        return false;
    }
    */
}
