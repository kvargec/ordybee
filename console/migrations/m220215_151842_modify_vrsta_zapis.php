<?php

use yii\db\Migration;

/**
 * Class m220215_151842_modify_vrsta_zapis
 */
class m220215_151842_modify_vrsta_zapis extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('vrsta_zapis', ['naziv' => 'pracenje_djeteta'], ['naziv' => 'pracenje djeteta']);
        $this->update('vrsta_zapis', ['naziv' => 'covid_potvrda'], ['naziv' => 'covid potvrda']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220215_151842_modify_vrsta_zapis cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220215_151842_modify_vrsta_zapis cannot be reverted.\n";

        return false;
    }
    */
}
