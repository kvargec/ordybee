<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pitanje}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%upitnik}}`
 * - `{{%vrsta_pitanja}}`
 */
class m210324_100637_create_pitanje_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pitanje}}', [
            'id' => $this->primaryKey(),
            'pitanje' => $this->string(250),
            'opis' => $this->text(),
            'upitnik' => $this->integer(),
            'is_required' => $this->boolean()->defaultValue(false),
            'vrsta_pitanja' => $this->integer(),
            'odgovori' => $this->json(),
        ]);

        // creates index for column `upitnik`
        $this->createIndex(
            '{{%idx-pitanje-upitnik}}',
            '{{%pitanje}}',
            'upitnik'
        );

        // add foreign key for table `{{%upitnik}}`
        $this->addForeignKey(
            '{{%fk-pitanje-upitnik}}',
            '{{%pitanje}}',
            'upitnik',
            '{{%upitnik}}',
            'id',
            'CASCADE'
        );

        // creates index for column `vrsta_pitanja`
        $this->createIndex(
            '{{%idx-pitanje-vrsta_pitanja}}',
            '{{%pitanje}}',
            'vrsta_pitanja'
        );

        // add foreign key for table `{{%vrsta_pitanja}}`
        $this->addForeignKey(
            '{{%fk-pitanje-vrsta_pitanja}}',
            '{{%pitanje}}',
            'vrsta_pitanja',
            '{{%vrsta_pitanja}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%upitnik}}`
        $this->dropForeignKey(
            '{{%fk-pitanje-upitnik}}',
            '{{%pitanje}}'
        );

        // drops index for column `upitnik`
        $this->dropIndex(
            '{{%idx-pitanje-upitnik}}',
            '{{%pitanje}}'
        );

        // drops foreign key for table `{{%vrsta_pitanja}}`
        $this->dropForeignKey(
            '{{%fk-pitanje-vrsta_pitanja}}',
            '{{%pitanje}}'
        );

        // drops index for column `vrsta_pitanja`
        $this->dropIndex(
            '{{%idx-pitanje-vrsta_pitanja}}',
            '{{%pitanje}}'
        );

        $this->dropTable('{{%pitanje}}');
    }
}
