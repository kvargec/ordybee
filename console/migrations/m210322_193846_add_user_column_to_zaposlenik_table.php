<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zaposlenik}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m210322_193846_add_user_column_to_zaposlenik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zaposlenik}}', 'user', $this->integer());

        // creates index for column `user`
        $this->createIndex(
            '{{%idx-zaposlenik-user}}',
            '{{%zaposlenik}}',
            'user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik-user}}',
            '{{%zaposlenik}}',
            'user',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik-user}}',
            '{{%zaposlenik}}'
        );

        // drops index for column `user`
        $this->dropIndex(
            '{{%idx-zaposlenik-user}}',
            '{{%zaposlenik}}'
        );

        $this->dropColumn('{{%zaposlenik}}', 'user');
    }
}
