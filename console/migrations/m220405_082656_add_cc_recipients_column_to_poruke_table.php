<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%poruke}}`.
 */
class m220405_082656_add_cc_recipients_column_to_poruke_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%poruke}}', 'cc_recipients', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%poruke}}', 'cc_recipients');
    }
}
