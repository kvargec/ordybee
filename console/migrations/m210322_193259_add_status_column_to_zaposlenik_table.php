<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zaposlenik}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%status_opce}}`
 */
class m210322_193259_add_status_column_to_zaposlenik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zaposlenik}}', 'status', $this->integer());

        // creates index for column `status`
        $this->createIndex(
            '{{%idx-zaposlenik-status}}',
            '{{%zaposlenik}}',
            'status'
        );

        // add foreign key for table `{{%status_opce}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik-status}}',
            '{{%zaposlenik}}',
            'status',
            '{{%status_opce}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%status_opce}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik-status}}',
            '{{%zaposlenik}}'
        );

        // drops index for column `status`
        $this->dropIndex(
            '{{%idx-zaposlenik-status}}',
            '{{%zaposlenik}}'
        );

        $this->dropColumn('{{%zaposlenik}}', 'status');
    }
}
