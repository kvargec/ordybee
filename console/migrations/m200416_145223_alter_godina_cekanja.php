<?php

use yii\db\Migration;

/**
 * Class m200416_145223_alter_godina_cekanja
 */
class m200416_145223_alter_godina_cekanja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%dijete}}', 'god_cekanja',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200416_145223_alter_godina_cekanja cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200416_145223_alter_godina_cekanja cannot be reverted.\n";

        return false;
    }
    */
}
