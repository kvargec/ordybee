<?php

use yii\db\Migration;

/**
 * Class m211015_134417_version_103
 */
class m211015_134417_version_103 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('postavke',['postavka'=>'verzijaOrdyBee','vrijednost'=>"1.0.3","dodatno"=>""],['postavka'=>'verzijaOrdyBee']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211015_134417_version_103 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211015_134417_version_103 cannot be reverted.\n";

        return false;
    }
    */
}
