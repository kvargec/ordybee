<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document_zahtjev}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%document}}`
 * - `{{%zahtjev}}`
 */
class m200408_195049_create_document_zahtjev_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%document_zahtjev}}', [
            'id' => $this->primaryKey(),
            'document' => $this->integer(),
            'zahtjev' => $this->integer(),
        ]);

        // creates index for column `document`
        $this->createIndex(
            '{{%idx-document_zahtjev-document}}',
            '{{%document_zahtjev}}',
            'document'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-document_zahtjev-document}}',
            '{{%document_zahtjev}}',
            'document',
            '{{%document}}',
            'id',
            'CASCADE'
        );

        // creates index for column `zahtjev`
        $this->createIndex(
            '{{%idx-document_zahtjev-zahtjev}}',
            '{{%document_zahtjev}}',
            'zahtjev'
        );

        // add foreign key for table `{{%zahtjev}}`
        $this->addForeignKey(
            '{{%fk-document_zahtjev-zahtjev}}',
            '{{%document_zahtjev}}',
            'zahtjev',
            '{{%zahtjev}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-document_zahtjev-document}}',
            '{{%document_zahtjev}}'
        );

        // drops index for column `document`
        $this->dropIndex(
            '{{%idx-document_zahtjev-document}}',
            '{{%document_zahtjev}}'
        );

        // drops foreign key for table `{{%zahtjev}}`
        $this->dropForeignKey(
            '{{%fk-document_zahtjev-zahtjev}}',
            '{{%document_zahtjev}}'
        );

        // drops index for column `zahtjev`
        $this->dropIndex(
            '{{%idx-document_zahtjev-zahtjev}}',
            '{{%document_zahtjev}}'
        );

        $this->dropTable('{{%document_zahtjev}}');
    }
}
