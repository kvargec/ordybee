<?php

use yii\db\Migration;

/**
 * Class m201007_140107_vrste_zapisa_podaci
 */
class m201007_140107_vrste_zapisa_podaci extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201007_140107_vrste_zapisa_podaci cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201007_140107_vrste_zapisa_podaci cannot be reverted.\n";

        return false;
    }
    */
}
