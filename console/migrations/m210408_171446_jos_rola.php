<?php

use yii\db\Migration;

/**
 * Class m210408_171446_jos_rola
 */
class m210408_171446_jos_rola extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('auth_item',['name','type'], [
            ['logoped',0],[ 'ravnateljstvo',0],['pedagog',0],['psiholog',0],['kuhinja',0],['ekonom',0]

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210408_171446_jos_rola cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210408_171446_jos_rola cannot be reverted.\n";

        return false;
    }
    */
}
