<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pedagoska_godina}}`.
 */
class m200430_100409_create_pedagoska_godina_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pedagoska_godina}}', [
            'id' => $this->primaryKey(),
            'od' => $this->date()->notNull(),
            'do' => $this->date()->notNull(),
            'aktivna' => $this->boolean()->defaultValue(false),
            'postavke' => $this->json(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pedagoska_godina}}');
    }
}
