<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%aktivnost_media}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%aktivnosti}}`
 * - `{{%media}}`
 * - `{{%status_opce}}`
 */
class m210328_182820_create_aktivnost_media_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%aktivnost_media}}', [
            'id' => $this->primaryKey(),
            'aktivnost' => $this->integer(),
            'media' => $this->integer(),
            'redoslijed' => $this->integer(),
            'status' => $this->integer(),
            'extra_settings' => $this->json(),
        ]);

        // creates index for column `aktivnost`
        $this->createIndex(
            '{{%idx-aktivnost_media-aktivnost}}',
            '{{%aktivnost_media}}',
            'aktivnost'
        );

        // add foreign key for table `{{%aktivnosti}}`
        $this->addForeignKey(
            '{{%fk-aktivnost_media-aktivnost}}',
            '{{%aktivnost_media}}',
            'aktivnost',
            '{{%aktivnosti}}',
            'id',
            'CASCADE'
        );

        // creates index for column `media`
        $this->createIndex(
            '{{%idx-aktivnost_media-media}}',
            '{{%aktivnost_media}}',
            'media'
        );

        // add foreign key for table `{{%media}}`
        $this->addForeignKey(
            '{{%fk-aktivnost_media-media}}',
            '{{%aktivnost_media}}',
            'media',
            '{{%media}}',
            'id',
            'CASCADE'
        );

        // creates index for column `status`
        $this->createIndex(
            '{{%idx-aktivnost_media-status}}',
            '{{%aktivnost_media}}',
            'status'
        );

        // add foreign key for table `{{%status_opce}}`
        $this->addForeignKey(
            '{{%fk-aktivnost_media-status}}',
            '{{%aktivnost_media}}',
            'status',
            '{{%status_opce}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%aktivnosti}}`
        $this->dropForeignKey(
            '{{%fk-aktivnost_media-aktivnost}}',
            '{{%aktivnost_media}}'
        );

        // drops index for column `aktivnost`
        $this->dropIndex(
            '{{%idx-aktivnost_media-aktivnost}}',
            '{{%aktivnost_media}}'
        );

        // drops foreign key for table `{{%media}}`
        $this->dropForeignKey(
            '{{%fk-aktivnost_media-media}}',
            '{{%aktivnost_media}}'
        );

        // drops index for column `media`
        $this->dropIndex(
            '{{%idx-aktivnost_media-media}}',
            '{{%aktivnost_media}}'
        );

        // drops foreign key for table `{{%status_opce}}`
        $this->dropForeignKey(
            '{{%fk-aktivnost_media-status}}',
            '{{%aktivnost_media}}'
        );

        // drops index for column `status`
        $this->dropIndex(
            '{{%idx-aktivnost_media-status}}',
            '{{%aktivnost_media}}'
        );

        $this->dropTable('{{%aktivnost_media}}');
    }
}
