<?php

use yii\db\Migration;

/**
 * Class m210316_101137_alter_priusutnost_table
 */
class m210316_101137_alter_priusutnost_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('prisutnost', 'pocetak',$this->string());
        $this->alterColumn('prisutnost', 'kraj',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210316_101137_alter_priusutnost_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210316_101137_alter_priusutnost_table cannot be reverted.\n";

        return false;
    }
    */
}
