<?php

use yii\db\Migration;

/**
 * Class m220216_103500_add_cjenikPravila_to_postavke
 */
class m220216_103500_add_cjenikPravila_to_postavke extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rules[] = ['rule' => ['type' => 'days', 'pattern' => '=1', 'amount_type' => 'price', 'amount' => '25']];
        $rules[] = ['rule' => ['type' => 'days', 'pattern' => '<51', 'amount_type' => 'percentage', 'amount' => '40']];
        $this->insert('postavke', ['postavka' => 'cjenikPravila', 'dodatno' => $rules]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('postavke', ['postavka' => 'cjenikPravila']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220216_103500_add_cjenikPravila_to_postavke cannot be reverted.\n";

        return false;
    }
    */
}
