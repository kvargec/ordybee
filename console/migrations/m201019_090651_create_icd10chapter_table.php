<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%icd10chapter}}`.
 */
class m201019_090651_create_icd10chapter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%icd10chapter}}', [
            'id' => $this->primaryKey(),
            'codes' => $this->string(),
            'text' => $this->text(),
        ]);

        $data = [
            ["A00-B99","Određene infekcijske i parazitske bolesti"],
            ["C00-D48","Neoplazme"],
            ["D50-D89","Bolesti krvi i krvotvornih organa i određeni poremećaji imunološkog sustava"],
            ["E00-E90","Endokrine, nutricijske i metaboličke bolesti"],
            ["F00-F99","Mentalni poremećaji i poremećaji ponašanja"],
            ["G00-G99","Bolesti živčanog sustava"],["H00-H59","Bolesti oka i adneksa"],
            ["H60-H95","Bolesti uha i mastoidnih procesa"],
            ["I00-I99","Bolesti cirkulacijskog (krvožilnog) sustava"],
            ["J00-J99","Bolesti dišnog (respiracijskog) sustava"],
            ["K00-K93","Bolesti probavnog sustava"],
            ["L00-L99","Bolesti kože i potkožnog tkiva"],
            ["M00-M99","Bolesti mišićno-koštanog sustava i vezivnog tkiva"],
            ["N00-N99","Bolesti genitalno-urinarnog sustava"],
            ["O00-O99","Trudnoća i porođaj"],
            ["P00-P96","Određena stanja porođajnog perioda (5 mj. prije i 1 mj. poslije)"],
            ["Q00-Q99","Prirođene malformacije, deformacije i kromosomske abnormalnosti"],
            ["R00-R99","Simptomi, znakovi i abnormalni klinički i laboratorijski nalazi, neklasificirani drugdje"],
            ["S00-T98","Ozljede, trovanja i određene druge posljedice s vanjskim uzrokom"],
            ["V01-Y98","Vanjski uzroci pobola i smrtnosti"],
            ["Z00-Z99","Čimbenici s utjecajem na zdravstveni status i kontakt s zdravstvenim ustanovama"],
            ["U00-U99","Kodovi za posebne svrhe"],
        ];
        $this->batchInsert('icd10chapter',['codes','text'],$data);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%icd10chapter}}');
    }
}
