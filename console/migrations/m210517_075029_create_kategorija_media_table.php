<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%kategorija_media}}`.
 */
class m210517_075029_create_kategorija_media_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%kategorija_media}}', [
            'id' => $this->primaryKey(),
            'kategorija' => $this->string(),
            'parent' => $this->integer()->defaultValue(0),
            'status' => $this->integer()
        ]);
        $this->addForeignKey('tostatus',
        'kategorija_media',
            'status',
            'status_opce',
            'id'
        );
        $status = \common\models\StatusOpce::findOne(['naziv' => 'Aktivno']);
    //    $this->batchInsert('kategorija_media', ['kategorija', 'parent', 'status'], [['Vježbe', 0, $status->id]]);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('tostatus', 'kategorija_media');
        $this->dropTable('{{%kategorija_media}}');
    }
}
