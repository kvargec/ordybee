<?php

use yii\db\Migration;

/**
 * Class m210408_094934_update_vrste_zapisa
 */
class m210408_094934_update_vrste_zapisa extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->delete('vrsta_zapis',['naziv'=>'standardni','grupa'=>'zdravstvo']);
        $this->update('vrsta_zapis',['title'=>'Zapis o cijepljenju','redoslijed'=>10],['naziv'=>'cijepljenje']);
        $this->update('vrsta_zapis',['title'=>'Zapis o cijepljenju 5u1','redoslijed'=>11],['naziv'=>'cijepljenje 5u1']);
        $this->update('vrsta_zapis',['title'=>'Zapis o cijepljenju 6u1','redoslijed'=>12],['naziv'=>'cijepljenje 6u1']);
        $this->update('vrsta_zapis',['title'=>'Zapis o bolesti','redoslijed'=>2],['naziv'=>'bolest']);
        $this->update('vrsta_zapis',['title'=>'Zapis o zaraznoj bolesti','redoslijed'=>3],['naziv'=>'bolest zarazna']);
        $this->update('vrsta_zapis',['title'=>'Zapis o kroničnoj bolesti','redoslijed'=>21],['naziv'=>'kronična bolest']);
        $this->update('vrsta_zapis',['title'=>'Zapis o alergijama','redoslijed'=>22],['naziv'=>'alergija']);
        $this->update('vrsta_zapis',['title'=>'Zapis o poteškoćama','redoslijed'=>23],['naziv'=>'poteškoće']);
        $this->update('vrsta_zapis',['title'=>'Zapis o mjerenju','redoslijed'=>24],['naziv'=>'mjerenje']);
        $this->update('vrsta_zapis',['title'=>'Upiši osnovne podatke','redoslijed'=>20],['naziv'=>'standardni']);
        $this->update('vrsta_zapis',['title'=>'Unesi bilješku','redoslijed'=>1],['naziv'=>'bilješka']);

        $this->update('vrsta_zapis',['title'=>'Zapis o observaciji','redoslijed'=>3],['naziv'=>'observacija','grupa'=>'logoped']);
        $this->update('vrsta_zapis',['title'=>'Zapis o vježbi','redoslijed'=>2],['naziv'=>'vježbe','grupa'=>'logoped']);
        $this->update('vrsta_zapis',['title'=>'Unesi bilješku','naziv'=>'bilješka','redoslijed'=>1],['naziv'=>'nalaz','grupa'=>'logoped']);

        $this->update('vrsta_zapis',['title'=>'Zapis o observaciji','redoslijed'=>3],['naziv'=>'observacija','grupa'=>'pedagog']);
        $this->update('vrsta_zapis',['title'=>'Unesi nalaz','redoslijed'=>4],['naziv'=>'vježbe','grupa'=>'pedagog']);
        $this->update('vrsta_zapis',['title'=>'Zapis o savjetovanju','redoslijed'=>2],['naziv'=>'savjetovanje','grupa'=>'pedagog']);
        $this->insert('vrsta_zapis',['title'=>'Unesi bilješku','redoslijed'=>1,'naziv'=>'bilješka','grupa'=>'pedagog']);

        $this->update('vrsta_zapis',['title'=>'Zapis o observaciji','redoslijed'=>3],['naziv'=>'observacija','grupa'=>'psiholog']);
        $this->update('vrsta_zapis',['title'=>'Unesi nalaz','redoslijed'=>4],['naziv'=>'vježbe','grupa'=>'psiholog']);
        $this->update('vrsta_zapis',['title'=>'Zapis o savjetovanju','redoslijed'=>2],['naziv'=>'savjetovanje','grupa'=>'psiholog']);
        $this->insert('vrsta_zapis',['title'=>'Unesi bilješku','redoslijed'=>1,'naziv'=>'bilješka','grupa'=>'psiholog']);

        $this->update('vrsta_zapis',['title'=>'Zapis o razvoju djeteta','redoslijed'=>3],['naziv'=>'izvjestaj o razvoju djeteta','grupa'=>'odgajatelj']);
        $this->update('vrsta_zapis',['title'=>'Zapis o aktivnostima djeteta','redoslijed'=>4],['naziv'=>'izvjestaj o aktivnostima','grupa'=>'odgajatelj']);
        $this->update('vrsta_zapis',['title'=>'Zapis o radu','redoslijed'=>2],['naziv'=>'zapis','grupa'=>'odgajatelj']);
        $this->insert('vrsta_zapis',['title'=>'Unesi bilješku','redoslijed'=>1,'naziv'=>'bilješka','grupa'=>'odgajatelj']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210408_094934_update_vrste_zapisa cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210408_094934_update_vrste_zapisa cannot be reverted.\n";

        return false;
    }
    */
}
