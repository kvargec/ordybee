<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sprema}}`.
 */
class m200602_083149_create_sprema_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sprema}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(100),
            'kratica' => $this->string(20),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sprema}}');
    }
}
