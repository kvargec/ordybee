<?php

use yii\db\Migration;

/**
 * Class m210517_075424_add_vrsta_media
 */
class m210517_075424_add_vrsta_media extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('media_type', ['naziv', 'icon'], [
            ['Video', 'video.png'],
            ['Audio', 'audio.png'],
            ['PDF', 'pdf.png'],
            ['PPT', 'ppt.png'],
            ['Image', 'image.png']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210517_075424_add_vrsta_media cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210517_075424_add_vrsta_media cannot be reverted.\n";

        return false;
    }
    */
}
