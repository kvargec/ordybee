<?php

use yii\db\Migration;

/**
 * Class m211007_104036_ver102
 */
class m211007_104036_ver102 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('postavke',['postavka'=>'verzijaOrdyBee','vrijednost'=>"1.0.2","dodatno"=>""],['postavka'=>'verzijaOrdyBee']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211007_104036_ver102 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211007_104036_ver102 cannot be reverted.\n";

        return false;
    }
    */
}
