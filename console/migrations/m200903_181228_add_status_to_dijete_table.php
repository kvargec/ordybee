<?php

use yii\db\Migration;

/**
 * Class m200903_181228_add_status_to_dijete_table
 */
class m200903_181228_add_status_to_dijete_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('status_djeteta', ['status'=>'neupisan',]);
        $this->insert('status_djeteta', ['status'=>'upisan',]);
        $this->insert('status_djeteta', ['status'=>'ispisan',]);
        $this->addColumn('{{%dijete}}', 'status', $this->integer());
        $this->createIndex(
            '{{%idx-dijete-status_id}}',
            '{{%dijete}}',
            'status'
        );

        // add foreign key for table `{{%vrsta_unosa}}`
        $this->addForeignKey(
            '{{%fk-dijete-status_id}}',
            '{{%dijete}}',
            'status',
            '{{%status_djeteta}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200903_181228_add_status_to_dijete_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200903_181228_add_status_to_dijete_table cannot be reverted.\n";

        return false;
    }
    */
}
