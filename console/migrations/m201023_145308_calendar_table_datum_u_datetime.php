<?php

use yii\db\Migration;

/**
 * Class m201023_145308_calendar_table_datum_u_datetime
 */
class m201023_145308_calendar_table_datum_u_datetime extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('calendar','dan',$this->dateTime());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('calendar','dan',$this->date());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201023_145308_calendar_table_datum_u_datetime cannot be reverted.\n";

        return false;
    }
    */
}
