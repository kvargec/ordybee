<?php

use yii\db\Migration;

/**
 * Class m200515_084825_update_zadovoljene_opcije_column_of_zahtjev_kriterij_table
 */
class m200515_084825_update_zadovoljene_opcije_column_of_zahtjev_kriterij_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('update zahtjev_kriterij set zadovoljene_opcije=\'1\' where zadovoljen=true and zadovoljene_opcije is null;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200515_084825_update_zadovoljene_opcije_column_of_zahtjev_kriterij_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200515_084825_update_zadovoljene_opcije_column_of_zahtjev_kriterij_table cannot be reverted.\n";

        return false;
    }
    */
}
