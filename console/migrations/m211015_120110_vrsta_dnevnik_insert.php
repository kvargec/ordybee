<?php

use yii\db\Migration;

/**
 * Class m211015_120110_vrsta_dnevnik_insert
 */
class m211015_120110_vrsta_dnevnik_insert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('vrsta_dnevnik',['naziv'=>'Dnevnik zapisa',]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('vrsta_dnevnik',['naziv'=>'Dnevnik zapisa']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211015_120110_vrsta_dnevnik_insert cannot be reverted.\n";

        return false;
    }
    */
}
