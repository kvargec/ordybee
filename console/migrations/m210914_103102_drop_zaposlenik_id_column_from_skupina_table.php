<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%skupina}}`.
 */
class m210914_103102_drop_zaposlenik_id_column_from_skupina_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // drops foreign key for table `{{%zaposlenik}}`
        $this->dropForeignKey(
            '{{%fk-skupina-zaposlenik_id}}',
            '{{%skupina}}'
        );

        // drops index for column `zaposlenik_id`
        $this->dropIndex(
            '{{%idx-skupina-zaposlenik_id}}',
            '{{%skupina}}'
        );

        $this->dropColumn('{{%skupina}}', 'zaposlenik_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%skupina}}', 'zaposlenik_id', $this->integer());

        // creates index for column `zaposlenik_id`
        $this->createIndex(
            '{{%idx-skupina-zaposlenik_id}}',
            '{{%skupina}}',
            'zaposlenik_id'
        );

        // add foreign key for table `{{%zaposlenik}}`
        $this->addForeignKey(
            '{{%fk-skupina-zaposlenik_id}}',
            '{{%skupina}}',
            'zaposlenik_id',
            '{{%zaposlenik}}',
            'id',
            'CASCADE'
        );
    }
}
