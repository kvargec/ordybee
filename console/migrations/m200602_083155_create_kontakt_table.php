<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%kontakt}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%vrste_kontakt}}`
 */
class m200602_083155_create_kontakt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%kontakt}}', [
            'id' => $this->primaryKey(),
            'vrsta' => $this->integer(),
            'vrijednost' => $this->string(50),
        ]);

        // creates index for column `vrsta`
        $this->createIndex(
            '{{%idx-kontakt-vrsta}}',
            '{{%kontakt}}',
            'vrsta'
        );

        // add foreign key for table `{{%vrste_kontakt}}`
        $this->addForeignKey(
            '{{%fk-kontakt-vrsta}}',
            '{{%kontakt}}',
            'vrsta',
            '{{%vrste_kontakt}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%vrste_kontakt}}`
        $this->dropForeignKey(
            '{{%fk-kontakt-vrsta}}',
            '{{%kontakt}}'
        );

        // drops index for column `vrsta`
        $this->dropIndex(
            '{{%idx-kontakt-vrsta}}',
            '{{%kontakt}}'
        );

        $this->dropTable('{{%kontakt}}');
    }
}
