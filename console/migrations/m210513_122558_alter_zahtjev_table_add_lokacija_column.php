<?php

use yii\db\Migration;

/**
 * Class m210513_122558_alter_zahtjev_table_add_lokacija_column
 */
class m210513_122558_alter_zahtjev_table_add_lokacija_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('zahtjev', 'lokacija', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('zahtjev', 'lokacija');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210513_122558_alter_zahtjev_table_add_lokacija_column cannot be reverted.\n";

        return false;
    }
    */
}
