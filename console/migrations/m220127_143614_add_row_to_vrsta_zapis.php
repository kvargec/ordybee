<?php

use yii\db\Migration;

/**
 * Class m220127_143614_add_row_to_vrsta_zapis
 */
class m220127_143614_add_row_to_vrsta_zapis extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('vrsta_zapis', ['title' => 'COVID potvrda', 'redoslijed' => 25, 'naziv' => 'covid potvrda', 'grupa' => 'zdravstvo']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('vrsta_zapis', ['naziv' => 'covid potvrda']);

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220127_143614_add_row_to_vrsta_zapis cannot be reverted.\n";

        return false;
    }
    */
}
