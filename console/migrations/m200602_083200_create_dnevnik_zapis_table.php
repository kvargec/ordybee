<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dnevnik_zapis}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%dnevnik}}`
 * - `{{%vrsta_zapis}}`
 * - `{{%status}}`
 */
class m200602_083200_create_dnevnik_zapis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dnevnik_zapis}}', [
            'id' => $this->primaryKey(),
            'dnevnik'=>$this->integer(),
            'zapis' => $this->text(),
            'vrsta' => $this->integer(),
            'status' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);

        // creates index for column `dnevnik(integer)`
        $this->createIndex(
            '{{%idx-dnevnik_zapis-dnevnik}}',
            '{{%dnevnik_zapis}}',
            'dnevnik'
        );

        // add foreign key for table `{{%dnevnik}}`
        $this->addForeignKey(
            '{{%fk-dnevnik_zapis-dnevnik}}',
            '{{%dnevnik_zapis}}',
            'dnevnik',
            '{{%dnevnik}}',
            'id',
            'CASCADE'
        );

        // creates index for column `vrsta`
        $this->createIndex(
            '{{%idx-dnevnik_zapis-vrsta}}',
            '{{%dnevnik_zapis}}',
            'vrsta'
        );

        // add foreign key for table `{{%vrsta_zapis}}`
        $this->addForeignKey(
            '{{%fk-dnevnik_zapis-vrsta}}',
            '{{%dnevnik_zapis}}',
            'vrsta',
            '{{%vrsta_zapis}}',
            'id',
            'CASCADE'
        );

        // creates index for column `status`
        $this->createIndex(
            '{{%idx-dnevnik_zapis-status}}',
            '{{%dnevnik_zapis}}',
            'status'
        );

        // add foreign key for table `{{%status}}`
        $this->addForeignKey(
            '{{%fk-dnevnik_zapis-status}}',
            '{{%dnevnik_zapis}}',
            'status',
            '{{%status_opce}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%dnevnik}}`
        $this->dropForeignKey(
            '{{%fk-dnevnik_zapis-dnevnik(integer)}}',
            '{{%dnevnik_zapis}}'
        );

        // drops index for column `dnevnik(integer)`
        $this->dropIndex(
            '{{%idx-dnevnik_zapis-dnevnik(integer)}}',
            '{{%dnevnik_zapis}}'
        );

        // drops foreign key for table `{{%vrsta_zapis}}`
        $this->dropForeignKey(
            '{{%fk-dnevnik_zapis-vrsta}}',
            '{{%dnevnik_zapis}}'
        );

        // drops index for column `vrsta`
        $this->dropIndex(
            '{{%idx-dnevnik_zapis-vrsta}}',
            '{{%dnevnik_zapis}}'
        );

        // drops foreign key for table `{{%status}}`
        $this->dropForeignKey(
            '{{%fk-dnevnik_zapis-status}}',
            '{{%dnevnik_zapis}}'
        );

        // drops index for column `status`
        $this->dropIndex(
            '{{%idx-dnevnik_zapis-status}}',
            '{{%dnevnik_zapis}}'
        );

        $this->dropTable('{{%dnevnik_zapis}}');
    }
}
