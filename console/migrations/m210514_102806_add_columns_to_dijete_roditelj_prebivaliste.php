<?php

use yii\db\Migration;

/**
 * Class m210514_102806_add_columns_to_dijete_roditelj_prebivaliste
 */
class m210514_102806_add_columns_to_dijete_roditelj_prebivaliste extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('roditelj', 'adresa_prebivalista', $this->string());
        $this->addColumn('roditelj', 'prebivaliste_jednako_boraviste', $this->boolean());
        $this->addColumn('dijete', 'adresa_prebivalista', $this->string());
        $this->addColumn('dijete', 'prebivaliste_jednako_boraviste', $this->boolean());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('roditelj', 'adresa_prebivalista');
        $this->dropColumn('roditelj', 'prebivaliste_jednako_boraviste');
        $this->dropColumn('dijete', 'adresa_prebivalista');
        $this->dropColumn('dijete', 'prebivaliste_jednako_boraviste');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210514_102806_add_columns_to_dijete_roditelj_prebivaliste cannot be reverted.\n";

        return false;
    }
    */
}
