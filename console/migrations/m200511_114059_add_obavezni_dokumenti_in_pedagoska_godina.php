<?php

use yii\db\Migration;
use common\models\PedagoskaGodina;

/**
 * Class m200511_114059_add_obavezni_dokumenti_in_pedagoska_godina
 */
class m200511_114059_add_obavezni_dokumenti_in_pedagoska_godina extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $aktivnaGodina=PedagoskaGodina::findOne(['aktivna'=>true]);
        $postavke=$aktivnaGodina->postavke;
        if($postavke == null) {
            $postavke = [];
        }
        $postavke['obavezni_dokumenti'] = [
            'Preslika rodnog lista djeteta ili izvatka iz matice rođenih',
            'Preslike osobne iskaznice',
            'Potvrda o prebivalištu djeteta (ne starija od šest mjeseci)',
            'Potvrda Hrvatskog zavoda za mirovinsko osiguranje o radno-pravnom statusu (ne starija od 30 dana)',
            'Preslika Rješenja da je roditelj djeteta žrtva ili invalid Domovinskog rata',
            'Preslike izvatka iz matične knjige rođenih ili rodnog lista ostale malodobne djece',
            'Preslika medicinske dokumentacije djeteta koja potvrđuje posebne potrebe u rastu i razvoju djeteta',
            'Preslika Rješenja o skrbništvu za roditelje koji žive sami',
            'Preslika Rješenja o skrbništvu za uzdržavanu djecu',
            'Preslika Rješenja da je roditelj djeteta korisnik doplataka za dijete',
        ];

        $this->update (
            'pedagoska_godina',
            ['postavke'=>$postavke], ['id'=>$aktivnaGodina->id]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200511_114059_add_obavezni_dokumenti_in_pedagoska_godina cannot be reverted.\n";

        return false;
    }
    */
}
