<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%notifikacije}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m200409_123138_create_notifikacije_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%notifikacije}}', [
            'id' => $this->primaryKey(),
            'user' => $this->integer(),
            'akcija' => $this->string(100),
            'akcija_id' => $this->integer(),
            'radnja' => $this->string(250),
            'created_at' => $this->timestamp(),
            'read_at' => $this->timestamp(),
            'sadrzaj' => $this->text(),
            'status' => $this->integer(),
        ]);

        // creates index for column `user`
        $this->createIndex(
            '{{%idx-notifikacije-user}}',
            '{{%notifikacije}}',
            'user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-notifikacije-user}}',
            '{{%notifikacije}}',
            'user',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-notifikacije-user}}',
            '{{%notifikacije}}'
        );

        // drops index for column `user`
        $this->dropIndex(
            '{{%idx-notifikacije-user}}',
            '{{%notifikacije}}'
        );

        $this->dropTable('{{%notifikacije}}');
    }
}
