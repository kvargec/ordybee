<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrste_obroka}}`.
 */
class m221215_143848_create_vrste_obroka_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrste_obroka}}', [
            'id' => $this->primaryKey(),
            'vrsta' => $this->string()->notNull(),
            'cijena' => $this->decimal(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrste_obroka}}');
    }
}
