<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zahtjev}}`.
 */
class m210420_062637_add_napomena_column_to_zahtjev_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zahtjev}}', 'napomena', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%zahtjev}}', 'napomena');
    }
}
