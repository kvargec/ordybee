<?php

use yii\db\Migration;

/**
 * Class m210324_172712_vrste_pitanja_podaci
 */
class m210324_172712_vrste_pitanja_podaci extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('vrsta_pitanja', ['vrsta_pitanje','tip'],[
            ['Jedan odgovor','radio'],
            ['Više odgovora','checkbox'],
            ['Kratki unos','text'],
            ['Duži unos','textarea']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210324_172712_vrste_pitanja_podaci cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210324_172712_vrste_pitanja_podaci cannot be reverted.\n";

        return false;
    }
    */
}
