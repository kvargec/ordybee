<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrsta_kriterija}}`.
 */
class m200430_100456_create_vrsta_kriterija_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrsta_kriterija}}', [
            'id' => $this->primaryKey(),
            'vrsta' => $this->string(128)->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrsta_kriterija}}');
    }
}
