<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%prisutnost}}`.
 */
class m201023_124311_add_status_razlog_izostanka_columns_to_prisutnost_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('prisutnost','razlog_izostanka',$this->string());
        $this->addColumn('prisutnost','status',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('prisutnost','status');
        $this->dropColumn('prisutnost','razlog_izostanka');
    }
}
