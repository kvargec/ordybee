<?php

use yii\db\Migration;

/**
 * Class m210517_094429_alter_media_table_add_grupe
 */
class m210517_094429_alter_media_table_add_grupe extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('media', 'recipients', $this->json());
            $this->addColumn('media', 'kategorija', $this->integer());
            $this->addForeignKey('tokategorija', 'media', 'kategorija', 'kategorija_media', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('tokategorija', 'media');
        $this->dropColumn('media', 'recipients');
        $this->dropColumn('media', 'kategorija');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210517_094429_alter_media_table_add_grupe cannot be reverted.\n";

        return false;
    }
    */
}
