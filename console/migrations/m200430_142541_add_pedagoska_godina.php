<?php

use yii\db\Migration;

/**
 * Class m200430_142541_add_pedagoska_godina
 */
class m200430_142541_add_pedagoska_godina extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('pedagoska_godina', [
            'od' => '2019-09-01',
            'do' => '2020-08-31',
            'aktivna' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('pedagoska_godina');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200430_142541_add_pedagoska_godina cannot be reverted.\n";

        return false;
    }
    */
}
