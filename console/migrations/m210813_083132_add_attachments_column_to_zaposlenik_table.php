<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zaposlenik}}`.
 */
class m210813_083132_add_attachments_column_to_zaposlenik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zaposlenik}}', 'attachments', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%zaposlenik}}', 'attachments');
    }
}
