<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zaposlenje}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%zaposlenik}}`
 * - `{{%vrsta_zaposlenja}}`
 * - `{{%radno_mjesto}}`
 */
class m200602_083153_create_zaposlenje_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zaposlenje}}', [
            'id' => $this->primaryKey(),
            'dat_pocetka' => $this->date(),
            'date_kraj' => $this->date()->null(),
            'koeficijent' => $this->float(),
            'zaposlenik' => $this->integer(),
            'vrsta' => $this->integer(),
            'r_mjesto' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);

        // creates index for column `zaposlenik`
        $this->createIndex(
            '{{%idx-zaposlenje-zaposlenik}}',
            '{{%zaposlenje}}',
            'zaposlenik'
        );

        // add foreign key for table `{{%zaposlenik}}`
        $this->addForeignKey(
            '{{%fk-zaposlenje-zaposlenik}}',
            '{{%zaposlenje}}',
            'zaposlenik',
            '{{%zaposlenik}}',
            'id',
            'CASCADE'
        );

        // creates index for column `vrsta`
        $this->createIndex(
            '{{%idx-zaposlenje-vrsta}}',
            '{{%zaposlenje}}',
            'vrsta'
        );

        // add foreign key for table `{{%vrsta_zaposlenja}}`
        $this->addForeignKey(
            '{{%fk-zaposlenje-vrsta}}',
            '{{%zaposlenje}}',
            'vrsta',
            '{{%vrsta_zaposlenja}}',
            'id',
            'CASCADE'
        );

        // creates index for column `r_mjesto`
        $this->createIndex(
            '{{%idx-zaposlenje-r_mjesto}}',
            '{{%zaposlenje}}',
            'r_mjesto'
        );

        // add foreign key for table `{{%radno_mjesto}}`
        $this->addForeignKey(
            '{{%fk-zaposlenje-r_mjesto}}',
            '{{%zaposlenje}}',
            'r_mjesto',
            '{{%radno_mjesto}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%zaposlenik}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenje-zaposlenik}}',
            '{{%zaposlenje}}'
        );

        // drops index for column `zaposlenik`
        $this->dropIndex(
            '{{%idx-zaposlenje-zaposlenik}}',
            '{{%zaposlenje}}'
        );

        // drops foreign key for table `{{%vrsta_zaposlenja}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenje-vrsta}}',
            '{{%zaposlenje}}'
        );

        // drops index for column `vrsta`
        $this->dropIndex(
            '{{%idx-zaposlenje-vrsta}}',
            '{{%zaposlenje}}'
        );

        // drops foreign key for table `{{%radno_mjesto}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenje-r_mjesto}}',
            '{{%zaposlenje}}'
        );

        // drops index for column `r_mjesto`
        $this->dropIndex(
            '{{%idx-zaposlenje-r_mjesto}}',
            '{{%zaposlenje}}'
        );

        $this->dropTable('{{%zaposlenje}}');
    }
}
