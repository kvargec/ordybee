<?php

use yii\db\Migration;

/**
 * Class m210411_163837_promijenti_type_roles
 */
class m210411_163837_promijenti_type_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('auth_item',['type'=>1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210411_163837_promijenti_type_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210411_163837_promijenti_type_roles cannot be reverted.\n";

        return false;
    }
    */
}
