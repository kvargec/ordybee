<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrsta_obitelji}}`.
 */
class m210512_110506_create_vrsta_obitelji_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrsta_obitelji}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(),
        ]);
        $this->batchInsert('vrsta_obitelji', ['naziv'], [
            ['Bračna zajednica'], ['Izvanbračna zajednica'], ['Životno partnerstvo'], ['Jednoroditeljska obitelj'], ['Samohrani roditelj'], ['Skrbništvo'], ['Udomiteljstvo']
        ]);
        $this->addForeignKey('to_vrsta_obitelji',
            'zahtjev',
          'vrsta_obitelji',
            'vrsta_obitelji',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('to_vrsta_obitelji', 'zahtjev');
        $this->dropTable('{{%vrsta_obitelji}}');
    }
}
