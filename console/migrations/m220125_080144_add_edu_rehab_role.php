<?php

use yii\db\Migration;

/**
 * Class m220125_080144_add_edu_rehab_role
 */
class m220125_080144_add_edu_rehab_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $rehabRole = $auth->getRole('edukacijski rehabilitator');
        // create a role named "odgajatelj"
        if (!isset($rehabRole)) {
            $rehabRole = $auth->createRole('edukacijski rehabilitator');
            $rehabRole->description = 'Edukacijski rehabilitator';
            $auth->add($rehabRole); // create permission for certain tasks
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220125_080144_add_edu_rehab_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220125_080144_add_edu_rehab_role cannot be reverted.\n";

        return false;
    }
    */
}
