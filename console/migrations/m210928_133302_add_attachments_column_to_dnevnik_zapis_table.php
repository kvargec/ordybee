<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%dnevnik_zapis}}`.
 */
class m210928_133302_add_attachments_column_to_dnevnik_zapis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%dnevnik_zapis}}', 'attachments', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%dnevnik_zapis}}', 'attachments');
    }
}
