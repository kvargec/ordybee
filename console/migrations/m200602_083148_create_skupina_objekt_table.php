<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%skupina_objekt}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%skupina}}`
 * - `{{%objekt}}`
 */
class m200602_083148_create_skupina_objekt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%skupina_objekt}}', [
            'id' => $this->primaryKey(),
            'skupina' => $this->integer(),
            'objekt' => $this->integer(),
            'aktivna' => $this->boolean(),
        ]);

        // creates index for column `skupina`
        $this->createIndex(
            '{{%idx-skupina_objekt-skupina}}',
            '{{%skupina_objekt}}',
            'skupina'
        );

        // add foreign key for table `{{%skupina}}`
        $this->addForeignKey(
            '{{%fk-skupina_objekt-skupina}}',
            '{{%skupina_objekt}}',
            'skupina',
            '{{%skupina}}',
            'id',
            'CASCADE'
        );

        // creates index for column `objekt`
        $this->createIndex(
            '{{%idx-skupina_objekt-objekt}}',
            '{{%skupina_objekt}}',
            'objekt'
        );

        // add foreign key for table `{{%objekt}}`
        $this->addForeignKey(
            '{{%fk-skupina_objekt-objekt}}',
            '{{%skupina_objekt}}',
            'objekt',
            '{{%objekt}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%skupina}}`
        $this->dropForeignKey(
            '{{%fk-skupina_objekt-skupina}}',
            '{{%skupina_objekt}}'
        );

        // drops index for column `skupina`
        $this->dropIndex(
            '{{%idx-skupina_objekt-skupina}}',
            '{{%skupina_objekt}}'
        );

        // drops foreign key for table `{{%objekt}}`
        $this->dropForeignKey(
            '{{%fk-skupina_objekt-objekt}}',
            '{{%skupina_objekt}}'
        );

        // drops index for column `objekt`
        $this->dropIndex(
            '{{%idx-skupina_objekt-objekt}}',
            '{{%skupina_objekt}}'
        );

        $this->dropTable('{{%skupina_objekt}}');
    }
}
