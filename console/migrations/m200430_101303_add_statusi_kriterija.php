<?php

use yii\db\Migration;

/**
 * Class m200430_101303_add_statusi_kriterija
 */
class m200430_101303_add_statusi_kriterija extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $str = file_get_contents(__DIR__ . '/data/statusi_kriterija.txt');
        $lines = explode(PHP_EOL, $str);
        $db = Yii::$app->db;
        foreach ($lines as $line) {
            if (!$line) {
                continue;
            }

            $status = $line;
            $sql = "INSERT INTO public.status_kriterija(status) VALUES ('$status');";
            $command = $db->createCommand($sql);
            $command->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $db = Yii::$app->db;
        $sql = 'DELETE FROM public.status_kriterija;';
        $command = $db->createCommand($sql);
        $command->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200430_101303_add_statusi_kriterija cannot be reverted.\n";

        return false;
    }
    */
}
