<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%poruke}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%status_opce}}`
 * - `{{%vrsta_poruke}}`
 */
class m210321_181007_create_poruke_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%poruke}}', [
            'id' => $this->primaryKey(),
            'subject' => $this->string(255),
            'message' => $this->text(),
            'sender' => $this->integer(),
            'recipients' => $this->json(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'send_at' => $this->timestamp(),
            'status' => $this->integer(),
            'attachments' => $this->json(),
            'priority' => $this->tinyInteger(),
            'vrsta_poruke' => $this->integer(),
        ]);

        // creates index for column `status`
        $this->createIndex(
            '{{%idx-poruke-status}}',
            '{{%poruke}}',
            'status'
        );

        // add foreign key for table `{{%status_opce}}`
        $this->addForeignKey(
            '{{%fk-poruke-status}}',
            '{{%poruke}}',
            'status',
            '{{%status_opce}}',
            'id',
            'CASCADE'
        );

        // creates index for column `vrsta_poruke`
        $this->createIndex(
            '{{%idx-poruke-vrsta_poruke}}',
            '{{%poruke}}',
            'vrsta_poruke'
        );

        // add foreign key for table `{{%vrsta_poruke}}`
        $this->addForeignKey(
            '{{%fk-poruke-vrsta_poruke}}',
            '{{%poruke}}',
            'vrsta_poruke',
            '{{%vrste_poruke}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%status_opce}}`
        $this->dropForeignKey(
            '{{%fk-poruke-status}}',
            '{{%poruke}}'
        );

        // drops index for column `status`
        $this->dropIndex(
            '{{%idx-poruke-status}}',
            '{{%poruke}}'
        );

        // drops foreign key for table `{{%vrsta_poruke}}`
        $this->dropForeignKey(
            '{{%fk-poruke-vrsta_poruke}}',
            '{{%poruke}}'
        );

        // drops index for column `vrsta_poruke`
        $this->dropIndex(
            '{{%idx-poruke-vrsta_poruke}}',
            '{{%poruke}}'
        );

        $this->dropTable('{{%poruke}}');
    }
}
