<?php

use yii\db\Migration;

/**
 * Class m210625_144349_new_fix_table_pitanje
 */
class m210625_144349_new_fix_table_pitanje extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('pitanje',['odgovori' => null], ['pitanje'=>'Ukoliko je dijete osjetljivo na drugi podražaj, koji?']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210625_144349_new_fix_table_pitanje cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210625_144349_new_fix_table_pitanje cannot be reverted.\n";

        return false;
    }
    */
}
