<?php

use yii\db\Migration;

/**
 * Class m201021_141545_add_vrste_zaposlenja
 */
class m201021_141545_add_vrste_zaposlenja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('vrsta_zaposlenja',['naziv'],[
            ['određeno'],['neodređeno'],['pola radnog vremena']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('vrsta_zaposlenja');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201021_141545_add_vrste_zaposlenja cannot be reverted.\n";

        return false;
    }
    */
}
