<?php

use yii\db\Migration;

/**
 * Class m220223_090039_add_row_to_postavke
 */
class m220223_090039_add_row_to_postavke extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('postavke', ['postavka' => 'backendWeb', 'vrijednost' => null]);
        $this->insert('postavke', ['postavka' => 'frontendWeb', 'vrijednost' => null]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('postavke', ['postavka' => 'backendWeb']);
        $this->delete('postavke', ['postavka' => 'frontendWeb']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220223_090039_add_row_to_postavke cannot be reverted.\n";

        return false;
    }
    */
}
