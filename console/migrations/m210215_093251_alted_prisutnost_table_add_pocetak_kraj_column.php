<?php

use yii\db\Migration;

/**
 * Class m210215_093251_alted_prisutnost_table_add_pocetak_kraj_column
 */
class m210215_093251_alted_prisutnost_table_add_pocetak_kraj_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('prisutnost', 'pocetak', $this->timestamp());
            $this->addColumn('prisutnost', 'kraj', $this->timestamp());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('prisutnost', 'pocetak');
        $this->dropColumn('prisutnost', 'kraj');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210215_093251_alted_prisutnost_table_add_pocetak_kraj_column cannot be reverted.\n";

        return false;
    }
    */
}
