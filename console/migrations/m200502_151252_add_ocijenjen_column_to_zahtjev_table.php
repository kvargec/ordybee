<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zahtjev}}`.
 */
class m200502_151252_add_ocijenjen_column_to_zahtjev_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zahtjev}}', 'ocijenjen', $this->boolean()->defaultValue(false));
        $this->update('zahtjev', ['ocijenjen'=>false]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%zahtjev}}', 'ocijenjen');
    }
}
