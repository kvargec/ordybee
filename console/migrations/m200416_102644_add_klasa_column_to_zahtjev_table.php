<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zahtjev}}`.
 */
class m200416_102644_add_klasa_column_to_zahtjev_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zahtjev}}', 'klasa', $this->string(150));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%zahtjev}}', 'klasa');
    }
}
