<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dijete}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%vrsta_programa}}`
 */
class m200408_193222_create_dijete_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dijete}}', [
            'id' => $this->primaryKey(),
            'ime' => $this->string(150)->notNull(),
            'prezime' => $this->string(150)->notNull(),
            'spol' => $this->string(1),
            'dat_rod' => $this->date(),
            'oib' => $this->string(13),
            'adresa' => $this->string(150),
            'mjesto' => $this->string(150),
            'prebivaliste' => $this->string(200),
            'sestra' => $this->string(1),
            'cekanje' => $this->string(1),
            'god_cekanja' => $this->integer(),
            'razvoj' => $this->string(50),
            'vrsta_programa' => $this->integer(),
            'teskoce' => $this->string(100),
            'dijagnostika' => $this->string(200),
            'druge_potrebe' => $this->string(200),
            'posebne' => $this->string(200),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);

        // creates index for column `vrsta_programa`
        $this->createIndex(
            '{{%idx-dijete-vrsta_programa}}',
            '{{%dijete}}',
            'vrsta_programa'
        );

        // add foreign key for table `{{%vrsta_programa}}`
        $this->addForeignKey(
            '{{%fk-dijete-vrsta_programa}}',
            '{{%dijete}}',
            'vrsta_programa',
            '{{%vrsta_programa}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%vrsta_programa}}`
        $this->dropForeignKey(
            '{{%fk-dijete-vrsta_programa}}',
            '{{%dijete}}'
        );

        // drops index for column `vrsta_programa`
        $this->dropIndex(
            '{{%idx-dijete-vrsta_programa}}',
            '{{%dijete}}'
        );

        $this->dropTable('{{%dijete}}');
    }
}
