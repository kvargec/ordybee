<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objekt}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%mjesto}}`
 * - `{{%pedagoska_godina}}`
 */
class m200602_083146_create_objekt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%objekt}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(100)->notNull(),
            'adresa' => $this->string(255),
            'sifra' => $this->string(20),
            'mjesto' => $this->integer(),
            'ped_godina' => $this->integer(),
        ]);

        // creates index for column `mjesto`
        $this->createIndex(
            '{{%idx-objekt-mjesto}}',
            '{{%objekt}}',
            'mjesto'
        );

        // add foreign key for table `{{%mjesto}}`
        $this->addForeignKey(
            '{{%fk-objekt-mjesto}}',
            '{{%objekt}}',
            'mjesto',
            '{{%mjesto}}',
            'id',
            'CASCADE'
        );

        // creates index for column `ped_godina`
        $this->createIndex(
            '{{%idx-objekt-ped_godina}}',
            '{{%objekt}}',
            'ped_godina'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-objekt-ped_godina}}',
            '{{%objekt}}',
            'ped_godina',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%mjesto}}`
        $this->dropForeignKey(
            '{{%fk-objekt-mjesto}}',
            '{{%objekt}}'
        );

        // drops index for column `mjesto`
        $this->dropIndex(
            '{{%idx-objekt-mjesto}}',
            '{{%objekt}}'
        );

        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-objekt-ped_godina}}',
            '{{%objekt}}'
        );

        // drops index for column `ped_godina`
        $this->dropIndex(
            '{{%idx-objekt-ped_godina}}',
            '{{%objekt}}'
        );

        $this->dropTable('{{%objekt}}');
    }
}
