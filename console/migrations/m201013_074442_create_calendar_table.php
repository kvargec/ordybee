<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%calendar}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%dan_type}}`
 */
class m201013_074442_create_calendar_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%calendar}}', [
            'id' => $this->primaryKey(),
            'dan' => $this->date(),
            'vrsta' => $this->integer(),
            'napomena' => $this->text(),
        ]);

        // creates index for column `vrsta`
        $this->createIndex(
            '{{%idx-calendar-vrsta}}',
            '{{%calendar}}',
            'vrsta'
        );

        // add foreign key for table `{{%dan_type}}`
        $this->addForeignKey(
            '{{%fk-calendar-vrsta}}',
            '{{%calendar}}',
            'vrsta',
            '{{%dan_type}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%dan_type}}`
        $this->dropForeignKey(
            '{{%fk-calendar-vrsta}}',
            '{{%calendar}}'
        );

        // drops index for column `vrsta`
        $this->dropIndex(
            '{{%idx-calendar-vrsta}}',
            '{{%calendar}}'
        );

        $this->dropTable('{{%calendar}}');
    }
}
