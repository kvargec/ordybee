<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%vrsta_programa}}`.
 */
class m220510_133954_add_parent_column_to_vrsta_programa_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%vrsta_programa}}', 'parent', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%vrsta_programa}}', 'parent');
    }
}
