<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%grupa_proizvod}}`.
 */
class m210326_184803_create_grupa_proizvod_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%grupa_proizvod}}', [
            'id' => $this->primaryKey(),
            'grupa' => $this->string(150),
            'parent' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%grupa_proizvod}}');
    }
}
