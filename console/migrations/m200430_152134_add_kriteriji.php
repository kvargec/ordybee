<?php

use yii\db\Migration;
use common\models\PedagoskaGodina;
use common\models\VrstaKriterija;
use common\models\StatusKriterija;

/**
 * Class m200430_152134_add_kriteriji
 */
class m200430_152134_add_kriteriji extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $aktivnaGodinaId=PedagoskaGodina::findOne(['aktivna'=>true])->id;

        $osnovniKriterijId=VrstaKriterija::findOne(['vrsta'=>'osnovni'])->id;
        $dodatniKriterijId=VrstaKriterija::findOne(['vrsta'=>'dodatni'])->id;

        $aktivniKriterijId=StatusKriterija::findOne(['status'=>'aktivan'])->id;

        $columns = [
            'pedagoska_godina_id',
            'naziv',
            'vrsta_kriterija_id',
            'bodovi',
            'izravan_upis',
            'redoslijed',
            'status_kriterija_id'
        ];

        $osnovniKriteriji = [
            "Prebivalište na području grada Vrbovca"=> 50,
            "Oba zaposlena roditelja"=> 40,
            "Jedan roditelj zaposlen, drugi roditelj na redovnom školovanju"=> 40,
            "Oba roditelja na redovnom školovanju"=> 40,
            "Oba roditelja zaposlena u Obiteljskom poljoprivrednom gospodarstvu"=> 40,
            "Zaposlen samohrani roditelj/skrbnik djeteta koje žive s jednim roditeljem/skrbnikom"=> 40,
            "Djeca s liste čekanja od upisa za svaku prethodnu pedagošku godinu (u kontinuitetu)"=> 10,
            "Jedan roditelj zaposlen, drugi roditelj nezaposlen"=> 5
        ];

        $redoslijed = 1;
        foreach($osnovniKriteriji as $naziv=>$bodovi) {
            if($bodovi == 'izravan upis') {
                $columns = [
                    'pedagoska_godina_id'=>$aktivnaGodinaId,
                    'naziv'=>$naziv,
                    'vrsta_kriterija_id'=>$osnovniKriterijId,
                    'izravan_upis'=>true,
                    'redoslijed'=>$redoslijed,
                    'status_kriterija_id'=>$aktivniKriterijId,
                ];
            } else {
                $columns = [
                    'pedagoska_godina_id'=>$aktivnaGodinaId,
                    'naziv'=>$naziv,
                    'vrsta_kriterija_id'=>$osnovniKriterijId,
                    'bodovi'=>$bodovi,
                    'redoslijed'=>$redoslijed,
                    'status_kriterija_id'=>$aktivniKriterijId,
                ];
            }

            $this->insert('kriterij', $columns);
            $redoslijed++;
        }

        $dodatniKriterij = [
            "Djeca roditelja žrtava i invalida domovinskog rata"=>30,
            "Djeca iz obitelji troje ili više maloljetne djece, po djetetu"=>10,
            "Djeca s teškoćama u razvoju"=>"izravan upis",
            "Djeca koja žive u iznimno teškim socijalnim i zdravstvenim prilikama"=>"izravan upis",
            "Djeca samohranih roditelja i djeca u udomiteljskim obiteljima"=>15,
            "Djeca u godini prije polaska u osnovnu školu"=>10,
            "Djeca roditelja koji primaju doplatak za djecu"=>5
        ];

        foreach($dodatniKriterij as $naziv=>$bodovi) {
            if($bodovi == 'izravan upis') {
                $columns = [
                    'pedagoska_godina_id'=>$aktivnaGodinaId,
                    'naziv'=>$naziv,
                    'vrsta_kriterija_id'=>$dodatniKriterijId,
                    'izravan_upis'=>true,
                    'redoslijed'=>$redoslijed,
                    'status_kriterija_id'=>$aktivniKriterijId,
                ];
            } else {
                $columns = [
                    'pedagoska_godina_id'=>$aktivnaGodinaId,
                    'naziv'=>$naziv,
                    'vrsta_kriterija_id'=>$dodatniKriterijId,
                    'bodovi'=>$bodovi,
                    'redoslijed'=>$redoslijed,
                    'status_kriterija_id'=>$aktivniKriterijId,
                ];
            }

            $this->insert('kriterij', $columns);
            $redoslijed++;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $db = Yii::$app->db;
        $sql = 'DELETE FROM public.kriterij;';
        $command = $db->createCommand($sql);
        $command->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200430_152134_add_kriteriji cannot be reverted.\n";

        return false;
    }
    */
}
