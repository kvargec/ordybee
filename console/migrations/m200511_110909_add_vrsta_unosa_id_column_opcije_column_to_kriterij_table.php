<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%kriterij}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%vrsta_unosa}}`
 */
class m200511_110909_add_vrsta_unosa_id_column_opcije_column_to_kriterij_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%kriterij}}', 'vrsta_unosa_id', $this->integer());
        $this->addColumn('{{%kriterij}}', 'opcije', $this->json());

        // creates index for column `vrsta_unosa_id`
        $this->createIndex(
            '{{%idx-kriterij-vrsta_unosa_id}}',
            '{{%kriterij}}',
            'vrsta_unosa_id'
        );

        // add foreign key for table `{{%vrsta_unosa}}`
        $this->addForeignKey(
            '{{%fk-kriterij-vrsta_unosa_id}}',
            '{{%kriterij}}',
            'vrsta_unosa_id',
            '{{%vrsta_unosa}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%vrsta_unosa}}`
        $this->dropForeignKey(
            '{{%fk-kriterij-vrsta_unosa_id}}',
            '{{%kriterij}}'
        );

        // drops index for column `vrsta_unosa_id`
        $this->dropIndex(
            '{{%idx-kriterij-vrsta_unosa_id}}',
            '{{%kriterij}}'
        );

        $this->dropColumn('{{%kriterij}}', 'vrsta_unosa_id');
        $this->dropColumn('{{%kriterij}}', 'opcije');
    }
}
