<?php

use yii\db\Migration;

/**
 * Class m221216_092823_add_cijenaObroka_to_postavke
 */
class m221216_092823_add_cijenaObroka_to_postavke extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('postavke', ['postavka' => 'cijenaObroka', 'vrijednost' => 0]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('postavke', ['postavka' => 'cijenaObroka']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221216_092823_add_cijenaObroka_to_postavke cannot be reverted.\n";

        return false;
    }
    */
}
