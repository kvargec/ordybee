<?php

use yii\db\Migration;

/**
 * Class m210922_075320_add_postavke_verzija
 */
class m210922_075320_add_postavke_verzija extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

		$db = Yii::$app->db;
        $sql = "SELECT setval('postavke_id_seq', 21, true);";
        $command = $db->createCommand($sql);
        $command->execute();
		$this->insert('postavke',['postavka'=>'verzijaOrdyBee','vrijednost'=>"1.0","dodatno"=>""]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210922_075320_add_postavke_verzija cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210922_075320_add_postavke_verzija cannot be reverted.\n";

        return false;
    }
    */
}
