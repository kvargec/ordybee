<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%kriterij}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%pedagoska_godina}}`
 * - `{{%vrsta_kriterija}}`
 * - `{{%status_kriterija}}`
 */
class m200430_100638_create_kriterij_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%kriterij}}', [
            'id' => $this->primaryKey(),
            'pedagoska_godina_id' => $this->integer()->notNull(),
            'naziv' => $this->string(256)->notNull(),
            'vrsta_kriterija_id' => $this->integer()->notNull(),
            'bodovi' => $this->integer()->defaultValue(0),
            'izravan_upis' => $this->boolean()->defaultValue(false),
            'redoslijed' => $this->integer()->defaultValue(1),
            'status_kriterija_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `pedagoska_godina_id`
        $this->createIndex(
            '{{%idx-kriterij-pedagoska_godina_id}}',
            '{{%kriterij}}',
            'pedagoska_godina_id'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-kriterij-pedagoska_godina_id}}',
            '{{%kriterij}}',
            'pedagoska_godina_id',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );

        // creates index for column `vrsta_kriterija_id`
        $this->createIndex(
            '{{%idx-kriterij-vrsta_kriterija_id}}',
            '{{%kriterij}}',
            'vrsta_kriterija_id'
        );

        // add foreign key for table `{{%vrsta_kriterija}}`
        $this->addForeignKey(
            '{{%fk-kriterij-vrsta_kriterija_id}}',
            '{{%kriterij}}',
            'vrsta_kriterija_id',
            '{{%vrsta_kriterija}}',
            'id',
            'CASCADE'
        );

        // creates index for column `status_kriterija_id`
        $this->createIndex(
            '{{%idx-kriterij-status_kriterija_id}}',
            '{{%kriterij}}',
            'status_kriterija_id'
        );

        // add foreign key for table `{{%status_kriterija}}`
        $this->addForeignKey(
            '{{%fk-kriterij-status_kriterija_id}}',
            '{{%kriterij}}',
            'status_kriterija_id',
            '{{%status_kriterija}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-kriterij-pedagoska_godina_id}}',
            '{{%kriterij}}'
        );

        // drops index for column `pedagoska_godina_id`
        $this->dropIndex(
            '{{%idx-kriterij-pedagoska_godina_id}}',
            '{{%kriterij}}'
        );

        // drops foreign key for table `{{%vrsta_kriterija}}`
        $this->dropForeignKey(
            '{{%fk-kriterij-vrsta_kriterija_id}}',
            '{{%kriterij}}'
        );

        // drops index for column `vrsta_kriterija_id`
        $this->dropIndex(
            '{{%idx-kriterij-vrsta_kriterija_id}}',
            '{{%kriterij}}'
        );

        // drops foreign key for table `{{%status_kriterija}}`
        $this->dropForeignKey(
            '{{%fk-kriterij-status_kriterija_id}}',
            '{{%kriterij}}'
        );

        // drops index for column `status_kriterija_id`
        $this->dropIndex(
            '{{%idx-kriterij-status_kriterija_id}}',
            '{{%kriterij}}'
        );

        $this->dropTable('{{%kriterij}}');
    }
}
