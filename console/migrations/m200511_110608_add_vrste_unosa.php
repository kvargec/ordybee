<?php

use yii\db\Migration;

/**
 * Class m200511_110608_add_vrste_unosa
 */
class m200511_110608_add_vrste_unosa extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('vrsta_unosa', ['vrsta'=>'checkbox',]);
        $this->insert('vrsta_unosa', ['vrsta'=>'select',]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $db = Yii::$app->db;
        $sql = 'DELETE FROM public.vrsta_unosa;';
        $command = $db->createCommand($sql);
        $command->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200511_110608_add_vrste_unosa cannot be reverted.\n";

        return false;
    }
    */
}
