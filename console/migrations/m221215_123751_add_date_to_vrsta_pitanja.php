<?php

use yii\db\Migration;

/**
 * Class m221215_123751_add_date_to_vrsta_pitanja
 */
class m221215_123751_add_date_to_vrsta_pitanja extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('vrsta_pitanja', [
            'vrsta_pitanje' => 'Datumi',
            'tip' => 'text',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('vrsta_pitanja', ['vrsta_pitanje' => 'Datumi']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221215_123751_add_date_to_vrsta_pitanja cannot be reverted.\n";

        return false;
    }
    */
}
