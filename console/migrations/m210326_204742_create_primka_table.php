<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%primka}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m210326_204742_create_primka_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%primka}}', [
            'id' => $this->primaryKey(),
            'broj_primke' => $this->string(250),
            'ulazni_racun' => $this->integer(),
            'datum_primke' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'primio' => $this->string(150),
            'unos' => $this->integer(),
        ]);

        // creates index for column `unos`
        $this->createIndex(
            '{{%idx-primka-unos}}',
            '{{%primka}}',
            'unos'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-primka-unos}}',
            '{{%primka}}',
            'unos',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-primka-unos}}',
            '{{%primka}}'
        );

        // drops index for column `unos`
        $this->dropIndex(
            '{{%idx-primka-unos}}',
            '{{%primka}}'
        );

        $this->dropTable('{{%primka}}');
    }
}
