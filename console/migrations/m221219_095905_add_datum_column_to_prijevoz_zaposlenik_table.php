<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%prijevoz_zaposlenik}}`.
 */
class m221219_095905_add_datum_column_to_prijevoz_zaposlenik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%prijevoz_zaposlenik}}', 'datum', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%prijevoz_zaposlenik}}', 'datum');
    }
}
