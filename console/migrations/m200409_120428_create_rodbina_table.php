<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rodbina}}`.
 */
class m200409_120428_create_rodbina_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rodbina}}', [
            'id' => $this->primaryKey(),
            'dijete' => $this->integer(),
            'sibling'=>$this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rodbina}}');
    }
}
