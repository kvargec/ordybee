<?php

use yii\db\Migration;

/**
 * Class m210514_103414_dodaj_drzavljanstvo
 */
class m210514_103414_dodaj_drzavljanstvo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dijete','drzavljanstvo',$this->string(150));
        $this->addColumn('roditelj','drzavljanstvo',$this->string(150));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210514_103414_dodaj_drzavljanstvo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210514_103414_dodaj_drzavljanstvo cannot be reverted.\n";

        return false;
    }
    */
}
