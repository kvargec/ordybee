<?php

use yii\db\Migration;

/**
 * Class m221216_100421_add_data_to_vrsta_prijevoza_table
 */
class m221216_100421_add_data_to_prijevoz_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('prijevoz', ['vrsta', 'cijena'], [
            ['Javni prijevoz', 0],
            ['Osobni automobil', 0],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('prijevoz', ['vrsta' => 'Javni prijevoz']);
        $this->delete('prijevoz', ['vrsta' => 'Osobni automobil']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221216_100421_add_data_to_vrsta_prijevoza_table cannot be reverted.\n";

        return false;
    }
    */
}
