<?php

use yii\db\Migration;

/**
 * Class m210408_055859_dodaj_role
 */
class m210408_055859_dodaj_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('auth_item',['name','type'], [
            ['superadmin',0],[ 'administracija',0],['zdravstveno',0],

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210408_055859_dodaj_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210408_055859_dodaj_role cannot be reverted.\n";

        return false;
    }
    */
}
