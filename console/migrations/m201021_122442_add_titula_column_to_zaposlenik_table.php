<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zaposlenik}}`.
 */
class m201021_122442_add_titula_column_to_zaposlenik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zaposlenik}}', 'titula', $this->string(200));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%zaposlenik}}', 'titula');
    }
}
