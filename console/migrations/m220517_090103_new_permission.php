<?php

use yii\db\Migration;

/**
 * Class m220517_090103_new_permission
 */
class m220517_090103_new_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $permission = $auth->createPermission('user-management');
        $permission->description = 'User Management';
        $auth->add($permission);

        $administratorRole = $auth->getRole('superadmin');
        $auth->addChild($administratorRole, $auth->getPermission('user-management'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220517_090103_new_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220517_090103_new_permission cannot be reverted.\n";

        return false;
    }
    */
}
