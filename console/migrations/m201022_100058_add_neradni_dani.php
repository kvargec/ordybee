<?php

use yii\db\Migration;

/**
 * Class m201022_100058_add_neradni_dani
 */
class m201022_100058_add_neradni_dani extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->batchInsert('calendar', ['dan','vrsta','napomena'],[
                ['2020-01-01','2','Nova godina'],
                ['2020-01-06','2','Sveta tri kralja'],
                ['2020-04-12','2','Uskrs'],
                ['2020-04-13','2','Uskrsni ponedjeljak'],
                ['2020-05-01','2','Praznik rada'],
                ['2020-05-30','2','Dand državnosti'],
                ['2020-06-11','2','Tijelovo'],
                ['2020-06-22','2','Dan antifašističke borbe'],
                ['2020-08-05','2','Dan pobjede i domovinske zahvalnosti i Dan hrvatskih branitelja'],
                ['2020-08-15','2','Velika Gospa'],
                ['2020-11-01','2','Dan svih svetih'],
                ['2020-11-18','2','Dan sjećanja na žrtve Domovinskog rata'],
                ['2020-12-25','2','Božić'],
                ['2020-12-26','2','Sveti Stjepan'],
                ['2021-01-01','2','Nova godina'],
                ['2021-01-06','2','Sveta tri kralja'],
                ['2021-04-04','2','Uskrs'],
                ['2021-04-05','2','Uskrsni ponedjeljak'],
                ['2021-05-01','2','Praznik rada'],
                ['2021-05-30','2','Dand državnosti'],
                ['2021-06-03','2','Tijelovo'],
                ['2021-06-22','2','Dan antifašističke borbe'],
                ['2021-08-05','2','Dan pobjede i domovinske zahvalnosti i Dan hrvatskih branitelja'],
                ['2021-08-15','2','Velika Gospa'],
                ['2021-11-01','2','Dan svih svetih'],
                ['2021-11-18','2','Dan sjećanja na žrtve Domovinskog rata'],
                ['2021-12-25','2','Božić'],
                ['2021-12-26','2','Sveti Stjepan'],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('calendar');


    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201022_100058_add_neradni_dani cannot be reverted.\n";

        return false;
    }
    */
}
