<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%prijevoz_zaposlenik}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%prijevoz}}`
 * - `{{%zaposlenik}}`
 */
class m221215_143610_create_prijevoz_zaposlenik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%prijevoz_zaposlenik}}', [
            'id' => $this->primaryKey(),
            'prijevoz_id' => $this->integer()->notNull(),
            'zaposlenik_id' => $this->integer()->notNull(),
            'cijena' => $this->decimal(2),
        ]);

        // creates index for column `prijevoz_id`
        $this->createIndex(
            '{{%idx-prijevoz_zaposlenik-prijevoz_id}}',
            '{{%prijevoz_zaposlenik}}',
            'prijevoz_id'
        );

        // add foreign key for table `{{%prijevoz}}`
        $this->addForeignKey(
            '{{%fk-prijevoz_zaposlenik-prijevoz_id}}',
            '{{%prijevoz_zaposlenik}}',
            'prijevoz_id',
            '{{%prijevoz}}',
            'id',
            'CASCADE'
        );

        // creates index for column `zaposlenik_id`
        $this->createIndex(
            '{{%idx-prijevoz_zaposlenik-zaposlenik_id}}',
            '{{%prijevoz_zaposlenik}}',
            'zaposlenik_id'
        );

        // add foreign key for table `{{%zaposlenik}}`
        $this->addForeignKey(
            '{{%fk-prijevoz_zaposlenik-zaposlenik_id}}',
            '{{%prijevoz_zaposlenik}}',
            'zaposlenik_id',
            '{{%zaposlenik}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%prijevoz}}`
        $this->dropForeignKey(
            '{{%fk-prijevoz_zaposlenik-prijevoz_id}}',
            '{{%prijevoz_zaposlenik}}'
        );

        // drops index for column `prijevoz_id`
        $this->dropIndex(
            '{{%idx-prijevoz_zaposlenik-prijevoz_id}}',
            '{{%prijevoz_zaposlenik}}'
        );

        // drops foreign key for table `{{%zaposlenik}}`
        $this->dropForeignKey(
            '{{%fk-prijevoz_zaposlenik-zaposlenik_id}}',
            '{{%prijevoz_zaposlenik}}'
        );

        // drops index for column `zaposlenik_id`
        $this->dropIndex(
            '{{%idx-prijevoz_zaposlenik-zaposlenik_id}}',
            '{{%prijevoz_zaposlenik}}'
        );

        $this->dropTable('{{%prijevoz_zaposlenik}}');
    }
}
