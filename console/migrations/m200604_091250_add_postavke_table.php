<?php

use yii\db\Migration;

/**
 * Class m200604_091250_add_postavke_table
 */
class m200604_091250_add_postavke_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('postavke', ['postavka'=>'nazivVrtica','vrijednost'=>'Dječji vrtić Vrbovec']);
        $this->insert('postavke', ['postavka'=>'nazivRavnatelj','vrijednost'=>'Ravnatelj']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200604_091250_add_postavke_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200604_091250_add_postavke_table cannot be reverted.\n";

        return false;
    }
    */
}
