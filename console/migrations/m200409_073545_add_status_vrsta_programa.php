<?php

use yii\db\Migration;

/**
 * Class m200409_073545_add_status_vrsta_programa
 */
class m200409_073545_add_status_vrsta_programa extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $data=array(["U izradi"],['Predan'],[ 'U obradi'],[ 'Upisan'],[ 'Na čekanju'],[ 'Odbijen']);
        $this->batchInsert('status',['status'],$data);
        $data2=[
            ['redoviti 10-satni program'],['skraćeni program učenja engleskog jezika']
        ];
        $this->batchInsert('vrsta_programa',['vrsta'],$data2);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200409_073545_add_status_vrsta_programa cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200409_073545_add_status_vrsta_programa cannot be reverted.\n";

        return false;
    }
    */
}
