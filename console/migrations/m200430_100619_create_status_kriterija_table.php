<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%status_kriterija}}`.
 */
class m200430_100619_create_status_kriterija_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%status_kriterija}}', [
            'id' => $this->primaryKey(),
            'status' => $this->string(128)->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%status_kriterija}}');
    }
}
