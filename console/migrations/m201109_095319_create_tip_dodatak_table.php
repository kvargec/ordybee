<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tip_dodatak}}`.
 */
class m201109_095319_create_tip_dodatak_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tip_dodatak}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string()
        ]);
        $this->batchInsert('{{%tip_dodatak}}',['naziv'],[['jelovnik']]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tip_dodatak}}');
    }
}
