<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dnevnik}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%zaposlenik}}`
 * - `{{%pedagoska_godina}}`
 * - `{{%vrsta_dnevnika}}`
 */
class m200602_083157_create_dnevnik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dnevnik}}', [
            'id' => $this->primaryKey(),
            'sifra' => $this->string(50),
            'zaposlenik' => $this->integer(),
            'ped_godina' => $this->integer(),
            'vrsta_dnevnika' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);

        // creates index for column `zaposlenik`
        $this->createIndex(
            '{{%idx-dnevnik-zaposlenik}}',
            '{{%dnevnik}}',
            'zaposlenik'
        );

        // add foreign key for table `{{%zaposlenik}}`
        $this->addForeignKey(
            '{{%fk-dnevnik-zaposlenik}}',
            '{{%dnevnik}}',
            'zaposlenik',
            '{{%zaposlenik}}',
            'id',
            'CASCADE'
        );

        // creates index for column `ped_godina`
        $this->createIndex(
            '{{%idx-dnevnik-ped_godina}}',
            '{{%dnevnik}}',
            'ped_godina'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-dnevnik-ped_godina}}',
            '{{%dnevnik}}',
            'ped_godina',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );

        // creates index for column `vrsta_dnevnika`
        $this->createIndex(
            '{{%idx-dnevnik-vrsta_dnevnika}}',
            '{{%dnevnik}}',
            'vrsta_dnevnika'
        );

        // add foreign key for table `{{%vrsta_dnevnika}}`
        $this->addForeignKey(
            '{{%fk-dnevnik-vrsta_dnevnika}}',
            '{{%dnevnik}}',
            'vrsta_dnevnika',
            '{{%vrsta_dnevnik}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%zaposlenik}}`
        $this->dropForeignKey(
            '{{%fk-dnevnik-zaposlenik}}',
            '{{%dnevnik}}'
        );

        // drops index for column `zaposlenik`
        $this->dropIndex(
            '{{%idx-dnevnik-zaposlenik}}',
            '{{%dnevnik}}'
        );

        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-dnevnik-ped_godina}}',
            '{{%dnevnik}}'
        );

        // drops index for column `ped_godina`
        $this->dropIndex(
            '{{%idx-dnevnik-ped_godina}}',
            '{{%dnevnik}}'
        );

        // drops foreign key for table `{{%vrsta_dnevnika}}`
        $this->dropForeignKey(
            '{{%fk-dnevnik-vrsta_dnevnika}}',
            '{{%dnevnik}}'
        );

        // drops index for column `vrsta_dnevnika`
        $this->dropIndex(
            '{{%idx-dnevnik-vrsta_dnevnika}}',
            '{{%dnevnik}}'
        );

        $this->dropTable('{{%dnevnik}}');
    }
}
