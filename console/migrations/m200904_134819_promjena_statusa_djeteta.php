<?php

use yii\db\Migration;

/**
 * Class m200904_134819_promjena_statusa_djeteta
 */
class m200904_134819_promjena_statusa_djeteta extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $db = Yii::$app->db;
        $sql = 'UPDATE dijete SET status=1';
        $command = $db->createCommand($sql);
        $command->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200904_134819_promjena_statusa_djeteta cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200904_134819_promjena_statusa_djeteta cannot be reverted.\n";

        return false;
    }
    */
}
