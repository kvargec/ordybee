<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%notifikacije_dokument}}`.
 */
class m210226_094213_create_notifikacije_dokument_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%notifikacije_dokument}}', [
            'id' => $this->primaryKey(),
            'notifikacija' => $this->integer(),
            'dokument' => $this->integer()
        ]);
        $this->addForeignKey('tonotifikacije', '{{%notifikacije_dokument}}','notifikacija','notifikacije','id');
        $this->addForeignKey('todocument', '{{%notifikacije_dokument}}','dokument','document','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('tonotifikacije','{{%notifikacije_dokument}}');
        $this->dropForeignKey('todocument','{{%notifikacije_dokument}}');
        $this->dropTable('{{%notifikacije_dokument}}');
    }
}
