<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zahtjev}}`.
 */
class m200512_085115_add_predani_obavezni_dokumenti_column_to_zahtjev_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zahtjev}}', 'predani_obavezni_dokumenti', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%zahtjev}}', 'predani_obavezni_dokumenti');
    }
}
