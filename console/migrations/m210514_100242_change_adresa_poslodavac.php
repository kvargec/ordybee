<?php

use yii\db\Migration;

/**
 * Class m210514_100242_change_adresa_poslodavac
 */
class m210514_100242_change_adresa_poslodavac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('roditelj','adresa_poslodavca',$this->string(250));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210514_100242_change_adresa_poslodavac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210514_100242_change_adresa_poslodavac cannot be reverted.\n";

        return false;
    }
    */
}
