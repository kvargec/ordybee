<?php

use yii\db\Migration;

/**
 * Class m210513_172932_add_cetvrt_column_to_table_dijete
 */
class m210513_172932_add_cetvrt_column_to_table_dijete extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dijete','cetvrt',$this->string(150));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removeColumn('dijete','cetvrt');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210513_172932_add_cetvrt_column_to_table_dijete cannot be reverted.\n";

        return false;
    }
    */
}
