<?php

use yii\db\Migration;

/**
 * Class m210531_132105_alter_pravo_columns
 */
class m210531_132105_alter_pravo_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('zahtjev','pravo_pocetak',$this->timestamp());
        $this->alterColumn('zahtjev','pravo_kraj',$this->timestamp());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210531_132105_alter_pravo_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210531_132105_alter_pravo_columns cannot be reverted.\n";

        return false;
    }
    */
}
