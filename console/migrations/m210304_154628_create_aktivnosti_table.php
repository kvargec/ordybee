<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%aktivnosti}}`.
 */
class m210304_154628_create_aktivnosti_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%aktivnosti}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(),
            'datum_pocetka' => $this->integer(),
            'datum_kraja' => $this->integer(),
            'trajanje' => $this->string(),
            'opis' => $this->string(),
            'skupine' => $this->json(),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%aktivnosti}}');
    }
}
