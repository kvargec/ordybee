<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%odgovori}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%pitanje}}`
 * - `{{%user}}`
 */
class m210324_101040_create_odgovori_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%odgovori}}', [
            'id' => $this->primaryKey(),
            'pitanje' => $this->integer(),
            'user' => $this->integer(),
            'value' => $this->json(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);

        // creates index for column `pitanje`
        $this->createIndex(
            '{{%idx-odgovori-pitanje}}',
            '{{%odgovori}}',
            'pitanje'
        );

        // add foreign key for table `{{%pitanje}}`
        $this->addForeignKey(
            '{{%fk-odgovori-pitanje}}',
            '{{%odgovori}}',
            'pitanje',
            '{{%pitanje}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user`
        $this->createIndex(
            '{{%idx-odgovori-user}}',
            '{{%odgovori}}',
            'user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-odgovori-user}}',
            '{{%odgovori}}',
            'user',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%pitanje}}`
        $this->dropForeignKey(
            '{{%fk-odgovori-pitanje}}',
            '{{%odgovori}}'
        );

        // drops index for column `pitanje`
        $this->dropIndex(
            '{{%idx-odgovori-pitanje}}',
            '{{%odgovori}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-odgovori-user}}',
            '{{%odgovori}}'
        );

        // drops index for column `user`
        $this->dropIndex(
            '{{%idx-odgovori-user}}',
            '{{%odgovori}}'
        );

        $this->dropTable('{{%odgovori}}');
    }
}
