<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrste_kontakt}}`.
 */
class m200602_083154_create_vrste_kontakt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrste_kontakt}}', [
            'id' => $this->primaryKey(),
            'vrsta' => $this->string(50),
            'redoslijed' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrste_kontakt}}');
    }
}
