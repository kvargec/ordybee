<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%dijete}}`.
 */
class m210409_181247_add_mjesto_rodjenja_column_to_dijete_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%dijete}}', 'mjesto_rod', $this->string(250));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%dijete}}', 'mjesto_rod');
    }
}
