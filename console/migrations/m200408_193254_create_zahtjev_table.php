<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zahtjev}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%dijete}}`
 */
class m200408_193254_create_zahtjev_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zahtjev}}', [
            'id' => $this->primaryKey(),
            'urbroj' => $this->string(50),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
            'user' => $this->integer(),
            'dijete' => $this->integer(),
        ]);

        // creates index for column `user`
        $this->createIndex(
            '{{%idx-zahtjev-user}}',
            '{{%zahtjev}}',
            'user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-zahtjev-user}}',
            '{{%zahtjev}}',
            'user',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `dijete`
        $this->createIndex(
            '{{%idx-zahtjev-dijete}}',
            '{{%zahtjev}}',
            'dijete'
        );

        // add foreign key for table `{{%dijete}}`
        $this->addForeignKey(
            '{{%fk-zahtjev-dijete}}',
            '{{%zahtjev}}',
            'dijete',
            '{{%dijete}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-zahtjev-user}}',
            '{{%zahtjev}}'
        );

        // drops index for column `user`
        $this->dropIndex(
            '{{%idx-zahtjev-user}}',
            '{{%zahtjev}}'
        );

        // drops foreign key for table `{{%dijete}}`
        $this->dropForeignKey(
            '{{%fk-zahtjev-dijete}}',
            '{{%zahtjev}}'
        );

        // drops index for column `dijete`
        $this->dropIndex(
            '{{%idx-zahtjev-dijete}}',
            '{{%zahtjev}}'
        );

        $this->dropTable('{{%zahtjev}}');
    }
}
