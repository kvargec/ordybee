<?php

use yii\db\Migration;

/**
 * Class m200911_120750_add_dat_upisa_to_dijete_table
 */
class m200911_120750_add_dat_upisa_to_dijete_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%dijete}}', 'dat_upisa', $this->date());
        $this->addColumn('{{%dijete}}', 'dat_ispisa', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%dijete}}', 'dat_upisa');
        $this->dropColumn('{{%dijete}}', 'dat_ispisa');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200911_120750_add_dat_upisa_to_dijete_table cannot be reverted.\n";

        return false;
    }
    */
}
