<?php

use yii\db\Migration;

/**
 * Class m210830_135142_adding_new_role
 */
class m210830_135142_adding_new_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
		$odgajateljRole = $auth->getRole('odgajatelj');
        // create a role named "odgajatelj"
		if(!isset($odgajateljRole)){
			$odgajateljRole = $auth->createRole('odgajatelj');
			$odgajateljRole->description = 'Odgajatelj';
			$auth->add($odgajateljRole); // create permission for certain tasks
			$permission = $auth->createPermission('user-odgajatelj');
			$permission->description = 'User Odgajatelj';
			$auth->add($permission);
		}





    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210830_135142_adding_new_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210830_135142_adding_new_role cannot be reverted.\n";

        return false;
    }
    */
}
