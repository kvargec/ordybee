<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%skupina}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%zaposlenik}}`
 */
class m210825_133808_add_zaposlenik_id_column_to_skupina_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%skupina}}', 'zaposlenik_id', $this->integer());

        // creates index for column `zaposlenik_id`
        $this->createIndex(
            '{{%idx-skupina-zaposlenik_id}}',
            '{{%skupina}}',
            'zaposlenik_id'
        );

        // add foreign key for table `{{%zaposlenik}}`
        $this->addForeignKey(
            '{{%fk-skupina-zaposlenik_id}}',
            '{{%skupina}}',
            'zaposlenik_id',
            '{{%zaposlenik}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%zaposlenik}}`
        $this->dropForeignKey(
            '{{%fk-skupina-zaposlenik_id}}',
            '{{%skupina}}'
        );

        // drops index for column `zaposlenik_id`
        $this->dropIndex(
            '{{%idx-skupina-zaposlenik_id}}',
            '{{%skupina}}'
        );

        $this->dropColumn('{{%skupina}}', 'zaposlenik_id');
    }
}
