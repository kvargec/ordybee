<?php

use yii\db\Migration;

/**
 * Class m210514_113908_alter_dijete_roditelj_table
 */
class m210514_113908_alter_dijete_roditelj_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('roditelj', 'prebivaliste_jednako_boraviste');
        $this->addColumn('roditelj', 'prebivaliste_jednako_boraviste', $this->string(1));
        $this->dropColumn('dijete', 'prebivaliste_jednako_boraviste');
        $this->addColumn('dijete', 'prebivaliste_jednako_boraviste', $this->string(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210514_113908_alter_dijete_roditelj_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210514_113908_alter_dijete_roditelj_table cannot be reverted.\n";

        return false;
    }
    */
}
