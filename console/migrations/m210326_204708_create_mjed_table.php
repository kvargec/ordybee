<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mjed}}`.
 */
class m210326_204708_create_mjed_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mjed}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(150),
            'kratica' => $this->string(20),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mjed}}');
    }
}
