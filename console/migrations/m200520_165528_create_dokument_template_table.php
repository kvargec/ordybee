<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dokument_template}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%pedagoska_godina}}`
 */
class m200520_165528_create_dokument_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dokument_template}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(250),
            'template' => $this->string(150)->defaultValue('memorandum'),
            'slug' => $this->string(250),
            'sadrzaj' => $this->text(),
            'ped_godina' => $this->integer(),
        ]);

        // creates index for column `ped_godina`
        $this->createIndex(
            '{{%idx-dokument_template-ped_godina}}',
            '{{%dokument_template}}',
            'ped_godina'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-dokument_template-ped_godina}}',
            '{{%dokument_template}}',
            'ped_godina',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-dokument_template-ped_godina}}',
            '{{%dokument_template}}'
        );

        // drops index for column `ped_godina`
        $this->dropIndex(
            '{{%idx-dokument_template-ped_godina}}',
            '{{%dokument_template}}'
        );

        $this->dropTable('{{%dokument_template}}');
    }
}
