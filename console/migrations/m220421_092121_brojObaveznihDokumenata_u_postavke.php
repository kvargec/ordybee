<?php

use yii\db\Migration;

/**
 * Class m220421_092121_brojObaveznihDokumenata_u_postavke
 */
class m220421_092121_brojObaveznihDokumenata_u_postavke extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('postavke', ['postavka' => 'brojObaveznihDokumenata', 'vrijednost' => 0]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('postavke', ['postavka' => 'brojObaveznihDokumenata']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220421_092121_brojObaveznihDokumenata_u_postavke cannot be reverted.\n";

        return false;
    }
    */
}
