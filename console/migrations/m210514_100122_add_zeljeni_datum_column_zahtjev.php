<?php

use yii\db\Migration;

/**
 * Class m210514_100122_add_zeljeni_datum_column_zahtjev
 */
class m210514_100122_add_zeljeni_datum_column_zahtjev extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('zahtjev','zeljeni_datum',$this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210514_100122_add_zeljeni_datum_column_zahtjev cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210514_100122_add_zeljeni_datum_column_zahtjev cannot be reverted.\n";

        return false;
    }
    */
}
