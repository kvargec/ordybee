<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document}}`.
 */
class m200408_194948_create_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%document}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(150),
            'filename' => $this->string(250),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%document}}');
    }
}
