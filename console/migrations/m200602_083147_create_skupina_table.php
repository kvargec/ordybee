<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%skupina}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%pedagoska_godina}}`
 */
class m200602_083147_create_skupina_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%skupina}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(150)->notNull(),
            'sifra' => $this->string(20),
            'slika' => $this->string(255),
            'ped_godina' => $this->integer(),
        ]);

        // creates index for column `ped_godina`
        $this->createIndex(
            '{{%idx-skupina-ped_godina}}',
            '{{%skupina}}',
            'ped_godina'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-skupina-ped_godina}}',
            '{{%skupina}}',
            'ped_godina',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-skupina-ped_godina}}',
            '{{%skupina}}'
        );

        // drops index for column `ped_godina`
        $this->dropIndex(
            '{{%idx-skupina-ped_godina}}',
            '{{%skupina}}'
        );

        $this->dropTable('{{%skupina}}');
    }
}
