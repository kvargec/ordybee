<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zahtjev_kriterij}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%zahtjev}}`
 * - `{{%kriterij}}`
 */
class m200430_100707_create_zahtjev_kriterij_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zahtjev_kriterij}}', [
            'id' => $this->primaryKey(),
            'zahtjev_id' => $this->integer()->notNull(),
            'kriterij_id' => $this->integer()->notNull(),
            'zadovoljen' => $this->boolean()->notNull(),
        ]);

        // creates index for column `zahtjev_id`
        $this->createIndex(
            '{{%idx-zahtjev_kriterij-zahtjev_id}}',
            '{{%zahtjev_kriterij}}',
            'zahtjev_id'
        );

        // add foreign key for table `{{%zahtjev}}`
        $this->addForeignKey(
            '{{%fk-zahtjev_kriterij-zahtjev_id}}',
            '{{%zahtjev_kriterij}}',
            'zahtjev_id',
            '{{%zahtjev}}',
            'id',
            'CASCADE'
        );

        // creates index for column `kriterij_id`
        $this->createIndex(
            '{{%idx-zahtjev_kriterij-kriterij_id}}',
            '{{%zahtjev_kriterij}}',
            'kriterij_id'
        );

        // add foreign key for table `{{%kriterij}}`
        $this->addForeignKey(
            '{{%fk-zahtjev_kriterij-kriterij_id}}',
            '{{%zahtjev_kriterij}}',
            'kriterij_id',
            '{{%kriterij}}',
            'id',
            'CASCADE'
        );

        $this->createIndex('{{%idx-zahtjev_kriterij-secondary_key}}', '{{%zahtjev_kriterij}}', ['zahtjev_id', 'kriterij_id'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            '{{%idx-zahtjev_kriterij-secondary_key}}',
            '{{%zahtjev_kriterij}}'
        );

        // drops foreign key for table `{{%zahtjev}}`
        $this->dropForeignKey(
            '{{%fk-zahtjev_kriterij-zahtjev_id}}',
            '{{%zahtjev_kriterij}}'
        );

        // drops index for column `zahtjev_id`
        $this->dropIndex(
            '{{%idx-zahtjev_kriterij-zahtjev_id}}',
            '{{%zahtjev_kriterij}}'
        );

        // drops foreign key for table `{{%kriterij}}`
        $this->dropForeignKey(
            '{{%fk-zahtjev_kriterij-kriterij_id}}',
            '{{%zahtjev_kriterij}}'
        );

        // drops index for column `kriterij_id`
        $this->dropIndex(
            '{{%idx-zahtjev_kriterij-kriterij_id}}',
            '{{%zahtjev_kriterij}}'
        );

        $this->dropTable('{{%zahtjev_kriterij}}');
    }
}
