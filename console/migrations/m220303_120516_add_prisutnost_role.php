<?php

use yii\db\Migration;

/**
 * Class m220303_120516_add_prisutnost_role
 */
class m220303_120516_add_prisutnost_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    { {
            $auth = Yii::$app->authManager;
            $rehabRole = $auth->getRole('prisutnost');
            // create a role named "odgajatelj"
            if (!isset($rehabRole)) {
                $rehabRole = $auth->createRole('prisutnost');
                $rehabRole->description = 'Prisutnost';
                $auth->add($rehabRole); // create permission for certain tasks
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220303_120516_add_prisutnost_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220303_120516_add_prisutnost_role cannot be reverted.\n";

        return false;
    }
    */
}
