<?php

use yii\db\Migration;

/**
 * Class m211105_102020_add_row_to_postavke_table
 */
class m211105_102020_add_row_to_postavke_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('postavke', [
            'postavka' => 'prisutnostType',
            'vrijednost' => 'simple',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('postavke', ['postavka' => 'prisutnostType']);

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211105_102020_add_row_to_postavke_table cannot be reverted.\n";

        return false;
    }
    */
}
