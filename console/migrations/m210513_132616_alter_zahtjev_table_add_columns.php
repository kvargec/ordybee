<?php

use yii\db\Migration;

/**
 * Class m210513_132616_alter_zahtjev_table_add_columns
 */
class m210513_132616_alter_zahtjev_table_add_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('zahtjev', 'posjeduje_pravo', $this->boolean());
        $this->addColumn('zahtjev', 'pravo_pocetak', $this->date());
        $this->addColumn('zahtjev', 'pravo_kraj', $this->date());
        $this->addColumn('zahtjev', 'pravo_naziv', $this->string());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('zahtjev', 'posjeduje_pravo');
        $this->dropColumn('zahtjev', 'pravo_pocetak');
        $this->dropColumn('zahtjev', 'pravo_kraj');
        $this->dropColumn('zahtjev', 'pravo_naziv');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210513_132616_alter_zahtjev_table_add_columns cannot be reverted.\n";

        return false;
    }
    */
}
