<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tvrtka}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%mjesto}}`
 * - `{{%status_opce}}`
 */
class m210302_201451_create_tvrtka_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tvrtka}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(250),
            'sifra' => $this->string(30),
            'oib' => $this->string(13),
            'adresa' => $this->string(150),
            'mjesto' => $this->integer(),
            'status' => $this->integer(),
            'djelatnost' => $this->string(250),
        ]);

        // creates index for column `mjesto`
        $this->createIndex(
            '{{%idx-tvrtka-mjesto}}',
            '{{%tvrtka}}',
            'mjesto'
        );

        // add foreign key for table `{{%mjesto}}`
        $this->addForeignKey(
            '{{%fk-tvrtka-mjesto}}',
            '{{%tvrtka}}',
            'mjesto',
            '{{%mjesto}}',
            'id',
            'CASCADE'
        );

        // creates index for column `status`
        $this->createIndex(
            '{{%idx-tvrtka-status}}',
            '{{%tvrtka}}',
            'status'
        );

        // add foreign key for table `{{%status_opce}}`
        $this->addForeignKey(
            '{{%fk-tvrtka-status}}',
            '{{%tvrtka}}',
            'status',
            '{{%status_opce}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%mjesto}}`
        $this->dropForeignKey(
            '{{%fk-tvrtka-mjesto}}',
            '{{%tvrtka}}'
        );

        // drops index for column `mjesto`
        $this->dropIndex(
            '{{%idx-tvrtka-mjesto}}',
            '{{%tvrtka}}'
        );

        // drops foreign key for table `{{%status_opce}}`
        $this->dropForeignKey(
            '{{%fk-tvrtka-status}}',
            '{{%tvrtka}}'
        );

        // drops index for column `status`
        $this->dropIndex(
            '{{%idx-tvrtka-status}}',
            '{{%tvrtka}}'
        );

        $this->dropTable('{{%tvrtka}}');
    }
}
