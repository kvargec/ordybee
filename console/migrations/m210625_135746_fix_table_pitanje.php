<?php

use yii\db\Migration;

/**
 * Class m210625_135746_fix_table_pitanje
 */
class m210625_135746_fix_table_pitanje extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('pitanje',['odgovori' => '{"0": "zvuk;", "1": "dodir;", "2": "miris;", "3": "svjetlosne promjene;", "4": "okus;", "5": "vrtnju, ljuljanje;", "6": "drugo"}'], ['pitanje'=>'Pokazuje li dijete preosjetljivost na podražaje iz okoline (molimo označite kvadratiće ispred svega što se odnosi na vaše dijete):']);
        $this->update('pitanje',['odgovori' => '{"0": "dijete je spretno u kretanju;", "1": "nespretno je (češće pada, spotiče se, sudara s predmetima);", "2": "sklono je povredama;", "3": "pojačano je motorno aktivno (živahno);", "4": "hoda na prstima;", "5": "ne voli se kretati;"}'], ['pitanje'=>'Kakva je motorika vašeg djeteta (molimo označite kvadratiće ispred svega što se odnosi na vaše dijete):']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210625_135746_fix_table_pitanje cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210625_135746_fix_table_pitanje cannot be reverted.\n";

        return false;
    }
    */
}
