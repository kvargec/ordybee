<?php

use yii\db\Migration;

/**
 * Class m201007_142840_prosiri_vrste_zapisa
 */
class m201007_142840_prosiri_vrste_zapisa extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('vrsta_zapis','grupa',$this->string(100));
        $this->insert('vrsta_zapis',['naziv'=>'standardni','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'cijepljenje','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'cijepljenje 5u1','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'cijepljenje 6u1','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'bolest','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'bolest zarazna','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'osnovni podaci','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'kronična bolest','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'alergija','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'poteškoće','grupa'=>'zdravstvo']);
        $this->insert('vrsta_zapis',['naziv'=>'mjerenje','grupa'=>'zdravstvo']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201007_142840_prosiri_vrste_zapisa cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201007_142840_prosiri_vrste_zapisa cannot be reverted.\n";

        return false;
    }
    */
}
