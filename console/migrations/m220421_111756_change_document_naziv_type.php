<?php

use yii\db\Migration;

/**
 * Class m220421_111756_change_document_naziv_type
 */
class m220421_111756_change_document_naziv_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('document', 'naziv', 'text');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('document', 'naziv', 'varchar(150)');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220421_111756_change_document_naziv_type cannot be reverted.\n";

        return false;
    }
    */
}
