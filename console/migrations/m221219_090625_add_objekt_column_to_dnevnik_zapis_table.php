<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%dnevnik_zapis}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%objekt}}`
 */
class m221219_090625_add_objekt_column_to_dnevnik_zapis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%dnevnik_zapis}}', 'objekt', $this->integer());

        // creates index for column `objekt`
        $this->createIndex(
            '{{%idx-dnevnik_zapis-objekt}}',
            '{{%dnevnik_zapis}}',
            'objekt'
        );

        // add foreign key for table `{{%objekt}}`
        $this->addForeignKey(
            '{{%fk-dnevnik_zapis-objekt}}',
            '{{%dnevnik_zapis}}',
            'objekt',
            '{{%objekt}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%objekt}}`
        $this->dropForeignKey(
            '{{%fk-dnevnik_zapis-objekt}}',
            '{{%dnevnik_zapis}}'
        );

        // drops index for column `objekt`
        $this->dropIndex(
            '{{%idx-dnevnik_zapis-objekt}}',
            '{{%dnevnik_zapis}}'
        );

        $this->dropColumn('{{%dnevnik_zapis}}', 'objekt');
    }
}
