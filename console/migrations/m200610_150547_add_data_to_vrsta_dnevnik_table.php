<?php

use yii\db\Migration;

/**
 * Class m200610_150547_add_data_to_vrsta_dnevnik_table
 */
class m200610_150547_add_data_to_vrsta_dnevnik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $db = Yii::$app->db;
        $sql = 'DELETE FROM public.dokument_template;';
        $command = $db->createCommand($sql);
        $command->execute();

        $str = file_get_contents(__DIR__ . '/data/vrste_dnevnika.txt');
        $lines = explode(PHP_EOL, $str);
        $db = Yii::$app->db;
        foreach ($lines as $line) {
            if (!$line) {
                continue;
            }

            $line = str_replace('\N', 'null', $line);
            
            [$id, $naziv, $postavke, $status] = explode("\t", $line);
            $postavke = str_replace('\r\n', '\n', $postavke);
            $sql = "INSERT INTO public.vrsta_dnevnik(id, naziv, postavke, status)
                    VALUES ($id, '$naziv', E'$postavke', $status);";
            $command = $db->createCommand($sql);
            $command->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $db = Yii::$app->db;
        $sql = 'DELETE FROM public.vrsta_dnevnik;';
        $command = $db->createCommand($sql);
        $command->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200610_150547_add_data_to_vrsta_dnevnik_table cannot be reverted.\n";

        return false;
    }
    */
}
