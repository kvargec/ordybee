<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%stavke_narudjba}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%ulazni_racun}}`
 * - `{{%proizvod}}`
 */
class m210326_204731_create_stavke_narudjba_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%stavke_narudjba}}', [
            'id' => $this->primaryKey(),
            'ulazni_racun' => $this->integer(),
            'proizvod' => $this->integer(),
            'kolicina' => $this->float(),
        ]);

        // creates index for column `ulazni_racun`
        $this->createIndex(
            '{{%idx-stavke_narudjba-ulazni_racun}}',
            '{{%stavke_narudjba}}',
            'ulazni_racun'
        );

        // add foreign key for table `{{%ulazni_racun}}`
        $this->addForeignKey(
            '{{%fk-stavke_narudjba-ulazni_racun}}',
            '{{%stavke_narudjba}}',
            'ulazni_racun',
            '{{%ulazni_racun}}',
            'id',
            'CASCADE'
        );

        // creates index for column `proizvod`
        $this->createIndex(
            '{{%idx-stavke_narudjba-proizvod}}',
            '{{%stavke_narudjba}}',
            'proizvod'
        );

        // add foreign key for table `{{%proizvod}}`
        $this->addForeignKey(
            '{{%fk-stavke_narudjba-proizvod}}',
            '{{%stavke_narudjba}}',
            'proizvod',
            '{{%proizvod}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%ulazni_racun}}`
        $this->dropForeignKey(
            '{{%fk-stavke_narudjba-ulazni_racun}}',
            '{{%stavke_narudjba}}'
        );

        // drops index for column `ulazni_racun`
        $this->dropIndex(
            '{{%idx-stavke_narudjba-ulazni_racun}}',
            '{{%stavke_narudjba}}'
        );

        // drops foreign key for table `{{%proizvod}}`
        $this->dropForeignKey(
            '{{%fk-stavke_narudjba-proizvod}}',
            '{{%stavke_narudjba}}'
        );

        // drops index for column `proizvod`
        $this->dropIndex(
            '{{%idx-stavke_narudjba-proizvod}}',
            '{{%stavke_narudjba}}'
        );

        $this->dropTable('{{%stavke_narudjba}}');
    }
}
