<?php

use yii\db\Migration;

/**
 * Class m221216_100429_add_data_to_vrsta_obroka_table
 */
class m221216_100429_add_data_to_vrste_obroka_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('vrste_obroka', ['vrsta', 'cijena'], [
            ['Doručak', 0],
            ['Ručak', 0],
            ['Večera', 0],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('vrste_obroka', ['vrsta' => 'Doručak']);
        $this->delete('vrste_obroka', ['vrsta' => 'Ručak']);
        $this->delete('vrste_obroka', ['vrsta' => 'Večera']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221216_100429_add_data_to_vrsta_obroka_table cannot be reverted.\n";

        return false;
    }
    */
}
