<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zahtjev}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%pedagoska_godina}}`
 */
class m210422_055244_add_pd_godina_column_to_zahtjev_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zahtjev}}', 'pd_godina', $this->integer());

        // creates index for column `pd_godina`
        $this->createIndex(
            '{{%idx-zahtjev-pd_godina}}',
            '{{%zahtjev}}',
            'pd_godina'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-zahtjev-pd_godina}}',
            '{{%zahtjev}}',
            'pd_godina',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-zahtjev-pd_godina}}',
            '{{%zahtjev}}'
        );

        // drops index for column `pd_godina`
        $this->dropIndex(
            '{{%idx-zahtjev-pd_godina}}',
            '{{%zahtjev}}'
        );

        $this->dropColumn('{{%zahtjev}}', 'pd_godina');
    }
}
