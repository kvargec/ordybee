<?php

use yii\db\Migration;

/**
 * Class m201020_145419_add_vrsta_zapis
 */
class m201020_145419_add_vrsta_zapis extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('vrsta_zapis', ['naziv','grupa'], [
            ['observacija', 'logoped'],
            ['nalaz', 'logoped'],
            ['vježbe', 'logoped'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('vrsta_zapis',['grupa'=>'logoped']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201020_145419_add_vrsta_zapis cannot be reverted.\n";

        return false;
    }
    */
}
