<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%odg_skupina}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%zaposlenik}}`
 * - `{{%skupina}}`
 * - `{{%status_opce}}`
 */
class m210328_183528_create_odg_skupina_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%odg_skupina}}', [
            'id' => $this->primaryKey(),
            'odgajatelj' => $this->integer(),
            'skupina' => $this->integer(),
            'date_from' => $this->timestamp(),
            'date_to' => $this->timestamp(),
            'status' => $this->integer(),
        ]);

        // creates index for column `odgajatelj`
        $this->createIndex(
            '{{%idx-odg_skupina-odgajatelj}}',
            '{{%odg_skupina}}',
            'odgajatelj'
        );

        // add foreign key for table `{{%zaposlenik}}`
        $this->addForeignKey(
            '{{%fk-odg_skupina-odgajatelj}}',
            '{{%odg_skupina}}',
            'odgajatelj',
            '{{%zaposlenik}}',
            'id',
            'CASCADE'
        );

        // creates index for column `skupina`
        $this->createIndex(
            '{{%idx-odg_skupina-skupina}}',
            '{{%odg_skupina}}',
            'skupina'
        );

        // add foreign key for table `{{%skupina}}`
        $this->addForeignKey(
            '{{%fk-odg_skupina-skupina}}',
            '{{%odg_skupina}}',
            'skupina',
            '{{%skupina}}',
            'id',
            'CASCADE'
        );

        // creates index for column `status`
        $this->createIndex(
            '{{%idx-odg_skupina-status}}',
            '{{%odg_skupina}}',
            'status'
        );

        // add foreign key for table `{{%status_opce}}`
        $this->addForeignKey(
            '{{%fk-odg_skupina-status}}',
            '{{%odg_skupina}}',
            'status',
            '{{%status_opce}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%zaposlenik}}`
        $this->dropForeignKey(
            '{{%fk-odg_skupina-odgajatelj}}',
            '{{%odg_skupina}}'
        );

        // drops index for column `odgajatelj`
        $this->dropIndex(
            '{{%idx-odg_skupina-odgajatelj}}',
            '{{%odg_skupina}}'
        );

        // drops foreign key for table `{{%skupina}}`
        $this->dropForeignKey(
            '{{%fk-odg_skupina-skupina}}',
            '{{%odg_skupina}}'
        );

        // drops index for column `skupina`
        $this->dropIndex(
            '{{%idx-odg_skupina-skupina}}',
            '{{%odg_skupina}}'
        );

        // drops foreign key for table `{{%status_opce}}`
        $this->dropForeignKey(
            '{{%fk-odg_skupina-status}}',
            '{{%odg_skupina}}'
        );

        // drops index for column `status`
        $this->dropIndex(
            '{{%idx-odg_skupina-status}}',
            '{{%odg_skupina}}'
        );

        $this->dropTable('{{%odg_skupina}}');
    }
}
