<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zaposlenik}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%mjesto}}`
 * - `{{%sprema}}`
 */
class m200602_083152_create_zaposlenik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zaposlenik}}', [
            'id' => $this->primaryKey(),
            'ime' => $this->string(150),
            'prezime' => $this->string(150),
            'adresa' => $this->string(200),
            'mjesto' => $this->integer(),
            'oib' => $this->string(11),
            'dat_rodjenja' => $this->date(),
            'sprema' => $this->integer(),
            'spol' => $this->string(2),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);

        // creates index for column `mjesto`
        $this->createIndex(
            '{{%idx-zaposlenik-mjesto}}',
            '{{%zaposlenik}}',
            'mjesto'
        );

        // add foreign key for table `{{%mjesto}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik-mjesto}}',
            '{{%zaposlenik}}',
            'mjesto',
            '{{%mjesto}}',
            'id',
            'CASCADE'
        );

        // creates index for column `sprema`
        $this->createIndex(
            '{{%idx-zaposlenik-sprema}}',
            '{{%zaposlenik}}',
            'sprema'
        );

        // add foreign key for table `{{%sprema}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik-sprema}}',
            '{{%zaposlenik}}',
            'sprema',
            '{{%sprema}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%mjesto}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik-mjesto}}',
            '{{%zaposlenik}}'
        );

        // drops index for column `mjesto`
        $this->dropIndex(
            '{{%idx-zaposlenik-mjesto}}',
            '{{%zaposlenik}}'
        );

        // drops foreign key for table `{{%sprema}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik-sprema}}',
            '{{%zaposlenik}}'
        );

        // drops index for column `sprema`
        $this->dropIndex(
            '{{%idx-zaposlenik-sprema}}',
            '{{%zaposlenik}}'
        );

        $this->dropTable('{{%zaposlenik}}');
    }
}
