<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zaposlenik_kontakt}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%zaposlenik}}`
 * - `{{%kontakt}}`
 */
class m200602_083155_create_zaposlenik_kontakt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zaposlenik_kontakt}}', [
            'id' => $this->primaryKey(),
            'zaposlenik' => $this->integer(),
            'kontakt' => $this->integer(),
        ]);

        // creates index for column `zaposlenik`
        $this->createIndex(
            '{{%idx-zaposlenik_kontakt-zaposlenik}}',
            '{{%zaposlenik_kontakt}}',
            'zaposlenik'
        );

        // add foreign key for table `{{%zaposlenik}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik_kontakt-zaposlenik}}',
            '{{%zaposlenik_kontakt}}',
            'zaposlenik',
            '{{%zaposlenik}}',
            'id',
            'CASCADE'
        );

        // creates index for column `kontakt`
        $this->createIndex(
            '{{%idx-zaposlenik_kontakt-kontakt}}',
            '{{%zaposlenik_kontakt}}',
            'kontakt'
        );

        // add foreign key for table `{{%kontakt}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik_kontakt-kontakt}}',
            '{{%zaposlenik_kontakt}}',
            'kontakt',
            '{{%kontakt}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%zaposlenik}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik_kontakt-zaposlenik}}',
            '{{%zaposlenik_kontakt}}'
        );

        // drops index for column `zaposlenik`
        $this->dropIndex(
            '{{%idx-zaposlenik_kontakt-zaposlenik}}',
            '{{%zaposlenik_kontakt}}'
        );

        // drops foreign key for table `{{%kontakt}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik_kontakt-kontakt}}',
            '{{%zaposlenik_kontakt}}'
        );

        // drops index for column `kontakt`
        $this->dropIndex(
            '{{%idx-zaposlenik_kontakt-kontakt}}',
            '{{%zaposlenik_kontakt}}'
        );

        $this->dropTable('{{%zaposlenik_kontakt}}');
    }
}
