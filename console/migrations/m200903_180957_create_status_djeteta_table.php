<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%status_djeteta}}`.
 */
class m200903_180957_create_status_djeteta_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%status_djeteta}}', [
            'id' => $this->primaryKey(),
            'status' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%status_djeteta}}');
    }
}
