<?php

use yii\db\Migration;

/**
 * Class m210322_182639_dodaj_vrste_poruke
 */
class m210322_182639_dodaj_vrste_poruke extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('vrste_poruke', ['vrsta'],[
            ['Obavijest'],
            ['Zahtjev'],
            ['Odluka'],
            ['Informacija']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210322_182639_dodaj_vrste_poruke cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210322_182639_dodaj_vrste_poruke cannot be reverted.\n";

        return false;
    }
    */
}
