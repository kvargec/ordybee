<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%intervju}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%zahtjev}}`
 */
class m200525_075408_create_intervju_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%intervju}}', [
            'id' => $this->primaryKey(),
            'zahtjev' => $this->integer(),
            'vrijeme' => $this->timestamp(),
            'lokacija' => $this->string(150),
            'napomena' => $this->text(),
            'odrzan' => $this->boolean()->defaultValue(false),
        ]);

        // creates index for column `zahtjev`
        $this->createIndex(
            '{{%idx-intervju-zahtjev}}',
            '{{%intervju}}',
            'zahtjev'
        );

        // add foreign key for table `{{%zahtjev}}`
        $this->addForeignKey(
            '{{%fk-intervju-zahtjev}}',
            '{{%intervju}}',
            'zahtjev',
            '{{%zahtjev}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%zahtjev}}`
        $this->dropForeignKey(
            '{{%fk-intervju-zahtjev}}',
            '{{%intervju}}'
        );

        // drops index for column `zahtjev`
        $this->dropIndex(
            '{{%idx-intervju-zahtjev}}',
            '{{%intervju}}'
        );

        $this->dropTable('{{%intervju}}');
    }
}
