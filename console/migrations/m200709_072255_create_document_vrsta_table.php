<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%document_vrsta}}`.
 */
class m200709_072255_create_document_vrsta_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%document_vrsta}}', [
            'id' => $this->primaryKey(),
            'vrsta' => $this->string(),
            'opis' => $this->string(),
        ]);
        $this->addColumn('{{%document}}', 'vrsta_dokumenta', $this->integer());
        $this->createIndex(
            '{{%idx-document_vrsta}}',
            '{{%document}}',
            'vrsta_dokumenta'
        );

        // add foreign key for table `{{%dnevnik}}`
        $this->addForeignKey(
            '{{%fk-document_vrsta}}',
            '{{%document}}',
            'vrsta_dokumenta',
            '{{%document_vrsta}}',
            'id',
            'CASCADE'
        );
        $data2=[
            ['upload','Korisnički upload'],
            ['rjesenjeupis','Rješenja za upis'],
            ['ugovorupis','Ugovor za upis'],
            ['odlukaupis','Odluka o upisu'],

        ];
        $this->batchInsert('document_vrsta',['vrsta','opis'],$data2);
        $db = Yii::$app->db;
        $sql = "UPDATE document SET vrsta_dokumenta=1";
        $command = $db->createCommand($sql);
        $command->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%document_vrsta}}');
    }
}
