<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%vrsta_zapis}}`.
 */
class m210408_094547_add_title_column_to_vrsta_zapis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%vrsta_zapis}}', 'title', $this->string(255));
        $this->addColumn('{{%vrsta_zapis}}', 'redoslijed', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%vrsta_zapis}}', 'title');
        $this->dropColumn('{{%vrsta_zapis}}', 'redoslijed');
    }
}
