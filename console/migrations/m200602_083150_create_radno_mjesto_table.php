<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%radno_mjesto}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%sprema}}`
 */
class m200602_083150_create_radno_mjesto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%radno_mjesto}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(100),
            'sprema' => $this->integer(),
            'koeficijent' => $this->float(),
            'sifra' => $this->string(20),
        ]);

        // creates index for column `sprema`
        $this->createIndex(
            '{{%idx-radno_mjesto-sprema}}',
            '{{%radno_mjesto}}',
            'sprema'
        );

        // add foreign key for table `{{%sprema}}`
        $this->addForeignKey(
            '{{%fk-radno_mjesto-sprema}}',
            '{{%radno_mjesto}}',
            'sprema',
            '{{%sprema}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%sprema}}`
        $this->dropForeignKey(
            '{{%fk-radno_mjesto-sprema}}',
            '{{%radno_mjesto}}'
        );

        // drops index for column `sprema`
        $this->dropIndex(
            '{{%idx-radno_mjesto-sprema}}',
            '{{%radno_mjesto}}'
        );

        $this->dropTable('{{%radno_mjesto}}');
    }
}
