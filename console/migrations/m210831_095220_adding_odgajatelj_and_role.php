<?php

use yii\db\Migration;

/**
 * Class m210831_095220_adding_odgajatelj_and_role
 */
class m210831_095220_adding_odgajatelj_and_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $odgajateljRole = $auth->getRole('odgajatelj');

        // create user "admin" with password "verysecret"
        $checkUser = \Da\User\Model\User::find()->where(['username'=>'odgajatelj'])->one();
        if (!isset($checkUser)) {
            $user = new \Da\User\Model\User([
                'scenario' => 'create', 
                'email' => "odgajatelj@nema.com", 
                'username' => "odgajatelj", 
                'password' => "12345678"  // >6 characters!
            ]);
            $user->confirmed_at = time();
            $user->save();
			$auth->assign($odgajateljRole, $user->id);
        }else{
			$auth->assign($odgajateljRole, $checkUser->id);
		}
        // assign role to our admin-user

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210831_095220_adding_odgajatelj_and_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210831_095220_adding_odgajatelj_and_role cannot be reverted.\n";

        return false;
    }
    */
}
