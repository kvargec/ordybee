<?php

use yii\db\Migration;

/**
 * Class m210625_145013_new_fix_table_pitanje_2
 */
class m210625_145013_new_fix_table_pitanje_2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('pitanje',['odgovori' => '{"0": "da", "1": "djelomično", "2": "ne"}',], ['pitanje'=>'Samostalnost u odijevanju – oblačenje:']);
        $this->update('pitanje',['odgovori' => '{"0": "da", "1": "djelomično", "2": "ne"}',], ['pitanje'=>'Samostalnost u odijevanju – Svlačenje:']);
        $this->update('pitanje',['odgovori' => '{"0": "da", "1": "djelomično", "2": "ne"}',], ['pitanje'=>'Samostalnost u odijevanju – Obuvanje:']);
        $this->update('pitanje',['odgovori' => '{"0": "da", "1": "djelomično", "2": "ne"}','vrsta_pitanja'=>1], ['pitanje'=>'Samostalnost u odijevanju – Izuvanje:']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210625_145013_new_fix_table_pitanje_2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210625_145013_new_fix_table_pitanje_2 cannot be reverted.\n";

        return false;
    }
    */
}
