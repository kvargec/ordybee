<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrsta_pitanja}}`.
 */
class m210324_081400_create_vrsta_pitanja_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrsta_pitanja}}', [
            'id' => $this->primaryKey(),
            'vrsta_pitanje' => $this->string(250),
            'opis' => $this->text(),
            'tip' => $this->string(250),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrsta_pitanja}}');
    }
}
