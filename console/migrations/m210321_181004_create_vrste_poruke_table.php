<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrste_poruke}}`.
 */
class m210321_181004_create_vrste_poruke_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrste_poruke}}', [
            'id' => $this->primaryKey(),
            'vrsta' => $this->string(150),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrste_poruke}}');
    }
}
