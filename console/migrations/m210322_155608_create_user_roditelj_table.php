<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_roditelj}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%roditelj}}`
 */
class m210322_155608_create_user_roditelj_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_roditelj}}', [
            'id' => $this->primaryKey(),
            'user' => $this->integer(),
            'roditelj' => $this->integer(),
            'username' => $this->string(150),
            'password' => $this->string(150),
        ]);

        // creates index for column `user`
        $this->createIndex(
            '{{%idx-user_roditelj-user}}',
            '{{%user_roditelj}}',
            'user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_roditelj-user}}',
            '{{%user_roditelj}}',
            'user',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `roditelj`
        $this->createIndex(
            '{{%idx-user_roditelj-roditelj}}',
            '{{%user_roditelj}}',
            'roditelj'
        );

        // add foreign key for table `{{%roditelj}}`
        $this->addForeignKey(
            '{{%fk-user_roditelj-roditelj}}',
            '{{%user_roditelj}}',
            'roditelj',
            '{{%roditelj}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_roditelj-user}}',
            '{{%user_roditelj}}'
        );

        // drops index for column `user`
        $this->dropIndex(
            '{{%idx-user_roditelj-user}}',
            '{{%user_roditelj}}'
        );

        // drops foreign key for table `{{%roditelj}}`
        $this->dropForeignKey(
            '{{%fk-user_roditelj-roditelj}}',
            '{{%user_roditelj}}'
        );

        // drops index for column `roditelj`
        $this->dropIndex(
            '{{%idx-user_roditelj-roditelj}}',
            '{{%user_roditelj}}'
        );

        $this->dropTable('{{%user_roditelj}}');
    }
}
