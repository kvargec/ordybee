<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%media}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%media_type}}`
 * - `{{%user}}`
 */
class m210328_180014_create_media_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%media}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(100),
            'opis' => $this->text(),
            'media_type' => $this->integer(),
            'size' => $this->integer(),
            'filename' => $this->string(255),
            'created_at' => $this->timestamp(),
            'author' => $this->string(255),
            'user_id' => $this->integer(),
        ]);

        // creates index for column `media_type`
        $this->createIndex(
            '{{%idx-media-media_type}}',
            '{{%media}}',
            'media_type'
        );

        // add foreign key for table `{{%media_type}}`
        $this->addForeignKey(
            '{{%fk-media-media_type}}',
            '{{%media}}',
            'media_type',
            '{{%media_type}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-media-user_id}}',
            '{{%media}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-media-user_id}}',
            '{{%media}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%media_type}}`
        $this->dropForeignKey(
            '{{%fk-media-media_type}}',
            '{{%media}}'
        );

        // drops index for column `media_type`
        $this->dropIndex(
            '{{%idx-media-media_type}}',
            '{{%media}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-media-user_id}}',
            '{{%media}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-media-user_id}}',
            '{{%media}}'
        );

        $this->dropTable('{{%media}}');
    }
}
