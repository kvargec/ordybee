<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%roditelj}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%dijete}}`
 */
class m200408_194433_create_roditelj_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%roditelj}}', [
            'id' => $this->primaryKey(),
            'ime' => $this->string(150)->notNull(),
            'prezime' => $this->string(150)->notNull(),
            'spol' => $this->string(1),
            'dat_rod' => $this->date(),
            'oib' => $this->string(13),
            'adresa' => $this->string(150),
            'mjesto' => $this->string(150),
            'prebivaliste' => $this->string(200),
            'radno' => $this->string(200),
            'zanimanje' => $this->string(200),
            'poslodavaca' => $this->string(200),
            'mobitel' => $this->string(50),
            'email' => $this->string(50),
            'dijete' => $this->integer(),
        ]);

        // creates index for column `dijete`
        $this->createIndex(
            '{{%idx-roditelj-dijete}}',
            '{{%roditelj}}',
            'dijete'
        );

        // add foreign key for table `{{%dijete}}`
        $this->addForeignKey(
            '{{%fk-roditelj-dijete}}',
            '{{%roditelj}}',
            'dijete',
            '{{%dijete}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%dijete}}`
        $this->dropForeignKey(
            '{{%fk-roditelj-dijete}}',
            '{{%roditelj}}'
        );

        // drops index for column `dijete`
        $this->dropIndex(
            '{{%idx-roditelj-dijete}}',
            '{{%roditelj}}'
        );

        $this->dropTable('{{%roditelj}}');
    }
}
