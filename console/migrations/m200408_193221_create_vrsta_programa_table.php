<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrsta_programa}}`.
 */
class m200408_193221_create_vrsta_programa_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrsta_programa}}', [
            'id' => $this->primaryKey(),
            'vrsta'=>$this->string(50),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrsta_programa}}');
    }
}
