<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mailbox}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%poruke}}`
 * - `{{%mstatus}}`
 * - `{{%msg_folders}}`
 */
class m210321_184004_create_mailbox_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mailbox}}', [
            'id' => $this->primaryKey(),
            'user' => $this->integer(),
            'message' => $this->integer(),
            'created_at' => $this->timestamp(),
            'last_change_at' => $this->timestamp(),
            'mstatus' => $this->integer(),
            'folder' => $this->integer(),
        ]);

        // creates index for column `user`
        $this->createIndex(
            '{{%idx-mailbox-user}}',
            '{{%mailbox}}',
            'user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-mailbox-user}}',
            '{{%mailbox}}',
            'user',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `message`
        $this->createIndex(
            '{{%idx-mailbox-message}}',
            '{{%mailbox}}',
            'message'
        );

        // add foreign key for table `{{%poruke}}`
        $this->addForeignKey(
            '{{%fk-mailbox-message}}',
            '{{%mailbox}}',
            'message',
            '{{%poruke}}',
            'id',
            'CASCADE'
        );

        // creates index for column `mstatus`
        $this->createIndex(
            '{{%idx-mailbox-mstatus}}',
            '{{%mailbox}}',
            'mstatus'
        );

        // add foreign key for table `{{%mstatus}}`
        $this->addForeignKey(
            '{{%fk-mailbox-mstatus}}',
            '{{%mailbox}}',
            'mstatus',
            '{{%mstatus}}',
            'id',
            'CASCADE'
        );

        // creates index for column `folder`
        $this->createIndex(
            '{{%idx-mailbox-folder}}',
            '{{%mailbox}}',
            'folder'
        );

        // add foreign key for table `{{%msg_folders}}`
        $this->addForeignKey(
            '{{%fk-mailbox-folder}}',
            '{{%mailbox}}',
            'folder',
            '{{%msg_folders}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-mailbox-user}}',
            '{{%mailbox}}'
        );

        // drops index for column `user`
        $this->dropIndex(
            '{{%idx-mailbox-user}}',
            '{{%mailbox}}'
        );

        // drops foreign key for table `{{%poruke}}`
        $this->dropForeignKey(
            '{{%fk-mailbox-message}}',
            '{{%mailbox}}'
        );

        // drops index for column `message`
        $this->dropIndex(
            '{{%idx-mailbox-message}}',
            '{{%mailbox}}'
        );

        // drops foreign key for table `{{%mstatus}}`
        $this->dropForeignKey(
            '{{%fk-mailbox-mstatus}}',
            '{{%mailbox}}'
        );

        // drops index for column `mstatus`
        $this->dropIndex(
            '{{%idx-mailbox-mstatus}}',
            '{{%mailbox}}'
        );

        // drops foreign key for table `{{%msg_folders}}`
        $this->dropForeignKey(
            '{{%fk-mailbox-folder}}',
            '{{%mailbox}}'
        );

        // drops index for column `folder`
        $this->dropIndex(
            '{{%idx-mailbox-folder}}',
            '{{%mailbox}}'
        );

        $this->dropTable('{{%mailbox}}');
    }
}
