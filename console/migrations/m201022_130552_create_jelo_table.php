<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%jelo}}`.
 */
class m201022_130552_create_jelo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%jelo}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(200),
            'podaci' => $this->json(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%jelo}}');
    }
}
