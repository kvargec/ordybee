<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%postavke}}`.
 */
class m200604_090540_create_postavke_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%postavke}}', [
            'id' => $this->primaryKey(),
            'postavka' => $this->string(100),
            'vrijednost' => $this->string(250),
            'dodatno' => $this->json(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%postavke}}');
    }
}
