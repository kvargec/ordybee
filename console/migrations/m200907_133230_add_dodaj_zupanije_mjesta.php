<?php

use yii\db\Migration;

/**
 * Class m200907_133230_add_dodaj_zupanije_mjesta
 */
class m200907_133230_add_dodaj_zupanije_mjesta extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $db = Yii::$app->db;


        $str = file_get_contents(__DIR__ . '/data/zupanije.txt');
        $lines = explode(PHP_EOL, $str);
        $db = Yii::$app->db;
        foreach ($lines as $line) {
            if (!$line) {
                continue;
            }



            $zupi = explode(",", $line);

            $sql = "INSERT INTO public.zupanija(id, naziv)
                    VALUES ($zupi[1],'$zupi[2]');";
            $command = $db->createCommand($sql);
            $command->execute();

        }
        $this->insert('pedagoska_godina', [
            'od' => '2020-09-01',
            'do' => '2021-08-31',
            'aktivna' => true,
        ]);
        $this->insert('mjesto', ['naziv'=>'Vrbovec','zip'=>'10340','zupanija'=>1]);
        $this->insert('objekt', ['naziv'=>'Centalni objekt','adresa'=>'Ulica 7. svibnja 12a','mjesto'=>1,'sifra'=>'O-001','ped_godina'=>2]);
        $this->insert('objekt', ['naziv'=>'Dislocirani objekt 1','adresa'=>'Ulica poginulih branitelja bb','mjesto'=>1,'sifra'=>'O-002','ped_godina'=>2]);
        $this->insert('objekt', ['naziv'=>'Dislocirani objekt 2','adresa'=>'Ulica Augusta Šenoe 11','mjesto'=>1,'sifra'=>'O-003','ped_godina'=>2]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200907_133230_add_dodaj_zupanije_mjesta cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_133230_add_dodaj_zupanije_mjesta cannot be reverted.\n";

        return false;
    }
    */
}
