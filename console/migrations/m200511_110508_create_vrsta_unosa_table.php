<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrsta_unosa}}`.
 */
class m200511_110508_create_vrsta_unosa_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrsta_unosa}}', [
            'id' => $this->primaryKey(),
            'vrsta' => $this->string()->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrsta_unosa}}');
    }
}
