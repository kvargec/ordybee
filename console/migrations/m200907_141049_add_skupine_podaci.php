<?php

use yii\db\Migration;

/**
 * Class m200907_141049_add_skupine_podaci
 */
class m200907_141049_add_skupine_podaci extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('skupina',['naziv'=>'Sovice','sifra'=>'S-001-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>1,'objekt'=>1,'aktivna'=>1]);
        $this->insert('skupina',['naziv'=>'Bubamare','sifra'=>'S-002-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>2,'objekt'=>1,'aktivna'=>1]);
        $this->insert('skupina',['naziv'=>'Lavići','sifra'=>'S-003-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>3,'objekt'=>1,'aktivna'=>1]);
        $this->insert('skupina',['naziv'=>'Leptirići','sifra'=>'S-004-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>4,'objekt'=>1,'aktivna'=>1]);
        $this->insert('skupina',['naziv'=>'Pčelice','sifra'=>'S-005-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>5,'objekt'=>1,'aktivna'=>1]);
        $this->insert('skupina',['naziv'=>'Pandice','sifra'=>'S-006-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>6,'objekt'=>1,'aktivna'=>1]);

        $this->insert('skupina',['naziv'=>'Pužići','sifra'=>'S-007-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>7,'objekt'=>2,'aktivna'=>1]);
        $this->insert('skupina',['naziv'=>'Ribice','sifra'=>'S-008-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>8,'objekt'=>2,'aktivna'=>1]);
        $this->insert('skupina',['naziv'=>'Loptice','sifra'=>'S-009-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>9,'objekt'=>2,'aktivna'=>1]);

        $this->insert('skupina',['naziv'=>'Točkice','sifra'=>'S-010-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>10,'objekt'=>3,'aktivna'=>1]);
        $this->insert('skupina',['naziv'=>'Ružice','sifra'=>'S-011-2','ped_godina'=>2]);
        $this->insert('skupina_objekt',['skupina'=>11,'objekt'=>3,'aktivna'=>1]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200907_141049_add_skupine_podaci cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_141049_add_skupine_podaci cannot be reverted.\n";

        return false;
    }
    */
}
