<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cjenik}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%vrsta_programa}}`
 * - `{{%pedagoska_godina}}`
 */
class m210507_000106_create_cjenik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cjenik}}', [
            'id' => $this->primaryKey(),
            'program' => $this->integer(),
            'cijena' => $this->float(),
            'povlastena' => $this->float(),
            'ped_godina' => $this->integer(),
        ]);

        // creates index for column `program`
        $this->createIndex(
            '{{%idx-cjenik-program}}',
            '{{%cjenik}}',
            'program'
        );

        // add foreign key for table `{{%vrsta_programa}}`
        $this->addForeignKey(
            '{{%fk-cjenik-program}}',
            '{{%cjenik}}',
            'program',
            '{{%vrsta_programa}}',
            'id',
            'CASCADE'
        );

        // creates index for column `ped_godina`
        $this->createIndex(
            '{{%idx-cjenik-ped_godina}}',
            '{{%cjenik}}',
            'ped_godina'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-cjenik-ped_godina}}',
            '{{%cjenik}}',
            'ped_godina',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%vrsta_programa}}`
        $this->dropForeignKey(
            '{{%fk-cjenik-program}}',
            '{{%cjenik}}'
        );

        // drops index for column `program`
        $this->dropIndex(
            '{{%idx-cjenik-program}}',
            '{{%cjenik}}'
        );

        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-cjenik-ped_godina}}',
            '{{%cjenik}}'
        );

        // drops index for column `ped_godina`
        $this->dropIndex(
            '{{%idx-cjenik-ped_godina}}',
            '{{%cjenik}}'
        );

        $this->dropTable('{{%cjenik}}');
    }
}
