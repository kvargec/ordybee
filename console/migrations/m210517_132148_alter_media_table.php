<?php

use yii\db\Migration;

/**
 * Class m210517_132148_alter_media_table
 */
class m210517_132148_alter_media_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('media', 'attachments', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('media', 'attachments');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210517_132148_alter_media_table cannot be reverted.\n";

        return false;
    }
    */
}
