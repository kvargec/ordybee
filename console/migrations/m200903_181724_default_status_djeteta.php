<?php

use yii\db\Migration;

/**
 * Class m200903_181724_default_status_djeteta
 */
class m200903_181724_default_status_djeteta extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%dijete}}', 'status', $this->integer()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200903_181724_default_status_djeteta cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200903_181724_default_status_djeteta cannot be reverted.\n";

        return false;
    }
    */
}
