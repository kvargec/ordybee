<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dan_type}}`.
 */
class m201013_074232_create_dan_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dan_type}}', [
            'id' => $this->primaryKey(),
            'vrsta' => $this->string(100),
        ]);
        $this->insert('{{%dan_type}}',['vrsta'=>'radni dan']);
        $this->insert('{{%dan_type}}',['vrsta'=>'praznik']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dan_type}}');
    }
}
