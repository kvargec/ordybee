<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mjesto}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%zupanija}}`
 */
class m200602_082726_create_mjesto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mjesto}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(150)->notNull(),
            'zip' => $this->string(10),
            'zupanija' => $this->integer(),
        ]);

        // creates index for column `zupanija`
        $this->createIndex(
            '{{%idx-mjesto-zupanija}}',
            '{{%mjesto}}',
            'zupanija'
        );

        // add foreign key for table `{{%zupanija}}`
        $this->addForeignKey(
            '{{%fk-mjesto-zupanija}}',
            '{{%mjesto}}',
            'zupanija',
            '{{%zupanija}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%zupanija}}`
        $this->dropForeignKey(
            '{{%fk-mjesto-zupanija}}',
            '{{%mjesto}}'
        );

        // drops index for column `zupanija`
        $this->dropIndex(
            '{{%idx-mjesto-zupanija}}',
            '{{%mjesto}}'
        );

        $this->dropTable('{{%mjesto}}');
    }
}
