<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%upitnik}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%pedagoska_godina}}`
 * - `{{%status_opce}}`
 */
class m210324_070522_create_upitnik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%upitnik}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(250),
            'opis' => $this->text(),
            'ped_godina' => $this->integer(),
            'date_publish' => $this->timestamp(),
            'date_end' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'creator' => $this->integer(),
            'status' => $this->integer(),
            'upitnik_for' => $this->json(),
            'is_required' => $this->boolean(),
        ]);

        // creates index for column `ped_godina`
        $this->createIndex(
            '{{%idx-upitnik-ped_godina}}',
            '{{%upitnik}}',
            'ped_godina'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-upitnik-ped_godina}}',
            '{{%upitnik}}',
            'ped_godina',
            '{{%pedagoska_godina}}',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            '{{%idx-upitnik-creator}}',
            '{{%upitnik}}',
            'creator'
        );

        // add foreign key for table `{{%pedagoska_godina}}`
        $this->addForeignKey(
            '{{%fk-upitnik-user}}',
            '{{%upitnik}}',
            'creator',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `status`
        $this->createIndex(
            '{{%idx-upitnik-status}}',
            '{{%upitnik}}',
            'status'
        );

        // add foreign key for table `{{%status_opce}}`
        $this->addForeignKey(
            '{{%fk-upitnik-status}}',
            '{{%upitnik}}',
            'status',
            '{{%status_opce}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%pedagoska_godina}}`
        $this->dropForeignKey(
            '{{%fk-upitnik-ped_godina}}',
            '{{%upitnik}}'
        );

        // drops index for column `ped_godina`
        $this->dropIndex(
            '{{%idx-upitnik-ped_godina}}',
            '{{%upitnik}}'
        );

        // drops foreign key for table `{{%status_opce}}`
        $this->dropForeignKey(
            '{{%fk-upitnik-status}}',
            '{{%upitnik}}'
        );

        // drops index for column `status`
        $this->dropIndex(
            '{{%idx-upitnik-status}}',
            '{{%upitnik}}'
        );

        $this->dropTable('{{%upitnik}}');
    }
}
