<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%jelovnik}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%obrok}}`
 * - `{{%jelo}}`
 */
class m201022_130843_create_jelovnik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%jelovnik}}', [
            'id' => $this->primaryKey(),
            'datum' => $this->date(),
            'obrok' => $this->integer(),
            'jelo' => $this->integer(),
            'redoslijed' => $this->integer(),
        ]);

        // creates index for column `obrok`
        $this->createIndex(
            '{{%idx-jelovnik-obrok}}',
            '{{%jelovnik}}',
            'obrok'
        );

        // add foreign key for table `{{%obrok}}`
        $this->addForeignKey(
            '{{%fk-jelovnik-obrok}}',
            '{{%jelovnik}}',
            'obrok',
            '{{%obrok}}',
            'id',
            'CASCADE'
        );

        // creates index for column `jelo`
        $this->createIndex(
            '{{%idx-jelovnik-jelo}}',
            '{{%jelovnik}}',
            'jelo'
        );

        // add foreign key for table `{{%jelo}}`
        $this->addForeignKey(
            '{{%fk-jelovnik-jelo}}',
            '{{%jelovnik}}',
            'jelo',
            '{{%jelo}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%obrok}}`
        $this->dropForeignKey(
            '{{%fk-jelovnik-obrok}}',
            '{{%jelovnik}}'
        );

        // drops index for column `obrok`
        $this->dropIndex(
            '{{%idx-jelovnik-obrok}}',
            '{{%jelovnik}}'
        );

        // drops foreign key for table `{{%jelo}}`
        $this->dropForeignKey(
            '{{%fk-jelovnik-jelo}}',
            '{{%jelovnik}}'
        );

        // drops index for column `jelo`
        $this->dropIndex(
            '{{%idx-jelovnik-jelo}}',
            '{{%jelovnik}}'
        );

        $this->dropTable('{{%jelovnik}}');
    }
}
