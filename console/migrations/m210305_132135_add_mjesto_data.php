<?php

use yii\db\Migration;

/**
 * Class m210305_132135_add_mjesto_data
 */
class m210305_132135_add_mjesto_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('mjesto','sifra',$this->integer());

                    $file = fopen(__DIR__ . '/data/naselja.csv', "r");
                    set_time_limit(0);
                    $redak = fgetcsv($file,'',';');
                    while ($redak = fgetcsv($file,'',';')){
                        $this->insert('mjesto',[
                            'naziv' => $redak[0],
                            'zupanija' => $redak[4],
                            'sifra' => $redak[1]
                        ]);
                    }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('mjesto', 'sifra');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210305_132135_add_mjesto_data cannot be reverted.\n";

        return false;
    }
    */
}
