<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dijete_skupina}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%dijete}}`
 * - `{{%skupina}}`
 */
class m200907_123209_create_dijete_skupina_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dijete_skupina}}', [
            'id' => $this->primaryKey(),
            'dijete' => $this->integer(),
            'skupina' => $this->integer(),
        ]);

        // creates index for column `dijete`
        $this->createIndex(
            '{{%idx-dijete_skupina-dijete}}',
            '{{%dijete_skupina}}',
            'dijete'
        );

        // add foreign key for table `{{%dijete}}`
        $this->addForeignKey(
            '{{%fk-dijete_skupina-dijete}}',
            '{{%dijete_skupina}}',
            'dijete',
            '{{%dijete}}',
            'id',
            'CASCADE'
        );

        // creates index for column `skupina`
        $this->createIndex(
            '{{%idx-dijete_skupina-skupina}}',
            '{{%dijete_skupina}}',
            'skupina'
        );

        // add foreign key for table `{{%skupina}}`
        $this->addForeignKey(
            '{{%fk-dijete_skupina-skupina}}',
            '{{%dijete_skupina}}',
            'skupina',
            '{{%skupina}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%dijete}}`
        $this->dropForeignKey(
            '{{%fk-dijete_skupina-dijete}}',
            '{{%dijete_skupina}}'
        );

        // drops index for column `dijete`
        $this->dropIndex(
            '{{%idx-dijete_skupina-dijete}}',
            '{{%dijete_skupina}}'
        );

        // drops foreign key for table `{{%skupina}}`
        $this->dropForeignKey(
            '{{%fk-dijete_skupina-skupina}}',
            '{{%dijete_skupina}}'
        );

        // drops index for column `skupina`
        $this->dropIndex(
            '{{%idx-dijete_skupina-skupina}}',
            '{{%dijete_skupina}}'
        );

        $this->dropTable('{{%dijete_skupina}}');
    }
}
