<?php

use yii\db\Migration;

/**
 * Class m220222_135020_add_row_to_postavke
 */
class m220222_135020_add_row_to_postavke extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('postavke', ['postavka' => 'eupisi', 'vrijednost' => 'false']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('postavke', ['postavka' => 'eupisi']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220222_135020_add_row_to_postavke cannot be reverted.\n";

        return false;
    }
    */
}
