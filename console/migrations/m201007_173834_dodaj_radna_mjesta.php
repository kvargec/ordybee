<?php

use yii\db\Migration;

/**
 * Class m201007_173834_dodaj_radna_mjesta
 */
class m201007_173834_dodaj_radna_mjesta extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $dataSprema=[
            ['nije definirano','NA'],
            ['niža stručna sprema','NSS'],
            ['srednja stručna sprema','SSS'],
            ['viša stručna sprema ili specijalist','VŠS'],
            ['visoka stručna sprema','VSS']
        ];
        $this->batchInsert('sprema',['naziv','kratica'],$dataSprema);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201007_173834_dodaj_radna_mjesta cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201007_173834_dodaj_radna_mjesta cannot be reverted.\n";

        return false;
    }
    */
}
