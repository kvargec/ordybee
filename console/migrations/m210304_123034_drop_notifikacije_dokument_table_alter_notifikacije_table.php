<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%notifikacije_dokument_table_alter_notifikacije}}`.
 */
class m210304_123034_drop_notifikacije_dokument_table_alter_notifikacije_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifikacije', 'attachments', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifikacije', 'attachments');
    }
}
