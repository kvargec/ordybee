<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mstatus}}`.
 */
class m210321_181005_create_mstatus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mstatus}}', [
            'id' => $this->primaryKey(),
            'status' => $this->string(150),
        ]);
        $this->batchInsert('mstatus', ['status'],[
            ['sent'],
            ['delivered'],
            ['opened'],
            ['deleted'],
            ['replied']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mstatus}}');
    }
}
