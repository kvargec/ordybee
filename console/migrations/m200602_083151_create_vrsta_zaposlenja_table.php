<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrsta_zaposlenja}}`.
 */
class m200602_083151_create_vrsta_zaposlenja_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrsta_zaposlenja}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrsta_zaposlenja}}');
    }
}
