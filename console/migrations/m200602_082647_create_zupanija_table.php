<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zupanija}}`.
 */
class m200602_082647_create_zupanija_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zupanija}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(250),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%zupanija}}');
    }
}
