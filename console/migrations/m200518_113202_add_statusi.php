<?php

use yii\db\Migration;

/**
 * Class m200518_113202_add_statusi
 */
class m200518_113202_add_statusi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200518_113202_add_statusi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200518_113202_add_statusi cannot be reverted.\n";

        return false;
    }
    */
}
