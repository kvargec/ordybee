<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zaposlenik_obrok}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%vrste_obroka}}`
 * - `{{%zaposlenik}}`
 */
class m221215_144011_create_zaposlenik_obrok_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%zaposlenik_obrok}}', [
            'id' => $this->primaryKey(),
            'obrok_id' => $this->integer()->notNull(),
            'zaposlenik_id' => $this->integer()->notNull(),
            'cijena' => $this->decimal(2),
        ]);

        // creates index for column `obrok_id`
        $this->createIndex(
            '{{%idx-zaposlenik_obrok-obrok_id}}',
            '{{%zaposlenik_obrok}}',
            'obrok_id'
        );

        // add foreign key for table `{{%vrste_obroka}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik_obrok-obrok_id}}',
            '{{%zaposlenik_obrok}}',
            'obrok_id',
            '{{%vrste_obroka}}',
            'id',
            'CASCADE'
        );

        // creates index for column `zaposlenik_id`
        $this->createIndex(
            '{{%idx-zaposlenik_obrok-zaposlenik_id}}',
            '{{%zaposlenik_obrok}}',
            'zaposlenik_id'
        );

        // add foreign key for table `{{%zaposlenik}}`
        $this->addForeignKey(
            '{{%fk-zaposlenik_obrok-zaposlenik_id}}',
            '{{%zaposlenik_obrok}}',
            'zaposlenik_id',
            '{{%zaposlenik}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%vrste_obroka}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik_obrok-obrok_id}}',
            '{{%zaposlenik_obrok}}'
        );

        // drops index for column `obrok_id`
        $this->dropIndex(
            '{{%idx-zaposlenik_obrok-obrok_id}}',
            '{{%zaposlenik_obrok}}'
        );

        // drops foreign key for table `{{%zaposlenik}}`
        $this->dropForeignKey(
            '{{%fk-zaposlenik_obrok-zaposlenik_id}}',
            '{{%zaposlenik_obrok}}'
        );

        // drops index for column `zaposlenik_id`
        $this->dropIndex(
            '{{%idx-zaposlenik_obrok-zaposlenik_id}}',
            '{{%zaposlenik_obrok}}'
        );

        $this->dropTable('{{%zaposlenik_obrok}}');
    }
}
