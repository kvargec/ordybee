<?php

use yii\db\Migration;

/**
 * Class m210226_084804_alter_notifikacije_table
 */
class m210226_084804_alter_notifikacije_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('notifikacije', 'sender_trash', $this->boolean()->defaultValue(false));
        $this->addColumn('notifikacije', 'sender_delete', $this->boolean()->defaultValue(false));
        $this->addColumn('notifikacije','user_trash', $this->boolean()->defaultValue(false));
        $this->addColumn('notifikacije','user_delete', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('notifikacije','sender_trash');
        $this->dropColumn('notifikacije','sender_delete');
        $this->dropColumn('notifikacije','user_trash');
        $this->dropColumn('notifikacije','user_delete');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210226_084804_alter_notifikacije_table cannot be reverted.\n";

        return false;
    }
    */
}
