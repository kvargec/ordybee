<?php

use yii\db\Migration;

/**
 * Class m201021_142955_add_zaposlenje
 */
class m201021_142955_add_zaposlenje extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if(file_exists(__DIR__ . '/data/zaposlenici.csv')){
            $fp = fopen(__DIR__ . '/data/zaposlenici.csv','r');
            set_time_limit(0);
            $vrstaZaposlenja = \common\models\VrstaZaposlenja::find()->where(['naziv'=>'neodređeno'])->one();
            while ($redak = fgetcsv($fp,"",',')){
                $imeprezime = explode(' ',$redak[1]);

                $zaposlenik = \common\models\Zaposlenik::find()->where(['ime'=>$imeprezime[1],'prezime'=>$imeprezime[0]])->one();
                if(strlen($redak[2])>1){
                    $radnoMjesto = \common\models\RadnoMjesto::find()->where(['ilike','naziv',$redak[2]])->one();

                }elseif($imeprezime[0] == 'Marjanac' && $imeprezime[1] == 'Višnja'){
                    $radnoMjesto = \common\models\RadnoMjesto::find()->where(['naziv'=>'tajnica'])->one();

                }else{
                    $radnoMjesto = \common\models\RadnoMjesto::find()->where(['naziv'=>'odgojiteljice'])->one();

                }
                $dataZaposljenje[] = [$vrstaZaposlenja->id,$zaposlenik->id,1,'2020-01-01',date('yy-m-d H:i:s',time()),date('yy-m-d H:i:s',time()),$radnoMjesto->id];
            }
        }
        fclose($fp);
        $this->batchInsert('zaposlenje',['vrsta','zaposlenik','koeficijent','dat_pocetka','updated_at','created_at','r_mjesto'],$dataZaposljenje);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('zaposlenje');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201021_142955_add_zaposlenje cannot be reverted.\n";

        return false;
    }
    */
}
