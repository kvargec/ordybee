<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ulazni_racun}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%tvrtka}}`
 */
class m210326_165731_create_ulazni_racun_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ulazni_racun}}', [
            'id' => $this->primaryKey(),
            'br_racuna'=>$this->string(150),
            'dobavljac' => $this->integer(),
            'naslov' => $this->string(250),
            'napomena' => $this->text(),
            'datum_racuna' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'preuzeo' => $this->string(250),
            'iznos' => $this->float()->defaultValue(0),
        ]);

        // creates index for column `dobavljac`
        $this->createIndex(
            '{{%idx-ulazni_racun-dobavljac}}',
            '{{%ulazni_racun}}',
            'dobavljac'
        );

        // add foreign key for table `{{%tvrtka}}`
        $this->addForeignKey(
            '{{%fk-ulazni_racun-dobavljac}}',
            '{{%ulazni_racun}}',
            'dobavljac',
            '{{%tvrtka}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%tvrtka}}`
        $this->dropForeignKey(
            '{{%fk-ulazni_racun-dobavljac}}',
            '{{%ulazni_racun}}'
        );

        // drops index for column `dobavljac`
        $this->dropIndex(
            '{{%idx-ulazni_racun-dobavljac}}',
            '{{%ulazni_racun}}'
        );

        $this->dropTable('{{%ulazni_racun}}');
    }
}
