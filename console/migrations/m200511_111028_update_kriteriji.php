<?php

use yii\db\Migration;
use common\models\VrstaUnosa;

/**
 * Class m200511_111028_update_kriteriji
 */
class m200511_111028_update_kriteriji extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $checkboxId=VrstaUnosa::findOne(['vrsta'=>'checkbox'])->id;
        $selectId=VrstaUnosa::findOne(['vrsta'=>'select'])->id;
        $this->update ('kriterij', ['vrsta_unosa_id'=>$checkboxId]);

        $this->update (
            'kriterij',
            ['vrsta_unosa_id'=>$selectId,
             'opcije'=>[
                0=>'0 godina',
                1=>'1 godina',
                2=>'2 godina',
                3=>'3 godina',
                4=>'4 godina',
                5=>'5 godina',
             ]
            ],
            ['in','naziv','Djeca s liste čekanja od upisa za svaku prethodnu pedagošku godinu (u kontinuitetu)',]
        );

        $this->update (
            'kriterij',
            ['vrsta_unosa_id'=>$selectId,
                'opcije'=>[
                    0=>'0 djece',
                    1=>'1 djece',
                    2=>'2 djece',
                    3=>'3 djece',
                    4=>'4 djece',
                    5=>'5 djece',
                ]
            ],
            ['in','naziv','Djeca iz obitelji troje ili više maloljetne djece, po djetetu',]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200511_111028_update_kriteriji cannot be reverted.\n";

        return false;
    }
    */
}
