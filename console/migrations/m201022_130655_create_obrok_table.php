<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%obrok}}`.
 */
class m201022_130655_create_obrok_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%obrok}}', [
            'id' => $this->primaryKey(),
            'obrok' => $this->string(200),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%obrok}}');
    }
}
