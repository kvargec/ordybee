<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%zahtjev_kriterij}}`.
 */
class m200511_132538_add_zadovoljene_opcije_column_to_zahtjev_kriterij_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%zahtjev_kriterij}}', 'zadovoljene_opcije', $this->json());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%zahtjev_kriterij}}', 'zadovoljene_opcije');
    }
}
