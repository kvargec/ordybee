<?php

use yii\db\Migration;

/**
 * Class m201023_112019_add_obroks_to_obrok_table
 */
class m201023_112019_add_obroks_to_obrok_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->batchInsert('obrok',['obrok'], [
                ['dorucak'],['rucak'],['uzina']
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201023_112019_add_obroks_to_obrok_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201023_112019_add_obroks_to_obrok_table cannot be reverted.\n";

        return false;
    }
    */
}
