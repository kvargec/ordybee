<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vrsta_dnevnik}}`.
 */
class m200602_083156_create_vrsta_dnevnik_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vrsta_dnevnik}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(150),
            'postavke' => $this->json(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vrsta_dnevnik}}');
    }
}
