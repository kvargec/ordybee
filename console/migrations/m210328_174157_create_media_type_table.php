<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%media_type}}`.
 */
class m210328_174157_create_media_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%media_type}}', [
            'id' => $this->primaryKey(),
            'naziv' => $this->string(100),
            'icon' => $this->string(50),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%media_type}}');
    }
}
