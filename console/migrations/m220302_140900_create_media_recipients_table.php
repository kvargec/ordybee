<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%media_recipients}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%media}}`
 * - `{{%user}}`
 */
class m220302_140900_create_media_recipients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%media_recipients}}', [
            'id' => $this->primaryKey(),
            'media' => $this->integer()->notNull(),
            'user' => $this->integer()->notNull(),
            'created_at' => $this->dateTime(),
        ]);

        // creates index for column `media`
        $this->createIndex(
            '{{%idx-media_recipients-media}}',
            '{{%media_recipients}}',
            'media'
        );

        // add foreign key for table `{{%media}}`
        $this->addForeignKey(
            '{{%fk-media_recipients-media}}',
            '{{%media_recipients}}',
            'media',
            '{{%media}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user`
        $this->createIndex(
            '{{%idx-media_recipients-user}}',
            '{{%media_recipients}}',
            'user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-media_recipients-user}}',
            '{{%media_recipients}}',
            'user',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%media}}`
        $this->dropForeignKey(
            '{{%fk-media_recipients-media}}',
            '{{%media_recipients}}'
        );

        // drops index for column `media`
        $this->dropIndex(
            '{{%idx-media_recipients-media}}',
            '{{%media_recipients}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-media_recipients-user}}',
            '{{%media_recipients}}'
        );

        // drops index for column `user`
        $this->dropIndex(
            '{{%idx-media_recipients-user}}',
            '{{%media_recipients}}'
        );

        $this->dropTable('{{%media_recipients}}');
    }
}
