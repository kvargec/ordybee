<?php

namespace console\controllers;
use common\models\Roditelj;
use common\models\UserRoditelj;
use common\models\Zahtjev;
use Yii;
use common\controllers\ZaposlenikController;
use Codeception\Lib\Di;
use common\models\Calendar;
use common\models\Dijete;
use common\models\DijeteSkupina;
use common\models\Prisutnost;
use common\models\Zaposlenik;
use yii\base\BaseObject;
use yii\console\Controller;
use Da\User\Model\User;
use Da\User\Model\Profile;

class PrisutnostController extends Controller{
    
    public function actionDemo(){

        $imena=['administracija'=>'12345678','ekonom'=>'12345678','kuhinja'=>'12345678','logoped'=>'12345678','odgajatelj'=>'12345678','pedagog'=>'12345678','psiholog'=>'12345678','ravnateljstvo'=>'12345678','zdrastveno'=>'12345678'];
        foreach ($imena as $kor=>$loz){
            $korisnik = new User();
            $korisnik->username=$kor;
            $korisnik->email = $kor .'@nema.com';
            $korisnik->password_hash = Yii::$app->getSecurity()->generatePasswordHash($loz);
            $korisnik->auth_key = 'SHby0gr6ooWwjITQK4zH2ZryzhOauTZA';//Yii::$app->getSecurity()->generateRandomKey();
            $korisnik->flags = 0;

            $korisnik->scenario = 'create';
            $korisnik->created_at = time();
            $korisnik->updated_at = time();
            $korisnik->confirmed_at = time();
            $korisnik->save();
            $temp=Profile::findOne($korisnik->id);
            $temp->name=$kor;
            $temp->save();
            $auth = \Yii::$app->authManager;

            //$korisnik->profile->name=$kor;
            $korisnik->save();
            switch ($kor){
                case 'odgajatelj':
                    $temp=\common\models\Zaposlenik::find()->where(['id'=>2])->one();
                    $authorRole = $auth->getRole('odgajatelj');
                    $auth->assign($authorRole, $korisnik->id);
                    $temp->user=$korisnik->id;
                    $temp->save();
                    break;
                case 'logoped':
                    $temp=\common\models\Zaposlenik::find()->where(['id'=>42])->one();
                    $authorRole = $auth->getRole('logoped');
                    $auth->assign($authorRole, $korisnik->id);
                    $temp->user=$korisnik->id;
                    $temp->save();
                    break;
                case 'pedagog':
                    $temp=\common\models\Zaposlenik::find()->where(['id'=>37])->one();
                    $authorRole = $auth->getRole('pedagog');
                    $auth->assign($authorRole, $korisnik->id);
                    $temp->user=$korisnik->id;
                    $temp->save();
                    break;
                case 'ekonom':
                    $temp=\common\models\Zaposlenik::find()->where(['id'=>32])->one();
                    $authorRole = $auth->getRole('ekonom');
                    $auth->assign($authorRole, $korisnik->id);
                    $temp->user=$korisnik->id;
                    $temp->save();
                    break;
                case 'psiholog':
                    $temp=\common\models\Zaposlenik::find()->where(['id'=>35])->one();
                    $authorRole = $auth->getRole('psiholog');
                    $auth->assign($authorRole, $korisnik->id);
                    $temp->user=$korisnik->id;
                    $temp->save();
                    break;
                case 'ravnateljstvo':
                    $temp=\common\models\Zaposlenik::find()->where(['id'=>23])->one();
                    $authorRole = $auth->getRole('ravnateljstvo');
                    $auth->assign($authorRole, $korisnik->id);
                    $temp->user=$korisnik->id;
                    $temp->save();
                    break;
                case 'zdrastveno':
                    $temp=\common\models\Zaposlenik::find()->where(['id'=>13])->one();
                    $authorRole = $auth->getRole('zdravsteno');
                    $auth->assign($authorRole, $korisnik->id);
                    $temp->user=$korisnik->id;
                    $temp->save();
                    break;
                case 'administracija':
                    $authorRole = $auth->getRole('administracija');
                    $auth->assign($authorRole, $korisnik->id);
                    $temp=\common\models\Zaposlenik::find()->where(['id'=>27])->one();
                    $temp->user=$korisnik->id;
                    $temp->save();
                    break;
                case 'kuhinja':
                    $authorRole = $auth->getRole('kuhinja');
                    $auth->assign($authorRole, $korisnik->id);
                    $temp=\common\models\Zaposlenik::find()->where(['id'=>11])->one();
                    $temp->user=$korisnik->id;
                    $temp->save();
                    break;
            }
        }
    }
    public function actionDjeca($skupina=null){
        $neradniDan = Calendar::find()->where(['dan'=>date('yy-m-d',time())])->one();
        if(!$neradniDan){
            if (!(date('D') == 'Sat' || date('D') == 'Sun')) {
                if($skupina != null){
                    $djeca = Dijete::find()->leftJoin('dijete_skupina','dijete.id=dijete_skupina.dijete')->where(['dijete_skupina.skupina'=>$skupina])->all();
                }else{
                    $djeca=DijeteSkupina::find()->all();
                }
                $count = 0;
                foreach ($djeca as $dijete) {
                    $provjera = Prisutnost::find()->where(['datum' => date('yy-m-d',time()),'entitet'=>'dijete','entitet_id'=>$dijete->dijete ])->one();
                    if(empty($provjera)) {
                        $prisutnost = new Prisutnost();
                        $prisutnost->entitet = 'dijete';
                        $prisutnost->trajanje = 8;
                        $prisutnost->entitet_id = $dijete->dijete;
                        $prisutnost->datum = date('yy-m-d', time());
                        $prisutnost->status = 1;
                        $prisutnost->save();
                        $count++;
                    }
                }
                $this->stdout("UBACENO: ".$count." djece");
                return;
            }else
                $this->stdout("Vikend");
                return;
        }else{
            $this->stdout("Neradni dan");
            return;
        }

    }
    public function actionZaposlenik(){
        $neradniDan = Calendar::find()->where(['dan'=>date('yy-m-d',time())])->one();
        if(!$neradniDan){
            if (!(date('D') == 'Sat' || date('D') == 'Sun')) {
                $zaposlenici=Zaposlenik::find()->where(['status'=>2])->all();
                $count = 0;
                foreach ($zaposlenici as $zaposlenik) {
                    $provjera = Prisutnost::find()->where(['datum' => date('Y-m-d',time()),'entitet'=>'zaposlenik','entitet_id'=>$zaposlenik->id ])->one();
                    if(empty($provjera)) {
                        $prisutnost = new Prisutnost();
                        $prisutnost->entitet = 'zaposlenik';
                        $prisutnost->trajanje = 8;
                        $prisutnost->entitet_id = $zaposlenik->id;
                        $prisutnost->datum = date('Y-m-d', time());
                        $prisutnost->status = 1;
                        $zaDolaznostRand=rand(6,9);
                        $poc=new \DateTime(date('H:i', strtotime('today '.$zaDolaznostRand.' hours', time())));
                        $kraj=new \DateTime(date('H:i', strtotime('today '.($zaDolaznostRand+8).' hours', time())));
                        $prisutnost->pocetak=$poc->format('H:i');
                        $prisutnost->kraj=$kraj->format('H:i');
                        $prisutnost->save();
                        $count++;
                    }
                }
                $this->stdout("UBACENO: ".$count." djece");
                return;
            }else
                $this->stdout("Vikend");
            return;
        }else{
            $this->stdout("Neradni dan");
            return;
        }

    }/*
    public function actionDjecaRazdoblje($skupina=null,$razdoblje){
        
        foreach ($razdoblje as $dan){

        }
        $neradniDan = Calendar::find()->where(['dan'=>date('yy-m-d',time())])->one();
        if(!$neradniDan){
            if (!(date('D') == 'Sat' || date('D') == 'Sun')) {
                if($skupina != null){
                    $djeca = Dijete::find()->leftJoin('dijete_skupina','dijete.id=dijete_skupina.dijete')->where(['dijete_skupina.skupina'=>$skupina])->all();
                }else{
                    $djeca=DijeteSkupina::find()->all();
                }
                $count = 0;
                foreach ($djeca as $dijete) {
                    $provjera = Prisutnost::find()->where(['datum' => date('yy-m-d',time()),'entitet'=>'dijete','entitet_id'=>$dijete->dijete ])->one();
                    if(empty($provjera)) {
                        $prisutnost = new Prisutnost();
                        $prisutnost->entitet = 'dijete';
                        $prisutnost->trajanje = 8;
                        $prisutnost->entitet_id = $dijete->dijete;
                        $prisutnost->datum = date('yy-m-d', time());
                        $prisutnost->status = 1;
                        $prisutnost->save();
                        $count++;
                    }
                }
                $this->stdout("UBACENO: ".$count." djece");
                return;
            }else
                $this->stdout("Vikend");
            return;
        }else{
            $this->stdout("Neradni dan");
            return;
        }

    }*/
    function actionUserroditelj()
    {
        $dijete=Zahtjev::find()->all();
        foreach ($dijete as $dij){

            $roditelji=Roditelj::find()->where(['dijete'=>$dij->dijete])->all();
            foreach ($roditelji as $roditelj){

                $test=UserRoditelj::find()->where(['user'=>$dij->user,'roditelj'=>$roditelj->id])->one();

                if(!isset($test)){
                    $temp=new UserRoditelj();
                    $temp->user=$dij->user;
                    $temp->roditelj=$roditelj->id;
                    $temp->username="-";
                    $temp->password="-";
                    $temp->save();
                }
            }
        }


    }
}
