<?php

namespace console\controllers;

use common\models\Postavke;
use common\models\Roditelj;
use common\models\UserRoditelj;
use common\models\Zaposlenje;
use common\models\Zahtjev;
use common\models\Media;
use common\models\MediaRecipients;
use Yii;
use common\controllers\ZaposlenikController;
use Codeception\Lib\Di;
use common\models\Calendar;
use common\models\Dijete;
use common\models\DijeteSkupina;
use common\models\Prisutnost;
use common\models\Zaposlenik;
use yii\base\BaseObject;
use yii\console\Controller;
use Da\User\Model\User;
use Da\User\Model\Profile;

class UtilController extends Controller
{

    public function actionVerzija()
    {
        $verzija = Postavke::find()->where(['postavka' => 'verzijaOrdyBee'])->one();

        $this->stdout($verzija->vrijednost);
        return;
    }

    public function actionUbacikorisnike($file)
    {
        $dir = "console/db/";
        if (file_exists($dir . $file . '.csv')) {
            $fp = fopen($dir . $file . '.csv', "r");
            set_time_limit(0);
            while ($redak = fgetcsv($fp, 0, ',')) {
                $test = new User();
                $test->username = $redak[3];
                $test->email = $redak[5];
                $test->password_hash = Yii::$app->getSecurity()->generatePasswordHash($redak[4]);
                $test->auth_key = Yii::$app->getSecurity()->generateRandomKey();
                $test->flags = 0;
                $test->scenario = 'create';
                $test->created_at = time();
                $test->updated_at = time();
                $test->confirmed_at = time();
                if ($test->save()) {
                    //  echo 'User jeee -' . $redak['2'];
                } else {
                    print_r($test->getErrors());
                    echo 'User neee -' . $redak[1] . '<br/>';
                };

                $auth = Yii::$app->authManager;
                $odgajateljRole = $auth->getRole('odgajatelj');
                $auth->assign($odgajateljRole, $test->id);

                $zaposlenik = Zaposlenik::find()->where(['ime' => $redak[1], 'prezime' => $redak[2]])->one();
                $zaposlenik->user = $test->id;
                $zaposlenik->save();
            }
            fclose($fp);
        } else {
            echo $dir . $file;
            echo 'Nema ga: ' . $file . '.csv';
        };
    }
    public function actionCreateMediaRecipients()
    {
        $media_ids = MediaRecipients::find()->select('media')->asArray()->all();
        $media = Media::find()->where(['not in', 'id', array_column($media_ids, "media")])->all();

        foreach ($media as $model) {
            foreach ($model->recipients as $key => $value) {
                switch ($key) {
                    case 'roditelji':
                        if ($value[0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $value])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($value[0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $value])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($value[0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $value])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($value[0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $value])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                }
            }
            if (isset($model->cc_recipients)) {
                foreach ($model->cc_recipients as $key => $value) {
                    switch ($key) {
                        case 'roditelji':
                            if ($value[0] == 0) {
                                $kojiUseri = UserRoditelj::find()->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new MediaRecipients();
                                    $temp->media = $model->id;
                                    $temp->user = $item->user;
                                    $temp->created_at = date('Y-m-d H:i:s', time());
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            } else {
                                $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $value])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new MediaRecipients();
                                    $temp->media = $model->id;
                                    $temp->user = $item->user;
                                    $temp->created_at = date('Y-m-d H:i:s', time());
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            }
                            break;
                        case 'skupine':
                            if ($value[0] == 0) {
                                $kojiUseri = UserRoditelj::find()->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new MediaRecipients();
                                    $temp->media = $model->id;
                                    $temp->user = $item->user;
                                    $temp->created_at = date('Y-m-d H:i:s', time());
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            } else {
                                $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $value])->select('dijete')->all();
                                $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                                $roditeljiSvi = array();
                                foreach ($roditelji as $rod) {
                                    $roditeljiSvi[] = $rod->id;
                                }
                                $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new MediaRecipients();
                                    $temp->media = $model->id;
                                    $temp->user = $item->user;
                                    $temp->created_at = date('Y-m-d H:i:s', time());
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            }
                            break;
                        case 'zaposlenici':
                            if ($value[0] == 0) {
                                $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new MediaRecipients();
                                    $temp->media = $model->id;
                                    $temp->user = $item->user;
                                    $temp->created_at = date('Y-m-d H:i:s', time());
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            } else {
                                $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $value])->asArray()->all();
                                $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new MediaRecipients();
                                    $temp->media = $model->id;
                                    $temp->user = $item->user;
                                    $temp->created_at = date('Y-m-d H:i:s', time());
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            }
                            break;
                        case 'pojedini':
                            if ($value[0] == 0) {
                                $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new MediaRecipients();
                                    $temp->media = $model->id;
                                    $temp->user = $item->user;
                                    $temp->created_at = date('Y-m-d H:i:s', time());
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            } else {
                                $kojiUseri = Zaposlenik::find()->where(['in', 'id', $value])->andWhere(['!=', 'status', 3])->all();
                                foreach ($kojiUseri as $item) {
                                    $temp = new MediaRecipients();
                                    $temp->media = $model->id;
                                    $temp->user = $item->user;
                                    $temp->created_at = date('Y-m-d H:i:s', time());
                                    if ($temp->save()) {
                                    } else {
                                        var_dump($temp->errors);
                                        die();
                                    };
                                }
                            }
                            break;
                    }
                }
            }
        }
    }
    public function actionSwapColumns()
    {
        $connection = Yii::$app->getDb();
        $command_2 = $connection->createCommand("UPDATE \"dijete\" SET ime=prezime, prezime=ime;    
        ");
        $result_2 = $command_2->queryAll();
        var_dump($result_2);
    }
}
