<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use kartik\grid\GridView;

//AppAsset::register($this);
//$this->context->layout='@common/views/layouts/pdf';
/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */

//$this->context->layout='@common/views/layouts/pdf';
?>
<style>

    .panel {
        margin-bottom: 20px;
        background-color: #fff;
        border-radius: 4px;
    }
    .panel-heading {
        padding: 10px 15px;
        border-bottom: 1px solid #ddd;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }
    .panel-success > .panel-heading {
         background-color: #e4a40f;
         color: #fff;
     }
    .panel-body {
        padding: 15px;
    }
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }
    .table-bordered {
         border: 1px solid #ddd;
     }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
    table{
        border-spacing: 0;
    }
    th,td {
        text-align: left;
        border: 1px solid #ddd;
        border-spacing: 0;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
</style>
<div class="shop-create site-about">

    <h2><?= Yii::t('app','Upitnik')?></h2>
    <div class="col s12 m12 l8">

        <div class="panel panel-success">
            <div class="panel-heading">
                <span class="card-title"><?= $upitnik->naziv.'<br />' ?></span>
                <span class="card-title"><?= 'Dijete:'.$dijete->prezime.', '.$dijete->ime.'<br />' ?></span>
                <?php foreach ($roditelji as $roditelj) { ?>
                    <span class="card-title"><?= 'Roditelj:'.$roditelj->prezime.', '.$roditelj->ime.'<br />' ?></span>
                <?php } ?>
                <hr />
            </div>
            <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $odgovori,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                            'label'=>'Pitanja i Odgovori',
                            'format'=>'raw',
                            'value'=>function($data) {
                                switch($data->pitanje0->vrsta_pitanja){
                                    case 1:
                                        $tt=json_decode($data->pitanje0->odgovori,true);

                                        return $data->pitanje0->pitanje.'<br />'.$tt[json_decode($data->value)->odgovori];
                                        break;
                                    case 2:
                                        $sviOdg=array();
                                        foreach (json_decode($data->value)->odgovori as $odg){
                                            $tt=json_decode($data->pitanje0->odgovori,true);
                                            $sviOdg[]=$tt[$odg];
                                        };
                                        return $data->pitanje0->pitanje.'<br />'.implode("<br />",$sviOdg);
                                        break;
                                    case 3:
                                    case 4:
                                        return $data->pitanje0->pitanje.'<br />'.json_decode($data->value)->odgovori;
                                        break;
                                }
                            }
                    ],

                ],
            ]); ?>
            </div>  
        </div>
    </div>
</div>