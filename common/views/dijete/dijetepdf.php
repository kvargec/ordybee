<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;


//AppAsset::register($this);
//$this->context->layout='@common/views/layouts/pdf';
/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
$dijeteId = $dijete->id;

//$this->context->layout='@common/views/layouts/pdf';
?>
<style>

    .panel {
        margin-bottom: 20px;
        background-color: #fff;
        border-radius: 4px;
    }
    .panel-heading {
        padding: 10px 15px;
        border-bottom: 1px solid #ddd;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }
    .panel-success > .panel-heading {
         background-color: #e4a40f;
         color: #fff;
     }
    .panel-body {
        padding: 15px;
    }
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }
    .table-bordered {
         border: 1px solid #ddd;
     }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
    table{
        border-spacing: 0;
    }
    th,td {
        text-align: left;
        border: 1px solid #ddd;
        border-spacing: 0;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
</style>
<div class="shop-create site-about">

    <h2><?= Yii::t('app','Podaci u zahtjevu za upis')?></h2>
    <div class="col s12 m12 l8">

        <div class="panel panel-success">
            <div class="panel-heading">
                <?php echo $dijete->ime . ', '.$dijete->prezime; ?>
                <hr />
            </div>
            <div class="panel-body">
            <?= DetailView::widget([
        'model' => $dijete,
        'attributes' => [
            'ime',
            'prezime',
            'spol',
            'dat_rod',
            'oib',
            'adresa',
            'mjesto',
            'prebivaliste',
            'sestra',
            'cekanje',
            'god_cekanja',
            'razvoj',
            'vrsta_programa',
            'teskoce',
            'dijagnostika',
            'druge_potrebe',
            'posebne',
            'create_at',
            'update_at',
        ],
    ]) ?>
<h3><?= Yii::t('app','Roditelji')?></h3>
    <?= \kartik\grid\GridView::widget([
            'dataProvider' => $roditelji,
            'showPageSummary' => false,
            'columns' => [
                'ime',
                'prezime',
                'spol',
                'dat_rod',
                'oib',
                'adresa',
                'mjesto',
                'prebivaliste',
                'radno',
                'zanimanje',
                'poslodavca',
                'mobitel',
                'email'
            ]
    ])?>

    <h3><?= Yii::t('app','Kartoni zdravstvene službe')?></h3>
    <?= GridView::widget([
        'dataProvider' => $zdravstvena,
        //'filterModel' => $searchModel,
        'columns' => [
            'zapis0.zapis',
            [
                'format'=>'html',
                'value'=>function($data){
                    $tmp =  json_decode($data->dodatno);
                    $text = '';
                    foreach ($tmp as $key=>$value){
                        $text.= $key.': '.  $value .'<br>';
                    }
                    return $text;

                }
            ],
            [
                'format' =>'raw',
                'header'=>'Nalaz',
                'value' => function($data){
                    if($data->zapis0->vrsta0->naziv == 'nalaz')
                        return Html::a('<i class="fa fa-file-pdf-o" style="font-size:24px"></i>',\yii\helpers\Url::to(['/dijete-zapis/zapispdf','id'=>$data->id]));
                    else
                        return;
                },

            ]
        ],
    ]); ?>

    <h3><?= Yii::t('app','Kartoni logopeda/rehabilitatora')?></h3>
    <?= GridView::widget([
        'dataProvider' => $logoped,
        //'filterModel' => $searchModel,
        'columns' => [
            'zapis0.zapis',
            [
                'format'=>'html',
                'value'=>function($data){
                    $tmp =  json_decode($data->dodatno);
                    $text = '';
                    foreach ($tmp as $key=>$value){
                        $text.= $key.': '.  $value .'<br>';
                    }
                    return $text;

                }
            ],
            [
                'format' =>'raw',
                'header'=>'Nalaz',
                'value' => function($data){
                    if($data->zapis0->vrsta0->naziv == 'nalaz')
                        return Html::a('<i class="fa fa-file-pdf-o" style="font-size:24px"></i>',\yii\helpers\Url::to(['/dijete-zapis/zapispdf','id'=>$data->id]));
                    else
                        return;
                },

            ]
        ],
    ]); ?>

    <h3><?= Yii::t('app','Kartoni psihologa')?></h3>
    <?= GridView::widget([
        'dataProvider' => $psiholog,
        //'filterModel' => $searchModel,
        'columns' => [
            'zapis0.zapis',
            [
                'format'=>'html',
                'value'=>function($data){
                    $tmp =  json_decode($data->dodatno);
                    $text = '';
                    foreach ($tmp as $key=>$value){
                        $text.= $key.': '.  $value .'<br>';
                    }
                    return $text;

                }
            ],
            [
                'format' =>'raw',
                'header'=>'Nalaz',
                'value' => function($data){
                    if($data->zapis0->vrsta0->naziv == 'nalaz')
                        return Html::a('<i class="fa fa-file-pdf-o" style="font-size:24px"></i>',\yii\helpers\Url::to(['/dijete-zapis/zapispdf','id'=>$data->id]));
                    else
                        return;
                },

            ]
        ],
    ]); ?>

    <h3><?= Yii::t('app','Kartoni pedagoga')?></h3>
    <?= GridView::widget([
        'dataProvider' => $pedagog,
        //'filterModel' => $searchModel,
        'columns' => [
            'zapis0.zapis',
            [
                'format'=>'html',
                'value'=>function($data){
                    $tmp =  json_decode($data->dodatno);
                    $text = '';
                    foreach ($tmp as $key=>$value){
                        $text.= $key.': '.  $value .'<br>';
                    }
                    return $text;

                }
            ],
            [
                'format' =>'raw',
                'value' => function($data){
                    if($data->zapis0->vrsta0->naziv == 'nalaz')
                        return Html::a('<i class="fa fa-file-pdf-o" style="font-size:24px"></i>',\yii\helpers\Url::to(['/dijete-zapis/zapispdf','id'=>$data->id]));
                    else
                        return;
                },

            ]
        ],
    ]); ?>
</div>
</div>
