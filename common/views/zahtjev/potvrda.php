<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use dektrium\user\models\User;
use yii\helpers\ArrayHelper;

$this->title = 'POTVRDA O PREDANOM ZAHTJEVU I PRILOZIMA ZA DIJETE:';
?>

<html>
    <head>
    <style>
        body { color: #000000; }
        td { vertical-align: top;}
        .tablePDF { width:100%; background-color: #ffffff; font-size:11px; border-spacing: 0px;}
        .tablePDF td { text-align:left; background-color: #ffffff; padding:2px 2px 2px 2px; margin: 0;}
        .tablePDf tr.crta td { border-bottom: 1px solid #003399;}
        .tablePDF th { background-color: #ffffff; padding:2px 2px 2px 2px; border:1px solid #003399;}
        .tablePDF td.sredina { text-align:center;}
        .tablePDF tr.desno { text-align:right;}
        .desno { text-align:right;}
        .kv-page-summary-container td{ text-align:right; font-weight:bold;}
        body{}
        img{ padding:5px; border:1px solid #000;}
        .obican{ font-family:Helvetica; font-size:16px;}
        .veliki{ font-family:Helvetica; font-size:24px; text-transform: uppercase;}
        </style>
    </head>
<body>
<table class="tablePDF obican">
    <tr>
        <td style = "width: 300px;">
        <img src="<?=Yii::getAlias('@webroot')?>/img/logo-small.png" style="width: 150px; margin: 0;"><br>
            <?php
            $podaciVrtic=\common\models\Postavke::find()->where(['postavka'=>'vrticPodaci'])->one();
            $nazivVrtica=\common\models\Postavke::find()->where(['postavka'=>'nazivVrtica'])->one();

            ?>
            <?= Yii::t('app','OIB: ').$podaciVrtic->dodatno[0]['OIB']?><br/>
            <?= $nazivVrtica->vrijednost?><br/>
            <?= $podaciVrtic->dodatno[0]['Adresa']?><br/>
            <?= $podaciVrtic->dodatno[0]['ZIP'].' '.$podaciVrtic->dodatno[0]['Mjesto']?><br/>
        </td>
        <td></td><td></td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; padding-top: 50px;">

        POTVRDA O PREDANOM ZAHTJEVU I PRILOZIMA ZA DIJETE:<br>
            <?= Yii::t('app','ZAHTJEV ZA UPIS DJETETA U ').$nazivVrtica->vrijednost?><br>
            <?php if(empty($zahtjev->lokacija)){

            }else{
                $objekt=\common\models\Objekt::find()->where(['id'=>$zahtjev->lokacija])->one();
                echo 'Za lokaciju: '.$objekt->naziv.' '.$objekt->adresa.', '.$objekt->mjesto0->naziv;
            } ?>
        <?php echo $dijete->ime." ".$dijete->prezime; ?>

        </td>
    </tr>
    <tr>
        <td style = "width: 300px; padding-top: 50px;">
        <!-- <strong>URBR:</strong> <?php $model->urbroj;?><br> -->
        <strong>Klasa:</strong> <br>
        <strong>Datum:</strong> <?php echo date('d.m.Y.',time())?><br>
        <strong>Šifra djeteta:</strong> <?=$model->urbroj;?><br>
        </td>
        <td></td><td></td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; padding-top: 50px;">
        <center>
        Predani prilozi
        </center>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; border: 1px solid black; padding: 30px;">
        <?php
        if(empty($model->predani_obavezni_dokumenti)){
            foreach ($dokumenti as $dokument) {


                ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php echo is_object($dokument)?$dokument->naziv:$dokument; ?><br><br>
                <?php
            }
        }else{
        foreach ($model->predani_obavezni_dokumenti as $dokument) {
            echo $dokument.'<br />';
            }
        }


        ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; padding-top: 30px;">
        Napomena: <?php echo isset($model->napomena)?$model->napomena:''; ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="width: 100%; padding-top: 200px;">
        Ova potvrda izdana je elektroničkim putem i valjana je bez žiga i potpisa
        </td>
    </tr>
</table>
</body>
</html>
