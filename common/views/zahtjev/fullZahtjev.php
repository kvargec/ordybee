<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
$zahtjevId=$zahtjev->id;

$teskoceData = ['a' => Yii::t('app','oštećenja vida'),
    'b' => Yii::t('app','oštećenja sluha'),
    'c' => Yii::t('app','poremećaji govorno –glasovne komunikacije i specifične teškoće u učenju'),
    'd' => Yii::t('app','tjelesni invaliditet'),
    'e' => Yii::t('app','intelektualne teškoće (sindromi…)'),
    'f' => Yii::t('app','poremećaji u ponašanju uvjetovani organskim faktorima, ADHD'),
    'g' => Yii::t('app','poremećaj socijalne komunikacije; poremećaj iz autističnog spektra; autizam'),
    'h' => Yii::t('app','postojanje više vrsta i stupnjeva teškoća u psihofizičkom razvoju')];

$dijagnostikaData = [
    'a' => Yii::t('app','vještačenjem stručnog povjerenstva socijalne skrbi'),
    'b' => Yii::t('app','nalazom i mišljenjem specijalizirane ustanove'),
    'c' => Yii::t('app','nalazom i mišljenjem ostalih stručnjaka')
];
?>
<div class="shop-create site-about">

    <h1><?= Yii::t('app','Podaci u zahtjevu za upis')?></h1>

    <div class="col s12 m12 l4">

        <blockquote>
            <?php $vrtic=\common\models\Postavke::find()->where(['postavka'=>'nazivVrtica'])->one()?>
            <p><?= Yii::t('app','Unijeli ste sve potrebne podatke za predaju zahtjeva za upis djeteta u ').$vrtic->vrijednost ?><br><?= Yii::t('app','Nakon što pregledate sve podatke, pošaljite prijavu za upisom na poveznicu Pošalji zahtjev')?>  </p>
        </blockquote>


    </div>

    <div class="col s12 m12 l8">
        <h4><?= Yii::t('app','Pregled unešenih podataka')?></h4>
        <div class="panel panel-success">
            <div class="panel-heading">
                <?php echo $zahtjev->dijete0->ime.', '.$zahtjev->dijete0->prezime.' (Željeni datum upisa:'.$zahtjev->zeljeni_datum.') '; ?>
                <hr />
                Željena lokacija upisa:
                <?php
                $temp="Sve lokacije";
                if($zahtjev->lokacija>0){
                    $objekt=\common\models\Objekt::find()->where(['id'=>$zahtjev->lokacija])->one();
                    $temp=$objekt->fullPodaci();
                }
                echo $temp;

                ?>
            </div>
            <div class="panel-body">

                <?= DetailView::widget([
                    'model' => $dijete,
                    'attributes' => [
                        'ime',
                        'prezime',
                        'drzavljanstvo',
                        'spol',
                        'dat_rod',
                        'oib',
                        [
                            'label'=>'Mjesto stanovanja/boravišta',
                            'format'=>'html',
                            'value'=>function($model){
                                $temp='';
                                $temp.=$model->adresa.'<br/>';
                                if(!empty($model->cetvrt)){
                                    return $temp.$model->mjesto.', '.$model->cetvrt;
                                }else{
                                    return $temp.$model->mjesto;
                                }
                            }
                        ],
                        [
                            'label'=>'Prebivalište',
                            'value'=>function($data){
                                if($data->prebivaliste_jednako_boraviste==='N'){
                                    return $data->adresa_prebivalista.', '.$data->prebivaliste;
                                }else{
                                    return $data->adresa.',  '.$data->mjesto;
                                }
                            }
                        ],

                        [
                            'label'=>'Brat/sestra u vrtiću',
                            'value'=>function($data){
                                return $data->sestra=='D'?'Da':'Ne';
                            }
                        ],
                        [
                            'label'=>'Lista čekanja',
                            'value'=>function($data){
                                return $data->cekanje=='D'?'Da, godina:'.$data->god_cekanja:'Ne';
                            }
                        ],
                        'razvoj',
                        'vrstaPrograma.vrsta',
                        [
                            'label'=>'Teškoće u razvoju',
                            'value'=>function($data) use ($teskoceData){
                                $podaci=json_decode($data->teskoce);
                                $tekst=array();
                                if(is_array($podaci)) {
                                    foreach ($podaci as $pod) {
                                        $tekst[] = $teskoceData[$pod];

                                    }
                                }
                                return implode(", ",$tekst);
                            }
                        ],
                        [
                            'label'=>'Dijagnostički postupak za utvrđivanje teškoća',
                            'value'=>function($data) use ($dijagnostikaData){
                                $podaci=json_decode($data->dijagnostika);

                                $tekst=array();
                                if(is_array($podaci)){
                                    foreach($podaci as $pod){
                                        $tekst[]=$dijagnostikaData[$pod];

                                    }
                                }
                                return implode(", ",$tekst);
                            }
                        ],
                        'druge_potrebe',
                        'posebne',
                    ],
                ]) ?>
            </div>

        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <?= Yii::t('app','Roditelji')?>

            </div>
            <div class="panel-body">
                <?= yii\grid\GridView::widget([
                    'dataProvider' => $roditelji,
                    'columns' => [
                        [
                            'label'=>'Osnovni podaci',
                            'format'=>'html',
                            'value'=>function($data){
                                return $data->osnovniPodaci("N");
                            }
                        ],
                        [
                            'label'=>'Boravište/stanovanje',
                            'format'=>'html',
                            'value'=>function($data){
                                return $data->adresa.',  '.$data->mjesto;
                            }
                        ],
                        [
                            'label'=>'Prebivalište',
                            'format'=>'html',
                            'value'=>function($data){
                                if($data->prebivaliste_jednako_boraviste==='N'){
                                    return $data->adresa_prebivalista.', '.$data->prebivaliste;
                                }else{
                                    return $data->adresa.',  '.$data->mjesto;
                                }
                            }
                        ],
                        [
                            'label'=>'Podaci o poslodavcu/zanimanje',
                            'format'=>'html',
                            'value'=>function($data){
                                $tekst='Zanimanje: '.$data->zanimanje.'<br />';
                                $tekst.='Radno vrijeme: '.$data->radno.'<br />';
                                $tekst.=''.$data->poslodavaca.'<br />';
                                $tekst.=''.$data->adresa_poslodavca.'<br />';
                                return $tekst;
                            }
                        ],
                        [
                            'label'=>'Kontakt',
                            'format'=>'html',
                            'value'=>function($data){
                                return $data->mobitel.'<br />'.$data->email;
                            }
                        ],


                        /* ['class' => 'yii\grid\ActionColumn',
                                'template' => '{update}',
                                // 'visible' => Yii::$app->user->can('admin'),
                                'buttons' => [

                                    'update' => function ($url, $data) {

                                        $url2 = \Yii::$app->urlManager->createUrl(['roditelj/update', 'id' => $data->id]);
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url2, [
                                            'title' => Yii::t('app', 'Uredi roditelja'),
                                            'class' => 'text-center block'
                                        ]);
                                    },

                                ],
                            ],*/
                    ],
                    'showHeader' => true,
                    'summary'=>'',
                ])
                ?>

            </div>

        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <?= Yii::t('app','Ostala djeca u obitelji')?>

            </div>
            <div class="panel-body">
                <?= yii\grid\GridView::widget([
                    'dataProvider' => $siblings,
                    'columns' =>[
                        'ime',
                        'prezime',
                        'spol',
                        'dat_rod',
                        'oib',
                        'drzavljanstvo',
                        [
                            'label'=>'Boravište/stanovanje',
                            'value'=>function($data){
                                return $data->adresa.',  '.$data->mjesto;
                            }
                        ],
                        [
                            'label'=>'Prebivalište',
                            'value'=>function($data){
                                if($data->prebivaliste_jednako_boraviste==='N'){
                                    return $data->adresa_prebivalista.', '.$data->prebivaliste;
                                }else{
                                    return $data->adresa.',  '.$data->mjesto;
                                }
                            }
                        ],

                    ],
                    'showHeader' => true,
                    'summary'=>'',
                ])
                ?>
            </div>

        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <?= Yii::t('app','Dodani dokumenti')?>
            </div>
            <div class="panel-body">
                <?= yii\grid\GridView::widget([
                    'dataProvider' => $documents,
                    'columns' => [
                        'naziv',
                        'filename',
                    ],
                    'showHeader' => true,
                    'summary'=>'',
                ])
                ?>
            </div>
        </div>
    </div>

    <!-- <p>OSTALA DOKUMENTACIJA :</p> -->
    <hr>
    <strong><?= Yii::t('app','Roditelj na razgovor treba ponijeti:')?></strong><br><br>
    <ul>
        <li><?= Yii::t('app','Potvrdu o obavljenom sistematskom pregledu')?></li>
        <li><?= Yii::t('app','Presliku iskaznice imunizacije te original iskaznicu na uvid')?></li>
        <li><?= Yii::t('app','Kopiju zdravstvene iskaznice djeteta')?></li>
        <li><?= Yii::t('app','Kopiju medicinske/logopedske dokumentacije (ukoliko postoji)')?></li>
        <li><?= Yii::t('app','Kopiju rješenje Centra za socijalnu skrb te nalaze i mišljenja tijela vještačenja (ukoliko
                postoji)')?></li>
    </ul>
    <hr />

    <strong><?php
        $izjava=\common\models\Postavke::find()->where(['postavka'=>'izjava'])->one();
        if(!empty($izjava)) {echo $izjava->vrijednost; }

        ?><br>
        <?= Yii::t('app','PREDAJOM ZAHTJEVA DIJETE NIJE UPISANO U DJEČJI VRTIĆ – DIJETE JE UPISANO U VRTIĆ U TRENUTKU KAD
                    RODITELJ POTPIŠE UGOVOR S DJEČJIM VRTIĆEM')?><br>
        <?= Yii::t('app','DJEČJI VRTIĆ ZADRŽAVA PRAVO RASPOREDA DJETETA U SKUPINE I OBJEKTE')?><br></strong><br>


    <div class="shop-form">
        <div class="col s12 m12 l12">

            <div class="col s12 m12 l6">
            </div>
            <div class="col s12 m12 l6">
            </div>

        </div>
    </div>
</div>
</div>
