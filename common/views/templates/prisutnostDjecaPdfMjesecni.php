<?php
/**
 * @var \common\models\Zaposlenik[] $zaposlenici
 *
 */
?>
<head>
	<style>
        body {
            color: #000000;
            font-size: 14px;
        }

        h1 {
            text-align: center;
        }
		table{
            border-spacing: 0px;
            border-collapse: separate;
		}
        .recordtable, td {
            border: 1px solid black;
			padding: 0;

        }
		p {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: sans-serif;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .bold {
            font-weight: bold
        }

        .red {
            color: #eb4b50
        }

        .left {
            text-align: left
        }

        .right {
            text-align: right
        }

        .bg-grey {
            background: rgb(194, 194, 194)
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin: 20px 0
        }

        th,
        td {
            border-collapse: collapse;
            border: 1px solid grey;
        }

        .no-border th,.no-border td {border: none;}
        th,
        td {
            padding: 5px;
    
        }

        thead tr:last-of-type th {
            font-size: 14px;
        }
        td:first-child { text-align: left }

	</style>
</head>
<?php 
    $ime=\common\models\Postavke::find()->where(['postavka'=>'nazivVrtica'])->one();
    $podaci=\common\models\Postavke::find()->where(['postavka'=>'vrticPodaci'])->one();
    $ravnatelj=\common\models\Postavke::find()->where(['postavka'=>'naziv Ravnatelj'])->one();
    //print("<pre>".print_r($podaci,true)."</pre>");die(); 
?>


<h1><?= Yii::t('app', 'Prisutnost zaposlenika za period {mjesec}.{godina}.', ['mjesec' => $mjesec['naziv'], 'godina' => $mjesec['godina']]) ?></h1>

<body>
    <p class="uppercase bold"><?= $ime->vrijednost;?></p>
    <p class="bold">Adresa: <?php if (!empty($podaci->dodatno)) {if (is_array($podaci->dodatno[0]) && array_key_exists('Adresa', $podaci->dodatno[0])) {echo $podaci->dodatno[0]['Adresa'];}} ?>, <?php if (!empty($podaci->dodatno)) {if (is_array($podaci->dodatno[0]) && array_key_exists('Mjesto', $podaci->dodatno[0])) {echo $podaci->dodatno[0]['Mjesto'];}} ?></p>
    <p class="bold">OIB: <?php if (!empty($podaci->dodatno)) {if (is_array($podaci->dodatno[0]) && array_key_exists('OIB', $podaci->dodatno[0])) {echo $podaci->dodatno[0]['OIB'];}} ?></p>
    <p class="bold">
		<?= Yii::t('app', 'Evidencija o prisutnosti djece za <span class="red uppercase">{mjesec}.{godina}.</span> godine', ['mjesec' => $mjesec['naziv'], 'godina' => $mjesec['godina']]) ?>
	</p>
    <p>Dostavljeno: <span><?= date("d.m.Y.");?></span><br></p>
    <p><br/>Skupina: <?= $skupina;?></p>
    <table class="table table-bordered table-striped desktop">
        <tr>
            <th class="bg-grey">#</th>
            <th class="bg-grey">Ime i prezime</th>
            <th class="bg-grey">Broj prisutnih dana</th>
        </tr>
        <?php
        $id=0;
        foreach ($prisutnost as $redak) {
        $id++;
        ?>
        <tr>
            <td style="text-align:center">
                <?php echo $id ?>
            </td>
            <td>
                <?php echo $redak[0] ?>
            </td>
            <td style="text-align:center">
                <?php echo $redak[1]; ?>
            </td>     
        </tr>
        <?php 
        } 
        ?>
    </table>
    
    <table class="no-border">
        <tr>
            <td class="left">
                <?php if (!empty($podaci->dodatno)) {if (is_array($podaci->dodatno[0]) && array_key_exists('Mjesto', $podaci->dodatno[0])) {echo $podaci->dodatno[0]['Mjesto'];}} ?>, <span><?= date("d.m.Y.");?></span>
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="left">
                KLASA : 

            </td>
            <td class="right bold">Ravnateljica:</td>
        </tr>
        <tr>
            <td class="left">
                URBROJ: 
            </td>
            <td class="right"><?= $ravnatelj->vrijednost;?></td>
        </tr>
    </table>
</body>

</html>