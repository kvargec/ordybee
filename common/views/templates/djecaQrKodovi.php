<?php

use Da\QrCode\QrCode;
use Da\QrCode\Format\BookMarkFormat;
use yii\helpers\Url;
?>
<html>
<head>
    <style>
        .table-bordered, .table-bordered td, .table-bordered th {
            border: 1px solid #ddd;
            text-align: left;
        }
        .table-bordered {
            border-collapse: collapse;
            width: 100%;
        }
        .table-bordered th,.table-bordered td {
            padding: 15px;
        }
        .table{
            width: 100%;
        }
        img{
            display: block;
            margin-right: auto;
            margin-left: auto;
            width: 50%;

        }
    </style>
</head>
<body>
<table >
<?php
foreach ($djeca as $dijete) {
    //$format = new BookMarkFormat(['title' => $dijete->ime.' '.$dijete->prezime, 'url' => Url::to(['dijete/view', 'id' => $dijete->id], true)]);
    //$format = new BookMarkFormat(['title' => $dijete->ime.' '.$dijete->prezime, 'url' => Url::to(['dijete/zabiljeziprisutnost', 'id' => $dijete->id])]);
    $format = new BookMarkFormat(['title' => $dijete->ime.' '.$dijete->prezime, 'url' => Url::to(['dijete/zabiljezi-prisutnost', 'id' => $dijete->id])]);
    //print("<pre>".print_r(Url::to(['dijete/view', 'id' => $dijete->id], true),true)."</pre>");die();

    $qrkod=(new QrCode($format))->setSize(250)->setMargin(1)->useForegroundColor(0, 0, 0);
    ?>
    <tr>
        <td><br /><br />
        <div>Ime: <span><?=$dijete->ime?></span></div>
        <div>Prezime: <span><?=$dijete->prezime?></span></div><br /><br />
        </td>
    </tr>
    <tr>
        <td style="text-align:left" colspan="2"><img src="<?= $qrkod->writeDataUri() ?>" /></td><br /> 
        <!-- <td style="text-align:left" colspan="2"><img src="<?//= Url::to(['api/controllers/dijete/zabiljeziprisutnost', 'id' => $dijete->id]) ?>" /></td><br /> -->
    </tr>
    <br />
    <br />
    
<?php
}
?>
</table>
</body>
</html>

