<head>
    <style>
        body {
            color: #000000;
            font-size: 14px;
            font-family: sans-serif;
        }

        .podaci {
            position: absolute;
            top: 230px;
            display: flex;
            width: 100%;
        }

        .lijevo,
        .desno {
            width: 25%;
            display: inline-block;
        }

        .sredina {
            width: 50%;
            display: inline-block;
        }

        table {
            padding-left: 15px;
            width: 89%;
        }

        td {
            padding: 3px;
            padding-bottom: 10px;

        }

        td.model {
            padding-top: 10px;
        }

        .podaci td {
            text-align: right;
        }

        .podaci td.bigger {
            font-size: 16px
        }

        .podaci tr td:nth-of-type(2) {
            padding-right: 38mm;
        }
    </style>
</head>
<?php
/*
$mjesec mjesec i godina, rezultat Utils::getMjesecPed() actiona
$skupina skupina model
$dijete dijete model
$roditelji oba modela za roditelje
$podaciVrtića['nazivVrtica'] - redak nazivVrtica iz Postavke 
$podaciVrtića['naziv Ravnatelj'] - redak naziv Ravnatelj iz Postavke 
$podaciVrtića['vrticPodaci'] - redak vrticPodaci iz Postavke 
*/
$ime = $podaciVrtica['nazivVrtica']; //\common\models\Postavke::find()->where(['postavka'=>'nazivVrtica'])->one();
$podaci = $podaciVrtica['vrticPodaci']; //\common\models\Postavke::find()->where(['postavka'=>'vrticPodaci'])->one();
$ravnatelj = $podaciVrtica['naziv Ravnatelj']; //\common\models\Postavke::find()->where(['postavka'=>'naziv Ravnatelj'])->one();
$dodatno = $podaci['dodatno']; //dodatno stupac u potrazi za IBAN-om
$jsonPodaci = json_decode($dodatno, TRUE);
$key = "IBAN";
$sljedecimj = $mjesec['mjesec'] + 1;
$sljgodina = $mjesec['godina'];
if ($sljedecimj == 13) {
    $sljedecimj = 1;
    $sljgodina = $mjesec['godina'] + 1;
}
$pozivNaBroj = str_pad($dijete->id, 6, '0', STR_PAD_LEFT);
?>
<h2><?= $ime['vrijednost'] ?></h2>
<p>Skupina: <?= $skupina->naziv ?></p>
<p><?php if (isset($roditelji)) {
        echo $roditelji->ime . ' ' . $roditelji->prezime;
    } ?></p>
<p>Adresa: <?php if (isset($roditelji)) {
                $roditelji->adresa . ', ' . $roditelji->mjesto;
            } ?></p>

<img src="./uplatnica.jpg">
<div class="podaci">
    <table>
        <tr>
            <td width="23%" style="line-height:15px"><?php if (isset($roditelji)) {
                                                            $roditelji->ime . ' ' . $roditelji->prezime;
                                                        } ?></td>
            <td width="17%" style="font-size:16px;text-align:right;letter-spacing: 3px;">HRK</td>
            <td width="30%" style="font-size:18px;text-align:right;padding-right:7px;letter-spacing: 3px;"><span></span><span>****<?php if (isset($konacnaCijena)) {
                                                                                                                                        echo Yii::$app->formatter->asDecimal($konacnaCijena, 2);
                                                                                                                                    } ?></span></td>
            <td width="7%"></td>
            <td width="21%" style="text-align:right">HRK ******<?php if (isset($konacnaCijena)) {
                                                                    echo Yii::$app->formatter->asDecimal($konacnaCijena, 2);
                                                                } ?></td>

        </tr>
        <tr>
            <td width="23%"><?php if (isset($roditelji)) {
                                $roditelji->adresa;
                            } ?></td>
            <td width="16%"></td>
            <td width="30%"></td>
            <td width="7%"></td>
            <td width="21%" style="text-align:right"><?php if (isset($roditelji)) {
                                                            $roditelji->ime . ' ' . $roditelji->prezime;
                                                        } ?></td>
        </tr>
        <tr>
            <td width="23%"><?php if (isset($roditelji)) {
                                $roditelji->mjesto;
                            } ?></td>
            <td width="16%"></td>
            <td width="30%"></td>
            <td width="7%"></td>
            <td width="21%" style="text-align:right">&nbsp;</td>
        </tr>

        <tr>
            <td width="23%"></td>
            <td width="16%"></td>
            <td width="30%" style="font-size:16px;text-align:right;padding-right:7px;letter-spacing: 4px;"><?php if (isset($jsonPodaci[0][$key])) {
                                                                                                                echo $jsonPodaci[0][$key];
                                                                                                            } ?></td>
            <td width="7%"></td>
            <td width="21%" style="text-align:right"><?php if (isset($jsonPodaci[0][$key])) {
                                                            echo $jsonPodaci[0][$key];
                                                        } ?></td>
        </tr>
        <tr>
            <td width="23%"></td>
            <td width="16%" style="font-size:16px;padding-right:7px;letter-spacing: 4px;">HR63</td>
            <td width="30%" style="font-size:16px;text-align:right;padding-right:7px;letter-spacing: 3px;"><span></span><span><?= $pozivNaBroj ?></span></td>
            <td width="7%"></td>
            <td width="22%" style="text-align:right" class="model">HR63 <?= $pozivNaBroj ?></td>
        </tr>
        <tr>
            <td width="23%">GU za obrazovanje<br>Sredstva za predskol odgoj<br><?php if (isset($roditelji)) {
                                                                                    $roditelji->mjesto;
                                                                                } ?></td>
            <td width="16%"></td>
            <td width="30%" style="padding-top:0"><?= $ime['vrijednost'] ?><br>akontacija: <?= $mjesec['mjesec'] ?>/<?= $mjesec['godina'] ?><br><span style="font-size:10px">oslobođeno plaćanja PDV-a po čl.39.st.1. t. i. Zakona o PDV-u</span><br>Platiti do: 25.<?= $sljedecimj ?>.<?= $sljgodina ?>.</td>
            <td width="7%"></td>
            <td width="21%"><?= $ime['vrijednost'] ?><br>skupina: <?= $skupina->naziv ?><br>akontacija: <?= $mjesec['mjesec'] ?>/<?= $mjesec['godina'] ?></td>
        </tr>
        <tr>
            <td colspan="2" style="padding:5px">
                <!--<img src="./barcode.gif" width="22%">-->
            </td>
            <td colspan="3"></td>

        </tr>
    </table>
    <!-- <div class="lijevo">
        <p class="valuta-iznos">HRK ******234,00</p>
<p class="ime-prezime">Krešo Vargec</p>
<p class="iban">HR23340002569874</p>
<p class="model-poziv">HR63 7803-25458-24380030610</p>
<p class="opis">Ime vrtića<br>skupina: ime skupine<br>akontacija: 06/2021</p>
    </div>
    <div class="sredina">
        <p class="valuta-iznos">HRK ******234,00</p>
<p class="ime-prezime">Krešo Vargec</p>
<p class="iban">HR23340002569874</p>
<p class="model-poziv">HR63 7803-25458-24380030610</p>
<p class="opis">Ime vrtića<br>skupina: ime skupine<br>akontacija: 06/2021</p>
    </div>
    <div class="desno">
        <p class="valuta-iznos">HRK ******234,00</p>
<p class="ime-prezime">Krešo Vargec</p>
<p>&nbsp;</p>
<p class="iban">HR23340002569874</p>
<p class="model-poziv">HR63 7803-25458-24380030610</p>
<p class="opis">Ime vrtića<br>skupina: ime skupine<br>akontacija: 06/2021</p>
    </div> -->

</div>