<?php

/**
 *@var \common\models\Dijete[] $djeca
 */

?>
<h1><?= Yii::t('app', 'Prisutnost djece za skupinu {skupina} za period {pocetak} - {kraj}', ['skupina' => $skupina->naziv, 'pocetak' => Yii::$app->formatter->asDate($datumi['pon'], 'php:j.n.Y.'), 'kraj' => Yii::$app->formatter->asDate($datumi['pet'], 'php:j.n.Y.')]) ?></h1>
<table class="table table-striped table-bordered desktop">
    <tr>
        <td><?= Yii::t('app', 'Dijete') ?></td>
        <td style="text-align: center"><?= Yii::t('app', 'PON') ?><br> <?php echo date('j.n.Y', strtotime($datumi['pon'])) ?>&nbsp;</td>
        <td style="text-align: center"><?= Yii::t('app', 'UTO') ?><br> <?php echo date('j.n.Y', strtotime($datumi['uto'])) ?>&nbsp;</td>
        <td style="text-align: center"><?= Yii::t('app', 'SRI') ?><br> <?php echo date('j.n.Y', strtotime($datumi['sri'])) ?>&nbsp;</td>
        <td style="text-align: center"><?= Yii::t('app', 'ČET') ?><br> <?php echo date('j.n.Y', strtotime($datumi['cet'])) ?>&nbsp;</td>
        <td style="text-align: center"><?= Yii::t('app', 'PET') ?><br> <?php echo date('j.n.Y', strtotime($datumi['pet'])) ?>&nbsp;</td>
    </tr>
    <?php
    foreach ($djeca as $dijete) {
        echo '<tr><td>' . $dijete->ime . ' ' . $dijete->prezime . '</td>';
        foreach ($datumi as $key => $datum) {
            $dodatak = '';
            $dijete_prisutno = '';
            $prisutnost = $dijete->getPrisutnost()->where(['datum' => $datum])->one();
            if (isset($prisutnost)) {
                if ($prisutnost->status == 0) {

                    if ($prisutnost->razlog_izostanka != null) {
                        $dodatak = Yii::t('app', 'Razlog izostanka: ') . $prisutnost->razlog_izostanka;
                    }
                    $dijete_prisutno = false;
                } else {
                    $dijete_prisutno = true;
                }
            } else {
                $dijete_prisutno = false;
            } ?>
            <td style="text-align: center">
                <?= $dijete_prisutno ? Yii::t('app', 'Da') : Yii::t('app', 'Ne') ?>
                <?= isset($dodatak) ? '<br>' . $dodatak : '' ?>
            </td>

    <?php }
        echo '</tr>';
    }
    ?>

</table>