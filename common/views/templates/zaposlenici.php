<head>
    <style>
        body {
            color: #000000;
            font-size: 14px;
        }

        h1 {
            text-align: center;
        }

        table {
            border-spacing: 0px;
            border-collapse: separate;
        }

        p {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: sans-serif;
        }

        table {
            width: 100%;
        }

        table,
        th,
        td {
            border-collapse: collapse;
            border: 1px solid black;
        }

        th,
        td {
            padding: 5px;

        }

        thead tr:last-of-type th {
            font-size: 14px;
        }

        td:first-child {
            text-align: left
        }
    </style>
</head>
<table>
    <tr>
        <td>#</td>
        <td>Username</td>
        <td>Password</td>
        <td>Ime Zaposlenika</td>
    </tr>
    <?php
    $count = 1;
    foreach ($zaposlenici as $zaposlenik) {
    ?>
        <tr>
            <td>
                <?php
                echo $count;
                ?>
            </td>
            <td>
                <?php
                echo $zaposlenik['username'];
                ?>
            </td>
            <td>
                <?php
                echo $zaposlenik['password'];
                ?>
            </td>
            <td>
                <?php
                echo $zaposlenik['ime_zaposlenika'];
                ?>
            </td>
        </tr>
    <?php
        $count++;
    }
    ?>
</table>