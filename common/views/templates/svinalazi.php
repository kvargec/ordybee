<?php
/* @var backend\controllers\DijeteZapisController.php $dijete as $dijete */
/* @var backend\controllers\DijeteZapisController.php $svizapisi as $zapisi */
/* @var backend\controllers\DijeteZapisController.php $tip as $tip */
?>
<html>
<head>
    <style>
        body { color: #000000; font-size: 14px; }
        h1 { text-align:center; }
        ul { list-style-type: none; }
        .recordtable {  margin-left: auto; margin-right: auto;}
        .recordtable, td {border: 1px solid black;}
        .recordtable tr:nth-child(even){background-color: #f2f2f2;}
    </style>
</head>
<body>
    <div class="container">
        <h1><?php echo $dijete->ime . ' ' . $dijete->prezime . ' - '  . $tip ?></h1>
        <table class="recordtable">
        <?php
            foreach($zapisi as $item) {
                $tmp = json_decode($item->dodatno);
                //var_dump($zapisi);die()          
                ?>
                <tr>
                    <td><?= Yii::t('app','Ime i prezime: ')?><?php echo $item->zapis0->zapis?></td>
                    <td>
                    <?php
                    foreach($tmp as $key => $value) {
                        ?>
                        <ul>
                            <li><?php echo $key . ': ' . $value;?></li>
                        </ul> 
                    <?php
                    }
                    ?>
                    </td>      
                </tr>
            <?php 
            }
        ?>
        </table>
    </div>
</body>





