<?php
/**
 * @var \common\models\Zaposlenik[] $zaposlenici
 *
 */
?>
<head>
    <style>
        body { color: #000000; font-size: 14px; }
        h1 { text-align:center; }
        .recordtable, td {border: 1px solid black;}
    </style>
</head>
<h1><?= Yii::t('app', 'Prisutnost zaposlenika za period {pocetak} - {kraj}', ['pocetak' => Yii::$app->formatter->asDate($datumi['pon'], 'php:n.j.Y.'), 'kraj' => Yii::$app->formatter->asDate($datumi['pet'], 'php:j.n.Y.')]) ?></h1>


<table class="table table-bordered table-striped desktop recordtable">

    <tr>
        <td><?= Yii::t('app',  'Zaposlenik') ?></td>
        <td style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday this week')) ?>&nbsp;</td>
        <td style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday this week')) ?>&nbsp;</td>
        <td style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday this week')) ?>&nbsp;</td>
        <td style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday this week')) ?>&nbsp;</td>
        <td style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday this week')) ?>&nbsp;</td>
    </tr>
        <?php
        foreach ($zaposlenici as $zaposlenik) {?>
            <tr>
                <td><?= $zaposlenik->ime.' '.$zaposlenik->prezime ?></td>
      <?php      foreach ($datumi as $datum) {
                $prisutnost = $zaposlenik->getPrisutnost()->where(['datum' => $datum])->one();
                if (isset($prisutnost)){
                    $content = '';
                    $content.=  Yii::t('app','Vrijeme početka: ') . $prisutnost->pocetak.'<br><br>';
                    $content.= Yii::t('app','Vrijeme kraja: ') . $prisutnost->kraj.'<br><br>';
                    $start = new DateTime($prisutnost->pocetak);
                    $kraj = new DateTime($prisutnost->kraj);
                    $razlika = $kraj->diff($start);
                    $content.= Yii::t('app','Ukupno: ') . $razlika->format('%hh %imin').'<br><br>';
                }else{
                    $content = Yii::t('app', 'Ne');
                }
                echo '<td style="text-align: center">'.$content.'</td>';
                } ?>
            </tr>
      <?php }
        ?>
</table>
