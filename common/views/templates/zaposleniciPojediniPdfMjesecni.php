<?php

/**
 * @var \common\models\Zaposlenik[] $zaposlenici
 *
 */
?>

<head>
    <style>
        body {
            color: #000000;
            font-size: 16px;
        }

        h1 {
            text-align: center;
        }

        table {
            border-spacing: 0px;
            border-collapse: separate;
        }

        .recordtable,
        td {
            border: 1px solid black;
            padding: 0;

        }

        p {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: sans-serif;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .bold {
            font-weight: bold
        }

        .red {
            color: #eb4b50
        }

        .left {
            text-align: left
        }

        .right {
            text-align: right
        }

        .bg-grey {
            background: rgb(194, 194, 194)
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin: 20px 0
        }

        .table-striped tr:nth-of-type(2n) td {
            background-color: #cadbe6;
        }

        th,
        td {
            border-collapse: collapse;
            border: 1px solid grey;
        }

        .no-border th,
        .no-border td {
            border: none;
        }

        th,
        td {
            padding: 5px;

        }

        thead tr:last-of-type th {
            font-size: 14px;
        }

        td:first-child {
            text-align: left
        }
    </style>
</head>
<?php
$ime = \common\models\Postavke::find()->where(['postavka' => 'nazivVrtica'])->one();
$podaci = \common\models\Postavke::find()->where(['postavka' => 'vrticPodaci'])->one();
$ravnatelj = \common\models\Postavke::find()->where(['postavka' => 'naziv Ravnatelj'])->one();
$tip = \common\models\Postavke::find()->where(['postavka' => 'prisutnostType'])->one();

?>
<p class="uppercase "><?= $ime->vrijednost; ?></p>
<p class="">Adresa: <?php if (!empty($podaci->dodatno)) {
                        if (is_array($podaci->dodatno[0]) && array_key_exists('Adresa', $podaci->dodatno[0])) {
                            echo $podaci->dodatno[0]['Adresa'];
                        }
                    } ?>,<br> <?php if (!empty($podaci->dodatno)) {
                                    if (is_array($podaci->dodatno[0]) && array_key_exists('ZIP', $podaci->dodatno[0])) {
                                        echo $podaci->dodatno[0]['ZIP'];
                                    }
                                } ?>
    <?php if (!empty($podaci->dodatno)) {
        if (is_array($podaci->dodatno[0]) && array_key_exists('Mjesto', $podaci->dodatno[0])) {
            echo $podaci->dodatno[0]['Mjesto'];
        }
    } ?></p>
<p class="">OIB: <?php if (!empty($podaci->dodatno)) {
                        if (is_array($podaci->dodatno[0]) && array_key_exists('OIB', $podaci->dodatno[0])) {
                            echo $podaci->dodatno[0]['OIB'];
                        }
                    } ?></p>



<h1><?= 'Stanje efektive i neefektive za zaposlenika/icu: ' . $zaNaziv ?></h1>



<table class="table table-bordered table-striped desktop">

    <tr>
        <?php
        foreach ($prisutnost[0] as $polje) { ?>
            <th>
                <?php echo $polje ?>
            </th>
        <?php } ?>
    </tr>
    <?php
    for ($i = 1; $i < count($prisutnost); $i++) {
    ?>
        <tr <?php if ($i % 2 == 0) {
                echo 'style="background-color: #cadbe696;"';
            } ?>>
            <?php
            for ($j = 0; $j < count($prisutnost[0]); $j++) {
                $textCenter = $j > 0 ? ' style="text-align:center; font-size:16px"' : '';
            ?>
                <td <?php echo $textCenter ?>>
                    <?php

                    echo isset($prisutnost[$i][$j]) ? $prisutnost[$i][$j] : '/';


                    ?>
                </td>
            <?php } ?>
        </tr>


    <?php }
    ?>

</table>
<table class="no-border">
    <tr>
        <td class="left">
            <?php if (!empty($podaci->dodatno)) {
                if (is_array($podaci->dodatno[0]) && array_key_exists('Mjesto', $podaci->dodatno[0])) {
                    echo $podaci->dodatno[0]['Mjesto'];
                }
            } ?>, <span><?= date("d.m.Y."); ?></span>
        </td>
        <td></td>
    </tr>
    <tr>
        <td class="left">
            KLASA :

        </td>
        <td class="right bold">Ravnateljica:</td>
    </tr>
    <tr>
        <td class="left">
            URBROJ:
        </td>
        <td class="right">
            <?php if (isset($ravnatelj)) {
                echo $ravnatelj->vrijednost;
            }
            ?></td>
    </tr>
</table>