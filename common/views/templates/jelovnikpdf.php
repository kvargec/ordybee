<html>

<head>
    <style>
        .table-bordered,
        .table-bordered td,
        .table-bordered th {
            border: 1px solid #ddd;
            text-align: left;
        }

        .table-bordered {
            border-collapse: collapse;
            width: 100%;
        }

        .table-bordered th,
        .table-bordered td {
            padding: 15px;
        }

        .table {
            width: 100%;
        }

        img {
            display: block;
            margin-right: auto;
            margin-left: auto;
            width: 50%;

        }
    </style>
</head>

<body>

    <table class="table">
        <tr>
            <td style="width: 300px;">
                OIB: 44444146578<br />
                Dječji vrtić Vrbovec<br />
                7. svibnja 12a<br />
                10340 Vrbovec<br />
            </td>
            <td colspan="3" style="text-align: right">
                Vrbovec,<br />
                <?php echo date('j.n.Y.', time()) ?>
            </td>
        </tr>
    </table>
    <h3 style="text-align: center"><?= Yii::t('app', 'Jelovnik za period ') ?><?= date('j.n.Y', strtotime($datum['pon'])) ?> - <?= date('j.n.Y', strtotime($datum['pet'])) ?></h3>

    <div style="text-align: center;">
        <?php if (!empty($dodatak)) :
        ?>

            <img style="max-height:120px;" src="<?= Yii::getAlias('@backend/web/media/') . $dodatak->dodatak ?>">
        <?php endif; ?>
    </div>
    <br> <br>
    <table class="table-bordered table-striped table" style="font-size:14px">
        <tr>
            <td></td>
            <th><?= Yii::t('app', 'Ponedjeljak') ?><br><?php echo date('j.n.Y', strtotime($datum['pon'])); ?></th>
            <th><?= Yii::t('app', 'Utorak') ?><br><?php echo date('j.n.Y', strtotime($datum['uto'])); ?></th>
            <th><?= Yii::t('app', 'Srijeda') ?><br><?php echo date('j.n.Y', strtotime($datum['sri'])); ?></th>
            <th><?= Yii::t('app', 'Četvrtak') ?><br><?php echo date('j.n.Y', strtotime($datum['cet'])); ?></th>
            <th><?= Yii::t('app', 'Petak') ?><br><?php echo date('j.n.Y', strtotime($datum['pet'])); ?></th>
        </tr>
        <tr>
            <th sty><?= Yii::t('app', 'Doručak') ?></th>
            <td> <?= $model->dorucak->pon; ?></td>
            <td> <?= $model->dorucak->uto; ?></td>
            <td> <?= $model->dorucak->sri; ?></td>
            <td> <?= $model->dorucak->cet; ?></td>
            <td> <?= $model->dorucak->pet; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('app', 'Ručak') ?></th>
            <td> <?= $model->rucak->pon; ?></td>
            <td> <?= $model->rucak->uto; ?></td>
            <td> <?= $model->rucak->sri; ?></td>
            <td> <?= $model->rucak->cet; ?></td>
            <td> <?= $model->rucak->pet; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('app', 'Užina') ?></th>
            <td> <?= $model->uzina->pon ?></td>
            <td> <?= $model->uzina->uto ?></td>
            <td> <?= $model->uzina->sri ?></td>
            <td> <?= $model->uzina->cet ?></td>
            <td> <?= $model->uzina->pet ?></td>
        </tr>
    </table>

    <h5><?= Yii::t('app', 'NAPOMENA: U slučaju potrebe Vrtić zadržava pravo izmjene jelovnika.') ?></h5>
    <p>
        <?= Yii::t('app', 'Zajutrak: Keksi ili voće ili mlijeko sa žitnim pahuljicama i sl.') ?> <br />
        <?= Yii::t('app', 'Međuobrok: Svježe voće') ?>
    </p>
</body>


</html>