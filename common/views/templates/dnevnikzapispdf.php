<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use common\models\Skupina;
/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>
<html>

<head>
    <style>
        body {
            color: #000000;
        }

        td {
            vertical-align: top;
        }

        .tablePDF {
            width: 100%;
            background-color: #ffffff;
            font-size: 11px;
            border-spacing: 0px;
        }

        .tablePDF td {
            text-align: left;
            background-color: #ffffff;
            padding: 2px 2px 2px 2px;
            margin: 0;
        }

        .tablePDf tr.crta td {
            border-bottom: 1px solid #003399;
        }

        .tablePDF th {
            background-color: #ffffff;
            padding: 2px 2px 2px 2px;
            border: 1px solid #003399;
        }

        .tablePDF td.sredina {
            text-align: center;
        }

        .tablePDF tr.desno {
            text-align: right;
        }

        .desno {
            text-align: right;
        }

        .kv-page-summary-container td {
            text-align: right;
            font-weight: bold;
        }

        img {
            padding: 5px;

        }

        .obican {
            font-family: Helvetica;
            font-size: 16px;
        }

        .veliki {
            font-family: Helvetica;
            font-size: 24px;
            text-transform: uppercase;
        }

        .equal {
            display: flex;
            flex-wrap: wrap;
        }
    </style>
</head>

<body>
    <table class="tablePDF obican">
        <tr>
            <td style="text-align: left">
                <?php echo $dnevnik_rada->dnevnik0->zaposlenik0->fullName ?><br />
                <?php echo $dnevnik_rada->vrsta0->title ?><br />
                <?php echo date('d.m.Y.', time()) ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 100%; padding-top: 50px;">
                <h2><?php echo $dnevnik_rada->zapis ?></h2>
            </td>
        </tr>

    </table>
    <div class="shop-create">
        <div class="row">
            <div class="col s12 m12">
                <table>
                    <?php
                    foreach ($dnevnik_rada->vrijednosti_atributa as $vrsta_key => $vrijednosti) {
                        $skupina = '';
                        $vrsta = '';
                        if (array_key_exists('skupina', $vrijednosti)) {
                            if (!empty($vrijednosti['skupina']))
                                $skupina = Skupina::find()->where(['id' => $vrijednosti['skupina']])->one();
                        }
                        switch ($vrsta_key) {
                            case 'neposredni':
                                $vrsta = 'Neposredni rad';
                                break;
                            case 'posredni':
                                $vrsta = 'Posredni rad';
                                break;
                            case 'vanustanove':
                                $vrsta = 'Rad van ustanove';
                                break;
                            case 'bolovanje':
                                $vrsta = 'Bolovanje';
                                break;
                            case 'slobodno':
                                $vrsta = 'Slobodan dan';
                                break;
                            case 'godisnji':
                                $vrsta = 'Godišnji';
                                break;
                            case 'porodiljni':
                                $vrsta = 'Porodiljni';
                                break;
                            case 'dopust':
                                $vrsta = 'Dopust';
                                break;
                        }
                    ?>
                        <tr>
                            <td style="width: 30%; padding-top: 10px;">Početak:</td>
                            <td style="width: 70%; padding-top: 10px;"> <?php
                                                                        switch ($vrsta_key) {
                                                                            case 'neposredni':
                                                                            case 'posredni':
                                                                            case 'vanustanove':
                                                                                echo $vrijednosti['start'];
                                                                                break;
                                                                            case 'bolovanje':
                                                                            case 'slobodno':
                                                                            case 'godisnji':
                                                                            case 'porodiljni':
                                                                            case 'dopust':
                                                                                echo $vrijednosti['pocetak'];
                                                                                break;
                                                                        }
                                                                        ?></td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-top: 10px;">Kraj:</td>
                            <td style="width: 70%; padding-top: 10px;"> <?php
                                                                        switch ($vrsta_key) {
                                                                            case 'neposredni':
                                                                            case 'posredni':
                                                                            case 'vanustanove':
                                                                                echo $vrijednosti['end'];
                                                                                break;
                                                                            case 'bolovanje':
                                                                            case 'slobodno':
                                                                            case 'godisnji':
                                                                            case 'porodiljni':
                                                                            case 'dopust':
                                                                                echo $vrijednosti['kraj'];
                                                                                break;
                                                                        }
                                                                        ?></td>
                        </tr>
                        <tr>
                            <td style="width: 30%; padding-top: 10px;">Vrsta rada:</td>
                            <td style="width: 70%; padding-top: 10px;"> <?= $vrsta; ?></td>
                        </tr>
                        <?php
                        if (!empty($skupina)) { ?>
                            <tr>
                                <td style="width: 30%; padding-top: 10px;">Skupina:</td>
                                <td style="width: 70%; padding-top: 10px;"><?= $skupina->naziv; ?></td>
                            </tr>
                        <?php } ?>
                        <?php
                        if (array_key_exists('aktivnost', $vrijednosti)) { ?>
                            <tr>
                                <td style="width: 30%; padding-top: 10px;">Aktivnosti: </td>
                                <td style="width: 70%; padding-top: 10px;">
                                    <ul>
                                        <?php
                                        if (is_array($vrijednosti['aktivnost'])) {
                                            foreach ($vrijednosti['aktivnost'] as $aktivnost) {
                                        ?>
                                                <li><?= $aktivnost; ?></li>
                                            <?php
                                            }
                                        } else { ?>
                                            <li><?= $vrijednosti['aktivnost']; ?></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </td>
                            </tr>
                        <?php
                        } ?>
                    <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row equal mt-15 mb-15">
            <?php
            if (isset($dnevnik_rada->attachments)) {
                foreach ($dnevnik_rada->attachments['attachments'] as $slika) {
                    $fileLink = 'dnevnik/' . $dnevnik_rada->id . '/' . $slika;
                    $mime = null;
                    if (file_exists($fileLink)) {
                        $mime = mime_content_type($fileLink);
                    }
                    if ($mime) {
                        if (strstr($mime, "image/")) {
            ?>

                            <img width="30%" class="cover" src="<?= $fileLink ?>">

            <?php
                        }
                    }
                }
            }
            ?>
        </div>
    </div>
</body>

</html>