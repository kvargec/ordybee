<head>
    <style>
        body {
            color: #000000;
            font-size: 14px;
        }

        h1 {
            text-align: center;
        }

        table {
            border-spacing: 0px;
            border-collapse: separate;
        }

        p {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: sans-serif;
        }

        table {
            width: 100%;
        }

        table,
        th,
        td {
            border-collapse: collapse;
            border: 1px solid black;
        }

        th,
        td {
            padding: 5px;

        }

        thead tr:last-of-type th {
            font-size: 14px;
        }

        td:first-child {
            text-align: left
        }
    </style>
</head>
<table>
    <tr>
        <td>#</td>
        <td>Username</td>
        <td>Password</td>
        <td>Ime Roditelja</td>
        <td>Dijete</td>
    </tr>
    <?php
    $count = 1;
    foreach ($roditelji as $roditelj) {
    ?>
        <tr>
            <td>
                <?php
                echo $count;
                ?>
            </td>
            <td>
                <?php
                echo $roditelj['username'];
                ?>
            </td>
            <td>
                <?php
                echo $roditelj['password'];
                ?>
            </td>
            <td>
                <?php
                echo $roditelj['ime_roditelja'];
                ?>
            </td>
            <td>
                <?php
                if (isset($roditelj['dijete'])) {
                    echo $roditelj['dijete'];
                }
                ?>
            </td>
        </tr>
    <?php
        $count++;
    }
    ?>
</table>