<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\DetailView;


//AppAsset::register($this);
//$this->context->layout='@common/views/layouts/pdf';
/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */

//$this->context->layout='@common/views/layouts/pdf';
?>
<style>

    .panel {
        margin-bottom: 20px;
        background-color: #fff;
        border-radius: 4px;
    }
    .panel-heading {
        padding: 10px 15px;
        border-bottom: 1px solid #ddd;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }
    .panel-success > .panel-heading {
         background-color: #e4a40f;
         color: #fff;
     }
    .panel-body {
        padding: 15px;
    }
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }
    .table-bordered {
         border: 1px solid #ddd;
     }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
    table{
        border-spacing: 0;
    }
    th,td {
        text-align: left;
        border: 1px solid #ddd;
        border-spacing: 0;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #ddd;
    }
</style>
<div class="shop-create site-about">

    <h2><?= Yii::t('app','Podaci o zaposlenju')?></h2>
    <div class="col s12 m12 l8">

        <div class="panel panel-success">
            <div class="panel-heading">
                <?php echo $zaposlenje->zaposlenik0->ime . ', '.$zaposlenje->zaposlenik0->prezime; ?>
                <hr />
            </div>
            <div class="panel-body">
            <?= DetailView::widget([
                'model' => $zaposlenje,
                'attributes' => [
                    [   'attribute' =>'dat_pocetka',
                        'value' => function ($data){
                            return Yii::$app->formatter->asDate($data->dat_pocetka, 'php:j.n.Y');
                        },
                    ],
                    [   'attribute' =>'date_kraj',
                        'value' => function ($data){
                            return Yii::$app->formatter->asDate($data->date_kraj, 'php:j.n.Y');
                        },
                    ],
                    'koeficijent',
                    'zaposlenik0.ime',
                    'zaposlenik0.prezime',
                    'vrsta0.naziv',
                    'rMjesto.naziv',
                    'created_at',
                    'updated_at',
                ],
            ]) ?>

</div>
</div>
