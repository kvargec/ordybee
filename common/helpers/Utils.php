<?php


namespace common\helpers;

use common\models\Notifikacije;
use common\models\PedagoskaGodina;
use common\models\Prisutnost;
use Yii;
use Mpdf;
use yii\web\Controller;
use yii\web\UploadedFile;

class Utils
{
    public static function list_dir_files($directory, $only_extensions = [])
    {
        $files = [];
        foreach (scandir($directory) as $file) {
            $path = "$directory/$file";
            if (is_file($path)) {
                if (empty($only_extensions) || in_array(pathinfo($path, PATHINFO_EXTENSION), $only_extensions)) {
                    $files[] = $file;
                }
            }
        }
        return $files;
    }

    public static function ispisPDF($content, $naziv, $tip, $orient = 'P', $dirPath = null)
    {
        $tempDir = Yii::getAlias('@frontend/web') . "/upload/pdftmp/";

        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);
        $mpdf->showImageErrors = true;
        $mpdf->setFooter('{PAGENO}');
        $mpdf->WriteHTML($content, \Mpdf\HTMLParserMode::DEFAULT_MODE);

        switch ($tip) {
            case 'download':
                $pdfPath = $naziv . '.pdf';
                $mpdf->Output($pdfPath, \Mpdf\Output\Destination::DOWNLOAD);
                break;
            case 'file':
                $filename = "$naziv.pdf";
                if ($dirPath == null) {
                    $pdfPath = Yii::getAlias('@frontend/web') . "/upload/" . $naziv . '.pdf';
                } else {
                    $pdfPath = "$dirPath/$filename";
                }
                $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
                return $pdfPath;
                break;
        }
    }
    public static function url_slug($str, $options = array())
    {


        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => true,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        $char_map = array(
            // Latin


            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
            'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
            'ß' => 'ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
            'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
            'ÿ' => 'y',
            // Latin symbols
            '©' => '(c)',
            // Greek
            'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
            'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
            'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
            'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
            'Ϋ' => 'Y',
            'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
            'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
            'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
            'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
            'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
            // Turkish
            'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
            'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
            // Russian
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
            'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
            'Я' => 'Ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
            'я' => 'ya',
            // Ukrainian
            'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
            'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
            // Czech
            'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
            'Ž' => 'Z',
            'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
            'ž' => 'z',
            // Polish
            'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
            'Ż' => 'Z',
            'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
            'ż' => 'z',
            'Đ' => 'D', 'đ' => 'd', 'Ć' => 'C', 'ć' => 'c',
            // Latvian
            'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
            'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
            'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
            'š' => 's', 'ū' => 'u', 'ž' => 'z'
        );


        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }

    public static function sendNotification($user_id, $akcija, $akcija_id, $radnja, $sadrzaj)
    {
        $notifikacija = new Notifikacije();
        $notifikacija->user = $user_id;
        $notifikacija->status = 1;
        $notifikacija->radnja = $radnja;
        $notifikacija->akcija_id = $akcija_id;
        $notifikacija->akcija = $akcija;
        $notifikacija->sadrzaj = $sadrzaj;
        $notifikacija->save();
    }
    public static function calculateDays($period, $entitet = 'dijete', $eID, $weekmode = 0)
    {
        /*
         * weekmode
         * 0 => skip sun and sat
         * 1 => skip sat
         * 2 => don't skip
         */

        $return = [];
        $pocetak = '';
        $zadnji = '';
        $ukupno = 0;
        $opravdano = 0;
        $neopravdano = 0;

        $period2 = Prisutnost::find()->where(['entitet' => $entitet, 'entitet_id' => $eID, 'status' => 0])->andWhere(['between', 'datum', $period->start->format('Y-m-d'), $period->end->format('Y-m-d')])->orderBy('datum ASC')->all();

        foreach ($period2 as $day) {
            $day2 = new \DateTime(date('Y-m-d', strtotime($day->datum)));
            if ($day2->format('N') >= 6) { //skip saturday and sunday
                continue;
            }
            //$izostanak = Prisutnost::find()->where(['entitet' => $entitet, 'entitet_id' => $eID, 'datum' => $day->format('Y-m-d'), 'status' => 0])->one();

            //            if (!empty($izostanak)){

            if (empty($pocetak)) {
                $pocetak = $day2->format('Y-m-d');
            }
            $ukupno++;
            if ($day->razlog_izostanka == 'osobni') {
                $neopravdano++;
            } elseif ($day->razlog_izostanka == 'zdravstveni') {
                $opravdano++;
            }
            if ($day2->format('N') == 5) {
                $iduci = date('Y-m-d', strtotime('+3 day', $day2->getTimestamp())); //ponedjeljak
            } else {
                $iduci = date('Y-m-d', strtotime('+1 day', $day2->getTimestamp())); //iduci dan
            }
            $iduci = Prisutnost::find()->where(['entitet' => $entitet, 'entitet_id' => $eID, 'datum' => $iduci, 'status' => 0])->orderBy('datum ASC')->one();

            if (empty($iduci)) { //iduci dan je dijete u vrticu
                $tmp = new \stdClass();
                $tmp->pocetak = $pocetak;
                $tmp->kraj =Yii::$app->formatter->asDate($day2);
                $tmp->ukupno = $ukupno;
                $tmp->ukupno_opravdano = $opravdano;
                $tmp->ukupno_neopravdano = $neopravdano;
                $return[] = $tmp;
                $pocetak = '';
                $ukupno = 0;
                $opravdano = 0;
                $neopravdano = 0;
            } else {
                continue;
            }
            /*          }else{
                continue;
            }*/
            $zadnji = $day2;
        }

        if (empty($return) and $ukupno > 0) {
            $tmp = new \stdClass();
            $tmp->pocetak = $pocetak;
            $tmp->kraj = Yii::$app->formatter->asDate($zadnji);
            $tmp->ukupno = $ukupno + 1;
            $tmp->ukupno_opravdano = $opravdano;
            $tmp->ukupno_neopravdano = $neopravdano;
            $return[] = $tmp;
        }
        return $return;
    }
    public static function calculateDaysIspricnica($period, $entitet = 'dijete', $eID, $weekmode = 0)
    {
        /*
         * weekmode
         * 0 => skip sun and sat
         * 1 => skip sat
         * 2 => don't skip
         */

        $return = [];
        $pocetak = '';
        $zadnji = '';
        $ukupno = 0;

        //$period2 = Prisutnost::find()->where(['entitet' => $entitet, 'entitet_id' => $eID, 'status' => 0])->andWhere(['between','datum',$period->start->format('Y-m-d'),$period->end->format('Y-m-d')])->all();

        foreach ($period as $day) {
            //$day=new \DateTime(date('Y-m-d', strtotime($day)));
            if ($day->format('N') >= 6) { //skip saturday and sunday
                continue;
            }
            //$izostanak = Prisutnost::find()->where(['entitet' => $entitet, 'entitet_id' => $eID, 'datum' => $day->format('Y-m-d'), 'status' => 0])->one();

            //            if (!empty($izostanak)){

            if (empty($pocetak)) {
                $pocetak = $day->format('Y-m-d');
            }

            $ukupno++;
            if ($day->format('N') == 5) {
                $iduci = date('Y-m-d', strtotime('+3 day', $day->getTimestamp())); //ponedjeljak
            } else {
                $iduci = date('Y-m-d', strtotime('+1 day', $day->getTimestamp())); //iduci dan
            }
            // $iduci = Prisutnost::find()->where(['entitet' => $entitet, 'entitet_id' => $eID, 'datum' => $iduci, 'status' => 0])->one();

            if (empty($iduci)) { //iduci dan je dijete u vrticu
                $tmp = new \stdClass();
                $tmp->pocetak = $pocetak;
                $tmp->kraj = $day->format('Y-m-d');
                $tmp->ukupno = $ukupno;
                $return[] = $tmp;
                $pocetak = '';
                $ukupno = 0;
            } else {
                continue;
            }
            /*          }else{
                          continue;
                      }*/
            $zadnji = $day;
        }

        if (empty($return) and $ukupno > 0) {
            $tmp = new \stdClass();
            $tmp->pocetak = $pocetak;
            $tmp->kraj = $zadnji;
            $tmp->ukupno = $ukupno + 1;
            $return[] = $tmp;
        }
        return $return;
    }
    public static function mjeseciPedGod()
    {
        $pedGodina = PedagoskaGodina::getAktivna();

        $mjeseci = [];
        $godPoc = date("Y", strtotime($pedGodina->od));
        $godKraj = date("Y", strtotime($pedGodina->do));
        for ($i = 0; $i < 12; $i++) {
            if (($i + 9) < 13) {
                $mjesecNaziv = $i+9;//Yii::$app->formatter->asDate(mktime(1, 0, 0, $i + 9), 'MMM');
                $mjeseci[$i] = $mjesecNaziv . ' / ' . $godPoc;
            } else {
                $mjesecNaziv = $i-3;//Yii::$app->formatter->asDate(mktime(1, 0, 0, $i - 3), 'MMM');
                $mjeseci[$i] = $mjesecNaziv . ' / ' . $godKraj;
            }
        }
        return $mjeseci;
    }
    public static function mjeseciPedGodAll()
    {
        $sveGodine = PedagoskaGodina::find()->all();
        $j=0;
        foreach ($sveGodine as $pedGodina){
            $mjeseci = [];
            $godPoc = date("Y", strtotime($pedGodina->od));
            $godKraj = date("Y", strtotime($pedGodina->do));
            for ($i = 0; $i < 12; $i++) {
                if (($i + 9) < 13) {
                    $mjesecNaziv = $i+9;//Yii::$app->formatter->asDate(mktime(1, 0, 0, $i + 9), 'MMM');
                    $mjeseci[$j] = $mjesecNaziv . ' / ' . $godPoc;
                } else {
                    $mjesecNaziv = $i-3;//Yii::$app->formatter->asDate(mktime(1, 0, 0, $i - 3), 'MMM');
                    $mjeseci[$j] = $mjesecNaziv . ' / ' . $godKraj;
                }
                $j++;
            }
        }

        return $mjeseci;
    }
    public static function getMjesecPed($mjesec)
    {
        $pedGodina = PedagoskaGodina::getAktivna();
        $godPoc = date("Y", strtotime($pedGodina->od));
        $godKraj = date("Y", strtotime($pedGodina->do));
        if (($mjesec + 9) < 13) {
            $m = 9 + $mjesec;
            $god = $godPoc;
        } else {
            $m = $mjesec - 3;
            $god = $godKraj;
        }
        $mjesecNaziv = Yii::$app->formatter->asDate(mktime(0, 0, 0, $m), 'MMM');
        return ['mjesec' => $m, 'naziv' => $mjesecNaziv, 'godina' => $god];
    }
}
