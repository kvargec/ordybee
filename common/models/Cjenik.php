<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cjenik".
 *
 * @property int $id
 * @property int|null $program
 * @property float|null $cijena
 * @property float|null $povlastena
 * @property int|null $ped_godina
 *
 * @property PedagoskaGodina $pedGodina
 * @property VrstaPrograma $program0
 */
class Cjenik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cjenik';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['program', 'ped_godina'], 'default', 'value' => null],
            [['program', 'ped_godina'], 'integer'],
            [['cijena', 'povlastena'], 'number'],
            [['ped_godina'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['ped_godina' => 'id']],
            [['program'], 'exist', 'skipOnError' => true, 'targetClass' => VrstaPrograma::className(), 'targetAttribute' => ['program' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'program' => Yii::t('app', 'Program'),
            'cijena' => Yii::t('app', 'Cijena'),
            'povlastena' => Yii::t('app', 'Povlastena'),
            'ped_godina' => Yii::t('app', 'Ped Godina'),
        ];
    }

    /**
     * Gets query for [[PedGodina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'ped_godina']);
    }

    /**
     * Gets query for [[Program0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProgram0()
    {
        return $this->hasOne(VrstaPrograma::className(), ['id' => 'program']);
    }
}
