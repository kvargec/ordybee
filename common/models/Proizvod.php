<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "proizvod".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $sifra
 * @property int|null $grupa
 * @property string|null $opis
 * @property int|null $mjed
 *
 * @property GrupaProizvod $grupa0
 * @property Mjed $mjed0
 * @property StavkeNarudjba[] $stavkeNarudjbas
 */
class Proizvod extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proizvod';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['grupa', 'mjed'], 'default', 'value' => null],
            [['grupa', 'mjed'], 'integer'],
            [['opis'], 'string'],
            [['naziv'], 'string', 'max' => 250],
            [['sifra'], 'string', 'max' => 150],
            [['grupa'], 'exist', 'skipOnError' => true, 'targetClass' => GrupaProizvod::className(), 'targetAttribute' => ['grupa' => 'id']],
            [['mjed'], 'exist', 'skipOnError' => true, 'targetClass' => Mjed::className(), 'targetAttribute' => ['mjed' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'sifra' => Yii::t('app', 'Sifra'),
            'grupa' => Yii::t('app', 'Grupa'),
            'opis' => Yii::t('app', 'Opis'),
            'mjed' => Yii::t('app', 'Mjed'),
        ];
    }

    /**
     * Gets query for [[Grupa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGrupa0()
    {
        return $this->hasOne(GrupaProizvod::className(), ['id' => 'grupa']);
    }

    /**
     * Gets query for [[Mjed0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMjed0()
    {
        return $this->hasOne(Mjed::className(), ['id' => 'mjed']);
    }

    /**
     * Gets query for [[StavkeNarudjbas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStavkeNarudjbas()
    {
        return $this->hasMany(StavkeNarudjba::className(), ['proizvod' => 'id']);
    }
}
