<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "document".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $filename
 * @property int vrsta_dokumenta
 *
 * @property DocumentVrsta $vrstaDokumenta
 * @property DocumentZahtjev[] $documentZahtjevs
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'string'],
            [['filename'], 'string', 'max' => 250],
        ];
    }

    const OBAVEZNI_DOKUMENTI = [
        'Preslika rodnog lista djeteta ili izvatka iz matice rođenih',
        'Preslike osobne iskaznice',
        'Potvrda o prebivalištu djeteta (ne starija od šest mjeseci)',
        'Potvrda Hrvatskog zavoda za mirovinsko osiguranje o radno-pravnom statusu (ne starija od 30 dana)',
        'Preslika Rješenja da je roditelj djeteta žrtva ili invalid Domovinskog rata',
        'Preslike izvatka iz matične knjige rođenih ili rodnog lista ostale malodobne djece',
        'Preslika medicinske dokumentacije djeteta koja potvrđuje posebne potrebe u rastu i razvoju djeteta',
        'Preslika Rješenja o skrbništvu za roditelje koji žive sami',
        'Preslika Rješenja o skrbništvu za uzdržavanu djecu',
        'Preslika Rješenja da je roditelj djeteta korisnik doplataka za dijete',
    ];

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'filename' => Yii::t('app', 'Naziv datoteke'),
            'vrsta_dokumenta' => Yii::t('app', 'Vrsta dokumenta'),
        ];
    }

    /**
     * Gets query for [[DocumentZahtjevs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentZahtjevs()
    {
        return $this->hasMany(DocumentZahtjev::className(), ['document' => 'id']);
    }
    public function getVrstaDokumenta()
    {
        return $this->hasOne(DocumentVrsta::className(), ['id' => 'vrsta_dokumenta']);
    }
}
