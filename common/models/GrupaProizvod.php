<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "grupa_proizvod".
 *
 * @property int $id
 * @property string|null $grupa
 * @property int|null $parent
 *
 * @property Proizvod[] $proizvods
 */
class GrupaProizvod extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grupa_proizvod';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent'], 'default', 'value' => null],
            [['parent'], 'integer'],
            [['grupa'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'grupa' => Yii::t('app', 'Grupa'),
            'parent' => Yii::t('app', 'Parent'),
        ];
    }

    /**
     * Gets query for [[Proizvods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProizvods()
    {
        return $this->hasMany(Proizvod::className(), ['grupa' => 'id']);
    }
}
