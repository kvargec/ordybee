<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vrste_poruke".
 *
 * @property int $id
 * @property string|null $vrsta
 *
 * @property Poruke[] $porukes
 */
class VrstePoruke extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrste_poruke';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vrsta'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vrsta' => Yii::t('app', 'Vrsta'),
        ];
    }

    /**
     * Gets query for [[Porukes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPorukes()
    {
        return $this->hasMany(Poruke::className(), ['vrsta_poruke' => 'id']);
    }
}
