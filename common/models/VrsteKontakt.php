<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vrste_kontakt".
 *
 * @property int $id
 * @property string|null $vrsta
 * @property int|null $redoslijed
 *
 * @property Kontakt[] $kontakts
 */
class VrsteKontakt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrste_kontakt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['redoslijed'], 'default', 'value' => null],
            [['redoslijed'], 'integer'],
            [['vrsta'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vrsta' => Yii::t('app', 'Vrsta'),
            'redoslijed' => Yii::t('app', 'Redoslijed'),
        ];
    }

    /**
     * Gets query for [[Kontakts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKontakts()
    {
        return $this->hasMany(Kontakt::className(), ['vrsta' => 'id']);
    }
}
