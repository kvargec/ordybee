<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prijevoz".
 *
 * @property int $id
 * @property string $vrsta
 * @property float|null $cijena
 *
 * @property PrijevozZaposlenik[] $prijevozZaposleniks
 */
class Prijevoz extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prijevoz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vrsta'], 'required'],
            [['cijena'], 'number'],
            [['vrsta'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vrsta' => Yii::t('app', 'Vrsta'),
            'cijena' => Yii::t('app', 'Cijena'),
        ];
    }

    /**
     * Gets query for [[PrijevozZaposleniks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrijevozZaposleniks()
    {
        return $this->hasMany(PrijevozZaposlenik::className(), ['prijevoz_id' => 'id']);
    }
}
