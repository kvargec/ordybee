<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "postavke".
 *
 * @property int $id
 * @property string|null $postavka
 * @property string|null $vrijednost
 * @property string|null $dodatno
 */
class Postavke extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'postavke';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dodatno'], 'safe'],
            [['postavka'], 'string', 'max' => 100],
            [['vrijednost'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'postavka' => 'Postavka',
            'vrijednost' => 'Vrijednost',
            'dodatno' => 'Dodatno',
        ];
    }
}
