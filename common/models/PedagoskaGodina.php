<?php

namespace common\models;
use yii\behaviors\AttributeBehavior;
use Yii;

/**
 * This is the model class for table "pedagoska_godina".
 *
 * @property int $id
 * @property string $od
 * @property string $do
 * @property bool|null $aktivna
 * @property array|null $postavke
 *
 * @property Kriterij[] $kriterijs
 */
class PedagoskaGodina extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedagoska_godina';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['od'],
                    self::EVENT_BEFORE_UPDATE => ['od'],
                ],
                'value' => function ($event) {
                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->dateFormat, 4);
                    $date = date_create_from_format($format, $this->od);
                    if(!$date) {
                        // fallback, if method 'save' was called twice on same model
                        return $this->od;
                    }

                    return date_format($date, 'Y-m-d');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['do'],
                    self::EVENT_BEFORE_UPDATE => ['do'],
                ],
                'value' => function ($event) {
                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->dateFormat, 4);
                    $date = date_create_from_format($format, $this->do);
                    if(!$date) {
                        // fallback, if method 'save' was called twice on same model
                        return $this->do;
                    }

                    return date_format($date, 'Y-m-d');
                },
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['od', 'do'], 'required'],
            [['od', 'do', 'postavke'], 'safe'],
            [['aktivna'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'od' => Yii::t('app', 'Od'),
            'do' => Yii::t('app', 'Do'),
            'aktivna' => Yii::t('app', 'Aktivna'),
            'postavke' => Yii::t('app', 'Postavke'),
        ];
    }
    public function afterFind() {
        parent::afterFind ();
        $this->od=Yii::$app->formatter->asDate($this->od);
        $this->do=Yii::$app->formatter->asDate($this->do);
    }
    /**
     * Gets query for [[Kriterijs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKriterijs()
    {
        return $this->hasMany(Kriterij::className(), ['pedagoska_godina_id' => 'id']);
    }

    public static function getAktivna()
    {
        return self::findOne(['aktivna'=>true]);
    }
    public static function setAktivna()
    {
        $id = self::find()->max('id');
        $aktivna_godina = self::find()->where(['id'=>$id])->one();
        $aktivna_godina->aktivna = true;
        $aktivna_godina->save();
    }
    public static function getProsla()
    {
        $sadasnja = self::find()->max('id');
        $prosla = self::find()->where(['<', 'id', $sadasnja])->orderBy('id desc')->one();

        return $prosla;
    }
    public function getNaziv(){
        $temp=date("Y",strtotime($this->od));
        $temp.="./";
        $temp.=date("Y",strtotime($this->do));
        $temp.=".";
        return $temp;
    }
}
