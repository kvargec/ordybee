<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rodbina".
 *
 * @property int $id
 * @property int|null $dijete
 * @property int|null $sibling
 */
class Rodbina extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rodbina';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dijete', 'sibling'], 'default', 'value' => null],
            [['dijete', 'sibling'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dijete' => Yii::t('app', 'Dijete'),
            'sibling' => Yii::t('app', 'Sibling'),
        ];
    }
}
