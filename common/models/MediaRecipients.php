<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "media_recipients".
 *
 * @property int $id
 * @property int $media
 * @property int $user
 * @property string|null $created_at
 *
 * @property Media $media0
 * @property User $user0
 */
class MediaRecipients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media_recipients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['media', 'user'], 'required'],
            [['media', 'user'], 'default', 'value' => null],
            [['media', 'user'], 'integer'],
            [['created_at'], 'safe'],
            [['media'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['media' => 'id']],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'media' => Yii::t('app', 'Media'),
            'user' => Yii::t('app', 'User'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * Gets query for [[Media0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMedia0()
    {
        return $this->hasOne(Media::className(), ['id' => 'media']);
    }

    /**
     * Gets query for [[User0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }
}
