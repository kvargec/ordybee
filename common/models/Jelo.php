<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jelo".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $podaci
 *
 * @property Jelovnik[] $jelovniks
 */
class Jelo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jelo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['podaci'], 'safe'],
            [['naziv'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'naziv' => 'Naziv',
            'podaci' => 'Podaci',
        ];
    }

    /**
     * Gets query for [[Jelovniks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJelovniks()
    {
        return $this->hasMany(Jelovnik::className(), ['jelo' => 'id']);
    }
}
