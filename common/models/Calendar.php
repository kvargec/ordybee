<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calendar".
 *
 * @property int $id
 * @property string|null $dan
 * @property int|null $vrsta
 * @property string|null $napomena
 *
 * @property DanType $vrsta0
 */
class Calendar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dan'], 'safe'],
            [['vrsta'], 'default', 'value' => null],
            [['vrsta'], 'integer'],
            [['napomena'], 'string'],
            [['vrsta'], 'exist', 'skipOnError' => true, 'targetClass' => DanType::className(), 'targetAttribute' => ['vrsta' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dan' => 'Dan',
            'vrsta' => 'Vrsta',
            'napomena' => 'Napomena',
        ];
    }

    /**
     * Gets query for [[Vrsta0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVrsta0()
    {
        return $this->hasOne(DanType::className(), ['id' => 'vrsta']);
    }
}
