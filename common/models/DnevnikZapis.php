<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\UploadedFile;
use \yii\helpers\Url;

/**
 * This is the model class for table "dnevnik_zapis".
 *
 * @property int $id
 * @property int|null $dnevnik
 * @property string|null $zapis
 * @property int|null $vrsta
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $vrijednosti_atributa 
 *
 * @property Dnevnik $dnevnik0
 * @property StatusOpce $status0
 * @property VrstaZapis $vrsta0
 * @property DijeteZapis $dijeteZapis
 */
class DnevnikZapis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dnevnik_zapis';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dnevnik', 'vrsta', 'status'], 'default', 'value' => null],
            [['dnevnik', 'vrsta', 'status'], 'integer'],
            [['zapis'], 'string'],
            [['created_at', 'updated_at', 'vrijednosti_atributa'], 'safe'],
            [['dnevnik'], 'exist', 'skipOnError' => true, 'targetClass' => Dnevnik::className(), 'targetAttribute' => ['dnevnik' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusOpce::className(), 'targetAttribute' => ['status' => 'id']],
            [['vrsta'], 'exist', 'skipOnError' => true, 'targetClass' => VrstaZapis::className(), 'targetAttribute' => ['vrsta' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dnevnik' => Yii::t('app', 'Dnevnik'),
            'zapis' => Yii::t('app', 'Zapis'),
            'vrsta' => Yii::t('app', 'Vrsta'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'vrijednosti_atributa' => Yii::t('app', 'Vrijednosti Atributa'),
        ];
    }

    /**
     * Gets query for [[Dnevnik0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnevnik0()
    {
        return $this->hasOne(Dnevnik::className(), ['id' => 'dnevnik']);
    }

    /**
     * Gets query for [[Status0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusOpce::className(), ['id' => 'status']);
    }

    /**
     * Gets query for [[Vrsta0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVrsta0()
    {
        return $this->hasOne(VrstaZapis::className(), ['id' => 'vrsta']);
    }

    public function getDijeteZapis()
    {
        return $this->hasOne(DijeteZapis::className(), ['zapis' => 'id']);
    }

    public function upload($id)
    {
        //print("<pre>".print_r($this,true)."</pre>");die();  
        if (empty(UploadedFile::getInstances($this, 'attachments'))) {
            return true;
        }
        $this->attachments = UploadedFile::getInstances($this, 'attachments');

        if (!file_exists(Yii::getAlias('@backend') . '/web/dnevnik/' . $id)) {
            mkdir(Yii::getAlias('@backend') . '/web/dnevnik/' . $id, 0777, true);
        }
        $dir = Yii::getAlias('@backend') . '/web/dnevnik/' . $id;
        $realAttachments = [];

        if (!empty(UploadedFile::getInstances($this, 'attachments'))) {
            foreach ($this->attachments as $file) {
                $fileName = preg_replace("/\s+/", "_", $file->baseName);
                $realAttachments[] = $fileName . '.' . $file->extension;
                $file->saveAs($dir . '/' . $fileName . '.' . $file->extension);
            }

            $this->attachments = ['attachments' => $realAttachments];
        }

        return true;
    }

    public function getFilePaths($id)
    {
        $preview = [];
        //$dir = Yii::getAlias('@backend') . '/web/dnevnik/' . $id;
        $dir = Url::to('@web/dnevnik/') . $id;

        if (isset($this->attachments['attachments'])) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $url = $dir . '/' . explode($id . '/', $attachment)[0];
                $preview[] = $url;
            }
        }

        return $preview;
    }

    public function getFilePathsConfig()
    {
        $config = [];
        $count = 0;

        if (isset($this->attachments['attachments'])) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $fileName = $attachment;
                $fileId = $fileName;
                $type = pathinfo($fileName);
                $config[] = [
                    'key' => $fileId,
                    'caption' => $fileName,
                    'url' => Url::to(['/dnevnik-zapis/delete-file']),
                ];
                if ($type == 'pdf') {
                    $config[] = ['type' => 'pdf'];
                }
                $count++;
            }
        }

        return $config;
    }

    public function getFilePathsBackend($id)
    {
        $return = [];
        if (isset($this->attachments['attachments'])) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $url = Url::to('@web/dnevnik/') . $id . '/' . explode($id . '/', $attachment)[0];
                $return[] = $url;
            }
        }

        return $return;
    }
    
    public function getDijeteFilePaths($id)
    {
        $preview = [];
        $dijete = DijeteZapis::find()->where(['id' => $id])->one();
        $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->dijete;

        if (isset($this->attachments['attachments'])) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $url = $dir . '/' . explode($dijete->dijete . '/', $attachment)[0];
                $preview[] = $url;
            }
        }

        return $preview;
    }
    public function getDijeteFilePathsConfig()
    {
        $config = [];
        $count = 0;

        if (isset($this->attachments['attachments'])) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $fileName = $attachment;
                $fileId = $fileName;
                $type = pathinfo($fileName);
                $config[] = [
                    'key' => $fileId,
                    'caption' => $fileName,
                    'url' => Url::to(['/dijete-zapis/delete-file']),
                ];
                if ($type == 'pdf') {
                    $config[] = ['type' => 'pdf'];
                }
                $count++;
            }
        }

        return $config;
    }
}
