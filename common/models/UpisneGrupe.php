<?php

namespace common\models;

use yii\behaviors\AttributeBehavior;
use Yii;

/**
 * This is the model class for table "upisne_grupe".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $date_poc
 * @property string|null $date_kraj
 * @property int|null $broj
 * @property int|null $ped_godina
 *
 * @property PedagoskaGodina $pedGodina
 */
class UpisneGrupe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'upisne_grupe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['broj', 'ped_godina'], 'default', 'value' => null],
            [['broj', 'ped_godina'], 'integer'],
            [['naziv'], 'string', 'max' => 150],
            [['ped_godina'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['ped_godina' => 'id']],
        ];
    }
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['date_poc'],
                    self::EVENT_BEFORE_UPDATE => ['date_poc'],
                ],
                'value' => function ($event) {
                    if (!$this->date_poc) {
                        return $this->date_poc;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->dateFormat, 4);
                    return date_format(date_create_from_format($format, $this->date_poc), 'Y-m-d');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['date_kraj'],
                    self::EVENT_BEFORE_UPDATE => ['date_kraj'],
                ],
                'value' => function ($event) {
                    if (!$this->date_kraj) {
                        return $this->date_kraj;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->dateFormat, 4);
                    return date_format(date_create_from_format($format, $this->date_kraj), 'Y-m-d');
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'date_poc' => Yii::t('app', 'Date Poc'),
            'date_kraj' => Yii::t('app', 'Date Kraj'),
            'broj' => Yii::t('app', 'Broj'),
            'ped_godina' => Yii::t('app', 'Ped Godina'),
        ];
    }
    public function afterFind()
    {
        parent::afterFind();
        $this->date_poc = Yii::$app->formatter->asDate($this->date_poc);
        $this->date_kraj = Yii::$app->formatter->asDate($this->date_kraj);
    }
    /**
     * Gets query for [[PedGodina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'ped_godina']);
    }
}
