<?php

namespace common\models;

use frontend\components\validators\OibValidator;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use \yii\helpers\Url;

/**
 * This is the model class for table "zaposlenik".
 *
 * @property int $id
 * @property string|null $ime
 * @property string|null $prezime
 * @property string|null $adresa
 * @property int|null $mjesto
 * @property string|null $oib
 * @property string|null $dat_rodjenja
 * @property int|null $sprema
 * @property string|null $spol
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $status
 * @property int|null $user
 *
 * @property Dnevnik[] $dnevniks
 * @property Mjesto $mjesto0
 * @property Sprema $sprema0
 * @property ZaposlenikKontakt[] $zaposlenikKontakts
 * @property Zaposlenje[] $zaposlenjes
 * @property Prisutnost $prisutnost
 * @property ZaposlenikKontakt $kontakts
 *
 */
class Zaposlenik extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function ($event) {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_VALIDATE => ['dat_rodjenja'],
                    self::EVENT_BEFORE_INSERT => ['dat_rodjenja'],
                    self::EVENT_BEFORE_UPDATE => ['dat_rodjenja'],
                ],
                'value' => function ($event) {

                    if (!$this->dat_rodjenja) {

                        return $this->dat_rodjenja;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    //var_dump(date_create_from_format(Yii::$app->formatter->datetimeFormat, $this->vrijeme));
                    //die();
                    $kaj = new \DateTime($this->dat_rodjenja);

                    return date_format($kaj, 'Y-m-d H:i');
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zaposlenik';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mjesto', 'sprema'], 'default', 'value' => null],
            [['mjesto', 'sprema', 'status', 'user'], 'integer'],
            [['dat_rodjenja', 'created_at', 'updated_at', 'attachments'], 'safe'],
            [['attachments'], 'file', 'maxFiles' => 10],
            [['ime', 'prezime'], 'string', 'max' => 150],
            [['adresa'], 'string', 'max' => 200],
            [['oib'], 'string', 'max' => 11],
            ['oib', OibValidator::className()],
            [['spol'], 'string', 'max' => 2],
            [['mjesto'], 'exist', 'skipOnError' => true, 'targetClass' => Mjesto::className(), 'targetAttribute' => ['mjesto' => 'id']],
            [['sprema'], 'exist', 'skipOnError' => true, 'targetClass' => Sprema::className(), 'targetAttribute' => ['sprema' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ime' => Yii::t('app', 'Ime'),
            'prezime' => Yii::t('app', 'Prezime'),
            'adresa' => Yii::t('app', 'Adresa'),
            'mjesto' => Yii::t('app', 'Mjesto'),
            'oib' => Yii::t('app', 'Oib'),
            'dat_rodjenja' => Yii::t('app', 'Dat Rodjenja'),
            'sprema' => Yii::t('app', 'Sprema'),
            'spol' => Yii::t('app', 'Spol'),
            'attachments' => Yii::t('app', 'Attachments'),
        ];
    }

    public function createZaposlenikKontakt()
    {
        $zaposlenikKontakt = new ZaposlenikKontakt();
        $zaposlenikKontakt->zaposlenik = $this->id;
        return $zaposlenikKontakt;
    }

    /**
     * Gets query for [[Dnevniks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnevniks()
    {
        return $this->hasMany(Dnevnik::className(), ['zaposlenik' => 'id']);
    }

    /**
     * Gets query for [[Mjesto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMjesto0()
    {
        return $this->hasOne(Mjesto::className(), ['id' => 'mjesto']);
    }

    /**
     * Gets query for [[Sprema0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSprema0()
    {
        return $this->hasOne(Sprema::className(), ['id' => 'sprema']);
    }

    /**
     * Gets query for [[ZaposlenikKontakts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenikKontakts()
    {
        return $this->hasMany(ZaposlenikKontakt::className(), ['zaposlenik' => 'id']);
    }

    /**
     * Gets query for [[Zaposlenjes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenjes()
    {
        return $this->hasMany(Zaposlenje::className(), ['zaposlenik' => 'id']);
    }
    public function getRadnoMjesto0()
    {
        return $this->hasOne(RadnoMjesto::className(), ['id' => 'r_mjesto'])->via('zaposlenjes');
    }

    public function getFullName()
    {
        return "$this->ime $this->prezime";
    }
    public function getPrisutnost()
    {
        return $this->hasMany(Prisutnost::className(), ['entitet_id' => 'id'])->onCondition(['entitet' => 'zaposlenik']);
    }
    public function getPrijevozi()
    {
        return $this->hasMany(PrijevozZaposlenik::className(), ['zaposlenik_id' => 'id']);
    }
    public function getObroci()
    {
        return $this->hasMany(ZaposlenikObrok::className(), ['zaposlenik_id' => 'id']);
    }
    public function getKontakts()
    {
        return $this->hasMany(ZaposlenikKontakt::className(), ['zaposlenik' => 'id']);
    }
    public function getKontaktDetails()
    {
        return $this->hasMany(Kontakt::className(), ['id' => 'kontakt'])->via('kontakts');
    }

    public function upload($id)
    {
        if (!file_exists(Yii::getAlias('@backend') . '/web/zaposlenici/')) {
            mkdir(Yii::getAlias('@backend') . '/web/zaposlenici/', 0777, true);
        }
        $dir = Yii::getAlias('@backend') . '/web/zaposlenici/' . $id;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $zap = $this::find()->where(['id' => $id])->one();
        $oldFiles = null;
        if (!is_null($zap)) {
            $oldFiles = $zap->attachments;
        }
        $files = UploadedFile::getInstances($this, 'attachments');
        $realAttachments = [];

        if (is_array($oldFiles) && array_key_exists('attachments', $oldFiles)) {
            foreach ($oldFiles['attachments'] as $attach) {
                $realAttachments[] = $attach;
            }
        }
        foreach ($files as $file) {
            $fileName = preg_replace("/\s+/", "_", $file->baseName);
            if (!in_array($fileName . '.' . $file->extension, $realAttachments)) {
                $realAttachments[] = $fileName . '.' . $file->extension;
            }
            $file->saveAs($dir . '/' . $fileName . '.' . $file->extension);
        }
        $this->attachments = ['attachments' => $realAttachments];
        return true;
    }

    public function getFilePaths($id)
    {
        $return = array();
        if (is_array($this->attachments) && array_key_exists('attachments', $this->attachments)) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $url = $url = Yii::getAlias('@frontendUrl') . 'admin/zaposlenici/' . $id . '/' . explode($id . '/', $attachment)[0];
                //Yii::getAlias('@backend').'/web/zaposlenici/'.$id.'/'.explode('zaposlenici/', $attachment)[0];
                $return[] = $url;
            }
        }
        return $return;
    }

    public function getFilePathsBackend($id)
    {
        $return = array();
        if (is_array($this->attachments) && array_key_exists('attachments', $this->attachments)) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $url = Url::to('@web/zaposlenici/') . $id . '/' . explode($id . '/', $attachment)[0];
                $return[] = $url;
            }
        }
        return $return;
    }
}
