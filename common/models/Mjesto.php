<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mjesto".
 *
 * @property int $id
 * @property string $naziv
 * @property string|null $zip
 * @property int|null $zupanija
 * @property int $sifra
 *
 * @property Zupanija $zupanija0
 * @property Objekt[] $objekts
 * @property Zaposlenik[] $zaposleniks
 */
class Mjesto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mjesto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'required'],
            [['zupanija'], 'default', 'value' => null],
            [['zupanija','sifra'], 'integer'],
            [['naziv'], 'string', 'max' => 150],
            [['zip'], 'string', 'max' => 10],
            [['zupanija'], 'exist', 'skipOnError' => true, 'targetClass' => Zupanija::className(), 'targetAttribute' => ['zupanija' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Mjesto'),
            'zip' => Yii::t('app', 'Zip'),
            'zupanija' => Yii::t('app', 'Županija'),
            'sifra' => Yii::t('app','Šifra'),
        ];
    }

    /**
     * Gets query for [[Zupanija0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZupanija0()
    {
        return $this->hasOne(Zupanija::className(), ['id' => 'zupanija']);
    }

    /**
     * Gets query for [[Objekts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjekts()
    {
        return $this->hasMany(Objekt::className(), ['mjesto' => 'id']);
    }

    /**
     * Gets query for [[Zaposleniks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposleniks()
    {
        return $this->hasMany(Zaposlenik::className(), ['mjesto' => 'id']);
    }
}
