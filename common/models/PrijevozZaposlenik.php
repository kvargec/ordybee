<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prijevoz_zaposlenik".
 *
 * @property int $id
 * @property int $prijevoz_id
 * @property int $zaposlenik_id
 *
 * @property Prijevoz $prijevoz
 * @property Zaposlenik $zaposlenik
 */
class PrijevozZaposlenik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prijevoz_zaposlenik';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prijevoz_id', 'zaposlenik_id'], 'required'],
            [['prijevoz_id', 'zaposlenik_id'], 'default', 'value' => null],
            [['prijevoz_id', 'zaposlenik_id'], 'integer'],
            [['prijevoz_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prijevoz::className(), 'targetAttribute' => ['prijevoz_id' => 'id']],
            [['zaposlenik_id'], 'exist', 'skipOnError' => true, 'targetClass' => Zaposlenik::className(), 'targetAttribute' => ['zaposlenik_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'prijevoz_id' => Yii::t('app', 'Prijevoz ID'),
            'zaposlenik_id' => Yii::t('app', 'Zaposlenik ID'),
        ];
    }

    /**
     * Gets query for [[Prijevoz]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrijevoz()
    {
        return $this->hasOne(Prijevoz::className(), ['id' => 'prijevoz_id']);
    }

    /**
     * Gets query for [[Zaposlenik]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenik()
    {
        return $this->hasOne(Zaposlenik::className(), ['id' => 'zaposlenik_id']);
    }
}
