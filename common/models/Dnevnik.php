<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "dnevnik".
 *
 * @property int $id
 * @property string|null $sifra
 * @property int|null $zaposlenik
 * @property int|null $ped_godina
 * @property int|null $vrsta_dnevnika
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property PedagoskaGodina $pedGodina
 * @property VrstaDnevnik $vrstaDnevnika
 * @property Zaposlenik $zaposlenik0
 * @property DnevnikZapis[] $dnevnikZapis
 */
class Dnevnik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dnevnik';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zaposlenik', 'ped_godina', 'vrsta_dnevnika'], 'default', 'value' => null],
            [['zaposlenik', 'ped_godina', 'vrsta_dnevnika'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['sifra'], 'string', 'max' => 50],
            [['ped_godina'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['ped_godina' => 'id']],
            [['vrsta_dnevnika'], 'exist', 'skipOnError' => true, 'targetClass' => VrstaDnevnik::className(), 'targetAttribute' => ['vrsta_dnevnika' => 'id']],
            [['zaposlenik'], 'exist', 'skipOnError' => true, 'targetClass' => Zaposlenik::className(), 'targetAttribute' => ['zaposlenik' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sifra' => Yii::t('app', 'Sifra'),
            'zaposlenik' => Yii::t('app', 'Zaposlenik'),
            'ped_godina' => Yii::t('app', 'Ped Godina'),
            'vrsta_dnevnika' => Yii::t('app', 'Vrsta Dnevnika'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // prilikom spremanja zapisa atribut pedagoska godina se postavlja na trenutno aktivnu godinu
        //$this->ped_godina = PedagoskaGodina::getAktivna()->id;

        return true;
    }

    public function createDnevnikZapis()
    {
        $dnevnikZapis = new DnevnikZapis();
        $dnevnikZapis->dnevnik = $this->id;
        return $dnevnikZapis;
    }

    /**
     * Gets query for [[PedGodina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'ped_godina']);
    }

    /**
     * Gets query for [[VrstaDnevnika]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVrstaDnevnika()
    {
        return $this->hasOne(VrstaDnevnik::className(), ['id' => 'vrsta_dnevnika']);
    }

    /**
     * Gets query for [[Zaposlenik0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenik0()
    {
        return $this->hasOne(Zaposlenik::className(), ['id' => 'zaposlenik']);
    }

    /**
     * Gets query for [[DnevnikZapis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnevnikZapis()
    {
        return $this->hasMany(DnevnikZapis::className(), ['dnevnik' => 'id']);
    }
}
