<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jelovnik".
 *
 * @property int $id
 * @property string|null $datum
 * @property int|null $obrok
 * @property int|null $jelo
 * @property int|null $redoslijed
 *
 * @property Jelo $jelo0
 * @property Obrok $obrok0
 */
class Jelovnik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jelovnik';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['datum'], 'safe'],
            [['obrok', 'jelo', 'redoslijed'], 'default', 'value' => null],
            [['obrok', 'jelo', 'redoslijed'], 'integer'],
            [['jelo'], 'exist', 'skipOnError' => true, 'targetClass' => Jelo::className(), 'targetAttribute' => ['jelo' => 'id']],
            [['obrok'], 'exist', 'skipOnError' => true, 'targetClass' => Obrok::className(), 'targetAttribute' => ['obrok' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datum' => 'Datum',
            'obrok' => 'Obrok',
            'jelo' => 'Jelo',
            'redoslijed' => 'Redoslijed',
        ];
    }

    /**
     * Gets query for [[Jelo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJelo0()
    {
        return $this->hasOne(Jelo::className(), ['id' => 'jelo']);
    }

    /**
     * Gets query for [[Obrok0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObrok0()
    {
        return $this->hasOne(Obrok::className(), ['id' => 'obrok']);
    }
}
