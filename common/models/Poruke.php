<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use \yii\helpers\Url;

/**
 * This is the model class for table "poruke".
 *
 * @property int $id
 * @property string|null $subject
 * @property string|null $message
 * @property int|null $sender
 * @property array|null $recipients
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $send_at
 * @property int|null $status
 * @property array|null $attachments
 * @property int|null $priority
 * @property int|null $vrsta_poruke
 *
 * @property Mailbox[] $mailboxes
 * @property StatusOpce $status0
 * @property VrstePoruke $vrstaPoruke
 */
class Poruke extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $grupacija;
    public static function tableName()
    {
        return 'poruke';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['sender', 'status', 'priority', 'vrsta_poruke'], 'default', 'value' => null],
            [['sender', 'status', 'priority', 'vrsta_poruke'], 'integer'],
            [['recipients', 'created_at', 'updated_at', 'send_at', 'attachments'], 'safe'],
            [['subject'], 'string', 'max' => 255],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusOpce::className(), 'targetAttribute' => ['status' => 'id']],
            [['vrsta_poruke'], 'exist', 'skipOnError' => true, 'targetClass' => VrstePoruke::className(), 'targetAttribute' => ['vrsta_poruke' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject' => Yii::t('app', 'Subject'),
            'message' => Yii::t('app', 'Message'),
            'sender' => Yii::t('app', 'Sender'),
            'recipients' => Yii::t('app', 'Recipients'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'send_at' => Yii::t('app', 'Send At'),
            'status' => Yii::t('app', 'Status'),
            'attachments' => Yii::t('app', 'Attachments'),
            'priority' => Yii::t('app', 'Priority'),
            'vrsta_poruke' => Yii::t('app', 'Vrsta Poruke'),
        ];
    }

    /**
     * Gets query for [[Mailboxes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMailboxes()
    {
        return $this->hasMany(Mailbox::className(), ['message' => 'id']);
    }

    /**
     * Gets query for [[Status0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusOpce::className(), ['id' => 'status']);
    }

    /**
     * Gets query for [[VrstaPoruke]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVrstaPoruke()
    {
        return $this->hasOne(VrstePoruke::className(), ['id' => 'vrsta_poruke']);
    }
    //public function upload($id)
    //{
    //    $this->attachments = UploadedFile::getInstances($this, 'attachments');
    //
    //    if (!file_exists(Yii::getAlias('@backend') . '/web/poruke')) {
    //        mkdir(Yii::getAlias('@backend') . '/web/poruke', 0777, true);
    //    }
    //    $dir = Yii::getAlias('@backend') . '/web/poruke/' . $id;
    //    if (!file_exists($dir)) {
    //        mkdir($dir, 0777, true);
    //    }
    //    $realAttachments = [];
    //
    //    foreach ($this->attachments as $file) {
    //        $file_name = preg_replace("/\s+/", "_", $file->baseName);
    //        $file_name = str_replace(["[", "]"], "", $file_name);
    //        $realAttachments[] = $file_name . '.' . $file->extension;
    //        //var_dump($this->getAttachDir() . $file->baseName . '.' . $file->extension);
    //        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
    //    }
    //    $this->attachments = ['attachments' => $realAttachments];
    //    return true;
    //}
    //public function getFilePaths($id)
    //{
    //    $preview = [];
    //    $dir = Yii::getAlias('@backend') . '/web/poruke/' . $id;
    //    if (isset($this->attachments['attachments'])) {
    //        foreach ($this->attachments['attachments'] as $attachment) {
    //            $url = $dir . '/' . $attachment;
    //            $preview[] = $url;
    //        }
    //    }
    //    return $preview;
    //}
    public function getFilePaths($id)
    {
        $preview = [];
        $dir = Url::to('@web/poruke/') . $id;

        if (isset($this->attachments['attachments'])) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $url = $dir . '/' . explode($id . '/', $attachment)[0];
                $preview[] = $url;
            }
        }

        return $preview;
    }

    public function getFilePathsConfig()
    {
        $config = [];
        $count = 0;

        if (isset($this->attachments['attachments'])) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $fileName = $attachment;
                $fileId = $fileName;
                $type = pathinfo($fileName);
                $config[] = [
                    'key' => $fileId,
                    'caption' => $fileName,
                    'url' => Url::to(['/poruke/delete-file']),
                ];
                if ($type == 'pdf') {
                    $config[] = ['type' => 'pdf'];
                }
                $count++;
            }
        }

        return $config;
    }
    public function getCcGrupacija()
    {
        if (!empty($this->cc_recipients)) {
            return key($this->cc_recipients);
        } else {
            return null;
        }
    }
    public function setCcGrupacija($value)
    {
        $this->_ccgrupacija = $value;
    }
}
