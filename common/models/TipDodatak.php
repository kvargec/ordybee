<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tip_dodatak".
 *
 * @property int $id
 * @property string|null $naziv
 *
 * @property DodatakSadrzaj[] $dodatakSadrzajs
 */
class TipDodatak extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tip_dodatak';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'naziv' => 'Naziv',
        ];
    }

    /**
     * Gets query for [[DodatakSadrzajs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDodatakSadrzajs()
    {
        return $this->hasMany(DodatakSadrzaj::className(), ['tip' => 'id']);
    }
}
