<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ocijenjen_zahtjev".
 *
 * @property int|null $id
 * @property string|null $urbroj
 * @property string|null $create_at
 * @property string|null $update_at
 * @property int|null $user
 * @property int|null $dijete
 * @property int|null $status
 * @property string|null $klasa
 * @property bool|null $ocijenjen
 * @property int|null $bodovi
 * @property bool|null $izravan_upis
 */
class OcijenjenZahtjev extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ocijenjen_zahtjev';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user', 'dijete', 'status', 'bodovi'], 'default', 'value' => null],
            [['id', 'user', 'dijete', 'status', 'bodovi'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['ocijenjen', 'izravan_upis'], 'boolean'],
            [['urbroj'], 'string', 'max' => 50],
            [['klasa'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'urbroj' => Yii::t('app', 'Urbroj'),
            'create_at' => Yii::t('app', 'Create At'),
            'update_at' => Yii::t('app', 'Update At'),
            'user' => Yii::t('app', 'User'),
            'dijete' => Yii::t('app', 'Dijete'),
            'status' => Yii::t('app', 'Status'),
            'klasa' => Yii::t('app', 'Klasa'),
            'ocijenjen' => Yii::t('app', 'Ocijenjen'),
            'bodovi' => Yii::t('app', 'Bodovi'),
            'izravan_upis' => Yii::t('app', 'Izravan Upis'),
        ];
    }
}
