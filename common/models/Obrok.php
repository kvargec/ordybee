<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "obrok".
 *
 * @property int $id
 * @property string|null $obrok
 *
 * @property Jelovnik[] $jelovniks
 */
class Obrok extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'obrok';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['obrok'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'obrok' => 'Obrok',
        ];
    }

    /**
     * Gets query for [[Jelovniks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJelovniks()
    {
        return $this->hasMany(Jelovnik::className(), ['obrok' => 'id']);
    }
}
