<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pitanje".
 *
 * @property int $id
 * @property string|null $pitanje
 * @property string|null $opis
 * @property int|null $upitnik
 * @property bool|null $is_required
 * @property int|null $vrsta_pitanja
 * @property string|null $odgovori
 *
 * @property Odgovori[] $odgovoris
 * @property Upitnik $upitnik0
 * @property VrstaPitanja $vrstaPitanja
 */
class Pitanje extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pitanje';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['opis'], 'string'],
            [['upitnik', 'vrsta_pitanja'], 'default', 'value' => null],
            [['upitnik', 'vrsta_pitanja'], 'integer'],
            [['is_required'], 'boolean'],
            [['odgovori'], 'safe'],
            [['pitanje'], 'string', 'max' => 250],
            [['upitnik'], 'exist', 'skipOnError' => true, 'targetClass' => Upitnik::className(), 'targetAttribute' => ['upitnik' => 'id']],
            [['vrsta_pitanja'], 'exist', 'skipOnError' => true, 'targetClass' => VrstaPitanja::className(), 'targetAttribute' => ['vrsta_pitanja' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pitanje' => Yii::t('app', 'Pitanje'),
            'opis' => Yii::t('app', 'Opis'),
            'upitnik' => Yii::t('app', 'Upitnik'),
            'is_required' => Yii::t('app', 'Is Required'),
            'vrsta_pitanja' => Yii::t('app', 'Vrsta Pitanja'),
            'odgovori' => Yii::t('app', 'Odgovori'),
        ];
    }

    /**
     * Gets query for [[Odgovoris]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOdgovoris()
    {
        return $this->hasMany(Odgovori::className(), ['pitanje' => 'id']);
    }

    /**
     * Gets query for [[Upitnik0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpitnik0()
    {
        return $this->hasOne(Upitnik::className(), ['id' => 'upitnik']);
    }

    /**
     * Gets query for [[VrstaPitanja]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVrstaPitanja()
    {
        return $this->hasOne(VrstaPitanja::className(), ['id' => 'vrsta_pitanja']);
    }
}
