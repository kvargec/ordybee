<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zahtjev_kriterij".
 *
 * @property int $id
 * @property int $zahtjev_id
 * @property int $kriterij_id
 * @property bool $zadovoljen
 * @property array|null $zadovoljene_opcije
 *
 * @property Kriterij $kriterij
 * @property Zahtjev $zahtjev
 */
class ZahtjevKriterij extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zahtjev_kriterij';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zahtjev_id', 'kriterij_id', 'zadovoljen'], 'required'],
            [['zahtjev_id', 'kriterij_id'], 'default', 'value' => null],
            [['zahtjev_id', 'kriterij_id'], 'integer'],
            [['zadovoljen'], 'boolean'],
            [['zadovoljene_opcije'], 'safe'],
            [['zahtjev_id', 'kriterij_id'], 'unique', 'targetAttribute' => ['zahtjev_id', 'kriterij_id']],
            [['kriterij_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kriterij::className(), 'targetAttribute' => ['kriterij_id' => 'id']],
            [['zahtjev_id'], 'exist', 'skipOnError' => true, 'targetClass' => Zahtjev::className(), 'targetAttribute' => ['zahtjev_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'zahtjev_id' => Yii::t('app', 'Zahtjev ID'),
            'kriterij_id' => Yii::t('app', 'Kriterij ID'),
            'zadovoljen' => Yii::t('app', 'Zadovoljen'),
            'zadovoljene_opcije' => Yii::t('app', 'Zadovoljene Opcije'),
        ];
    }

    /**
     * Gets query for [[Kriterij]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKriterij()
    {
        return $this->hasOne(Kriterij::className(), ['id' => 'kriterij_id']);
    }

    /**
     * Gets query for [[Zahtjev]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZahtjev()
    {
        return $this->hasOne(Zahtjev::className(), ['id' => 'zahtjev_id']);
    }
}
