<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dijete_zapis".
 *
 * @property int $id
 * @property int $dijete
 * @property int|null $zapis
 * @property string|null $dodatno
 *
 * @property Dijete $dijete0
 * @property DnevnikZapis $zapis0
 */
class DijeteZapis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dijete_zapis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dijete'], 'required'],
            [['dijete', 'zapis'], 'default', 'value' => null],
            [['dijete', 'zapis'], 'integer'],
            [['dodatno'], 'safe'],
            [['dijete'], 'exist', 'skipOnError' => true, 'targetClass' => Dijete::className(), 'targetAttribute' => ['dijete' => 'id']],
            [['zapis'], 'exist', 'skipOnError' => true, 'targetClass' => DnevnikZapis::className(), 'targetAttribute' => ['zapis' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dijete' => Yii::t('app', 'Dijete'),
            'zapis' => Yii::t('app', 'Zapis'),
            'dodatno' => Yii::t('app', 'Dodatno'),
        ];
    }

    /**
     * Gets query for [[Dijete0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDijete0()
    {
        return $this->hasOne(Dijete::className(), ['id' => 'dijete']);
    }

    /**
     * Gets query for [[Zapis0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZapis0()
    {
        return $this->hasOne(DnevnikZapis::className(), ['id' => 'zapis']);
    }
}
