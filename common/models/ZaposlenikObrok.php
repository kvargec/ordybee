<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zaposlenik_obrok".
 *
 * @property int $id
 * @property int $obrok_id
 * @property int $zaposlenik_id
 *
 * @property VrsteObroka $obrok
 * @property Zaposlenik $zaposlenik
 */
class ZaposlenikObrok extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zaposlenik_obrok';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['obrok_id', 'zaposlenik_id'], 'required'],
            [['obrok_id', 'zaposlenik_id'], 'default', 'value' => null],
            [['obrok_id', 'zaposlenik_id'], 'integer'],
            [['obrok_id'], 'exist', 'skipOnError' => true, 'targetClass' => VrsteObroka::className(), 'targetAttribute' => ['obrok_id' => 'id']],
            [['zaposlenik_id'], 'exist', 'skipOnError' => true, 'targetClass' => Zaposlenik::className(), 'targetAttribute' => ['zaposlenik_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'obrok_id' => Yii::t('app', 'Obrok ID'),
            'zaposlenik_id' => Yii::t('app', 'Zaposlenik ID'),
        ];
    }

    /**
     * Gets query for [[Obrok]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObrok()
    {
        return $this->hasOne(VrsteObroka::className(), ['id' => 'obrok_id']);
    }

    /**
     * Gets query for [[Zaposlenik]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenik()
    {
        return $this->hasOne(Zaposlenik::className(), ['id' => 'zaposlenik_id']);
    }
}
