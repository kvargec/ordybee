<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dijete_skupina".
 *
 * @property int $id
 * @property int|null $dijete
 * @property int|null $skupina
 *
 * @property Dijete $dijete0
 * @property Skupina $skupina0
 */
class DijeteSkupina extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dijete_skupina';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dijete', 'skupina'], 'default', 'value' => null],
            [['dijete', 'skupina'], 'integer'],
            [['dijete'], 'exist', 'skipOnError' => true, 'targetClass' => Dijete::className(), 'targetAttribute' => ['dijete' => 'id']],
            [['skupina'], 'exist', 'skipOnError' => true, 'targetClass' => Skupina::className(), 'targetAttribute' => ['skupina' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dijete' => Yii::t('app', 'Dijete'),
            'skupina' => Yii::t('app', 'Skupina'),
        ];
    }

    /**
     * Gets query for [[Dijete0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDijete0()
    {
        return $this->hasOne(Dijete::className(), ['id' => 'dijete']);
    }

    /**
     * Gets query for [[Skupina0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSkupina0()
    {
        return $this->hasOne(Skupina::className(), ['id' => 'skupina']);
    }

    public function getRoditeljs()
    {
        return $this->hasMany(Roditelj::className(), ['dijete' => 'id'])->via('dijete0');
    }
}
