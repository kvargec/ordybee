<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "msg_folders".
 *
 * @property int $id
 * @property string|null $folder_name
 * @property int|null $user
 * @property string|null $icon
 * @property int|null $redoslijed
 *
 * @property Mailbox[] $mailboxes
 */
class MsgFolders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'msg_folders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user', 'redoslijed'], 'default', 'value' => null],
            [['user', 'redoslijed'], 'integer'],
            [['folder_name'], 'string', 'max' => 255],
            [['icon'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'folder_name' => Yii::t('app', 'Folder Name'),
            'user' => Yii::t('app', 'User'),
            'icon' => Yii::t('app', 'Icon'),
            'redoslijed' => Yii::t('app', 'Redoslijed'),
        ];
    }

    /**
     * Gets query for [[Mailboxes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMailboxes()
    {
        return $this->hasMany(Mailbox::className(), ['folder' => 'id']);
    }
}
