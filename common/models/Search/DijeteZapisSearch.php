<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DijeteZapis;

/**
 * DijeteZapisSearch represents the model behind the search form of `common\models\DijeteZapis`.
 */
class DijeteZapisSearch extends DijeteZapis
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dijete', 'zapis'], 'integer'],
            [['dodatno'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DijeteZapis::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dijete' => $this->dijete,
            'zapis' => $this->zapis,
        ]);

        $query->andFilterWhere(['ilike', 'dodatno', $this->dodatno]);

        return $dataProvider;
    }
}
