<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Upitnik;

/**
 * UpitnikSearch represents the model behind the search form of `common\models\Upitnik`.
 */
class UpitnikSearch extends Upitnik
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ped_godina', 'creator', 'status'], 'integer'],
            [['naziv', 'opis', 'date_publish', 'date_end', 'created_at', 'upitnik_for'], 'safe'],
            [['is_required'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Upitnik::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ped_godina' => $this->ped_godina,
            'date_publish' => $this->date_publish,
            'date_end' => $this->date_end,
            'created_at' => $this->created_at,
            'creator' => $this->creator,
            'status' => $this->status,
            'is_required' => $this->is_required,
        ]);

        $query->andFilterWhere(['ilike', 'naziv', $this->naziv])
            ->andFilterWhere(['ilike', 'opis', $this->opis])
            ->andFilterWhere(['ilike', 'upitnik_for', $this->upitnik_for]);

        return $dataProvider;
    }
}
