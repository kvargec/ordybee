<?php

namespace common\models\Search;

use Codeception\Lib\Di;
use common\models\DijeteSkupina;
use common\models\PedagoskaGodina;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Dijete;

/**
 * DijeteSearch represents the model behind the search form of `common\models\Dijete`.
 */
class DijeteSearch extends Dijete
{
    public $full_name;
    public $skupina;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'god_cekanja', 'vrsta_programa', 'status'], 'integer'],
            [['ime', 'prezime', 'spol', 'dat_rod', 'oib', 'adresa', 'mjesto', 'prebivaliste', 'sestra', 'cekanje', 'razvoj', 'teskoce', 'dijagnostika', 'druge_potrebe', 'posebne', 'create_at', 'update_at', 'full_name', 'skupina', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (empty($params['sort'])) {
            $query = Dijete::find()->orderBy(['prezime' => SORT_ASC]);
        } else {
            $query = Dijete::find();
        }
        // add conditions that should always apply here
        $query->joinWith(['skupina0']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'prezime' => SORT_ASC,
                ]
            ]
        ]);
        $dataProvider->sort->attributes['skupina'] = [
            'asc' => ['skupina.naziv' => SORT_ASC],
            'desc' => ['skupina.naziv' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $aktivnaGodina=PedagoskaGodina::getAktivna();
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dat_rod' => $this->dat_rod,
            'god_cekanja' => $this->god_cekanja,
            'vrsta_programa' => $this->vrsta_programa,
            'status' => $this->status,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['ilike', 'ime', $this->ime])
            ->andFilterWhere(['ilike', 'prezime', $this->prezime])
            ->andFilterWhere(['ilike', 'spol', $this->spol])
            ->andFilterWhere(['ilike', 'oib', $this->oib])
            ->andFilterWhere(['ilike', 'adresa', $this->adresa])
            ->andFilterWhere(['ilike', 'mjesto', $this->mjesto])
            ->andFilterWhere(['ilike', 'prebivaliste', $this->prebivaliste])
            ->andFilterWhere(['ilike', 'sestra', $this->sestra])
            ->andFilterWhere(['ilike', 'cekanje', $this->cekanje])
            ->andFilterWhere(['ilike', 'razvoj', $this->razvoj])
            ->andFilterWhere(['ilike', 'teskoce', $this->teskoce])
            ->andFilterWhere(['ilike', 'dijagnostika', $this->dijagnostika])
            ->andFilterWhere(['ilike', 'druge_potrebe', $this->druge_potrebe])
            ->andFilterWhere(['ilike', 'posebne', $this->posebne])
            ->andFilterWhere(['ilike', 'skupina.naziv', $this->skupina])
            ->andFilterWhere(['skupina.ped_godina'=>$aktivnaGodina->id]);

        if (isset($this->full_name)) {
            $query->andFilterWhere(['or', ['ilike', 'ime', $this->full_name], ['ilike', 'prezime', $this->full_name]]);
        }
        return $dataProvider;
    }
}
