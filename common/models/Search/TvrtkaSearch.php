<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tvrtka;

/**
 * TvrtkaSearch represents the model behind the search form of `common\models\Tvrtka`.
 */
class TvrtkaSearch extends Tvrtka
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'mjesto', 'status'], 'integer'],
            [['naziv', 'sifra', 'oib', 'adresa', 'djelatnost'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tvrtka::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mjesto' => $this->mjesto,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['ilike', 'naziv', $this->naziv])
            ->andFilterWhere(['ilike', 'sifra', $this->sifra])
            ->andFilterWhere(['ilike', 'oib', $this->oib])
            ->andFilterWhere(['ilike', 'adresa', $this->adresa])
            ->andFilterWhere(['ilike', 'djelatnost', $this->djelatnost]);

        return $dataProvider;
    }
}
