<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DijeteSkupina;

/**
 * DijeteSkupinaSearch represents the model behind the search form of `common\models\DijeteSkupina`.
 */
class DijeteSkupinaSearch extends DijeteSkupina
{
    public $ime_djeteta;
    public $dat_rod;
    public $dat_upisa;
    public $oib;
    public $adresa;
    public $ime_majke;
    public $ime_oca;
    public $kontakt_majke;
    public $kontakt_oca;
    public $zanimanje_majke;
    public $zanimanje_oca;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dijete', 'skupina'], 'integer'],
            [['ime_djeteta', 'ime_majke', 'ime_oca', 'dat_rod', 'dat_upisa', 'oib', 'adresa', 'kontakt_majke', 'kontakt_oca', 'zanimanje_majke', 'zanimanje_oca'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $condition = [])
    {
        $query = DijeteSkupina::find()->where($condition);

        // add conditions that should always apply here
        $query->joinWith(['dijete0', 'roditeljs']);
        //$query->joinWith(['dijete0.roditeljs']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ]
        ]);
        $dataProvider->sort->attributes['ime'] = [
            'asc' => ['dijete.prezime' => SORT_ASC],
            'desc' => ['dijete.prezime' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['oib'] = [
            'asc' => ['dijete.oib' => SORT_ASC],
            'desc' => ['dijete.oib' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['adresa'] = [
            'asc' => ['dijete.adresa' => SORT_ASC],
            'desc' => ['dijete.adresa' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['dat_rod'] = [
            'asc' => ['dijete.dat_rod' => SORT_ASC],
            'desc' => ['dijete.dat_rod' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['dat_upisa'] = [
            'asc' => ['dijete.dat_upisa' => SORT_ASC],
            'desc' => ['dijete.dat_upisa' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['i_majke'] = [
            'asc' => ['roditelj.prezime' => SORT_ASC],
            'desc' => ['roditelj.prezime' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['i_oca'] = [
            'asc' => ['roditelj.prezime' => SORT_ASC],
            'desc' => ['roditelj.prezime' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kont_majke'] = [
            'asc' => ['roditelj.mobitel' => SORT_ASC],
            'desc' => ['roditelj.mobitel' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kont_oca'] = [
            'asc' => ['roditelj.mobitel' => SORT_ASC],
            'desc' => ['roditelj.mobitel' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['zan_majke'] = [
            'asc' => ['roditelj.zanimanje' => SORT_ASC],
            'desc' => ['roditelj.zanimanje' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['zan_oca'] = [
            'asc' => ['roditelj.zanimanje' => SORT_ASC],
            'desc' => ['roditelj.zanimanje' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dijete' => $this->dijete,
            'skupina' => $this->skupina,
        ]);
        $query->andFilterWhere(['or', ['ilike', 'dijete.ime', $this->ime_djeteta], ['ilike', 'dijete.prezime', $this->ime_djeteta]])
            ->andFilterWhere(['=', 'dijete.dat_rod', $this->dat_rod])
            ->andFilterWhere(['=', 'dijete.dat_upisa', $this->dat_upisa])
            ->andFilterWhere(['ilike', 'dijete.oib', $this->oib])
            ->andFilterWhere(['or', ['ilike', 'dijete.adresa', $this->adresa], ['ilike', 'dijete.mjesto', $this->adresa]])
            ->andFilterWhere(['or', ['ilike', 'roditelj.ime', $this->ime_majke], ['ilike', 'roditelj.prezime', $this->ime_majke]])
            ->andFilterWhere(['or', ['ilike', 'roditelj.ime', $this->ime_oca], ['ilike', 'roditelj.prezime', $this->ime_oca]])
            ->andFilterWhere(['ilike', 'roditelj.mobitel', $this->kontakt_majke])
            ->andFilterWhere(['ilike', 'roditelj.mobitel', $this->kontakt_oca])
            ->andFilterWhere(['ilike', 'roditelj.zanimanje', $this->zanimanje_majke])
            ->andFilterWhere(['ilike', 'roditelj.zanimanje', $this->zanimanje_oca]);

        return $dataProvider;
    }
}
