<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DokumentTemplate;

/**
 * DokumentTemplateSearch represents the model behind the search form of `common\models\DokumentTemplate`.
 */
class DokumentTemplateSearch extends DokumentTemplate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ped_godina'], 'integer'],
            [['naziv', 'template', 'slug', 'sadrzaj'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DokumentTemplate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ped_godina' => $this->ped_godina,
        ]);

        $query->andFilterWhere(['ilike', 'naziv', $this->naziv])
            ->andFilterWhere(['ilike', 'template', $this->template])
            ->andFilterWhere(['ilike', 'slug', $this->slug])
            ->andFilterWhere(['ilike', 'sadrzaj', $this->sadrzaj]);

        return $dataProvider;
    }
}
