<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Primka;

/**
 * PrimkaSearch represents the model behind the search form of `common\models\Primka`.
 */
class PrimkaSearch extends Primka
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ulazni_racun', 'unos'], 'integer'],
            [['broj_primke', 'datum_primke', 'created_at', 'primio'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Primka::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ulazni_racun' => $this->ulazni_racun,
            'datum_primke' => $this->datum_primke,
            'created_at' => $this->created_at,
            'unos' => $this->unos,
        ]);

        $query->andFilterWhere(['ilike', 'broj_primke', $this->broj_primke])
            ->andFilterWhere(['ilike', 'primio', $this->primio]);

        return $dataProvider;
    }
}
