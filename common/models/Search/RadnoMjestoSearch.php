<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RadnoMjesto;

/**
 * RadnoMjestoSearch represents the model behind the search form of `common\models\RadnoMjesto`.
 */
class RadnoMjestoSearch extends RadnoMjesto
{
    public $sprema;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sprema'], 'integer'],
            [['naziv', 'sifra', 'sprema'], 'safe'],
            [['koeficijent'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RadnoMjesto::find();
        $query->joinWith(['sprema0']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['sprema'] = [
            'asc' => ['sprema.naziv' => SORT_ASC],
            'desc' => ['sprema.naziv' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sprema' => $this->sprema,
            'koeficijent' => $this->koeficijent,
        ]);

        $query->andFilterWhere(['ilike', 'radno_mjesto.naziv', $this->naziv])
            ->andFilterWhere(['ilike', 'sifra', $this->sifra])
            ->andFilterWhere(['ilike', 'sprema.naziv', $this->sprema]);

        return $dataProvider;
    }
}
