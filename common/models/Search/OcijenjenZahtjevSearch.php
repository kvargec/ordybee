<?php

namespace common\models\search;

use common\models\PedagoskaGodina;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Zahtjev;
use yii\db\Query;

class OcijenjenZahtjevSearch extends Zahtjev
{
    public function rules()
    {
        return [
            [['id', 'user', 'dijete', 'status','bodovi'], 'integer'],
            [['urbroj', 'create_at', 'update_at', 'klasa','dijete_dat_rod'], 'safe'],
            [['ocijenjen'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$status=7)
    {
        // $query = ZahtjevKriterij::find();
        // $query->joinWith(['zahtjev', 'kriterij', 'dijete0']);
        $aktivnaGodina = PedagoskaGodina::getAktivna();
        $query = new Query;
        $query
        // ->select('zahtjev.*, sum(kriterij.bodovi) as bodovi, sum(kriterij.izravan_upis::int)>0 as izravan_upis')
        ->select('
            zahtjev.*,
            dijete.dat_rod as dijete_dat_rod,
            sum(kriterij.bodovi*((zahtjev_kriterij.zadovoljene_opcije->>0)::float)) as bodovi,
            sum(kriterij.izravan_upis::int)>0 as izravan_upis,
            CASE
                WHEN sum(kriterij.izravan_upis::int)>0 THEN \'IZRAVAN UPIS\'
                ELSE cast(sum(kriterij.bodovi*((zahtjev_kriterij.zadovoljene_opcije->>0)::float)) as text)
            END AS bodovi_izravan_upis
        ')
        ->from(['zahtjev_kriterij'])
        // ->from(['zahtjev_kriterij', 'zahtjev', 'kriterij'])
        ->innerJoin('kriterij', 'zahtjev_kriterij.kriterij_id = kriterij.id')
        ->innerJoin('zahtjev', 'zahtjev_kriterij.zahtjev_id = zahtjev.id')
        ->innerJoin('dijete', 'zahtjev.dijete = dijete.id')
        ->where(['zahtjev_kriterij.zadovoljen'=>true,'zahtjev.status'=>$status])
            ->andWhere(['zahtjev.pd_godina'=>$aktivnaGodina->id])
        // ->andWhere(['zahtjev_kriterij.zahtjev_id = zahtjev.id'])
        // ->andWhere(['zahtjev_kriterij.kriterij_id = kriterij.id'])
        ->groupBy('zahtjev.id, dijete.dat_rod')
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        $dataProvider->sort->attributes['izravan_upis'] = [
            'asc' => ['izravan_upis' => SORT_ASC],
            'desc' => ['izravan_upis' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['bodovi'] = [
            'asc' => ['bodovi' => SORT_ASC],
            'desc' => ['bodovi' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['dijete_dat_rod'] = [
            'asc' => ['dijete_dat_rod' => SORT_ASC],
            'desc' => ['dijete_dat_rod' => SORT_DESC],
        ];

        $dataProvider->sort->defaultOrder = [
            'izravan_upis' => SORT_DESC,
            'bodovi' => SORT_DESC,
            'dijete_dat_rod' => SORT_ASC,
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'user' => $this->user,
            'dijete' => $this->dijete,
            'status' => $this->status,
            'ocijenjen' => $this->ocijenjen,
        ]);

        $query->andFilterWhere(['ilike', 'urbroj', $this->urbroj])
            ->andFilterWhere(['ilike', 'klasa', $this->klasa]);

        return $dataProvider;
    }
}
