<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ZaposlenikSkupina;

/**
 * ZaposlenikSkupinaSearch represents the model behind the search form of `common\models\ZaposlenikSkupina`.
 */
class ZaposlenikSkupinaSearch extends ZaposlenikSkupina
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'zaposlenik', 'skupina', 'ped_godina'], 'integer'],
            [['aktivno'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ZaposlenikSkupina::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'zaposlenik' => $this->zaposlenik,
            'skupina' => $this->skupina,
            'ped_godina' => $this->ped_godina,
            'aktivno' => $this->aktivno,
        ]);

        return $dataProvider;
    }
}
