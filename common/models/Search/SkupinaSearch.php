<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Skupina;
use common\models\PedagoskaGodina;

/**
 * SkupinaSearch represents the model behind the search form of `common\models\Skupina`.
 */
class SkupinaSearch extends Skupina
{
    public $objekt_naziv;
    public $full_name_zaposlenik;
    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['id', 'ped_godina'], 'integer'],
            [['naziv', 'sifra', 'slika', 'objekt_naziv', 'full_name_zaposlenik'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Skupina::find();

        // add conditions that should always apply here
        $query->joinWith(['objekts', 'zaposleniks']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ]
        ]);
        $dataProvider->sort->attributes['objekt_naziv'] = [
            'asc' => ['objekt.naziv' => SORT_ASC],
            'desc' => ['objekt.naziv' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['zaposlenik_ime'] = [
            'asc' => ['zaposlenik.ime' => SORT_ASC],
            'desc' => ['zaposlenik.ime' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        // grid filtering conditions
        $query->andFilterWhere([
            'skupina.id' => $this->id,
            'skupina.ped_godina' => $godina->id //$this->ped_godina,
        ]);

        $query->andFilterWhere(['ilike', 'skupina.naziv', $this->naziv])
            ->andFilterWhere(['ilike', 'skupina.sifra', $this->sifra])
            ->andFilterWhere(['ilike', 'skupina.slika', $this->slika])
            ->andFilterWhere(['ilike', 'objekt.naziv', $this->objekt_naziv]);



        if (isset($this->full_name_zaposlenik)) {
            $query->andFilterWhere(['or', ['ilike', 'zaposlenik.ime', $this->full_name_zaposlenik], ['ilike', 'zaposlenik.prezime', $this->full_name_zaposlenik]]);
        }

        return $dataProvider;
    }
}
