<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PedagoskaGodina;

/**
 * PedagoskaGodinaSearch represents the model behind the search form of `common\models\PedagoskaGodina`.
 */
class PedagoskaGodinaSearch extends PedagoskaGodina
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['od', 'do', 'postavke'], 'safe'],
            [['aktivna'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PedagoskaGodina::find()->orderBy('id desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'od' => $this->od,
            'do' => $this->do,
            'aktivna' => $this->aktivna,
        ]);

        $query->andFilterWhere(['ilike', 'postavke', $this->postavke]);

        return $dataProvider;
    }
}
