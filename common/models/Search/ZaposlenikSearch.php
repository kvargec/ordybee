<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Zaposlenik;

/**
 * ZaposlenikSearch represents the model behind the search form of `common\models\Zaposlenik`.
 */
class ZaposlenikSearch extends Zaposlenik
{
    public $mjesto;
    public $radno_mjesto;
    public $kontakt;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sprema', 'status'], 'integer'],
            [['ime', 'prezime', 'adresa', 'oib', 'dat_rodjenja', 'spol', 'created_at', 'updated_at', 'mjesto', 'titula', 'radno_mjesto', 'kontakt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zaposlenik::find();

        // add conditions that should always apply here
        $query->joinWith(['mjesto0', 'zaposlenjes', 'radnoMjesto0', 'kontaktDetails']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'prezime' => SORT_ASC,
                ]
            ]
        ]);
        $dataProvider->sort->attributes['mjesto'] = [
            'asc' => ['mjesto.naziv' => SORT_ASC],
            'desc' => ['mjesto.naziv' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['radno_mjesto'] = [
            'asc' => ['radno_mjesto.naziv' => SORT_ASC],
            'desc' => ['radno_mjesto.naziv' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontakt'] = [
            'asc' => ['kontakt.vrijednost' => SORT_ASC],
            'desc' => ['kontakt.vrijednost' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dat_rodjenja' => $this->dat_rodjenja,
            'sprema' => $this->sprema,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'ime', $this->ime])
            ->andFilterWhere(['ilike', 'titula', $this->titula])
            ->andFilterWhere(['ilike', 'prezime', $this->prezime])
            ->andFilterWhere(['ilike', 'adresa', $this->adresa])
            ->andFilterWhere(['ilike', 'oib', $this->oib])
            ->andFilterWhere(['ilike', 'spol', $this->spol])
            ->andFilterWhere(['!=', 'status', 3])
            ->andFilterWhere(['ilike', 'radno_mjesto.naziv', $this->radno_mjesto])
            ->andFilterWhere(['ilike', 'mjesto.naziv', $this->mjesto])
            ->andFilterWhere(['ilike', 'kontakt.vrijednost', $this->kontakt]);

        return $dataProvider;
    }
}
