<?php

namespace common\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Mailbox;

/**
 * MailboxSearch represents the model behind the search form of `common\models\Mailbox`.
 */
class MailboxSearch extends Mailbox
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user', 'message', 'mstatus', 'folder'], 'integer'],
            [['created_at', 'last_change_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $folder = null)
    {
        $query = Mailbox::find()->select(['user', 'message', 'mstatus', 'folder', 'created_at'])->orderBy('created_at DESC')->distinct();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $userID = Yii::$app->user->id;

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user' => $userID,
            'message' => $this->message,
            'created_at' => $this->created_at,
            'last_change_at' => $this->last_change_at,
            'mstatus' => $this->mstatus,

        ]);

        if ($folder != null) {
            $query->andFilterWhere([
                'folder' => $folder
            ])->andFilterWhere(['not', ['mstatus' => 4]]); //->orderBy('created_at DESC');
        }

        return $dataProvider;
    }
}
