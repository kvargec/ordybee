<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Notifikacije;

/**
 * NotifikacijeSearch represents the model behind the search form of `common\models\Notifikacije`.
 */
class NotifikacijeSearch extends Notifikacije
{
    public $inbox;
    public $sent;
    public $trash;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user', 'akcija_id', 'status'], 'integer'],
            [['akcija', 'radnja', 'created_at', 'read_at', 'sadrzaj'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notifikacije::find()->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user' => $this->user,
            'akcija_id' => $this->akcija_id,
            'created_at' => $this->created_at,
            'read_at' => $this->read_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['ilike', 'akcija', $this->akcija])
            ->andFilterWhere(['ilike', 'radnja', $this->radnja])
            ->andFilterWhere(['ilike', 'sadrzaj', $this->sadrzaj]);

        if (isset($this->inbox)){
            $query = $query->andWhere(['user' => $this->inbox])->andWhere(['user_trash' => false, 'user_delete' => false]);
        }
        if (isset($this->trash)){
            $query = $query->andWhere(['or', ['sender_id' => $this->trash, 'sender_trash' =>true, 'sender_delete' => false], ['user' => $this->trash, 'user_trash' => true, 'user_delete' => false]]);
        }
        if (isset($this->sent)){
            $query = $query->andWhere(['sender_id' => $this->sent])->andWhere(['sender_delete' => false, 'sender_trash' => false]);
        }
        return $dataProvider;
    }
}
