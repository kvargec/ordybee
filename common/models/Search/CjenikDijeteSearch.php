<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CjenikDijete;

/**
 * CjenikDijeteSearch represents the model behind the search form of `common\models\CjenikDijete`.
 */
class CjenikDijeteSearch extends CjenikDijete
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dijete'], 'integer'],
            [['cijena'], 'number'],
            [['dat_poc', 'dat_kraj'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CjenikDijete::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dijete' => $this->dijete,
            'cijena' => $this->cijena,
            'dat_poc' => $this->dat_poc,
            'dat_kraj' => $this->dat_kraj,
        ]);

        return $dataProvider;
    }
}
