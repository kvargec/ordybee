<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Aktivnosti;

/**
 * AktivnostiSearch represents the model behind the search form of `common\models\Aktivnosti`.
 */
class AktivnostiSearch extends Aktivnosti
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'datum_pocetka', 'datum_kraja', 'status', 'created_at', 'author_id', 'updater_id'], 'integer'],
            [['naziv', 'trajanje', 'opis', 'skupine'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Aktivnosti::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'datum_pocetka' => $this->datum_pocetka,
            'datum_kraja' => $this->datum_kraja,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'author_id' => $this->author_id,
            'updater_id' => $this->updater_id,
        ]);

        $query->andFilterWhere(['ilike', 'naziv', $this->naziv])
            ->andFilterWhere(['ilike', 'trajanje', $this->trajanje])
            //->andFilterWhere(['like', 'datum_pocetka', $this->datum_pocetka])
            //->andFilterWhere(['like', 'datum_kraja', $this->datum_kraja])
            ->andFilterWhere(['ilike', 'opis', $this->opis])
            ->andFilterWhere(['ilike', 'skupine', $this->skupine]);

        return $dataProvider;
    }
}
