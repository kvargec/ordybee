<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Zaposlenje;

/**
 * ZaposlenjeSearch represents the model behind the search form of `common\models\Zaposlenje`.
 */
class ZaposlenjeSearch extends Zaposlenje
{
    public $r_mjesto;
    public $full_name_zaposlenik;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'zaposlenik', 'vrsta', 'r_mjesto'], 'integer'],
            [['dat_pocetka', 'date_kraj', 'created_at', 'updated_at', 'r_mjesto', 'full_name_zaposlenik'], 'safe'],
            [['koeficijent'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zaposlenje::find();

        // add conditions that should always apply here
        $query->joinWith(['rMjesto']);
        $query->joinWith(['zaposlenik0']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['r_mjesto'] = [
            'asc' => ['radno_mjesto.naziv' => SORT_ASC],
            'desc' => ['radno_mjesto.naziv' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['ime'] = [
            'asc' => ['zaposlenik.ime' => SORT_ASC],
            'desc' => ['zaposlenik.ime' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dat_pocetka' => $this->dat_pocetka,
            'date_kraj' => $this->date_kraj,
            'koeficijent' => $this->koeficijent,
            'zaposlenik' => $this->zaposlenik,
            'vrsta' => $this->vrsta,
            'r_mjesto' => $this->r_mjesto,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        $query->andFilterWhere(['ilike', 'radno_mjesto.naziv', $this->r_mjesto]);

        if (isset($this->full_name_zaposlenik)) {
            $query->andFilterWhere(['or', ['ilike', 'zaposlenik.ime', $this->full_name_zaposlenik], ['ilike', 'zaposlenik.prezime', $this->full_name_zaposlenik]]);
        }

        return $dataProvider;
    }
}
