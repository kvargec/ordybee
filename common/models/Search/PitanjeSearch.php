<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pitanje;

/**
 * PitanjeSearch represents the model behind the search form of `common\models\Pitanje`.
 */
class PitanjeSearch extends Pitanje
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'upitnik', 'vrsta_pitanja'], 'integer'],
            [['pitanje', 'opis', 'odgovori'], 'safe'],
            [['is_required'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pitanje::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'upitnik' => $this->upitnik,
            'is_required' => $this->is_required,
            'vrsta_pitanja' => $this->vrsta_pitanja,
        ]);

        $query->andFilterWhere(['ilike', 'pitanje', $this->pitanje])
            ->andFilterWhere(['ilike', 'opis', $this->opis])
            ->andFilterWhere(['ilike', 'odgovori', $this->odgovori]);

        return $dataProvider;
    }
}
