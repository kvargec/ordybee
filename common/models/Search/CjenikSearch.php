<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Cjenik;

/**
 * CjenikSearch represents the model behind the search form of `common\models\Cjenik`.
 */
class CjenikSearch extends Cjenik
{
    public $program;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ped_godina'], 'integer'],
            [['cijena', 'povlastena'], 'number'],
            [['program'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cjenik::find();

        // add conditions that should always apply here
        $query->joinWith(['program0']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['program'] = [
            'asc' => ['vrsta_programa.vrsta' => SORT_ASC],
            'desc' => ['vrsta_programa.vrsta' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cijena' => $this->cijena,
            'povlastena' => $this->povlastena,
            'ped_godina' => $this->ped_godina,
        ]);
        $query->andFilterWhere(['ilike', 'vrsta_programa.vrsta', $this->program]);

        return $dataProvider;
    }
}
