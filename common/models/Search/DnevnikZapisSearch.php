<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DnevnikZapis;

/**
 * DnevnikZapisSearch represents the model behind the search form of `common\models\DnevnikZapis`.
 */
class DnevnikZapisSearch extends DnevnikZapis
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dnevnik', 'vrsta', 'status'], 'integer'],
            [['zapis', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $condition = [])
    {
        $query = DnevnikZapis::find()->where($condition);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dnevnik' => $this->dnevnik,
            'vrsta' => $this->vrsta,
            'status' => $this->status,
            'vrijednosti_atributa' => $this->vrijednosti_atributa,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'zapis', $this->zapis]);

        return $dataProvider;
    }
}
