<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Roditelj;

/**
 * RoditeljSearch represents the model behind the search form of `common\models\Roditelj`.
 */
class RoditeljSearch extends Roditelj
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dijete'], 'integer'],
            [['ime', 'prezime', 'spol', 'dat_rod', 'oib', 'adresa', 'mjesto', 'prebivaliste', 'radno', 'zanimanje', 'poslodavaca', 'mobitel', 'email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Roditelj::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dat_rod' => $this->dat_rod,
            'dijete' => $this->dijete,
        ]);

        $query->andFilterWhere(['ilike', 'ime', $this->ime])
            ->andFilterWhere(['ilike', 'prezime', $this->prezime])
            ->andFilterWhere(['ilike', 'spol', $this->spol])
            ->andFilterWhere(['ilike', 'oib', $this->oib])
            ->andFilterWhere(['ilike', 'adresa', $this->adresa])
            ->andFilterWhere(['ilike', 'mjesto', $this->mjesto])
            ->andFilterWhere(['ilike', 'prebivaliste', $this->prebivaliste])
            ->andFilterWhere(['ilike', 'radno', $this->radno])
            ->andFilterWhere(['ilike', 'zanimanje', $this->zanimanje])
            ->andFilterWhere(['ilike', 'poslodavaca', $this->poslodavaca])
            ->andFilterWhere(['ilike', 'mobitel', $this->mobitel])
            ->andFilterWhere(['ilike', 'email', $this->email]);

        return $dataProvider;
    }
}
