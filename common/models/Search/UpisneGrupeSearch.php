<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UpisneGrupe;

/**
 * UpisneGrupeSearch represents the model behind the search form of `common\models\UpisneGrupe`.
 */
class UpisneGrupeSearch extends UpisneGrupe
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'broj', 'ped_godina'], 'integer'],
            [['naziv', 'date_poc', 'date_kraj'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UpisneGrupe::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_poc' => $this->date_poc,
            'date_kraj' => $this->date_kraj,
            'broj' => $this->broj,
            'ped_godina' => $this->ped_godina,
        ]);

        $query->andFilterWhere(['ilike', 'naziv', $this->naziv]);

        return $dataProvider;
    }
}
