<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MediaRecipients;

/**
 * MediaRecipientsSearch represents the model behind the search form of `common\models\MediaRecipients`.
 */
class MediaRecipientsSearch extends MediaRecipients
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'media', 'user'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MediaRecipients::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'media' => $this->media,
            'user' => $this->user,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
