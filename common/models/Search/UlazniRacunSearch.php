<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UlazniRacun;

/**
 * UlazniRacunSearch represents the model behind the search form of `common\models\UlazniRacun`.
 */
class UlazniRacunSearch extends UlazniRacun
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dobavljac'], 'integer'],
            [['naslov', 'napomena', 'datum_racuna', 'created_at', 'preuzeo'], 'safe'],
            [['iznos'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UlazniRacun::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dobavljac' => $this->dobavljac,
            'datum_racuna' => $this->datum_racuna,
            'created_at' => $this->created_at,
            'iznos' => $this->iznos,
        ]);

        $query->andFilterWhere(['ilike', 'naslov', $this->naslov])
            ->andFilterWhere(['ilike', 'napomena', $this->napomena])
            ->andFilterWhere(['ilike', 'preuzeo', $this->preuzeo]);

        return $dataProvider;
    }
}
