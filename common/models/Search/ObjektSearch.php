<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Objekt;

/**
 * ObjektSearch represents the model behind the search form of `common\models\Objekt`.
 */
class ObjektSearch extends Objekt
{
    public $mjesto_naziv;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'mjesto', 'ped_godina'], 'integer'],
            [['naziv', 'adresa', 'sifra', 'mjesto_naziv'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Objekt::find();

        // add conditions that should always apply here
        $query->joinWith(['mjesto0']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['mjesto_naziv'] = [
            'asc' => ['mjesto.naziv' => SORT_ASC],
            'desc' => ['mjesto.naziv' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mjesto' => $this->mjesto,
            'ped_godina' => $this->ped_godina,
        ]);

        $query->andFilterWhere(['ilike', 'objekt.naziv', $this->naziv])
            ->andFilterWhere(['ilike', 'adresa', $this->adresa])
            ->andFilterWhere(['ilike', 'sifra', $this->sifra])
            ->andFilterWhere(['ilike', 'mjesto.naziv', $this->mjesto_naziv]);

        return $dataProvider;
    }
}
