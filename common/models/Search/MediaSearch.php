<?php

namespace common\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Media;

/**
 * MediaSearch represents the model behind the search form of `common\models\Media`.
 */
class MediaSearch extends Media
{
    public $mediaType;
    public $mediaRecipients;
    public $user_username;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'media_type', 'size', 'user_id'], 'integer'],
            [['naziv', 'opis', 'filename', 'author', 'mediaType', 'mediaRecipients', 'user_username'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Media::find();

        // add conditions that should always apply here
        $query->joinWith(['mediaType', 'user', 'mediaRecipients']);
        $userID = Yii::$app->user->id;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        $dataProvider->sort->attributes['mediaType'] = [
            'asc' => ['media_type.naziv' => SORT_ASC],
            'desc' => ['media_type.naziv' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['user_username'] = [
            'asc' => ['user.username' => SORT_ASC],
            'desc' => ['user.username' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'media_type' => $this->media_type,
            'size' => $this->size,
            'created_at' => $this->created_at,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['ilike', 'media.naziv', $this->naziv])
            ->andFilterWhere(['ilike', 'opis', $this->opis])
            ->andFilterWhere(['ilike', 'filename', $this->filename])
            ->andFilterWhere(['ilike', 'author', $this->author])
            ->andFilterWhere(['ilike', 'user.username', $this->user_username])
            ->andFilterWhere(['ilike', 'media_type.naziv', $this->mediaType]);

        if (Yii::$app->user->can('superadmin') || Yii::$app->user->can('administracija')) {
        } else {
            $query->andFilterWhere(['=', 'media_recipients.user', $userID]);
        }

        return $dataProvider;
    }
}
