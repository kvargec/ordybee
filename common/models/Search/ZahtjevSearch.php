<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Zahtjev;

/**
 * ZahtjevSearch represents the model behind the search form of `common\models\Zahtjev`.
 */
class ZahtjevSearch extends Zahtjev
{
    public $dijete_ime;
    public $dijete_prezime;
    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        // add related fields to searchable attributes

        return array_merge(parent::attributes(), ['dijete.prezime', 'dijete.ime']);
    }
    public function rules()
    {
        return [
            [['id', 'user', 'dijete', 'status', 'pd_godina'], 'integer'],
            [['urbroj', 'dijete_prezime', 'dijete_ime', 'update_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $condition = [])
    {
        $query = Zahtjev::find()->where($condition);

        // add conditions that should always apply here
        $query->joinWith(['dijete0']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['status' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        $dataProvider->sort->attributes['dijete_ime'] = [
            'asc' => ['dijete.ime' => SORT_ASC],
            'desc' => ['dijete.ime' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['dijete_prezime'] = [
            'asc' => ['dijete.prezime' => SORT_ASC],
            'desc' => ['dijete.prezime' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'zahtjev.create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'user' => $this->user,
            'zahtjev.status' => $this->status,
            'pd_godina' => $this->pd_godina
        ]);

        $query->andFilterWhere(['ilike', 'urbroj', $this->urbroj]);
        $query->andFilterWhere(['ilike', 'dijete.prezime', $this->dijete_prezime]);
        $query->andFilterWhere(['ilike', 'dijete.ime', $this->dijete_ime]);

        return $dataProvider;
    }
}
