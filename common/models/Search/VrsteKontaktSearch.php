<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VrsteKontakt;

/**
 * VrsteKontaktSearch represents the model behind the search form of `common\models\VrsteKontakt`.
 */
class VrsteKontaktSearch extends VrsteKontakt
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'redoslijed'], 'integer'],
            [['vrsta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VrsteKontakt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'redoslijed' => $this->redoslijed,
        ]);

        $query->andFilterWhere(['ilike', 'vrsta', $this->vrsta]);

        return $dataProvider;
    }
}
