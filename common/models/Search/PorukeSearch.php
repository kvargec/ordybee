<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Poruke;

/**
 * PorukeSearch represents the model behind the search form of `common\models\Poruke`.
 */
class PorukeSearch extends Poruke
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sender', 'status', 'priority', 'vrsta_poruke'], 'integer'],
            [['subject', 'message', 'recipients', 'created_at', 'updated_at', 'send_at', 'attachments'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Poruke::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sender' => $this->sender,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'send_at' => $this->send_at,
            'status' => $this->status,
            'priority' => $this->priority,
            'vrsta_poruke' => $this->vrsta_poruke,
        ]);

        $query->andFilterWhere(['ilike', 'subject', $this->subject])
            ->andFilterWhere(['ilike', 'message', $this->message])
            ->andFilterWhere(['ilike', 'recipients', $this->recipients])
            ->andFilterWhere(['ilike', 'attachments', $this->attachments]);

        return $dataProvider;
    }
}
