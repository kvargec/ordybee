<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Intervju;

/**
 * IntervjuSearch represents the model behind the search form of `common\models\Intervju`.
 */
class IntervjuSearch extends Intervju
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'zahtjev'], 'integer'],
            [['vrijeme', 'lokacija', 'napomena'], 'safe'],
            [['odrzan'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Intervju::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'zahtjev' => $this->zahtjev,
            'vrijeme' => $this->vrijeme,
            'odrzan' => $this->odrzan,
        ]);

        $query->andFilterWhere(['ilike', 'lokacija', $this->lokacija])
            ->andFilterWhere(['ilike', 'napomena', $this->napomena]);

        return $dataProvider;
    }
}
