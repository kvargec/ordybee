<?php

namespace common\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Dnevnik;
use common\models\PedagoskaGodina;

/**
 * DnevnikSearch represents the model behind the search form of `common\models\Dnevnik`.
 */
class DnevnikSearch extends Dnevnik
{
    public $full_name_zaposlenik;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'zaposlenik', 'ped_godina', 'vrsta_dnevnika'], 'integer'],
            [['sifra', 'created_at', 'updated_at', 'full_name_zaposlenik'], 'safe'], //'zaposlenik_ime', 'zaposlenik_prezime'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dnevnik::find();

        // add conditions that should always apply here
        $query->joinWith(['zaposlenik0']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['ime'] = [
            'asc' => ['zaposlenik.ime' => SORT_ASC],
            'desc' => ['zaposlenik.ime' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'zaposlenik' => $this->zaposlenik,
            'ped_godina' => $godina->id, //$this->ped_godina,
            'vrsta_dnevnika' => $this->vrsta_dnevnika,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'sifra', $this->sifra]);

        if (isset($this->full_name_zaposlenik)) {
            $query->andFilterWhere(['or', ['ilike', 'zaposlenik.ime', $this->full_name_zaposlenik], ['ilike', 'zaposlenik.prezime', $this->full_name_zaposlenik]]);
        }
        return $dataProvider;
    }
}
