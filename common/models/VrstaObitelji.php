<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vrsta_obitelji".
 *
 * @property int $id
 * @property string|null $naziv
 *
 * @property Zahtjev[] $zahtjevs
 */
class VrstaObitelji extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrsta_obitelji';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
        ];
    }

    /**
     * Gets query for [[Zahtjevs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZahtjevs()
    {
        return $this->hasMany(Zahtjev::className(), ['vrsta_obitelji' => 'id']);
    }
}
