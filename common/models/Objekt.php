<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "objekt".
 *
 * @property int $id
 * @property string $naziv
 * @property string|null $adresa
 * @property string|null $sifra
 * @property int|null $mjesto
 * @property int|null $ped_godina
 *
 * @property Mjesto $mjesto0
 * @property PedagoskaGodina $pedGodina
 * @property SkupinaObjekt[] $skupinaObjekts
 */
class Objekt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objekt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'required'],
            [['mjesto', 'ped_godina'], 'default', 'value' => null],
            [['mjesto', 'ped_godina'], 'integer'],
            [['naziv'], 'string', 'max' => 100],
            [['adresa'], 'string', 'max' => 255],
            [['sifra'], 'string', 'max' => 20],
            [['mjesto'], 'exist', 'skipOnError' => true, 'targetClass' => Mjesto::className(), 'targetAttribute' => ['mjesto' => 'id']],
            [['ped_godina'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['ped_godina' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'adresa' => Yii::t('app', 'Adresa'),
            'sifra' => Yii::t('app', 'Sifra'),
            'mjesto' => Yii::t('app', 'Mjesto'),
            'ped_godina' => Yii::t('app', 'Ped Godina'),
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // prilikom spremanja zapisa atribut pedagoska godina se postavlja na trenutno aktivnu godinu
        //$this->ped_godina = PedagoskaGodina::getAktivna()->id;

        return true;
    }

    /**
     * Gets query for [[Mjesto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMjesto0()
    {
        return $this->hasOne(Mjesto::className(), ['id' => 'mjesto']);
    }

    /**
     * Gets query for [[PedGodina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'ped_godina']);
    }

    /**
     * Gets query for [[SkupinaObjekts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSkupinaObjekts()
    {
        return $this->hasMany(SkupinaObjekt::className(), ['objekt' => 'id']);
    }
    public function fullPodaci(){
        return $this->naziv.', '.$this->adresa.' '.$this->mjesto0->naziv;
    }
}
