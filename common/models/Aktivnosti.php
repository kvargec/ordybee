<?php

namespace common\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "aktivnosti".
 *
 * @property int $id
 * @property string|null $naziv
 * @property int|null $datum_pocetka
 * @property int|null $datum_kraja
 * @property string|null $trajanje
 * @property string|null $opis
 * @property string|null $skupine
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $author_id
 * @property int|null $updater_id
 */
class Aktivnosti extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aktivnosti';
    }

    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'updater_id',
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['datum_kraja'],
                    self::EVENT_BEFORE_UPDATE => ['datum_kraja'],
                ],
                'value' => function ($event) {
                    if(!$this->datum_kraja) {
                        return $this->datum_kraja;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    return date_format(date_create_from_format($format, $this->datum_kraja), 'Y-m-d H:i');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['datum_pocetka'],
                    self::EVENT_BEFORE_UPDATE => ['datum_pocetka'],
                ],
                'value' => function ($event) {
                    if(!$this->datum_pocetka) {
                        return $this->datum_pocetka;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    return date_format(date_create_from_format($format, $this->datum_pocetka), 'Y-m-d H:i');
                },
            ],

        ];
    }
    public function rules()
    {
        return [
            [['status', 'created_at', 'author_id', 'updater_id'], 'default', 'value' => null],
            [['status', 'created_at', 'author_id', 'updater_id'], 'integer'],
            [['datum_pocetka', 'datum_kraja'], 'date', 'format' => Yii::$app->formatter->datetimeFormat],
            [['skupine'], 'safe'],
            [['naziv', 'trajanje', 'opis'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'naziv' => 'Naziv',
            'datum_pocetka' => 'Datum Pocetka',
            'datum_kraja' => 'Datum Kraja',
            'trajanje' => 'Trajanje',
            'opis' => 'Opis',
            'skupine' => 'Skupine',
            'status' => 'Status',
            'created_at' => 'Created At',
            'user_id' => 'User ID',
        ];
    }
}
