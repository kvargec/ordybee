<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "skupina_objekt".
 *
 * @property int $id
 * @property int|null $skupina
 * @property int|null $objekt
 * @property bool|null $aktivna
 *
 * @property Objekt $objekt0
 * @property Skupina $skupina0
 */
class SkupinaObjekt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'skupina_objekt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['skupina', 'objekt'], 'default', 'value' => null],
            [['skupina', 'objekt'], 'integer'],
            [['aktivna'], 'boolean'],
            [['objekt'], 'exist', 'skipOnError' => true, 'targetClass' => Objekt::className(), 'targetAttribute' => ['objekt' => 'id']],
            [['skupina'], 'exist', 'skipOnError' => true, 'targetClass' => Skupina::className(), 'targetAttribute' => ['skupina' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'skupina' => Yii::t('app', 'Skupina'),
            'objekt' => Yii::t('app', 'Objekt'),
            'aktivna' => Yii::t('app', 'Aktivna'),
        ];
    }

    /**
     * Gets query for [[Objekt0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjekt0()
    {
        return $this->hasOne(Objekt::className(), ['id' => 'objekt']);
    }

    /**
     * Gets query for [[Skupina0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSkupina0()
    {
        return $this->hasOne(Skupina::className(), ['id' => 'skupina']);
    }

    public function getObjekts()
    {
        return $this->hasMany(Objekt::className(), ['id' => 'objekt'])->via('skupinaObjekts');
    }

}
