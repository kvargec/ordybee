<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vrsta_unosa".
 *
 * @property int $id
 * @property string $vrsta
 *
 * @property Kriterij[] $kriterijs
 */
class VrstaUnosa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrsta_unosa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vrsta'], 'required'],
            [['vrsta'], 'string', 'max' => 255],
            [['vrsta'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vrsta' => Yii::t('app', 'Vrsta'),
        ];
    }

    /**
     * Gets query for [[Kriterijs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKriterijs()
    {
        return $this->hasMany(Kriterij::className(), ['vrsta_unosa_id' => 'id']);
    }
}
