<?php

namespace common\models;

use Yii;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "roditelj".
 *
 * @property int $id
 * @property string $ime
 * @property string $prezime
 * @property string|null $spol
 * @property string|null $dat_rod
 * @property string|null $oib
 * @property string|null $adresa
 * @property string|null $mjesto
 * @property string|null $prebivaliste
 * @property string|null $drzavljanstvo
 * @property string|null $radno
 * @property string|null $zanimanje
 * @property string|null $poslodavaca
 * @property string|null $adresa_poslodavaca
 * @property string|null $mobitel
 * @property string|null $email
 * @property int|null $dijete
 * @property string|null $adresa_prebivalista
 * @property string|null $prebivaliste_jednako_boraviste
 *
 * @property Dijete $dijete0
 */
class Roditelj extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'roditelj';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ime', 'prezime', 'oib', 'adresa', 'mjesto', 'mobitel', 'zanimanje', 'poslodavaca'], 'required'], //'spol', 'drzavljanstvo', 'adresa_poslodavca'
            ['dat_rod', 'date', 'format' => Yii::$app->formatter->dateFormat],
            [['dijete'], 'default', 'value' => null],
            [['dijete'], 'integer'],
            [['ime', 'prezime', 'adresa', 'mjesto', 'drzavljanstvo', 'adresa_prebivalista'], 'string', 'max' => 150],

            [['spol', 'prebivaliste_jednako_boraviste'], 'string', 'max' => 1],
            [['oib'], 'string', 'max' => 13],
            [['prebivaliste', 'radno', 'zanimanje', 'poslodavaca', 'adresa_poslodavca'], 'string', 'max' => 200],
            [['mobitel', 'email'], 'string', 'max' => 50],
            [['dijete'], 'exist', 'skipOnError' => true, 'targetClass' => Dijete::className(), 'targetAttribute' => ['dijete' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['dat_rod'],
                    self::EVENT_BEFORE_UPDATE => ['dat_rod'],
                ],
                'value' => function ($event) {
                    if (!$this->dat_rod) {
                        return $this->dat_rod;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->dateFormat, 4);
                    return date_format(date_create_from_format($format, $this->dat_rod), 'Y-m-d');
                },
            ],
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->dat_rod = Yii::$app->formatter->asDate($this->dat_rod);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ime' => Yii::t('app', 'Ime'),
            'prezime' => Yii::t('app', 'Prezime'),
            'spol' => Yii::t('app', 'Spol'),
            'dat_rod' => Yii::t('app', 'Dat Rod'),
            'oib' => Yii::t('app', 'Oib'),
            'adresa' => Yii::t('app', 'Adresa'),
            'mjesto' => Yii::t('app', 'Mjesto'),
            'prebivaliste' => Yii::t('app', 'Prebivaliste'),
            'radno' => Yii::t('app', 'Radno vrijeme'),
            'drzavljanstvo' => Yii::t('app', 'Državljanstvo'),
            'zanimanje' => Yii::t('app', 'Zanimanje'),
            'poslodavaca' => Yii::t('app', 'Naziv poslodavca'),
            'adresa_poslodavaca' => Yii::t('app', 'Adresa poslodavca'),
            'mobitel' => Yii::t('app', 'Mobitel'),
            'email' => Yii::t('app', 'Email'),
            'dijete' => Yii::t('app', 'Dijete'),
            'adresa_prebivalista' => Yii::t('app', 'Adresa prebivališta'),
        ];
    }

    /**
     * Gets query for [[Dijete0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDijete0()
    {
        return $this->hasOne(Dijete::className(), ['id' => 'dijete']);
    }
    public function osnovniPodaci($tip = 'R')
    {
        // R za sve u isti redak, N svaki podatak u novi red
        $razmak = $tip == 'R' ? ", " : "<br />";
        $tekst = $this->ime . ' ' . $this->prezime . $razmak . Yii::$app->formatter->asDate($this->dat_rod);
        $tekst .= $razmak . 'Spol: ' . $this->spol;
        $tekst .= $razmak . 'OIB: ' . $this->oib . $razmak . 'Državljanstvo: ' . $this->drzavljanstvo;
        return $tekst;
    }
}
