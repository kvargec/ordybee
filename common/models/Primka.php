<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "primka".
 *
 * @property int $id
 * @property string|null $broj_primke
 * @property int|null $ulazni_racun
 * @property string|null $datum_primke
 * @property string|null $created_at
 * @property string|null $primio
 * @property int|null $unos
 *
 * @property User $unos0
 */
class Primka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'primka';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ulazni_racun', 'unos'], 'default', 'value' => null],
            [['ulazni_racun', 'unos'], 'integer'],
            [['datum_primke', 'created_at'], 'safe'],
            [['broj_primke'], 'string', 'max' => 250],
            [['primio'], 'string', 'max' => 150],
            [['unos'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['unos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'broj_primke' => Yii::t('app', 'Broj Primke'),
            'ulazni_racun' => Yii::t('app', 'Ulazni Racun'),
            'datum_primke' => Yii::t('app', 'Datum Primke'),
            'created_at' => Yii::t('app', 'Created At'),
            'primio' => Yii::t('app', 'Primio'),
            'unos' => Yii::t('app', 'Unos'),
        ];
    }

    /**
     * Gets query for [[Unos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnos0()
    {
        return $this->hasOne(User::className(), ['id' => 'unos']);
    }
}
