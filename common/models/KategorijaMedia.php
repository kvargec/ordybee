<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kategorija_media".
 *
 * @property int $id
 * @property string|null $kategorija
 * @property int|null $parent
 * @property int|null $status
 *
 * @property StatusOpce $status0
 */
class KategorijaMedia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kategorija_media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent', 'status'], 'default', 'value' => null],
            [['parent', 'status'], 'integer'],
            [['kategorija'], 'string', 'max' => 255],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusOpce::className(), 'targetAttribute' => ['status' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kategorija' => Yii::t('app', 'Kategorija'),
            'parent' => Yii::t('app', 'Parent'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[Status0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusOpce::className(), ['id' => 'status']);
    }
}
