<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "document_vrsta".
 *
 * @property int $id
 * @property string|null $vrsta
 * @property string|null $opis
 *
 * @property Document[] $documents
 */
class DocumentVrsta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document_vrsta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vrsta', 'opis'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vrsta' => Yii::t('app', 'Vrsta'),
            'opis' => Yii::t('app', 'Opis'),
        ];
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['vrsta_dokumenta' => 'id']);
    }
}
