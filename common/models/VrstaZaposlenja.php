<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vrsta_zaposlenja".
 *
 * @property int $id
 * @property string|null $naziv
 *
 * @property Zaposlenje[] $zaposlenjes
 */
class VrstaZaposlenja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrsta_zaposlenja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
        ];
    }

    /**
     * Gets query for [[Zaposlenjes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenjes()
    {
        return $this->hasMany(Zaposlenje::className(), ['vrsta' => 'id']);
    }
}
