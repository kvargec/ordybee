<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stavke_narudjba".
 *
 * @property int $id
 * @property int|null $ulazni_racun
 * @property int|null $proizvod
 * @property float|null $kolicina
 *
 * @property Proizvod $proizvod0
 * @property UlazniRacun $ulazniRacun
 */
class StavkeNarudjba extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stavke_narudjba';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ulazni_racun', 'proizvod'], 'default', 'value' => null],
            [['ulazni_racun', 'proizvod'], 'integer'],
            [['kolicina'], 'number'],
            [['proizvod'], 'exist', 'skipOnError' => true, 'targetClass' => Proizvod::className(), 'targetAttribute' => ['proizvod' => 'id']],
            [['ulazni_racun'], 'exist', 'skipOnError' => true, 'targetClass' => UlazniRacun::className(), 'targetAttribute' => ['ulazni_racun' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ulazni_racun' => Yii::t('app', 'Ulazni Racun'),
            'proizvod' => Yii::t('app', 'Proizvod'),
            'kolicina' => Yii::t('app', 'Kolicina'),
        ];
    }

    /**
     * Gets query for [[Proizvod0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProizvod0()
    {
        return $this->hasOne(Proizvod::className(), ['id' => 'proizvod']);
    }

    /**
     * Gets query for [[UlazniRacun]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUlazniRacun()
    {
        return $this->hasOne(UlazniRacun::className(), ['id' => 'ulazni_racun']);
    }
}
