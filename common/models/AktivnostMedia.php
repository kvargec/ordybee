<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "aktivnost_media".
 *
 * @property int $id
 * @property int|null $aktivnost
 * @property int|null $media
 * @property int|null $redoslijed
 * @property int|null $status
 * @property string|null $extra_settings
 *
 * @property Aktivnosti $aktivnost0
 * @property Media $media0
 * @property StatusOpce $status0
 */
class AktivnostMedia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aktivnost_media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aktivnost', 'media', 'redoslijed', 'status'], 'default', 'value' => null],
            [['aktivnost', 'media', 'redoslijed', 'status'], 'integer'],
            [['extra_settings'], 'safe'],
            [['aktivnost'], 'exist', 'skipOnError' => true, 'targetClass' => Aktivnosti::className(), 'targetAttribute' => ['aktivnost' => 'id']],
            [['media'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['media' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusOpce::className(), 'targetAttribute' => ['status' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'aktivnost' => Yii::t('app', 'Aktivnost'),
            'media' => Yii::t('app', 'Media'),
            'redoslijed' => Yii::t('app', 'Redoslijed'),
            'status' => Yii::t('app', 'Status'),
            'extra_settings' => Yii::t('app', 'Extra Settings'),
        ];
    }

    /**
     * Gets query for [[Aktivnost0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAktivnost0()
    {
        return $this->hasOne(Aktivnosti::className(), ['id' => 'aktivnost']);
    }

    /**
     * Gets query for [[Media0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMedia0()
    {
        return $this->hasOne(Media::className(), ['id' => 'media']);
    }

    /**
     * Gets query for [[Status0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusOpce::className(), ['id' => 'status']);
    }
}
