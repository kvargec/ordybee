<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "icd10classification".
 *
 * @property int $id
 * @property int $set_id
 * @property string|null $code
 * @property string|null $name
 * @property string|null $name_latin
 * @property string|null $comment
 * @property int|null $sex
 * @property int|null $sex_reject
 * @property int|null $age_min
 * @property int|null $age_max
 * @property int|null $rare_disease
 * @property int|null $age_reject
 * @property int|null $excluding
 * @property int|null $including
 * @property int|null $bg_drug_count
 * @property int|null $cod999
 *
 * @property Icd10set $set
 */
class Icd10classification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'icd10classification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['set_id'], 'required'],
            [['set_id', 'sex', 'sex_reject', 'age_min', 'age_max', 'rare_disease', 'age_reject', 'excluding', 'including', 'bg_drug_count', 'cod999'], 'default', 'value' => null],
            [['set_id', 'sex', 'sex_reject', 'age_min', 'age_max', 'rare_disease', 'age_reject', 'excluding', 'including', 'bg_drug_count', 'cod999'], 'integer'],
            [['name', 'name_latin', 'comment'], 'string'],
            [['code'], 'string', 'max' => 255],
            [['set_id'], 'exist', 'skipOnError' => true, 'targetClass' => Icd10set::className(), 'targetAttribute' => ['set_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'set_id' => 'Set ID',
            'code' => 'Code',
            'name' => 'Name',
            'name_latin' => 'Name Latin',
            'comment' => 'Comment',
            'sex' => 'Sex',
            'sex_reject' => 'Sex Reject',
            'age_min' => 'Age Min',
            'age_max' => 'Age Max',
            'rare_disease' => 'Rare Disease',
            'age_reject' => 'Age Reject',
            'excluding' => 'Excluding',
            'including' => 'Including',
            'bg_drug_count' => 'Bg Drug Count',
            'cod999' => 'Cod999',
        ];
    }

    /**
     * Gets query for [[Set]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSet()
    {
        return $this->hasOne(Icd10set::className(), ['id' => 'set_id']);
    }
}
