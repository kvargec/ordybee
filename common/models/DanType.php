<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dan_type".
 *
 * @property int $id
 * @property string|null $vrsta
 *
 * @property Calendar[] $calendars
 */
class DanType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dan_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vrsta'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vrsta' => 'Vrsta',
        ];
    }

    /**
     * Gets query for [[Calendars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCalendars()
    {
        return $this->hasMany(Calendar::className(), ['vrsta' => 'id']);
    }
}
