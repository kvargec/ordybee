<?php

namespace common\models;

use common\helpers\Utils;
use frontend\components\validators\OibValidator;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "dijete".
 *
 * @property int $id
 * @property string $ime
 * @property string $prezime
 * @property string|null $spol
 * @property string|null $dat_rod
 * @property string|null $dat_ispisa
 * @property string|null $dat_upisa
 * @property string|null $oib
 * @property string|null $adresa
 * @property string|null $mjesto
 * @property string|null $mjesto_rod
 * @property string|null $prebivaliste
 * @property string|null $sestra
 * @property string|null $drzavljanstvo
 * * @property string|null $cetvrt
 * @property string|null $cekanje
 * @property int|null $god_cekanja
 * @property string|null $razvoj
 * @property int|null $vrsta_programa
 * @property array|null $teskoce
 * @property string|null $dijagnostika
 * @property string|null $druge_potrebe
 * @property string|null $posebne
 * @property string|null $create_at
 * @property string|null $update_at
 * @property int $status
 * @property string|null $adresa_prebivalista
 * @property string|null $prebivaliste_jednako_boraviste
 *
 * @property VrstaPrograma $vrstaPrograma
 * @property Status $status0
 * @property Roditelj[] $roditeljs
 * @property Zahtjev[] $zahtjevs
 * @property Prisutnost $prisutnost
 */
class Dijete extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dijete';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //  [['ime', 'prezime', 'adresa', 'mjesto','drzavljanstvo','spol','mjesto_rod','oib','dat_rod'], 'required'],
            [['create_at', 'update_at'], 'safe'],
            [['god_cekanja', 'dat_ispisa', 'dat_upisa'], 'default', 'value' => null],
            [['dat_rod'], 'date', 'format' => Yii::$app->formatter->dateFormat],
            [['dat_ispisa', 'dat_upisa'], 'date'],
            [['vrsta_programa', 'status'], 'integer'],
            [['ime', 'prezime', 'adresa', 'mjesto', 'cetvrt', 'drzavljanstvo', 'adresa_prebivalista'], 'string', 'max' => 150],
            [['spol', 'sestra', 'cekanje', 'prebivaliste_jednako_boraviste'], 'string', 'max' => 1],
            [['oib', 'god_cekanja'], 'string', 'max' => 13],
            ['oib', OibValidator::className()],
            [['prebivaliste', 'dijagnostika', 'druge_potrebe', 'posebne', 'mjesto_rod'], 'string', 'max' => 200],
            [['razvoj'], 'string', 'max' => 50],
            // ['teskoce', 'each', 'rule' => ['string']],
            [['vrsta_programa'], 'exist', 'skipOnError' => true, 'targetClass' => VrstaPrograma::className(), 'targetAttribute' => ['vrsta_programa' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusDjeteta::className(), 'targetAttribute' => ['status' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['dat_rod'],
                    self::EVENT_BEFORE_UPDATE => ['dat_rod'],
                ],
                'value' => function ($event) {
                    if (!$this->dat_rod) {
                        return $this->dat_rod;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->dateFormat, 4);
                    return date_format(date_create_from_format($format, $this->dat_rod), 'Y-m-d');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['dat_upisa'],
                    self::EVENT_BEFORE_UPDATE => ['dat_upisa'],
                ],
                'value' => function ($event) {
                    if (!$this->dat_upisa) {
                        return $this->dat_upisa;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->dateFormat, 4);
                    return date_format(date_create_from_format($format, $this->dat_upisa), 'Y-m-d');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['dat_ispisa'],
                    self::EVENT_BEFORE_UPDATE => ['dat_ispisa'],
                ],
                'value' => function ($event) {
                    if (!$this->dat_ispisa) {
                        return $this->dat_ispisa;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->dateFormat, 4);
                    return date_format(date_create_from_format($format, $this->dat_ispisa), 'Y-m-d');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_UPDATE => ['create_at'],
                ],
                'value' => function ($event) {
                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    $date = date_create_from_format($format, $this->create_at);
                    if (!$date) {
                        // fallback, if method 'save' was called twice on same model
                        return $this->create_at;
                    }

                    return date_format($date, 'Y-m-d H:i:s');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['update_at', 'create_at'],
                    self::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->dat_rod = Yii::$app->formatter->asDate($this->dat_rod);
        $this->dat_upisa = Yii::$app->formatter->asDate($this->dat_upisa);
        $this->dat_ispisa = Yii::$app->formatter->asDate($this->dat_ispisa);
        $this->create_at = Yii::$app->formatter->asDateTime($this->create_at);
        $this->update_at = Yii::$app->formatter->asDateTime($this->update_at);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ime' => Yii::t('app', 'Ime'),
            'prezime' => Yii::t('app', 'Prezime'),
            'spol' => Yii::t('app', 'Spol'),
            'dat_rod' => Yii::t('app', 'Dat Rod'),
            'oib' => Yii::t('app', 'Oib'),
            'adresa' => Yii::t('app', 'Adresa'),
            'mjesto' => Yii::t('app', 'Mjesto'),
            'mjesto_rod' => Yii::t('app', 'Mjesto rođenja'),
            'prebivaliste' => Yii::t('app', 'Prebivaliste'),
            'sestra' => Yii::t('app', 'Sestra'),
            'drzavljanstvo' => Yii::t('app', 'Državljanstvo'),
            'cekanje' => Yii::t('app', 'Cekanje'),
            'god_cekanja' => Yii::t('app', 'God Cekanja'),
            'razvoj' => Yii::t('app', 'Razvoj'),
            'vrsta_programa' => Yii::t('app', 'Vrsta Programa'),
            'teskoce' => Yii::t('app', 'Teskoce'),
            'dijagnostika' => Yii::t('app', 'Dijagnostika'),
            'druge_potrebe' => Yii::t('app', 'Druge Potrebe'),
            'posebne' => Yii::t('app', 'Posebne'),
            'create_at' => Yii::t('app', 'Create At'),
            'update_at' => Yii::t('app', 'Update At'),
            'dat_upisa' => Yii::t('app', 'Datum upisa'),
            'dat_ispisa' => Yii::t('app', 'Datum ispisa'),
            'status' => Yii::t('app', 'Status'),
            'adresa_prebivalista' => Yii::t('app', 'Adresa prebivališta'),
        ];
    }

    /**
     * Gets query for [[VrstaPrograma]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVrstaPrograma()
    {
        return $this->hasOne(VrstaPrograma::className(), ['id' => 'vrsta_programa']);
    }
    public function getStatus0()
    {
        return $this->hasOne(StatusDjeteta::className(), ['id' => 'status']);
    }

    /**
     * Gets query for [[Roditeljs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoditeljs()
    {
        return $this->hasMany(Roditelj::className(), ['dijete' => 'id']);
    }

    /**
     * Gets query for [[Zahtjevs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZahtjevs()
    {
        return $this->hasMany(Zahtjev::className(), ['dijete' => 'id']);
    }
    public function getFullName()
    {
        return $this->prezime . ' ' . $this->ime;
    }
    public function getPrisutnost()
    {
        return $this->hasMany(Prisutnost::className(), ['entitet_id' => 'id'])->onCondition(['entitet' => 'dijete']);
    }
    public function getPrisutnost2($id,$datum)
    {
        $temp=Prisutnost::find()->where(['datum' => $datum,'entitet' => 'dijete','entitet_id'=>$id])->one();
        return $temp;
    }
    public function brojZapisa($tipdnevnika)
    {
        $zapisi = VrstaZapis::findAll(['grupa' => $tipdnevnika]);
        $zapisi_ids = [];
        foreach ($zapisi as $item) {
            $zapisi_ids[] = $item->id;
        }
        $dijete_zapisi = DijeteZapis::find()->joinWith(['zapis0'])->where(['dnevnik_zapis.vrsta' => $zapisi_ids, 'dijete' => $this->id])->all();
        return $dijete_zapisi;
    }
    public function getBrojMjIzostanaka()
    {
        $date_start = date('Y-m-d', strtotime('first day of this month', time()));
        $date_end = date('Y-m-d', strtotime('last day of this month', time()));
        return $this->hasMany(Prisutnost::class, ['entitet_id' => 'id'])->onCondition(['entitet' => 'dijete'])->andWhere(['status' => 0])->andWhere(['between', 'datum', $date_start, $date_end])->count();
    }
    public function getIntervaliIzostanaka()
    {
        $pedGodina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $start_date = new \DateTime(date('Y-m-d', strtotime($pedGodina->od)));
        $end_date = new \DateTime(date('Y-m-d', strtotime($pedGodina->do)));
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($start_date, $interval, $end_date);

        $return = Utils::calculateDays($period, 'dijete', $this->id, 0);

        return $return;
    }
    public function getPrisutnostMjesec($mjesec)
    {
        $godina = date('Y', time());
        return Prisutnost::find()->where(['entitet' => 'dijete', 'entitet_id' => $this->id, 'status' => 1])->andWhere(['extract(MONTH FROM datum)' => $mjesec, 'extract(YEAR FROM datum)' => $godina])->count();
    }
    public function getSkupina0()
    {
        $pedGodina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        return $this->hasOne(Skupina::className(), ['id' => 'skupina'])->viaTable('dijete_skupina', ['dijete' => 'id'])->andWhere(['skupina.ped_godina'=> $pedGodina->id]);
    }
    public function getCovidPotvrda()
    {
        $covid_vrsta = VrstaZapis::find()->where(['naziv' => 'covid_potvrda'])->one();
        $covid_zapis = DijeteZapis::find()->joinWith(['zapis0'])->where(['dnevnik_zapis.vrsta' => $covid_vrsta->id, 'dijete' => $this->id])->one();
        $potvrda = $covid_zapis ? 'DA' : 'NE';
        return $potvrda;
    }
    public function getVrstadepdrop()
    {
        if (!empty($this->vrsta_programa)) {
            return $this->vrsta_programa;
        } else {
            return null;
        }
    }
    public function setVrstadepdrop($value)
    {
        $this->_vrsta_depdrop = $value;
    }
}
