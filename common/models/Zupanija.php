<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zupanija".
 *
 * @property int $id
 * @property string|null $naziv
 *
 * @property Mjesto[] $mjestos
 */
class Zupanija extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zupanija';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
        ];
    }

    /**
     * Gets query for [[Mjestos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMjestos()
    {
        return $this->hasMany(Mjesto::className(), ['zupanija' => 'id']);
    }
}
