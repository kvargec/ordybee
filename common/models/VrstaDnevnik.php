<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "vrsta_dnevnik".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $postavke
 * @property int|null $status
 *
 * @property Dnevnik[] $dnevniks
 */
class VrstaDnevnik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrsta_dnevnik';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['postavke'], 'safe'],
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['naziv'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'postavke' => Yii::t('app', 'Postavke'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[Dnevniks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnevniks()
    {
        return $this->hasMany(Dnevnik::className(), ['vrsta_dnevnika' => 'id']);
    }

    public function getDnevnikZapisFormAttribs() {
        if(!$this->postavke) {
            return null;
        }

        $postavke = json_decode ($this->postavke, true);

        if(!array_key_exists ('form', $postavke)) {
            return null;
        }

        $form = $postavke['form'];

        $formAttribs = [];
        foreach($form as $k => $v) {
            $type = ArrayHelper::getValue($v, 'type');
            $options = ArrayHelper::getValue($v, 'options', []);
            if($type == 'date') {
                $type = 'widget';
                $widgetClass = '\\kartik\\widgets\\DatePicker';
                $options['pluginOptions'] = ['format'=>'dd-M-yyyy'];
                $formAttribs[$k] = ['label'=>$k, 'type'=>$type, 'widgetClass'=>$widgetClass, 'options'=>$options];
            } else {
                $formAttribs[$k] = ['label'=>$k, 'type'=>$type, 'options'=>$options];
            }
        }

        return $formAttribs;
    }
}
