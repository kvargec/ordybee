<?php

namespace common\models;

use Yii;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "upitnik".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $opis
 * @property int|null $ped_godina
 * @property string|null $date_publish
 * @property string|null $date_end
 * @property string|null $created_at
 * @property int|null $creator
 * @property int|null $status
 * @property string|null $upitnik_for
 * @property bool|null $is_required
 *
 * @property Pitanje[] $pitanjes
 * @property PedagoskaGodina $pedGodina
 * @property StatusOpce $status0
 * @property User $creator0
 */
class Upitnik extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'upitnik';
    }
    public function behaviors()
    {
        return [

            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_UPDATE => ['created_at'],
                ],
                'value' => function ($event) {
                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    $date = date_create_from_format($format, $this->created_at);
                    if(!$date) {
                        // fallback, if method 'save' was called twice on same model
                        return $this->created_at;
                    }

                    return date_format($date, 'Y-m-d H:i:s');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => [ 'created_at']
                ],
                'value' => date('Y-m-d H:i:s'),
            ],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['opis'], 'string'],
            [['ped_godina', 'creator', 'status'], 'default', 'value' => null],
            [['ped_godina', 'creator', 'status'], 'integer'],
            [['date_publish', 'date_end', 'created_at', 'upitnik_for'], 'safe'],
            [['is_required'], 'boolean'],
            [['naziv'], 'string', 'max' => 250],
            [['ped_godina'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['ped_godina' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusOpce::className(), 'targetAttribute' => ['status' => 'id']],
            [['creator'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'opis' => Yii::t('app', 'Opis'),
            'ped_godina' => Yii::t('app', 'Ped Godina'),
            'date_publish' => Yii::t('app', 'Date Publish'),
            'date_end' => Yii::t('app', 'Date End'),
            'created_at' => Yii::t('app', 'Created At'),
            'creator' => Yii::t('app', 'Creator'),
            'status' => Yii::t('app', 'Status'),
            'upitnik_for' => Yii::t('app', 'Upitnik For'),
            'is_required' => Yii::t('app', 'Is Required'),
        ];
    }

    /**
     * Gets query for [[Pitanjes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPitanjes()
    {
        return $this->hasMany(Pitanje::className(), ['upitnik' => 'id']);
    }

    /**
     * Gets query for [[PedGodina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'ped_godina']);
    }

    /**
     * Gets query for [[Status0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusOpce::className(), ['id' => 'status']);
    }

    /**
     * Gets query for [[Creator0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreator0()
    {
        return $this->hasOne(User::className(), ['id' => 'creator']);
    }
}
