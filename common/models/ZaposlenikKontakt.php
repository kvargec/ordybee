<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zaposlenik_kontakt".
 *
 * @property int $id
 * @property int|null $zaposlenik
 * @property int|null $kontakt
 *
 * @property Kontakt $kontakt0
 * @property Zaposlenik $zaposlenik0
 */
class ZaposlenikKontakt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zaposlenik_kontakt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zaposlenik', 'kontakt'], 'default', 'value' => null],
            [['zaposlenik', 'kontakt'], 'integer'],
            [['kontakt'], 'exist', 'skipOnError' => true, 'targetClass' => Kontakt::className(), 'targetAttribute' => ['kontakt' => 'id']],
            [['zaposlenik'], 'exist', 'skipOnError' => true, 'targetClass' => Zaposlenik::className(), 'targetAttribute' => ['zaposlenik' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'zaposlenik' => Yii::t('app', 'Zaposlenik'),
            'kontakt' => Yii::t('app', 'Kontakt'),
        ];
    }

    /**
     * Gets query for [[Kontakt0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKontakt0()
    {
        return $this->hasOne(Kontakt::className(), ['id' => 'kontakt']);
    }

    /**
     * Gets query for [[Zaposlenik0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenik0()
    {
        return $this->hasOne(Zaposlenik::className(), ['id' => 'zaposlenik']);
    }
}
