<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "icd10chapter".
 *
 * @property int $id
 * @property string|null $codes
 * @property string|null $text
 *
 * @property Icd10set[] $icd10sets
 */
class Icd10chapter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'icd10chapter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['codes'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codes' => 'Codes',
            'text' => 'Text',
        ];
    }

    /**
     * Gets query for [[Icd10sets]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIcd10sets()
    {
        return $this->hasMany(Icd10set::className(), ['chapter_id' => 'id']);
    }
}
