<?php

namespace common\models;

use Yii;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "mailbox".
 *
 * @property int $id
 * @property int|null $user
 * @property int|null $message
 * @property string|null $created_at
 * @property string|null $last_change_at
 * @property int|null $mstatus
 * @property int|null $folder
 *
 * @property MsgFolders $folder0
 * @property Mstatus $mstatus0
 * @property Poruke $message0
 * @property User $user0
 */
class Mailbox extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mailbox';
    }
    public function behaviors()
    {
        return [

            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_UPDATE => ['created_at'],
                ],
                'value' => function ($event) {
                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    $date = date_create_from_format($format, $this->created_at);
                    if(!$date) {
                        // fallback, if method 'save' was called twice on same model
                        return $this->created_at;
                    }

                    return date_format($date, 'Y-m-d H:i:s');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['last_change_at', 'created_at'],
                    self::EVENT_BEFORE_UPDATE => ['last_change_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user', 'message', 'mstatus', 'folder'], 'default', 'value' => null],
            [['user', 'message', 'mstatus', 'folder'], 'integer'],
            [['created_at', 'last_change_at'], 'safe'],
            [['folder'], 'exist', 'skipOnError' => true, 'targetClass' => MsgFolders::className(), 'targetAttribute' => ['folder' => 'id']],
            [['mstatus'], 'exist', 'skipOnError' => true, 'targetClass' => Mstatus::className(), 'targetAttribute' => ['mstatus' => 'id']],
            [['message'], 'exist', 'skipOnError' => true, 'targetClass' => Poruke::className(), 'targetAttribute' => ['message' => 'id']],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user' => Yii::t('app', 'User'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
            'last_change_at' => Yii::t('app', 'Last Change At'),
            'mstatus' => Yii::t('app', 'Mstatus'),
            'folder' => Yii::t('app', 'Folder'),
        ];
    }

    /**
     * Gets query for [[Folder0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFolder0()
    {
        return $this->hasOne(MsgFolders::className(), ['id' => 'folder']);
    }

    /**
     * Gets query for [[Mstatus0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMstatus0()
    {
        return $this->hasOne(Mstatus::className(), ['id' => 'mstatus']);
    }

    /**
     * Gets query for [[Message0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMessage0()
    {
        return $this->hasOne(Poruke::className(), ['id' => 'message']);
    }

    /**
     * Gets query for [[User0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }
}
