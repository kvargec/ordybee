<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dodatak_sadrzaj".
 *
 * @property int $id
 * @property string|null $dodatak
 * @property string|null $datum_pocetak
 * @property string|null $datum_kraj
 * @property int|null $tip
 *
 * @property TipDodatak $tip0
 */
class DodatakSadrzaj extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dodatak_sadrzaj';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['datum_pocetak', 'datum_kraj'], 'safe'],
            [['tip'], 'default', 'value' => null],
            [['tip'], 'integer'],
            [['dodatak'], 'string', 'max' => 255],
            [['tip'], 'exist', 'skipOnError' => true, 'targetClass' => TipDodatak::className(), 'targetAttribute' => ['tip' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dodatak' => 'Dodatak',
            'datum_pocetak' => 'Datum Pocetak',
            'datum_kraj' => 'Datum Kraj',
            'tip' => 'Tip',
        ];
    }

    /**
     * Gets query for [[Tip0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTip0()
    {
        return $this->hasOne(TipDodatak::className(), ['id' => 'tip']);
    }
}
