<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "status_opce".
 *
 * @property int $id
 * @property string|null $naziv
 *
 * @property DnevnikZapis[] $dnevnikZapis
 */
class StatusOpce extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_opce';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
        ];
    }

    /**
     * Gets query for [[DnevnikZapis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnevnikZapis()
    {
        return $this->hasMany(DnevnikZapis::className(), ['status' => 'id']);
    }
}
