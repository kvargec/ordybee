<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "zaposlenje".
 *
 * @property int $id
 * @property string|null $dat_pocetka
 * @property string|null $date_kraj
 * @property float|null $koeficijent
 * @property int|null $zaposlenik
 * @property int|null $vrsta
 * @property int|null $r_mjesto
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property RadnoMjesto $rMjesto
 * @property VrstaZaposlenja $vrsta0
 * @property Zaposlenik $zaposlenik0
 */
class Zaposlenje extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zaposlenje';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dat_pocetka', 'date_kraj', 'created_at', 'updated_at'], 'safe'],
            [['koeficijent'], 'number'],
            [['zaposlenik', 'vrsta', 'r_mjesto'], 'default', 'value' => null],
            [['zaposlenik', 'vrsta', 'r_mjesto'], 'integer'],
            [['r_mjesto'], 'exist', 'skipOnError' => true, 'targetClass' => RadnoMjesto::className(), 'targetAttribute' => ['r_mjesto' => 'id']],
            [['vrsta'], 'exist', 'skipOnError' => true, 'targetClass' => VrstaZaposlenja::className(), 'targetAttribute' => ['vrsta' => 'id']],
            [['zaposlenik'], 'exist', 'skipOnError' => true, 'targetClass' => Zaposlenik::className(), 'targetAttribute' => ['zaposlenik' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dat_pocetka' => Yii::t('app', 'Datum početka'),
            'date_kraj' => Yii::t('app', 'Datum završetka'),
            'koeficijent' => Yii::t('app', 'Koeficijent'),
            'zaposlenik' => Yii::t('app', 'Zaposlenik'),
            'vrsta' => Yii::t('app', 'Vrsta Zaposlenja'),
            'r_mjesto' => Yii::t('app', 'Radno Mjesto'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[RMjesto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRMjesto()
    {
        return $this->hasOne(RadnoMjesto::className(), ['id' => 'r_mjesto']);
    }

    /**
     * Gets query for [[Vrsta0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVrsta0()
    {
        return $this->hasOne(VrstaZaposlenja::className(), ['id' => 'vrsta']);
    }

    /**
     * Gets query for [[Zaposlenik0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenik0()
    {
        return $this->hasOne(Zaposlenik::className(), ['id' => 'zaposlenik']);
    }
}
