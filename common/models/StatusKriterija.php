<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "status_kriterija".
 *
 * @property int $id
 * @property string $status
 *
 * @property Kriterij[] $kriterijs
 */
class StatusKriterija extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_kriterija';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'string', 'max' => 128],
            [['status'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[Kriterijs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKriterijs()
    {
        return $this->hasMany(Kriterij::className(), ['status_kriterija_id' => 'id']);
    }
}
