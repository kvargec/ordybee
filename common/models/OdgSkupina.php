<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "odg_skupina".
 *
 * @property int $id
 * @property int|null $odgajatelj
 * @property int|null $skupina
 * @property string|null $date_from
 * @property string|null $date_to
 * @property int|null $status
 *
 * @property Skupina $skupina0
 * @property StatusOpce $status0
 * @property Zaposlenik $odgajatelj0
 */
class OdgSkupina extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'odg_skupina';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['odgajatelj', 'skupina', 'status'], 'default', 'value' => null],
            [['odgajatelj', 'skupina', 'status'], 'integer'],
            [['date_from', 'date_to'], 'safe'],
            [['skupina'], 'exist', 'skipOnError' => true, 'targetClass' => Skupina::className(), 'targetAttribute' => ['skupina' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusOpce::className(), 'targetAttribute' => ['status' => 'id']],
            [['odgajatelj'], 'exist', 'skipOnError' => true, 'targetClass' => Zaposlenik::className(), 'targetAttribute' => ['odgajatelj' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'odgajatelj' => Yii::t('app', 'Odgajatelj'),
            'skupina' => Yii::t('app', 'Skupina'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[Skupina0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSkupina0()
    {
        return $this->hasOne(Skupina::className(), ['id' => 'skupina']);
    }

    /**
     * Gets query for [[Status0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusOpce::className(), ['id' => 'status']);
    }

    /**
     * Gets query for [[Odgajatelj0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOdgajatelj0()
    {
        return $this->hasOne(Zaposlenik::className(), ['id' => 'odgajatelj']);
    }
}
