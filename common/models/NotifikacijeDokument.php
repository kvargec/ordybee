<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notifikacije_dokument".
 *
 * @property int $id
 * @property int|null $notifikacija
 * @property int|null $dokument
 *
 * @property Document $dokument0
 * @property Notifikacije $notifikacija0
 */
class NotifikacijeDokument extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifikacije_dokument';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notifikacija', 'dokument'], 'default', 'value' => null],
            [['notifikacija', 'dokument'], 'integer'],
            [['dokument'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['dokument' => 'id']],
            [['notifikacija'], 'exist', 'skipOnError' => true, 'targetClass' => Notifikacije::className(), 'targetAttribute' => ['notifikacija' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notifikacija' => 'Notifikacija',
            'dokument' => 'Dokument',
        ];
    }

    /**
     * Gets query for [[Dokument0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDokument0()
    {
        return $this->hasOne(Document::className(), ['id' => 'dokument']);
    }

    /**
     * Gets query for [[Notifikacija0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNotifikacija0()
    {
        return $this->hasOne(Notifikacije::className(), ['id' => 'notifikacija']);
    }
}
