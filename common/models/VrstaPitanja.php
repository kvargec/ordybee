<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vrsta_pitanja".
 *
 * @property int $id
 * @property string|null $vrsta_pitanje
 * @property string|null $opis
 * @property string|null $tip
 *
 * @property Pitanje[] $pitanjes
 */
class VrstaPitanja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrsta_pitanja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['opis'], 'string'],
            [['vrsta_pitanje', 'tip'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vrsta_pitanje' => Yii::t('app', 'Vrsta Pitanje'),
            'opis' => Yii::t('app', 'Opis'),
            'tip' => Yii::t('app', 'Tip'),
        ];
    }

    /**
     * Gets query for [[Pitanjes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPitanjes()
    {
        return $this->hasMany(Pitanje::className(), ['vrsta_pitanja' => 'id']);
    }
}
