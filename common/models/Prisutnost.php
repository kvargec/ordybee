<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prisutnost".
 *
 * @property int $id
 * @property string|null $datum
 * @property float|null $trajanje
 * @property string|null $entitet
 * @property int|null $entitet_id
 * @property string|null $razlog_izostanka
 * @property int|null $status
 * @property $pocetak
 * @property $kraj
 */
class Prisutnost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prisutnost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['datum'], 'safe'],
            [['trajanje'], 'number'],
            [['entitet_id', 'status'], 'default', 'value' => null],
            [['entitet_id', 'status'], 'integer'],
            [['entitet'], 'string', 'max' => 100],
            [['razlog_izostanka'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datum' => 'Datum',
            'trajanje' => 'Trajanje',
            'entitet' => 'Entitet',
            'entitet_id' => 'Entitet ID',
            'razlog_izostanka' => 'Razlog Izostanka',
            'status' => 'Status',
        ];
    }
}
