<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vrste_obroka".
 *
 * @property int $id
 * @property string $vrsta
 * @property float|null $cijena
 *
 * @property ZaposlenikObrok[] $zaposlenikObroks
 */
class VrsteObroka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrste_obroka';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vrsta'], 'required'],
            [['cijena'], 'number'],
            [['vrsta'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vrsta' => Yii::t('app', 'Vrsta'),
            'cijena' => Yii::t('app', 'Cijena'),
        ];
    }

    /**
     * Gets query for [[ZaposlenikObroks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenikObroks()
    {
        return $this->hasMany(ZaposlenikObrok::className(), ['obrok_id' => 'id']);
    }
}
