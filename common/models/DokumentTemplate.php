<?php

namespace common\models;

use common\helpers\Utils;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "dokument_template".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $template
 * @property string|null $slug
 * @property string|null $sadrzaj
 * @property int|null $ped_godina
 *
 * @property PedagoskaGodina $pedGodina
 */
class DokumentTemplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dokument_template';
    }
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'value'=>function(){
                    return Utils::url_slug($this->naziv);
                },
                'ensureUnique'=>true
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sadrzaj'], 'string'],
            [['ped_godina'], 'default', 'value' => null],
            [['ped_godina'], 'integer'],
            [['naziv', 'slug'], 'string', 'max' => 250],
            [['template'], 'string', 'max' => 150],
            [['template'], 'default', 'value' => 'memorandum'],
            [['ped_godina'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['ped_godina' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // prilikom spremanja zapisa atribut pedagoska godina se postavlja na trenutno aktivnu godinu
        //$this->ped_godina = PedagoskaGodina::getAktivna()->id;

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'template' => Yii::t('app', 'Template'),
            'slug' => Yii::t('app', 'Slug'),
            'sadrzaj' => Yii::t('app', 'Sadrzaj'),
            'ped_godina' => Yii::t('app', 'Ped Godina'),
        ];
    }

    /**
     * Gets query for [[PedGodina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'ped_godina']);
    }
}
