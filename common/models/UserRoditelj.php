<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_roditelj".
 *
 * @property int $id
 * @property int|null $user
 * @property int|null $roditelj
 * @property string|null $username
 * @property string|null $password
 *
 * @property Roditelj $roditelj0
 * @property User $user0
 */
class UserRoditelj extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_roditelj';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user', 'roditelj'], 'default', 'value' => null],
            [['user', 'roditelj'], 'integer'],
            [['username', 'password'], 'string', 'max' => 150],
            [['roditelj'], 'exist', 'skipOnError' => true, 'targetClass' => Roditelj::className(), 'targetAttribute' => ['roditelj' => 'id']],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user' => Yii::t('app', 'User'),
            'roditelj' => Yii::t('app', 'Roditelj'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * Gets query for [[Roditelj0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoditelj0()
    {
        return $this->hasOne(Roditelj::className(), ['id' => 'roditelj']);
    }

    /**
     * Gets query for [[User0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }
}
