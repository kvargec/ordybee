<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "odgovori".
 *
 * @property int $id
 * @property int|null $pitanje
 * @property int|null $user
 * @property string|null $value
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Pitanje $pitanje0
 * @property User $user0
 */
class Odgovori extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'odgovori';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pitanje', 'user'], 'default', 'value' => null],
            [['pitanje', 'user'], 'integer'],
            [['value', 'created_at', 'updated_at'], 'safe'],
            [['pitanje'], 'exist', 'skipOnError' => true, 'targetClass' => Pitanje::className(), 'targetAttribute' => ['pitanje' => 'id']],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pitanje' => Yii::t('app', 'Pitanje'),
            'user' => Yii::t('app', 'User'),
            'value' => Yii::t('app', 'Value'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Pitanje0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPitanje0()
    {
        return $this->hasOne(Pitanje::className(), ['id' => 'pitanje']);
    }

    /**
     * Gets query for [[User0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }
}
