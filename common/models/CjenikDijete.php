<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cjenik_dijete".
 *
 * @property int $id
 * @property int $dijete
 * @property float|null $cijena
 * @property string|null $dat_poc
 * @property string|null $dat_kraj
 *
 * @property Dijete $dijete0
 */
class CjenikDijete extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cjenik_dijete';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dijete'], 'required'],
            [['dijete'], 'default', 'value' => null],
            [['dijete'], 'integer'],
            [['cijena'], 'number'],
            [['dat_poc', 'dat_kraj'], 'safe'],
            [['dijete'], 'exist', 'skipOnError' => true, 'targetClass' => Dijete::className(), 'targetAttribute' => ['dijete' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dijete' => Yii::t('app', 'Dijete'),
            'cijena' => Yii::t('app', 'Cijena'),
            'dat_poc' => Yii::t('app', 'Dat Poc'),
            'dat_kraj' => Yii::t('app', 'Dat Kraj'),
        ];
    }

    /**
     * Gets query for [[Dijete0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDijete0()
    {
        return $this->hasOne(Dijete::className(), ['id' => 'dijete']);
    }
}
