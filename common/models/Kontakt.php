<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kontakt".
 *
 * @property int $id
 * @property int|null $vrsta
 * @property string|null $vrijednost
 *
 * @property VrsteKontakt $vrsta0
 * @property ZaposlenikKontakt[] $zaposlenikKontakts
 */
class Kontakt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kontakt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vrsta'], 'default', 'value' => null],
            [['vrsta'], 'integer'],
            [['vrijednost'], 'string', 'max' => 50],
            [['vrsta'], 'exist', 'skipOnError' => true, 'targetClass' => VrsteKontakt::className(), 'targetAttribute' => ['vrsta' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vrsta' => Yii::t('app', 'Vrsta'),
            'vrijednost' => Yii::t('app', 'Vrijednost'),
        ];
    }

    /**
     * Gets query for [[Vrsta0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVrsta0()
    {
        return $this->hasOne(VrsteKontakt::className(), ['id' => 'vrsta']);
    }

    /**
     * Gets query for [[ZaposlenikKontakts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenikKontakts()
    {
        return $this->hasMany(ZaposlenikKontakt::className(), ['kontakt' => 'id']);
    }
}
