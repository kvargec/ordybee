<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vrsta_zapis".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $grupa
 *
 * @property DnevnikZapis[] $dnevnikZapis
 */
class VrstaZapis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrsta_zapis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv','grupa'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'grupa' => Yii::t('app', 'Grupa'),
        ];
    }

    /**
     * Gets query for [[DnevnikZapis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDnevnikZapis()
    {
        return $this->hasMany(DnevnikZapis::className(), ['vrsta' => 'id']);
    }
}
