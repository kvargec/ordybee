<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "radno_mjesto".
 *
 * @property int $id
 * @property string|null $naziv
 * @property int|null $sprema
 * @property float|null $koeficijent
 * @property string|null $sifra
 *
 * @property Sprema $sprema0
 * @property Zaposlenje[] $zaposlenjes
 */
class RadnoMjesto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'radno_mjesto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sprema'], 'default', 'value' => null],
            [['sprema'], 'integer'],
            [['koeficijent'], 'number'],
            [['naziv'], 'string', 'max' => 100],
            [['sifra'], 'string', 'max' => 20],
            [['sprema'], 'exist', 'skipOnError' => true, 'targetClass' => Sprema::className(), 'targetAttribute' => ['sprema' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'sprema' => Yii::t('app', 'Sprema'),
            'koeficijent' => Yii::t('app', 'Koeficijent'),
            'sifra' => Yii::t('app', 'Sifra'),
        ];
    }

    /**
     * Gets query for [[Sprema0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSprema0()
    {
        return $this->hasOne(Sprema::className(), ['id' => 'sprema']);
    }

    /**
     * Gets query for [[Zaposlenjes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenjes()
    {
        return $this->hasMany(Zaposlenje::className(), ['r_mjesto' => 'id']);
    }
}
