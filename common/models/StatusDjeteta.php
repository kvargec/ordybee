<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "status_djeteta".
 *
 * @property int $id
 * @property string|null $status
 *
 * @property Dijete[] $dijetes
 */
class StatusDjeteta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_djeteta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[Dijetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDijetes()
    {
        return $this->hasMany(Dijete::className(), ['status' => 'id']);
    }
}
