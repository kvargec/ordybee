<?php

namespace common\models;

use app\models\VrstaObitelji;
use common\helpers\Utils;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "zahtjev".
 *
 * @property int $id
 * @property string|null $urbroj
 * @property string|null $klasa
 * @property string|null $create_at
 * @property string|null $update_at
 * @property string|null $zeljeni_datum
 * @property string $napomena
 * @property int|null $user
 * @property int|null $dijete
 * @property int|null $status
 * @property int|null $pd_godina
 * @property bool|null $ocijenjen
 * @property array|null $predani_obavezni_dokumenti
 * @property int $vrsta_obitelji
 * @property int|null $lokacija
 * @property bool|null $posjeduje_pravo
 * @property string|null $pravo_pocetak
 * @property string|null $pravo_kraj
 * @property string|null $pravo_naziv


 *
 * @property DocumentZahtjev[] $documentZahtjevs
 * @property Dijete $dijete0
 * @property Status $status0
 * @property PedagoskaGodina $pdGodina
 * @property User $user0
 * @property ZahtjevKriterij[] $zahtjevKriterijs
 * @property Kriterij[] $kriterijs
 * @property \common\models\VrstaObitelji $vrsta_obitelji0
 */
class Zahtjev extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_UPDATE => ['create_at'],
                ],
                'value' => function ($event) {
                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    $date = date_create_from_format($format, $this->create_at);
                    if (!$date) {
                        // fallback, if method 'save' was called twice on same model
                        return $this->create_at;
                    }

                    return date_format($date, 'Y-m-d H:i:s');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['update_at'],
                    self::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
                'value' => function ($event) {
                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    $date = date_create_from_format($format, $this->update_at);
                    if (!$date) {
                        // fallback, if method 'save' was called twice on same model
                        return $this->update_at;
                    }

                    return date_format($date, 'Y-m-d H:i:s');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_VALIDATE => ['pravo_pocetak'],
                    self::EVENT_BEFORE_INSERT => ['pravo_pocetak'],
                    self::EVENT_BEFORE_UPDATE => ['pravo_pocetak'],
                ],
                'value' => function ($event) {

                    if (!$this->pravo_pocetak) {

                        return $this->pravo_pocetak;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    //var_dump(date_create_from_format(Yii::$app->formatter->datetimeFormat, $this->vrijeme));
                    //die();
                    $kaj = new \DateTime($this->pravo_pocetak);

                    return date_format($kaj, 'Y-m-d H:i');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_VALIDATE => ['pravo_kraj'],
                    self::EVENT_BEFORE_INSERT => ['pravo_kraj'],
                    self::EVENT_BEFORE_UPDATE => ['pravo_kraj'],
                ],
                'value' => function ($event) {

                    if (!$this->pravo_kraj) {

                        return $this->pravo_kraj;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    //var_dump(date_create_from_format(Yii::$app->formatter->datetimeFormat, $this->vrijeme));
                    //die();
                    $kaj = new \DateTime($this->pravo_kraj);

                    return date_format($kaj, 'Y-m-d H:i');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_VALIDATE => ['zeljeni_datum'],
                    self::EVENT_BEFORE_INSERT => ['zeljeni_datum'],
                    self::EVENT_BEFORE_UPDATE => ['zeljeni_datum'],
                ],
                'value' => function ($event) {

                    if (!$this->zeljeni_datum) {

                        return $this->zeljeni_datum;
                    }

                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    //var_dump(date_create_from_format(Yii::$app->formatter->datetimeFormat, $this->vrijeme));
                    //die();
                    $kaj = new \DateTime($this->zeljeni_datum);

                    return date_format($kaj, 'Y-m-d H:i');
                },
            ],

        ];
    }
    public static function tableName()
    {
        return 'zahtjev';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_at', 'update_at', 'predani_obavezni_dokumenti', 'pravo_pocetak', 'pravo_kraj', 'zeljeni_datum'], 'safe'],
            [['user', 'dijete', 'status', 'vrsta_obitelji', 'lokacija'], 'default', 'value' => null],
            [['user', 'dijete', 'status', 'vrsta_obitelji', 'lokacija'], 'integer'],
            [['napomena', 'pravo_naziv'], 'string'],
            //     [['zeljeni_datum','vrsta_obitelji'], 'required'],
            [['ocijenjen', 'posjeduje_pravo'], 'boolean'],
            [['urbroj', 'klasa'], 'string', 'max' => 50],
            [['dijete'], 'exist', 'skipOnError' => true, 'targetClass' => Dijete::className(), 'targetAttribute' => ['dijete' => 'id']],
            [['vrsta_obitelji'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\VrstaObitelji::className(), 'targetAttribute' => ['vrsta_obitelji' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status' => 'id']],
            [['pd_godina'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['pd_godina' => 'id']],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user' => 'id']],
            [['lokacija'], 'exist', 'skipOnError' => true, 'targetClass' => Objekt::className(), 'targetAttribute' => ['lokacija' => 'id']],

        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->create_at = Yii::$app->formatter->asDateTime($this->create_at);
        $this->update_at = Yii::$app->formatter->asDateTime($this->update_at);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'urbroj' => Yii::t('app', 'Urbroj'),
            'create_at' => Yii::t('app', 'Izrađen'),
            'update_at' => Yii::t('app', 'Ažuriran'),
            'user' => Yii::t('app', 'User'),
            'dijete' => Yii::t('app', 'Dijete'),
            'status' => Yii::t('app', 'Status'),
            'napomena' => Yii::t('app', 'Napomena'),
            'pd_godina' => Yii::t('app', 'Ped. godina'),
            'klasa' => Yii::t('app', 'Klasa'),
            'zeljeni_datum' => Yii::t('app', 'Željeni datum polaska'),
            'ocijenjen' => Yii::t('app', 'Ocijenjen'),
            'predani_obavezni_dokumenti' => Yii::t('app', 'Predani Obavezni Dokumenti'),
        ];
    }

    /**
     * Gets query for [[DocumentZahtjevs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentZahtjevs()
    {
        return $this->hasMany(DocumentZahtjev::className(), ['zahtjev' => 'id']);
    }

    /**
     * Gets query for [[Dijete0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDijete0()
    {
        return $this->hasOne(Dijete::className(), ['id' => 'dijete']);
    }

    /**
     * Gets query for [[Status0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
    public function getPdGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'pd_godina']);
    }
    /**
     * Gets query for [[User0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }

    /**
     * Gets query for [[ZahtjevKriterijs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZahtjevKriterijs()
    {
        return $this->hasMany(ZahtjevKriterij::className(), ['zahtjev_id' => 'id']);
    }

    /**
     * Gets query for [[Kriterijs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKriterijs()
    {
        return $this->hasMany(Kriterij::className(), ['id' => 'kriterij_id'])->viaTable('zahtjev_kriterij', ['zahtjev_id' => 'id']);
    }

    public function getBodovi()
    {
        if (!$this->ocijenjen) {
            return null;
        }

        $aktivniKriterijIds = array_map(function ($row) {
            return $row->id;
        }, Kriterij::getAktivniKriteriji());
        $condition = ['zahtjev_id' => $this->id, ['in', 'kriterij_id', $aktivniKriterijIds]];
        $condition = ['zahtjev_id' => $this->id];
        $zadovoljeniKriteriji = \common\models\ZahtjevKriterij::find()->where($condition)->all();
        $sum = 0;
        foreach ($zadovoljeniKriteriji as $zadovoljeniKriterij) {
            if (!$zadovoljeniKriterij->zadovoljen) {
                continue;
            }

            if ($zadovoljeniKriterij->kriterij->izravan_upis) {
                return PHP_INT_MAX;
            }

            $multiplyBy = 1;
            if (VrstaUnosa::findOne($zadovoljeniKriterij->kriterij->vrsta_unosa_id)->vrsta == 'select') {
                $multiplyBy = $zadovoljeniKriterij->zadovoljene_opcije;
            }

            $sum += $multiplyBy * $zadovoljeniKriterij->kriterij->bodovi;
        }

        return $sum;
    }

    public function getIzravanUpis()
    {
        if (!$this->ocijenjen) {
            return null;
        }

        $aktivniKriterijIds = array_map(function ($row) {
            return $row->id;
        }, Kriterij::getAktivniKriteriji());
        $condition = ['zahtjev_id' => $this->id, ['in', 'kriterij_id', $aktivniKriterijIds]];
        $condition = ['zahtjev_id' => $this->id];
        $zadovoljeniKriteriji = \common\models\ZahtjevKriterij::find()->where($condition)->all();
        foreach ($zadovoljeniKriteriji as $zadovoljeniKriterij) {
            if (!$zadovoljeniKriterij->zadovoljen) {
                continue;
            }

            if ($zadovoljeniKriterij->kriterij->izravan_upis) {
                return true;
            }
        }

        return false;
    }

    public function getDijeteDatRod()
    {
        return $this->dijete0->dat_rod;
    }

    public function getDirPath()
    {

        return Yii::getAlias("@frontend/web/upload/zahtjevi/$this->id");
    }
    public function getVrsta_obitelji0()
    {
        return $this->hasOne(\common\models\VrstaObitelji::class, ['id' => 'vrsta_obitelji']);
    }
    public function getObjekt()
    {
        return $this->hasOne(Objekt::class, ['id' => 'lokacija']);
    }
}
