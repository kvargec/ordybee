<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ulazni_racun".
 *
 * @property int $id
 * @property int|null $dobavljac
 * @property string|null $naslov
 * @property string|null $napomena
 * @property string|null $datum_racuna
 * @property string|null $created_at
 * @property string|null $preuzeo
 * @property float|null $iznos
 *
 * @property StavkeNarudjba[] $stavkeNarudjbas
 * @property Tvrtka $dobavljac0
 */
class UlazniRacun extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ulazni_racun';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dobavljac'], 'default', 'value' => null],
            [['dobavljac'], 'integer'],
            [['napomena'], 'string'],
            [['datum_racuna', 'created_at'], 'safe'],
            [['iznos'], 'number'],
            [['naslov', 'preuzeo'], 'string', 'max' => 250],
            [['dobavljac'], 'exist', 'skipOnError' => true, 'targetClass' => Tvrtka::className(), 'targetAttribute' => ['dobavljac' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dobavljac' => Yii::t('app', 'Dobavljac'),
            'naslov' => Yii::t('app', 'Naslov'),
            'napomena' => Yii::t('app', 'Napomena'),
            'datum_racuna' => Yii::t('app', 'Datum Racuna'),
            'created_at' => Yii::t('app', 'Created At'),
            'preuzeo' => Yii::t('app', 'Preuzeo'),
            'iznos' => Yii::t('app', 'Iznos'),
        ];
    }

    /**
     * Gets query for [[StavkeNarudjbas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStavkeNarudjbas()
    {
        return $this->hasMany(StavkeNarudjba::className(), ['ulazni_racun' => 'id']);
    }

    /**
     * Gets query for [[Dobavljac0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDobavljac0()
    {
        return $this->hasOne(Tvrtka::className(), ['id' => 'dobavljac']);
    }
}
