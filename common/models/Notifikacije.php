<?php

namespace common\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "notifikacije".
 *
 * @property int $id
 * @property int|null $user
 * @property string|null $akcija
 * @property int|null $akcija_id
 * @property string|null $radnja
 * @property string|null $created_at
 * @property string|null $read_at
 * @property string|null $sadrzaj
 * @property int|null $status
 * @property string $subject
 * @property integer $sender_id
 * @property boolean $is_deleted
 * @property boolean $user_trash
 * @property boolean $user_delete
 * @property boolean $sender_trash
 * @property boolean $sender_delete
 *
 * @property User $user0
 */
class Notifikacije extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                ],
                'value' => function ($event) {
                    return date('Y-m-d H:i:s');
                }
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifikacije';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sender_id', 'sadrzaj', 'subject'], 'required'],
            [['user', 'akcija_id', 'status'], 'default', 'value' => null],
            [['user', 'akcija_id', 'status', 'sender_id'], 'integer'],
            [['created_at', 'read_at'], 'safe'],
            [['sadrzaj'], 'string'],
            [['akcija', 'subject'], 'string', 'max' => 100],
            [['radnja'], 'string', 'max' => 250],
            [['is_deleted', 'user_delete', 'user_trash', 'sender_trash', 'sender_delete'], 'boolean'],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user' => Yii::t('app', 'User'),
            'akcija' => Yii::t('app', 'Akcija'),
            'akcija_id' => Yii::t('app', 'Akcija ID'),
            'radnja' => Yii::t('app', 'Radnja'),
            'created_at' => Yii::t('app', 'Created At'),
            'read_at' => Yii::t('app', 'Read At'),
            'sadrzaj' => Yii::t('app', 'Sadrzaj'),
            'status' => Yii::t('app', 'Status'),
            'is_deleted' => Yii::t('app', 'Obrisano'),
            'subject' => Yii::t('app', 'Naslov'),
            'sender_id' => Yii::t('app', 'Pošiljatelj')
        ];
    }

    /**
     * Gets query for [[User0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user']);
    }
    public function getNotifikacija2document()
    {
        return $this->hasMany(NotifikacijeDokument::className(), ['notifikacija' => 'id']);
    }
    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['id' => 'dokument'])->via('notifikacija2document');
    }
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }
    public function uploadfirst($dir, $filename)
    {
        $this->attachments = UploadedFile::getInstances($this, 'attachments');

        if ($this->validate()) {
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $realAttachments = [];
            $date = date_create();

            foreach ($this->attachments as $file) {
                $fileName = preg_replace("/\s+/", "_", $file->baseName);
                $realAttachments[] = '/files/' . $date->format("Y") . '/' . $date->format("m") . '/' . $fileName . '-' . time() . '.' . $file->extension;
                //var_dump($this->getAttachDir() . $file->baseName . '.' . $file->extension);
                $file->saveAs($this->getAttachDir() . $fileName . '-' . time() . '.' . $file->extension);
            }
            $this->attachments = ['attachments' => $realAttachments];
            return true;
        } else {
            return false;
        }
    }
    public function uploadother($dir, $filename)
    {
        $this->attachments = UploadedFile::getInstances($this, 'attachments');

        if ($this->validate()) {
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $realAttachments = [];
            $date = date_create();

            foreach ($this->attachments as $file) {
                $fileName = preg_replace("/\s+/", "_", $file->baseName);
                $realAttachments[] = '/files/' . $date->format("Y") . '/' . $date->format("m") . '/' . $fileName . '-' . time() . '.' . $file->extension;
                //var_dump($this->getAttachDir() . $file->baseName . '.' . $file->extension);
                //$file->saveAs($this->getAttachDir() . $file->baseName . '.' . $file->extension);
            }
            $this->attachments = ['attachments' => $realAttachments];
            return true;
        } else {
            return false;
        }
    }

    public function getAttachDir()
    {
        $date = date_create();
        return Yii::getAlias('@backend') . '/web/files/' . $date->format("Y") . '/' . $date->format("m") . '/';
    }
}
