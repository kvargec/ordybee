<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "skupina".
 *
 * @property int $id
 * @property string $naziv
 * @property string|null $sifra
 * @property string|null $slika
 * @property int|null $ped_godina
 *
 * @property PedagoskaGodina $pedGodina
 * @property SkupinaObjekt[] $skupinaObjekts
 */
class Skupina extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'skupina';
    }

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'required'],
            [['ped_godina'], 'default', 'value' => null],
            [['ped_godina'], 'integer'],
            [['naziv'], 'string', 'max' => 150],
            [['sifra'], 'string', 'max' => 20],
            [['slika'], 'string', 'max' => 255],
            [['ped_godina'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['ped_godina' => 'id']],
            // [['imageFile'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'sifra' => Yii::t('app', 'Sifra'),
            'slika' => Yii::t('app', 'Slika'),
            'ped_godina' => Yii::t('app', 'Ped Godina'),
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        // prilikom spremanja zapisa atribut pedagoska godina se postavlja na trenutno aktivnu godinu
        //$this->ped_godina = PedagoskaGodina::getAktivna()->id;

        $imageFile = UploadedFile::getInstance($this, 'imageFile');
        if ($imageFile) {
            $directory = self::getImageDir();
            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }
            $fileName = $imageFile->baseName . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;
            if ($imageFile->saveAs($filePath)) {
                $this->slika = $fileName;
            }
        }

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->imageFile = $this->slika;
    }

    public static function getImageDir()
    {
        return Yii::getAlias('@frontend/web/upload/');
    }

    public function getImagePath()
    {
        return $this->getImageDir() . $this->slika;
    }

    public function getImageUrl()
    {
        return Yii::$app->urlManagerFrontend->baseUrl . "/upload/$this->slika";
    }

    /**
     * Gets query for [[PedGodina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'ped_godina']);
    }

    /**
     * Gets query for [[SkupinaObjekts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSkupinaObjekts()
    {
        return $this->hasMany(SkupinaObjekt::className(), ['skupina' => 'id']);
    }
    /**
     * Gets query for [[ZaposelniciSkupine]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposleniciSkupine()
    {
        return $this->hasMany(ZaposlenikSkupina::className(), ['skupina' => 'id']);
    }

    public function getObjekts()
    {
        return $this->hasMany(Objekt::className(), ['id' => 'objekt'])->via('skupinaObjekts');
    }

    public function getZaposleniks()
    {
        return $this->hasMany(Zaposlenik::className(), ['id' => 'zaposlenik'])->via('zaposleniciSkupine');
    }
}
