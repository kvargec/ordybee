<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zaposlenik_skupina".
 *
 * @property int $id
 * @property int|null $zaposlenik
 * @property int|null $skupina
 * @property int|null $ped_godina
 * @property bool|null $aktivno
 *
 * @property PedagoskaGodina $pedGodina
 * @property Skupina $skupina0
 * @property Zaposlenik $zaposlenik0
 */
class ZaposlenikSkupina extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zaposlenik_skupina';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zaposlenik', 'skupina', 'ped_godina'], 'default', 'value' => null],
            [['zaposlenik', 'skupina', 'ped_godina'], 'integer'],
            [['aktivno'], 'boolean'],
            [['ped_godina'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['ped_godina' => 'id']],
            [['skupina'], 'exist', 'skipOnError' => true, 'targetClass' => Skupina::className(), 'targetAttribute' => ['skupina' => 'id']],
            [['zaposlenik'], 'exist', 'skipOnError' => true, 'targetClass' => Zaposlenik::className(), 'targetAttribute' => ['zaposlenik' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'zaposlenik' => Yii::t('app', 'Zaposlenik'),
            'skupina' => Yii::t('app', 'Skupina'),
            'ped_godina' => Yii::t('app', 'Ped Godina'),
            'aktivno' => Yii::t('app', 'Aktivno'),
        ];
    }

    /**
     * Gets query for [[PedGodina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'ped_godina']);
    }

    /**
     * Gets query for [[Skupina0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSkupina0()
    {
        return $this->hasOne(Skupina::className(), ['id' => 'skupina']);
    }

    /**
     * Gets query for [[Zaposlenik0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposlenik0()
    {
        return $this->hasOne(Zaposlenik::className(), ['id' => 'zaposlenik']);
    }
}
