<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kriterij".
 *
 * @property int $id
 * @property int $pedagoska_godina_id
 * @property string $naziv
 * @property int $vrsta_kriterija_id
 * @property int|null $bodovi
 * @property bool|null $izravan_upis
 * @property int|null $redoslijed
 * @property int $status_kriterija_id
 * @property int $vrsta_unosa_id 
 * @property array|null $opcije 
 *
 * @property PedagoskaGodina $pedagoskaGodina
 * @property StatusKriterija $statusKriterija
 * @property VrstaKriterija $vrstaKriterija
 * @property VrstaUnosa $vrstaUnosa 
 * @property ZahtjevKriterij[] $zahtjevKriterijs
 * @property Zahtjev[] $zahtjevs
 */
class Kriterij extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kriterij';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pedagoska_godina_id', 'naziv', 'vrsta_kriterija_id', 'status_kriterija_id', 'vrsta_unosa_id'], 'required'],
            [['pedagoska_godina_id', 'vrsta_kriterija_id', 'bodovi', 'redoslijed', 'status_kriterija_id', 'vrsta_unosa_id'], 'default', 'value' => null],
            [['pedagoska_godina_id', 'vrsta_kriterija_id', 'bodovi', 'redoslijed', 'status_kriterija_id', 'vrsta_unosa_id'], 'integer'],
            [['izravan_upis'], 'boolean'],
            [['opcije'], 'safe'],
            [['naziv'], 'string', 'max' => 256],
            [['pedagoska_godina_id'], 'exist', 'skipOnError' => true, 'targetClass' => PedagoskaGodina::className(), 'targetAttribute' => ['pedagoska_godina_id' => 'id']],
            [['status_kriterija_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusKriterija::className(), 'targetAttribute' => ['status_kriterija_id' => 'id']],
            [['vrsta_kriterija_id'], 'exist', 'skipOnError' => true, 'targetClass' => VrstaKriterija::className(), 'targetAttribute' => ['vrsta_kriterija_id' => 'id']],
            [['vrsta_unosa_id'], 'exist', 'skipOnError' => true, 'targetClass' => VrstaUnosa::className(), 'targetAttribute' => ['vrsta_unosa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pedagoska_godina_id' => Yii::t('app', 'Pedagoska Godina ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'vrsta_kriterija_id' => Yii::t('app', 'Vrsta Kriterija ID'),
            'bodovi' => Yii::t('app', 'Bodovi'),
            'izravan_upis' => Yii::t('app', 'Izravan Upis'),
            'redoslijed' => Yii::t('app', 'Redoslijed'),
            'status_kriterija_id' => Yii::t('app', 'Status Kriterija ID'),
        ];
    }

    /**
     * Gets query for [[PedagoskaGodina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedagoskaGodina()
    {
        return $this->hasOne(PedagoskaGodina::className(), ['id' => 'pedagoska_godina_id']);
    }

    /**
     * Gets query for [[StatusKriterija]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatusKriterija()
    {
        return $this->hasOne(StatusKriterija::className(), ['id' => 'status_kriterija_id']);
    }

    /**
     * Gets query for [[VrstaKriterija]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVrstaKriterija()
    {
        return $this->hasOne(VrstaKriterija::className(), ['id' => 'vrsta_kriterija_id']);
    }

    /**
     * Gets query for [[ZahtjevKriterijs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZahtjevKriterijs()
    {
        return $this->hasMany(ZahtjevKriterij::className(), ['kriterij_id' => 'id']);
    }

    public static function getAktivniKriteriji()
    {
        $aktivnaGodina = PedagoskaGodina::findOne(['aktivna' => true]);
        $statusAktivan = StatusKriterija::findOne(['status' => 'aktivan']);
        $condition = ['pedagoska_godina_id' => $aktivnaGodina->id, 'status_kriterija_id' => $statusAktivan->id];
        return Kriterij::find()->where($condition)->orderBy([
            'redoslijed' => SORT_ASC
        ])->all();
    }
}
