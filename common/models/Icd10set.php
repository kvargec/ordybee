<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "icd10set".
 *
 * @property int $id
 * @property int $chapter_id
 * @property string|null $codes
 * @property string|null $name
 *
 * @property Icd10classification[] $icd10classifications
 * @property Icd10chapter $chapter
 */
class Icd10set extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'icd10set';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chapter_id'], 'required'],
            [['chapter_id'], 'default', 'value' => null],
            [['chapter_id'], 'integer'],
            [['name'], 'string'],
            [['codes'], 'string', 'max' => 255],
            [['chapter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Icd10chapter::className(), 'targetAttribute' => ['chapter_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chapter_id' => 'Chapter ID',
            'codes' => 'Codes',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Icd10classifications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIcd10classifications()
    {
        return $this->hasMany(Icd10classification::className(), ['set_id' => 'id']);
    }

    /**
     * Gets query for [[Chapter]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChapter()
    {
        return $this->hasOne(Icd10chapter::className(), ['id' => 'chapter_id']);
    }
}
