<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vrsta_programa".
 *
 * @property int $id
 * @property string|null $vrsta
 *
 * @property Dijete[] $dijetes
 */
class VrstaPrograma extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vrsta_programa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vrsta'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vrsta' => Yii::t('app', 'Vrsta'),
        ];
    }

    /**
     * Gets query for [[Dijetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDijetes()
    {
        return $this->hasMany(Dijete::className(), ['vrsta_programa' => 'id']);
    }
}
