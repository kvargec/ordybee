<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "document_zahtjev".
 *
 * @property int $id
 * @property int|null $document
 * @property int|null $zahtjev
 *
 * @property Document $document0
 * @property Zahtjev $zahtjev0
 */
class DocumentZahtjev extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document_zahtjev';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['document', 'zahtjev'], 'default', 'value' => null],
            [['document', 'zahtjev'], 'integer'],
            [['document'], 'exist', 'skipOnError' => true, 'targetClass' => Document::className(), 'targetAttribute' => ['document' => 'id']],
            [['zahtjev'], 'exist', 'skipOnError' => true, 'targetClass' => Zahtjev::className(), 'targetAttribute' => ['zahtjev' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'document' => Yii::t('app', 'Document'),
            'zahtjev' => Yii::t('app', 'Zahtjev'),
        ];
    }

    /**
     * Gets query for [[Document0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocument0()
    {
        return $this->hasOne(Document::className(), ['id' => 'document']);
    }

    /**
     * Gets query for [[Zahtjev0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZahtjev0()
    {
        return $this->hasOne(Zahtjev::className(), ['id' => 'zahtjev']);
    }
}
