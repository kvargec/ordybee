<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mjed".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $kratica
 *
 * @property Proizvod[] $proizvods
 */
class Mjed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mjed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'string', 'max' => 150],
            [['kratica'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'kratica' => Yii::t('app', 'Kratica'),
        ];
    }

    /**
     * Gets query for [[Proizvods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProizvods()
    {
        return $this->hasMany(Proizvod::className(), ['mjed' => 'id']);
    }
}
