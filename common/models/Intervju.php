<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "intervju".
 *
 * @property int $id
 * @property int|null $zahtjev
 * @property string|null $vrijeme
 * @property string|null $lokacija
 * @property string|null $napomena
 * @property bool|null $odrzan
 *
 * @property Zahtjev $zahtjev0
 */
class Intervju extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'intervju';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zahtjev'], 'default', 'value' => null],
            [['zahtjev'], 'integer'],
            [['vrijeme'], 'safe'],
            [['napomena'], 'string'],
            [['odrzan'], 'boolean'],
            [['lokacija'], 'string', 'max' => 150],
            [['zahtjev'], 'exist', 'skipOnError' => true, 'targetClass' => Zahtjev::className(), 'targetAttribute' => ['zahtjev' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'zahtjev' => Yii::t('app', 'Zahtjev'),
            'vrijeme' => Yii::t('app', 'Vrijeme'),
            'lokacija' => Yii::t('app', 'Lokacija'),
            'napomena' => Yii::t('app', 'Napomena'),
            'odrzan' => Yii::t('app', 'Odrzan'),
        ];
    }

    /**
     * Gets query for [[Zahtjev0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZahtjev0()
    {
        return $this->hasOne(Zahtjev::className(), ['id' => 'zahtjev']);
    }
}
