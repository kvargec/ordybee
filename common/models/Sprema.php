<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sprema".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $kratica
 *
 * @property RadnoMjesto[] $radnoMjestos
 * @property Zaposlenik[] $zaposleniks
 */
class Sprema extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sprema';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv'], 'string', 'max' => 100],
            [['kratica'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'kratica' => Yii::t('app', 'Kratica'),
        ];
    }

    /**
     * Gets query for [[RadnoMjestos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRadnoMjestos()
    {
        return $this->hasMany(RadnoMjesto::className(), ['sprema' => 'id']);
    }

    /**
     * Gets query for [[Zaposleniks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZaposleniks()
    {
        return $this->hasMany(Zaposlenik::className(), ['sprema' => 'id']);
    }
}
