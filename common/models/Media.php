<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\UploadedFile;
use \yii\helpers\Url;

/**
 * This is the model class for table "media".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $opis
 * @property int|null $media_type
 * @property int|null $size
 * @property string|null $filename
 * @property string|null $created_at
 * @property string|null $author
 * @property int|null $user_id
 * @property array|null $recipients
 * @property array|null $attachments

 *
 * @property MediaType $mediaType
 * @property User $user
 */
class Media extends \yii\db\ActiveRecord
{
    public $_grupacija;
    public $_ccgrupacija;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media';
    }
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_UPDATE => ['created_at'],
                ],
                'value' => function ($event) {
                    // removing prefix 'php:' (see common\config\main.php)
                    $format = substr(Yii::$app->formatter->datetimeFormat, 4);
                    $date = date_create_from_format($format, $this->created_at);
                    if (!$date) {
                        // fallback, if method 'save' was called twice on same model
                        return $this->created_at;
                    }

                    return date_format($date, 'Y-m-d H:i:s');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }
    public function afterFind()
    {
        parent::afterFind();
        $this->created_at = Yii::$app->formatter->asDateTime($this->created_at);
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['naziv', 'media_type', 'kategorija'], 'required'],
            [['opis'], 'string'],
            [['media_type', 'size', 'user_id'], 'default', 'value' => null],
            [['media_type', 'size', 'user_id'], 'integer'],
            [['created_at', 'recipients'], 'safe'],
            [['naziv'], 'string', 'max' => 100],
            [['filename', 'author'], 'string', 'max' => 255],
            [['media_type'], 'exist', 'skipOnError' => true, 'targetClass' => MediaType::className(), 'targetAttribute' => ['media_type' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'opis' => Yii::t('app', 'Opis'),
            'media_type' => Yii::t('app', 'Media Type'),
            'size' => Yii::t('app', 'Size'),
            'filename' => Yii::t('app', 'Filename'),
            'created_at' => Yii::t('app', 'Created At'),
            'author' => Yii::t('app', 'Author'),
            'user_id' => Yii::t('app', 'User ID'),
            'recipients' => Yii::t('app', 'Recipients'),

        ];
    }
    /**
     * Gets query for [[MediaType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMediaType()
    {
        return $this->hasOne(MediaType::className(), ['id' => 'media_type']);
    }

    /**
     * Gets query for [[MediaRecipients]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMediaRecipients()
    {
        return $this->hasMany(MediaRecipients::className(), ['media' => 'id']);
    }
    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getGrupacija()
    {
        if (!empty($this->recipients)) {
            return key($this->recipients);
        } else {
            return null;
        }
    }
    public function setGrupacija($value)
    {
        $this->_grupacija = $value;
    }
    public function getCcGrupacija()
    {
        if (!empty($this->cc_recipients)) {
            return key($this->cc_recipients);
        } else {
            return null;
        }
    }
    public function setCcGrupacija($value)
    {
        $this->_ccgrupacija = $value;
    }
    public function getRecipients()
    {
        if (!empty($this->recipients)) {
            $recipients = $this->recipients;
            return $recipients[key($recipients)];
        } else
            return [];
    }
    public function getCcRecipients()
    {
        if (!empty($this->cc_recipients)) {
            $cc_recipients = $this->cc_recipients;
            return $cc_recipients[key($cc_recipients)];
        } else
            return [];
    }
    public function getMediaPaths()
    {
        $return = array();
        $weburl = Postavke::find()->where(['postavka' => 'backendWeb'])->one();
        if (isset($this->attachments)) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $url = $weburl->vrijednost . '/media/' . explode('media/', $attachment)[0];
                $return[] = $url;
            }
        }
        return $return;
    }

    public function getFilePathsBackend()
    {
        $return = array();
        if (isset($this->attachments)) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $url = Url::to('@web/media/') . explode('media/', $attachment)[0];
                $return[] = $url;
            }
        }
        return $return;
    }

    //public function getFilePaths()
    //{
    //    $return = array();
    //    foreach ($this->attachments['attachments'] as $attachment) {
    //        $url = Yii::getAlias('@frontendUrl') . '/admin/media/' . explode('media/', $attachment)[0];
    //        $return[] = $url;
    //    }
    //    return $return;
    //}
    public function getFilePaths($id)
    {
        $preview = [];
        $dir = Url::to('@web/media/') . $id;

        if (isset($this->attachments['attachments'])) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $url = $dir . '/' . explode($id . '/', $attachment)[0];
                $preview[] = $url;
            }
        }

        return $preview;
    }

    public function getFilePathsConfig()
    {
        $config = [];
        $count = 0;

        if (isset($this->attachments['attachments'])) {
            foreach ($this->attachments['attachments'] as $attachment) {
                $fileName = $attachment;
                $fileId = $fileName;
                $type = pathinfo($fileName);
                $config[] = [
                    'key' => $fileId,
                    'caption' => $fileName,
                    'url' => Url::to(['/media/delete-file']),
                ];
                if ($type == 'pdf') {
                    $config[] = ['type' => 'pdf'];
                }
                $count++;
            }
        }

        return $config;
    }

    //public function upload()
    //{
    //    if (empty(UploadedFile::getInstances($this, 'attachments'))) {
    //        return true;
    //    }
    //    $this->attachments = UploadedFile::getInstances($this, 'attachments');
    //
    //    if (!file_exists(Yii::getAlias('@backend') . '/web/media')) {
    //        mkdir(Yii::getAlias('@backend') . '/web/media', 0777, true);
    //    }
    //    $dir = Yii::getAlias('@backend') . '/web/media';
    //
    //    $realAttachments = [];
    //    $added_size = 0;
    //    foreach ($this->attachments as $file) {
    //        $fileName = preg_replace("/\s+/", "_", $file->baseName);
    //        $realAttachments[] = $fileName . '.' . $file->extension;
    //        //var_dump($this->getAttachDir() . $file->baseName . '.' . $file->extension);
    //        $file->saveAs($dir . '/' . $fileName . '.' . $file->extension);
    //        $size = filesize($dir . '/' . $fileName . '.' . $file->extension);
    //        $rounded = $size;
    //        $added_size += $rounded;
    //    }
    //    $this->size = round($added_size / 1024 / 1024);
    //    $this->attachments = ['attachments' => $realAttachments];
    //    return true;
    //}
}
