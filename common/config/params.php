<?php
$protocol = isset($_SERVER['HTTPS']) ? "https" : "http" ;

return [
    'adminEmail' => 'info@ordybee.com',
    'supportEmail' => 'info@ordybee.com',
    'senderEmail' => 'info@ordybee.com',
    'senderName' => 'Dv Vrbovec - ordybee',
    'user.passwordResetTokenExpire' => 3600,
    'aplikName'=>'OrdyBee DV Vrbovec',
    'frontendUrl'=>$protocol.'://'.$_SERVER['HTTP_HOST'].'/'
];
