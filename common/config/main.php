<?php
$config = [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@bower-asset' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => 'OrdyBee',
    'sourceLanguage' => 'en-US',
    'language' => 'hr-HR',
    'timeZone'=>'Europe/Zagreb',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'formatter' => [
            'dateFormat' => 'php:d.m.Y.',
            'datetimeFormat' => 'php:d.m.Y. H:i',
            'locale' => 'hr_HR@calendar=gregorian',
            'nullDisplay' => '',
            'defaultTimeZone' => 'Europe/Zagreb',
        ],
        'i18n' => [
            'translations' => [
                /* 'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],*/
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    //'sourceLanguage' => 'en-US',
                    /* 'fileMap' => [
                        'app' => 'app.php',
                    ],*/
                    // 'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation']
                ],
                'user*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    //'sourceLanguage' => 'en-US',

                    'fileMap' => [
                        'app' => 'user.php',
                    ],

                    // 'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation']
                ],
                'usuario*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en',

                    'fileMap' => [
                        'usuario' => 'user.php',
                    ],

                    // 'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation']
                ],
                'calendar*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en-US',
                    'language' => 'hr-HR',
                    'fileMap' => [
                        'app' => 'calendar.php',
                    ],

                    // 'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation']
                ],
            ],
        ],
    ],

    'modules' => [
        'user' => [
            'class' => Da\User\Module::class,
            //'enableUnconfirmedLogin' => true,
            'enableRegistration' => true,
            'mailParams' => [
                'fromEmail' => function () {
                    return [Yii::$app->params['senderEmail'] => 'info@ordybee.com'];
                }
            ],
            'administratorPermissionName' => 'user-management'
        ],
        /*'rbac' => 'dektrium\rbac\RbacWebModule',*/

        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
    ],
];

if (!YII_ENV_TEST) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [ //here
            'crud' => [ // generator name
                // 'class' => 'yii\gii\generators\crud\Generator', // generator class
                'class' => 'common\gii\crud\Generator', // generator class
                'templates' => [ //setting for out templates
                    'kartik' => '@common/gii/crud/default', // template name => path to template
                ]
            ]
        ],
    ];
}

return $config;
