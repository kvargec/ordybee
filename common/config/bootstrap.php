<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
$protocol = isset($_SERVER['HTTPS']) ? "https" : "http" ;
Yii::setAlias('@frontendUrl',$protocol.'://'.$_SERVER['HTTP_HOST'].'');
