<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
$urlAplik=\common\models\Postavke::find()->where(['postavka'=>'urlAplik'])->one();
$podaciVrtic=\common\models\Postavke::find()->where(['postavka'=>'vrticPodaci'])->one();
$nazivVrtica=\common\models\Postavke::find()->where(['postavka'=>'nazivVrtica'])->one();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div>Poštovani/a,<br>
    imate promjene na Vašem zahtjevu <?= $zahtjev->urbroj; ?>.<br>
    Promjene možete vidjeti na : <?php echo $urlAplik->vrijednost ?>/portal pod poveznicom 'Moji zahtjevi'</div>
    <div class="footer">Lijepi pozdrav,<br>
        Vaš
    <?php echo $nazivVrtica->vrijednost ?></div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>