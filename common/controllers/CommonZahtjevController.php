<?php

namespace common\controllers;

use common\models\Status;
use Yii;
use common\models\Zahtjev;
use common\models\Search\ZahtjevSearch;
use common\models\Dijete;
use common\models\Search\DijeteSearch;
use common\models\Roditelj;
use common\models\Search\RoditeljSearch;
use common\models\Document;
use common\models\Search\DocumentSearch;
use common\models\Rodbina;
use common\models\Search\RodbinaSearch;
use common\models\DocumentZahtjev;
use common\models\Search\DocumentZahtjevSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;
use Mpdf;

class CommonZahtjevController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionPosalji($id)
    {
        $userID = Yii::$app->user->identity->id;
        $zahtjev = Zahtjev::find()->where(['id' => $id])->one();
        $status = Status::find()->where(['status' => 'Predan'])->one();
        $zahtjev->status = $status->id;
        $zahtjev->save();
        $userID = Yii::$app->user->identity->id;
        $urlAplik = \common\models\Postavke::find()->where(['postavka' => 'urlAplik'])->one();
        $emailAdmin = \common\models\Postavke::find()->where(['postavka' => 'adminMail'])->one();
        $this->actionIzradipotvrda($id);
        $message = Yii::$app->mailer->compose('mailPotvrda', [
            'user' => Yii::$app->user->identity,
            'zahtjev' => $zahtjev
        ])
            ->setFrom('info@ordybee.com')
            ->setTo(Yii::$app->user->identity->email)
            //->setTo('kvargec@gmail.com')
            ->setSubject("Promjena na $zahtjev->urbroj zahtjevu");
        $message->attach($this->getPdfPathPotvrde() . $id . '.pdf');
        $message->send();
        $message = Yii::$app->mailer->compose('noviZahtjev', [
            'user' => Yii::$app->user->identity,
            'zahtjev' => $zahtjev
        ])
            ->setFrom('info@ordybee.com')
            ->setTo($emailAdmin->vrijednost)
            //->setTo('kvargec@gmail.com')
            ->setSubject("Predan zahtjev $zahtjev->urbroj ");
        //   $message->attach($this->getPdfPathZahtjevi().$id.'.pdf');
        $message->send();
        return $this->redirect(['pdfpotvrda', 'id' => $id]);
    }

    /**
     * Create potvrda pdf for Zahtjev.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPdfpotvrda($id)
    {
        $orient = 'P';
        $model = $this->findModel($id);

        $dijete = Dijete::find()->where(['id' => $model->dijete])->one();

        $dokumenti = DocumentZahtjev::find()->where(['zahtjev' => $model->id])->all();
        $documentIds = [];
        foreach ($dokumenti as $doc) {
            $documentIds[] = $doc->document;
        }

        $dokumenti = Document::find()->where(['id' => $documentIds])->all();

        $pdfPath = $this->getPdfPathPotvrde() . $model->id . '.pdf';

        $content = $this->renderPartial('@common/views/zahtjev/potvrda', [
            'model' => $model,
            'dijete' => $dijete,
            'dokumenti' => $dokumenti
        ]);

        $tempDir = Yii::getAlias('@frontend/web') . "/upload/pdftmp/";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($content);
        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);

        // $this->sendPotvrda($id);

        return $this->redirect(['pdfzahtjev', 'id' => $id]);
    }
    public function actionIzradipotvrda($id)
    {
        $orient = 'P';
        $model = $this->findModel($id);

        $dijete = Dijete::find()->where(['id' => $model->dijete])->one();

        $dokumenti = DocumentZahtjev::find()->where(['zahtjev' => $model->id])->all();
        $documentIds = [];
        foreach ($dokumenti as $doc) {
            $documentIds[] = $doc->document;
        }

        $dokumenti = Document::find()->where(['id' => $documentIds])->all();
        //$dokumenti = $model->predani_obavezni_dokumenti ? $model->predani_obavezni_dokumenti : [];

        $pdfPath = $this->getPdfPathPotvrde() . $model->id . '.pdf';

        $content = $this->renderPartial('@common/views/zahtjev/potvrda', [
            'model' => $model,
            'dijete' => $dijete,
            'dokumenti' => $dokumenti
        ]);

        $tempDir = Yii::getAlias('@frontend/web') . "/upload/pdftmp/";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($content, \Mpdf\HTMLParserMode::DEFAULT_MODE);
        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
    }
    public function actionMojapotvrda($id)
    {
        $pdfPath = Yii::getAlias('@frontend/web') . '/upload/potvrde/' . $id . '.pdf';
        // if(!file_exists($pdfPath)) {
        //     $this->actionIzradipotvrda($id);
        // }
        $this->actionIzradipotvrda($id);
        return Yii::$app->response->sendFile($pdfPath, basename($pdfPath), ['inline' => 'false']);
    }
    /**
     * Create Zahtjev pdf.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPdfzahtjev($id)
    {
        $zahtjevId = (int)$id;
        $orient = 'P';
        $zahtjev = $this->findModel($zahtjevId);

        $userID = Yii::$app->user->identity->id;
        $zahtjev = Zahtjev::find()->where(['id' => $zahtjevId])->one();

        $dijete = Dijete::find()->where(['id' => $zahtjev->dijete])->one();

        $query = Roditelj::find()->where(['dijete' => $zahtjev->dijete])->andWhere('LENGTH(ime)>0');
        $dataProviderRoditelji = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => false
        ]);

        $roditelji = Roditelj::find()->where(['dijete' => $zahtjev->dijete])->andWhere('LENGTH(ime)>0')->all();
        $roditelj1 = null;
        $roditelj2 = null;
        if (!empty($roditelji)) {
            $roditelj1 = $roditelji[0];
            $roditelj2 = isset($roditelji[1]) ? $roditelji[1] : null;
        }

        $rodbina = Rodbina::find()->where(['dijete' => $zahtjev->dijete])->all();
        $siblingIds = [];
        if (!empty($rodbina)) {
            foreach ($rodbina as $sestra) {
                $siblingIds[] = $sestra->sibling;
            }
        }

        $query = Dijete::find()->where(['id' => $siblingIds]);
        $dataProviderSiblings = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => false

        ]);

        $dokumenti = DocumentZahtjev::find()->where(['zahtjev' => $zahtjevId])->all();
        $documentIds = [];
        if (!empty($dokumenti)) {
            foreach ($dokumenti as $doc) {
                $documentIds[] = $doc->document;
            }
        }


        $query = Document::find()->where(['id' => $documentIds]);
        $dataProviderDocuments = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => false

        ]);

        $dokumenti = Document::find()->where(['id' => $documentIds])->all();
        $pdfPath = $this->getPdfPathZahtjevi() . $zahtjev->id . '.pdf';
        //  $this->layout='@common/views/layouts/pdf';
        $content = $this->renderPartial('@common/views/zahtjev/zahtjevpdf', [
            'zahtjev' => $zahtjev,
            'dijete' => $dijete,
            'roditelji' => $dataProviderRoditelji,
            'roditelj1' => $roditelj1,
            'roditelj2' => $roditelj2,
            'siblings' => $dataProviderSiblings,
            'documents' => $dataProviderDocuments,
            'dokumenti' => $dokumenti,
        ]);
        //  $content=$this->renderContent($content);
        $tempDir = Yii::getAlias('@frontend/web') . "/upload/pdftmp/";

        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($content, \Mpdf\HTMLParserMode::DEFAULT_MODE);
        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::DOWNLOAD);
    }

    public function actionPosaljiPotvrdu($id)
    {
        if (!Yii::$app->request->isAjax) {
            return;
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $email = Yii::$app->request->post('email');
        if (!$email) {
            return;
        }
        $napomena = Yii::$app->request->post('napomena');
        if (!$napomena) {
            $napomena = '';
        }

        $this->sendPotvrda($id, $email, $napomena);

        return [
            'message' => 'Potvrda je poslana.',
        ];
    }

    private function sendPotvrda($id, $useremail, $napomena)
    {
        $zahtjevId = (int)$id;
        $orient = 'P';
        $zahtjev = $this->findModel($zahtjevId);
        // $useremail = $zahtjev->user0->email;
        $pdfPath = Yii::getAlias('@frontend/web') . '/upload/potvrde/' . $id . '.pdf';
        // if(!file_exists($pdfPath)) {
        //     $this->actionIzradipotvrda($id, $napomena);
        // }
        $this->actionIzradipotvrda($id);

        $message = Yii::$app->mailer->compose('mailPotvrda2', [
            'url' => Url::toRoute(['mojapotvrda', 'id' => $id], true),
        ])
            ->setFrom('info@dv-vrbovec.hr')
            //->setTo('kvargec@gmail.com')
            ->setTo($useremail)
            ->setSubject("Promjena na $zahtjev->urbroj zahtjevu");
        $message->attach($this->getPdfPathPotvrde() . $id . '.pdf');
        $message->send();
        $podaciVrtic = \common\models\Postavke::find()->where(['postavka' => 'vrticPodaci'])->one();
        $urlAplik = \common\models\Postavke::find()->where(['postavka' => 'urlAplik'])->one();
        $emailAdmin = \common\models\Postavke::find()->where(['postavka' => 'adminMail'])->one();
        $porukaTekst = 'Poštovani,<br />';
        $porukaTekst .= 'dodan je zahtjev: ' . $zahtjev->urbroj;
        $porukaTekst .= '.<br /> Provjerite na <a href="' . $urlAplik->vrijednost . '">[LINK]</a>';
        $poruka = Yii::$app->mailer->compose()
            ->setTo($emailAdmin->vrijednost)
            ->setFrom('info@ordybee.com')
            ->setSubject('Novi zahtjev za upis predan')
            // ->setTextBody($porukaTekst)
            ->setHtmlBody($porukaTekst);


        return $poruka->send();

        return;
    }

    private function sendZahtjev($id)
    {

        $zahtjevId = (int)$id;
        $orient = 'P';
        $zahtjev = $this->findModel($zahtjevId);
        $userID = Yii::$app->user->identity->id;
        $urlAplik = \common\models\Postavke::find()->where(['postavka' => 'urlAplik'])->one();
        $emailAdmin = \common\models\Postavke::find()->where(['postavka' => 'adminMail'])->one();

        $message = Yii::$app->mailer->compose('mailPotvrda', [
            'user' => Yii::$app->user->identity,
            'zahtjev' => $zahtjev
        ])
            ->setFrom('info@ordybee.com')
            ->setTo($emailAdmin->vrijednost)
            //->setTo('kvargec@gmail.com')
            ->setSubject("Promjena na $zahtjev->urbroj zahtjevu");
        $message->attach($this->getPdfPathZahtjevi() . $id . '.pdf');
        $message->send();

        return;
    }

    private function getPdfPathPotvrde()
    {
        return Yii::getAlias('@frontend/web') . "/upload/potvrde/";
    }

    private function getPdfPathZahtjevi()
    {
        return Yii::getAlias('@frontend/web') . "/upload/zahtjevi/";
    }
}
