<?php
namespace backend\reports;

use koolreport\KoolReport;

class Testreport extends KoolReport
{
    use \koolreport\yii2\Friendship;
    // By adding above statement, you have claim the friendship between two frameworks
    // As a result, this report will be able to accessed all databases of Yii2
    // There are no need to define the settings() function anymore
    // while you can do so if you have other datasources rather than those
    // defined in Laravel.
    

    function setup()
    {
        $this->src("default")
        ->query('SELECT count(id) as broj,CAST(status as VARCHAR ), CAST(EXTRACT (DOW from datum) as VARCHAR )as dan FROM "prisutnost" where status=1 and entitet=\'dijete\' group by status,EXTRACT (DOW from datum)')
            ->pipe($this->dataStore("status"));
        $this->src("default")
            ->query('SELECT count(id) as broj,CAST(status as VARCHAR ), CAST(EXTRACT (DOW from datum) as VARCHAR )as dan FROM "prisutnost" where entitet=\'zaposlenik\' group by status,EXTRACT (DOW from datum)')
            ->pipe($this->dataStore("status2"));
    }
}