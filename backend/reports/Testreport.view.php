<?php
use \koolreport\widgets\koolphp\Table;
use \koolreport\widgets\google\DonutChart;
?>
<html>
<head>
    <title>My Report</title>
</head>
<body>

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title">Primjer izvještaja</span>
    </div>
    <div class="card-body">
        
<?php
DonutChart::create(array(
    "title"=>"Broj prisutnih",
    "dataSource"=>$this->dataStore("status"),
    "columns"=>array(
        "dan",
        "broj"=>array("label"=>"Broj","type"=>"number","prefix"=>""),
    ),
    "colorScheme"=>array(
        "#003d4e",
        "#9dbacc",
        "#f0ba3c",
        "#136880",
        "#e4a40f" 
    )
));
?>
<?php
Table::create([
    "dataSource"=>$this->dataStore("status"),
    "cssClass"=>array(
        "table"=>"table-bordered table-striped table-hover"
    )
]);
?>
<?php
DonutChart::create(array(
    "title"=>"Broj prisutnih zaposlenih",
    "dataSource"=>$this->dataStore("status2"),
    "columns"=>array(
        "dan",
        "broj"=>array("label"=>"Broj","type"=>"number","prefix"=>""),
        
    ),
    "colorScheme"=>array(
        "#003d4e",
        "#9dbacc",
        "#f0ba3c",
        "#136880",
        "#e4a40f" 
    )
));
?>
<?php
Table::create([
    "dataSource"=>$this->dataStore("status2"),
    "cssClass"=>array(
        "table"=>"table-bordered table-striped table-hover"
    )
]);
?>
    </div></div></div>
 
</body>
</html>

