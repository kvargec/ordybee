<?php

namespace backend\controllers;

use common\models\Mjesto;
use linslin\yii2\curl\Curl;
use Yii;
use common\models\Tvrtka;
use common\models\Search\TvrtkaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TvrtkaController implements the CRUD actions for Tvrtka model.
 */
class TvrtkaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tvrtka models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TvrtkaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tvrtka model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tvrtka model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tvrtka();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tvrtka model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tvrtka model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tvrtka model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tvrtka the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tvrtka::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionGetFromOib(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset($_POST['oib'])) {
            $oib = $_POST['oib'];
            $curl = new Curl();

            /*$curl->setOptions(array(

                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'key' =>'Ocp-Apim-Subscription-Key',
                    'value' => '4c35198de5684c8a8c88c693a28891cd'
                ),
            ));*/
            /* $curl->setGetParams([
                 'Ocp-Apim-Subscription-Key'=>'4c35198de5684c8a8c88c693a28891cd'
             ]);*/
            $response = $curl->setHeaders([
                'Ocp-Apim-Subscription-Key' => '4c35198de5684c8a8c88c693a28891cd'
            ])->get('https://sudreg-api.pravosudje.hr/javni/subjekt_detalji?tipIdentifikatora=oib&identifikator=' . $oib);
            $response=json_decode($response);
            if (!empty($response)) {
                $djelatnost = '';
                $item=$response->predmeti_poslovanja[0];
                $djelatnost .= $item->djelatnost_tekst;
                /*foreach ($response->predmeti_poslovanja as $item) {
                    $djelatnost .= $item->djelatnost_tekst . '<br>';
                }*/
                $mjesto=Mjesto::find()->where(['sifra'=>$response->sjedista[0]->sifra_naselja])->one();

                return [
                    'status' => 'success',
                    'naziv' => $response->tvrtke[0]->ime,
                    'sifra' => $response->mbs,
                    'adresa' => $response->sjedista[0]->ulica . ' ' . $response->sjedista[0]->kucni_broj,
                    'mjesto' => $mjesto->id,
                    'djelatnost' => $djelatnost
                ];
            }
        }

    return [
        'status' => 'error',
        'message' => Yii::t('app', 'Tvrtka sa OIB-om: {oib} nije pronađena', ['oib' => $oib])];
    }
}
