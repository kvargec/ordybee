<?php

namespace backend\controllers;

use Yii;
use Mpdf;
use common\models\Zaposlenje;
use common\models\Search\ZaposlenjeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ZaposlenjeController implements the CRUD actions for Zaposlenje model.
 */
class ZaposlenjeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Zaposlenje models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZaposlenjeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zaposlenje model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Zaposlenje model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Zaposlenje();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Zaposlenje model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Zaposlenje model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Zaposlenje model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zaposlenje the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zaposlenje::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    private function printPDF($sadrzaj,$pdfPath,$nazivDokumenta,$orient='P'){
        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format'=>'A4', 12, '', 'margin_left'=>15, 'margin_right'=>15, 'margin_top'=>16, 'margin_bottom'=>16, 'mgh'=>1, 'mgf'=>1, 'orientation'=>$orient,'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath,\Mpdf\Output\Destination::FILE );
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }

    public function actionPdfPrint ($id) {
        $zaposlenje = Zaposlenje::findOne($id);
        $htmlContent = $this->renderPartial('@common/views/zaposlenje/zaposlenjepdf', [
            'zaposlenje'=>$zaposlenje,
        ]);
        return $this::printPDF($htmlContent, Yii::getAlias('@webroot')."/zaposlenje/".$zaposlenje->zaposlenik0->prezime.", ".$zaposlenje->zaposlenik0->ime."-".$zaposlenje->id.".pdf", $zaposlenje->zaposlenik0->prezime.", ".$zaposlenje->zaposlenik0->ime."-".$zaposlenje->id.".pdf");
    }
}
