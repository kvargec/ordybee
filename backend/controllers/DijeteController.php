<?php

namespace backend\controllers;

use common\models\DijeteZapis;
use common\models\Roditelj;
use common\models\StatusDjeteta;
use Yii;
use Mpdf;
use common\models\Dijete;
use common\models\Search\DijeteSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\PedagoskaGodina;
use common\models\VrstaPrograma;

/**
 * DijeteController implements the CRUD actions for Dijete model.
 */
class DijeteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dijete models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DijeteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'godina' => $godina
        ]);
    }

    /**
     * Displays a single Dijete model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $roditelji = new ActiveDataProvider([
            'query' => Roditelj::find()->where(['dijete' => $id]),
        ]);
        $zdravstvena = new ActiveDataProvider(
            [
                'query' => DijeteZapis::find()->joinWith(['zapis0', 'zapis0.vrsta0'])->where(['dijete' => $id, 'vrsta_zapis.grupa' => 'zdravstvo'])->orderBy('id DESC'),
            ]
        );
        $logoped = new ActiveDataProvider(
            [
                'query' => DijeteZapis::find()->joinWith(['zapis0', 'zapis0.vrsta0'])->where(['dijete' => $id, 'vrsta_zapis.grupa' => 'logoped'])->orderBy('id DESC'),
            ]
        );
        $psiholog = new ActiveDataProvider(
            [
                'query' => DijeteZapis::find()->joinWith(['zapis0', 'zapis0.vrsta0'])->where(['dijete' => $id, 'vrsta_zapis.grupa' => 'psiholog'])->orderBy('id DESC'),
            ]
        );
        $pedagog = new ActiveDataProvider(
            [
                'query' => DijeteZapis::find()->joinWith(['zapis0', 'zapis0.vrsta0'])->where(['dijete' => $id, 'vrsta_zapis.grupa' => 'pedagog'])->orderBy('id DESC'),
            ]
        );

        return $this->render('view', [
            'model' => $this->findModel($id),
            'roditelji' => $roditelji,
            'zdravstvena' => $zdravstvena,
            'logoped' => $logoped,
            'psiholog' => $psiholog,
            'pedagog' => $pedagog
        ]);
    }

    /**
     * Creates a new Dijete model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dijete();
        //provjeri postoje li podvrste vrsta programa, 
        $podvrsta = false;
        $podvrstePrograma = VrstaPrograma::find()->where(['>', 'parent', 0])->one();
        if ($podvrstePrograma) {
            $podvrsta = true;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $post = Yii::$app->request->post('Dijete');
            if (isset($post['vrstadepdrop'])) {
                if (empty($post['vrsta_programa'])) {
                    if (empty($post['vrstadepdrop'])) {
                        $model->addError('vrstadepdrop', Yii::t('app', 'Potrebno je odabrati vrstu programa!'));
                        return $this->render('create', [
                            'model' => $model,
                            'podvrsta' => $podvrsta
                        ]);
                    } else {
                        $model->vrsta_programa = $post['vrstadepdrop'];
                    }
                } 
            } else {
                if (empty($post['vrsta_programa'])) {
                    $model->addError('vrsta_programa', Yii::t('app', 'Potrebno je odabrati vrstu programa!'));
                    return $this->render('create', [
                        'model' => $model,
                        'podvrsta' => $podvrsta
                    ]);
                } else {
                    $model->vrsta_programa = $post['vrsta_programa'];
                }
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'podvrsta' => $podvrsta
        ]);
    }
    public function actionRoditelji($id, $skupina = null)
    {
        $roditelji = Roditelj::find()->where(['dijete' => $id])->orderBy('id ASC')->all();
        $koliko = count($roditelji);

        if (Yii::$app->request->post('Roditelj')) {
            $post = Yii::$app->request->post('Roditelj');
            $i = 0;
            foreach ($post as $item) {
                if (isset($item) and $item != '') {
                    if ($item['id'] != '') {
                        $modelRoditelj = Roditelj::find()->where(['id' => $item['id']])->one();
                        if (!isset($modelRoditelj)) {
                            $modelRoditelj = new Roditelj();
                        }
                    } else {
                        $modelRoditelj = new Roditelj();
                    }

                    $modelRoditelj->ime = $item['ime'];
                    $modelRoditelj->prezime = $item['prezime'];
                    $modelRoditelj->dat_rod = trim($item['dat_rod']);
                    $modelRoditelj->oib = substr(trim($item['oib']), 0, 13);
                    $modelRoditelj->adresa = $item['adresa'];
                    $modelRoditelj->mjesto = $item['mjesto'];
                    $modelRoditelj->prebivaliste = $item['prebivaliste'];
                    $modelRoditelj->radno = $item['radno'];
                    $modelRoditelj->zanimanje = $item['zanimanje'];
                    $modelRoditelj->poslodavaca = $item['poslodavaca'];
                    $modelRoditelj->mobitel = $item['mobitel'];
                    $modelRoditelj->email = $item['email'];
                    $modelRoditelj->dijete = $id;

                    if ($i == 0) {
                        $modelRoditelj->spol = "Ž";
                    } else {
                        $modelRoditelj->spol = "M";
                    }
                    if ($modelRoditelj->save()) {
                    } else {
                        var_dump($modelRoditelj->getErrors());
                        die();
                    };
                }
                $i++;
            }
            if ($skupina == null) {
                return $this->redirect(['view', 'id' => $id]);
            } else {
                return $this->redirect((['skupina/view', 'id' => $skupina]));
            }
        } else {
        }
        if ($koliko < 2) {
            for ($koliko; $koliko <= 2; $koliko++) {
                $roditelji[] = new Roditelj();
            }
        }

        return $this->render('roditelji', [
            'modelRoditelj' => $roditelji,
            'id' => $id,
            'skupina' => $skupina
        ]);
    }
    /**
     * Updates an existing Dijete model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $skupina = null)
    {
        $model = $this->findModel($id);
        $podvrsta = false;
        $podvrstePrograma = VrstaPrograma::find()->where(['>', 'parent', 0])->one();
        if ($podvrstePrograma) {
            $podvrsta = true;
        }

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Dijete');
            $status = StatusDjeteta::find()->where(['status' => 'ispisan'])->one();
            if (!empty($model->dat_ispisa)) {
                $model->status = $status->id;
            }
            if (isset($post['vrstadepdrop'])) {
                if (empty($post['vrsta_programa'])) {
                    if (empty($post['vrstadepdrop'])) {
                        $model->addError('vrstadepdrop', Yii::t('app', 'Potrebno je odabrati vrstu programa!'));
                        return $this->render('create', [
                            'model' => $model,
                            'podvrsta' => $podvrsta
                        ]);
                    } else {
                        $model->vrsta_programa = $post['vrstadepdrop'];
                    }
                } 
            } else {
                if (empty($post['vrsta_programa'])) {
                    $model->addError('vrsta_programa', Yii::t('app', 'Potrebno je odabrati vrstu programa!'));
                    return $this->render('create', [
                        'model' => $model,
                        'podvrsta' => $podvrsta
                    ]);
                } else {
                    $model->vrsta_programa = $post['vrsta_programa'];
                }
            }
            $model->save();
            if ($model->save(false)) {
                if ($skupina == null) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->redirect((['skupina/view', 'id' => $skupina]));
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'podvrsta' => $podvrsta
        ]);
    }

    /**
     * Deletes an existing Dijete model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Dijete model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dijete the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dijete::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    private function printPDF($sadrzaj, $pdfPath, $nazivDokumenta, $orient = 'P')
    {
        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }

    public function actionPdfPrint($id)
    {
        $dijete = Dijete::findOne($id);
        $roditelji = new ActiveDataProvider([
            'query' => Roditelj::find()->where(['dijete' => $id]),
        ]);
        $zdravstvena = new ActiveDataProvider(
            [
                'query' => DijeteZapis::find()->joinWith(['zapis0', 'zapis0.vrsta0'])->where(['dijete' => $id, 'vrsta_zapis.grupa' => 'zdravstvo'])->orderBy('id DESC'),
            ]
        );
        $logoped = new ActiveDataProvider(
            [
                'query' => DijeteZapis::find()->joinWith(['zapis0', 'zapis0.vrsta0'])->where(['dijete' => $id, 'vrsta_zapis.grupa' => 'logoped'])->orderBy('id DESC'),
            ]
        );
        $psiholog = new ActiveDataProvider(
            [
                'query' => DijeteZapis::find()->joinWith(['zapis0', 'zapis0.vrsta0'])->where(['dijete' => $id, 'vrsta_zapis.grupa' => 'psiholog'])->orderBy('id DESC'),
            ]
        );
        $pedagog = new ActiveDataProvider(
            [
                'query' => DijeteZapis::find()->joinWith(['zapis0', 'zapis0.vrsta0'])->where(['dijete' => $id, 'vrsta_zapis.grupa' => 'pedagog'])->orderBy('id DESC'),
            ]
        );
        $htmlContent = $this->renderPartial('@common/views/dijete/dijetepdf', [
            'dijete' => $dijete,
            'roditelji' => $roditelji,
            'zdravstvena' => $zdravstvena,
            'logoped' => $logoped,
            'psiholog' => $psiholog,
            'pedagog' => $pedagog
        ]);
        return $this::printPDF($htmlContent, Yii::getAlias('@webroot') . "/djeca/" . $dijete->ime . "-" . $dijete->id . ".pdf", $dijete->ime . "-" . $dijete->id . ".pdf");
    }
}
