<?php

namespace backend\controllers;

use common\models\MsgFolders;
use Yii;
use common\models\Mailbox;
use common\models\Roditelj;
use common\models\Skupina;
use common\models\Zaposlenje;
use common\models\Zaposlenik;
use common\models\User;
use common\models\Search\MailboxSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MailboxController implements the CRUD actions for Mailbox model.
 */
class MailboxController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ['verbs' => ['class' => VerbFilter::className(), 'actions' => ['delete' => ['POST'],],],];
    }

    /**
     * Lists all Mailbox models.
     * @return mixed
     */
    public function actionIndex($folder = 'inbox')
    {
        $searchModel = new MailboxSearch();
        $msgFolder = MsgFolders::find()->where(['folder_name' => $folder])->one();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $msgFolder->id);
        $folders = MsgFolders::find()->where(['in', 'user', [0, Yii::$app->user->id]])->orderBy('redoslijed')->all();
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'folders' => $folders, 'msgFolder' => $msgFolder]);
    }

    /**
     * Displays a single Mailbox model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', ['model' => $this->findModel($id),]);
    }

    /**
     * Finds the Mailbox model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mailbox the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mailbox::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Creates a new Mailbox model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mailbox();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing Mailbox model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', ['model' => $model,]);
    }

    /**
     * Deletes an existing Mailbox model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function ccrec($k, $v)
    {
        $ccc = '';
        switch ($k) {
            case 'roditelji':
                if (isset($v[0])) {
                    if ($v[0] == '0') {
                        $ccc .= 'Svi roditelji, ';
                    } else {
                        $kojiUseri = Roditelj::find()->where(['id' => $v])->all();
                        if (isset($kojiUseri)) {
                            foreach ($kojiUseri as $ku) {
                                $ccc .= $ku->ime . ' ' . $ku->prezime . ', ';
                            }
                        }
                    }
                } else {
                    $ccc .= '';
                }
                break;
            case 'skupine':
                if (isset($v[0])) {
                    if ($v[0] == '0') {
                        $ccc .= 'Sve skupine, ';
                    } else {
                        $kojiUseri = Skupina::find()->where(['id' => $v])->all();
                        if (isset($kojiUseri)) {
                            foreach ($kojiUseri as $ku) {
                                $ccc .= $ku->naziv . ', ';
                            }
                        }
                    }
                } else {
                    $ccc .= '';
                }
                break;
            case 'zaposlenici':
                if (isset($v[0])) {
                    if ($v[0] == '0') {
                        $ccc .= 'Sva radna mjesta, ';
                    } else {
                        $radnaMjesta = Zaposlenje::find()->where(['r_mjesto' => $v])->one();
                        $rmz = $radnaMjesta->zaposlenik;
                        $kojiUseri = Zaposlenik::find()->where(['id' => $rmz])->all();
                        if (isset($kojiUseri)) {
                            foreach ($kojiUseri as $ku) {
                                $ccc .= $ku->ime . ' ' . $ku->prezime . ', ';
                            }
                        }
                    }
                } else {
                    $ccc .= '';
                }

                break;
            case 'pojedini':
                if (isset($v[0])) {
                    if ($v[0] == '0') {
                        $ccc .= 'Svi zaposlenici, ';
                    } else {
                        $kojiUseri = Zaposlenik::find()->where(['id' => $v])->all();
                        if (isset($kojiUseri)) {
                            foreach ($kojiUseri as $ku) {
                                $ccc .= $ku->ime . ' ' . $ku->prezime . ', ';
                            }
                        }
                    }
                } else {
                    $ccc .= '';
                }
                break;
            case 'admin':
                if (isset($v[0])) {
                    if ($v[0] == '0') {
                        $ccc .= 'Svi administratori, ';
                    } else {
                        $kojiUseri = User::find()->where(['id' => $v])->all();
                        if (isset($kojiUseri)) {
                            foreach ($kojiUseri as $ku) {
                                $ccc .= $ku->username . ', ';
                            }
                        }
                    }
                } else {
                    $ccc .= '';
                }
                break;
        }
        return $ccc;
    }
}
