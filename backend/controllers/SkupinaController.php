<?php

namespace backend\controllers;

use common\helpers\Utils;
use common\models\DijeteSkupina;
use common\models\Dijete;
use common\models\Roditelj;
use common\models\Skupina;
use common\models\SkupinaObjekt;
use common\models\ZaposlenikSkupina;
use common\models\PedagoskaGodina;
use common\models\Search\DijeteSkupinaSearch;
use common\models\Search\SkupinaSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii2tech\spreadsheet\Spreadsheet;
use Mpdf;

/**
 * SkupinaController implements the CRUD actions for Skupina model.
 */
class SkupinaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Skupina models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SkupinaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Skupina model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        //$query = DijeteSkupina::find()->joinWith(['dijete0'])->where(['skupina'=>$id,'dijete.status'=>2]);
        //$dataProvider= new ActiveDataProvider([
        //    'query' => $query->orderBy('prezime,ime'),
        //    'pagination' => ['pageSize' => 100]
        //]);  

        $searchModel = new DijeteSkupinaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['skupina' => $id, 'dijete.status' => 2]);

        $model2 = new DijeteSkupina();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'model2' => $model2,

            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Creates a new Skupina model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Skupina();

        if ($this->save($model)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    private function save($model)
    {
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();

            foreach ($model->skupinaObjekts as $skupinaObjekt) {
                $skupinaObjekt->delete();
            }
            foreach ($model->zaposleniciSkupine as $zaposlenikSkupina) {
                $zaposlenikSkupina->delete();
            }

            if (empty($model->ped_godina)) {
                $model->ped_godina = $godina->id;
                $model->save();
            }

            $objektIds = Yii::$app->request->post('objektIds', []);
            foreach ($objektIds as $objektId) {
                $skupinaObjekt = new SkupinaObjekt();
                $skupinaObjekt->skupina = $model->id;
                $skupinaObjekt->objekt = $objektId;
                $skupinaObjekt->aktivna = true;
                $skupinaObjekt->save();
            }

            $zaposlenikIds = Yii::$app->request->post('zaposlenikIds', []);
            foreach ($zaposlenikIds as $zaposlenikId) {
                $zaposlenikSkupina = new ZaposlenikSkupina();
                $zaposlenikSkupina->skupina = $model->id;
                $zaposlenikSkupina->zaposlenik = $zaposlenikId;
                $zaposlenikSkupina->aktivno = true;
                $zaposlenikSkupina->ped_godina = $godina->id;
                $zaposlenikSkupina->save();
            }

            return true;
        }

        return false;
    }

    /**
     * Updates an existing Skupina model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->save($model)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Skupina model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionPopis($id)
    {
        $imeSkupine = "";
        $model = $this->findModel($id);
        $imeSkupine = $model->naziv;
        $tempTekst = '<br /><br /><br /><table class="table table-striped table-bordered">';
        $djeca = DijeteSkupina::find()->where(['skupina' => $id])->all();
        foreach ($djeca as $kid) {
            $tempTekst .= '<tr><td>' . $kid->dijete0->ime . '</td><td>' . $kid->dijete0->prezime . '</td></tr>';
        }
        $tempTekst .= '</table>';
        $contentFull = Controller::renderPartial('@common/views/templates/memorandum', [
            'naslov' => Yii::t('app', 'Popis djece skupine ') . $imeSkupine,
            'content' => $tempTekst
        ]);
        $utils = Utils::ispisPDF($contentFull, 'popisSkupine', 'download', 'P');
    }
    public function actionXlsexport($id)
    {

        $exporter = new Spreadsheet([
            'writerType' => 'Xlsx',
            'dataProvider' => new ActiveDataProvider([
                'query' => DijeteSkupina::find()->where(['skupina' => $id]),

            ]),
            'columns' => [
                [
                    'header' => Yii::t('app', 'Ime i prezime'),
                    'value' => function ($dijete) {
                        return $dijete->dijete0->ime . ' ' . $dijete->dijete0->prezime;
                    }
                ],
                'dijete0.dat_rod:text:Datum rođenja',
                'dijete0.oib:text:OIB',
                [
                    'header' => Yii::t('app', 'Adresa'),
                    'value' => function ($dijete) {
                        return $dijete->dijete0->adresa . ' ' . $dijete->dijete0->mjesto;
                    }
                ],
                [
                    'header' => Yii::t('app', 'Majka'),
                    'value' => function ($dijete) {
                        $mama = Roditelj::find()->where(['dijete' => $dijete->dijete, 'spol' => 'Ž'])->one();
                        return $mama->ime . ' ' . $mama->prezime;
                    }
                ],
                [
                    'header' => Yii::t('app', 'Kontakt majke'),
                    'value' => function ($dijete) {
                        $mama = Roditelj::find()->where(['dijete' => $dijete->dijete, 'spol' => 'Ž'])->one();
                        return $mama->mobitel;
                    }
                ],
                [
                    'header' => Yii::t('app', 'Otac'),
                    'value' => function ($dijete) {
                        $mama = Roditelj::find()->where(['dijete' => $dijete->dijete, 'spol' => 'M'])->one();
                        return $mama->ime . ' ' . $mama->prezime;
                    }
                ],
                [
                    'header' => Yii::t('app', 'Kontakt oca'),
                    'value' => function ($dijete) {
                        $mama = Roditelj::find()->where(['dijete' => $dijete->dijete, 'spol' => 'M'])->one();
                        return $mama->mobitel;
                    }
                ]
            ],
        ]);
        return $exporter->save(__DIR__ . '/../web/popisDjece.xls');
    }
    /**
     * Finds the Skupina model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Skupina the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Skupina::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionQrCodes($id)
    {
        $skupina = Skupina::find()->where(['id' => $id])->one();
        $djeca_ids = DijeteSkupina::find()->joinWith(['dijete0'])->where(['skupina' => $id, 'dijete.status' => 2])->all();
        $djeca = [];
        foreach ($djeca_ids as $dijete_id) {
            $djeca[] = Dijete::find()->where(['id' => $dijete_id->dijete])->one();
        }
        $sadrzaj = $this->renderPartial('@common/views/templates/djecaQrKodovi', ['djeca' => $djeca]);
        $documentTitle = 'QR Kodovi - Grupa ' . $skupina->naziv . '.pdf';
        $documentPath = Yii::getAlias('@webroot') . "/skupine/" . $skupina->naziv . '/' . $skupina->naziv . '.pdf';

        //print("<pre>".print_r($sadrzaj,true)."</pre>");die();
        $this->printPDF($sadrzaj, $documentPath, $documentTitle);
    }

    private function printPDF($sadrzaj, $pdfPath, $nazivDokumenta, $orient = 'P')
    {
        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }
}
