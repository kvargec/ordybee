<?php

namespace backend\controllers;

use Yii;
use Mpdf;
use common\models\Objekt;
use common\models\PedagoskaGodina;
use common\models\Search\ObjektSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ObjektController implements the CRUD actions for Objekt model.
 */
class ObjektController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Objekt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ObjektSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Objekt model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Objekt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Objekt();
        $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $model->ped_godina = $godina->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Objekt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Objekt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Objekt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Objekt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Objekt::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    private function printPDF($sadrzaj,$pdfPath,$nazivDokumenta,$orient='P'){
        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format'=>'A4', 12, '', 'margin_left'=>15, 'margin_right'=>15, 'margin_top'=>16, 'margin_bottom'=>16, 'mgh'=>1, 'mgf'=>1, 'orientation'=>$orient,'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath,\Mpdf\Output\Destination::FILE );
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }

    public function actionPdfPrint ($id) {
        $objekt = Objekt::findOne($id);
        $htmlContent = $this->renderPartial('@common/views/objekt/objektpdf', [
            'objekt'=>$objekt,
        ]);
        return $this::printPDF($htmlContent, Yii::getAlias('@webroot')."/zaposlenje/".$objekt->naziv."-".$objekt->id.".pdf", $objekt->naziv."-".$objekt->id.".pdf");
    }
}
