<?php

namespace backend\controllers;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use phpDocumentor\Reflection\Element;
use yii\imagine\Image;
use common\models\DodatakSadrzaj;
use common\models\Jelo;
use common\models\Obrok;
use Imagine\Image\ImageInterface;
use Yii;
use common\models\Jelovnik;
use common\models\Search\JelovnikSearch;
use yii\base\DynamicModel;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Mpdf;
use yii\web\UploadedFile;

/**
 * JelovnikController implements the CRUD actions for Jelovnik model.
 */
class JelovnikController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Jelovnik models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JelovnikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jelovnik model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($tjedan=null)
    {
        if($tjedan == null)
            $tjedan='0';
        $datum = $this::getDatum($tjedan);
        $dorucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $rucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $uzina = new DynamicModel(['pon','uto','sri','cet','pet']);
        $model= new DynamicModel(['dorucak','rucak','uzina']);
        $model->rucak = $rucak;
        $model->uzina=$uzina;
        $model->dorucak=$dorucak;
        $model2['rucak']=$rucak;
        $model2['dorucak']=$dorucak;
        $model2['uzina']=$uzina;

        foreach ($model as $obrok=>$item){
            $tmpObrok = Obrok::find()->where(['obrok'=>$obrok])->one();
            foreach ($item as $dan=>$jelo){
                $jeloZapis = Jelovnik::find()->where(['datum'=>$datum[$dan],'obrok'=>$tmpObrok->id])->orderBy('redoslijed ASC')->all();

                if(!empty($jeloZapis)) {
                    $tmp = '';
                    foreach ($jeloZapis as $item) {
                        $tmp .= strtolower($item->jelo0->naziv) . ', ';
                    }
                    $model->$obrok->$dan = ucfirst(substr($tmp,0,-2));
                }
            }
        }
        return $this->render('view', ['model' =>$model,'datum' =>$datum,'tjedan'=>$tjedan]);

    }

    /**
     * Creates a new Jelovnik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($tjedan=null)
    {
        $datum = $this::getDatum($tjedan);
        $dorucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $rucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $uzina = new DynamicModel(['pon','uto','sri','cet','pet']);
        $dodatak = new DodatakSadrzaj();
        if (isset($_POST['DynamicModel'])) {
            $dodatak->attributes = $_POST['DodatakSadrzaj'];
            $imageFile = UploadedFile::getInstance($dodatak, 'dodatak');

            $directory = Yii::getAlias('@backend/web/media/') ;
            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }
            if(isset($imageFile)){
                $fileName = $imageFile->name ;
                $filePath = $directory . $fileName;
                if ($imageFile->saveAs($filePath)) {
                    Image::$thumbnailBackgroundAlpha=0;
                    $imagine = new Imagine();
                    $image = $imagine->open($filePath);
                    $image->thumbnail(new Box(400,400))->save($filePath,['quality'=>100]);

                    $dodatak->dodatak = $fileName;
                    $dodatak->tip=1;
                    $dodatak->datum_pocetak = $datum['pon'];
                    $dodatak->datum_kraj = $datum['pet'];
                    $dodatak->save();
                }
            }



            Jelovnik::deleteAll("datum between '".$datum['pon']."' and '".$datum['pet']."'");
            foreach ($_POST['DynamicModel'] as $key=>$value){ //$key je dorucak rucak ili uzina, value je polje
                $obrok = Obrok::find()->where(['obrok'=>$key])->one();
                foreach ($value as $dan=>$listaJela) {
                    if (!empty($listaJela)) {
                        foreach ($listaJela as $index => $jelo) {
                            $jeloZapis = Jelo::find()->where(['naziv' => $jelo])->one();
                            if (empty($jeloZapis)) {

                                $jeloZapis = new Jelo();
                                $jeloZapis->naziv = $jelo;
                                $jeloZapis->save(false);
                            }

                            $jelovnik = new Jelovnik();
                            $jelovnik->datum = $datum[$dan];
                            $jelovnik->jelo = $jeloZapis->id;
                            $jelovnik->obrok = $obrok->id;
                            $jelovnik->redoslijed = $index + 1;
                            $jelovnik->save(false);
                        }
                    }
                }


            }
            return $this->redirect(['/jelovnik/view','tjedan'=>$tjedan]);

        }

        return $this->render('create', [
            'rucak' => $rucak,
            'dorucak'=>$dorucak,
            'uzina'=>$uzina,
            'datum'=>$datum,
            'dodatak' => $dodatak
        ]);
    }

    /**
     * Updates an existing Jelovnik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($tjedan=null)
    {
        $datum = $this::getDatum($tjedan);
        $dorucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $rucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $uzina = new DynamicModel(['pon','uto','sri','cet','pet']);
        $model= new DynamicModel(['dorucak','rucak','uzina']);
        $model->rucak = $rucak;
        $model->uzina=$uzina;
        $model->dorucak=$dorucak;
        $model2['rucak']=$rucak;
        $model2['dorucak']=$dorucak;
        $model2['uzina']=$uzina;
        $dodatak = DodatakSadrzaj::find()->where("datum_pocetak between '".date('m-d-y',strtotime($datum['pon']))."' and '".date('m-d-y',strtotime($datum['pet']))."'")->orderBy(['id' => SORT_DESC])->one();

        if (empty($dodatak)){
            $dodatak = new DodatakSadrzaj();
        }
        foreach ($model as $obrok=>$item){
            $tmpObrok = Obrok::find()->where(['obrok'=>$obrok])->one();
            foreach ($item as $dan=>$jelo){
                $jeloZapis = Jelovnik::find()->where(['datum'=>$datum[$dan],'obrok'=>$tmpObrok->id])->orderBy('redoslijed asc')->all();
                $tmp = [];
                foreach ($jeloZapis as $item){
                    $tmp[$item->redoslijed-1] = $item->jelo0->naziv;


                }
                $model->$obrok->$dan = $tmp;
            }
        }

        if (isset($_POST['DynamicModel'])){
            $dodatak->attributes = $_POST['DodatakSadrzaj'];
            $imageFile = UploadedFile::getInstance($dodatak, 'dodatak');

            $directory = Yii::getAlias('@backend/web/media/');
            if (!is_dir($directory)) {
                FileHelper::createDirectory($directory);
            }
            if(isset($imageFile)){
                $fileName = $imageFile->name ;
                $filePath = $directory . $fileName;
                if ($imageFile->saveAs($filePath)) {
                    Image::$thumbnailBackgroundAlpha=0;
                    $imagine = new Imagine();
                    $image = $imagine->open($filePath);
                    $image->thumbnail(new Box(400,400))->save($filePath,['quality'=>100]);


                    $dodatak->dodatak = $fileName;
                }
            }else {
                $dodatak->dodatak = empty($dodatak->oldAttributes['dodatak'])?'':$dodatak->oldAttributes['dodatak'];
            }
            $dodatak->datum_kraj = $datum['pet'];
            $dodatak->datum_pocetak = $datum['pon'];
            $dodatak->tip = 1;
            $dodatak->save();
            foreach ($_POST['DynamicModel'] as $key=>$value) {
                $obrok=Obrok::find()->where(['obrok'=>$key])->one();
                foreach ($value as $dan=>$listaJela){
                    if (!empty($listaJela)) {
                        $jelovnik = Jelovnik::find()->where(['datum' => $datum[$dan], 'obrok' => $obrok->id]);
                        if ($jelovnik->count() > sizeof($listaJela)) {
                            Jelovnik::deleteAll('redoslijed >' . sizeof($listaJela));
                        }
                        foreach ($listaJela as $index => $jelo) {
                            $jeloTmp = Jelo::find()->where(['naziv' => $jelo])->one();
                            if (empty($jeloTmp)) {
                                $jeloTmp = new Jelo();
                                $jeloTmp->naziv = $jelo;
                                $jeloTmp->save(false);
                            }
                            $jelovnik = Jelovnik::find()->where(['datum' => $datum[$dan], 'obrok' => $obrok->id, 'redoslijed' => $index + 1])->one();
                            if (empty($jelovnik)) {
                                $jelovnik = new Jelovnik();
                                $jelovnik->obrok = $obrok->id;
                                $jelovnik->datum = $datum[$dan];
                                $jelovnik->redoslijed = $index + 1;
                            }
                            $jelovnik->jelo = $jeloTmp->id;
                            $jelovnik->save(false);

                        }
                    }
                }

            }
            return $this->redirect(['view','tjedan'=>$tjedan]);
        }
        return $this->render('update', ['model' =>$model,'datum' =>$datum, 'dodatak'=>$dodatak]);

    }

    /**
     * Deletes an existing Jelovnik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Jelovnik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Jelovnik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jelovnik::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPdf($tjedan){
        $datum = $this::getDatum($tjedan);
        $dodatak = DodatakSadrzaj::find()->where("datum_pocetak between '".date('m-d-y',strtotime($datum['pon']))."' and '".date('m-d-y',strtotime($datum['pet']))."'")->orderBy(['id' => SORT_DESC])->one();
        $dorucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $rucak = new DynamicModel(['pon','uto','sri','cet','pet']);
        $uzina = new DynamicModel(['pon','uto','sri','cet','pet']);
        $model= new DynamicModel(['dorucak','rucak','uzina']);
        $model->rucak = $rucak;
        $model->uzina=$uzina;
        $model->dorucak=$dorucak;
        $model2['rucak']=$rucak;
        $model2['dorucak']=$dorucak;
        $model2['uzina']=$uzina;

        foreach ($model as $obrok=>$item){
            $tmpObrok = Obrok::find()->where(['obrok'=>$obrok])->one();
            foreach ($item as $dan=>$jelo){
                $jeloZapis = Jelovnik::find()->where(['datum'=>$datum[$dan],'obrok'=>$tmpObrok->id])->orderBy('redoslijed ASC')->all();

                if(!empty($jeloZapis)) {
                    $tmp = '';
                    foreach ($jeloZapis as $item) {
                        $tmp .= strtolower($item->jelo0->naziv) . ', ';
                    }
                    $model->$obrok->$dan = ucfirst(substr($tmp,0,-2));
                }
            }
        }
        $htmlContent =  $this->renderPartial('../../../common/views/templates/jelovnikpdf', ['model' =>$model,'datum' =>$datum,'dodatak'=>$dodatak]);
        return $this::printPDF($htmlContent,
            Yii::getAlias('@webroot')."/jelovnik/jelovnik".date('j.n.Y',strtotime( $datum['pon']))."-".date('j.n.Y',strtotime( $datum['pet'])).".pdf",
            "jelovnik".date('j.n.Y',strtotime( $datum['pon']))."-".date('j.n.Y',strtotime( $datum['pet'])).".pdf");
    }
    private function printPDF($sadrzaj,$pdfPath,$nazivDokumenta,$orient='P'){

        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format'=>'A4', 12, '', 'margin_left'=>15, 'margin_right'=>15, 'margin_top'=>16, 'margin_bottom'=>16, 'mgh'=>1, 'mgf'=>1, 'orientation'=>$orient,'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath,\Mpdf\Output\Destination::FILE );
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }


    private function getDatum($tjedan){
        $datum['pon'] = date('Y-m-d', strtotime('monday this week'));
        $datum['uto'] = date('Y-m-d', strtotime('tuesday this week'));
        $datum['sri'] = date('Y-m-d', strtotime('wednesday this week'));
        $datum['cet'] = date('Y-m-d', strtotime('thursday this week'));
        $datum['pet'] = date('Y-m-d', strtotime('friday this week'));
        if ($tjedan == '0'){
            return $datum;
        }else{
            $datum['pon'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['pon'])));
            $datum['uto'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['uto'])));
            $datum['sri'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['sri'])));
            $datum['cet'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['cet'])));
            $datum['pet'] = date('Y-m-d', strtotime($tjedan.' week',strtotime($datum['pet'])));
        }
        return $datum;

    }
}
