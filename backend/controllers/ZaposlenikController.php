<?php

namespace backend\controllers;

use Yii;
use common\models\Zaposlenik;
use common\models\Search\ZaposlenikSearch;
use common\models\Kontakt;
use Da\User\Model\User;
use common\helpers\Utils;
use common\models\Postavke;
use common\models\ZaposlenikKontakt;
use common\models\Search\ZaposlenikKontaktSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use Mpdf;
use yii\web\UploadedFile;
//use common\helpers\Utils;


/**
 * ZaposlenikController implements the CRUD actions for Zaposlenik model.
 */
class ZaposlenikController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Zaposlenik models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZaposlenikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIndex2()
    {
        $searchModel = new ZaposlenikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIndex3()
    {
        $searchModel = new ZaposlenikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index3', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Zaposlenik model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $zaposlenikKontakt = $model->createZaposlenikKontakt();
        $kontakt = new Kontakt();
        if ($kontakt->load(Yii::$app->request->post()) && $kontakt->save()) {
            $postojeciKontakt = Kontakt::find()
                ->where(['<>', 'id', $kontakt->id])
                ->andWhere(['vrsta' => $kontakt->vrsta, 'vrijednost' => $kontakt->vrijednost])
                ->one();
            if ($postojeciKontakt) {
                $kontakt->delete();
                $kontakt = $postojeciKontakt;
            }
            $zaposlenikKontakt->kontakt = $kontakt->id;
            $zaposlenikKontakt->save();
            $kontakt = new Kontakt(); //reset model
        }

        $searchModel = new ZaposlenikKontaktSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['zaposlenik' => $id]);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'kontakt' => $kontakt,
            'zaposlenikKontakt' => $zaposlenikKontakt,
        ]);
    }

    /**
     * Creates a new Zaposlenik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Zaposlenik();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $zaposlenik = Zaposlenik::find()->where(['oib' => $model->oib])->andWhere(['!=', 'status', 3])->all();
            if (count($zaposlenik) > 0) {
                $model->addError('oib', Yii::t('app', 'Već postoji zaposlenik s ovim OIB-om!'));
                return $this->render('create', ['model' => $model]);
            }
            $model->upload($model->id);
            $model->status = 2;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Zaposlenik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->upload($model->id);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Zaposlenik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->status = 3;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Zaposlenik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zaposlenik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zaposlenik::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    private function printPDF($sadrzaj, $pdfPath, $nazivDokumenta, $orient = 'P')
    {
        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }

    public function actionPdfPrint($id)
    {
        $zaposlenik = Zaposlenik::findOne($id);
        $searchModel = new ZaposlenikKontaktSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['zaposlenik' => $id]);
        $htmlContent = $this->renderPartial('@common/views/zaposlenik/zaposlenikpdf', [
            'zaposlenik' => $zaposlenik,
            'dataProvider' => $dataProvider,
        ]);
        return $this::printPDF($htmlContent, Yii::getAlias('@webroot') . "/zaposlenici/" . $zaposlenik->ime . "-" . $zaposlenik->id . ".pdf", $zaposlenik->ime . "-" . $zaposlenik->id . ".pdf");
    }

    public function actionDeleteAttachments($id)
    {
        $model = Zaposlenik::findOne($id);
        $model->attachments = ['attachments' => []];
        $model->save();
        $dir = Yii::getAlias('@backend') . '/web/zaposlenici/' . $id;
        $files = glob($dir . '/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
        return $this->redirect(['index']);
    }

    public function actionDeleteSglAttachment($id, $title)
    {
        $model = Zaposlenik::findOne($id);
        $attName = array_search($title, $model->attachments);
        if ($attName) {
            unset($model->attachments['attachments'][$attName]);
        }
        $model->save();
        $dir = Yii::getAlias('@backend') . '/web/zaposlenici/' . $id;
        array_map('unlink', glob($dir . $title));

        return $this->redirect(['index']);
    }
    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function actionGenerirajUsere()
    {
        set_time_limit(0);

        $zaposleniciNotExisting = Zaposlenik::find()->where(['user' => null])->all();
        $zaposleniciToPrint = [];

        foreach ($zaposleniciNotExisting as $zaposlenik) {
            $user = new User();
            $ime = Utils::url_slug($zaposlenik->ime);
            $prezime = Utils::url_slug($zaposlenik->prezime);
            $username = substr($ime, 0, 1) . '_' . substr($prezime, 0, 10) . '_' . rand(1, 100);
            $zaposlenik_kontakti = ZaposlenikKontakt::find()->select('kontakt')->where(['zaposlenik' => $zaposlenik->id])->asArray()->all();
            $email = Kontakt::find()->where(['in', 'id', array_column($zaposlenik_kontakti, 'kontakt')])->andWhere(['vrsta' => 2])->one();
            if (isset($email) && !empty($email)) {
                $email_in_use = User::find()->where(['email' => $email])->one();
                if ($email_in_use) {
                    $user->email = $username . '@placeholder.com';
                } else {
                    $user->email = $email->vrijednost;
                }
            } else {
                $user->email = $username . '@nema.com';
            }
            $user->username = $username;
            $user->auth_key = Yii::$app->getSecurity()->generateRandomKey();
            $password = $this->generateRandomString(8);
            $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
            $user->flags = 0;
            $user->created_at = time();
            $user->updated_at = time();
            $user->confirmed_at = time();
            if ($user->save()) {
                $zaposlenik->user = $user->id;
                $zaposlenik->save();
                $zaposleniciToPrint[] = ['username' => $user->username, 'password' => $password, 'ime_zaposlenika' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
            }
        }
        $ime_vrtica = Postavke::find()->where(['postavka' => 'nazivVrtica'])->one();
        $temp_chld = array_column($zaposleniciToPrint, 'ime_zaposlenika');
        array_multisort($temp_chld, SORT_ASC, $zaposleniciToPrint);
        $parsed_name = Utils::url_slug($ime_vrtica->vrijednost);
        $naziv = 'ZaposleniciUseri-' . $parsed_name;
        $dir = Yii::getAlias('@webroot') . '/useri-zaposlenici/';
        //$htmlContent = $this->renderPartial('@common/views/templates/zaposlenici', ['zaposlenici' => $zaposleniciToPrint]);
        //Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $puni_path = $dir . $naziv . '.csv';
        $fp = fopen($puni_path, "a+");
        foreach ($zaposleniciToPrint as $item) {
            fputcsv($fp, $item);
        }
        fclose($fp);
        if (!is_file($puni_path)) {
            throw new \yii\web\NotFoundHttpException('The file does not exists.');
        }
        return Yii::$app->response->sendFile($puni_path, basename($puni_path));
    }
}
