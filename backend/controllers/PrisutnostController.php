<?php

namespace backend\controllers;

use common\helpers\Utils;
use common\models\Dijete;
use common\models\Dnevnik;
use common\models\VrstaDnevnik;
use common\models\DijeteSkupina;
use common\models\Prisutnost;
use common\models\Skupina;
use common\models\DnevnikZapis;
use common\models\Zaposlenik;
use common\models\Postavke;
use common\models\StatusDjeteta;
use common\models\PedagoskaGodina;
use DateTime;
use kartik\widgets\TimePicker;
use Mpdf\Tag\P;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use SebastianBergmann\CodeCoverage\Util;
use Yii;
use yii\base\DynamicModel;
use function GuzzleHttp\Psr7\str;
use function Symfony\Component\String\s;

class PrisutnostController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDjeca($id, $tjedan)
    {
        $datum = $this->getDatum($tjedan);
        $status_upisan = StatusDjeteta::find()->where(['status' => 'upisan'])->one();
        $djeca = Dijete::find()->leftJoin('dijete_skupina', 'dijete_skupina.dijete = dijete.id')->where(['dijete_skupina.skupina' => $id])->andWhere(['dijete.status' => $status_upisan->id])->orderBy(['prezime' => SORT_ASC])->all();
        // foreach ($datum as $key=>$value){
        //   foreach ($djeca as $dijete){
        //     $prisutnost = Prisutnost::find()->where(['datum'=>$value,'entitet'=>'dijete','entitet_id'=>$dijete->id])->one();
        //                if(empty($prisutnost)){
        //                    $prisutnost = new Prisutnost();
        //                    $prisutnost->entitet = 'dijete';
        //                    $prisutnost->trajanje = 8.00;
        //                    $prisutnost->entitet_id = $dijete->id;
        //                    $prisutnost->datum = $value;
        //                    $prisutnost->status = 1;
        //                    $prisutnost->save(false);
        //                    }
        //                }
        //            }

        $skupina = Skupina::findOne($id);
        return $this->render('djeca', ['djeca' => $djeca, 'skupina' => $skupina, 'datumi' => $datum, 'tjedan' => $tjedan]);
    }
    public function actionDodajDijete($id, $datum, $tjedan = null)
    {
        $godina = PedagoskaGodina::getAktivna();
        $skupina = Skupina::find()->leftJoin('dijete_skupina', 'dijete_skupina.skupina = skupina.id')->where(['dijete_skupina.dijete' => $id])->andWhere(['skupina.ped_godina' => $godina->id])->one(); //DijeteSkupina::find()->where(['dijete' => $id])->one();
        //        if($datum>date('yy-m-d',time())){
        //            Yii::$app->session->setFlash('error', "Prisutnost je moguće unijeti samo za ".date('d.m.Y',time()));
        //            return $this->redirect(['/prisutnost/djeca', 'id'=>$skupina->skupina]);
        //        }
        $prisutnost = Prisutnost::find()->where(['entitet' => 'dijete', 'entitet_id' => $id, 'datum' => $datum])->one();
        if (empty($prisutnost)) {
            $prisutnost = new Prisutnost();
            $prisutnost->entitet_id = $id;
            $prisutnost->entitet = 'dijete';
            $prisutnost->datum = $datum;
        }
        $prisutnost->status = 1;
        $prisutnost->razlog_izostanka = null;
        $prisutnost->save();
        if (!Yii::$app->request->isAjax) {
            $this->redirect(['/prisutnost/razlog-izostanka', 'id' => $skupina->id, 'tjedan' => $tjedan]);
        } else {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'message' => Yii::t('app', 'Unesena prisutnost'),
                'id' => $id . $datum,
                'dodatak' => '',
            ];
        }
    }

    public function actionObrisiDijete($id, $datum, $tjedan = null)
    {
        $godina = PedagoskaGodina::getAktivna();
        $prisutnost = Prisutnost::find()->where(['entitet' => 'dijete', 'entitet_id' => $id, 'datum' => $datum])->one();
        $prisutnost->status = 0;
        $prisutnost->save();
        $skupina = Skupina::find()->leftJoin('dijete_skupina', 'dijete_skupina.skupina = skupina.id')->where(['dijete_skupina.dijete' => $id])->andWhere(['skupina.ped_godina' => $godina->id])->one(); //DijeteSkupina::find()->where(['dijete' => $id])->one();
        $dodatak = $prisutnost->razlog_izostanka == null ? 'Razlog izostanka: -' : 'Razlog izostanka: ' . $prisutnost->razlog_izostanka;
        if (!Yii::$app->request->isAjax) {
            $this->redirect(['/prisutnost/razlog-izostanka', 'id' => $skupina->id, 'tjedan' => $tjedan]);
        } else {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'message' => Yii::t('app', 'Unesena prisutnost'),
                'id' => $id . $datum,
                'dodatak' => $dodatak,
            ];
        }
    }
    public function actionZaposlenici($tjedan = 0)
    {
        $postavka = Postavke::find()->where(['postavka' => 'prisutnostType'])->one();
        if ($postavka->vrijednost == 'advanced') {
            return $this->redirect(['zaposlenici-advanced', 'tjedan' => $tjedan]);
        } else {
            return $this->redirect(['zaposlenici-simple', 'tjedan' => $tjedan]);
        }
    }
    public function actionZaposleniciAdvanced($tjedan = 0)
    {
        $datumi = $this->getDatum($tjedan);
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->all();
        return $this->render('zaposlenici-advanced', ['zaposlenici' => $zaposlenici, 'datumi' => $datumi]);
    }
    public function actionZaposleniciSimple($tjedan = 0)
    {
        $datumi = $this->getDatum($tjedan);
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->all();
        return $this->render('zaposlenici-simple', ['zaposlenici' => $zaposlenici, 'datumi' => $datumi]);
    }
    public function actionIzvjesce()
    {
        $datumi = $this->getDatum(0);
        $godina = PedagoskaGodina::getAktivna();
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->all();
        $skupine = Skupina::find()->where(['ped_godina' => $godina->id])->all();
        $prisutnost = Prisutnost::find()->select('count(status) as counters, datum')->groupBy('datum')->where(['entitet' => 'dijete', 'status' => 1])->createCommand()->queryAll();
        //         SELECT CAST(datum AS DATE) [Date], 
        //    Count(1)  [prisutnost Count]   
        // FROM prisutnost
        // WHERE entitet='dijete' AND status=1
        // GROUP BY CAST(datum AS DATE)
        return $this->render('izvjesce', ['zaposlenici' => $zaposlenici, 'datumi' => $datumi, 'skupine' => $skupine, 'prisutnost' => $prisutnost]);
    }
    public function actionDodajZaposlenik()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset($_POST['id'], $_POST['datum'])) {
            $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $_POST['id'], 'datum' => $_POST['datum']]);
            if (empty($prisutnost)) {
                $prisutnost = new Prisutnost();
            }
            if (isset($_POST['prisutnost_type']) && $_POST['prisutnost_type'] == 'advanced') {
                if ($_POST['razlog'] && $_POST['razlog'] == 'rad') {
                    if (empty($_POST['begin_time']) || empty($_POST['end_time'])) {
                        return [
                            'message' => Yii::t('app', 'Neispravan unos, vrijeme početka i kraja ne smiju biti prazni!!!'),
                            'status' => 'error',
                        ];
                    }
                    if (strtotime($_POST['end_time']) - strtotime($_POST['begin_time']) < 0) {
                        return [
                            'message' => Yii::t('app', 'Neispravan unos, vrijeme početka ne može biti manje od vremena kraja!!!'),
                            'status' => 'error',
                        ];
                    }
                }
                $prisutnost->datum = $_POST['datum'];
                $prisutnost->entitet_id = $_POST['id'];
                $prisutnost->entitet = 'zaposlenik';
                $prisutnost->pocetak = $_POST['begin_time'];
                $prisutnost->kraj = $_POST['end_time'];
                if ($_POST['razlog'] && $_POST['razlog'] != 'rad') {
                    $prisutnost->razlog_izostanka = $_POST['razlog'];
                    $prisutnost->pocetak = '08:00';
                    $prisutnost->kraj = '16:00';
                    $prisutnost->status = 0;

                    //Unos u dnevnik_zapis ako nije bilo rada
                    $godina = PedagoskaGodina::getAktivna();
                    $vrstaDnevnika = VrstaDnevnik::find()->where(['naziv' => 'Dnevnik zapisa'])->one();
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $_POST['id'], 'vrsta_dnevnika' => $vrstaDnevnika->id, 'ped_godina' => $godina->id])->one();
                    $dnevnik_zapis = new DnevnikZapis();
                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $_POST['id'];
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $_POST['id'];
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();
                        $dnevnik_zapis->dnevnik = $novi_dnevnik->id;
                    } else {
                        $dnevnik_zapis->dnevnik = $dnevnik->id;
                    }
                    $vrsta_zapis = \common\models\VrstaZapis::find()->where(['naziv' => 'grupa zapis'])->one();
                    $dnevnik_zapis->vrsta = $vrsta_zapis->id;
                    switch ($_POST['razlog']) {
                        case 'godisnji':
                            $dnevnik_zapis->zapis = 'Godišnji';
                            $dnevnik_zapis->vrijednosti_atributa = ['godisnji' => ['pocetak' => $_POST['datum'], 'kraj' => $_POST['datum']]];
                            break;
                        case 'bolovanje':
                            $dnevnik_zapis->zapis = 'Bolovanje';
                            $dnevnik_zapis->vrijednosti_atributa = ['bolovanje' => ['pocetak' => $_POST['datum'], 'kraj' => $_POST['datum']]];
                            break;
                        case 'slobodni_dan':
                        case 'neradno':
                            $dnevnik_zapis->zapis = 'Slobodno';
                            $dnevnik_zapis->vrijednosti_atributa = ['slobodno' => ['pocetak' => $_POST['datum'], 'kraj' => $_POST['datum']]];
                            break;
                        case 'porodiljni':
                            $dnevnik_zapis->zapis = 'Porodiljni';
                            $dnevnik_zapis->vrijednosti_atributa = ['porodiljni' => ['pocetak' => $_POST['datum'], 'kraj' => $_POST['datum']]];
                            break;
                        case 'placeni_dopust':
                            $dnevnik_zapis->zapis = 'Dopust';
                            $dnevnik_zapis->vrijednosti_atributa = ['dopust' => ['pocetak' => $_POST['datum'], 'kraj' => $_POST['datum']]];
                            break;
                    }
                    $dnevnik_zapis->save();
                } else {
                    $prisutnost->status = 1;
                }
                $prisutnost->trajanje = (strtotime($_POST['end_time']) - strtotime($_POST['begin_time'])) / 3600;
                $prisutnost->save();
            }
            if (isset($_POST['prisutnost_type']) && $_POST['prisutnost_type'] == 'simple') {
                $prisutnost->datum = $_POST['datum'];
                $prisutnost->entitet_id = $_POST['id'];
                $prisutnost->entitet = 'zaposlenik';
                $prisutnost->pocetak = '08:00';
                if ($_POST['razlog'] && $_POST['razlog'] == 'rad_4h') {
                    $prisutnost->kraj = '12:00';
                } else {
                    $prisutnost->kraj = '16:00';
                }
                if ($_POST['razlog'] && ($_POST['razlog'] == 'rad_8h' || $_POST['razlog'] == 'rad_4h')) {
                    $prisutnost->razlog_izostanka = null;
                    $prisutnost->status = 1;
                } else {
                    $prisutnost->razlog_izostanka = $_POST['razlog'];
                    $prisutnost->status = 0;

                    //Unos u dnevnik_zapis ako nije bilo rada
                    $godina = PedagoskaGodina::getAktivna();
                    $vrstaDnevnika = VrstaDnevnik::find()->where(['naziv' => 'Dnevnik zapisa'])->one();
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $_POST['id'], 'vrsta_dnevnika' => $vrstaDnevnika->id, 'ped_godina' => $godina->id])->one();
                    $dnevnik_zapis = new DnevnikZapis();
                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $_POST['id'];
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $_POST['id'];
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();
                        $dnevnik_zapis->dnevnik = $novi_dnevnik->id;
                    } else {
                        $dnevnik_zapis->dnevnik = $dnevnik->id;
                    }
                    $vrsta_zapis = \common\models\VrstaZapis::find()->where(['naziv' => 'grupa zapis'])->one();
                    $dnevnik_zapis->vrsta = $vrsta_zapis->id;
                    switch ($_POST['razlog']) {
                        case 'godisnji':
                            $dnevnik_zapis->zapis = 'Godišnji';
                            $dnevnik_zapis->vrijednosti_atributa = ['godisnji' => ['pocetak' => Yii::$app->formatter->asDate('now', 'php:d.m.Y'), 'kraj' => Yii::$app->formatter->asDate('now', 'php:d.m.Y')]];
                            break;
                        case 'bolovanje':
                            $dnevnik_zapis->zapis = 'Bolovanje';
                            $dnevnik_zapis->vrijednosti_atributa = ['bolovanje' => ['pocetak' => Yii::$app->formatter->asDate('now', 'php:d.m.Y'), 'kraj' => Yii::$app->formatter->asDate('now', 'php:d.m.Y')]];
                            break;
                        case 'slobodni_dan':
                        case 'neradno':
                            $dnevnik_zapis->zapis = 'Slobodno';
                            $dnevnik_zapis->vrijednosti_atributa = ['slobodno' => ['pocetak' => Yii::$app->formatter->asDate('now', 'php:d.m.Y'), 'kraj' => Yii::$app->formatter->asDate('now', 'php:d.m.Y')]];
                            break;
                        case 'porodiljni':
                            $dnevnik_zapis->zapis = 'Porodiljni';
                            $dnevnik_zapis->vrijednosti_atributa = ['porodiljni' => ['pocetak' => Yii::$app->formatter->asDate('now', 'php:d.m.Y'), 'kraj' => Yii::$app->formatter->asDate('now', 'php:d.m.Y')]];
                            break;
                        case 'placeni_dopust':
                            $dnevnik_zapis->zapis = 'Dopust';
                            $dnevnik_zapis->vrijednosti_atributa = ['dopust' => ['pocetak' => Yii::$app->formatter->asDate('now', 'php:d.m.Y'), 'kraj' => Yii::$app->formatter->asDate('now', 'php:d.m.Y')]];
                            break;
                    }
                    $dnevnik_zapis->save();
                }
                $prisutnost->trajanje = (strtotime($prisutnost->kraj) - strtotime($prisutnost->pocetak)) / 3600;
                $prisutnost->save();
            }
            if ($prisutnost->save()) {
                $htmlRespnse = $this->renderPartial('zaposlenici-prisutnost-summary', ['prisutnost' => $prisutnost]);
                return [
                    'message' => Yii::t('app', 'Uspješno unesena prisutnost'),
                    'status' => 'success',
                    'value' => $htmlRespnse,
                ];
            }
        }
    }
    public function actionOpravdajDijete()
    {
        if (isset($_POST['id'], $_POST['razlog'], $_POST['datum'])) {
            $prisutnost = Prisutnost::find()->where(['entitet' => 'dijete', 'entitet_id' => $_POST['id'], 'datum' => $_POST['datum']])->one();
            if (empty($prisutnost)) {
                $prisutnost = new Prisutnost();
                $prisutnost->datum = $_POST['datum'];
                $prisutnost->entitet = 'dijete';
                $prisutnost->entitet_id = $_POST['id'];
                $prisutnost->status = 0;
            }
            $prisutnost->razlog_izostanka = $_POST['razlog'];
            $prisutnost->save(false);
        }
        $godina = PedagoskaGodina::getAktivna();
        $skupina = Skupina::find()->leftJoin('dijete_skupina', 'dijete_skupina.skupina = skupina.id')->where(['dijete_skupina.dijete' => $_POST['id']])->andWhere(['skupina.ped_godina' => $godina->id])->one(); //DijeteSkupina::find()->where(['dijete' => $_POST['id']])->one();
        $this->redirect(['/prisutnost/razlog-izostanka', 'id' => $skupina->id, 'tjedan' => $_POST['tjedan'], '#' => 'dijete' . $_POST['id']]);
    }
    public function actionRazlogIzostanka($id, $tjedan)
    {
        $datum = $this->getDatum($tjedan);
        $status_upisan = StatusDjeteta::find()->where(['status' => 'upisan'])->one();
        $djeca = Dijete::find()->leftJoin('dijete_skupina', 'dijete_skupina.dijete = dijete.id')->where(['dijete_skupina.skupina' => $id])->andWhere(['status' => $status_upisan->id])->orderBy(['prezime' => SORT_ASC])->all();
        $skupina = Skupina::findOne($id);
        return $this->render('djeca-razlog-izostanka', ['djeca' => $djeca, 'skupina' => $skupina, 'datumi' => $datum, 'tjedan' => $tjedan]);
    }


    private function getDatum($tjedan)
    {
        $datum['pon'] = date('Y-m-d', strtotime('monday this week'));
        $datum['uto'] = date('Y-m-d', strtotime('tuesday this week'));
        $datum['sri'] = date('Y-m-d', strtotime('wednesday this week'));
        $datum['cet'] = date('Y-m-d', strtotime('thursday this week'));
        $datum['pet'] = date('Y-m-d', strtotime('friday this week'));
        if ($tjedan == '0') {
            return $datum;
        } else {
            $datum['pon'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pon'])));
            $datum['uto'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['uto'])));
            $datum['sri'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['sri'])));
            $datum['cet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['cet'])));
            $datum['pet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pet'])));
        }
        return $datum;
    }
    public function actionUpdateZaposlenikAdvanced()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $_POST['id'], 'datum' => $_POST['datum']]);
        $htmlResponse = $this->renderPartial('zaposlenik-edit-forma-advanced', [
            'prisutnost' => $prisutnost
        ]);
        return [
            'value' => $htmlResponse
        ];
    }
    public function actionUpdateZaposlenikSimple()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $_POST['id'], 'datum' => $_POST['datum']]);
        $htmlResponse = $this->renderPartial('zaposlenik-edit-forma-simple', [
            'prisutnost' => $prisutnost
        ]);
        return [
            'value' => $htmlResponse
        ];
    }
    public function actionDjecaPdf($skupina, $tjedan)
    {
        $datum = $this->getDatum($tjedan);
        $skupina = Skupina::findOne(['id' => $skupina]);
        $djeca = Dijete::find()->leftJoin('dijete_skupina', 'dijete_skupina.dijete = dijete.id')->where(['dijete_skupina.skupina' => $skupina->id])->orderBy(['prezime' => SORT_ASC])->all();
        $htmlContent = $this->renderPartial('@common/views/templates/prisutnostDjecaPdf', [
            'datumi' => $datum,
            'djeca' => $djeca,
            'skupina' => $skupina
        ]);
        $naziv = 'Prisutnost-' . $skupina->naziv . '-' . Yii::$app->formatter->asDate($datum['pon'], 'php:j.n.Y.') . '-' . Yii::$app->formatter->asDate($datum['pet'], 'php:j.n.Y.');
        Utils::ispisPDF($htmlContent, $naziv, 'download', 'P');
    }
    public function actionZaposleniciPdf()
    {
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->all();
        $datum = $this->getDatum(0);
        $htmlContent = $this->renderPartial('@common/views/templates/prisutnostZaposleniciPdf', ['zaposlenici' => $zaposlenici, 'datumi' => $datum]);
        $naziv = 'Prisutnost zaposlenika za ' . Yii::$app->formatter->asDate($datum['pon'], 'php:j.n.Y.') . '-' . Yii::$app->formatter->asDate($datum['pet'], 'php:j.n.Y.');
        Utils::ispisPDF($htmlContent, $naziv, 'download', 'P');
    }
    public function actionZaposleniciPdfMjesecni()
    {
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all();
        $mjesec = Utils::getMjesecPed($_POST['mjesec']);
        $numberDays = cal_days_in_month(CAL_GREGORIAN, $mjesec['mjesec'], date('Y'));
        $poc = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-01';
        $kraj = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $numberDays;

        $header = ['Ime i prezime'];
        for ($i = 1; $i < $numberDays + 1; $i++) {
            $dan = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $i;
            if (date('N', strtotime($dan)) >= 6) {
                $temp = '<span style="color:#eb4b50">' . $i . '</span>';
            } else {
                $temp = $i;
            }
            $header = array_merge($header, [$temp]);
        }
        $retci = [];
        $razlog = [
            'godisnji' => 'G',
            'placeni_dopust' => 'PD',
            'bolovanje' => 'B',
            'slobodni_dan' => 'S',
            'porodiljni' => 'P',
            'neradno' => 'BL',
            'ostalo' => 'O'
        ];
        foreach ($zaposlenici as $zaposlenik) {
            $zaNaziv = $zaposlenik->ime . ' ' . $zaposlenik->prezime;
            $temp = [];
            $temp[0] = '<span style="font-weight: bold;text-align:left" class="left">' . $zaNaziv . '</span>';
            $prisutnost = $zaposlenik->getPrisutnost()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet' => 'zaposlenik'])->orderBy('datum ASC')->all(); //Prisutnost::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet_id' => $zaposlenik->id, 'entitet' => 'zaposlenik'])->orderBy('datum ASC')->all();
            if (!empty($prisutnost)) {
                foreach ($prisutnost as $prist) {
                    $dan = date("j", strtotime($prist->datum));
                    if ($prist->status == 1) {
                        $tip = \common\models\Postavke::find()->where(['postavka' => 'prisutnostType'])->one();
                        if ($tip->vrijednost == 'advanced') {
                            $start_pris = new DateTime($prist->pocetak);
                            $kraj_pris = new DateTime($prist->kraj);
                            $temp[$dan] = $kraj_pris->diff($start_pris); //round($prist->trajanje, 0);
                        } else {
                            $temp[$dan] = round($prist->trajanje, 0);
                        }
                    } else {
                        $temp[$dan] = '<span style="color:red">' . $razlog[$prist->razlog_izostanka] . '</span>';
                    }
                }
                /*		for($j=count($temp);$j<=$numberDays;$j){
                    $temp[]='';
                }*/
                $retci[] = $temp;
            }
        }
        $prisutnostFull = array_merge([$header], $retci);
        $htmlContent = $this->renderPartial('@common/views/templates/prisutnostZaposleniciPdfMjesecni', ['prisutnost' => $prisutnostFull, 'mjesec' => $mjesec]);
        $naziv = 'Zaposlenici-' . $mjesec['mjesec'] . '/' . $mjesec['godina'] . '';
        Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
    }
    public function actionDjecaPdfMjesecni()
    {
        $view_type = $_POST['view_type'];

        if ($view_type == 'simple') {
            $skupina_id = $_POST['skupine'];
            $skupina = '';
            $djeca = null;
            if ($skupina_id == 0) {
                $skupina = 'Sve skupine';
                $djeca = Dijete::find()->orderBy('prezime,ime ASC')->all();
            } else {
                $sk = Skupina::find()->where(['id' => $skupina_id])->one();
                $skupina = $sk->naziv;
                $djeca_ids = DijeteSkupina::find()->select('dijete')->where(['skupina' => $skupina_id])->asArray()->all();
                $djeca = Dijete::find()->where(['in', 'id', array_column($djeca_ids, 'dijete')])->andWhere(['status' => 2])->orderBy('prezime,ime ASC')->all();
            }
            //print("<pre>".print_r($djeca,true)."</pre>");die(); 
            $mjesec = Utils::getMjesecPed($_POST['mjesec']);
            $numberDays = cal_days_in_month(CAL_GREGORIAN, $mjesec['mjesec'], date('Y'));
            $poc = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-01';
            $kraj = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $numberDays;

            $retci = [];

            foreach ($djeca as $dijete) {
                $temp = [];
                $zaNaziv = $dijete->ime . ' ' . $dijete->prezime;
                $temp[0] = '<span style="font-weight: bold">' . $zaNaziv . '</span>';
                $prisutnost = Prisutnost::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet_id' => $dijete->id, 'entitet' => 'dijete'])->orderBy('datum ASC')->all();
                $dan = 0;
                if (!empty($prisutnost)) {
                    foreach ($prisutnost as $prist) {
                        if ($prist->status == 1) {
                            $dan++;
                        }
                    }
                    $temp[1] = $dan;
                    $retci[] = $temp;
                }
            }
            $prisutnostFull = array_merge($retci);
            $htmlContent = $this->renderPartial('@common/views/templates/prisutnostDjecaPdfMjesecni', ['prisutnost' => $prisutnostFull, 'mjesec' => $mjesec, 'skupina' => $skupina]);
            $naziv = 'Djeca-' . $mjesec['mjesec'] . '/' . $mjesec['godina'] . '';
            Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
        } elseif ($view_type == 'advanced') {

            $skupina_id = $_POST['skupine'];
            $skupina = '';
            $djeca = null;
            if ($skupina_id == 0) {
                $skupina = 'Sve skupine';
                $djeca = Dijete::find()->where(['status' => 2])->orderBy('prezime,ime ASC')->all();
            } else {
                $sk = Skupina::find()->where(['id' => $skupina_id])->one();
                $skupina = $sk->naziv;
                $djeca_ids = DijeteSkupina::find()->select('dijete')->where(['skupina' => $skupina_id])->asArray()->all();
                $djeca = Dijete::find()->where(['in', 'id', array_column($djeca_ids, "dijete")])->andWhere(['status' => 2])->orderBy('prezime,ime ASC')->all();
            }

            $mjesec = Utils::getMjesecPed($_POST['mjesec']);
            $numberDays = cal_days_in_month(CAL_GREGORIAN, $mjesec['mjesec'], date('Y'));
            $poc = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-01';
            $kraj = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $numberDays;
            $header = ['Ime i prezime'];
            $ukupnoDani = [];
            for ($i = 1; $i < $numberDays + 1; $i++) {
                $dan = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $i;
                if (date('N', strtotime($dan)) >= 6) {
                    $temp = '<span style="color:#eb4b50">' . $i . '</span>';
                } else {
                    $temp = $i;
                }
                $ukupnoDani[$i] = 0;
                $header = array_merge($header, [$temp]);
            }

            $retci = [];

            foreach ($djeca as $dijete) {
                $zaNaziv = $dijete->ime . ' ' . $dijete->prezime;
                $temp = [];
                $temp[0] = '<span style="font-weight: bold;text-align:left" class="left">' . $zaNaziv . '</span>';
                $prisutnost = Prisutnost::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet_id' => $dijete->id, 'entitet' => 'dijete'])->orderBy('datum ASC')->all();
                foreach ($prisutnost as $prist) {
                    $dan = date("j", strtotime($prist->datum));
                    if ($prist->status == 1) {
                        $temp[$dan] = '+';
                        if (isset($ukupnoDani[$dan])) {
                            $ukupnoDani[$dan]++;
                        } else {
                            $ukupnoDani[$dan] = 1;
                        }
                    } else {
                        if (isset($prist->razlog_izostanka)) {
                            switch ($prist->razlog_izostanka) {
                                case 'godisnji':
                                    $temp[$dan] = '<span style="color:red">G</span>';
                                    break;
                                case 'zdravstveni':
                                    $temp[$dan] = '<span style="color:red">-</span>';
                                    break;
                                case 'osobni':
                                    $temp[$dan] = '<span style="color:red">-</span>';
                                    break;
                                case 'blagdan':
                                    $temp[$dan] = '<span style="color:red">P</span>';
                                    break;
                                default:
                                    $temp[$dan] = '<span style="color:red">-</span>';
                                    break;
                            }
                        }
                    }
                }

                $retci[] = $temp;
            }
            $prisutnostFull = array_merge([$header], $retci);
            $htmlContent = $this->renderPartial('@common/views/templates/prisutnostDjecaPdfMjesecniAdvanced', ['prisutnost' => $prisutnostFull, 'mjesec' => $mjesec, 'skupina' => $skupina, 'ukupno' => $ukupnoDani]);
            $naziv = 'Djeca-Advanced-' . $mjesec['mjesec'] . '/' . $mjesec['godina'] . '';
            Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
        }
    }
    public function actionDjecaExcel($skupina, $tjedan)
    {
        $datum = $this->getDatum($tjedan);
        $skupina = Skupina::findOne(['id' => $skupina]);
        $djeca = Dijete::find()->leftJoin('dijete_skupina', 'dijete_skupina.dijete = dijete.id')->where(['dijete_skupina.skupina' => $skupina->id, 'status' => 2])->orderBy(['prezime' => SORT_ASC])->all();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $rowCount = 1;
        $sheet->setCellValue('A1', Yii::t('app', 'Dijete'));
        $sheet->setCellValue('B1', Yii::t('app', 'Ponedjeljak' . PHP_EOL . Yii::$app->formatter->asDate($datum['pon'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('B1')->getAlignment()->setWrapText(true);
        $sheet->setCellValue('C1', Yii::t('app', 'Utorak' . PHP_EOL . Yii::$app->formatter->asDate($datum['uto'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('C1')->getAlignment()->setWrapText(true);
        $sheet->setCellValue('D1', Yii::t('app', 'Srijeda' . PHP_EOL . Yii::$app->formatter->asDate($datum['sri'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
        $sheet->setCellValue('E1', Yii::t('app', 'Četvrtak' . PHP_EOL . Yii::$app->formatter->asDate($datum['cet'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('E1')->getAlignment()->setWrapText(true);
        $sheet->setCellValue('F1', Yii::t('app', 'Petak' . PHP_EOL . Yii::$app->formatter->asDate($datum['pet'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('F1')->getAlignment()->setWrapText(true);


        $rowCount++;
        foreach ($djeca as $item) {
            $columnPointer = 'B';
            $sheet->setCellValue('A' . $rowCount, $item->ime . ' ' . $item->prezime);
            foreach ($datum as $dat) {
                $dodatak = '';
                $prisutnost = $item->getPrisutnost()->where(['datum' => $dat])->one();
                if (isset($prisutnost)) {
                    if ($prisutnost->status == 0) {

                        if ($prisutnost->razlog_izostanka != null) {
                            $dodatak = Yii::t('app', 'Razlog izostanka: ') . $prisutnost->razlog_izostanka;
                        }
                        $dijete_prisutno = false;
                    } else {
                        $dijete_prisutno = true;
                    }
                } else {
                    $dijete_prisutno = false;
                }
                $cellContent = '';
                if ($dijete_prisutno) {
                    $cellContent = Yii::t('app', 'Da');
                } else {
                    if (isset($dodatak)) {
                        $cellContent = Yii::t('app', 'Ne') . "\n" . $dodatak;
                    } else {
                        $cellContent = Yii::t('app', 'Ne');
                    }
                }
                $sheet->setCellValue($columnPointer . $rowCount, $cellContent);
                $spreadsheet->getActiveSheet()->getStyle($columnPointer . $rowCount)->getAlignment()->setWrapText(true);

                $columnPointer++;
            }
            $rowCount++;
        }

        $writer = new Xlsx($spreadsheet);
        $naziv = 'Prisutnost-' . $skupina->naziv . '-' . Yii::$app->formatter->asDate($datum['pon'], 'php:mdy') . '-' . Yii::$app->formatter->asDate($datum['pet'], 'php:mdY');
        if (!file_exists(Yii::getAlias('@backend/web/prisutnost'))) {
            mkdir(Yii::getAlias('@backend/web/prisutnost'));
        }
        $writer->save('prisutnost/' . $naziv . '.xlsx');
        Yii::$app->response->sendFile('prisutnost/' . $naziv . '.xlsx');
    }
    public function actionZaposleniciExcel()
    {
        $datumi = $this->getDatum(0);
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->all();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $rowCount = 1;
        $sheet->setCellValue('A1', Yii::t('app', 'Zaposlenik'));
        $sheet->setCellValue('B1', Yii::t('app', 'Ponedjeljak' . "\n" . Yii::$app->formatter->asDate($datumi['pon'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('B1')->getAlignment()->setWrapText(true);
        $sheet->setCellValue('C1', Yii::t('app', 'Utorak' . PHP_EOL . Yii::$app->formatter->asDate($datumi['uto'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('C1')->getAlignment()->setWrapText(true);
        $sheet->setCellValue('D1', Yii::t('app', 'Srijeda' . PHP_EOL . Yii::$app->formatter->asDate($datumi['sri'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
        $sheet->setCellValue('E1', Yii::t('app', 'Četvrtak' . PHP_EOL . Yii::$app->formatter->asDate($datumi['cet'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('E1')->getAlignment()->setWrapText(true);
        $sheet->setCellValue('F1', Yii::t('app', 'Petak' . PHP_EOL . Yii::$app->formatter->asDate($datumi['pet'], 'php:j.n.Y.')));
        $spreadsheet->getActiveSheet()->getStyle('F1')->getAlignment()->setWrapText(true);
        $rowCount++;
        foreach ($zaposlenici as $zaposlenik) {
            $columnPointer = 'B';
            $sheet->setCellValue('A' . $rowCount, $zaposlenik->ime . ' ' . $zaposlenik->prezime);
            foreach ($datumi as $datum) {
                $content = '';
                $prisutnost = $zaposlenik->getPrisutnost()->where(['datum' => $datum])->one();
                if (isset($prisutnost)) {
                    $content .=  Yii::t('app', 'Vrijeme početka: ') . $prisutnost->pocetak . "\n";
                    $content .= Yii::t('app', 'Vrijeme kraja: ') . $prisutnost->kraj . "\n";
                    $start = new DateTime($prisutnost->pocetak);
                    $kraj = new DateTime($prisutnost->kraj);
                    $razlika = $kraj->diff($start);
                    $content .= Yii::t('app', 'Ukupno: ') . $razlika->format('%hh %imin');
                } else {
                    $content = Yii::t('app', 'Ne');
                }
                $sheet->setCellValue($columnPointer . $rowCount, $content);
                $spreadsheet->getActiveSheet()->getStyle($columnPointer . $rowCount)->getAlignment()->setWrapText(true);
                $columnPointer++;
            }
            $rowCount++;
        }
        $writer = new Xlsx($spreadsheet);
        if (!file_exists(Yii::getAlias('@backend/web/prisutnost'))) {
            mkdir(Yii::getAlias('@backend/web/prisutnost'));
        }
        $naziv = 'Prisutnost-zaposlenika-' . Yii::$app->formatter->asDate($datumi['pon'], 'php:mdy') . '-' . Yii::$app->formatter->asDate($datumi['pet'], 'php:mdY');
        $writer->save('prisutnost/' . $naziv . '.xlsx');
        Yii::$app->response->sendFile('prisutnost/' . $naziv . '.xlsx');
    }
    public function actionZaposleniciPdfGodisnji()
    {
        // pedagoška godina početak i kraj
        $pedAkt = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $pedPoc1 = $pedAkt->od;
        $pedPoc = date("Y-m-d", strtotime($pedPoc1));
        $pedKraj1 = $pedAkt->do;
        $pedKraj = date("Y-m-d", strtotime($pedKraj1));
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all();
        // početak i kraj mjeseca iz forme
        $mjesec = Utils::getMjesecPed($_POST['mjesec']);
        $numberDays = cal_days_in_month(CAL_GREGORIAN, $mjesec['mjesec'], date('Y'));
        $poc = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-01';
        $kraj = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $numberDays;
        // početak tablice
        $header = ['Ime i prezime', 'Sati', 'Godišnji'];
        $retci = [];
        $ukupnoSati = 0;
        $ukupnoGodisnji = 0;

        foreach ($zaposlenici as $zaposlenik) {
            $zaNaziv = $zaposlenik->ime . ' ' . $zaposlenik->prezime;
            $temp = [];
            $temp[0] = '<span style="text-align:left" class="left">' . $zaNaziv . '</span>';
            $prisutnostGodisnji = $zaposlenik->getPrisutnost()->where(['between', 'datum', $pedPoc, $pedKraj])->andWhere(['entitet' => 'zaposlenik'])->all(); //Prisutnost::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet_id' => $zaposlenik->id, 'entitet' => 'zaposlenik'])->orderBy('datum ASC')->all();
            $prisutnostSati = $zaposlenik->getPrisutnost()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet' => 'zaposlenik'])->all(); //Prisutnost::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet_id' => $zaposlenik->id, 'entitet' => 'zaposlenik'])->orderBy('datum ASC')->all();
            $godisnjiCount = 0;
            $sati = 0;
            foreach ($prisutnostGodisnji as $prist) {
                $dan = date("j", strtotime($prist->datum));
                if ($prist->status == 0 && $prist->razlog_izostanka == 'godisnji') {
                    $godisnjiCount++;
                    $ukupnoGodisnji++;
                }
            }
            foreach ($prisutnostSati as $pris) {
                if ($pris->status == 1) {
                    $sati += $pris->trajanje;
                    $ukupnoSati += $pris->trajanje;
                }
            }
            $temp[1] = '<span style="color:black">' . round($sati, 2) . '</span>';
            $temp[2] = '<span style="color:green">' . $godisnjiCount . '</span>';
            /*		for($j=count($temp);$j<=$numberDays;$j){
				$temp[]='';
			}*/

            $retci[] = $temp;
        }
        $prisutnostFull = array_merge([$header], $retci);
        $htmlContent = $this->renderPartial('@common/views/templates/prisutnostZaposleniciPdfGodisnji', ['prisutnost' => $prisutnostFull, 'ukupnoSati' => round($ukupnoSati, 2), 'ukupnoGodisnji' => round($ukupnoGodisnji, 2), 'mjesec' => date("m")]);
        $naziv = 'Zaposlenici-Godisnji-' . date("m") . '/' . date("Y") . '';
        Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
    }
    public function actionZaposleniciPojediniPdfMjesecni()
    {
        $godina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $zaposlenik = Zaposlenik::find()->where(['!=', 'status', 3])->andWhere(['id' => $_POST['zaposlenici']])->one();
        $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => 2])->andWhere(['ped_godina' => $godina->id])->one();
        $zapisi = \common\models\DnevnikZapis::find()->where(['vrsta' => 28, 'dnevnik' => $dnevnik->id])->all();
        $mjeseci = Utils::mjeseciPedGod();
        $poc = date('Y-m-d', strtotime($godina->od));
        $kraj = date('Y-m-d', strtotime($godina->do));
        $temp = [];
        $header = ['Mjesec', 'Efektiva', 'Nefektiva', 'Godišnji'];

        $retci = [];


        $zaNaziv = $zaposlenik->ime . ' ' . $zaposlenik->prezime;




        foreach ($mjeseci as $mjesec) {
            $efektiva = 0;
            $neefektiva = 0;
            $godisnji = 0;
            foreach ($zapisi as $zapis) {

                $m = (int)substr($mjesec, 0, 2);
                $mj = date('F', strtotime('01.' . $m . '.2022.'));
                $temp[0] = '<span >' . $mj . '</span>';

                switch (key($zapis->vrijednosti_atributa)) {
                    case 'vanustanove':
                        if (date('F',  strtotime($zapis->vrijednosti_atributa['vanustanove']['end'])) == $mj && date('F',  strtotime($zapis->vrijednosti_atributa['vanustanove']['start'])) == $mj) {
                            $trajanje = strtotime($zapis->vrijednosti_atributa['vanustanove']['end']) - strtotime($zapis->vrijednosti_atributa['vanustanove']['start']);
                            $neefektiva += ($trajanje / 3600);
                        }
                        break;
                    case 'posredni':
                        if (date('F',  strtotime($zapis->vrijednosti_atributa['posredni']['end'])) == $mj && date('F',  strtotime($zapis->vrijednosti_atributa['posredni']['start'])) == $mj) {
                            $trajanje =  strtotime($zapis->vrijednosti_atributa['posredni']['end']) -  strtotime($zapis->vrijednosti_atributa['posredni']['start']);
                            $neefektiva += ($trajanje / 3600);
                        }
                        break;
                    case 'neposredni':
                        if (date('F',  strtotime($zapis->vrijednosti_atributa['neposredni']['end'])) == $mj && date('F',  strtotime($zapis->vrijednosti_atributa['neposredni']['start'])) == $mj) {
                            $trajanje =  strtotime($zapis->vrijednosti_atributa['neposredni']['end']) -  strtotime($zapis->vrijednosti_atributa['neposredni']['start']);
                            $efektiva += ($trajanje / 3600);
                        }
                        break;

                    case 'godisnji':
                        if (date('M',  strtotime($zapis->vrijednosti_atributa['godisnji']['end'])) == $mj && date('M',  strtotime($zapis->vrijednosti_atributa['godisnji']['start'])) == $mj) {
                            $trajanje =  strtotime($zapis->vrijednosti_atributa['godisnji']['end']) -  strtotime($zapis->vrijednosti_atributa['godisnji']['start']);
                            $godisnji += ($trajanje / 3600);
                        }
                        break;
                }
            }
            $temp[1] = $efektiva;
            $temp[2] = $neefektiva;
            $temp[3] = $godisnji;
            $retci[] = $temp;
        }



        /*		for($j=count($temp);$j<=$numberDays;$j){
				$temp[]='';
			}*/


        $prisutnostFull = array_merge([$header], $retci);
        $htmlContent = $this->renderPartial('@common/views/templates/zaposleniciPojediniPdfMjesecni', ['prisutnost' => $prisutnostFull, 'zaNaziv' => $zaNaziv]);
        $naziv = 'Zaposlenik-' . $zaNaziv . '';
        Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
    }
    public function actionZaposleniciEfektivaPdfMjesecni()
    {
        $godina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $zaposlenik = Zaposlenik::find()->where(['!=', 'status', 3])->andWhere(['id' => $_POST['zaposlenici']])->one();
        $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => 2])->andWhere(['ped_godina' => $godina->id])->one();
        $zapisi = \common\models\DnevnikZapis::find()->where(['vrsta' => 28, 'dnevnik' => $dnevnik->id])->all();
        // početak i kraj mjeseca iz forme
        $mjesec = Utils::getMjesecPed($_POST['mjesec']);
        $numberDays = cal_days_in_month(CAL_GREGORIAN, $mjesec['mjesec'], date('Y'));
        // priprema za tablicu
        $temp = [];
        $header = ['#', 'Dolazak', 'Odlazak', 'Efektiva', 'Ukupno'];
        $retci = [];
        $zaNaziv = $zaposlenik->ime . ' ' . $zaposlenik->prezime;
        $ukupnoE = 0;
        $ukupnoN = 0;
        // za svaki dan u mjesecu
        for ($dan = 1; $dan <= $numberDays; $dan++) {
            $efektiva = 0;
            $dolazak = 0;
            $odlazak = 0;
            // za svaki zapis u tom danu
            foreach ($zapisi as $zapis) {
                $temp[0] = '<span >' . $dan . '</span>';
                switch (key($zapis->vrijednosti_atributa)) {
                    case 'neposredni':
                        if (date('d.m.Y',  strtotime($zapis->vrijednosti_atributa['neposredni']['end'])) == $dan . '.' . $mjesec['mjesec'] . '.' . $mjesec['godina'] && date('d.m.Y',  strtotime($zapis->vrijednosti_atributa['neposredni']['start'])) == $dan . '.' . $mjesec['mjesec'] . '.' . $mjesec['godina']) {
                            $trajanje =  strtotime($zapis->vrijednosti_atributa['neposredni']['end']) -  strtotime($zapis->vrijednosti_atributa['neposredni']['start']);
                            $efektiva += round(($trajanje / 3600), 2);
                            $ukupnoE += $efektiva;
                            $ukupnoN += $efektiva + 2.5;
                            $dolazak = date('H:i',  strtotime($zapis->vrijednosti_atributa['neposredni']['start']));
                            $odlazak = date('H:i',  strtotime($zapis->vrijednosti_atributa['neposredni']['end']));
                        }
                        break;
                }
            }
            // dolazak i odlazak za svaki dan, temp[1] je druga ćelija
            $temp[1] = $dolazak;
            $temp[2] = $odlazak;
            $temp[3] = $efektiva;
            if ($efektiva == 0) {
                $temp[4] = 0;
            } else {
                $temp[4] = $efektiva + 2.5;
            }
            // spajanje ćelija u jedan red
            $retci[] = $temp;
        }
        // spajanje headera i redova
        $prisutnostFull = array_merge([$header], $retci);
        $htmlContent = $this->renderPartial('@common/views/templates/zaposleniciEfektivaPdfMjesecni', ['prisutnost' => $prisutnostFull, 'mjesec' => $mjesec, 'zaNaziv' => $zaNaziv, 'ukupnoE' => round($ukupnoE, 2), 'ukupnoN' => round($ukupnoN, 2)]);
        $naziv = 'Zaposlenik-' . $zaNaziv . '-' . $mjesec['mjesec'];
        Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
    }
}
