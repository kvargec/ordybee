<?php

namespace backend\controllers;

use common\helpers\Utils;
use common\models\Dijete;
use common\models\Notifikacije;
use common\models\Zahtjev;
use Yii;
use common\models\DijeteSkupina;
use common\models\Search\DijeteSkupinaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DijeteSkupinaController implements the CRUD actions for DijeteSkupina model.
 */
class DijeteSkupinaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DijeteSkupina models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DijeteSkupinaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DijeteSkupina model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DijeteSkupina model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DijeteSkupina();

        if ($model->load(Yii::$app->request->post())) {
            $nadjiClana = DijeteSkupina::find()->where(['dijete' => $model->dijete, 'skupina' => $model->skupina])->one();
            if (isset($nadjiClana) && $nadjiClana != null) {
            } else {
                $model->save();
            }
            $dete = Dijete::find()->where(['id' => $model->dijete])->one();
            $zahtjev = Zahtjev::find()->where(['dijete' => $dete->id])->one();
            Utils::sendNotification($zahtjev->user, null, $dete->id, null, "Vaše dijete " . $dete->getFullName() . " je upisano u skupinu " . $model->skupina0->naziv);
            $dete->status = 2;
            $dete->save();
            return $this->redirect(['skupina/view', 'id' => $model->skupina]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DijeteSkupina model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DijeteSkupina model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $idS = $model->skupina;

        $dete = Dijete::find()->where(['id' => $model->dijete])->one();
        $zahtjev = Zahtjev::find()->where(['dijete' => $dete->id])->one();
        if (isset($zahtjev)) {
            if (isset($zahtjev->user)) {
                Utils::sendNotification($zahtjev->user, null, $dete->id, null, "Vaše dijete " . $dete->getFullName() . " je ispisano iz skupine " . $model->skupina0->naziv);
            }
        }
        $model->delete();
        // $dete->status=1;
        $dete->save();
        return $this->redirect(['skupina/view', 'id' => $idS]);
    }

    /**
     * Finds the DijeteSkupina model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DijeteSkupina the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DijeteSkupina::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
