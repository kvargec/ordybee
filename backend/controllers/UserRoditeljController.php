<?php

namespace backend\controllers;

use Yii;
use common\models\UserRoditelj;
use common\models\Roditelj;
use common\models\Dijete;
use common\models\Postavke;
//use common\models\User;
use Da\User\Model\User;
use common\helpers\Utils;
use common\models\Search\UserRoditeljSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserRoditeljController implements the CRUD actions for UserRoditelj model.
 */
class UserRoditeljController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserRoditelj models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserRoditeljSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserRoditelj model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserRoditelj model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserRoditelj();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UserRoditelj model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserRoditelj model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserRoditelj model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserRoditelj the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserRoditelj::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function actionGenerirajUsere()
    {
        set_time_limit(0);
        $roditeljExisting = UserRoditelj::find()->select('roditelj')->asArray()->all();
        $roditeljiToCreate = Roditelj::find()->where(['not in', 'id', array_column($roditeljExisting, 'roditelj')])->orderBy('id ASC')->all();
        $roditeljiToPrint = [];
        foreach ($roditeljiToCreate as $roditelj) {
            $user = new User();
            $ime = Utils::url_slug($roditelj->ime);
            $prezime = Utils::url_slug($roditelj->prezime);
            $username = substr($ime, 0, 2) . '_' . substr($prezime, 0, 10) . '_' . rand(1, 100);
            $email = $roditelj->email;
            if (isset($email) && !empty($email)) {
                $email_in_use = User::find()->where(['email' => $email])->one();
                if ($email_in_use) {
                    $user->email = $username . '@placeholder.com';
                } else {
                    $user->email = $email;
                }
            } else {
                $user->email = $username . '@nema.com';
            }
            $user->username = $username;
            $dijete = Dijete::find()->where(['id' => $roditelj->dijete])->one();
            $user->auth_key = Yii::$app->getSecurity()->generateRandomKey();
            $password = $this->generateRandomString(8);
            $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
            $user->flags = 0;
            $user->created_at = time();
            $user->updated_at = time();
            $user->confirmed_at = time();
            if ($user->save()) {
                $userRoditelj = new UserRoditelj();
                $userRoditelj->user = $user->id;
                $userRoditelj->roditelj = $roditelj->id;
                $userRoditelj->username = $user->username;
                $userRoditelj->password = $password;
                $userRoditelj->save();
                if (isset($dijete) && !empty($dijete)) {
                    $roditeljiToPrint[] = ['username' => $user->username, 'password' => $password, 'ime_roditelja' => $roditelj->prezime . ' ' . $roditelj->ime, 'dijete' => $dijete->prezime . ' ' . $dijete->ime];
                } else {
                    $roditeljiToPrint[] = ['username' => $user->username, 'password' => $password, 'ime_roditelja' => $roditelj->prezime . ' ' . $roditelj->ime];
                }
            }
        }
        $ime_vrtica = Postavke::find()->where(['postavka' => 'nazivVrtica'])->one();
        //$temp_chld = array_column($roditeljiToPrint, 'dijete');
        //array_multisort($temp_chld, SORT_ASC, $roditeljiToPrint);
        $parsed_name = Utils::url_slug($ime_vrtica->vrijednost);
        $naziv = 'RoditeljiUseri-' . $parsed_name;
        $dir = Yii::getAlias('@webroot') . '/useri-roditelji/';
        //$htmlContent = $this->renderPartial('@common/views/templates/roditelji', ['roditelji' => $roditeljiToPrint]);
        //Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $puni_path = $dir . $naziv . '.csv';
        $fp = fopen($puni_path, "a+");
        foreach ($roditeljiToPrint as $item) {
            fputcsv($fp, $item);
        }
        fclose($fp);
        if (!is_file($puni_path)) {
            throw new \yii\web\NotFoundHttpException('The file does not exists.');
        }
        return Yii::$app->response->sendFile($puni_path, basename($puni_path));
    }
}
