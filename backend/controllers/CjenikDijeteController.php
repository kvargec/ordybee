<?php

namespace backend\controllers;

use Yii;
use common\models\CjenikDijete;
use common\models\Search\CjenikDijeteSearch;
use common\models\Cjenik;
use common\models\Skupina;
use common\models\Search\SkupinaSearch;
use common\models\VrstaPrograma;
use common\models\Dijete;
use common\models\DijeteSkupina;
use common\models\PedagoskaGodina;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * CjenikDijeteController implements the CRUD actions for CjenikDijete model.
 */
class CjenikDijeteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CjenikDijete models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CjenikDijeteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $searchModel2 = new SkupinaSearch();
        $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $query = Skupina::find()->where(['ped_godina' => $godina->id]);
        $dataProvider2 = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 100]
        ]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModel2' => $searchModel2,
            'dataProvider2' => $dataProvider2,
        ]);
    }

    /**
     * Displays a single CjenikDijete model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CjenikDijete model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CjenikDijete();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CjenikDijete model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CjenikDijete model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CjenikDijete model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CjenikDijete the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CjenikDijete::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionViewSkupina($id_skupina)
    {
        $djecaSkupina = DijeteSkupina::find()->with([
            'dijete0' => function ($query) {
                $query->andWhere(['status' => 2])->orderBy('prezime,ime ASC');
            },
        ])->andWhere(['skupina' => $id_skupina])->all();
        $podaciZaCjenik = [];

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $cjeniciPost = $post['DynamicModel'];
            foreach ($cjeniciPost as $cjenikPost) {
                $cjenikDijete = CjenikDijete::findOne(['id' => $cjenikPost['id_cjenik']]);
                if (empty($cjenikDijete)) {
                    $cjenikDijete = new CjenikDijete();
                }
                $cjenikDijete->cijena = $cjenikPost['cijena'];
                $cjenikDijete->dat_poc = date('Y-m-d');
                $cjenikDijete->dat_kraj = null;
                $cjenikDijete->save();
            }
        }

        foreach ($djecaSkupina as $dijete) {
            $temp = CjenikDijete::find()->where(['dijete' => $dijete->dijete])->one();
            if (!empty($temp)) {  //check if cjenik exists for dijete, if so, add data from entry
                $podaciZaCjenik[] = new \yii\base\DynamicModel(['naziv' => $dijete->dijete0->prezime . ' ' . $dijete->dijete0->ime, 'cijena' => $temp->cijena, 'id_dijete' => $dijete->dijete, 'id_cjenik' => $temp->id]);
            } else { //else create new entry for CjenikDijete and add basic price based on vrsta_programa
                $podaci = new CjenikDijete;
                $cijena = 0;
                $dijete_temp = Dijete::find()->where(['id' => $dijete->dijete])->one();
                $vrstaPrograma = VrstaPrograma::find()->where(['id' => $dijete_temp->vrsta_programa])->one();

                if (!empty($vrstaPrograma)) {
                    $vrstaNadPrograma = null;
                    $cjenik_2 = null;
                    $cjenik_1 = Cjenik::find()->where(['program' => $vrstaPrograma->id])->one();
                    if (isset($vrstaPrograma->parent) && $vrstaPrograma->parent > 0) {
                        $vrstaNadPrograma = VrstaPrograma::find()->where(['id' => $vrstaPrograma->parent])->one();
                        $cjenik_2 = Cjenik::find()->where(['program' => $vrstaNadPrograma->id])->one();
                    }
                    if (isset($cjenik_1) && isset($cjenik_2)) {
                        $cijena = $cjenik_1->cijena + $cjenik_2->cijena;
                    } else if (isset($cjenik_1)) {
                        $cijena = $cjenik_1->cijena;
                    }
                }
                $podaci->dijete = $dijete_temp->id;
                $podaci->cijena = $cijena;
                $podaci->dat_poc = date('Y-m-d');
                $podaci->dat_kraj = null;
                $podaci->save();

                $podaciZaCjenik[] = new \yii\base\DynamicModel(['naziv' => $dijete_temp->prezime . ' ' . $dijete_temp->ime, 'cijena' => $podaci->cijena, 'id_dijete' => $dijete_temp->id, 'id_cjenik' => $podaci->id]);
            }
        }

        return $this->render('view-skupina', [
            'model' => Skupina::findOne($id_skupina),
            'cjenik' => $podaciZaCjenik
        ]);
    }

    //public function actionCreateCjenik($id)
    //{
    //    $cjenici = [];
    //    $djecaSkupina = DijeteSkupina::find()->where(['skupina'=>$id])->all();
    //    
    //    foreach ($djecaSkupina as $dijeteSkupina) {
    //        $cjenik = $this->findModelByChild($dijeteSkupina->dijete);
    //       
    //        if (empty($cjenik)) {
    //            $cjenik = new CjenikDijete;

    //            $cijena = 0;
    //            $dijete = Dijete::find()->where(['id'=>$dijeteSkupina->dijete])->one();
    //            $vrstaPrograma = VrstaPrograma::find()->where(['id'=>$dijete->vrsta_programa])->one();
    //
    //            if (!empty($vrstaPrograma)) {
    //                $cjenik = Cjenik::find()->where(['program'=>$vrstaPrograma->id])->one();
    //                $cijena = $cjenik->cijena;
    //            }
    //            $cjenik->dijete = $dijete->id;
    //            $cjenik->cijena = $cijena;
    //            $cjenik->dat_poc = date('Y-m-d');
    //            $cjenik->dat_kraj = null;
    //            $cjenik->save();

    //            $cjenici[] = $cjenik;

    //        } else {
    //            $cjenici[] = $cjenik;
    //        }            
    //    }

    //    if(Yii::$app->request->post()) {
    //        //print("<pre>".print_r(Yii::$app->request->post(),true)."</pre>");die();
    //        $cjeniciPost = $_POST['cjenici'];
    //        foreach ($cjeniciPost as $cjenikPost) {
    //            $cjenikDijete = CjenikDijete::findOne(['dijete' => $cjenikPost['dijete']]);
    //            if (empty($cjenikDijete)) {
    //                $cjenikDijete = new CjenikDijete();
    //            }
    //            $cjenikDijete->cijena = $cjenikPost['cijena'];
    //            $cjenikDijete->dat_poc = date('Y-m-d');
    //            $cjenikDijete->dat_kraj = null;
    //            $cjenikDijete->save();
    //        }
    //    }

    //    return $this->render('create-cjenici', [
    //        'cjenici' => $cjenici,
    //        'skupina' => Skupina::findOne($id),
    //    ]);
    //}

    //public function actionCjenici($id){
    //    $skupina = Skupina::find()->one();
    //    $djeca = DijeteSkupina::find()->where(['skupina'=>$id])->all();
    //    
    //    return $this->render('cjenici', ['djeca'=>$djeca]);
    //}

    //protected function findModelByChild($id)
    //{
    //    if (($model = CjenikDijete::findOne(['dijete'=>$id])) !== null) {
    //        return $model;
    //    } else {
    //        return null;
    //    }
    //}
}
