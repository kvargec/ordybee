<?php

namespace backend\controllers;

use Yii;
use Mpdf;
use yii\helpers\Url;
use ZipArchive;
use common\helpers\Utils;
use common\models\Cjenik;
use common\models\Search\CjenikSearch;
use common\models\Dijete;
use common\models\CjenikDijete;
use common\models\DijeteSkupina;
use common\models\Skupina;
use common\models\Search\DijeteSearchUpisani;
use common\models\PedagoskaGodina;
use common\models\Roditelj;
use common\models\Postavke;
use common\models\VrstaPrograma;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CjenikController implements the CRUD actions for Cjenik model.
 */
class CjenikController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cjenik models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CjenikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIndexUplatnice()
    {
        $searchModel = new DijeteSearchUpisani();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-uplatnice', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Cjenik model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cjenik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cjenik();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cjenik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cjenik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cjenik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cjenik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cjenik::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionCreateUplatnica()
    {
        return $this->render('create-uplatnica');
    }
    public function actionCreateSveUplatnice()
    {
        return $this->render('create-sve-uplatnice');
    }

    private function printPDF($sadrzaj, $pdfPath, $nazivDokumenta, $orient = 'P')
    {
        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }

    public function actionUplatnica($dijete_id)
    {
        $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $sveSkupine = Skupina::find()->select('id')->where(['ped_godina' => $godina->id])->asArray()->all();
        $tmp = DijeteSkupina::find()->select('skupina')->where(['dijete' => $dijete_id])->andWhere(['in', 'skupina', array_column($sveSkupine, "id")])->one();

        $skupina = Skupina::find()->where(['in', 'id', $tmp->skupina])->one();
        $mjesec = Utils::getMjesecPed($_POST['mjesec']);

        $dijete = Dijete::findOne($dijete_id);
        // trazim vrstu programa, da povucem obican cjenik
        $cjenikStandard = null;
        if (isset($dijete->vrsta_programa)) {
            $cjenikStandard = Cjenik::findOne(['program' => $dijete->vrsta_programa]);
            $cjenikStandard_2 = null;
            $vrstaPrograma = VrstaPrograma::findOne(['id' => $dijete->vrsta_programa]);
            if (!empty($vrstaPrograma)) {
                if (isset($vrstaPrograma->parent) && $vrstaPrograma->parent > 0) {
                    $vrstaNadPrograma = VrstaPrograma::find()->where(['id' => $vrstaPrograma->parent])->one();
                    $cjenikStandard_2 = Cjenik::findOne(['program' => $vrstaNadPrograma->id]);
                }
            }
        } else {
            $cjenikId = Cjenik::find()->min('id');
            $cjenikStandard = Cjenik::findOne($cjenikId);
        }
        $roditelji = Roditelj::find()->where(['dijete' => $dijete_id])->one();
        $cjenik = CjenikDijete::findOne(['dijete' => $dijete_id]);
        $rules = Postavke::findOne(['postavka' => 'cjenikPravila']);
        $konacnaCijena = 0;
        if (isset($cjenik)) {
            $konacnaCijena = $cjenik->cijena;
        } else {
            if (isset($cjenikStandard) && isset($cjenikStandard_2)) {
                $konacnaCijena = $cjenikStandard->cijena + $cjenikStandard_2->cijena;
            } else if (isset($cjenikStandard)) {
                $konacnaCijena = $cjenikStandard->cijena;
            }
        }
        if (isset($rules)) {
            $rules_array = array_column($rules->dodatno, 'rule');
            $rule_needed = array_merge(...array_filter($rules_array, function ($a) {
                return $a['amount_type'] == 'price';
            }));
            $reduction = intval($rule_needed['amount']);
            $start_date = new \DateTime(date('Y-m-d', strtotime('1.' . strval($mjesec['mjesec']) . '.' . strval($mjesec['godina']), time())));
            $temp_date = '1.' . strval($mjesec['mjesec']) . '.' . strval($mjesec['godina']);
            $last_day = date('t', strtotime($temp_date));
            $end_date = new \DateTime(date('Y-m-d', strtotime(strval($last_day) . '.' . strval($mjesec['mjesec']) . '.' . strval($mjesec['godina']), time())));
            $interval = \DateInterval::createFromDateString('1 day');
            $period = new \DatePeriod($start_date, $interval, $end_date);
            $izostanci = Utils::calculateDays($period, 'dijete', $dijete_id, 0);
            $izostanaka = 0;
            $izostanaka_opravdano = 0;
            foreach ($izostanci as $izostanak) {
                $izostanaka += intval($izostanak->ukupno);
                $izostanaka_opravdano += intval($izostanak->ukupno_opravdano);
            }
            $price_reduction = $izostanaka_opravdano * $reduction;
            $konacnaCijena = $konacnaCijena - $price_reduction;
        }

        $podaciVrtica = yii\helpers\ArrayHelper::index(Postavke::find()->where(['postavka' => ['nazivVrtica', 'naziv Ravnatelj', 'vrticPodaci']])->asArray()->all(), 'postavka');

        $htmlContent = $this->renderPartial('@common/views/templates/uplatnica', [
            'mjesec' => $mjesec,
            'skupina' => $skupina,
            'dijete' => $dijete,
            'roditelji' => $roditelji,
            'podaciVrtica' => $podaciVrtica,
            'konacnaCijena' => $konacnaCijena
        ]);

        $naziv = 'Uplatnica_' . $mjesec['mjesec'] . '-' . $mjesec['godina'] . '-' . $dijete->prezime . '_' . $dijete->ime;
        $dirpath = Yii::getAlias('@webroot') . "/uplatnice/" . $skupina->id . '/' . $dijete->id . '/';

        //Utils::ispisPDF($htmlContent, $naziv, 'download', 'L', $dirpath);
        return $this->printPDF($htmlContent, $dirpath . $naziv . ".pdf", $naziv . ".pdf", "L");
    }

    public function actionUplatniceZaGrupu($skupina_id)
    {
        set_time_limit(0);
        $mjesec = Utils::getMjesecPed($_POST['mjesec']);
        $djeca_ids = DijeteSkupina::find()->select('dijete')->where(['skupina' => $skupina_id])->asArray()->all();
        $djeca = Dijete::find()->where(['in', 'id', array_column($djeca_ids, "dijete")])->orderBy('prezime,ime ASC')->all();
        $zipName = 'Skupina-' . $skupina_id . '.zip';
        $zipDir = Yii::getAlias('@webroot') . '/uplatnice/' . $skupina_id;
        if (!file_exists($zipDir)) {
            mkdir($zipDir, 0777, true);
        }
        $zip = new ZipArchive();
        $linkovi = '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body><table>';
        if ($zip->open($zipDir . '/' . $zipName, (ZipArchive::CREATE | ZipArchive::OVERWRITE)) === TRUE) {
            foreach ($djeca as $dijete) {
                $this->actionUplatnica($dijete->id);
                $naziv = 'Uplatnica_' . $mjesec['mjesec'] . '-' . $mjesec['godina'] . '-' . $dijete->prezime . '_' . $dijete->ime . '.pdf';
                $dirpath = Yii::getAlias('@webroot') . '/uplatnice/' . $skupina_id . '/' . $dijete->id . '/';

                $linkovi .= '<tr><td>' . $dijete->prezime . ' ' . $dijete->ime . '</td><td>' . $mjesec['mjesec'] . '-' . $mjesec['godina'] . '</td>';
                $linkovi .= '<td><a href="' . $dirpath . $naziv . '">Link</a></td></tr>';

                $filePath = $dirpath . $naziv;
                $zip->addFile($filePath, $naziv);
            }
            $linkovi .= '</table></body></html>';
            $naziv2 = 'Links-' . $skupina_id;
            $nazivFull = Yii::getAlias('@webroot') . '/uplatnice/' . $skupina_id . '/' . $naziv2 . '.html';
            $fp = fopen($nazivFull, 'w+');
            fputs($fp, $linkovi);
            fclose($fp);
            $zip->addFile($nazivFull, $naziv2);
            $zip->close();
        }

        if (!is_file($zipDir . '/' . $zipName)) {
            throw new \yii\web\NotFoundHttpException('The file does not exists.');
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename=' . $zipName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($zipDir . '/' . $zipName));
        ob_clean();
        flush();
        readfile($zipDir . '/' . $zipName);
        return Yii::$app->response->sendFile($zipDir . '/' . $zipName, $zipName);
    }

    public function actionUplatnicaSend($dijete_id)
    {
        $mjesec = Utils::getMjesecPed($_POST['mjesec']);
        $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $dijete = Dijete::findOne($dijete_id);
        $sveSkupine = Skupina::find()->select('id')->where(['ped_godina' => $godina->id])->asArray()->all();
        $tmp = DijeteSkupina::find()->select('skupina')->where(['dijete' => $dijete_id])->andWhere(['in', 'skupina', array_column($sveSkupine, "id")])->one();
        $skupina = Skupina::find()->where(['in', 'id', $tmp->skupina])->one();
        $roditelj = Roditelj::find()->where(['dijete' => $dijete_id])->asArray()->all();
        $useremail = $roditelj[0]['email'];
        if (empty($useremail)) {
            $useremail = $roditelj[1]['email'];
        }
        $this->actionUplatnica($dijete_id);
        $dirpath = Yii::getAlias('@webroot') . '/uplatnice/' . $skupina->id . '/';
        $pdfPath = $dijete->id . '/' . 'Uplatnica ' . $mjesec['mjesec'] . '-' . $mjesec['godina'] . ' - ' . $dijete->prezime . ' ' . $dijete->ime . '.pdf';

        Yii::$app->mailer->compose()
            ->setFrom('info@ordybee.com')
            //->setTo('mario.ogresta@gmail.com')
            ->setTo($useremail)
            //->setTo('info@ordybee.com')
            ->setSubject('Uplatnica')
            ->setTextBody('U privitku se nalazi uplatnica za ' . $dijete->prezime . ' ' . $dijete->ime . ' za mjesec ' . $mjesec["mjesec"] . '-' . $mjesec["godina"])
            ->attach($dirpath . $pdfPath)
            ->send();

        return ['ok' => true, 'message' => Yii::t('app', "Dokument je poslan.")];
    }
}
