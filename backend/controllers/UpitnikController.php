<?php

namespace backend\controllers;

use common\models\Dijete;
use common\models\Odgovori;
use common\models\Pitanje;
use common\models\Roditelj;
use common\models\Zaposlenik;
use common\models\UserRoditelj;
use Yii;
use common\models\Upitnik;
use common\models\Search\UpitnikSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Mpdf;

/**
 * UpitnikController implements the CRUD actions for Upitnik model.
 */
class UpitnikController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Upitnik models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UpitnikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $upitniciSvi = Upitnik::find()->where(['status' => 2])->all();
        $upitnici = [];

        foreach ($upitniciSvi as $u) {
            $zaKoga = $u->upitnik_for;

            foreach ($zaKoga as $zk) {
                if ($zk == 'zaposlenici') {
                    $upitnici[$u->id] = $u->naziv;
                }
            }
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'upitnici' => $upitnici,
        ]);
    }

    /**
     * Displays a single Upitnik model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $pitanja = Pitanje::find()->select('id')->where(['upitnik' => $id])->all();
        $pit = array();
        foreach ($pitanja as $item) {
            $pit[] = $item->id;
        }
        $pitanjaUpitnika = new ActiveDataProvider([
            'query' => Pitanje::find()->where(['upitnik' => $id])->orderBy('id DESC'),
        ]);
        $ispunjeniUpitnici = Odgovori::find()->select('user')->distinct()->where(['in', 'pitanje', $pit])->all();
        $infoUpitnici = new ActiveDataProvider([
            'query' => Odgovori::find()->select('user')->distinct()->where(['in', 'pitanje', $pit])
        ]);
        $upitnici = array();
        $label = Yii::t('app', 'Zaposlenik');
        foreach ($ispunjeniUpitnici as $iu) {
            $temp = UserRoditelj::find()->where(['user' => $iu->user])->one();

            if (isset($temp)) {
                $rod = Roditelj::find()->where(['id' => $temp->roditelj])->one();
                $upitnici[] = $rod->dijete;
                $label = Yii::t('app', 'Dijete');
            }
        }
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'upitnici' => $infoUpitnici,
            'label' => $label,
            'pitanjaUpitnika' => $pitanjaUpitnika
        ]);
    }

    /**
     * Creates a new Upitnik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPojedini($id, $dijete)
    {
        $pitanja = Pitanje::find()->select('id')->where(['upitnik' => $id])->all();
        $pit = array();
        foreach ($pitanja as $item) {
            $pit[] = $item->id;
        }
        $rod = Roditelj::find()->where(['dijete' => $dijete])->all();
        $rodPopis = array();
        foreach ($rod as $item) {
            $rodPopis[] = $item->id;
        }
        $userRod = UserRoditelj::find()->where(['in', 'roditelj', $rodPopis])->all();
        $odgovoriAha = Odgovori::find()->where(['in', 'pitanje', $pit])->andWhere(['in', 'user', $userRod]);
        $odgovori = new ActiveDataProvider([
            'query' => $odgovoriAha,
            'pagination' => false,
        ]);
        $dijete = Dijete::find()->where(['id' => $dijete])->one();
        return $this->render('pojedini', [
            'dijete' => $dijete,
            'odgovori' => $odgovori
        ]);
    }
    public function actionPojediniZaposlenici($id, $user)
    {
        $pitanja = Pitanje::find()->select('id')->where(['upitnik' => $id])->all();
        $upitnik = Upitnik::findOne($id);
        $pit = array();
        foreach ($pitanja as $item) {
            $pit[] = $item->id;
        }
        $zap = Zaposlenik::find()->where(['user' => $user])->one();

        $odgovoriAha = Odgovori::find()->where(['in', 'pitanje', $pit])->andWhere(['in', 'user', $user]);
        $odgovori = new ActiveDataProvider([
            'query' => $odgovoriAha,
            'pagination' => false,
        ]);
        // $dijete = Dijete::find()->where(['id' => $dijete])->one();
        return $this->render('pojedini-zaposlenici', [
            'zap' => $zap,
            'odgovori' => $odgovori,
            'upitnik' => $upitnik,
        ]);
    }
    public function actionCreate()
    {
        $model = new Upitnik();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Upitnik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Upitnik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Upitnik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Upitnik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView2($id)
    {
        $svaPitanja = Pitanje::find()->where(['upitnik' => $id])->orderBy('id ASC')->all();
        return $this->render('view2', [
            'model' => $this->findModel($id),
            'svaPitanja' => $svaPitanja
        ]);
    }
    public function actionSpremi()
    {
        $post = Yii::$app->request->post();
        $userID = Yii::$app->user->id;
        $odgovori = array();
        $pitanja = array();
        if ($post) {
            foreach ($post as $item => $value) {
                $idPit = explode("_", $item);

                if ($idPit[1] != 'csrf-backend') {
                    $forTest = Odgovori::find()->where(['pitanje' => $idPit[1], 'user' => $userID])->one();
                    if (isset($forTest)) {
                        $temp = $forTest;
                    } else {
                        $temp = new Odgovori();
                    }
                    $temp->user = $userID;
                    $temp->pitanje = $idPit[1];
                    $temp->value = json_encode(['odgovori' => $value]);
                    $temp->save();
                    $pitanja[] = $idPit[1];
                    $odgovori[$idPit[1]] = $value;
                }
            }
        }
        $pitanjaObjekti = Pitanje::find()->where(['in', 'id', $pitanja])->all();
        return $this->render('odgovori', [
            'pitanja' => $pitanjaObjekti,
            'odgovori' => $odgovori
        ]);
    }
    protected function findModel($id)
    {
        if (($model = Upitnik::findOne($id)) !== null) {

            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    private function printPDF($sadrzaj, $pdfPath, $nazivDokumenta, $orient = 'P')
    {
        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }

    public function actionPdfPrint($id, $dijete)
    {
        $upitnik = Upitnik::findOne($id);
        $pitanja = Pitanje::find()->select('id')->where(['upitnik' => $id])->all();
        $pit = array();
        foreach ($pitanja as $item) {
            $pit[] = $item->id;
        }
        $roditelji = Roditelj::find()->where(['dijete' => $dijete])->all();
        $roditeljiPopis = array();
        foreach ($roditelji as $item) {
            $roditeljiPopis[] = $item->id;
        }
        $userRoditelji = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiPopis])->all();
        $odgovoriAha = Odgovori::find()->where(['in', 'pitanje', $pit])->andWhere(['in', 'user', $userRoditelji]);
        $odgovori = new ActiveDataProvider([
            'query' => $odgovoriAha,
            'pagination' => false,
        ]);
        $dijete = Dijete::find()->where(['id' => $dijete])->one();
        $htmlContent = $this->renderPartial('@common/views/upitnik/upitnikpdf', [
            'upitnik' => $upitnik,
            'dijete' => $dijete,
            'odgovori' => $odgovori,
            'roditelji' => $roditelji
        ]);
        return $this::printPDF($htmlContent, Yii::getAlias('@webroot') . "/upitnik/" . $upitnik->naziv . "-" . $upitnik->id . ".pdf", $upitnik->naziv . "-" . $upitnik->id . ".pdf");
    }
    public function actionDuplicate($id)
    {
        $original = $this->findModel($id);
        $creator = Yii::$app->user->id;

        $model = new Upitnik;

        $model->naziv = $original->naziv;
        $model->opis = $original->opis;
        $model->ped_godina = $original->ped_godina;
        $model->status = 3;
        $model->upitnik_for = $original->upitnik_for;
        $model->is_required = $original->is_required;
        $model->creator = $creator;
        $model->save();

        return $this->redirect(['index']);
    }
}
