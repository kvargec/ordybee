<?php

namespace backend\controllers;

use Yii;
use common\models\PrijevozZaposlenik;
use common\models\Search\PrijevozZaposlenikSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Zaposlenik;
use common\models\Prijevoz;
use common\helpers\Utils;

/**
 * PrijevozZaposlenikController implements the CRUD actions for PrijevozZaposlenik model.
 */
class PrijevozZaposlenikController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PrijevozZaposlenik models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PrijevozZaposlenikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PrijevozZaposlenik model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PrijevozZaposlenik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PrijevozZaposlenik();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PrijevozZaposlenik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PrijevozZaposlenik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionZaposlenici($tjedan = 0)
    {

        $datumi = $this->getDatum($tjedan);
        $prijevozi = Prijevoz::find()->all();
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->all();
        return $this->render('zaposlenici', ['zaposlenici' => $zaposlenici, 'datumi' => $datumi, 'prijevozi' => $prijevozi]);
    }
    public function actionDodajZaposlenik()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset($_POST['id'], $_POST['datum'], $_POST['amount'])) {
            $prisutnost = PrijevozZaposlenik::findOne(['zaposlenik_id' => $_POST['id'], 'datum' => $_POST['datum']]);
            // if (empty($prisutnost)) {
            //     $prisutnost = new PrijevozZaposlenik();
            // }
            $koliko = $_POST['amount'];
            $prijevozCijena = Prijevoz::find()->where(['id' => $_POST['prijevoz']])->one();
            while ($koliko > 0) {
                $prisutnost = new PrijevozZaposlenik();
                $prisutnost->datum = $_POST['datum'];
                $prisutnost->zaposlenik_id = $_POST['id'];
                $prisutnost->prijevoz_id =  $_POST['prijevoz'];
                $prisutnost->cijena =  $prijevozCijena->cijena;

                $prisutnost->save();
                $koliko--;
            }
            $prisutnost->datum = $_POST['datum'];
            $prisutnost->zaposlenik_id = $_POST['id'];
            $prisutnost->prijevoz_id =  $_POST['prijevoz'];
            $prisutnost->cijena =  $prijevozCijena->cijena;

            // $prisutnost->save();


            $pr = Prijevoz::findOne(['id' => $prisutnost->prijevoz_id]);
            $htmlRespnse = $this->renderPartial('zaposlenici-prisutnost-summary', ['prisutnost' => $prisutnost, 'pr' => $pr]);
            return [
                'message' => Yii::t('app', 'Uspješno unesen prijevoz'),
                'status' => 'success',
                'value' => $htmlRespnse,
            ];
        }
    }
    /**
     * Finds the PrijevozZaposlenik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PrijevozZaposlenik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    private function getDatum($tjedan)
    {
        $datum['pon'] = date('Y-m-d', strtotime('monday this week'));
        $datum['uto'] = date('Y-m-d', strtotime('tuesday this week'));
        $datum['sri'] = date('Y-m-d', strtotime('wednesday this week'));
        $datum['cet'] = date('Y-m-d', strtotime('thursday this week'));
        $datum['pet'] = date('Y-m-d', strtotime('friday this week'));
        if ($tjedan == '0') {
            return $datum;
        } else {
            $datum['pon'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pon'])));
            $datum['uto'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['uto'])));
            $datum['sri'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['sri'])));
            $datum['cet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['cet'])));
            $datum['pet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pet'])));
        }
        return $datum;
    }
    protected function findModel($id)
    {
        if (($model = PrijevozZaposlenik::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionZaposleniciPrijevozPdfMjesecni()
    {
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all();

        $mjesec = Utils::getMjesecPed($_POST['mjesec']);
        $numberDays = cal_days_in_month(CAL_GREGORIAN, $mjesec['mjesec'], date('Y'));
        $poc = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-01';
        $kraj = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $numberDays;
        $header = ['Ime i prezime', 'Broj dana', 'Broj prijevoza', 'Cijena', 'Iznos'];
        $retci = [];

        foreach ($zaposlenici as $zaposlenik) {
            $zaNaziv = $zaposlenik->ime . ' ' . $zaposlenik->prezime;
            $temp = [];
            $temp[0] = '<span style="font-weight: bold;text-align:left" class="left">' . $zaNaziv . '</span>';
            $prisutnost = $zaposlenik->getPrisutnost()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet' => 'zaposlenik'])->andWhere(['status' => 1])->orderBy('datum ASC')->all(); //Prisutnost::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet_id' => $zaposlenik->id, 'entitet' => 'zaposlenik'])->orderBy('datum ASC')->all();
            $temp[1] = count($prisutnost);
            $prijevozi = PrijevozZaposlenik::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['zaposlenik_id' => $zaposlenik->id])->all();
            $temp[2] = count($prijevozi);
            if ($prijevozi) {
                $temp[3] = $prijevozi[0]->cijena;
            } else {
                $temp[3] = 0;
            }
            $total = 0;
            foreach ($prijevozi as $prijevoz) {
                $total += $prijevoz->cijena;
            }
            $temp[4] = $total;
            $retci[] = $temp;
        }
        $prijevozFull = array_merge([$header], $retci);
        $htmlContent = $this->renderPartial('@common/views/templates/zaposleniciPrijevozPdfMjesecni', ['prijevoz' => $prijevozFull, 'mjesec' => $mjesec]);
        $naziv = 'Zaposlenici_Prijevoz-' . $mjesec['mjesec'] . '/' . $mjesec['godina'] . '';
        Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
    }
}
