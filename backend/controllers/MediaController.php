<?php

namespace backend\controllers;

use common\models\Roditelj;
use common\models\UserRoditelj;
use common\models\Zaposlenik;
use common\models\Skupina;
use common\models\DijeteSkupina;
use common\models\Zaposlenje;
use common\models\StatusOpce;
use Yii;
use common\models\Media;
use common\models\MediaRecipients;
use common\models\Search\MediaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MediaController implements the CRUD actions for Media model.
 */
class MediaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Media models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MediaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    /**
     * Displays a single Media model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Media model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Media();

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Media');

            $model->grupacija = $post['grupacija'];
            $model->ccgrupacija = $post['ccgrupacija'];
            $model->user_id = Yii::$app->user->id;
            $model->created_at = date('Y-m-d H:i:s', time());
            $model->recipients = [$post['grupacija'] => $post['recipients']];
            $model->cc_recipients = [$post['ccgrupacija'] => $post['cc_recipients']];
            if ($model->save()) {
                //za sendera, da vidi što je poslao
                $temp = new MediaRecipients();
                $temp->media = $model->id;
                $temp->user = Yii::$app->user->id;
                $temp->created_at = date('Y-m-d H:i:s', time());
                $temp->save();
                switch ($post['grupacija']) {
                    case 'roditelji':
                        if ($post['recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['recipients']])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($post['recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['recipients']])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($post['recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['recipients']])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($post['recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['recipients']])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                }
                switch ($post['ccgrupacija']) {
                    case 'roditelji':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['cc_recipients']])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['cc_recipients']])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['cc_recipients']])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['cc_recipients']])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                }
                return $this->render('uploadfiles', ['model' => $model]);
            } else if (Yii::$app->user->isGuest) {
                $model->delete();
                return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Media model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post('Media');

            $model->grupacija = $post['grupacija'];
            $model->recipients = [$post['grupacija'] => $post['recipients']];
            $model->ccgrupacija = $post['ccgrupacija'];
            $model->cc_recipients = [$post['ccgrupacija'] => $post['cc_recipients']];

            if ($model->save()) {
                $old_recipients = MediaRecipients::find()->where(['media' => $id])->all();
                foreach ($old_recipients as $item) {
                    $item->delete();
                }
                switch ($post['grupacija']) {
                    case 'roditelji':
                        if ($post['recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['recipients']])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($post['recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['recipients']])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($post['recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['recipients']])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($post['recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['recipients']])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                }
                switch ($post['ccgrupacija']) {
                    case 'roditelji':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $post['cc_recipients']])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'skupine':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = UserRoditelj::find()->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojaDjeca = DijeteSkupina::find()->where(['in', 'skupina', $post['cc_recipients']])->select('dijete')->all();
                            $roditelji = Roditelj::find()->select('id')->where(['in', 'dijete', $kojaDjeca])->all();
                            $roditeljiSvi = array();
                            foreach ($roditelji as $rod) {
                                $roditeljiSvi[] = $rod->id;
                            }
                            $kojiUseri = UserRoditelj::find()->where(['in', 'roditelj', $roditeljiSvi])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'zaposlenici':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $radnaMjesta = Zaposlenje::find()->select('zaposlenik')->where(['in', 'r_mjesto', $post['cc_recipients']])->asArray()->all();
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', array_column($radnaMjesta, 'zaposlenik')])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                    case 'pojedini':
                        if ($post['cc_recipients'][0] == 0) {
                            $kojiUseri = Zaposlenik::find()->where(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        } else {
                            $kojiUseri = Zaposlenik::find()->where(['in', 'id', $post['cc_recipients']])->andWhere(['!=', 'status', 3])->all();
                            foreach ($kojiUseri as $item) {
                                $temp = new MediaRecipients();
                                $temp->media = $model->id;
                                $temp->user = $item->user;
                                $temp->created_at = date('Y-m-d H:i:s', time());
                                if ($temp->save()) {
                                } else {
                                    var_dump($temp->errors);
                                    die();
                                };
                            }
                        }
                        break;
                }
                return $this->render('uploadfiles', ['model' => $model]);
            } else if (Yii::$app->user->isGuest) {
                $model->delete();
                return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Media model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Media model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Media the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Media::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionGrupacije()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $pedGodina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $pedGodina = $pedGodina->id;
        $out = array();
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $grupacija = $ids; // empty($ids[0]) ? null : $ids[0]; ukoliko je multiple

            switch ($grupacija[0]) {
                case 'roditelji':
                    $roditelji = Roditelj::find()->all();
                    //vidjeti da je pedagoska godina
                    $out[] = ['id' => 0, 'name' => Yii::t('app', 'Svi roditelji')];
                    foreach ($roditelji as $roditelj) {
                        $out[] = ['id' => $roditelj->id, 'name' => $roditelj->prezime . ' ' . $roditelj->ime];
                    }
                    break;
                case 'skupine':
                    $skupine = Skupina::find()->where(['ped_godina' => $pedGodina])->all();
                    //vidjeti da je pedagoska godina
                    $out[] = ['id' => 0, 'name' => Yii::t('app', 'Sve skupine')];
                    foreach ($skupine as $skupina) {
                        $out[] = ['id' => $skupina->id, 'name' => $skupina->naziv];
                    }
                    break;
                case 'zaposlenici':
                    $radno_mjesto = \common\models\RadnoMjesto::find()->all();
                    //vidjeti da je pedagoska godina
                    $out[] = ['id' => 0, 'name' => Yii::t('app', 'Sva radna mjesta')];
                    foreach ($radno_mjesto as $rmjesto) {
                        $out[] = ['id' => $rmjesto->id, 'name' => $rmjesto->naziv];
                    }
                    break;
                case 'pojedini':
                    $zaposlenici = \common\models\Zaposlenik::find()->where(['!=', 'status', 3])->all();
                    $out[] = ['id' => 0, 'name' => Yii::t('app', 'Svi zaposlenici')];
                    foreach ($zaposlenici as $zaposlenik) {
                        $out[] = ['id' => $zaposlenik->id, 'name' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
                    }
                    break;
            }
            return ['output' => $out, 'selected' => ''];
        }
        return ['output' => [], 'selected' => ''];
    }

    public function actionUploadFile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
        } else if (isset($_POST) && isset($_POST['model_id'])) {
            $id = $_POST['model_id'];
            $model = $this->findModel($id);

            if (!file_exists(Yii::getAlias('@backend') . '/web/media/' . $id)) {
                mkdir(Yii::getAlias('@backend') . '/web/media/' . $id, 0777, true);
            }
            $dir = Yii::getAlias('@backend') . '/web/media/' . $id;
            $realAttachments = [];
            if (!empty($model->attachments['attachments'])) {
                foreach ($model->attachments['attachments'] as $item) {
                    $realAttachments[] = $item;
                }
            }
            $model->attachments = UploadedFile::getInstances($model, 'attachments');

            $added_size = 0;
            foreach ($model->attachments as $file) {
                $fileName = preg_replace("/\s+/", "_", $file->name);
                if (!in_array($fileName, $realAttachments)) {
                    $realAttachments[] = $fileName;
                }
                $file->saveAs($dir . '/' . $fileName);
                $size = filesize($dir . '/' . $fileName);
                $rounded = $size;
                $added_size += $rounded;
            }
            $model->size = round($added_size / 1024 / 1024);
            $model->attachments = ['attachments' => $realAttachments];

            if ($model->save()) {
                $preview = $model->getFilePaths($id);
                $config = $model->getFilePathsConfig();
                $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                return json_encode($out);
            } else if (Yii::$app->user->isGuest) {
                $model->delete();
                return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
            }
        } else {
            return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
        }
    }

    public function actionDeleteFile()
    {
        $post = Yii::$app->request->post();
        if (isset($post)) {
            $id = $post['model_id'];
            $filename = $post['key'];
            $model = $this->findModel($id);
            $allFiles = $model->attachments['attachments'];
            $dir = Yii::getAlias('@backend') . '/web/media/' . $id;
            $file = array_search($filename, $model->attachments['attachments'], true);

            if (isset($file)) {
                unset($allFiles[$file]);
                $attachments = array_values($allFiles);
                $model->attachments = ['attachments' => $attachments];
                unlink($dir . '/' . $filename);
                $model->save();
            }
        }

        $preview = $model->getFilePaths($id);
        $config = $model->getFilePathsConfig();
        $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];

        return json_encode($out);
    }
}
