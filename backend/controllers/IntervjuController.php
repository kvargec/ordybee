<?php

namespace backend\controllers;

use common\models\Dijete;
use common\models\Search\ZahtjevSearch;
use common\models\OcijenjenZahtjev;
use common\models\Search\OcijenjenZahtjevSearch;
use common\models\UpisneGrupe;
use Yii;
use common\models\Intervju;
use common\models\Search\IntervjuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IntervjuController implements the CRUD actions for Intervju model.
 */
class IntervjuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Intervju models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OcijenjenZahtjevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $upisneGrupe=UpisneGrupe::find()->orderBy('date_poc DESC')->all();
        $podaci=$dataProvider->getModels();

        $rezultati=array();
        foreach($upisneGrupe as $grupa){
            $rezultati[$grupa->id]=array();
            foreach ($podaci as $podatak){
                $dete=Dijete::find()->where(['id'=>$podatak['dijete']])->one();
                if(strtotime($dete->dat_rod)>=strtotime($grupa->date_poc) and strtotime($dete->dat_rod)<=strtotime($grupa->date_kraj)){
                    $rezultati[$grupa->id][]=$podatak;
                }
            }
        }
        return $this->render('index', [
            'upisneGrupe' => $upisneGrupe,
            'rezultati' => $rezultati,
        ]);
    }
    public function actionSnimi($id){
        if (!Yii::$app->request->isAjax) {
            return;
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $vrijeme = Yii::$app->request->post('vrijeme');
        if(!$vrijeme) {
            return;
        }else{
            $nadji=Intervju::find()->where(['zahtjev'=>$id])->one();
            if(isset($nadji)){
                $nadji->vrijeme=$vrijeme;
                $nadji->save();
            }else{
                $novo=new Intervju();
                $novo->zahtjev=$id;
                $novo->vrijeme=$vrijeme;
                $novo->save();
            }
        }

        return [
            'message' => Yii::t('app','Snimljeno vrijeme'),
        ];
    }
    /**
     * Displays a single Intervju model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Intervju model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Intervju();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Intervju model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Intervju model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Intervju model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Intervju the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Intervju::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
