<?php

namespace backend\controllers;

use common\helpers\Utils;
use common\models\DocumentVrsta;
use common\models\User;
use common\models\Dijete;
use common\models\Document;
use common\models\DocumentZahtjev;
use common\models\Rodbina;
use common\models\Roditelj;
use common\models\PedagoskaGodina;
use common\models\UpisneGrupe;
use common\models\VrstaKriterija;
use common\models\StatusKriterija;
use common\models\Kriterij;
use common\models\ZahtjevKriterij;
use common\models\VrstaUnosa;
use Yii;
use common\models\Zahtjev;
use common\models\Search\ZahtjevSearch;
use common\models\OcijenjenZahtjev;
use common\models\Search\OcijenjenZahtjevSearch;
use common\models\DokumentTemplate;
use common\models\Postavke;
use common\models\VrstaPrograma;
use backend\models\RjesenjeForm;
use backend\models\OdlukaForm;
use backend\models\UgovorForm;
use yii\base\BaseObject;
use yii\web\Controller;
use common\controllers\CommonZahtjevController;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

use yii\helpers\Html;
use kartik\builder\Form;

/**
 * ZahtjevController implements the CRUD actions for Zahtjev model.
 */
class ZahtjevController extends CommonZahtjevController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Zahtjev models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZahtjevSearch();
        $aktivnaGodina = PedagoskaGodina::getAktivna();
        $proslaGodina = PedagoskaGodina::getProsla();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['pd_godina' => [$aktivnaGodina->id, $proslaGodina->id]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zahtjev model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $zahtjev = Zahtjev::find()->where(['id' => $id])->one();

        $dijete = Dijete::find()->where(['id' => $zahtjev->dijete])->one();

        $query = Roditelj::find()->where(['dijete' => $zahtjev->dijete])->andWhere('LENGTH(ime)>0');
        $dataProviderRoditelji = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $rodbina = Rodbina::find()->where(['dijete' => $zahtjev->dijete])->all();
        $siblingIds = [];
        foreach ($rodbina as $sestra) {
            $siblingIds[] = $sestra->sibling;
        }

        $query = Dijete::find()->where(['id' => $siblingIds]);
        $dataProviderSiblings = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $dokumenti = DocumentZahtjev::find()->where(['zahtjev' => $id])->all();
        $documentIds = [];
        foreach ($dokumenti as $doc) {
            $documentIds[] = $doc->document;
        }

        $query = Document::find()->where(['id' => $documentIds]);
        $dataProviderDocuments = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
        $popisDokumenata = new ActiveDataProvider([
            'query' => DocumentZahtjev::find()->where(['zahtjev' => $id])->all()
        ]);
        $aktivniKriteriji = Kriterij::getAktivniKriteriji();

        $inputCheckbox = function ($name, $checked, $value) {
            $id = implode('-', explode(' ', strtolower($name)));
            $checked = $checked ? 'checked' : '';
            return "
            <div class=\"col-sm-10\">
                <label for=\"$id\">
                    <input type=\"checkbox\" id=\"$id\"
                        name=\"bodovi[$name]\" value=\"1\"
                        $checked />
                        $name
                </label>
            </div>
            <div class=\"col-sm-2\">
                $value
            </div>
            ";
        };

        $inputSelect = function ($name, $options, $value, $defaultSelected) {
            $selectOption = function ($value, $selected, $label) {
                $selected = $selected ? 'selected' : '';
                return "<option value=\"$value\" $selected>$label</option>";
            };

            $defaultSelected = $defaultSelected ? 'selected' : '';

            $id = implode('-', explode(' ', strtolower($name)));
            $options = implode(PHP_EOL, array_map(function ($option) use ($selectOption) {
                return $selectOption($option['value'], $option['selected'], $option['label']);
            }, $options));
            return "
            <div class=\"col-sm-10\">
                <label for=\"$id\">
                    <select id=\"$id\"
                        class=\"select-width\"
                        name=\"bodovi[$name]\" required>
                        <!-- <option value=\"\" $defaultSelected hidden></option>
                        -->
                        $options
                    </select>
                    $name
                </label>
            </div>
            <div class=\"col-sm-2\">
                $value
            </div>
            ";
        };

        $attributes = [];
        foreach ($aktivniKriteriji as $kriterij) {
            $zahtjevKriterij = ZahtjevKriterij::findOne(['zahtjev_id' => $id, 'kriterij_id' => $kriterij->id]);
            $checked = false;
            if ($zahtjevKriterij) {
                $checked = $zahtjevKriterij->zadovoljen;
            }

            if ($kriterij->izravan_upis) {
                $value = 'izravan upis';
            } else {
                $value = $kriterij->bodovi;
            }

            if (VrstaUnosa::findOne($kriterij->vrsta_unosa_id)->vrsta == 'checkbox') {
                $attributes[$kriterij->naziv] = [
                    'label' => $kriterij->naziv,
                    'type' => Form::INPUT_RAW,
                    'value' => $inputCheckbox($kriterij->naziv, $checked, $value),
                ];
            } else if (VrstaUnosa::findOne($kriterij->vrsta_unosa_id)->vrsta == 'select') {
                $opcije = $kriterij->opcije;
                if (!$opcije) {
                    $opcije = [];
                }
                $options = [];

                $selectFirst = !$checked;
                foreach ($opcije as $k => $v) {
                    $selected = false;
                    if ($checked && $k == $zahtjevKriterij->zadovoljene_opcije) {
                        $selected = true;
                    } else if ($selectFirst) {
                        $selected = true;
                        $selectFirst = false;
                    }
                    $options[] = ['value' => $k, 'selected' => $selected, 'label' => $v,];
                }

                $attributes[$kriterij->naziv] = [
                    'label' => $kriterij->naziv,
                    'type' => Form::INPUT_RAW,
                    'value' => $inputSelect($kriterij->naziv, $options, $value, false),
                ];
            }
        }

        $attributes['actions'] = ['type' => Form::INPUT_RAW, 'value' => Html::submitButton(Yii::t('app', 'Boduj'), ['class' => 'btn btn-primary'])];


        $inputCheckbox2 = function ($name, $checked) {
            $id = implode('-', explode(' ', strtolower($name)));
            $checked = $checked ? 'checked' : '';
            return "
            <div class=\"col-sm-10\">
                <label for=\"$id\">
                    <input type=\"checkbox\" id=\"$id\"
                        name=\"predaniObavezniDokumenti[$name]\" value=\"1\"
                        $checked />
                        $name
                </label>
            </div>
            ";
        };

        $obavezniDokumentiAttributes = [];
        $aktivnaGodina = PedagoskaGodina::getAktivna();
        $obavezniDokumenti = $aktivnaGodina->postavke['obavezni_dokumenti'];
        foreach ($obavezniDokumenti as $dokument) {
            $checked = false;
            if ($zahtjev->predani_obavezni_dokumenti && in_array($dokument, $zahtjev->predani_obavezni_dokumenti)) {
                $checked = true;
            }

            $obavezniDokumentiAttributes[$dokument] = [
                'label' => $dokument,
                'type' => Form::INPUT_RAW,
                'value' => $inputCheckbox2($dokument, $checked),
            ];
        }
        $obavezniDokumentiAttributes['actions'] = ['type' => Form::INPUT_RAW, 'value' => Html::submitButton(Yii::t('app', 'Završi'), ['class' => 'btn btn-primary'])];

        return $this->render('view2', [
            'zahtjev' => $zahtjev,
            'dijete' => $dijete,
            'roditelji' => $dataProviderRoditelji,
            'siblings' => $dataProviderSiblings,
            'documents' => $dataProviderDocuments,
            'attributes' => $attributes,
            'obavezniDokumentiAttributes' => $obavezniDokumentiAttributes,
        ]);

        return $this->render('view', [
            'zahtjev' => $zahtjev,
            'dijete' => $dijete,
            'roditelji' => $dataProviderRoditelji,
            'siblings' => $dataProviderSiblings,
            'documents' => $dataProviderDocuments,
            'attributes' => $attributes,
        ]);
    }

    /**
     * Creates a new Zahtjev model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Zahtjev();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Zahtjev model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            switch ($model->status0->status) {
                case 'Obrađen':
                case 'Odbijen':
                case 'U obradi':
                case 'Na čekanju':
                    $msg = Yii::t('app', 'Vaš zahtjev je ') . strtolower($model->status0->status);
                    break;
                case 'Upisan':
                    $msg = Yii::t('app', "Vaše dijete je upisano");
                    break;
                case 'Nepotpuna dokumentacija':
                    $msg = Yii::t('app', "Vaš zahtjev ima nepotpunu dokumentaciju");
                    break;
                default:
                    $msg = '';
            }
            if ($msg != '') {
                Utils::sendNotification($model->user, 'zahtjev', $model->id, 'view', $msg);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionObiljeziPredaneObavezneDokumente($id)
    {
        $model = $this->findModel($id);

        $predaniObavezniDokumenti = Yii::$app->request->post('predaniObavezniDokumenti');
        if (!$predaniObavezniDokumenti) {
            $predaniObavezniDokumenti = [];
        }

        $aktivnaGodina = PedagoskaGodina::getAktivna();
        $obavezniDokumenti = $aktivnaGodina->postavke['obavezni_dokumenti'];
        $predani_dokumenti = [];
        foreach ($obavezniDokumenti as $dokument) {
            if (array_key_exists($dokument, $predaniObavezniDokumenti)) {
                array_push($predani_dokumenti, $dokument);
            }
        }
        $zahtjev = Zahtjev::findOne($id);
        $zahtjev->predani_obavezni_dokumenti = $predani_dokumenti;
        $zahtjev->save();

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionBoduj($id)
    {
        $model = $this->findModel($id);

        $bodovi = Yii::$app->request->post('bodovi');
        if (!$bodovi) {
            $bodovi = [];
        }

        $aktivniKriteriji = Kriterij::getAktivniKriteriji();
        foreach ($aktivniKriteriji as $kriterij) {
            $zahtjevKriterij = ZahtjevKriterij::findOne(['zahtjev_id' => $id, 'kriterij_id' => $kriterij->id]);
            if (!$zahtjevKriterij) {
                $zahtjevKriterij = new ZahtjevKriterij();
            }

            $zahtjevKriterij->zahtjev_id = $id;
            $zahtjevKriterij->kriterij_id = $kriterij->id;
            $zahtjevKriterij->zadovoljen = false;
            if (array_key_exists($kriterij->naziv, $bodovi)) {
                $zahtjevKriterij->zadovoljen = true;
            }

            if (VrstaUnosa::findOne($kriterij->vrsta_unosa_id)->vrsta == 'select') {
                $zahtjevKriterij->zadovoljene_opcije = intval($bodovi[$kriterij->naziv]);
            } else if (VrstaUnosa::findOne($kriterij->vrsta_unosa_id)->vrsta == 'checkbox') {
                if ($zahtjevKriterij->zadovoljen == true) {
                    $zahtjevKriterij->zadovoljene_opcije = 1;
                } else {
                    $zahtjevKriterij->zadovoljene_opcije = null;
                }
            }

            $zahtjevKriterij->save();
        }

        $model->ocijenjen = true;
        $model->save();

        return $this->redirect(['view', 'id' => $id]);
    }

    private function getDokumentVariablesMapper($zahtjev)
    {

        $dijete = $zahtjev->dijete0;
        $roditelji = Roditelj::find()->where(['dijete' => $dijete->id])->all();
        $punoImeRoditelja = '';
        if (count($roditelji) > 1) {
            $roditelj1 = $roditelji[0];
            $roditelj2 = $roditelji[1];
            $punoImeRoditelja = "$roditelj1->ime $roditelj1->prezime i $roditelj2->ime $roditelj2->prezime";
        } else {
            $roditelj = $roditelji[0];
            $punoImeRoditelja = "$roditelj->ime $roditelj->prezime";
        }
        $datumPredajeZahtjeva = Yii::$app->formatter->asDate($zahtjev->create_at);

        $variablesMapper = [
            '%klasa%' => $zahtjev->klasa,
            '%urbroj%' => $zahtjev->urbroj,
            '%danasnjiDatum%' => Yii::$app->formatter->asDate(date('Y-m-d')),
            '%punoImeDjeteta%' => "$dijete->ime $dijete->prezime",
            '%adresaDjeteta%' => "$dijete->mjesto, $dijete->adresa",
            // '%nazivVrtica%' => "NAZIV_VRTICA",
            '%datumUpisaDjeteta%' => 'DATUM_UPISA_DJETETA',
            '%punoImeRoditelja%' => $punoImeRoditelja,
            '%datumPredajeZahtjeva%' => $datumPredajeZahtjeva,
            '%datumDonosenjaRjesenja%' => 'DATUM_DONOSENJA_RJESENJA',
            '%punoImeRavnatelja%' => Postavke::findOne(['postavka' => 'nazivRavnatelj'])->vrijednost,
        ];

        return $variablesMapper;
    }

    private function buildDokumentContent($zahtjev, $slug, $additionalVariablesMapper = null)
    {
        $predlozakDokumenta = DokumentTemplate::findOne(['slug' => $slug]);
        if (!$predlozakDokumenta) {
            throw new ServerErrorHttpException(Yii::t('app', "Fali predlozak dokumenta $predlozakDokumenta->naziv."));
        }

        if (!$predlozakDokumenta->template) {
            throw new ServerErrorHttpException(Yii::t('app', 'Fali predlozak.'));
        }

        $variablesMapper = $this->getDokumentVariablesMapper($zahtjev);
        if ($additionalVariablesMapper) {
            foreach ($additionalVariablesMapper as $k => $v) {
                $variablesMapper["%$k%"] = $v;
            }
        }
        $search = [];
        $replace = [];
        foreach ($variablesMapper as $k => $v) {
            $search[] = $k;
            $replace[] = $v;
        }
        $sadrzaj = str_replace($search, $replace, $predlozakDokumenta->sadrzaj);

        $contentFull = Controller::renderPartial("@common/views/templates/$predlozakDokumenta->template", [
            'naslov' => Yii::t('app', 'Rješenje'),
            'content' => $sadrzaj
        ]);

        return $contentFull;
    }

    public function actionGetRjesenje($id)
    {
        $zahtjev = $this->findModel($id);

        $form = Yii::$app->request->post('RjesenjeForm');
        $dokumentContent = $this->buildDokumentContent($zahtjev, 'rjesenje', $form);
        $documentPath = Yii::getAlias('@frontend/web') . "/dokumenti/" . $id;
        if (!file_exists($documentPath)) {
            mkdir($documentPath, 0777, true);
        }
        $documentTitle = 'rjesenjeoupisu' . $id;
        Utils::ispisPDF($dokumentContent, $documentTitle, 'file', 'P', $documentPath);
        $postojiLi = Document::find()->where(['filename' => $documentTitle . '.pdf'])->one();
        if (!isset($postojiLi)) {
            $dokument = new Document();
            $dokument->naziv = Yii::t('app', "Rješenje o upisu-") . $id;
            $dokument->filename = $documentTitle . '.pdf';
            $dokument->vrsta_dokumenta = 2;
            $dokument->save();
            $dokZahtjev = new DocumentZahtjev();
            $dokZahtjev->zahtjev = $id;
            $dokZahtjev->document = $dokument->id;
            $dokZahtjev->save();
        }

        $this->redirect(['view', 'id' => $id]);
    }
    public function actionGetUgovor($id)
    {
        $zahtjev = $this->findModel($id);

        $form = Yii::$app->request->post('UgovorForm');
        $documentPath = Yii::getAlias('@frontend/web') . "/dokumenti/" . $id;
        if (!file_exists($documentPath)) {
            mkdir($documentPath, 0777, true);
        }
        $dokumentContent = $this->buildDokumentContent($zahtjev, 'ugovor', $form);
        $documentTitle = 'ugovoroupisu' . $id;
        Utils::ispisPDF($dokumentContent, $documentTitle, 'file', 'P', $documentPath);
        $postojiLi = Document::find()->where(['filename' => $documentTitle . '.pdf'])->one();
        if (!isset($postojiLi)) {
            $dokument = new Document();
            $dokument->naziv = "Ugovor o upisu-" . $id;
            $dokument->filename = $documentTitle . '.pdf';
            $dokument->vrsta_dokumenta = 3;
            $dokument->save();
            $dokZahtjev = new DocumentZahtjev();
            $dokZahtjev->zahtjev = $id;
            $dokZahtjev->document = $dokument->id;
            $dokZahtjev->save();
        }
        $this->redirect(['view', 'id' => $id]);
    }

    public function actionGetOdluka($id)
    {
        $zahtjev = $this->findModel($id);

        $form = Yii::$app->request->post('OdlukaForm');
        $documentPath = Yii::getAlias('@frontend/web') . "/dokumenti/" . $id;
        if (!file_exists($documentPath)) {
            mkdir($documentPath, 0777, true);
        }
        $dokumentContent = $this->buildDokumentContent($zahtjev, 'odluka', $form);
        $documentTitle = 'odlukaoupisu' . $id;
        Utils::ispisPDF($dokumentContent, $documentTitle, 'file', 'P', $documentPath);
        $postojiLi = Document::find()->where(['filename' => $documentTitle . '.pdf'])->one();
        if (!isset($postojiLi)) {
            $dokument = new Document();
            $dokument->naziv = "Odluka o upisu-" . $id;
            $dokument->filename = $documentTitle . '.pdf';
            $dokument->vrsta_dokumenta = 4;
            $dokument->save();
            $dokZahtjev = new DocumentZahtjev();
            $dokZahtjev->zahtjev = $id;
            $dokZahtjev->document = $dokument->id;
            $dokZahtjev->save();
        }
        $this->redirect(['view', 'id' => $id]);
    }
    private function sendDokument($slug, $form, $zahtjevId)
    {
        $dokumentTemplate = DokumentTemplate::findOne(['slug' => $slug]);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $zahtjev = $this->findModel($zahtjevId);
            $dokumentContent = $this->buildDokumentContent($zahtjev, $slug, $form->attributes);
            $dir = Yii::getAlias('@frontend') . "/web/upload/zahtjevi/" . $zahtjevId;

            if (!file_exists($dir)) {
                $kaj = mkdir($dir, 0777, true);
            }
            $pdfPath = Utils::ispisPDF($dokumentContent, $slug, 'file', 'P', $zahtjev->getDirPath());
            $dokument = new Document();
            $vrsta = DocumentVrsta::find()->where(['naziv' => $slug])->one();
            $dokument->naziv = $vrsta->opis . '-' . $zahtjev->dijete0->prezime . ' ' . $zahtjev->dijete0->ime;
            $dokument->filename = $zahtjev->getDirPath() . '/' . $slug . '.pdf';

            $dokument->vrsta_dokumenta = $vrsta->id;
            $dokument->save();
            $docZahtjev = new DocumentZahtjev();
            $docZahtjev->zahtjev = $zahtjevId;
            $docZahtjev->document = $dokument->id;
            $docZahtjev->save();
            $useremail = User::findOne($zahtjev->user)->email;

            $message = Yii::$app->mailer->compose()
                ->setFrom('info@ordybee.com')
                //->setTo('kvargec@gmail.com')
                //->setTo($useremail)
                ->setTo('info@ordybee.com')
                ->setSubject('Message subject')
                ->setTextBody("U privitku se nalazi $dokumentTemplate->naziv za zahtjev $zahtjev->urbroj.")
                ->attach($pdfPath)
                // ->setHtmlBody('<b>HTML content</b>')
                ->send();

            return ['ok' => true, 'message' => Yii::t('app', "Dokument {document} je poslan.", ['document' => $dokumentTemplate->naziv])];
        }

        return ['ok' => false, 'message' => Yii::t('app', 'Sva polja u formi moraju biti popunjena.')];
    }

    public function actionSendRjesenje($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $form = new RjesenjeForm();
        return $this->sendDokument('rjesenjeupis', $form, $id);
    }
    public function actionSendRjesenjeIspis($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $form = new RjesenjeForm();
        return $this->sendDokument('rjesenjeispis', $form, $id);
    }
    public function actionSendOdluka($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $form = new OdlukaForm();
        return $this->sendDokument('odlukaupis', $form, $id);
    }

    public function actionSendUgovor($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $form = new UgovorForm();
        return $this->sendDokument('ugovorupis', $form, $id);
    }



    public function actionRangLista()
    {
        $searchModel = new OcijenjenZahtjevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $upisneGrupe = UpisneGrupe::find()->orderBy('date_poc DESC')->all();
        $podaci = $dataProvider->getModels();

        $rezultati = array();
        foreach ($upisneGrupe as $grupa) {
            $rezultati[$grupa->id] = array();
            foreach ($podaci as $podatak) {
                $dete = Dijete::find()->where(['id' => $podatak['dijete']])->one();
                if (strtotime($dete->dat_rod) >= strtotime($grupa->date_poc) and strtotime($dete->dat_rod) <= strtotime($grupa->date_kraj)) {
                    $rezultati[$grupa->id][] = $podatak;
                }
            }
        }

        $rjesenjeForm = new RjesenjeForm();
        $odlukaForm = new OdlukaForm();
        $ugovorForm = new UgovorForm();

        return $this->render('lista', [
            'upisneGrupe' => $upisneGrupe,
            'rezultati' => $rezultati,
            'rjesenjeForm' => $rjesenjeForm,
            'odlukaForm' => $odlukaForm,
            'ugovorForm' => $ugovorForm,
        ]);
    }
    public function actionRangListaFull()
    {
        $searchModel = new OcijenjenZahtjevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }
    public function actionIspisRangListe($status = 7)
    {
        $searchModel = new OcijenjenZahtjevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $status);
        $upisneGrupe = UpisneGrupe::find()->orderBy('date_poc DESC')->all();
        $podaci = $dataProvider->getModels();

        $rezultati = array();
        foreach ($upisneGrupe as $grupa) {
            $rezultati[$grupa->id] = array();
            foreach ($podaci as $podatak) {
                $dete = Dijete::find()->where(['id' => $podatak['dijete']])->one();
                if (strtotime($dete->dat_rod) >= strtotime($grupa->date_poc) and strtotime($dete->dat_rod) <= strtotime($grupa->date_kraj)) {
                    $rezultati[$grupa->id][] = $podatak;
                }
            }
        };
        $tempTekst = '';

        $formatter = \Yii::$app->formatter;
        foreach ($rezultati as $index => $rezultat) {
            foreach ($upisneGrupe as $grupa) {
                if ($grupa->id == $index) {
                    $nazivGrupe = $grupa;
                    break;
                }
            }
            $tempTekst .= '<h3>' . $nazivGrupe->naziv . '</h3><h4>' . $formatter->asDate($nazivGrupe->date_poc, 'php:d.m.Y.') . '-' . $formatter->asDate($nazivGrupe->date_kraj, 'php:d.m.Y.') . '</h4>';
            $tempTekst .= '<h4>' . Yii::t('app', 'Lista primljene djece') . '</h4>';
            $tempTekst .= '<table class="table table-striped table-bordered">
        <tr>
            <th>#</th>
            <th>' . Yii::t('app', 'Šifra') . '</th>
          
            <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
            <th>' . Yii::t('app', 'Bodovi') . '</th>

         
        </tr>';
            $i = 1;
            $j = 1;
            foreach ($rezultat as $rt) {
                $klasa = 'style="text-align:center"';
                $klasa2 = 'style="text-align:left"';

                $tempTekst .= '<tr>';
                $tempTekst .= '<td>' . $j . '.</td>';
                $temp = isset($rt['urbroj']) ? $rt['urbroj'] : '';
                $temp2 = isset($rt['dijete_dat_rod']) ? $formatter->asDate($rt['dijete_dat_rod'], 'php:d.m.Y.') : '';
                $temp3 = isset($rt['bodovi']) ? $rt['bodovi'] : '';
                $tempTekst .= '<td ' . $klasa . '>' . $temp . '</td>';
                $tempTekst .= '<td ' . $klasa . '>' . $temp2 . '</td>';
                $tempTekst .= '<td ' . $klasa . '>' . $temp3 . '</td>';
                $tempTekst .= '</tr>';
                if ($nazivGrupe->broj == $i) {

                    $tempTekst .= '</table>';
                    $tempTekst .= '<h4>' . Yii::t('app', 'Lista čekanja') . '</h4>';
                    $tempTekst .= '<table class="table table-striped table-bordered" >
                    <tr>
                        <th>#</th>
                        <th>' . Yii::t('app', 'Šifra') . '</th>
                 
                        <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
                        <th>' . Yii::t('app', 'Bodovi') . '</th>
            
                     
                    </tr>';
                    $j = 0;
                } else {
                    $klasa = 'style="text-align:center"';
                }
                $i++;
                $j++;
            }
            $tempTekst .= '</table>';
            $tempTekst .= '<hr />';
        }


        $contentFull = Controller::renderPartial('@common/views/templates/memorandum', [
            'naslov' => Yii::t('app', 'Rang lista upisa i liste čekanja'),
            'content' => $tempTekst
        ]);
        $utils = Utils::ispisPDF($contentFull, 'rangLista', 'download', 'P');
    }
    public function actionIspisRangListe2($status = 7)
    {
        $searchModel = new OcijenjenZahtjevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $status);
        $upisneGrupe = UpisneGrupe::find()->orderBy('date_poc DESC')->all();
        $podaci = $dataProvider->getModels();

        $rezultati = array();
        foreach ($upisneGrupe as $grupa) {
            $rezultati[$grupa->id] = array();
            foreach ($podaci as $podatak) {
                $dete = Dijete::find()->where(['id' => $podatak['dijete']])->one();
                if (strtotime($dete->dat_rod) >= strtotime($grupa->date_poc) and strtotime($dete->dat_rod) <= strtotime($grupa->date_kraj)) {
                    $rezultati[$grupa->id][] = $podatak;
                }
            }
        };
        $tempTekst = '';

        $formatter = \Yii::$app->formatter;
        foreach ($rezultati as $index => $rezultat) {
            foreach ($upisneGrupe as $grupa) {
                if ($grupa->id == $index) {
                    $nazivGrupe = $grupa;
                    break;
                }
            }
            $tempTekst .= '<h3>' . $nazivGrupe->naziv . '</h3><h4>' . $formatter->asDate($nazivGrupe->date_poc, 'php:d.m.Y.') . '-' . $formatter->asDate($nazivGrupe->date_kraj, 'php:d.m.Y.') . '</h4>';
            $tempTekst .= '<h4>' . Yii::t('app', 'Lista primljene djece') . '</h4>';
            $tempTekst .= '<table class="table table-striped table-bordered"  >
        <tr>
            <th>#</th>
            <th>' . Yii::t('app', 'Šifra') . '</th>
            <th>' . Yii::t('app', 'Ime i prezime') . '</th>
            <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
            <th>' . Yii::t('app', 'Bodovi') . '</th>

         
        </tr>';
            $i = 1;
            $j = 1;
            foreach ($rezultat as $rt) {
                $klasa = 'style="text-align:center"';
                $klasa2 = 'style="text-align:left"';

                $tempTekst .= '<tr>';
                $tempTekst .= '<td>' . $j . '.</td>';
                $temp = isset($rt['urbroj']) ? $rt['urbroj'] : '';
                $temp2 = isset($rt['dijete_dat_rod']) ? $formatter->asDate($rt['dijete_dat_rod'], 'php:d.m.Y.') : '';
                $temp3 = isset($rt['bodovi']) ? $rt['bodovi'] : '';
                $tempTekst .= '<td ' . $klasa . '>' . $temp . '</td>';

                $dijete = \common\models\Dijete::find()->where(['id' => $rt['dijete']])->one();
                $imePrezime = isset($dijete) ? $dijete->ime . ' ' . $dijete->prezime : '';
                $tempTekst .= '<td ' . $klasa2 . '>' . $imePrezime . '</td>';
                $tempTekst .= '<td ' . $klasa . '>' . $temp2 . '</td>';
                $tempTekst .= '<td ' . $klasa . '>' . $temp3 . '</td>';
                $tempTekst .= '</tr>';
                if ($nazivGrupe->broj == $i) {

                    $tempTekst .= '</table>';
                    $tempTekst .= '<h4>' . Yii::t('app', 'Lista čekanja') . '</h4>';
                    $tempTekst .= '<table class="table table-striped table-bordered"  >
                    <tr>
                        <th>#</th>
                        <th>' . Yii::t('app', 'Šifra') . '</th>
                        <th>' . Yii::t('app', 'Ime i prezime') . '</th>
                        <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
                        <th>' . Yii::t('app', 'Bodovi') . '</th>
            
                     
                    </tr>';
                    $j = 0;
                } else {
                    $klasa = 'style="text-align:center"';
                }
                $i++;
                $j++;
            }
            $tempTekst .= '</table>';
            $tempTekst .= '<hr />';
        }


        $contentFull = Controller::renderPartial('@common/views/templates/memorandum', [
            'naslov' => Yii::t('app', 'Rang lista upisa i liste čekanja'),
            'content' => $tempTekst
        ]);
        $utils = Utils::ispisPDF($contentFull, 'rangListaInterno', 'download', 'P');
    }
    public function actionIspisRangListeIntervju($status = 7)
    {
        $searchModel = new OcijenjenZahtjevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $status);
        $upisneGrupe = UpisneGrupe::find()->orderBy('date_poc DESC')->all();
        $podaci = $dataProvider->getModels();

        $rezultati = array();
        foreach ($upisneGrupe as $grupa) {
            $rezultati[$grupa->id] = array();
            foreach ($podaci as $podatak) {
                $dete = Dijete::find()->where(['id' => $podatak['dijete']])->one();
                if (strtotime($dete->dat_rod) >= strtotime($grupa->date_poc) and strtotime($dete->dat_rod) <= strtotime($grupa->date_kraj)) {
                    $rezultati[$grupa->id][] = $podatak;
                }
            }
        };
        $tempTekst = '';

        $formatter = \Yii::$app->formatter;
        foreach ($rezultati as $index => $rezultat) {
            foreach ($upisneGrupe as $grupa) {
                if ($grupa->id == $index) {
                    $nazivGrupe = $grupa;
                    break;
                }
            }
            $tempTekst .= '<h3>' . $nazivGrupe->naziv . '</h3><h4>' . $formatter->asDate($nazivGrupe->date_poc, 'php:d.m.Y.') . '-' . $formatter->asDate($nazivGrupe->date_kraj, 'php:d.m.Y.') . '</h4>';
            if ($status == 7) {
                $tempTekst .= '<h4>' . Yii::t('app', 'Lista primljene djece') . '</h4>';
            } else {
                $tempTekst .= '<h4>' . Yii::t('app', 'Lista s nepotpunom dokumentacijom') . '</h4>';
            }
            $tempTekst .= '<table class="table table-striped table-bordered" width="100%">
        <tr>
            <th>#</th>
            <th>' . Yii::t('app', 'Šifra') . '</th>
          
            <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
            <th>' . Yii::t('app', 'Bodovi') . '</th>
            <th>' . Yii::t('app', 'Intervju') . '</th>

         
        </tr>';
            $i = 1;
            $j = 1;
            foreach ($rezultat as $rt) {
                $klasa = 'style="text-align:center"';
                $klasa2 = 'style="text-align:left"';

                $tempTekst .= '<tr>';
                $tempTekst .= '<td>' . $j . '.</td>';
                $temp = isset($rt['urbroj']) ? $rt['urbroj'] : '';
                $temp2 = isset($rt['dijete_dat_rod']) ? $formatter->asDate($rt['dijete_dat_rod'], 'php:d.m.Y.') : '';
                $temp3 = isset($rt['bodovi']) ? $rt['bodovi'] : '';
                $tempTekst .= '<td ' . $klasa . '>' . $temp . '</td>';
                $tempTekst .= '<td ' . $klasa . '>' . $temp2 . '</td>';
                $tempTekst .= '<td ' . $klasa . '>' . $temp3 . '</td>';
                $intervju = \common\models\Intervju::find()->where(['zahtjev' => $rt['id']])->one();
                $datum = isset($intervju) ? $formatter->asDate($intervju->vrijeme, 'php:d.m.Y. H:i') : '';
                $tempTekst .= '<td ' . $klasa . '>' . $datum . '</td>';
                $tempTekst .= '</tr>';
                if ($nazivGrupe->broj == $i and $status == 7) {

                    $tempTekst .= '</table>';
                    $tempTekst .= '<h4>' . Yii::t('app', 'Lista čekanja') . '</h4>';
                    $tempTekst .= '<table class="table table-striped table-bordered" width="100%">
                    <tr>
                        <th>#</th>
                        <th>' . Yii::t('app', 'Šifra') . '</th>
                 
                        <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
                        <th>' . Yii::t('app', 'Bodovi') . '</th>
                        <th>' . Yii::t('app', 'Intervju') . '</th>
                     
                    </tr>';
                    $j = 0;
                } else {
                    $klasa = 'style="text-align:center"';
                }
                $i++;
                $j++;
            }
            $tempTekst .= '</table>';
            $tempTekst .= '<hr />';
        }

        if ($status == 7) {
            $contentFull = Controller::renderPartial('@common/views/templates/memorandum', [
                'naslov' => Yii::t('app', 'Rang lista upisa i liste čekanja'),
                'content' => $tempTekst
            ]);
        } elseif ($status == 8) {
            $contentFull = Controller::renderPartial('@common/views/templates/memorandum', [
                'naslov' => Yii::t('app', 'Rang lista s nepotpunom dokumentacijom'),
                'content' => $tempTekst
            ]);
        }

        $utils = Utils::ispisPDF($contentFull, 'rangListaIntervju' . time(), 'download', 'P');
    }
    public function actionIspisRangListeIntervju2($status = 7)
    {
        $searchModel = new OcijenjenZahtjevSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $status);
        $upisneGrupe = UpisneGrupe::find()->orderBy('date_poc DESC')->all();
        $podaci = $dataProvider->getModels();

        $rezultati = array();
        foreach ($upisneGrupe as $grupa) {
            $rezultati[$grupa->id] = array();
            foreach ($podaci as $podatak) {
                $dete = Dijete::find()->where(['id' => $podatak['dijete']])->one();
                if (strtotime($dete->dat_rod) >= strtotime($grupa->date_poc) and strtotime($dete->dat_rod) <= strtotime($grupa->date_kraj)) {
                    $rezultati[$grupa->id][] = $podatak;
                }
            }
        };
        $tempTekst = '';

        $formatter = \Yii::$app->formatter;
        foreach ($rezultati as $index => $rezultat) {
            foreach ($upisneGrupe as $grupa) {
                if ($grupa->id == $index) {
                    $nazivGrupe = $grupa;
                    break;
                }
            }
            $tempTekst .= '<h3>' . $nazivGrupe->naziv . '</h3><h4>' . $formatter->asDate($nazivGrupe->date_poc, 'php:d.m.Y.') . '-' . $formatter->asDate($nazivGrupe->date_kraj, 'php:d.m.Y.') . '</h4>';
            if ($status == 7) {
                $tempTekst .= '<h4>' . Yii::t('app', 'Lista primljene djece') . '</h4>';
            } else {
                $tempTekst .= '<h4>' . Yii::t('app', 'Lista s nepotpunom dokumentacijom') . '</h4>';
            }
            $tempTekst .= '<table class="table table-striped table-bordered" width="100%">
        <tr>
            <th>#</th>
            <th>' . Yii::t('app', 'Šifra') . '</th>
            <th>' . Yii::t('app', 'Ime i prezime') . '</th>
            <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
            <th>' . Yii::t('app', 'Bodovi') . '</th>
            <th>' . Yii::t('app', 'Intervju') . '</th>

         
        </tr>';
            $i = 1;
            $j = 1;
            foreach ($rezultat as $rt) {
                $klasa = 'style="text-align:center"';
                $klasa2 = 'style="text-align:left"';

                $tempTekst .= '<tr>';
                $tempTekst .= '<td>' . $j . '.</td>';
                $temp = isset($rt['urbroj']) ? $rt['urbroj'] : '';
                $temp2 = isset($rt['dijete_dat_rod']) ? $formatter->asDate($rt['dijete_dat_rod'], 'php:d.m.Y.') : '';
                $temp3 = isset($rt['bodovi']) ? $rt['bodovi'] : '';
                $tempTekst .= '<td ' . $klasa . '>' . $temp . '</td>';

                $dijete = \common\models\Dijete::find()->where(['id' => $rt['dijete']])->one();
                $imePrezime = isset($dijete) ? $dijete->ime . ' ' . $dijete->prezime : '';
                $tempTekst .= '<td ' . $klasa2 . '>' . $imePrezime . '</td>';
                $tempTekst .= '<td ' . $klasa . '>' . $temp2 . '</td>';
                $tempTekst .= '<td ' . $klasa . '>' . $temp3 . '</td>';
                $intervju = \common\models\Intervju::find()->where(['zahtjev' => $rt['id']])->one();

                $datum = isset($intervju) ? $formatter->asDate($intervju->vrijeme, 'php:d.m.Y. H:i') : '';

                $tempTekst .= '<td ' . $klasa . '>' . $datum . '</td>';
                $tempTekst .= '</tr>';
                if ($nazivGrupe->broj == $i and $status == 7) {

                    $tempTekst .= '</table>';
                    $tempTekst .= '<h4>' . Yii::t('app', 'Lista čekanja') . '</h4>';
                    $tempTekst .= '<table class="table table-striped table-bordered" width="100%">
                    <tr>
                        <th>#</th>
                        <th>' . Yii::t('app', 'Šifra') . '</th>
                        <th>' . Yii::t('app', 'Ime i prezime') . '</th>
                        <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
                        <th>' . Yii::t('app', 'Bodovi') . '</th>
                        <th>' . Yii::t('app', 'Intervju') . '</th>
                     
                    </tr>';
                    $j = 0;
                } else {
                    $klasa = 'style="text-align:center"';
                }
                $i++;
                $j++;
            }
            $tempTekst .= '</table>';
            $tempTekst .= '<hr />';
        }


        if ($status == 7) {
            $contentFull = Controller::renderPartial('@common/views/templates/memorandum', [
                'naslov' => Yii::t('app', 'Rang lista upisa i liste čekanja'),
                'content' => $tempTekst
            ]);
        } elseif ($status == 8) {
            $contentFull = Controller::renderPartial('@common/views/templates/memorandum', [
                'naslov' => Yii::t('app', 'Rang lista s nepotpunom dokumentacijom'),
                'content' => $tempTekst
            ]);
        }
        $utils = Utils::ispisPDF($contentFull, 'rangListaIntervjuInterno' . time(), 'download', 'P');
    }
    /**
     * Deletes an existing Zahtjev model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Zahtjev model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zahtjev the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zahtjev::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Generiraj zahtjeve za djecu koja nemaju, koja su ubačena u tablicu
     */
    public function actionGenerirajZahtjeve()
    {
        $djeca_ids = Zahtjev::find()->select('dijete')->asArray()->all();
        $djeca_bez_zahtjeva = Dijete::find()->where(['not in', 'id', array_column($djeca_ids, 'dijete')])->orderBy('prezime,ime ASC')->all();
        $ped_godina = PedagoskaGodina::getAktivna();

        foreach ($djeca_bez_zahtjeva as $dijete) {
            $zahtjev = new Zahtjev;
            $zahtjev->status = 4;
            $zahtjev->dijete = $dijete->id;
            $zahtjev->urbroj = 'auto-generiran';
            $zahtjev->create_at = date("Y-m-d H:i:s", time());
            $zahtjev->update_at = date("Y-m-d H:i:s", time());
            $zahtjev->pd_godina = $ped_godina->id;
            $zahtjev->user = Yii::$app->user->id;
            $zahtjev->save();
        }

        return $this->redirect(['index']);
    }
    public function actionVrstaPotprograma()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = array();
        if (isset($_POST['depdrop_parents'])) {
            $id = $_POST['depdrop_parents'];
            $potprogrami = VrstaPrograma::find()->where(['parent' => $id])->all();
            $selected = false;
            $out[] = ['id' => 0, 'name' => 'Bez potprograma'];
            foreach ($potprogrami as $potprogram) {
                $out[] = ['id' => $potprogram->id, 'name' => $potprogram->vrsta];
            }
            if (count($out) == 1) {
                $selected = 0;
            }
            return ['output' => $out, 'selected' => $selected];
        }
        return ['output' => [], 'selected' => ''];
    }
}
