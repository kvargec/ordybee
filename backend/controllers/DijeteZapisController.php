<?php

namespace backend\controllers;

use common\models\Calendar;
use common\models\Dijete;
use common\models\DijeteSkupina;
use common\models\Dnevnik;
use common\models\DnevnikZapis;
use common\models\Icd10classification;
use common\models\Icd10set;
use common\models\PedagoskaGodina;
use common\models\Prisutnost;
use common\models\Skupina;
use common\models\VrstaZapis;
use DateInterval;
use Yii;
use common\models\DijeteZapis;
use common\models\Search\DijeteZapisSearch;
use yii\base\BaseObject;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Mpdf;
use yii\web\UploadedFile;

/**
 * DijeteZapisController implements the CRUD actions for DijeteZapis model.
 */
class DijeteZapisController extends Controller
{
    private static function getBolestiList($cat_id, $subcat_id)
    {
        $bolesti = Icd10classification::find()->joinWith(['set'])->where(['icd10set.chapter_id' => $cat_id, 'icd10classification.set_id' => $subcat_id])->all();

        $returnArray = [];
        foreach ($bolesti as $item) {
            $tmp = new \stdClass();
            $tmp->id = $item->id;
            $tmp->name = $item->code . " " . $item->name;
            $returnArray[] = $tmp;
        }

        return $returnArray;
    }

    private static function getPodgrupaLista($cat_id)
    {
        $podgrupe = Icd10set::find()->where(['chapter_id' => $cat_id])->all();
        $returnArray = [];
        foreach ($podgrupe as $item) {
            $tmp = new \stdClass();
            $tmp->id = $item->id;
            $tmp->name = $item->codes . " " . $item->name;
            $returnArray[] = $tmp;
        }
        return $returnArray;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DijeteZapis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DijeteZapisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDijete($id, $tip)
    {

        $dataProvider = new SqlDataProvider([
            'sql' => "select dijete,dnevnik_zapis.zapis,created_at,dnevnik,vrsta,json_agg(dijete_zapis.id) from dnevnik_zapis left join dijete_zapis
on dijete_zapis.zapis = dnevnik_zapis.id
left join vrsta_zapis on dnevnik_zapis.vrsta = vrsta_zapis.id
where dijete = " . $id . " and vrsta_zapis.grupa = '" . $tip . "'
group by dnevnik_zapis.zapis,dnevnik,vrsta,created_at,dijete order by created_at desc",
        ]);
        $vrsteZapisa = VrstaZapis::find()->where(['grupa' => $tip])->all();
        $dijete = Dijete::find()->where(['id' => $id])->one();
        return $this->render('dijete', [
            //            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dijete' => $dijete,
            'vrsteZapisa' => $vrsteZapisa,
            'tip' => $tip
        ]);
    }
    /**
     * Displays a single DijeteZapis model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DijeteZapis model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionDatoteka($tip, $dijete)
    {
        $tipZapisa = VrstaZapis::findOne($tip);
        $dijete = Dijete::findOne($dijete);
        $dijeteZapis = new DijeteZapis();
        $dnevnikZapis = new DnevnikZapis();
        $dnevnikZapis->zapis = $dijete->ime . ' ' . $dijete->prezime . ' - ' . $tipZapisa->naziv;
        $model2 = new DynamicModel(['medij']);
        if (isset($_POST['DynamicModel']) && $dnevnikZapis->load(Yii::$app->request->post())) {
        }
    }

    public function actionCreate($tip, $dijete, $grupa)
    {
        $tipZapisa = VrstaZapis::findOne($tip);
        $dijete = Dijete::findOne($dijete);
        $dijeteZapis = new DijeteZapis();
        $dnevnikZapis = new DnevnikZapis();
        $dnevnikZapis->zapis = $dijete->ime . ' ' . $dijete->prezime . ' - ' . $tipZapisa->naziv;

        switch ($tipZapisa->naziv) {
            case 'mjerenje':
                $model2 = new DynamicModel(['visina', 'tezina', 'percentilevisine', 'percentiletezine', 'relativnatezina']);
                $view = '_form';
                break;
            case 'cijepljenje 5u1':
                //$temp = new DynamicModel(['bcg', 'diteper', 'polio', 'moparu', 'hib']);
                //$model2 = [$temp, $temp, $temp];

                $model2 = new DynamicModel(['bcg_1', 'diteper_1', 'polio_1', 'moparu_1', 'hib_1', 'bcg_2', 'diteper_2', 'polio_2', 'moparu_2', 'hib_2', 'bcg_3', 'diteper_3', 'polio_3', 'moparu_3', 'hib_3']);
                //$postojeci_zapisi = DijeteZapis::find()->joinWith(['zapis0'])->where(['dijete' => $dijete->id, 'dnevnik_zapis.vrsta' => $tip])->all();
                //$zapisi_vrijednosti = [];
                //foreach ($postojeci_zapisi as $item) {
                //
                //    $tmp = is_array($item->dodatno) ? $item->dodatno : json_decode($item->dodatno, true);
                //    $zapisi_vrijednosti[key($tmp)][] = $tmp[key($tmp)];
                //}
                //function sortFunction($a, $b)
                //{
                //    return strtotime($a) - strtotime($b);
                //}
                //$zapisi_vrijednosti_sorted = [];
                //$count = 0;
                //foreach ($zapisi_vrijednosti as $key => $item) {
                //    if (count($item) > $count) {
                //        $count = count($item);
                //    }
                //    usort($item, function ($a, $b) {
                //        return   strtotime($a) - strtotime($b);
                //    });
                //    $zapisi_vrijednosti_sorted[$key] = $item;
                //}
                $view = 'cijepljenje5u1_3';
                break;
            case 'cijepljenje 6u1':
                //$temp = new DynamicModel(['bcg', 'diteper', 'polio', 'moparu', 'hib', 'hbv']);
                //$model2 = [$temp, $temp, $temp];

                $model2 = new DynamicModel(['bcg_1', 'diteper_1', 'polio_1', 'moparu_1', 'hib_1', 'hbv_1', 'bcg_2', 'diteper_2', 'polio_2', 'moparu_2', 'hib_2', 'hbv_2', 'bcg_3', 'diteper_3', 'polio_3', 'moparu_3', 'hib_3', 'hbv_3', 'bcg_4', 'diteper_4', 'polio_4', 'moparu_4', 'hib_4', 'hbv_4']);
                //$postojeci_zapisi = DijeteZapis::find()->joinWith(['zapis0'])->where(['dijete' => $dijete->id, 'dnevnik_zapis.vrsta' => $tip])->all();
                //$zapisi_vrijednosti = [];
                //foreach ($postojeci_zapisi as $item) {
                //    $tmp = is_array($item->dodatno) ? $item->dodatno : json_decode($item->dodatno, true);
                //    foreach ($tmp as $key => $item) {
                //        $zapisi_vrijednosti[$key][] = $item;
                //    }
                //}
                //function sortFunction($a, $b)
                //{
                //    return strtotime($a) - strtotime($b);
                //}
                //$zapisi_vrijednosti_sorted = [];
                //$count = 0;
                //foreach ($zapisi_vrijednosti as $key => $item) {
                //    if (count($item) > $count) {
                //        $count = count($item);
                //    }
                //    usort($item, function ($a, $b) {
                //        return   strtotime($a) - strtotime($b);
                //    });
                //    $zapisi_vrijednosti_sorted[$key] = $item;
                //}
                $view = 'cijepljenje6u1_3';
                break;
            case 'standardni':
                $view = 'standardni';
                $model2 = new DynamicModel(['naziv', 'telefon', 'email', 'adresa', 'djelatnost']);
                break;
            case 'bilješka':
                $view = 'obicni-zapis';
                $model2 = new DynamicModel(['datum', 'biljeska']);
                break;
            case 'cijepljenje':
                $view = 'cijepljenje';
                $model2 = new DynamicModel(['datum', 'cjepivo']);
                break;
            case 'bolest':
                $model2 = new DynamicModel(['bolest', 'pocetak_izostanka', 'kraj_izostanka', 'attachments']); //'grupa', 'podgrupa', 'broj_dana',
                $model2->addRule(['pocetak_izostanka', 'kraj_izostanka'], 'required');
                $view = 'bolest';
                break;
            case 'bolest zarazna':
                $model2 = new DynamicModel(['bolest', 'pocetak_izostanka', 'kraj_izostanka', 'attachments']); //'grupa', 'podgrupa', 'broj_dana',
                $model2->addRule(['pocetak_izostanka', 'kraj_izostanka'], 'required');
                $view = 'bolest';
                break;
            case 'osnovni podaci':
                $view = 'standardni';
                $model2 = new DynamicModel(['naziv', 'telefon', 'email', 'adresa', 'djelatnost']);
                break;
            case 'kronična bolest':
                $view = 'kronicnebolesti';
                $model2 = new DynamicModel(['bolest', 'attachments']);
                break;
            case 'alergija':
                $model2 = new DynamicModel(['alergija', 'attachments']);
                $view = 'alergije';
                break;
            case 'poteškoće':
                $model2 = new DynamicModel(['poteskoce']);
                $view = 'poteskoce';
                break;
            case 'observacija':
                $model2 = new DynamicModel(['datum', 'misljenje']);
                $view = 'observacija';
                break;
            case 'nalaz':
                $model2 = new DynamicModel(['datum', 'misljenje', 'nalaz']);
                $zapis = VrstaZapis::find()->where(['naziv' => 'observacija', 'grupa' => $tipZapisa->grupa])->one();
                $tmp = DijeteZapis::find()->joinWith(['zapis0'])->where(['dnevnik_zapis.vrsta' => $zapis->id, 'dijete' => $dijete->id])->orderBy('id DESC')->one();
                if (!empty($tmp)) {
                    $zapis = json_decode($tmp->dodatno);
                    $model2->misljenje = $zapis->misljenje;
                }
                $view = 'nalaz';
                break;
            case 'datoteka':
                $model2 = new DynamicModel(['datum', 'opis', 'attachments']);
                $view = 'datoteka';
                break;
            case 'covid_potvrda':
                $model2 = new DynamicModel(['datum', 'opis', 'attachments']);
                $view = 'datoteka';
                break;
            case 'pracenje_djeteta':
                $model2 = new DynamicModel(['datum', 'misljenje', 'samostalnost1', 'samostalnost5', 'samostalnost2', 'motoricke1', 'motoricke5', 'motoricke2', 'motorickef1', 'motorickef5', 'motorickef2', 'drustvene1', 'drustvene5', 'drustvene2', 'sukob1', 'sukob5', 'sukob2', 'emocije1', 'emocije5', 'emocije2', 'slika1', 'slika5', 'slika2', 'igra1', 'igra5', 'igra2', 'pojmovi1', 'pojmovi5', 'pojmovi2', 'veze1', 'veze5', 'veze2', 'pamcenje1', 'pamcenje5', 'pamcenje2', 'govor1', 'govor5', 'govor2', 'jezik1', 'jezik5', 'jezik2', 'likovne1', 'likovne5', 'likovne2', 'boja1', 'boja5', 'boja2', 'spec1', 'spec5', 'spec2', 'biljeske']);
                $view = 'indpracenje';
                break;
            default:
                $model2 = new DynamicModel(['datum', 'biljeska']);
                $view = 'obicni-zapis';
                break;
        }


        if (isset($_POST['DynamicModel']) && $dnevnikZapis->load(Yii::$app->request->post())) {
            $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
            if ($grupa == 'logoped') {
                $dnevnik = Dnevnik::find()->where(['zaposlenik' => 43, 'ped_godina' => $godina->id])->one();
                if (empty($dnevnik)) {
                    $dnevnik = new Dnevnik();
                    $dnevnik->sifra = '00-43'; //promjeniti da ide id zaposlenika
                    $dnevnik->zaposlenik = 43; //promejeniti da ide id zaposlenika
                    $dnevnik->ped_godina = $godina->id;
                    $dnevnik->vrsta_dnevnika = 1;
                    $dnevnik->save();
                }
            } elseif ($tipZapisa->grupa == 'zdravstvo') {
                $dnevnik = Dnevnik::find()->where(['zaposlenik' => 13, 'ped_godina' => $godina->id])->one();
                if (empty($dnevnik)) {
                    $dnevnik = new Dnevnik();
                    $dnevnik->sifra = '00-13'; //promjeniti da ide id zaposlenika
                    $dnevnik->zaposlenik = 13; //promejeniti da ide id zaposlenika
                    $dnevnik->ped_godina = $godina->id;
                    $dnevnik->vrsta_dnevnika = 1;
                    $dnevnik->save();
                }
            } elseif ($tipZapisa->grupa == 'pedagog') {
                $dnevnik = Dnevnik::find()->where(['zaposlenik' => 41, 'ped_godina' => $godina->id])->one();
                if (empty($dnevnik)) {
                    $dnevnik = new Dnevnik();
                    $dnevnik->sifra = '00-41'; //promjeniti da ide id zaposlenika
                    $dnevnik->zaposlenik = 41; //promejeniti da ide id zaposlenika
                    $dnevnik->ped_godina = $godina->id;
                    $dnevnik->vrsta_dnevnika = 1;
                    $dnevnik->save();
                }
            } elseif ($tipZapisa->grupa == 'odgajatelj') {
                $dnevnik = Dnevnik::find()->where(['zaposlenik' => 42, 'ped_godina' => $godina->id])->one(); // PROMIJENITI ID U ODGOJITELJEV
                if (empty($dnevnik)) {
                    $dnevnik = new Dnevnik();
                    $dnevnik->sifra = '00-42'; //promjeniti da ide id zaposlenika OSTAVLJEN ID OD PSIHOLOGA
                    $dnevnik->zaposlenik = 42; //promejeniti da ide id zaposlenika OSTAVLJEN ID OD PSIHOLOGA
                    $dnevnik->ped_godina = $godina->id;
                    $dnevnik->vrsta_dnevnika = 1;
                    $dnevnik->save();
                }
            } else {
                $dnevnik = Dnevnik::find()->where(['zaposlenik' => 42, 'ped_godina' => $godina->id])->one();
                if (empty($dnevnik)) {
                    $dnevnik = new Dnevnik();
                    $dnevnik->sifra = '00-42'; //promjeniti da ide id zaposlenika
                    $dnevnik->zaposlenik = 42; //promejeniti da ide id zaposlenika
                    $dnevnik->ped_godina = $godina->id;
                    $dnevnik->vrsta_dnevnika = 1;
                    $dnevnik->save();
                }
            }

            switch ($tipZapisa->naziv) {
                    //case 'cijepljenje 5u1':
                    //    foreach ($_POST['DynamicModel'] as $key => $value) {
                    //        $tmp = [];
                    //        $tmp[$key] = $value;
                    //        $dnevnikZapistmp = new DnevnikZapis();
                    //        $dnevnikZapistmp->zapis = $dnevnikZapis->zapis;
                    //        $dnevnikZapistmp->vrsta = $tip;
                    //        $dnevnikZapistmp->dnevnik = $dnevnik->id;
                    //        $dnevnikZapistmp->created_at = date('Y-m-d H:i:s', time());
                    //        $dnevnikZapistmp->updated_at = date('Y-m-d H:i:s', time());
                    //        $dnevnikZapistmp->vrijednosti_atributa = $tmp;
                    //        $dnevnikZapistmp->save();
                    //        $dijeteZapistmp = new DijeteZapis();
                    //        $dijeteZapistmp->dijete = $dijete->id;
                    //        $dijeteZapistmp->zapis = $dnevnikZapistmp->id;
                    //        $dijeteZapistmp->dodatno = $tmp;
                    //        $dijeteZapistmp->save();
                    //    }
                    //    break;
                case 'datoteka':
                    if (empty(UploadedFile::getInstances($model2, 'attachments'))) {
                        return true;
                    }
                    $attachments = UploadedFile::getInstances($model2, 'attachments');

                    if (!file_exists(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id)) {
                        mkdir(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id, 0777, true);
                    }
                    $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id;

                    $realAttachments = [];

                    foreach ($attachments as $file) {
                        $file_name = time() . preg_replace("/\s+/", "_", $file->baseName);
                        $realAttachments[] = $file_name . '.' . $file->extension;
                        //var_dump($this->getAttachDir() . $file->baseName . '.' . $file->extension);
                        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
                    }
                    $attachments = ['attachments' => $realAttachments];
                    $dnevnikZapistmp = new DnevnikZapis();
                    $dnevnikZapistmp->zapis = $dnevnikZapis->zapis;
                    $dnevnikZapistmp->vrsta = $tip;
                    $dnevnikZapistmp->dnevnik = $dnevnik->id;
                    $dnevnikZapistmp->created_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->updated_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->vrijednosti_atributa = json_encode(['datum' => $_POST['DynamicModel']['datum'], 'opis' => $_POST['DynamicModel']['opis'], 'attachments' => $realAttachments]);
                    $dnevnikZapistmp->attachments = $attachments;
                    $dnevnikZapistmp->save();
                    $dijeteZapistmp = new DijeteZapis();
                    $dijeteZapistmp->dijete = $dijete->id;
                    $dijeteZapistmp->zapis = $dnevnikZapistmp->id;
                    $dijeteZapistmp->dodatno = json_encode(['datum' => $_POST['DynamicModel']['datum'], 'opis' => $_POST['DynamicModel']['opis'], 'attachments' => $realAttachments]);
                    $dijeteZapistmp->save();
                    break;
                case 'covid_potvrda':
                    $attachments = UploadedFile::getInstances($model2, 'attachments');

                    if (!file_exists(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id)) {
                        mkdir(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id, 0777, true);
                    }
                    $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id;

                    $realAttachments = [];

                    foreach ($attachments as $file) {
                        $file_name = time() . preg_replace("/\s+/", "_", $file->baseName);
                        $realAttachments[] = $file_name . '.' . $file->extension;
                        //var_dump($this->getAttachDir() . $file->baseName . '.' . $file->extension);
                        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
                    }
                    $attachments = ['attachments' => $realAttachments];
                    $dnevnikZapistmp = new DnevnikZapis();
                    $dnevnikZapistmp->zapis = $dnevnikZapis->zapis;
                    $dnevnikZapistmp->vrsta = $tip;
                    $dnevnikZapistmp->dnevnik = $dnevnik->id;
                    $dnevnikZapistmp->created_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->updated_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->vrijednosti_atributa = json_encode(['datum' => $_POST['DynamicModel']['datum'], 'opis' => $_POST['DynamicModel']['opis'], 'attachments' => $realAttachments]);
                    $dnevnikZapistmp->attachments = $attachments;
                    $dnevnikZapistmp->save();
                    $dijeteZapistmp = new DijeteZapis();
                    $dijeteZapistmp->dijete = $dijete->id;
                    $dijeteZapistmp->zapis = $dnevnikZapistmp->id;
                    $dijeteZapistmp->dodatno = json_encode(['datum' => $_POST['DynamicModel']['datum'], 'opis' => $_POST['DynamicModel']['opis'], 'attachments' => $realAttachments]);
                    $dijeteZapistmp->save();
                    break;
                case 'bolest':
                case 'bolest zarazna':
                    $attachments = UploadedFile::getInstances($model2, 'attachments');

                    if (!file_exists(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id)) {
                        mkdir(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id, 0777, true);
                    }
                    $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id;

                    $realAttachments = [];

                    foreach ($attachments as $file) {
                        $file_name = time() . preg_replace("/\s+/", "_", $file->baseName);
                        $realAttachments[] = $file_name . '.' . $file->extension;
                        //var_dump($this->getAttachDir() . $file->baseName . '.' . $file->extension);
                        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
                    }
                    $attachments = ['attachments' => $realAttachments];
                    $dnevnikZapistmp = new DnevnikZapis();
                    $dnevnikZapistmp->zapis = $dnevnikZapis->zapis;
                    $dnevnikZapistmp->vrsta = $tip;
                    $dnevnikZapistmp->dnevnik = $dnevnik->id;
                    $dnevnikZapistmp->created_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->updated_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->vrijednosti_atributa = json_encode(['bolest' => $_POST['DynamicModel']['bolest'], 'pocetak_izostanka' => $_POST['DynamicModel']['pocetak_izostanka'], 'kraj_izostanka' => $_POST['DynamicModel']['kraj_izostanka'], 'attachments' => $realAttachments]); //'grupa' => $_POST['DynamicModel']['grupa'], 'podgrupa' => $_POST['DynamicModel']['podgrupa'], 'broj_dana' => $_POST['DynamicModel']['broj_dana'],
                    $dnevnikZapistmp->attachments = $attachments;
                    $dnevnikZapistmp->save();
                    $dijeteZapistmp = new DijeteZapis();
                    $dijeteZapistmp->dijete = $dijete->id;
                    $dijeteZapistmp->zapis = $dnevnikZapistmp->id;
                    $dijeteZapistmp->dodatno = json_encode(['bolest' => $_POST['DynamicModel']['bolest'], 'pocetak_izostanka' => $_POST['DynamicModel']['pocetak_izostanka'], 'kraj_izostanka' => $_POST['DynamicModel']['kraj_izostanka'], 'attachments' => $realAttachments]); //'grupa' => $_POST['DynamicModel']['grupa'], 'podgrupa' => $_POST['DynamicModel']['podgrupa'], 'broj_dana' => $_POST['DynamicModel']['broj_dana'],
                    $dijeteZapistmp->save();
                    break;
                case 'kronična bolest':
                    $attachments = UploadedFile::getInstances($model2, 'attachments');

                    if (!file_exists(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id)) {
                        mkdir(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id, 0777, true);
                    }
                    $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id;

                    $realAttachments = [];

                    foreach ($attachments as $file) {
                        $file_name = time() . preg_replace("/\s+/", "_", $file->baseName);
                        $realAttachments[] = $file_name . '.' . $file->extension;
                        //var_dump($this->getAttachDir() . $file->baseName . '.' . $file->extension);
                        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
                    }
                    $attachments = ['attachments' => $realAttachments];
                    $dnevnikZapistmp = new DnevnikZapis();
                    $dnevnikZapistmp->zapis = $dnevnikZapis->zapis;
                    $dnevnikZapistmp->vrsta = $tip;
                    $dnevnikZapistmp->dnevnik = $dnevnik->id;
                    $dnevnikZapistmp->created_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->updated_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->vrijednosti_atributa = json_encode(['bolest' => $_POST['DynamicModel']['bolest'], 'attachments' => $realAttachments]);
                    $dnevnikZapistmp->attachments = $attachments;
                    $dnevnikZapistmp->save();
                    $dijeteZapistmp = new DijeteZapis();
                    $dijeteZapistmp->dijete = $dijete->id;
                    $dijeteZapistmp->zapis = $dnevnikZapistmp->id;
                    $dijeteZapistmp->dodatno = json_encode(['bolest' => $_POST['DynamicModel']['bolest'], 'attachments' => $realAttachments]);
                    $dijeteZapistmp->save();
                    break;
                case 'alergija':
                    $attachments = UploadedFile::getInstances($model2, 'attachments');

                    if (!file_exists(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id)) {
                        mkdir(Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id, 0777, true);
                    }
                    $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $dijete->id;

                    $realAttachments = [];

                    foreach ($attachments as $file) {
                        $file_name = time() . preg_replace("/\s+/", "_", $file->baseName);
                        $realAttachments[] = $file_name . '.' . $file->extension;
                        //var_dump($this->getAttachDir() . $file->baseName . '.' . $file->extension);
                        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
                    }
                    $attachments = ['attachments' => $realAttachments];
                    $dnevnikZapistmp = new DnevnikZapis();
                    $dnevnikZapistmp->zapis = $dnevnikZapis->zapis;
                    $dnevnikZapistmp->vrsta = $tip;
                    $dnevnikZapistmp->dnevnik = $dnevnik->id;
                    $dnevnikZapistmp->created_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->updated_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapistmp->vrijednosti_atributa = json_encode(['alergija' => $_POST['DynamicModel']['alergija'], 'attachments' => $realAttachments]);
                    $dnevnikZapistmp->attachments = $attachments;
                    $dnevnikZapistmp->save();
                    $dijeteZapistmp = new DijeteZapis();
                    $dijeteZapistmp->dijete = $dijete->id;
                    $dijeteZapistmp->zapis = $dnevnikZapistmp->id;
                    $dijeteZapistmp->dodatno = json_encode(['alergija' => $_POST['DynamicModel']['alergija'], 'attachments' => $realAttachments]);
                    $dijeteZapistmp->save();
                    break;
                default:
                    $dnevnikZapis->vrsta = $tip;
                    $dnevnikZapis->dnevnik = $dnevnik->id;
                    $dnevnikZapis->created_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapis->updated_at = date('Y-m-d H:i:s', time());

                    $dnevnikZapis->vrijednosti_atributa = json_encode($_POST['DynamicModel']);
                    $dnevnikZapis->save(false);
                    $dijeteZapis->dijete = $dijete->id;
                    $dijeteZapis->zapis = $dnevnikZapis->id;
                    $dijeteZapis->dodatno = json_encode($_POST['DynamicModel']);
                    $dijeteZapis->save();
                    break;
            }
            //brisanje prisutnosti ako je zapis bolest
            if ($tipZapisa->naziv == 'bolest') {
                $pocetak_bolest = new \DateTime($_POST['DynamicModel']['pocetak_izostanka']);
                $kraj_bolesti = new \DateTime($_POST['DynamicModel']['kraj_izostanka']);
                $kraj_bolesti = $kraj_bolesti->modify('+1 day'); //za petlju da ukluci i zadnji dan
                $interval = DateInterval::createFromDateString('1 day');
                $period = new \DatePeriod($pocetak_bolest, $interval, $kraj_bolesti);
                foreach ($period as $dan) { //iterate through every including start and end date
                    $praznik = Calendar::find()->where(['dan' => "\"" . $dan->format('Y-m-d') . "\""])->one();
                    if ((empty($praznik) || $praznik->vrsta != 2) && $dan->format('N') <= 5) { //ako dan nije praznik ili vikend
                        $tmp = Prisutnost::find()->where(['datum' => "\"" . $dan->format('Y-m-d') . "\"", 'entitet' => 'dijete', 'entitet_id' => $dijete->id])->one();
                        if (!empty($tmp)) {
                            $tmp->updateAttributes(['status' => 0, 'razlog_izostanka' => 'zdravstveni']);
                        } else {
                            $tmp = new Prisutnost();
                            $tmp->entitet_id = $dijete->id;
                            $tmp->entitet = 'dijete';
                            $tmp->status = 0;
                            $tmp->razlog_izostanka = 'zdravstveni';
                            $tmp->datum = $dan->format('Y-m-d');
                            $tmp->trajanje = 8;
                            $tmp->save();
                        }
                    }
                }
            }

            return $this->redirect(['dijete', 'id' => $dijete->id, 'tip' => $tipZapisa->grupa]);
        }

        return $this->render('create', [
            'view' => $view,
            'model2' => $model2,
            'dnevnikZapis' => $dnevnikZapis,
            'zapisi_vrijednosti' => isset($zapisi_vrijednosti_sorted) ? $zapisi_vrijednosti_sorted : ''
        ]);
    }

    public function actionDeleteFile()
    {
        if (isset($_POST)) {
            $id = $_POST['model_id'];
            $key = $_POST['key'];
            $dijeteZapis = $this->findModel($id);
            $dnevnikZapis = DnevnikZapis::find()->where(['id' => $dijeteZapis->zapis])->one();
            $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $dijeteZapis->dijete;
            $jsonDnevnikZapis = json_decode($dnevnikZapis->vrijednosti_atributa, true);
            $jsonDijeteZapis = json_decode($dijeteZapis->dodatno, true);
            $allFiles = $dnevnikZapis->attachments['attachments'];
            $allFilesDnevnikZapis = $jsonDnevnikZapis['attachments'];
            $allFilesDijeteZapis = $jsonDijeteZapis['attachments'];
            $file = array_search($key, $dnevnikZapis->attachments['attachments']);
            $fileDnevnikZapis = array_search($key, $jsonDnevnikZapis['attachments']);
            $fileDijeteZapis = array_search($key, $jsonDijeteZapis['attachments']);



            if ($file !== false || $fileDnevnikZapis !== false || $fileDijeteZapis !== false) {
                if ($file !== false) {
                    unset($allFiles[$file]);
                    $attachments = array_values($allFiles);
                    $dnevnikZapis->attachments = ['attachments' => $attachments];
                    $dnevnikZapis->save();
                }
                if ($fileDnevnikZapis !== false) {
                    unset($allFilesDnevnikZapis[$file]);
                    $attachments = array_values($allFilesDnevnikZapis);
                    $jsonDnevnikZapis['attachments'] = $attachments;
                    $dnevnikZapis->vrijednosti_atributa = json_encode($jsonDnevnikZapis);
                    $dnevnikZapis->save();
                }
                if ($fileDijeteZapis !== false) {
                    unset($allFilesDijeteZapis[$file]);
                    $attachments = array_values($allFilesDijeteZapis);
                    $jsonDijeteZapis['attachments'] = $attachments;
                    $dijeteZapis->dodatno = json_encode($jsonDijeteZapis);
                    $dijeteZapis->save();
                }
                unlink($dir . '/' . $key);
            }
            $out = [];
            return json_encode($out);
        }
    }

    /**
     * Updates an existing DijeteZapis model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dnevnikZapis = DnevnikZapis::findOne($model->zapis);
        if (is_array($dnevnikZapis->vrijednosti_atributa)) {
            $tmp = $dnevnikZapis->vrijednosti_atributa;
        } else {
            $tmp = json_decode($dnevnikZapis->vrijednosti_atributa);
            if (!is_object($tmp)) {
                $tmp = json_decode($tmp);
            }
        }

        switch ($dnevnikZapis->vrsta0->naziv) {
            case 'mjerenje':
                $model2 = new DynamicModel(['visina', 'tezina', 'percentilevisine', 'percentiletezine', 'relativnatezina']);
                $model2->visina = $tmp->visina;
                $model2->tezina = $tmp->tezina;
                $model2->percentilevisine = $tmp->percentilevisine;
                $model2->percentiletezine = $tmp->percentiletezine;
                $model2->relativnatezina = $tmp->relativnatezina;
                $view = '_form';
                break;
            case 'cijepljenje 5u1':
                $model2 = new DynamicModel(['bcg_1', 'diteper_1', 'polio_1', 'moparu_1', 'hib_1', 'bcg_2', 'diteper_2', 'polio_2', 'moparu_2', 'hib_2', 'bcg_3', 'diteper_3', 'polio_3', 'moparu_3', 'hib_3']);
                //$model2 = new DynamicModel(['bcg', 'diteper', 'polio', 'moparu', 'hib']);
                $model2->bcg_1 = $tmp->bcg_1;
                $model2->diteper_1 = $tmp->diteper_1;
                $model2->polio_1 = $tmp->polio_1;
                $model2->moparu_1 = $tmp->moparu_1;
                $model2->hib_1 = $tmp->hib_1;
                $model2->bcg_2 = $tmp->bcg_2;
                $model2->diteper_2 = $tmp->diteper_2;
                $model2->polio_2 = $tmp->polio_2;
                $model2->moparu_2 = $tmp->moparu_2;
                $model2->hib_2 = $tmp->hib_2;
                $model2->bcg_3 = $tmp->bcg_3;
                $model2->diteper_3 = $tmp->diteper_3;
                $model2->polio_3 = $tmp->polio_3;
                $model2->moparu_3 = $tmp->moparu_3;
                $model2->hib_3 = $tmp->hib_3;
                $view = 'cijepljenje5u1_3';
                break;
            case 'cijepljenje 6u1':
                //$model2 = new DynamicModel(['bcg', 'diteper', 'polio', 'moparu', 'hib', 'hbv']);
                $model2 = new DynamicModel(['bcg_1', 'diteper_1', 'polio_1', 'moparu_1', 'hib_1', 'hbv_1', 'bcg_2', 'diteper_2', 'polio_2', 'moparu_2', 'hib_2', 'hbv_2', 'bcg_3', 'diteper_3', 'polio_3', 'moparu_3', 'hib_3', 'hbv_3', 'bcg_4', 'diteper_4', 'polio_4', 'moparu_4', 'hib_4', 'hbv_4']);
                $model2->bcg_1 = $tmp->bcg_1;
                $model2->diteper_1 = $tmp->diteper_1;
                $model2->polio_1 = $tmp->polio_1;
                $model2->moparu_1 = $tmp->moparu_1;
                $model2->hib_1 = $tmp->hib_1;
                $model2->hbv_1 = $tmp->hbv_1;
                $model2->bcg_2 = $tmp->bcg_2;
                $model2->diteper_2 = $tmp->diteper_2;
                $model2->polio_2 = $tmp->polio_2;
                $model2->moparu_2 = $tmp->moparu_2;
                $model2->hib_2 = $tmp->hib_2;
                $model2->hbv_2 = $tmp->hbv_2;
                $model2->bcg_3 = $tmp->bcg_3;
                $model2->diteper_3 = $tmp->diteper_3;
                $model2->polio_3 = $tmp->polio_3;
                $model2->moparu_3 = $tmp->moparu_3;
                $model2->hib_3 = $tmp->hib_3;
                $model2->hbv_3 = $tmp->hbv_3;
                $model2->bcg_4 = $tmp->bcg_4;
                $model2->diteper_4 = $tmp->diteper_4;
                $model2->polio_4 = $tmp->polio_4;
                $model2->moparu_4 = $tmp->moparu_4;
                $model2->hib_4 = $tmp->hib_4;
                $model2->hbv_4 = $tmp->hbv_4;
                $view = 'cijepljenje6u1_3';
                break;
            case 'standardni':
                $model2 = new DynamicModel(['naziv', 'telefon', 'email', 'adresa', 'djelatnost']);
                $model2->naziv = $tmp->naziv;
                $model2->telefon = $tmp->telefon;
                $model2->email = $tmp->email;
                $model2->adresa = $tmp->adresa;
                $model2->djelatnost = $tmp->djelatnost;
                $view = 'standardni';

                break;
            case 'bilješka':
                $view = 'obicni-zapis';
                $model2 = new DynamicModel(['datum', 'biljeska']);
                $model2->datum = $tmp->datum;
                $model2->biljeska = $tmp->biljeska;
                break;
            case 'cijepljenje':
                break;
            case 'bolest':
                $model2 = new DynamicModel(['bolest', 'pocetak_izostanka', 'kraj_izostanka', 'attachments']); //'grupa', 'podgrupa', broj_dana
                $model2->bolest = (int) $tmp->bolest;
                $model2->pocetak_izostanka = $tmp->pocetak_izostanka;
                $model2->kraj_izostanka = $tmp->kraj_izostanka;
                $model2->attachments = $tmp->attachments;
                $view = 'bolest';
                break;
            case 'bolest zarazna':
                $model2 = new DynamicModel(['bolest', 'pocetak_izostanka', 'kraj_izostanka', 'attachments']); //'grupa', 'podgrupa', broj_dana
                $model2->bolest = (int) $tmp->bolest;
                $model2->pocetak_izostanka = $tmp->pocetak_izostanka;
                $model2->kraj_izostanka = $tmp->kraj_izostanka;
                $model2->attachments = $tmp->attachments;
                $view = 'bolest';
                break;
            case 'osnovni podaci':
                break;
            case 'kronična bolest':
                $model2 = new DynamicModel(['bolest', 'attachments']);
                $model2->bolest = $tmp->bolest;
                $model2->attachments = $tmp->attachments;
                $view = 'kronicnebolesti';
                break;
            case 'alergija':
                $model2 = new DynamicModel(['alergija', 'attachments']);
                $model2->alergija = $tmp->alergija;
                $model2->attachments = $tmp->attachments;
                $view = 'alergije';
                break;
            case 'poteškoće':
                $model2 = new DynamicModel(['poteskoce']);
                $model2->poteskoce = $tmp->poteskoce;
                $view = 'poteskoce';
                break;
            case 'observacija':
                $model2 = new DynamicModel(['datum', 'misljenje']);
                $model2->datum = $tmp->datum;
                $model2->misljenje = $tmp->misljenje;
                $view = 'observacija';
                break;
            case 'nalaz':
                $model2 = new DynamicModel(['datum', 'misljenje', 'nalaz']);
                $model2->datum = $tmp->datum;
                $model2->misljenje = $tmp->misljenje;
                $model2->nalaz = $tmp->nalaz;
                $view = 'nalaz';
                break;
            case 'covid_potvrda':
                $model2 = new DynamicModel(['id', 'datum', 'opis', 'attachments']);
                $model2->datum = $tmp->datum;
                $model2->opis = $tmp->opis;
                $model2->attachments = $tmp->attachments;
                $view = 'datoteka';
                break;
            case 'pracenje_djeteta':
                $model2 = new DynamicModel(['datum', 'misljenje', 'samostalnost1', 'samostalnost5', 'samostalnost2', 'motoricke1', 'motoricke5', 'motoricke2', 'motorickef1', 'motorickef5', 'motorickef2', 'drustvene1', 'drustvene5', 'drustvene2', 'sukob1', 'sukob5', 'sukob2', 'emocije1', 'emocije5', 'emocije2', 'slika1', 'slika5', 'slika2', 'igra1', 'igra5', 'igra2', 'pojmovi1', 'pojmovi5', 'pojmovi2', 'veze1', 'veze5', 'veze2', 'pamcenje1', 'pamcenje5', 'pamcenje2', 'govor1', 'govor5', 'govor2', 'jezik1', 'jezik5', 'jezik2', 'likovne1', 'likovne5', 'likovne2', 'boja1', 'boja5', 'boja2', 'spec1', 'spec5', 'spec2', 'biljeske']);
                $model2->samostalnost1 = $tmp->samostalnost1;
                isset($tmp->samostalnost5) ? $model2->samostalnost5 = $tmp->samostalnost5 : $model2->samostalnost5 = '';
                $model2->samostalnost2 = $tmp->samostalnost2;
                $model2->motoricke1 = $tmp->motoricke1;
                isset($tmp->motoricke5) ? $model2->motoricke5 = $tmp->motoricke5 : $model2->motoricke5 = '';
                $model2->motoricke2 = $tmp->motoricke2;
                $model2->motorickef1 = $tmp->motorickef1;
                isset($tmp->motorickef5) ? $model2->motorickef5 = $tmp->motorickef5 : $model2->motorickef5 = '';
                $model2->motorickef2 = $tmp->motorickef2;
                $model2->drustvene1 = $tmp->drustvene1;
                isset($tmp->drustvene5) ? $model2->drustvene5 = $tmp->drustvene5 : $model2->drustvene5 = '';
                $model2->drustvene2 = $tmp->drustvene2;
                $model2->sukob1 = $tmp->sukob1;
                isset($tmp->sukob5) ? $model2->sukob5 = $tmp->sukob5 : $model2->sukob5 = '';
                $model2->sukob2 = $tmp->sukob2;
                $model2->emocije1 = $tmp->emocije1;
                isset($tmp->emocije5) ? $model2->emocije5 = $tmp->emocije5 : $model2->emocije5 = '';
                $model2->emocije2 = $tmp->emocije2;
                $model2->slika1 = $tmp->slika1;
                isset($tmp->slika5) ? $model2->slika5 = $tmp->slika5 : $model2->slika5 = '';
                $model2->slika2 = $tmp->slika2;
                $model2->igra1 = $tmp->igra1;
                isset($tmp->igra5) ? $model2->igra5 = $tmp->igra5 : $model2->igra5 = '';
                $model2->igra2 = $tmp->igra2;
                $model2->pojmovi1 = $tmp->pojmovi1;
                isset($tmp->pojmovi5) ? $model2->pojmovi5 = $tmp->pojmovi5 : $model2->pojmovi5 = '';
                $model2->pojmovi2 = $tmp->pojmovi2;
                $model2->veze1 = $tmp->veze1;
                isset($tmp->veze5) ? $model2->veze5 = $tmp->veze5 : $model2->veze5 = '';
                $model2->veze2 = $tmp->veze2;
                $model2->pamcenje1 = $tmp->pamcenje1;
                isset($tmp->pamcenje5) ? $model2->pamcenje5 = $tmp->pamcenje5 : $model2->pamcenje5 = '';
                $model2->pamcenje2 = $tmp->pamcenje2;
                $model2->govor1 = $tmp->govor1;
                isset($tmp->govor5) ? $model2->govor5 = $tmp->govor5 : $model2->govor5 = '';
                $model2->govor2 = $tmp->govor2;
                $model2->jezik1 = $tmp->jezik1;
                isset($tmp->jezik5) ? $model2->jezik5 = $tmp->jezik5 : $model2->jezik5 = '';
                $model2->jezik2 = $tmp->jezik2;
                $model2->likovne1 = $tmp->likovne1;
                isset($tmp->likovne5) ? $model2->likovne5 = $tmp->likovne5 : $model2->likovne5 = '';
                $model2->likovne2 = $tmp->likovne2;
                $model2->boja1 = $tmp->boja1;
                isset($tmp->boja5) ? $model2->boja5 = $tmp->boja5 : $model2->boja5 = '';
                $model2->boja2 = $tmp->boja2;
                $model2->spec1 = $tmp->spec1;
                isset($tmp->spec5) ? $model2->spec5 = $tmp->spec5 : $model2->spec5 = '';
                $model2->spec2 = $tmp->spec2;
                $model2->biljeske = $tmp->biljeske;

                $view = 'indpracenje';
                break;
            default:
                $model2 = new DynamicModel(['datum', 'opis']);
                $model2->datum = $tmp->datum;
                $model2->opis = $tmp->opis;
                $view = 'vjezba';
                break;
        }
        if ($model2->load(Yii::$app->request->post()) && $dnevnikZapis->load(Yii::$app->request->post())) {
            switch ($dnevnikZapis->vrsta0->naziv) {
                case 'covid_potvrda':
                    $attachments = UploadedFile::getInstances($model2, 'attachments');
                    if (!file_exists(Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id)) {
                        mkdir(Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id, 0777, true);
                    }
                    $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id;
                    $realAttachments = [];
                    foreach ($attachments as $file) {
                        $file_name = time() . preg_replace("/\s+/", "_", $file->baseName);
                        $realAttachments[] = $file_name . '.' . $file->extension;
                        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
                    }
                    $attachments = ['attachments' => $realAttachments];
                    $dnevnikZapis->updated_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapis->vrijednosti_atributa = json_encode(['datum' => $_POST['DynamicModel']['datum'], 'opis' => $_POST['DynamicModel']['opis'], 'attachments' => $realAttachments]);
                    $dnevnikZapis->attachments = $attachments;
                    $dnevnikZapis->save();
                    $model->dodatno = json_encode(['datum' => $_POST['DynamicModel']['datum'], 'opis' => $_POST['DynamicModel']['opis'], 'attachments' => $realAttachments]);
                    $model->save();
                    break;
                case 'bolest':
                case 'bolest zarazna':
                    $attachments = UploadedFile::getInstances($model2, 'attachments');
                    if (!file_exists(Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id)) {
                        mkdir(Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id, 0777, true);
                    }
                    $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id;
                    $realAttachments = [];
                    foreach ($attachments as $file) {
                        $file_name = time() . preg_replace("/\s+/", "_", $file->baseName);
                        $realAttachments[] = $file_name . '.' . $file->extension;
                        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
                    }
                    $attachments = ['attachments' => $realAttachments];
                    $dnevnikZapis->updated_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapis->vrijednosti_atributa = json_encode(['bolest' => $_POST['DynamicModel']['bolest'], 'pocetak_izostanka' => $_POST['DynamicModel']['pocetak_izostanka'], 'kraj_izostanka' => $_POST['DynamicModel']['kraj_izostanka'], 'attachments' => $realAttachments]); //'grupa' => $_POST['DynamicModel']['grupa'], 'podgrupa' => $_POST['DynamicModel']['podgrupa'], 'broj_dana' => $_POST['DynamicModel']['broj_dana'],
                    $dnevnikZapis->attachments = $attachments;
                    $dnevnikZapis->save();
                    $model->dodatno = json_encode(['bolest' => $_POST['DynamicModel']['bolest'], 'pocetak_izostanka' => $_POST['DynamicModel']['pocetak_izostanka'], 'kraj_izostanka' => $_POST['DynamicModel']['kraj_izostanka'], 'attachments' => $realAttachments]); //'grupa' => $_POST['DynamicModel']['grupa'], 'podgrupa' => $_POST['DynamicModel']['podgrupa'], 'broj_dana' => $_POST['DynamicModel']['broj_dana'],
                    $model->save();
                    break;
                case 'kronična bolest':
                    $attachments = UploadedFile::getInstances($model2, 'attachments');
                    if (!file_exists(Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id)) {
                        mkdir(Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id, 0777, true);
                    }
                    $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id;
                    $realAttachments = [];
                    foreach ($attachments as $file) {
                        $file_name = time() . preg_replace("/\s+/", "_", $file->baseName);
                        $realAttachments[] = $file_name . '.' . $file->extension;
                        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
                    }
                    $attachments = ['attachments' => $realAttachments];
                    $dnevnikZapis->updated_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapis->vrijednosti_atributa = json_encode(['bolest' => $_POST['DynamicModel']['bolest'], 'attachments' => $realAttachments]);
                    $dnevnikZapis->attachments = $attachments;
                    $dnevnikZapis->save();
                    $model->dodatno = json_encode(['bolest' => $_POST['DynamicModel']['bolest'], 'attachments' => $realAttachments]);
                    $model->save();
                    break;
                case 'alergija':
                    $attachments = UploadedFile::getInstances($model2, 'attachments');
                    if (!file_exists(Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id)) {
                        mkdir(Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id, 0777, true);
                    }
                    $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $model->dijete0->id;
                    $realAttachments = [];
                    foreach ($attachments as $file) {
                        $file_name = time() . preg_replace("/\s+/", "_", $file->baseName);
                        $realAttachments[] = $file_name . '.' . $file->extension;
                        $file->saveAs($dir . '/' . $file_name . '.' . $file->extension);
                    }
                    $attachments = ['attachments' => $realAttachments];
                    $dnevnikZapis->updated_at = date('Y-m-d H:i:s', time());
                    $dnevnikZapis->vrijednosti_atributa = json_encode(['alergija' => $_POST['DynamicModel']['alergija'], 'attachments' => $realAttachments]);
                    $dnevnikZapis->attachments = $attachments;
                    $dnevnikZapis->save();
                    $model->dodatno = json_encode(['alergija' => $_POST['DynamicModel']['alergija'], 'attachments' => $realAttachments]);
                    $model->save();
                    break;
                default:
                    $dnevnikZapis->vrijednosti_atributa = json_encode($_POST['DynamicModel']);
                    $model->dodatno = json_encode($_POST['DynamicModel']);
                    $model->save();
                    $dnevnikZapis->save();
                    break;
            }

            return $this->redirect(['dijete', 'id' => $model->dijete0->id, 'tip' => $model->zapis0->vrsta0->grupa]);
        }

        return $this->render('update', [
            'view' => $view,
            'model2' => $model2,
            'modelId' => $model->id,
            'dnevnikZapis' => $dnevnikZapis,
        ]);
    }

    /**
     * Deletes an existing DijeteZapis model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $dijete_zapis = $this->findModel($id);
        $dnevnik_zapis = DnevnikZapis::find()->where(['id' => $dijete_zapis->zapis])->one();

        $idDijete = $dijete_zapis->dijete0->id;
        $grupaZapisa = $dijete_zapis->zapis0->vrsta0->grupa;
        $dir = Yii::getAlias('@backend') . '/web/media/dijete/' . $idDijete;

        $allFiles = $dnevnik_zapis->attachments['attachments'];
        foreach ($allFiles as $file) {
            unlink($dir . '/' . $file);
        }
        $dnevnik_zapis->delete();
        $dijete_zapis->delete();

        return $this->redirect(['dijete', 'id' => $idDijete, 'tip' => $grupaZapisa]);
    }

    /**
     * Finds the DijeteZapis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DijeteZapis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DijeteZapis::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionMjerenjeSkupina($skupina)
    {
        $djeca = Dijete::find()->leftJoin('dijete_skupina', 'dijete_skupina.dijete = dijete.id')->where(['dijete_skupina.skupina' => $skupina])->orderBy(['prezime' => SORT_ASC])->all();
        return $this->render('mjerenjeskupina', ['djeca' => $djeca, 'skupina' => Skupina::findOne(['id' => $skupina])]);
    }
    public function actionUnesiMjerenje()
    {
        if (!Yii::$app->request->isAjax) {
            return;
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $tmp['visina'] = Yii::$app->request->post('visina');
        $tmp['tezina'] = Yii::$app->request->post('tezina');
        $tmp['percentiletezine'] = Yii::$app->request->post('percentiletezine');
        $tmp['percentilevisine'] = Yii::$app->request->post('percentilevisine');
        $tmp['relativnatezina'] = Yii::$app->request->post('relativnatezina');
        $id = Yii::$app->request->post('id');
        if (!$id) {
            return;
        } else {
            $dijete = Dijete::findOne($id);
            $godina = PedagoskaGodina::find()->where(['aktivna' => true])->andWhere('\'' . date('Y-m-d', time()) . '\' between "od" and "do"')->one();
            $skupina = DijeteSkupina::find()->where(['dijete' => $dijete->id])->one();
            $dnevnik = Dnevnik::find()->where(['zaposlenik' => 13, 'ped_godina' => $godina->id])->one();
            if (empty($dnevnik)) {
                $dnevnik = new Dnevnik();
                $dnevnik->sifra = '00-13'; //promjeniti da ide id zaposlenika
                $dnevnik->zaposlenik = 13; //promejeniti da ide id zaposlenika
                $dnevnik->ped_godina = $godina->id;
                $dnevnik->vrsta_dnevnika = 1;
                $dnevnik->save();
            }
            $dijeteZapis = new DijeteZapis();
            $dnevnikZapis = new DnevnikZapis();
            $dnevnikZapis->vrijednosti_atributa = $tmp;
            $dnevnikZapis->zapis = 'Mjerenje djeteta ' . $dijete->getFullName() . ' skupine ' . $skupina->skupina0->naziv;
            $dnevnikZapis->vrsta = 12;
            $dnevnikZapis->dnevnik = $dnevnik->id;
            $dnevnikZapis->save(false);
            $dijeteZapis->dijete = $dijete->id;
            $dijeteZapis->zapis = $dnevnikZapis->id;
            $dijeteZapis->dodatno = $tmp;
            $dijeteZapis->save();
        }



        return [
            'message' => Yii::t('app', 'Uspješno uneseno mjerenje.'),
        ];
    }
    public function actionPodgrupa()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = self::getPodgrupaLista($cat_id);
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }
    public function actionBolesti($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'name' => '']];
        if (!is_null($q)) {
            $data = Icd10classification::find()->select('id,name,code')->where(['or', ['ilike', 'code', $q], ['ilike', 'name', $q]])->all();
            $values = [];
            foreach ($data as $item) {
                $tmp = new \stdClass();
                $tmp->name = $item->code . ' - ' . $item->name;
                $tmp->id = $item->code . ' - ' . $item->name;
                $values[] = $tmp;
            }
            $tmp = new \stdClass();
            $tmp->name = $q;
            $tmp->id = $q;
            $values[] = $tmp;

            $out['results'] = array_values($values);
        } elseif ($id > 0) {
            $result = Icd10classification::findOne($id);

            $out['results'] = ['id' => $id, 'name' => $result->code . ' - ' . $result->name];
        }
        return $out;
    }
    public function actionZapispdf($id)
    {
        $zapis = DijeteZapis::findOne($id);
        $htmlContent = $this->renderPartial('../../../common/views/templates/pdfnalaz', ['zapis' => $zapis]);
        return $this::printPDF($htmlContent, Yii::getAlias('@webroot') . "/nalazi/" . $zapis->dijete0->id . "-" . $zapis->zapis0->id . ".pdf", $zapis->dijete0->id . "-" . $zapis->zapis0->id . ".pdf");
    }

    private function printPDF($sadrzaj, $pdfPath, $nazivDokumenta, $orient = 'P')
    {

        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }

    public function actionPdfPrint($id, $tip)
    {
        $dijete = Dijete::findOne($id);
        $svizapisi = DijeteZapis::find()->joinWith(['zapis0', 'zapis0.vrsta0'])->where(['dijete' => $id])->andWhere(['vrsta_zapis.grupa' => $tip])->all();
        //print("<pre>".print_r($svizapisi,true)."</pre>");die();

        $htmlContent = $this->renderPartial('../../../common/views/templates/svinalazi', ['zapisi' => $svizapisi, 'dijete' => $dijete, 'tip' => $tip]);
        return $this::printPDF($htmlContent, Yii::getAlias('@webroot') . "/nalazi/" . $dijete->ime . "-" . $dijete->id . ".pdf", $dijete->ime . "-" . $dijete->id . ".pdf");
    }
}
