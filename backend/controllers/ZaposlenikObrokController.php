<?php

namespace backend\controllers;

use Yii;
use common\models\ZaposlenikObrok;
use common\models\Search\ZaposlenikObrokSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Zaposlenik;
use common\models\Obrok;
use common\helpers\Utils;

/**
 * ZaposlenikObrokController implements the CRUD actions for ZaposlenikObrok model.
 */
class ZaposlenikObrokController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ZaposlenikObrok models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZaposlenikObrokSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ZaposlenikObrok model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ZaposlenikObrok model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ZaposlenikObrok();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ZaposlenikObrok model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionZaposlenici($tjedan = 0)
    {

        $datumi = $this->getDatum($tjedan);
        $obroci = Obrok::find()->all();
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->all();
        return $this->render('zaposlenici', ['zaposlenici' => $zaposlenici, 'datumi' => $datumi, 'obroci' => $obroci]);
    }
    public function actionDodajZaposlenik()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset($_POST['id'], $_POST['datum'])) {

            if (isset($_POST['dorucak'])) {
                $prisutnost = new ZaposlenikObrok();
                $prisutnost->datum = $_POST['datum'];
                $prisutnost->zaposlenik_id = $_POST['id'];
                $prisutnost->obrok_id =  $_POST['dorucak'];
                $prisutnost->save();
            }
            if (isset($_POST['rucak'])) {
                $prisutnost = new ZaposlenikObrok();
                $prisutnost->datum = $_POST['datum'];
                $prisutnost->zaposlenik_id = $_POST['id'];
                $prisutnost->obrok_id =  $_POST['rucak'];
                $prisutnost->save();
            }
            if (isset($_POST['uzina'])) {
                $prisutnost = new ZaposlenikObrok();
                $prisutnost->datum = $_POST['datum'];
                $prisutnost->zaposlenik_id = $_POST['id'];
                $prisutnost->obrok_id =  $_POST['uzina'];
                $prisutnost->save();
            }
            // $htmlRespnse = $this->renderPartial('zaposlenici-prisutnost-summary', ['prisutnost' => $prisutnost]);
            $sviobroci = ZaposlenikObrok::find()->where(['datum' => $_POST['datum'], 'zaposlenik' => $_POST['id']]);
            $htmlRespnse = $this->renderPartial('zaposlenici-prisutnost-summary', ['prisutnost' => $prisutnost]);
            return [
                'message' => Yii::t('app', 'Uspješno unesen obrok'),
                'status' => 'success',
                'value' => $htmlRespnse,
            ];
        }
    }
    /**
     * Finds the PrijevozZaposlenik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PrijevozZaposlenik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    private function getDatum($tjedan)
    {
        $datum['pon'] = date('Y-m-d', strtotime('monday this week'));
        $datum['uto'] = date('Y-m-d', strtotime('tuesday this week'));
        $datum['sri'] = date('Y-m-d', strtotime('wednesday this week'));
        $datum['cet'] = date('Y-m-d', strtotime('thursday this week'));
        $datum['pet'] = date('Y-m-d', strtotime('friday this week'));
        if ($tjedan == '0') {
            return $datum;
        } else {
            $datum['pon'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pon'])));
            $datum['uto'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['uto'])));
            $datum['sri'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['sri'])));
            $datum['cet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['cet'])));
            $datum['pet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pet'])));
        }
        return $datum;
    }
    /**
     * Deletes an existing ZaposlenikObrok model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ZaposlenikObrok model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ZaposlenikObrok the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ZaposlenikObrok::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionZaposleniciObrociPdfMjesecni()
    {
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all();

        $mjesec = Utils::getMjesecPed($_POST['mjesec']);
        $numberDays = cal_days_in_month(CAL_GREGORIAN, $mjesec['mjesec'], date('Y'));
        $poc = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-01';
        $kraj = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $numberDays;
        $header = ['Ime i prezime', 'Broj dana', 'Broj obroka', 'Cijena', 'Iznos'];
        $retci = [];

        foreach ($zaposlenici as $zaposlenik) {
            $zaNaziv = $zaposlenik->ime . ' ' . $zaposlenik->prezime;
            $temp = [];
            $temp[0] = '<span style="font-weight: bold;text-align:left" class="left">' . $zaNaziv . '</span>';
            $prisutnost = $zaposlenik->getPrisutnost()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet' => 'zaposlenik'])->andWhere(['status' => 1])->orderBy('datum ASC')->all(); //Prisutnost::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['entitet_id' => $zaposlenik->id, 'entitet' => 'zaposlenik'])->orderBy('datum ASC')->all();
            $temp[1] = count($prisutnost);
            $obroci = ZaposlenikObrok::find()->where(['between', 'datum', $poc, $kraj])->andWhere(['zaposlenik_id' => $zaposlenik->id])->all();
            $temp[2] = count($obroci);
            if ($obroci) {
                $temp[3] = $obroci[0]->cijena;
            } else {
                $temp[3] = 0;
            }
            $total = 0;
            foreach ($obroci as $obrok) {
                $total += $obrok->cijena;
            }
            $temp[4] = $total;
            $retci[] = $temp;
        }
        $obrokFull = array_merge([$header], $retci);
        $htmlContent = $this->renderPartial('@common/views/templates/zaposleniciObrociPdfMjesecni', ['obroci' => $obrokFull, 'mjesec' => $mjesec]);
        $naziv = 'Zaposlenici_Obroci-' . $mjesec['mjesec'] . '/' . $mjesec['godina'] . '';
        Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
    }
}
