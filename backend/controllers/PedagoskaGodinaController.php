<?php

namespace backend\controllers;

use Yii;
use common\models\PedagoskaGodina;
use common\models\Search\PedagoskaGodinaSearch;
use console\controllers\NovaPedGodinaController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Cjenik;
use common\models\Dnevnik;
use common\models\DokumentTemplate;
use common\models\Kriterij;
use common\models\Objekt;
use common\models\Skupina;
use common\models\UpisneGrupe;
use common\models\DijeteSkupina;
use common\models\Upitnik;
use common\models\Zahtjev;
use common\models\ZaposlenikSkupina;
use common\models\SkupinaObjekt;
/**
 * PedagoskaGodinaController implements the CRUD actions for PedagoskaGodina model.
 */
class PedagoskaGodinaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PedagoskaGodina models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PedagoskaGodinaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PedagoskaGodina model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public static function getOldYear(){
        $aktivna_godina = null;
        if (!is_null(PedagoskaGodina::getAktivna())) {
            $aktivna_godina = PedagoskaGodina::getAktivna()->id;
        } else {
            PedagoskaGodina::setAktivna();
            $aktivna_godina = PedagoskaGodina::getAktivna()->id;
        }
        
        return $aktivna_godina;
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PedagoskaGodina model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PedagoskaGodina();

        $stara_godina = $this->getOldYear();
        $neaktivna_godina = PedagoskaGodina::find()->where(['id'=>$stara_godina])->one();
        $postavke = $neaktivna_godina->postavke;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $cjenici = Cjenik::find()->where(['ped_godina' => $stara_godina])->all();
            $dnevnici = Dnevnik::find()->where(['ped_godina' => $stara_godina])->all();
            $dokument_templates = DokumentTemplate::find()->where(['ped_godina' => $stara_godina])->all();
            $kriteriji = Kriterij::find()->where(['pedagoska_godina_id' => $stara_godina])->all();
            $objekti = Objekt::find()->where(['ped_godina' => $stara_godina])->all();        
            $skupine = Skupina::find()->where(['ped_godina' => $stara_godina])->all();
            $upisne_grupe = UpisneGrupe::find()->where(['ped_godina' => $stara_godina])->all();
            //$upitnici = Upitnik::find()->where(['ped_godina' => $stara_godina])->all();
            //$zahtjevi = Zahtjev::find()->where(['pd_godina' => $stara_godina])->all();
            $zaposlenik_skupina = ZaposlenikSkupina::find()->where(['ped_godina' => $stara_godina])->all();
            
            $model->postavke = $neaktivna_godina->postavke;
            $model->save();
            $neaktivna_godina->aktivna = false;
            $neaktivna_godina->save();

            $new_year = PedagoskaGodina::find()->max('id');
            $errors = []; 
            
            if (!empty($zaposlenik_skupina)) {
                foreach ($zaposlenik_skupina as $zap_skup) {
                    $zap_skup->aktivno = false;
                    $zap_skup->save();
                }
            } else {
                $errors[]='zaposlenik_skupina not found';
            }
            if (!empty($cjenici)) {
                foreach ($cjenici as $cjenik) {
                    $new_cjenik = new Cjenik();

                    $new_cjenik->program = $cjenik->program;
                    $new_cjenik->cijena = $cjenik->cijena;
                    $new_cjenik->ped_godina = $new_year;
                    $new_cjenik->povlastena = $cjenik->povlastena;
                    $new_cjenik->save();
                }
            } else {
                $errors[]='cjenici not found';
            }
            if (!empty($dnevnici)) {
                foreach ($dnevnici as $dnevnik) {
                    $new_dnevnik = new Dnevnik;

                    $new_dnevnik->sifra = $dnevnik->sifra;
                    $new_dnevnik->zaposlenik = $dnevnik->zaposlenik;
                    $new_dnevnik->ped_godina = $new_year;
                    $new_dnevnik->vrsta_dnevnika = $dnevnik->vrsta_dnevnika;
                    $new_dnevnik->save();
                }
            }else {
                $errors[]='dnevnici not found';
            }
            if (!empty($dokument_templates)) {
                foreach ($dokument_templates as $template) {
                    $new_template = new DokumentTemplate;

                    $new_template->naziv = $template->naziv;
                    $new_template->template = $template->template;
                    $new_template->slug = $template->slug;
                    $new_template->sadrzaj = $template->sadrzaj;
                    $new_template->ped_godina = $new_year;
                    $new_template->save();
                }
            } else {
                $errors[]='dokument_templates not found';
            }
            if (!empty($kriteriji)) {
                foreach ($kriteriji as $kriterij) {
                    $new_kriterij = new Kriterij;

                    $new_kriterij->pedagoska_godina_id = $new_year;
                    $new_kriterij->naziv = $kriterij->naziv;
                    $new_kriterij->vrsta_kriterija_id = $kriterij->vrsta_kriterija_id;
                    $new_kriterij->bodovi = $kriterij->bodovi;
                    $new_kriterij->izravan_upis = $kriterij->izravan_upis;
                    $new_kriterij->redoslijed = $kriterij->redoslijed;
                    $new_kriterij->status_kriterija_id = $kriterij->status_kriterija_id;
                    $new_kriterij->vrsta_unosa_id = $kriterij->vrsta_unosa_id;
                    $new_kriterij->opcije = $kriterij->opcije;
                    $new_kriterij->save();
                }
            } else {
                $errors[]='kriteriji not found';
            }
            if (!empty($objekti)) {
                foreach ($objekti as $objekt) {
                    $new_objekt = new Objekt;

                    $new_objekt->naziv = $objekt->naziv;
                    $new_objekt->adresa = $objekt->adresa;
                    $new_objekt->sifra = $objekt->sifra;
                    $new_objekt->mjesto = $objekt->mjesto;
                    $new_objekt->ped_godina = $new_year;
                    $new_objekt->save();
                }
            } else {
                $errors[]='objekti not found';
            }
            if (!empty($skupine)) {
                foreach ($skupine as $skupina) {
                    $new_skupina = new Skupina;
                    $djecaSkupine = DijeteSkupina::find()->where(['skupina' => $skupina->id])->all();

                    $stari_objekti = [];
                    $novi_objekti = [];
                    $stari_objekti_ids = SkupinaObjekt::find()->where(['skupina' => $skupina->id])->all();
                    foreach ($stari_objekti_ids as $stari_objekt_id) {
                        $stari_objekti[] = Objekt::find()->where(['id' => $stari_objekt_id->objekt])->one();
                    }
                    foreach ($stari_objekti as $stari_objekt) {
                        $novi_objekti[] = Objekt::find()->where(['sifra' => $stari_objekt->sifra])->andwhere(['!=', 'id', $stari_objekt->id])->one();
                    }
                    
                    $new_skupina->naziv = $skupina->naziv;
                    $new_skupina->sifra = $skupina->sifra;
                    $new_skupina->slika = $skupina->slika;
                    $new_skupina->ped_godina = $new_year;
                    $new_skupina->save();            

                    foreach ($novi_objekti as $novi_objekt) {
                        $new_skupina_objekt = new SkupinaObjekt;
                        
                        $new_skupina_objekt->objekt = $novi_objekt->id;
                        $new_skupina_objekt->skupina = $new_skupina->id;
                        $new_skupina_objekt->aktivna = true;
                        $new_skupina_objekt->save();
                    }

                    foreach ($djecaSkupine as $dijeteSkupina) {
                        $new_dijeteSkupina = new DijeteSkupina;

                        $new_dijeteSkupina->skupina = $new_skupina->id;
                        $new_dijeteSkupina->dijete = $dijeteSkupina->dijete;
                        $new_dijeteSkupina->save();
                    }
                }
            } else {
                $errors[]='skupine not found';
            }
            if (!empty($upisne_grupe)) {
                foreach ($upisne_grupe as $upisna_grupa) {
                    $new_grupa = new UpisneGrupe;

                    $new_grupa->naziv = $upisna_grupa->naziv;
                    $new_grupa->broj = $upisna_grupa->broj;
                    $new_grupa->ped_godina = $new_year;
                    $new_grupa->save();
                }
            } else {
                $errors[]='upisne_grupe not found';
            }
            if (!empty($errors)) {
                Yii::$app->session->setFlash('warning', Yii::t('app', 'Obavljeno s greškama: ').implode(', ',$errors));
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'postavke' =>$postavke
        ]);
    }

    /**
     * Updates an existing PedagoskaGodina model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PedagoskaGodina model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (is_null(PedagoskaGodina::getAktivna())) {
            PedagoskaGodina::setAktivna();
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the PedagoskaGodina model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PedagoskaGodina the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PedagoskaGodina::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
