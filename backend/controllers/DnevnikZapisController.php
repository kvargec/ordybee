<?php

namespace backend\controllers;

use common\models\Dijete;

use common\helpers\Utils;

use common\models\Postavke;
use common\models\StatusDjeteta;

use common\models\DijeteZapis;
use common\models\VrstaZapis;
use Yii;
use common\models\DnevnikZapis;
use common\models\Skupina;
use common\models\Prisutnost;
use common\models\Zaposlenik;
use common\models\Search\DnevnikZapisSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\Search\CalendarSearch;
use common\models\Search\SkupinaSearch;
use common\models\Search\ZaposlenikSearch;
use common\models\Aktivnosti;
use common\models\DijeteSkupina;
use common\models\Dnevnik;
use common\models\PedagoskaGodina;
use common\models\VrstaDnevnik;
use common\models\ZaposlenikSkupina;
use \yii\helpers\Url;
use yii\web\UploadedFile;
use Mpdf;
use DateTime;
use DateInterval;

use yii\data\ArrayDataProvider;
use ZipArchive;

/**
 * DnevnikZapisController implements the CRUD actions for DnevnikZapis model.
 */
class DnevnikZapisController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DnevnikZapis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DnevnikZapisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDnevnikRadaIndexSkupina()
    {
        $searchModel = new SkupinaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('dnevnik-rada-index-skupina.php', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDnevnikRadaIndexZaposlenika()
    {
        $searchModel = new ZaposlenikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('dnevnik-rada-index-zaposlenika.php', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDnevnikRadaIndexSkup($id)
    {
        /*    $searchModel = new DnevnikZapisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,['vrsta'=>28 ]);//'vrijednosti_atributa->vanustanove->skupina'=>$id
      */
        $zapisi = DnevnikZapis::find()->where(['vrsta' => 28])->orderBy(['created_at' => SORT_DESC])->all();
        $data = [];
        foreach ($zapisi as $zapis) {
            $key = key($zapis->vrijednosti_atributa);

            switch ($key) {
                case 'vanustanove':
                case 'posredni':
                case 'neposredni':
                    if (array_key_exists('skupina', $zapis->vrijednosti_atributa[$key])) {
                        if (!empty($zapis->vrijednosti_atributa[$key]['skupina'])) {
                            if (is_array($zapis->vrijednosti_atributa[$key]['skupina'])) {
                                foreach ($zapis->vrijednosti_atributa[$key]['skupina'] as $item) {
                                    if ($item == $id) {
                                        $data[] = $zapis;
                                    }
                                }
                            } else {
                                if ($zapis->vrijednosti_atributa[$key]['skupina'] == $id) {
                                    $data[] = $zapis;
                                }
                            }
                        }
                    }
                    break;
            }
        }

        $filteredData = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => ['created_at', 'zapis'],
            ],
        ]);

        return $this->render('dnevnik-rada-index', [
            //  'searchModel' => $searchModel,
            'filteredData' => $filteredData,
        ]);
    }
    public function actionDnevnikRadaIndexZap($id)
    {
        $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $dnevnik = Dnevnik::find()->where(['zaposlenik' => $id, 'ped_godina' => $godina->id, 'vrsta_dnevnika' => 2])->one();

        $searchModel = new DnevnikZapisSearch();
        if (!empty($dnevnik)) {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['vrsta' => 28, 'dnevnik' => $dnevnik->id]); //'vrijednosti_atributa->vanustanove->skupina'=>$id
        } else {
            $dataProvider = new ArrayDataProvider([
                'allModels' => null,
            ]);
        }

        $dataProvider->sort->attributes['created_at'] = [
            'desc' => [
                'created_at' => SORT_DESC,
            ],
            'label' => $searchModel->getAttributeLabel('id'),
        ];
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('dnevni-rada-z', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDijete($id, $tip)
    {
        $dijete = Dijete::find()->where(['id' => $id])->one();
        $vrsteZapisa = VrstaZapis::find()->where(['grupa' => $tip])->all();
        $searchModel = new DnevnikZapisSearch();
        $parametri = Yii::$app->request->queryParams;
        $parametri['DijeteZapis']['dijete'] = $id;
        $dataProvider = $searchModel->search($parametri);

        return $this->render('dijete', [
            'dijete' => $dijete,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'vrsteZapisa' => $vrsteZapisa
        ]);
    }

    /**
     * Displays a single DnevnikZapis model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DnevnikZapis model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($tip = 1)
    {
        $model = new DnevnikZapis();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DnevnikZapis model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DnevnikZapis model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $dijeteZapis = DijeteZapis::find()->where(['zapis' => $id])->one();
        $dijeteZapis->delete();
        return $this->redirect(['index']);
    }

    public function actionIzostanciDjece()
    {
        $mjesecIzForme = Utils::getMjesecPed($_POST['mjesec']);
        $mjesec = $mjesecIzForme['mjesec'];
        $godina = intval($mjesecIzForme['godina']);

        $firstDay = new DateTime();
        $firstDay->setDate($godina, $mjesec, 1);
        $februaryStartDate = $firstDay->format('Y-n-j');

        $lastDay = clone $firstDay;
        $lastDay->modify('first day of next month');
        $lastDay->modify('-1 day');
        $februaryEndDate = $lastDay->format('Y-n-j');

        $dnevnikZapisi = DijeteZapis::find()
            ->select(['dijete.id AS dijete_id', 'dijete.ime', 'dijete.prezime', 'dijete_zapis.*',])
            ->joinWith(['dijete0', 'zapis0']) // Assuming you have relations named 'dijete' and 'zapis0'

            ->andWhere(['IN', 'dnevnik_zapis.vrsta', [6, 7, 9]])
            // ->groupBy(['dijete.id', 'dijete.ime', 'dijete.prezime', 'dijete_zapis.id'])
            ->all();
        $result = array_filter($dnevnikZapisi, function ($item) use ($februaryStartDate, $februaryEndDate) {
            $dodatno = json_decode($item->dodatno, true);
            // echo '<pre>';
            // var_dump($dodatno);
            // echo '</pre>';
            if ($dodatno && isset($dodatno["pocetak_izostanka"], $dodatno["kraj_izostanka"])) {
                $startDate = $dodatno["pocetak_izostanka"];
                $endDate = $dodatno["kraj_izostanka"];

                return ($startDate >= $februaryStartDate && $startDate <= $februaryEndDate) || ($endDate >= $februaryStartDate && $endDate <= $februaryEndDate);
            }

            return false;
        });
        $groupedResult = array_reduce($result, function ($carry, $item) {
            $dijeteId = $item->dijete0->id;

            if (!isset($carry[$dijeteId])) {
                $carry[$dijeteId] = [];
            }

            $carry[$dijeteId][] = $item;


            return $carry;
        }, []);
        // echo '<pre>';
        // var_dump($groupedResult);
        // echo '</pre>';
        $htmlContent = $this->renderPartial('@backend/views/dnevnik-zapis/izostanci-djece', [
            'data' => $groupedResult,
            'mjesec' => $mjesec, 'godina' => $godina
        ]);
        $naziv = 'Izostanci djece za:' . $mjesec . '. mjesec ' . $godina . '. godine';
        $dirpath = Yii::getAlias('@webroot') . "/izostanci/";
        $pdf_file = $this::printPDFfull($htmlContent, $dirpath . $naziv . '.pdf', $naziv . ".pdf", "L");
        if (isset($pdf_file)) {
            return $pdf_file;
        } else {
            throw new \yii\web\NotFoundHttpException('PDF creation failed.');
        }
        // return $this->render('izostanci-djece', [
        //     'data' => $groupedResult,
        //     'mjesec' => $mjesec
        // ]);
    }
    public function actionViewdnevnikrada($id)
    {
        return $this->render('viewdnevnikrada', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionViewdnevniciradaskupine($id)
    {
        return $this->render('viewdnevniciradaskupine', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUploadFile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
        } else if (isset($_POST) && isset($_POST['model_id'])) {
            $id = $_POST['model_id'];
            $model = $this->findModel($id);

            //$model->upload($model->id);

            if (!file_exists(Yii::getAlias('@backend') . '/web/dnevnik/' . $id)) {
                mkdir(Yii::getAlias('@backend') . '/web/dnevnik/' . $id, 0777, true);
            }
            $dir = Yii::getAlias('@backend') . '/web/dnevnik/' . $id;
            $realAttachments = [];
            if (!empty($model->attachments['attachments'])) {
                foreach ($model->attachments['attachments'] as $item) {
                    $realAttachments[] = $item;
                }
            }
            $model->attachments = UploadedFile::getInstances($model, 'attachments');

            foreach ($model->attachments as $file) {
                $fileName = preg_replace("/\s+/", "_", $file->name);
                if (!in_array($fileName, $realAttachments)) {
                    $realAttachments[] = $fileName;
                }
                $file->saveAs($dir . '/' . $fileName);
            }
            $model->attachments = ['attachments' => $realAttachments];

            if ($model->save()) {
                $preview = $model->getFilePaths($id);
                $config = $model->getFilePathsConfig($id);
                $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                return json_encode($out);
            } else if (Yii::$app->user->isGuest) {
                $model->delete();
                return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
            }
        } else {
            return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
        }
    }

    public function actionDeleteFile()
    {
        if (isset($_POST)) {
            $id = $_POST['model_id'];
            $key = $_POST['key'];
            $model = $this->findModel($id);
            $allFiles = $model->attachments['attachments'];
            $dir = Yii::getAlias('@backend') . '/web/dnevnik/' . $id;

            $file = array_search($key, $model->attachments['attachments'], true);
            if (isset($file)) {
                unset($allFiles[$file]);
                $attachments = array_values($allFiles);
                $model->attachments = ['attachments' => $attachments];
                unlink($dir . '/' . $key);
                $model->save();
            }

            $preview = $model->getFilePaths($id);
            $config = $model->getFilePathsConfig($id);
            $out = ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];

            return json_encode($out);
        }
    }

    public function actionCreatednevnikrada()
    {
        $model = new DnevnikZapis();

        $vrsta_zapis = \common\models\VrstaZapis::find()->where(['naziv' => 'grupa zapis'])->one();
        $godina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $vrstaDnevnika = VrstaDnevnik::find()->where(['naziv' => 'Dnevnik zapisa'])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $vrijednosti = [];
            $vrijednosti['vrsta'] = Yii::$app->request->post('roles');
            $model->vrsta = $vrsta_zapis->id;
            $objekt = Yii::$app->request->post('objekt');
            $zaposlenik = null;
            if ($objekt != 0) {
                $model->objekt = $objekt;
            } else {
                $model->objekt = null;
            }

            if (Yii::$app->request->post('zaposlenik') === null) {
                $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => Yii::$app->user->id])->one();
                if (isset($zaposlenik)) {
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->andWhere(['ped_godina' => $godina->id])->one();

                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $zaposlenik->id;
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $zaposlenik->id;
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();

                        $model->dnevnik = $novi_dnevnik->id;
                    } else {
                        $model->dnevnik = $dnevnik->id;
                    }
                } else if (Yii::$app->user->isGuest) {
                    $model->delete();
                    return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
                }
            } else {
                $zaposlenik = \common\models\Zaposlenik::find()->where(['id' => Yii::$app->request->post('zaposlenik')])->one();
                if (isset($zaposlenik)) {
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->andWhere(['ped_godina' => $godina->id])->one();

                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $zaposlenik->id;
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $zaposlenik->id;
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();

                        $model->dnevnik = $novi_dnevnik->id;
                    } else {
                        $model->dnevnik = $dnevnik->id;
                    }
                }
            }
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                    $start = Yii::$app->request->post('bolovanje_start');
                    $end = Yii::$app->request->post('bolovanje_end');
                    break;
                case 'godisnji':
                    $start = Yii::$app->request->post('godisnji_start');
                    $end = Yii::$app->request->post('godisnji_end');
                    break;
                case 'slobodno':
                    $start = Yii::$app->request->post('slobodno_start');
                    $end = Yii::$app->request->post('slobodno_end');
                    break;
                case 'porodiljni':
                    $start = Yii::$app->request->post('porodiljni_start');
                    $end = Yii::$app->request->post('porodiljni_end');
                    break;
                case 'dopust':
                    $start = Yii::$app->request->post('dopust_start');
                    $end = Yii::$app->request->post('dopust_end');
                    break;
                case 'neposredni':
                    $start = Yii::$app->request->post('neposredni')['start'];
                    $end = Yii::$app->request->post('neposredni')['end'];
                    break;
                case 'posredni':
                    $start = Yii::$app->request->post('posredni')['start'];
                    $end = Yii::$app->request->post('posredni')['end'];
                    break;
                case 'vanustanove':
                    $start = Yii::$app->request->post('vanustanove')['start'];
                    $end = Yii::$app->request->post('vanustanove')['end'];
                    break;
            }
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                case 'godisnji':
                case 'slobodno':
                case 'porodiljni':
                case 'dopust':
                    $pocetak_date = Yii::$app->formatter->asDate($start, 'php:Y-m-d');
                    $kraj_date = Yii::$app->formatter->asDate($end, 'php:Y-m-d');

                    if ($pocetak_date != $kraj_date) {
                        $kraj_datetime = new Datetime(Yii::$app->formatter->asDateTime($start, 'php:Y-m-d H:i:s'));
                        $pocetak_datetime = new Datetime(Yii::$app->formatter->asDateTime($end, 'php:Y-m-d H:i:s'));
                        $interval = date_diff($pocetak_datetime, $kraj_datetime)->days;
                        $temp_poc = new DateTime($pocetak_date);
                        for ($i = 0; $i <= $interval; $i++) {
                            if ($i == 0) {
                                $temp_poc = $temp_poc;
                            } else {
                                $newinterval = new DateInterval('P1D');
                                $temp_poc = $temp_poc->add($newinterval);
                            }
                            $dan = Yii::$app->formatter->asDate($temp_poc, 'php:Y-m-d');
                            $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $dan]);
                            if (empty($prisutnost)) {
                                $prisutnost = new Prisutnost();
                                $prisutnost->datum = $dan;
                                $prisutnost->entitet_id = $zaposlenik->id;
                                $prisutnost->entitet = 'zaposlenik';
                                $prisutnost->status = 0;
                                $prisutnost->pocetak = '08:00';
                                $prisutnost->kraj = '16:00';
                                $prisutnost->trajanje = 8;
                                $prisutnost->save();
                            }
                        }
                    } else {
                        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $pocetak_date]);
                        if (empty($prisutnost)) {
                            $prisutnost = new Prisutnost();
                            $prisutnost->datum = $pocetak_date;
                            $prisutnost->entitet_id = $zaposlenik->id;
                            $prisutnost->entitet = 'zaposlenik';
                            $prisutnost->status = 0;
                            $prisutnost->pocetak = '08:00';
                            $prisutnost->kraj = '16:00';
                            $prisutnost->trajanje = 8;
                            $prisutnost->save();
                        }
                    }
                    break;
                case 'neposredni':
                case 'posredni':
                case 'vanustanove':
                    $pocetak_datetime = Yii::$app->formatter->asDateTime($start, 'php:Y-m-d H:i:s');
                    $pocetak_date = Yii::$app->formatter->asDate($start, 'php:Y-m-d');
                    $pocetak_time = Yii::$app->formatter->asTime($start);
                    $kraj_datetime = Yii::$app->formatter->asDateTime($end, 'php:Y-m-d H:i:s');
                    $kraj_date = Yii::$app->formatter->asDate($end, 'php:Y-m-d');
                    $kraj_time = Yii::$app->formatter->asTime($end);

                    if ($pocetak_date != $kraj_date) {
                        $kraj_dt = new Datetime(Yii::$app->formatter->asDateTime($pocetak_datetime, 'php:Y-m-d H:i:s'));
                        $pocetak_dt = new Datetime(Yii::$app->formatter->asDateTime($kraj_datetime, 'php:Y-m-d H:i:s'));
                        $interval = date_diff($pocetak_dt, $kraj_dt)->days;
                        $temp_poc = new DateTime($pocetak_date);
                        for ($i = 0; $i <= $interval; $i++) {
                            if ($i == 0) {
                                $temp_poc = $temp_poc;
                            } else {
                                $newinterval = new DateInterval('P1D');
                                $temp_poc = $temp_poc->add($newinterval);
                            }
                            $dan = Yii::$app->formatter->asDate($temp_poc, 'php:Y-m-d');
                            $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $dan]);
                            if (empty($prisutnost)) {
                                $prisutnost = new Prisutnost();
                                $prisutnost->datum = $dan;
                                $prisutnost->entitet_id = $zaposlenik->id;
                                $prisutnost->entitet = 'zaposlenik';
                                $prisutnost->status = 1;
                                if ($i == 0) {
                                    $prisutnost->pocetak = $pocetak_time;
                                    $prisutnost->trajanje = (strtotime('16:00') - strtotime($pocetak_time)) / 3600;
                                } else {
                                    $prisutnost->pocetak = '08:00';
                                }
                                if ($i == $interval) {
                                    $prisutnost->kraj = $kraj_time;
                                    $prisutnost->trajanje = (strtotime($kraj_time) - strtotime('08:00')) / 3600;
                                } else {
                                    $prisutnost->kraj = '16:00';
                                }
                                if (!$prisutnost->trajanje) {
                                    $prisutnost->trajanje = (strtotime('16:00') - strtotime('08:00')) / 3600;
                                }
                                $prisutnost->trajanje = $interval;
                                $prisutnost->save();
                            }
                        }
                    } else {
                        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $pocetak_date]);
                        $kraj_d = new Datetime(Yii::$app->formatter->asDate($pocetak_date, 'php:Y-m-d'));
                        $pocetak_d = new Datetime(Yii::$app->formatter->asDate($kraj_date, 'php:Y-m-d'));
                        if (empty($prisutnost)) {
                            $prisutnost = new Prisutnost();
                            $prisutnost->datum = $pocetak_date;
                            $prisutnost->entitet_id = $zaposlenik->id;
                            $prisutnost->entitet = 'zaposlenik';
                            $prisutnost->status = 1;
                            $prisutnost->pocetak = $pocetak_time;
                            if ($pocetak_d == $kraj_d) {
                                $prisutnost->kraj = $kraj_time;
                            } else if ($pocetak_d < $kraj_date) {
                                $prisutnost->kraj = '16:00';
                            }
                            $prisutnost->trajanje = (strtotime($kraj_time) - strtotime($pocetak_time)) / 3600;
                            $prisutnost->save();
                        } else {
                            if ($pocetak_d == $kraj_d) {
                                $prisutnost->kraj = $kraj_time;
                            } else if ($pocetak_d < $kraj_d) {
                                $prisutnost->kraj = '16:00';
                            }
                            $prisutnost->trajanje = (strtotime($kraj_time) - strtotime($pocetak_time)) / 3600;
                            $prisutnost->save();
                        }
                    }
                    break;
            }
            //$model->upload($model->id);
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                    $array = ['bolovanje' => ['pocetak' => Yii::$app->request->post('bolovanje_start'), 'kraj' => Yii::$app->request->post('bolovanje_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Bolovanje';
                    $model->save();

                    break;

                case 'godisnji':
                    $array = ['godisnji' => ['pocetak' => Yii::$app->request->post('godisnji_start'), 'kraj' => Yii::$app->request->post('godisnji_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Godišnji';
                    $model->save();

                    break;

                case 'slobodno':
                    $array = ['slobodno' => ['pocetak' => Yii::$app->request->post('slobodno_start'), 'kraj' => Yii::$app->request->post('slobodno_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Slobodno';
                    $model->save();

                    break;

                case 'porodiljni':
                    $array = ['porodiljni' => ['pocetak' => Yii::$app->request->post('porodiljni_start'), 'kraj' => Yii::$app->request->post('porodiljni_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Porodiljni';
                    $model->save();

                    break;

                case 'dopust':
                    $array = ['dopust' => ['pocetak' => Yii::$app->request->post('dopust_start'), 'kraj' => Yii::$app->request->post('dopust_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Dopust';
                    $model->save();

                    break;

                case 'vanustanove':
                    foreach (Yii::$app->request->post('vanustanove') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (isset($vrijednosti['skupina'])) {
                            $skupina = [];
                            foreach ($vrijednosti['skupina'] as $skup) {
                                $temp = \common\models\Skupina::find()->where(['id' => $skup])->one();
                                $skupina[] = $temp->naziv;
                            }
                            if (count($skupina) > 1) {
                                $model->zapis = "Zapis o radu skupina: " . implode(', ', $skupina);
                            } else {
                                $model->zapis = "Zapis o radu skupine: " . $skupina[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('vanustanove_attachments')

                    $model->vrijednosti_atributa = ['vanustanove' => $vrijednosti];
                    $model->save();
                    if ($model->save()) {
                        return $this->render('uploadfiles', ['model' => $model]);
                    } else if (Yii::$app->user->isGuest) {
                        $model->delete();
                        return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
                    }

                case 'posredni':
                    foreach (Yii::$app->request->post('posredni') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (isset($vrijednosti['skupina'])) {
                            $skupina = [];
                            foreach ($vrijednosti['skupina'] as $skup) {
                                $temp = \common\models\Skupina::find()->where(['id' => $skup])->one();
                                $skupina[] = $temp->naziv;
                            }
                            if (count($skupina) > 1) {
                                $model->zapis = "Zapis o radu skupina: " . implode(', ', $skupina);
                            } else {
                                $model->zapis = "Zapis o radu skupine: " . $skupina[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('posredni_attachments');
                    $model->vrijednosti_atributa = ['posredni' => $vrijednosti];
                    if ($model->save()) {
                        return $this->render('uploadfiles', ['model' => $model]);
                    } else if (Yii::$app->user->isGuest) {
                        $model->delete();
                        return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
                    }

                case 'neposredni':
                    foreach (Yii::$app->request->post('neposredni') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };

                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (isset($vrijednosti['skupina'])) {
                            $skupina = [];
                            foreach ($vrijednosti['skupina'] as $skup) {
                                $temp = \common\models\Skupina::find()->where(['id' => $skup])->one();
                                $skupina[] = $temp->naziv;
                            }
                            if (count($skupina) > 1) {
                                $model->zapis = "Zapis o radu skupina: " . implode(', ', $skupina);
                            } else {
                                $model->zapis = "Zapis o radu skupine: " . $skupina[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('neposredni_attachments');
                    $model->vrijednosti_atributa = ['neposredni' => $vrijednosti];
                    if ($model->save()) {
                        return $this->render('uploadfiles', ['model' => $model]);
                    } else if (Yii::$app->user->isGuest) {
                        $model->delete();
                        return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
                    }
                default:
                    $model->delete();
                    return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
            }
            return $this->redirect(['creatednevnikrada']);
        }

        $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => Yii::$app->user->id])->one();
        $zapisi_svi = null;
        $events = [];

        $rola = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
        $uloge = [];
        foreach ($rola as $tko => $opis) {
            $uloge[] = $tko;
        }
        if ($uloge[0] == 'superadmin' || $uloge[0] == 'administracija') {
            $dnevnici = Dnevnik::find()->where(['ped_godina' => $godina->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->all();
            $zapisi_svi = [];
            foreach ($dnevnici as $dnevnik) {
                $zapisi_svi[] = DnevnikZapis::find()->where(['vrsta' => $vrsta_zapis->id, 'dnevnik' => $dnevnik->id])->all();
            }
        } elseif (isset($zaposlenik)) {
            $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['ped_godina' => $godina->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->one();
            if (isset($dnevnik)) {
                $zapisi_svi[] = DnevnikZapis::find()->where(['vrsta' => $vrsta_zapis->id, 'dnevnik' => $dnevnik->id])->all();
            }
        }
        //var_dump($zaposlenik,$zapisi);
        //die();
        if (!empty($zapisi_svi)) {
            foreach ($zapisi_svi as $zapisi) {
                foreach ($zapisi as $zapis) {
                    switch (key($zapis->vrijednosti_atributa)) {
                        case 'vanustanove':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['vanustanove']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['vanustanove']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Rad van ustanove', 'start' => $start, 'end' => $end, 'color' => '#055a4b', 'vrsta' => 'Rad van ustanove']);
                            break;
                        case 'posredni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['posredni']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['posredni']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Posredni rad', 'start' => $start, 'end' => $end, 'color' => '#055a4b', 'vrsta' => 'Posredni rad']);
                            break;
                        case 'neposredni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['neposredni']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['neposredni']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Neposredni rad', 'start' => $start, 'end' => $end, 'color' => '#095367', 'vrsta' => 'Neposredni rad']);
                            break;
                        case 'bolovanje':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['bolovanje']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['bolovanje']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Bolovanje', 'start' => $start, 'end' => $end, 'color' => '#9c0d38', 'vrsta' => 'Bolovanje']);
                            break;
                        case 'godisnji':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['godisnji']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['godisnji']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Godišnji odmor', 'start' => $start, 'end' => $end, 'color' => '#9c0d38', 'vrsta' => 'Godišnji']);
                            break;
                        case 'slobodno':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['slobodno']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['slobodno']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Slobodni dani', 'start' => $start, 'end' => $end, 'color' => '#9c0d38', 'vrsta' => 'Slobodni dani']);
                            break;
                        case 'porodiljni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['porodiljni']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['porodiljni']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Porodiljni dopust', 'start' => $start, 'end' => $end, 'color' => '#9c0d38', 'vrsta' => 'Porodiljni dopust']);
                            break;
                        case 'dopust':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['dopust']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['dopust']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Dopust', 'start' => $start, 'end' => $end, 'color' => '#9c0d38', 'vrsta' => 'Dopust']);
                            break;
                    }
                }
            }
        }

        return $this->render('creatednevnikrada', [
            'model' => $model,
            'events' => $events
        ]);
    }

    public function actionUpdatednevnikrada($id)
    {
        $model = $this->findModel($id);

        $vrsta_zapis = \common\models\VrstaZapis::find()->where(['naziv' => 'grupa zapis'])->one();
        $vrstaDnevnika = VrstaDnevnik::find()->where(['naziv' => 'Dnevnik zapisa'])->one();
        $godina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $vrijednosti = [];
            $vrijednosti['vrsta'] = Yii::$app->request->post('roles');
            $model->vrsta = $vrsta_zapis->id;
            $objekt = Yii::$app->request->post('objekt');
            $zaposlenik = null;
            if ($objekt != 0) {
                $model->objekt = $objekt;
            } else {
                $model->objekt = null;
            }

            if (Yii::$app->request->post('zaposlenik') === null) {
                $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => Yii::$app->user->id])->one();
                if (isset($zaposlenik)) {
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->andWhere(['ped_godina' => $godina->id])->one();

                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $zaposlenik->id;
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $zaposlenik->id;
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();

                        $model->dnevnik = $novi_dnevnik->id;
                    } else {
                        $model->dnevnik = $dnevnik->id;
                    }
                } else if (Yii::$app->user->isGuest) {
                    $model->delete();
                    return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
                }
            } else {
                $zaposlenik = \common\models\Zaposlenik::find()->where(['id' => Yii::$app->request->post('zaposlenik')])->one();
                if (isset($zaposlenik)) {
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->andWhere(['ped_godina' => $godina->id])->one();

                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $zaposlenik->id;
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $zaposlenik->id;
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();

                        $model->dnevnik = $novi_dnevnik->id;
                    } else {
                        $model->dnevnik = $dnevnik->id;
                    }
                }
            }
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                    $start = Yii::$app->request->post('bolovanje_start');
                    $end = Yii::$app->request->post('bolovanje_end');
                    break;
                case 'godisnji':
                    $start = Yii::$app->request->post('godisnji_start');
                    $end = Yii::$app->request->post('godisnji_end');
                    break;
                case 'slobodno':
                    $start = Yii::$app->request->post('slobodno_start');
                    $end = Yii::$app->request->post('slobodno_end');
                    break;
                case 'porodiljni':
                    $start = Yii::$app->request->post('porodiljni_start');
                    $end = Yii::$app->request->post('porodiljni_end');
                    break;
                case 'dopust':
                    $start = Yii::$app->request->post('dopust_start');
                    $end = Yii::$app->request->post('dopust_end');
                    break;
                case 'neposredni':
                    $start = Yii::$app->request->post('neposredni')['start'];
                    $end = Yii::$app->request->post('neposredni')['end'];
                    break;
                case 'posredni':
                    $start = Yii::$app->request->post('posredni')['start'];
                    $end = Yii::$app->request->post('posredni')['end'];
                    break;
                case 'vanustanove':
                    $start = Yii::$app->request->post('vanustanove')['start'];
                    $end = Yii::$app->request->post('vanustanove')['end'];
                    break;
            }
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                case 'godisnji':
                case 'slobodno':
                case 'porodiljni':
                case 'dopust':
                    $pocetak_date = Yii::$app->formatter->asDate($start, 'php:Y-m-d');
                    $kraj_date = Yii::$app->formatter->asDate($end, 'php:Y-m-d');

                    if ($pocetak_date != $kraj_date) {
                        $kraj_datetime = new Datetime(Yii::$app->formatter->asDateTime($start, 'php:Y-m-d H:i:s'));
                        $pocetak_datetime = new Datetime(Yii::$app->formatter->asDateTime($end, 'php:Y-m-d H:i:s'));
                        $interval = date_diff($pocetak_datetime, $kraj_datetime)->days;
                        $temp_poc = new DateTime($pocetak_date);
                        for ($i = 0; $i <= $interval; $i++) {
                            if ($i == 0) {
                                $temp_poc = $temp_poc;
                            } else {
                                $newinterval = new DateInterval('P1D');
                                $temp_poc = $temp_poc->add($newinterval);
                            }
                            $dan = Yii::$app->formatter->asDate($temp_poc, 'php:Y-m-d');
                            $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $dan]);
                            if (empty($prisutnost)) {
                                $prisutnost = new Prisutnost();
                                $prisutnost->datum = $dan;
                                $prisutnost->entitet_id = $zaposlenik->id;
                                $prisutnost->entitet = 'zaposlenik';
                                $prisutnost->status = 0;
                                $prisutnost->pocetak = '08:00';
                                $prisutnost->kraj = '16:00';
                                $prisutnost->trajanje = 8;
                                $prisutnost->save();
                            }
                        }
                    } else {
                        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $pocetak_date]);
                        if (empty($prisutnost)) {
                            $prisutnost = new Prisutnost();
                            $prisutnost->datum = $pocetak_date;
                            $prisutnost->entitet_id = $zaposlenik->id;
                            $prisutnost->entitet = 'zaposlenik';
                            $prisutnost->status = 0;
                            $prisutnost->pocetak = '08:00';
                            $prisutnost->kraj = '16:00';
                            $prisutnost->trajanje = 8;
                            $prisutnost->save();
                        }
                    }
                    break;
                case 'neposredni':
                case 'posredni':
                case 'vanustanove':
                    $pocetak_datetime = Yii::$app->formatter->asDateTime($start, 'php:Y-m-d H:i:s');
                    $pocetak_date = Yii::$app->formatter->asDate($start, 'php:Y-m-d');
                    $pocetak_time = Yii::$app->formatter->asTime($start);
                    $kraj_datetime = Yii::$app->formatter->asDateTime($end, 'php:Y-m-d H:i:s');
                    $kraj_date = Yii::$app->formatter->asDate($end, 'php:Y-m-d');
                    $kraj_time = Yii::$app->formatter->asTime($end);

                    if ($pocetak_date != $kraj_date) {
                        $kraj_dt = new Datetime(Yii::$app->formatter->asDateTime($pocetak_datetime, 'php:Y-m-d H:i:s'));
                        $pocetak_dt = new Datetime(Yii::$app->formatter->asDateTime($kraj_datetime, 'php:Y-m-d H:i:s'));
                        $interval = date_diff($pocetak_dt, $kraj_dt)->days;
                        $temp_poc = new DateTime($pocetak_date);
                        for ($i = 0; $i <= $interval; $i++) {
                            if ($i == 0) {
                                $temp_poc = $temp_poc;
                            } else {
                                $newinterval = new DateInterval('P1D');
                                $temp_poc = $temp_poc->add($newinterval);
                            }
                            $dan = Yii::$app->formatter->asDate($temp_poc, 'php:Y-m-d');
                            $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $dan]);
                            if (empty($prisutnost)) {
                                $prisutnost = new Prisutnost();
                                $prisutnost->datum = $dan;
                                $prisutnost->entitet_id = $zaposlenik->id;
                                $prisutnost->entitet = 'zaposlenik';
                                $prisutnost->status = 1;
                                if ($i == 0) {
                                    $prisutnost->pocetak = $pocetak_time;
                                    $prisutnost->trajanje = (strtotime('16:00') - strtotime($pocetak_time)) / 3600;
                                } else {
                                    $prisutnost->pocetak = '08:00';
                                }
                                if ($i == $interval) {
                                    $prisutnost->kraj = $kraj_time;
                                    $prisutnost->trajanje = (strtotime($kraj_time) - strtotime('08:00')) / 3600;
                                } else {
                                    $prisutnost->kraj = '16:00';
                                }
                                if (!$prisutnost->trajanje) {
                                    $prisutnost->trajanje = (strtotime('16:00') - strtotime('08:00')) / 3600;
                                }
                                $prisutnost->trajanje = $interval;
                                $prisutnost->save();
                            }
                        }
                    } else {
                        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $pocetak_date]);
                        $kraj_d = new Datetime(Yii::$app->formatter->asDate($pocetak_date, 'php:Y-m-d'));
                        $pocetak_d = new Datetime(Yii::$app->formatter->asDate($kraj_date, 'php:Y-m-d'));
                        if (empty($prisutnost)) {
                            $prisutnost = new Prisutnost();
                            $prisutnost->datum = $pocetak_date;
                            $prisutnost->entitet_id = $zaposlenik->id;
                            $prisutnost->entitet = 'zaposlenik';
                            $prisutnost->status = 1;
                            $prisutnost->pocetak = $pocetak_time;
                            if ($pocetak_d == $kraj_d) {
                                $prisutnost->kraj = $kraj_time;
                            } else if ($pocetak_d < $kraj_date) {
                                $prisutnost->kraj = '16:00';
                            }
                            $prisutnost->trajanje = $kraj_time - $pocetak_time;
                            $prisutnost->save();
                        } else {
                            if ($pocetak_d == $kraj_d) {
                                $prisutnost->kraj = $kraj_time;
                            } else if ($pocetak_d < $kraj_d) {
                                $prisutnost->kraj = '16:00';
                            }
                            $prisutnost->trajanje = $kraj_time - $pocetak_time;
                            $prisutnost->save();
                        }
                    }
                    break;
            }

            //$model->upload($model->id);
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                    $array = ['bolovanje' => ['pocetak' => Yii::$app->request->post('bolovanje_start'), 'kraj' => Yii::$app->request->post('bolovanje_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Bolovanje';
                    $model->save();

                    break;

                case 'godisnji':
                    $array = ['godisnji' => ['pocetak' => Yii::$app->request->post('godisnji_start'), 'kraj' => Yii::$app->request->post('godisnji_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Godišnji';
                    $model->save();

                    break;

                case 'slobodno':
                    $array = ['slobodno' => ['pocetak' => Yii::$app->request->post('slobodno_start'), 'kraj' => Yii::$app->request->post('slobodno_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Slobodno';
                    $model->save();

                    break;

                case 'porodiljni':
                    $array = ['porodiljni' => ['pocetak' => Yii::$app->request->post('porodiljni_start'), 'kraj' => Yii::$app->request->post('porodiljni_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Porodiljni';
                    $model->save();

                    break;

                case 'dopust':
                    $array = ['dopust' => ['pocetak' => Yii::$app->request->post('dopust_start'), 'kraj' => Yii::$app->request->post('dopust_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Dopust';
                    $model->save();

                    break;

                case 'vanustanove':
                    foreach (Yii::$app->request->post('vanustanove') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (isset($vrijednosti['skupina'])) {
                            $skupina = [];
                            foreach ($vrijednosti['skupina'] as $skup) {
                                $temp = \common\models\Skupina::find()->where(['id' => $skup])->one();
                                $skupina[] = $temp->naziv;
                            }
                            if (count($skupina) > 1) {
                                $model->zapis = "Zapis o radu skupina: " . implode(', ', $skupina);
                            } else {
                                $model->zapis = "Zapis o radu skupine: " . $skupina[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('vanustanove_attachments');
                    $model->vrijednosti_atributa = ['vanustanove' => $vrijednosti];
                    if ($model->save()) {
                        return $this->render('uploadfiles', ['model' => $model]);
                    } else if (Yii::$app->user->isGuest) {
                        $model->delete();
                        return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
                    }

                case 'posredni':
                    foreach (Yii::$app->request->post('posredni') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (isset($vrijednosti['skupina'])) {
                            $skupina = [];
                            foreach ($vrijednosti['skupina'] as $skup) {
                                $temp = \common\models\Skupina::find()->where(['id' => $skup])->one();
                                $skupina[] = $temp->naziv;
                            }
                            if (count($skupina) > 1) {
                                $model->zapis = "Zapis o radu skupina: " . implode(', ', $skupina);
                            } else {
                                $model->zapis = "Zapis o radu skupine: " . $skupina[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('posredni_attachments');
                    $model->vrijednosti_atributa = ['posredni' => $vrijednosti];
                    if ($model->save()) {
                        return $this->render('uploadfiles', ['model' => $model]);
                    } else if (Yii::$app->user->isGuest) {
                        $model->delete();
                        return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
                    }

                case 'neposredni':
                    foreach (Yii::$app->request->post('neposredni') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (isset($vrijednosti['skupina'])) {
                            $skupina = [];
                            foreach ($vrijednosti['skupina'] as $skup) {
                                $temp = \common\models\Skupina::find()->where(['id' => $skup])->one();
                                $skupina[] = $temp->naziv;
                            }
                            if (count($skupina) > 1) {
                                $model->zapis = "Zapis o radu skupina: " . implode(', ', $skupina);
                            } else {
                                $model->zapis = "Zapis o radu skupine: " . $skupina[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('neposredni_attachments');
                    $model->vrijednosti_atributa = ['neposredni' => $vrijednosti];
                    if ($model->save()) {
                        return $this->render('uploadfiles', ['model' => $model]);
                    } else if (Yii::$app->user->isGuest) {
                        $model->delete();
                        return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
                    }
                default:
                    $model->delete();
                    return $this->redirect(Yii::$app->urlManager->createUrl('user/security/login'));
            }
            return $this->redirect(['viewdnevnikrada', 'id' => $model->id]);
        }

        $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => Yii::$app->user->id])->one();
        $zapisi_svi = null;
        $events = [];

        $rola = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
        $uloge = [];
        foreach ($rola as $tko => $opis) {
            $uloge[] = $tko;
        }
        if ($uloge[0] == 'superadmin') {
            $dnevnici = Dnevnik::find()->where(['ped_godina' => $godina->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->all();
            $zapisi_svi = [];
            foreach ($dnevnici as $dnevnik) {
                $zapisi_svi[] = DnevnikZapis::find()->where(['vrsta' => $vrsta_zapis->id, 'dnevnik' => $dnevnik->id])->all();
            }
        } elseif (isset($zaposlenik)) {
            $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['ped_godina' => $godina->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->one();
            if (isset($dnevnik)) {
                $zapisi_svi[] = DnevnikZapis::find()->where(['vrsta' => $vrsta_zapis->id, 'dnevnik' => $dnevnik->id])->all();
            }
        }
        //var_dump($zaposlenik,$zapisi);
        //die();
        if (!empty($zapisi_svi)) {
            foreach ($zapisi_svi as $zapisi) {
                foreach ($zapisi as $zapis) {

                    switch (key($zapis->vrijednosti_atributa)) {
                        case 'vanustanove':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['vanustanove']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['vanustanove']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Rad van ustanove', 'start' => $start, 'end' => $end, 'color' => '#055a4b']);
                            break;
                        case 'posredni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['posredni']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['posredni']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Posredni rad', 'start' => $start, 'end' => $end, 'color' => '#055a4b']);
                            break;
                        case 'neposredni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['neposredni']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['neposredni']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Neposredni rad', 'start' => $start, 'end' => $end, 'color' => '']);
                            break;
                        case 'bolovanje':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['bolovanje']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['bolovanje']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Bolovanje', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'godisnji':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['godisnji']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['godisnji']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Godišnji odmor', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'slobodno':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['slobodno']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['slobodno']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Slobodni dani', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'porodiljni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['porodiljni']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['porodiljni']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Porodiljni dopust', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'dopust':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['dopust']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['dopust']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Dopust', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                    }
                }
            }
        }

        return $this->render('updatednevnikrada', [
            'model' => $model,
            'events' => $events
        ]);
    }

    public function actionCreatednevnikradapedagog()
    {
        $model = new DnevnikZapis();

        $vrsta_zapis = \common\models\VrstaZapis::find()->where(['naziv' => 'grupa zapis'])->one();
        $godina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $vrstaDnevnika = VrstaDnevnik::find()->where(['naziv' => 'Dnevnik zapisa'])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $vrijednosti = [];
            $vrijednosti['vrsta'] = Yii::$app->request->post('roles');
            $model->vrsta = $vrsta_zapis->id;
            $objekt = Yii::$app->request->post('objekt');
            $zaposlenik = null;
            if ($objekt != 0) {
                $model->objekt = $objekt;
            } else {
                $model->objekt = null;
            }

            if (Yii::$app->request->post('zaposlenik') === null) {
                $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => Yii::$app->user->id])->one();
                if (isset($zaposlenik)) {
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->andWhere(['ped_godina' => $godina->id])->one();

                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $zaposlenik->id;
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $zaposlenik->id;
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();

                        $model->dnevnik = $novi_dnevnik->id;
                    } else {
                        $model->dnevnik = $dnevnik->id;
                    }
                }
            } else {
                $zaposlenik = \common\models\Zaposlenik::find()->where(['id' => Yii::$app->request->post('zaposlenik')])->one();
                if (isset($zaposlenik)) {
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->andWhere(['ped_godina' => $godina->id])->one();

                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $zaposlenik->id;
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $zaposlenik->id;
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();

                        $model->dnevnik = $novi_dnevnik->id;
                    } else {
                        $model->dnevnik = $dnevnik->id;
                    }
                }
            }
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                    $start = Yii::$app->request->post('bolovanje_start');
                    $end = Yii::$app->request->post('bolovanje_end');
                    break;
                case 'godisnji':
                    $start = Yii::$app->request->post('godisnji_start');
                    $end = Yii::$app->request->post('godisnji_end');
                    break;
                case 'slobodno':
                    $start = Yii::$app->request->post('slobodno_start');
                    $end = Yii::$app->request->post('slobodno_end');
                    break;
                case 'porodiljni':
                    $start = Yii::$app->request->post('porodiljni_start');
                    $end = Yii::$app->request->post('porodiljni_end');
                    break;
                case 'dopust':
                    $start = Yii::$app->request->post('dopust_start');
                    $end = Yii::$app->request->post('dopust_end');
                    break;
                case 'neposredni':
                    $start = Yii::$app->request->post('neposredni')['start'];
                    $end = Yii::$app->request->post('neposredni')['end'];
                    break;
                case 'posredni':
                    $start = Yii::$app->request->post('posredni')['start'];
                    $end = Yii::$app->request->post('posredni')['end'];
                    break;
                case 'vanustanove':
                    $start = Yii::$app->request->post('vanustanove')['start'];
                    $end = Yii::$app->request->post('vanustanove')['end'];
                    break;
            }
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                case 'godisnji':
                case 'slobodno':
                case 'porodiljni':
                case 'dopust':
                    $pocetak_date = Yii::$app->formatter->asDate($start, 'php:Y-m-d');
                    $kraj_date = Yii::$app->formatter->asDate($end, 'php:Y-m-d');

                    if ($pocetak_date != $kraj_date) {
                        $kraj_datetime = new Datetime(Yii::$app->formatter->asDateTime($start, 'php:Y-m-d H:i:s'));
                        $pocetak_datetime = new Datetime(Yii::$app->formatter->asDateTime($end, 'php:Y-m-d H:i:s'));
                        $interval = date_diff($pocetak_datetime, $kraj_datetime)->days;
                        $temp_poc = new DateTime($pocetak_date);
                        for ($i = 0; $i <= $interval; $i++) {
                            if ($i == 0) {
                                $temp_poc = $temp_poc;
                            } else {
                                $newinterval = new DateInterval('P1D');
                                $temp_poc = $temp_poc->add($newinterval);
                            }
                            $dan = Yii::$app->formatter->asDate($temp_poc, 'php:Y-m-d');
                            $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $dan]);
                            if (empty($prisutnost)) {
                                $prisutnost = new Prisutnost();
                                $prisutnost->datum = $dan;
                                $prisutnost->entitet_id = $zaposlenik->id;
                                $prisutnost->entitet = 'zaposlenik';
                                $prisutnost->status = 0;
                                $prisutnost->pocetak = '08:00';
                                $prisutnost->kraj = '16:00';
                                $prisutnost->trajanje = 8;
                                $prisutnost->save();
                            }
                        }
                    } else {
                        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $pocetak_date]);
                        if (empty($prisutnost)) {
                            $prisutnost = new Prisutnost();
                            $prisutnost->datum = $pocetak_date;
                            $prisutnost->entitet_id = $zaposlenik->id;
                            $prisutnost->entitet = 'zaposlenik';
                            $prisutnost->status = 0;
                            $prisutnost->pocetak = '08:00';
                            $prisutnost->kraj = '16:00';
                            $prisutnost->trajanje = 8;
                            $prisutnost->save();
                        }
                    }
                    break;
                case 'neposredni':
                case 'posredni':
                case 'vanustanove':
                    $pocetak_datetime = Yii::$app->formatter->asDateTime($start, 'php:Y-m-d H:i:s');
                    $pocetak_date = Yii::$app->formatter->asDate($start, 'php:Y-m-d');
                    $pocetak_time = Yii::$app->formatter->asTime($start);
                    $kraj_datetime = Yii::$app->formatter->asDateTime($end, 'php:Y-m-d H:i:s');
                    $kraj_date = Yii::$app->formatter->asDate($end, 'php:Y-m-d');
                    $kraj_time = Yii::$app->formatter->asTime($end);

                    if ($pocetak_date != $kraj_date) {
                        $kraj_dt = new Datetime(Yii::$app->formatter->asDateTime($pocetak_datetime, 'php:Y-m-d H:i:s'));
                        $pocetak_dt = new Datetime(Yii::$app->formatter->asDateTime($kraj_datetime, 'php:Y-m-d H:i:s'));
                        $interval = date_diff($pocetak_dt, $kraj_dt)->days;
                        $temp_poc = new DateTime($pocetak_date);
                        for ($i = 0; $i <= $interval; $i++) {
                            if ($i == 0) {
                                $temp_poc = $temp_poc;
                            } else {
                                $newinterval = new DateInterval('P1D');
                                $temp_poc = $temp_poc->add($newinterval);
                            }
                            $dan = Yii::$app->formatter->asDate($temp_poc, 'php:Y-m-d');
                            $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $dan]);
                            if (empty($prisutnost)) {
                                $prisutnost = new Prisutnost();
                                $prisutnost->datum = $dan;
                                $prisutnost->entitet_id = $zaposlenik->id;
                                $prisutnost->entitet = 'zaposlenik';
                                $prisutnost->status = 1;
                                if ($i == 0) {
                                    $prisutnost->pocetak = $pocetak_time;
                                    $prisutnost->trajanje = (strtotime('16:00') - strtotime($pocetak_time)) / 3600;
                                } else {
                                    $prisutnost->pocetak = '08:00';
                                }
                                if ($i == $interval) {
                                    $prisutnost->kraj = $kraj_time;
                                    $prisutnost->trajanje = (strtotime($kraj_time) - strtotime('08:00')) / 3600;
                                } else {
                                    $prisutnost->kraj = '16:00';
                                }
                                if (!$prisutnost->trajanje) {
                                    $prisutnost->trajanje = (strtotime('16:00') - strtotime('08:00')) / 3600;
                                }
                                $prisutnost->trajanje = $interval;
                                $prisutnost->save();
                            }
                        }
                    } else {
                        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $pocetak_date]);
                        $kraj_d = new Datetime(Yii::$app->formatter->asDate($pocetak_date, 'php:Y-m-d'));
                        $pocetak_d = new Datetime(Yii::$app->formatter->asDate($kraj_date, 'php:Y-m-d'));
                        if (empty($prisutnost)) {
                            $prisutnost = new Prisutnost();
                            $prisutnost->datum = $pocetak_date;
                            $prisutnost->entitet_id = $zaposlenik->id;
                            $prisutnost->entitet = 'zaposlenik';
                            $prisutnost->status = 1;
                            $prisutnost->pocetak = $pocetak_time;
                            if ($pocetak_d == $kraj_d) {
                                $prisutnost->kraj = $kraj_time;
                            } else if ($pocetak_d < $kraj_date) {
                                $prisutnost->kraj = '16:00';
                            }
                            $prisutnost->trajanje = $kraj_time - $pocetak_time;
                            $prisutnost->save();
                        } else {
                            if ($pocetak_d == $kraj_d) {
                                $prisutnost->kraj = $kraj_time;
                            } else if ($pocetak_d < $kraj_d) {
                                $prisutnost->kraj = '16:00';
                            }
                            $prisutnost->trajanje = $kraj_time - $pocetak_time;
                            $prisutnost->save();
                        }
                    }
                    break;
            }
            //$model->upload($model->id);
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                    $array = ['bolovanje' => ['pocetak' => Yii::$app->request->post('bolovanje_start'), 'kraj' => Yii::$app->request->post('bolovanje_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Bolovanje';
                    $model->save();

                    break;

                case 'godisnji':
                    $array = ['godisnji' => ['pocetak' => Yii::$app->request->post('godisnji_start'), 'kraj' => Yii::$app->request->post('godisnji_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Godišnji';
                    $model->save();

                    break;

                case 'slobodno':
                    $array = ['slobodno' => ['pocetak' => Yii::$app->request->post('slobodno_start'), 'kraj' => Yii::$app->request->post('slobodno_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Slobodno';
                    $model->save();

                    break;

                case 'porodiljni':
                    $array = ['porodiljni' => ['pocetak' => Yii::$app->request->post('porodiljni_start'), 'kraj' => Yii::$app->request->post('porodiljni_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Porodiljni';
                    $model->save();

                    break;

                case 'dopust':
                    $array = ['dopust' => ['pocetak' => Yii::$app->request->post('dopust_start'), 'kraj' => Yii::$app->request->post('dopust_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Dopust';
                    $model->save();

                    break;

                case 'vanustanove':
                    foreach (Yii::$app->request->post('vanustanove') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        $model->zapis = "Zapis o radu pedagoga";
                    }
                    $model->vrijednosti_atributa = ['vanustanove' => $vrijednosti];
                    $model->save();

                    return $this->render('uploadfiles', ['model' => $model]);

                case 'posredni':
                    foreach (Yii::$app->request->post('posredni') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (!empty($vrijednosti['grupacija']) && !empty($vrijednosti['pojedinci'])) {
                            $pojedinci = [];
                            foreach ($vrijednosti['pojedinci'] as $pojedinac) {
                                switch (($vrijednosti['grupacija'])) {
                                    case 'roditelji':
                                        $temp = \common\models\Roditelj::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                    case 'zaposlenici':
                                        $temp = \common\models\Zaposlenik::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                    case 'djeca':
                                        $temp = \common\models\Dijete::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                }
                            }
                            if (count($pojedinci) > 4) {
                                $model->zapis = "Zapis o radu pedagoga: " . implode(', ', array_slice($pojedinci, 0, 3)) . "...";
                            } elseif (count($pojedinci) > 1 && count($pojedinci) < 4) {
                                $model->zapis = "Zapis o radu pedagoga: " . implode(', ', $pojedinci);
                            } else {
                                $model->zapis = "Zapis o radu pedagoga: " . $pojedinci[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu pedagoga";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('posredni_attachments');
                    $model->vrijednosti_atributa = ['posredni' => $vrijednosti];
                    $model->save();

                    return $this->render('uploadfiles', ['model' => $model]);

                case 'neposredni':
                    foreach (Yii::$app->request->post('neposredni') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (!empty($vrijednosti['grupacija']) && !empty($vrijednosti['pojedinci'])) {
                            $pojedinci = [];
                            foreach ($vrijednosti['pojedinci'] as $pojedinac) {
                                switch (($vrijednosti['grupacija'])) {
                                    case 'roditelji':
                                        $temp = \common\models\Roditelj::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                    case 'zaposlenici':
                                        $temp = \common\models\Zaposlenik::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                    case 'djeca':
                                        $temp = \common\models\Dijete::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                }
                            }
                            if (count($pojedinci) > 4) {
                                $model->zapis = "Zapis o radu pedagoga: " . implode(', ', array_slice($pojedinci, 0, 3)) . "...";
                            } elseif (count($pojedinci) > 1 && count($pojedinci) < 4) {
                                $model->zapis = "Zapis o radu pedagoga: " . implode(', ', $pojedinci);
                            } else {
                                $model->zapis = "Zapis o radu pedagoga: " . $pojedinci[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu pedagoga";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('neposredni_attachments');
                    $model->vrijednosti_atributa = ['neposredni' => $vrijednosti];
                    $model->save();

                    return $this->render('uploadfiles', ['model' => $model]);
            }

            return $this->redirect(['creatednevnikrada']);
        }

        $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => Yii::$app->user->id])->one();
        $zapisi_svi = null;
        $events = [];

        $rola = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
        $uloge = [];
        foreach ($rola as $tko => $opis) {
            $uloge[] = $tko;
        }
        if ($uloge[0] == 'superadmin') {
            $dnevnici = Dnevnik::find()->where(['ped_godina' => $godina->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->all();
            $zapisi_svi = [];
            foreach ($dnevnici as $dnevnik) {
                $zapisi_svi[] = DnevnikZapis::find()->where(['vrsta' => $vrsta_zapis->id, 'dnevnik' => $dnevnik->id])->all();
            }
        } elseif (isset($zaposlenik)) {
            $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['ped_godina' => $godina->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->one();
            if (isset($dnevnik)) {
                $zapisi_svi[] = DnevnikZapis::find()->where(['vrsta' => $vrsta_zapis->id, 'dnevnik' => $dnevnik->id])->all();
            }
        }

        if (!empty($zapisi_svi)) {
            foreach ($zapisi_svi as $zapisi) {
                foreach ($zapisi as $zapis) {

                    switch (key($zapis->vrijednosti_atributa)) {
                        case 'vanustanove':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['vanustanove']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['vanustanove']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Rad van ustanove', 'start' => $start, 'end' => $end, 'color' => '#055a4b']);
                            break;
                        case 'posredni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['posredni']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['posredni']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Posredni rad', 'start' => $start, 'end' => $end, 'color' => '#055a4b']);
                            break;
                        case 'neposredni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['neposredni']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['neposredni']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Neposredni rad', 'start' => $start, 'end' => $end, 'color' => '']);
                            break;
                        case 'bolovanje':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['bolovanje']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['bolovanje']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Bolovanje', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'godisnji':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['godisnji']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['godisnji']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Godišenji odmor', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'slobodno':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['slobodno']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['slobodno']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Slobodni dani', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'porodiljni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['porodiljni']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['porodiljni']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Porodiljni dopust', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'dopust':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['dopust']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['dopust']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Dopust', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                    }
                }
            }
        }

        return $this->render('creatednevnikrada', [
            'model' => $model,
            'events' => $events
        ]);
    }

    public function actionUpdatednevnikradapedagog($id)
    {
        $model = $this->findModel($id);

        $vrsta_zapis = \common\models\VrstaZapis::find()->where(['naziv' => 'grupa zapis'])->one();
        $godina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $vrstaDnevnika = VrstaDnevnik::find()->where(['naziv' => 'Dnevnik zapisa'])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $vrijednosti = [];
            $vrijednosti['vrsta'] = Yii::$app->request->post('roles');
            $model->vrsta = $vrsta_zapis->id;
            $objekt = Yii::$app->request->post('objekt');
            $zaposlenik = null;
            if ($objekt != 0) {
                $model->objekt = $objekt;
            } else {
                $model->objekt = null;
            }

            if (Yii::$app->request->post('zaposlenik') === null) {
                $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => Yii::$app->user->id])->one();
                if (isset($zaposlenik)) {
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->andWhere(['ped_godina' => $godina->id])->one();

                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $zaposlenik->id;
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $zaposlenik->id;
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();

                        $model->dnevnik = $novi_dnevnik->id;
                    } else {
                        $model->dnevnik = $dnevnik->id;
                    }
                }
            } else {
                $zaposlenik = \common\models\Zaposlenik::find()->where(['id' => Yii::$app->request->post('zaposlenik')])->one();
                if (isset($zaposlenik)) {
                    $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->andWhere(['ped_godina' => $godina->id])->one();

                    if (!isset($dnevnik)) {
                        $novi_dnevnik = new Dnevnik();
                        $novi_dnevnik->zaposlenik = $zaposlenik->id;
                        $novi_dnevnik->ped_godina = $godina->id;
                        $novi_dnevnik->sifra = '00-' . $zaposlenik->id;
                        $novi_dnevnik->vrsta_dnevnika = $vrstaDnevnika->id;
                        $novi_dnevnik->save();

                        $model->dnevnik = $novi_dnevnik->id;
                    } else {
                        $model->dnevnik = $dnevnik->id;
                    }
                }
            }
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                    $start = Yii::$app->request->post('bolovanje_start');
                    $end = Yii::$app->request->post('bolovanje_end');
                    break;
                case 'godisnji':
                    $start = Yii::$app->request->post('godisnji_start');
                    $end = Yii::$app->request->post('godisnji_end');
                    break;
                case 'slobodno':
                    $start = Yii::$app->request->post('slobodno_start');
                    $end = Yii::$app->request->post('slobodno_end');
                    break;
                case 'porodiljni':
                    $start = Yii::$app->request->post('porodiljni_start');
                    $end = Yii::$app->request->post('porodiljni_end');
                    break;
                case 'dopust':
                    $start = Yii::$app->request->post('dopust_start');
                    $end = Yii::$app->request->post('dopust_end');
                    break;
                case 'neposredni':
                    $start = Yii::$app->request->post('neposredni')['start'];
                    $end = Yii::$app->request->post('neposredni')['end'];
                    break;
                case 'posredni':
                    $start = Yii::$app->request->post('posredni')['start'];
                    $end = Yii::$app->request->post('posredni')['end'];
                    break;
                case 'vanustanove':
                    $start = Yii::$app->request->post('vanustanove')['start'];
                    $end = Yii::$app->request->post('vanustanove')['end'];
                    break;
            }
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                case 'godisnji':
                case 'slobodno':
                case 'porodiljni':
                case 'dopust':
                    $pocetak_date = Yii::$app->formatter->asDate($start, 'php:Y-m-d');
                    $kraj_date = Yii::$app->formatter->asDate($end, 'php:Y-m-d');

                    if ($pocetak_date != $kraj_date) {
                        $kraj_datetime = new Datetime(Yii::$app->formatter->asDateTime($start, 'php:Y-m-d H:i:s'));
                        $pocetak_datetime = new Datetime(Yii::$app->formatter->asDateTime($end, 'php:Y-m-d H:i:s'));
                        $interval = date_diff($pocetak_datetime, $kraj_datetime)->days;
                        $temp_poc = new DateTime($pocetak_date);
                        for ($i = 0; $i <= $interval; $i++) {
                            if ($i == 0) {
                                $temp_poc = $temp_poc;
                            } else {
                                $newinterval = new DateInterval('P1D');
                                $temp_poc = $temp_poc->add($newinterval);
                            }
                            $dan = Yii::$app->formatter->asDate($temp_poc, 'php:Y-m-d');
                            $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $dan]);
                            if (empty($prisutnost)) {
                                $prisutnost = new Prisutnost();
                                $prisutnost->datum = $dan;
                                $prisutnost->entitet_id = $zaposlenik->id;
                                $prisutnost->entitet = 'zaposlenik';
                                $prisutnost->status = 0;
                                $prisutnost->pocetak = '08:00';
                                $prisutnost->kraj = '16:00';
                                $prisutnost->trajanje = 8;
                                $prisutnost->save();
                            }
                        }
                    } else {
                        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $pocetak_date]);
                        if (empty($prisutnost)) {
                            $prisutnost = new Prisutnost();
                            $prisutnost->datum = $pocetak_date;
                            $prisutnost->entitet_id = $zaposlenik->id;
                            $prisutnost->entitet = 'zaposlenik';
                            $prisutnost->status = 0;
                            $prisutnost->pocetak = '08:00';
                            $prisutnost->kraj = '16:00';
                            $prisutnost->trajanje = 8;
                            $prisutnost->save();
                        }
                    }
                    break;
                case 'neposredni':
                case 'posredni':
                case 'vanustanove':
                    $pocetak_datetime = Yii::$app->formatter->asDateTime($start, 'php:Y-m-d H:i:s');
                    $pocetak_date = Yii::$app->formatter->asDate($start, 'php:Y-m-d');
                    $pocetak_time = Yii::$app->formatter->asTime($start);
                    $kraj_datetime = Yii::$app->formatter->asDateTime($end, 'php:Y-m-d H:i:s');
                    $kraj_date = Yii::$app->formatter->asDate($end, 'php:Y-m-d');
                    $kraj_time = Yii::$app->formatter->asTime($end);

                    if ($pocetak_date != $kraj_date) {
                        $kraj_dt = new Datetime(Yii::$app->formatter->asDateTime($pocetak_datetime, 'php:Y-m-d H:i:s'));
                        $pocetak_dt = new Datetime(Yii::$app->formatter->asDateTime($kraj_datetime, 'php:Y-m-d H:i:s'));
                        $interval = date_diff($pocetak_dt, $kraj_dt)->days;
                        $temp_poc = new DateTime($pocetak_date);
                        for ($i = 0; $i <= $interval; $i++) {
                            if ($i == 0) {
                                $temp_poc = $temp_poc;
                            } else {
                                $newinterval = new DateInterval('P1D');
                                $temp_poc = $temp_poc->add($newinterval);
                            }
                            $dan = Yii::$app->formatter->asDate($temp_poc, 'php:Y-m-d');
                            $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $dan]);
                            if (empty($prisutnost)) {
                                $prisutnost = new Prisutnost();
                                $prisutnost->datum = $dan;
                                $prisutnost->entitet_id = $zaposlenik->id;
                                $prisutnost->entitet = 'zaposlenik';
                                $prisutnost->status = 1;
                                if ($i == 0) {
                                    $prisutnost->pocetak = $pocetak_time;
                                    $prisutnost->trajanje = (strtotime('16:00') - strtotime($pocetak_time)) / 3600;
                                } else {
                                    $prisutnost->pocetak = '08:00';
                                }
                                if ($i == $interval) {
                                    $prisutnost->kraj = $kraj_time;
                                    $prisutnost->trajanje = (strtotime($kraj_time) - strtotime('08:00')) / 3600;
                                } else {
                                    $prisutnost->kraj = '16:00';
                                }
                                if (!$prisutnost->trajanje) {
                                    $prisutnost->trajanje = (strtotime('16:00') - strtotime('08:00')) / 3600;
                                }
                                $prisutnost->trajanje = $interval;
                                $prisutnost->save();
                            }
                        }
                    } else {
                        $prisutnost = Prisutnost::findOne(['entitet' => 'zaposlenik', 'entitet_id' => $zaposlenik->id, 'datum' => $pocetak_date]);
                        $kraj_d = new Datetime(Yii::$app->formatter->asDate($pocetak_date, 'php:Y-m-d'));
                        $pocetak_d = new Datetime(Yii::$app->formatter->asDate($kraj_date, 'php:Y-m-d'));
                        if (empty($prisutnost)) {
                            $prisutnost = new Prisutnost();
                            $prisutnost->datum = $pocetak_date;
                            $prisutnost->entitet_id = $zaposlenik->id;
                            $prisutnost->entitet = 'zaposlenik';
                            $prisutnost->status = 1;
                            $prisutnost->pocetak = $pocetak_time;
                            if ($pocetak_d == $kraj_d) {
                                $prisutnost->kraj = $kraj_time;
                            } else if ($pocetak_d < $kraj_date) {
                                $prisutnost->kraj = '16:00';
                            }
                            $prisutnost->trajanje = $kraj_time - $pocetak_time;
                            $prisutnost->save();
                        } else {
                            if ($pocetak_d == $kraj_d) {
                                $prisutnost->kraj = $kraj_time;
                            } else if ($pocetak_d < $kraj_d) {
                                $prisutnost->kraj = '16:00';
                            }
                            $prisutnost->trajanje = $kraj_time - $pocetak_time;
                            $prisutnost->save();
                        }
                    }
                    break;
            }
            //$model->upload($model->id);
            switch (Yii::$app->request->post('roles')) {
                case 'bolovanje':
                    $array = ['bolovanje' => ['pocetak' => Yii::$app->request->post('bolovanje_start'), 'kraj' => Yii::$app->request->post('bolovanje_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Bolovanje';
                    $model->save();

                    break;

                case 'godisnji':
                    $array = ['godisnji' => ['pocetak' => Yii::$app->request->post('godisnji_start'), 'kraj' => Yii::$app->request->post('godisnji_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Godišnji';
                    $model->save();

                    break;

                case 'slobodno':
                    $array = ['slobodno' => ['pocetak' => Yii::$app->request->post('slobodno_start'), 'kraj' => Yii::$app->request->post('slobodno_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Slobodno';
                    $model->save();

                    break;

                case 'porodiljni':
                    $array = ['porodiljni' => ['pocetak' => Yii::$app->request->post('porodiljni_start'), 'kraj' => Yii::$app->request->post('porodiljni_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Porodiljni';
                    $model->save();

                    break;

                case 'dopust':
                    $array = ['dopust' => ['pocetak' => Yii::$app->request->post('dopust_start'), 'kraj' => Yii::$app->request->post('dopust_end')]];
                    $model->vrijednosti_atributa = $array;
                    $model->zapis = 'Dopust';
                    $model->save();

                    break;

                case 'vanustanove':
                    foreach (Yii::$app->request->post('vanustanove') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        $model->zapis = "Zapis o radu pedagoga";
                    }
                    $model->vrijednosti_atributa = ['vanustanove' => $vrijednosti];
                    $model->save();

                    return $this->render('uploadfiles', ['model' => $model]);

                case 'posredni':
                    foreach (Yii::$app->request->post('posredni') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (!empty($vrijednosti['grupacija']) && !empty($vrijednosti['pojedinci'])) {
                            $pojedinci = [];
                            foreach ($vrijednosti['pojedinci'] as $pojedinac) {
                                switch (($vrijednosti['grupacija'])) {
                                    case 'roditelji':
                                        $temp = \common\models\Roditelj::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                    case 'zaposlenici':
                                        $temp = \common\models\Zaposlenik::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                    case 'djeca':
                                        $temp = \common\models\Dijete::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                }
                            }
                            if (count($pojedinci) > 4) {
                                $model->zapis = "Zapis o radu pedagoga: " . implode(', ', array_slice($pojedinci, 0, 3)) . "...";
                            } elseif (count($pojedinci) > 1 && count($pojedinci) < 4) {
                                $model->zapis = "Zapis o radu pedagoga: " . implode(', ', $pojedinci);
                            } else {
                                $model->zapis = "Zapis o radu pedagoga: " . $pojedinci[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu pedagoga";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('posredni_attachments');
                    $model->vrijednosti_atributa = ['posredni' => $vrijednosti];
                    $model->save();

                    return $this->render('uploadfiles', ['model' => $model]);

                case 'neposredni':
                    foreach (Yii::$app->request->post('neposredni') as $key => $value) {
                        $vrijednosti[$key] = $value;
                    };
                    if (!empty($vrijednosti['zapis_custom'])) {
                        $model->zapis = $vrijednosti['zapis_custom'];
                    } else {
                        if (!empty($vrijednosti['grupacija']) && !empty($vrijednosti['pojedinci'])) {
                            $pojedinci = [];
                            foreach ($vrijednosti['pojedinci'] as $pojedinac) {
                                switch (($vrijednosti['grupacija'])) {
                                    case 'roditelji':
                                        $temp = \common\models\Roditelj::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                    case 'zaposlenici':
                                        $temp = \common\models\Zaposlenik::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                    case 'djeca':
                                        $temp = \common\models\Dijete::find()->where(['id' => $pojedinac])->one();
                                        $pojedinci[] = $temp->ime . ' ' . $temp->prezime;
                                        break;
                                }
                            }
                            if (count($pojedinci) > 4) {
                                $model->zapis = "Zapis o radu pedagoga: " . implode(', ', array_slice($pojedinci, 0, 3)) . "...";
                            } elseif (count($pojedinci) > 1 && count($pojedinci) < 4) {
                                $model->zapis = "Zapis o radu pedagoga: " . implode(', ', $pojedinci);
                            } else {
                                $model->zapis = "Zapis o radu pedagoga: " . $pojedinci[0];
                            }
                        } else {
                            $model->zapis = "Zapis o radu pedagoga";
                        }
                    }
                    //$model->attachments = Yii::$app->request->post('neposredni_attachments');
                    $model->vrijednosti_atributa = ['neposredni' => $vrijednosti];
                    $model->save();

                    return $this->render('uploadfiles', ['model' => $model]);
            }
            return $this->redirect(['creatednevnikrada']);
        }

        $zaposlenik = \common\models\Zaposlenik::find()->where(['user' => Yii::$app->user->id])->one();
        $zapisi_svi = null;
        $events = [];

        $rola = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
        $uloge = [];
        foreach ($rola as $tko => $opis) {
            $uloge[] = $tko;
        }
        if ($uloge[0] == 'superadmin') {
            $dnevnici = Dnevnik::find()->where(['ped_godina' => $godina->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->all();
            $zapisi_svi = [];
            foreach ($dnevnici as $dnevnik) {
                $zapisi_svi[] = DnevnikZapis::find()->where(['vrsta' => $vrsta_zapis->id, 'dnevnik' => $dnevnik->id])->all();
            }
        } elseif (isset($zaposlenik)) {
            $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zaposlenik->id])->andWhere(['ped_godina' => $godina->id])->andWhere(['vrsta_dnevnika' => $vrstaDnevnika->id])->one();
            if (isset($dnevnik)) {
                $zapisi_svi[] = DnevnikZapis::find()->where(['vrsta' => $vrsta_zapis->id, 'dnevnik' => $dnevnik->id])->all();
            }
        }

        if (!empty($zapisi_svi)) {
            foreach ($zapisi_svi as $zapisi) {
                foreach ($zapisi as $zapis) {

                    switch (key($zapis->vrijednosti_atributa)) {
                        case 'vanustanove':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['vanustanove']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['vanustanove']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Rad van ustanove', 'start' => $start, 'end' => $end, 'color' => '#055a4b']);
                            break;
                        case 'posredni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['posredni']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['posredni']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Posredni rad', 'start' => $start, 'end' => $end, 'color' => '#055a4b']);
                            break;
                        case 'neposredni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['neposredni']['start']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['neposredni']['end']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Neposredni rad', 'start' => $start, 'end' => $end, 'color' => '']);
                            break;
                        case 'bolovanje':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['bolovanje']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['bolovanje']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Bolovanje', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'godisnji':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['godisnji']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['godisnji']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Godišnji odmor', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'slobodno':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['slobodno']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['slobodno']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Slobodni dani', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'porodiljni':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['porodiljni']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['porodiljni']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Porodiljni dopust', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                        case 'dopust':
                            $start = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['dopust']['pocetak']));
                            $end = date('Y-m-d H:i', strtotime($zapis->vrijednosti_atributa['dopust']['kraj']));
                            $events[] = json_encode(['id' => $zapis->id, 'title' => $zapis->zapis . ', zaposlenik:' . $zapis->dnevnik0->zaposlenik0->ime . ' ' . $zapis->dnevnik0->zaposlenik0->prezime . ', vrsta: Dopust', 'start' => $start, 'end' => $end, 'color' => '#9c0d38']);
                            break;
                    }
                }
            }
        }

        return $this->render('updatednevnikrada', [
            'model' => $model,
            'events' => $events
        ]);
    }

    public function actionGrupacije()
    { {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $pedGodina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
            $pedGodina = $pedGodina->id;
            $out = array();
            if (isset($_POST['depdrop_parents'])) {
                //print("<pre>".print_r($_POST['depdrop_parents'],true)."</pre>");die();
                $ids = $_POST['depdrop_parents'];
                $grupacija = $ids; //empty($ids[0]) ? null : $ids[0]; ukoliko je multiple
                //$selected = '';
                switch ($grupacija[0]) {
                    case 'roditelji':
                        $roditelji = \common\models\Roditelj::find()->all();
                        //$out[]=['id'=>0,'name'=>Yii::t('app', 'Svi roditelji')];
                        foreach ($roditelji as $roditelj) {
                            $out[] = ['id' => $roditelj->id, 'name' => $roditelj->prezime . ' ' . $roditelj->ime];
                        }
                        break;
                    case 'djeca':
                        $djeca = \common\models\Dijete::find()->where(['status' => 2])->all();
                        //$out[]=['id'=>0,'name'=>Yii::t('app', 'Sva djeca')];
                        foreach ($djeca as $dijete) {
                            $out[] = ['id' => $dijete->id, 'name' => $dijete->prezime . ' ' . $dijete->ime];
                        }
                        break;
                    case 'zaposlenici':
                        $zaposlenici = \common\models\Zaposlenik::find()->where(['!=', 'status', 3])->all(); //->where(['status'=>$aktivan->id])->all();
                        //$out[]=['id'=>0,'name'=>Yii::t('app', 'Svi zaposlenici')];
                        foreach ($zaposlenici as $zaposlenik) {
                            $out[] = ['id' => $zaposlenik->id, 'name' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
                        }
                        break;
                }
                return ['output' => $out, 'selected' => ''];
            }
            return ['output' => [], 'selected' => ''];
        }
    }
    public function actionGrupacijezaposlenici()
    { {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $pedGodina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
            $pedGodina = $pedGodina->id;
            $out = array();
            if (isset($_POST['depdrop_parents'])) {
                //print("<pre>".print_r($_POST['depdrop_parents'],true)."</pre>");die();
                $ids = $_POST['depdrop_parents'];
                $grupacija = $ids; //empty($ids[0]) ? null : $ids[0]; ukoliko je multiple
                //$selected = '';
                switch ($grupacija[0]) {
                    case 'odgojitelji':
                        $zaposlenje = \common\models\RadnoMjesto::find()->where(['slug' => 'odgojitelj'])->one();
                        $zaposlenici_ids = \common\models\Zaposlenje::find()->select('zaposlenik')->where(['r_mjesto' => $zaposlenje->id])->asArray()->all();
                        $zaposlenici = \common\models\Zaposlenik::find()->where(['in', 'id', array_column($zaposlenici_ids, 'zaposlenik')])->andWhere(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all(); //->where(['status'=>$aktivan->id])->all();
                        foreach ($zaposlenici as $zaposlenik) {
                            $out[] = ['id' => $zaposlenik->id, 'name' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
                        }
                        break;
                    case 'pedagozi':
                        $zaposlenje = \common\models\RadnoMjesto::find()->where(['slug' => 'pedagog'])->one();
                        $zaposlenici_ids = \common\models\Zaposlenje::find()->select('zaposlenik')->where(['r_mjesto' => $zaposlenje->id])->asArray()->all();
                        $zaposlenici = \common\models\Zaposlenik::find()->where(['in', 'id', array_column($zaposlenici_ids, 'zaposlenik')])->andWhere(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all();
                        foreach ($zaposlenici as $zaposlenik) {
                            $out[] = ['id' => $zaposlenik->id, 'name' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
                        }
                        break;
                    case 'psiholozi':
                        $zaposlenje = \common\models\RadnoMjesto::find()->where(['slug' => 'psiholog'])->one();
                        $zaposlenici_ids = \common\models\Zaposlenje::find()->select('zaposlenik')->where(['r_mjesto' => $zaposlenje->id])->asArray()->all();
                        $zaposlenici = \common\models\Zaposlenik::find()->where(['in', 'id', array_column($zaposlenici_ids, 'zaposlenik')])->andWhere(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all();
                        foreach ($zaposlenici as $zaposlenik) {
                            $out[] = ['id' => $zaposlenik->id, 'name' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
                        }
                        break;
                    case 'logopedi':
                        $zaposlenje = \common\models\RadnoMjesto::find()->where(['slug' => 'logoped'])->one();
                        $zaposlenici_ids = \common\models\Zaposlenje::find()->select('zaposlenik')->where(['r_mjesto' => $zaposlenje->id])->asArray()->all();
                        $zaposlenici = \common\models\Zaposlenik::find()->where(['in', 'id', array_column($zaposlenici_ids, 'zaposlenik')])->andWhere(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all();
                        foreach ($zaposlenici as $zaposlenik) {
                            $out[] = ['id' => $zaposlenik->id, 'name' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
                        }
                        break;
                    case 'rehabilitatori':
                        $zaposlenje = \common\models\RadnoMjesto::find()->where(['slug' => 'rehabilitator'])->one();
                        $zaposlenici_ids = \common\models\Zaposlenje::find()->select('zaposlenik')->where(['r_mjesto' => $zaposlenje->id])->asArray()->all();
                        $zaposlenici = \common\models\Zaposlenik::find()->where(['in', 'id', array_column($zaposlenici_ids, 'zaposlenik')])->andWhere(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all();
                        foreach ($zaposlenici as $zaposlenik) {
                            $out[] = ['id' => $zaposlenik->id, 'name' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
                        }
                        break;
                    case 'zdravstveni_voditelji':
                        $zaposlenje = \common\models\RadnoMjesto::find()->where(['slug' => 'zdravstveni_voditelj'])->one();
                        $zaposlenici_ids = \common\models\Zaposlenje::find()->select('zaposlenik')->where(['r_mjesto' => $zaposlenje->id])->asArray()->all();
                        $zaposlenici = \common\models\Zaposlenik::find()->where(['in', 'id', array_column($zaposlenici_ids, 'zaposlenik')])->andWhere(['!=', 'status', 3])->orderBy('prezime,ime ASC')->all();
                        foreach ($zaposlenici as $zaposlenik) {
                            $out[] = ['id' => $zaposlenik->id, 'name' => $zaposlenik->prezime . ' ' . $zaposlenik->ime];
                        }
                        break;
                }
                return ['output' => $out, 'selected' => ''];
            }
            return ['output' => [], 'selected' => ''];
        }
    }

    public function actionDeletednevnikrada($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['creatednevnikrada']);
    }

    private function printPDF($sadrzaj, $pdfPath, $nazivDokumenta, $orient = 'P')
    {
        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 15, 'margin_right' => 15, 'margin_top' => 16, 'margin_bottom' => 16, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }
    private function printPDFfull($sadrzaj, $pdfPath, $nazivDokumenta, $orient = 'P')
    {
        $tempDir = Yii::getAlias('@webroot') . "/upload";
        $mpdf = new Mpdf\Mpdf(['C', 'format' => 'A4', 12, '', 'margin_left' => 5, 'margin_right' => 5, 'margin_top' => 5, 'margin_bottom' => 5, 'mgh' => 1, 'mgf' => 1, 'orientation' => $orient, 'tempDir' => $tempDir]);

        $mpdf->WriteHTML($sadrzaj);

        $pdfDir = dirname($pdfPath);
        if (!file_exists($pdfDir)) {
            mkdir($pdfDir, 0777, true);
        }

        $mpdf->Output($pdfPath, \Mpdf\Output\Destination::FILE);
        return Yii::$app->response->sendFile($pdfPath, $nazivDokumenta);
    }
    public function actionPdfDnevnikRadaPrint($id)
    {
        $dnevnik_rada = DnevnikZapis::findOne($id);

        $htmlContent = $this->renderPartial('@common/views/templates/dnevnikzapispdf', [
            'dnevnik_rada' => $dnevnik_rada,
        ]);
        $naziv = str_replace([" ", ":"], "_", $dnevnik_rada->zapis . "-" . $dnevnik_rada->id);
        $dirpath = Yii::getAlias('@webroot') . "/dnevnici_rada/";
        $pdf_file = $this::printPDF($htmlContent, $dirpath . $naziv . '.pdf', $naziv . ".pdf", "L");
        if (isset($pdf_file)) {
            return $pdf_file;
        } else {
            throw new \yii\web\NotFoundHttpException('PDF creation failed.');
        }
    }

    public function actionPdfPrintDnevnikRadaSvi($id)
    {
        set_time_limit(0);
        $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $skupina = Skupina::findOne($id);
        $zaposlenici_ids = ZaposlenikSkupina::find()->select('zaposlenik')->where(['skupina' => $id])->asArray()->all();
        if (empty($zaposlenici_ids)) {
            throw new \yii\web\NotFoundHttpException('Ne postoji zaposlenik dodjeljen skupini.');
        }
        $dnevnici_ids = Dnevnik::find()->select('id')->where(['in', 'zaposlenik', array_column($zaposlenici_ids, "zaposlenik")])->andWhere(['ped_godina' => $godina->id])->asArray()->all();
        $dnevnici_rada = DnevnikZapis::find()->where(['in', 'dnevnik', array_column($dnevnici_ids, "id")])->all();
        $dnevnici_rada_filtered = [];
        foreach ($dnevnici_rada as $dnevnik) {
            if (isset($dnevnik->vrijednosti_atributa)) {
                if (array_key_exists('neposredni', $dnevnik->vrijednosti_atributa)) {
                    $dnevnici_rada_filtered[] = $dnevnik;
                }
            }
        }
        $imeSkupine = str_replace([" ", ":"], "_", $skupina->naziv);
        $zipName = 'Dnevnici_Rada-Skupina-' . $imeSkupine . '.zip';
        $zipDir = Yii::getAlias('@webroot') . '/dnevnici_rada/';
        if (!file_exists($zipDir)) {
            mkdir($zipDir, 0777, true);
        }
        $zip = new ZipArchive();
        $archive = $zip->open($zipDir . $zipName, (ZipArchive::CREATE | ZipArchive::OVERWRITE));
        if ($archive === TRUE) {
            foreach ($dnevnici_rada_filtered as $dnevnik_rada) {
                $this->actionPdfDnevnikRadaPrint($dnevnik_rada->id);
                $naziv = str_replace([" ", ":"], "_", $dnevnik_rada->zapis . "-" . $dnevnik_rada->id) . ".pdf";
                $dirpath = Yii::getAlias('@webroot') . '/dnevnici_rada/';
                $filePath = $dirpath . $naziv;
                $zip->addFile($filePath, $naziv);
            }
            $zip->close();
            foreach ($dnevnici_rada_filtered as $dnevnik_rada) {
                $naziv = str_replace([" ", ":"], "_", $dnevnik_rada->zapis . "-" . $dnevnik_rada->id) . ".pdf";
                $dirpath = Yii::getAlias('@webroot') . '/dnevnici_rada/';
                $filePath = $dirpath . $naziv;
                unlink($filePath);
            }
        } else {
            echo "Error creating zip file: " . $archive;
        }
        $zipFullPath = $zipDir . $zipName;
        if (!is_file($zipFullPath)) {
            throw new \yii\web\NotFoundHttpException('The file does not exists.');
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename=' . $zipName);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($zipFullPath));
        ob_clean();
        flush();
        readfile($zipFullPath);
        //Yii::$app->response->on(\yii\web\Response::EVENT_AFTER_SEND, function ($event) {
        //    unlink($event->data);
        //}, $zipFullPath);
        Yii::$app->response->sendFile($zipFullPath, $zipName);
        return unlink($zipFullPath);
    }
    public function actionEvidencija()
    {
        $datumi = $this->getDatum(0);
        $godina = PedagoskaGodina::getAktivna();
        $zaposlenici = Zaposlenik::find()->where(['!=', 'status', 3])->all();
        $skupine = Skupina::find()->where(['ped_godina' => $godina->id])->all();

        return $this->render('evidencija', ['zaposlenici' => $zaposlenici, 'datumi' => $datumi, 'skupine' => $skupine]);
    }
    public function actionZaposleniciDodaci()
    {
        $godina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();
        $zaposlenik = Zaposlenik::find()->where(['!=', 'status', 3])->all();





        $mjesec = Utils::getMjesecPed($_POST['mjesec']);
        $numberDays = cal_days_in_month(CAL_GREGORIAN, $mjesec['mjesec'], date('Y'));
        $poc = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-01';
        $kraj = $mjesec['godina'] . '-' . $mjesec['mjesec'] . '-' . $numberDays;
        $temp = [];
        $header = ['Zaposlenik', 'Mentorstvo', 'Prekapaciranost', 'S teškoćama', 'Subota', 'Nedjelja', 'Noćni', 'Prekovremeno'];
        $retci = [];


        $ukupnoPre = 0;
        $ukupnoTe = 0;
        // koliko ima radnih dana u mjesecu
        $lastday = date("t", mktime(0, 0, 0, $mjesec['mjesec'], 1, $mjesec['godina']));
        $weekdays = 0;
        for ($d = 29; $d <= $lastday; $d++) {
            $wd = date("w", mktime(0, 0, 0, $mjesec['mjesec'], $d, $mjesec['godina']));
            if ($wd > 0 && $wd < 6) $weekdays++;
        }
        $radniDani = $weekdays + 20;

        foreach ($zaposlenik as $zap) {
            $zaNaziv = $zap->ime . ' ' . $zap->prezime;
            $sTeskocama = 0;
            $prekapaciranost = 0;
            $mentor = 0;
            $subotom = 0;
            $nedjeljom = 0;
            $prekovremeno = 0;
            $nocni = 0;
            $dnevnik = Dnevnik::find()->where(['zaposlenik' => $zap->id])->andWhere(['vrsta_dnevnika' => 2])->andWhere(['ped_godina' => $godina->id])->one();
            if (!isset($dnevnik)) {
                continue;
            }
            $temp[0] = '<span >' . $zaNaziv . '</span>';
            $zapisi = \common\models\DnevnikZapis::find()->where(['vrsta' => 28, 'dnevnik' => $dnevnik->id])->all();
            // za svaki zapis u tom danu
            $i = 1;
            foreach ($zapisi as $zapis) {


                switch (key($zapis->vrijednosti_atributa)) {
                    case 'neposredni':
                        $skupina = Skupina::findOne($zapis->vrijednosti_atributa['neposredni']['skupina']);
                        $imaSTeskocama = false;
                        $status_upisan = StatusDjeteta::find()->where(['status' => 'upisan'])->one();
                        $djecaBroj = Dijete::find()->leftJoin('dijete_skupina', 'dijete_skupina.dijete = dijete.id')->where(['dijete_skupina.skupina' => $skupina->id])->andWhere(['dijete.status' => $status_upisan->id])->orderBy(['prezime' => SORT_ASC])->all();
                        $brojDjece = count($djecaBroj);
                        // funkcija koja traži ima li u skupini djece s teškoćama
                        foreach ($djecaBroj as $ds) {
                            if ($ds->razvoj != 'uredan' || $ds->razvoj != null) {
                                $imaSTeskocama = true;
                                break;
                            }
                        }
                        if ($imaSTeskocama) {
                            $sTeskocama += 8;
                        }

                        if (isset($zapis->vrijednosti_atributa['neposredni']['mentor']) && $zapis->vrijednosti_atributa['neposredni']['mentor'] == 1) {
                            $mentor += 8;
                        }
                        if ($brojDjece >= (20 * 1.2)) {
                            $prekapaciranost += 8;
                        }
                        $trajanje =  strtotime($zapis->vrijednosti_atributa['neposredni']['end']) -  strtotime($zapis->vrijednosti_atributa['neposredni']['start']);

                        if (date('H', strtotime($zapis->vrijednosti_atributa['neposredni']['start'])) >= 22 || date('H', strtotime($zapis->vrijednosti_atributa['neposredni']['start'])) <= 6) {
                            $nocni += ($trajanje / 3600);
                        }

                        // funkcija za provjeru naziva dana za subotu i nedjelju
                        if (date('N', strtotime($zapis->vrijednosti_atributa['neposredni']['end'])) == 6) {
                            $subotom += ($trajanje / 3600);
                        }
                        if (date('N', strtotime($zapis->vrijednosti_atributa['neposredni']['end'])) == 7) {
                            $nedjeljom += ($trajanje / 3600);
                        }
                        $prekovremeno += ($trajanje / 3600);
                        break;
                    case 'posredni':
                        $trajanje =  strtotime($zapis->vrijednosti_atributa['posredni']['end']) -  strtotime($zapis->vrijednosti_atributa['posredni']['start']);
                        if (date('N', strtotime($zapis->vrijednosti_atributa['posredni']['end'])) == 6) {
                            $subotom += ($trajanje / 3600);
                        }
                        if (date('N', strtotime($zapis->vrijednosti_atributa['posredni']['end'])) == 7) {
                            $nedjeljom += ($trajanje / 3600);
                        }
                        if (date('H', strtotime($zapis->vrijednosti_atributa['posredni']['start'])) >= 22 || date('H', strtotime($zapis->vrijednosti_atributa['posredni']['start'])) <= 6) {
                            $nocni += ($trajanje / 3600);
                        }
                        $prekovremeno += ($trajanje / 3600);
                        break;
                    case 'vanustanove':
                        $trajanje =  strtotime($zapis->vrijednosti_atributa['vanustanove']['end']) -  strtotime($zapis->vrijednosti_atributa['vanustanove']['start']);
                        if (date('N', strtotime($zapis->vrijednosti_atributa['vanustanove']['end'])) == 6) {
                            $subotom += ($trajanje / 3600);
                        }
                        if (date('N', strtotime($zapis->vrijednosti_atributa['vanustanove']['end'])) == 7) {
                            $nedjeljom += ($trajanje / 3600);
                        }
                        if (date('H', strtotime($zapis->vrijednosti_atributa['vanustanove']['start'])) >= 22 || date('H', strtotime($zapis->vrijednosti_atributa['vanustanove']['start'])) <= 6) {
                            $nocni += ($trajanje / 3600);
                        }
                        $prekovremeno += ($trajanje / 3600);
                        break;
                }
                $prekovremeni = $prekovremeno - ($radniDani * 8);
                if ($prekovremeni <= 0) {
                    $prekovremeni = 0;
                }
                $i++;
            }
            $ukupnoPre += $prekapaciranost;
            $temp[1] = $mentor;
            $temp[2] = $prekapaciranost;
            $temp[3] = $sTeskocama;
            $temp[4] = round($subotom, 2);
            $temp[5] = round($nedjeljom, 2);
            $temp[6] = round($nocni, 2);
            $temp[7] = round($prekovremeni, 2);
            $retci[] = $temp;
        }



        /*		for($j=count($temp);$j<=$numberDays;$j){
				$temp[]='';
			}*/


        $prisutnostFull = array_merge([$header], $retci);
        $htmlContent = $this->renderPartial('@common/views/templates/zaposleniciDodaci', ['prisutnost' => $prisutnostFull, 'zaNaziv' => $zaNaziv, 'mjesec' => $mjesec, 'ukupnoPre' => $ukupnoPre, 'ukupnoTe' => $ukupnoTe]);
        $naziv = 'Dodaci_na_placu_' . $mjesec['mjesec'];
        Utils::ispisPDF($htmlContent, $naziv, 'download', 'L');
    }
    private function getDatum($tjedan)
    {
        $datum['pon'] = date('Y-m-d', strtotime('monday this week'));
        $datum['uto'] = date('Y-m-d', strtotime('tuesday this week'));
        $datum['sri'] = date('Y-m-d', strtotime('wednesday this week'));
        $datum['cet'] = date('Y-m-d', strtotime('thursday this week'));
        $datum['pet'] = date('Y-m-d', strtotime('friday this week'));
        if ($tjedan == '0') {
            return $datum;
        } else {
            $datum['pon'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pon'])));
            $datum['uto'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['uto'])));
            $datum['sri'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['sri'])));
            $datum['cet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['cet'])));
            $datum['pet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pet'])));
        }
        return $datum;
    }

    /**
     * Finds the DnevnikZapis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DnevnikZapis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DnevnikZapis::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
