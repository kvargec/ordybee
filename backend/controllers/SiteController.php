<?php

namespace backend\controllers;

use common\models\Prisutnost;
use common\models\Search\ZahtjevSearch;
use common\models\Skupina;
use common\models\StatusDjeteta;
use common\models\PedagoskaGodina;
use common\models\Zahtjev;
use common\models\Zaposlenje;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Dijete;
use common\models\Aktivnosti;
use common\models\Search\CalendarSearch;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'godina', 'docs', 'docsdjeca', 'docssluzbe', 'docsadmini'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'report', 'docs', 'docsdjeca', 'docssluzbe', 'docsadmini'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $rola = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
        $uloge = array();

        foreach ($rola as $tko => $opis) {
            $uloge[] = $tko;
        }
        if (empty($uloge[0])) {
            return $this->render('dashboard/prazno');
        } else {
            $datum = $this->getDatum(0);
            $status_upisan = StatusDjeteta::find()->where(['status' => 'upisan'])->one();
            $prosli_tjedan = $this->getDatum(-1);
            $pretprosli_tjedan = $this->getDatum(-2);
            $brojDjece = [];
            foreach ($datum as $key => $value) {
                $brojDjece[$key] = Prisutnost::find()->where(['datum' => $value, 'entitet' => 'dijete', 'status' => 1])->count();
            }
            $brojDjeceProsli = [];
            foreach ($prosli_tjedan as $key => $value) {
                $brojDjeceProsli[$key] = Prisutnost::find()->where(['datum' => $value, 'entitet' => 'dijete', 'status' => 1])->count();
            }
            $brojDjecePretprosli = [];
            foreach ($pretprosli_tjedan as $key => $value) {
                $brojDjecePretprosli[$key] = Prisutnost::find()->where(['datum' => $value, 'entitet' => 'dijete', 'status' => 1])->count();
            }
            $brojzahtjeva = Zahtjev::find()->count();
            //u izradi, predani, u obradi
            $ped_god = PedagoskaGodina::find()->where(['aktivna' => true])->one();
            $statusi = [1, 2, 3];
            $brojObradjenih = Zahtjev::find()->where(['in', 'status', $statusi])->count();
            $zaZahtjeve = ($brojzahtjeva - $brojObradjenih) . '/' . $brojzahtjeva;
            $upisaneDjece = Dijete::find()->where(['in', 'status', [2]])->count();
            $skupine = Skupina::find()->where(['ped_godina' => $ped_god->id])->all();
            $djecePoSkupini = [];
            $prisutneDjecePoSkupini = [];
            foreach ($skupine as $skupine2) {
                $djecePoSkupini[$skupine2->id] = Dijete::find()->leftJoin('dijete_skupina', 'dijete_skupina.dijete = dijete.id')->where(['dijete_skupina.skupina' => $skupine2->id])->andWhere(['dijete.status' => $status_upisan->id])->orderBy(['prezime' => SORT_ASC])->count();
                $prisutneDjecePoSkupini[$skupine2->id] = Prisutnost::find()->leftJoin('dijete_skupina', 'dijete_skupina.dijete = prisutnost.entitet_id')->where(['dijete_skupina.skupina' => $skupine2->id, 'datum' => date('Y-m-d', time()), 'entitet' => 'dijete', 'status' => 1])->count();
            }
            $prisutneDjece = Prisutnost::find()->where(['datum' => date('Y-m-d', time()), 'entitet' => 'dijete', 'status' => 1])->count();
            $zaposlenih = Zaposlenje::find()->where('date_kraj is null')->orWhere('date_kraj > NOW()')->count();
            $prisutniZaposleni = Prisutnost::find()->where(['datum' => date('Y-m-d', time()), 'entitet' => 'zaposlenik', 'status' => 1])->count();
            $search = new CalendarSearch();
            $dataProvider = $search->search(Yii::$app->request->getQueryParams());
            $dataProvider->pagination = false;
            $events = array();
            foreach ($dataProvider->models as $item) {
                $events[] = json_encode(['id' => $item->id, 'title' => $item->napomena, 'start' => $item->dan]);
            }
            $aktivnosti = Aktivnosti::find()->all();
            foreach ($aktivnosti as $aktiv) {
                $events[] = json_encode(['id' => $aktiv->id, 'title' => $aktiv->naziv, 'start' => $aktiv->datum_pocetka, 'end' => $aktiv->datum_kraja]);
            }



            $options = [
                'obradeni' => $brojzahtjeva - $brojObradjenih,
                'brojZahtjeva' => $brojzahtjeva,
                'zahtjevi' => $zaZahtjeve,
                'upisane' => $upisaneDjece,
                'zaposlenih' => $zaposlenih,
                'prisutneDjece' => $prisutneDjece,
                'skupine' => $skupine,
                'prisutniZaposleni' => $prisutniZaposleni,
                'brojDjece' => $brojDjece,
                'brojDjeceProsli' => $brojDjeceProsli,
                'brojDjecePretprosli' => $brojDjecePretprosli,
                'dataProvider' => $dataProvider,
                'events' => $events,
                'djecePoSkupini' => $djecePoSkupini,
                'prisutneDjecePoSkupini' => $prisutneDjecePoSkupini
            ];
            return $this->render('dashboard/' . $uloge[0], $options);
        }
    }
    public function actionReport()
    {
        $report = new \backend\reports\Testreport;
        $report->run();
        return $this->render('report', array(
            "report" => $report
        ));
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    private function getDatum($tjedan)
    {
        $datum['pon'] = date('Y-m-d', strtotime('monday this week'));
        $datum['uto'] = date('Y-m-d', strtotime('tuesday this week'));
        $datum['sri'] = date('Y-m-d', strtotime('wednesday this week'));
        $datum['cet'] = date('Y-m-d', strtotime('thursday this week'));
        $datum['pet'] = date('Y-m-d', strtotime('friday this week'));
        if ($tjedan == '0') {
            return $datum;
        } else {
            $datum['pon'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pon'])));
            $datum['uto'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['uto'])));
            $datum['sri'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['sri'])));
            $datum['cet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['cet'])));
            $datum['pet'] = date('Y-m-d', strtotime($tjedan . ' week', strtotime($datum['pet'])));
        }
        return $datum;
    }
    public function actionGodina()
    {
        if (isset($_POST['ped_godina'])) {
            $_SESSION['ped_godina'] = $_POST['ped_godina'];
        }
        return $this->redirect(isset(Yii::$app->request->referrer) ? Yii::$app->request->referrer : Yii::$app->homeUrl);
    }
    public function actionDocs()
    {
        $this->layout = 'docslayout';
        return $this->render("docs/docs");
    }
    public function actionDocsdjeca()
    {
        $this->layout = 'docslayout';
        return $this->render("docs/docsdjeca");
    }
    public function actionDocssluzbe()
    {
        $this->layout = 'docslayout';
        return $this->render("docs/docssluzbe");
    }
    public function actionDocsadmini()
    {
        $this->layout = 'docslayout';
        return $this->render("docs/docsadmini");
    }
}
