<?php

namespace backend\controllers;

use common\models\Document;
use common\models\NotifikacijeDokument;
use common\models\RadnoMjesto;
use common\models\Zaposlenik;
use common\models\Zaposlenje;
use Yii;
use common\models\Notifikacije;
use common\models\Search\NotifikacijeSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NotifikacijeController implements the CRUD actions for Notifikacije model.
 */
class NotifikacijeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notifikacije models.
     * @return mixed
     */
    public function actionIndex($inbox = null, $sent=null, $trash=null)
    {
        $select_tab = '';
        $searchModel = new NotifikacijeSearch();
        if ($inbox){
            $searchModel->inbox = $inbox;
            $select_tab = 'inbox';
        }
        if ($sent){
            $searchModel->sent=$sent;
            $select_tab = 'sent';
        }
        if($trash){
            $searchModel->trash=$trash;
            $select_tab = 'trash';
        }
        $count = Notifikacije::find()->where(['user' => Yii::$app->user->id, 'read_at' => null, 'user_trash' => false, 'user_delete' => false])->count();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'count' => $count,
            'select_tab' => $select_tab
        ]);
    }

    /**
     * Displays a single Notifikacije model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $tip)
    {
        $model = $this->findModel($id);
        if($model->user == Yii::$app->user->id && $model->read_at == null){
            $model->updateAttributes(['read_at' => date('Y-m-d H:i:s', time())]);
        }
        $count = Notifikacije::find()->where(['user' => Yii::$app->user->id, 'user_trash' => false, 'user_delete' => false, 'read_at' => null])->count();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'count' => $count,
            'tip' => $tip
        ]);
    }

    /**
     * Creates a new Notifikacije model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notifikacije();

        if ($model->load(Yii::$app->request->post()) ) {
            $users = $model->user['user'];
            $grupe = $model->user['grupa'];
            if (isset($users)) {
                foreach ($users as $key=>$user) {
                    $model2 = new Notifikacije();
                    $model2->user = $user;
                    $model2->sadrzaj = $model->sadrzaj;
                    $model2->created_at = $model->created_at;
                    $model2->sender_id = $model->sender_id;
                    $model2->subject = $model->subject;
                    $model2->is_deleted = false;
                    $model2->save();
                    if ($key == 0){
                        $model2->uploadfirst($model->getAttachDir(), $model->id);
                    }else{
                        $model2->uploadother($model->getAttachDir(), $model->id);

                    }
                    $model2->save();
                }
            }
            if (!empty($grupe)) {
                foreach ($grupe as $grupa) {
                    if ($grupa == 'roditelji') {
                        //za roditelje slanje notifiakcije
                    } else {
                        $radnoMjesto = RadnoMjesto::find()->where(['naziv' => $grupa])->one();
                        $zaposlenici = Zaposlenik::find()->joinWith('zaposlenjes')->where(['zaposlenje.r_mjesto' => $radnoMjesto->id])->all();

                        //za zaposlenike slanje notifikacije
                    }
                }
            }
            Yii::$app->session->setFlash('success', Yii::t('app', 'Notifikacija uspješno poslana'));
            return $this->redirect(['/notifikacije/index', 'sent' => Yii::$app->user->id]);
        }
        $mediaProvider = new ActiveDataProvider([
            'query' => Document::find(),
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        return $this->render('create', [
            'model' => $model,
            'mediaProvider' => $mediaProvider
        ]);
    }

    /**
     * Updates an existing Notifikacije model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Notifikacije model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notifikacije model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notifikacije the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notifikacije::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public static function logCRUD($controller, $model_id, $action, $user_id){
        $notifikacija = new Notifikacije();
        $notifikacija->user = $user_id;
        $notifikacija->akcija = $controller;
        $notifikacija->akcija_id = $model_id;
        $notifikacija->radnja = $action;
        if($action == 'view'){
            $notifikacija->read_at = date('Y-m-d H:i:s');
        }
        $notifikacija->status = 1;
        $notifikacija->save();
    }

    public static function getNotifications($user_id){
        $notifikacije = Notifikacije::find()->where(['user' => $user_id, 'read_at' => null])->all();
        return $notifikacije;
    }
    public function actionProcitajNotifikaciju($id){
        $notifikacija = $this->findModel($id);
        $notifikacija->updateAttributes(['read_at' => date('Y-m-d H:i:s', time())]);
        return $this->render('read', ['notifikacija' => $notifikacija]);

    }
    public function actionTrash($id){
        $model = Notifikacije::findOne(['id' => $id]);
        if($model->user == Yii::$app->user->id){
            $model->updateAttributes(['user_trash' => true]);
            return $this->redirect(['/notifikacije/index', 'inbox' => Yii::$app->user->id]);
        }
        if ($model->sender_id == Yii::$app->user->id){
            $model->updateAttributes(['sender_trash' => true]);
            return $this->redirect(['/notifikacije/index', 'sent' => Yii::$app->user->id]);
        }
        return $this->redirect(['/notifikacije/index', 'inbox' => Yii::$app->user->id]);

    }
    public function actionEmptyTrash($id){
        $model = Notifikacije::findOne(['id' => $id]);
        if($model->user == Yii::$app->user->id){
            $model->updateAttributes(['user_delete' => true]);
        }
        if ($model->sender_id == Yii::$app->user->id){
            $model->updateAttributes(['sender_delete' => true]);
        }
        return $this->redirect(['/notifikacije/index', 'trash' => Yii::$app->user->id]);

    }

}
