<?php

namespace backend\controllers;

use common\models\Search\DijeteSearch;
use Yii;
use common\models\Dnevnik;
use common\models\Search\DnevnikSearch;
use common\models\DnevnikZapis;
use common\models\Search\DnevnikZapisSearch;
use yii\data\ActiveDataProvider;
use yii\debug\models\timeline\DataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DnevnikController implements the CRUD actions for Dnevnik model.
 */
class DnevnikController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dnevnik models.
     * @return mixed
     */
    public function actionIndex($tip)
    {
        $searchModel = new DijeteSearch();
        $parametri=Yii::$app->request->queryParams;
        if(!isset($parametri['DijeteSearch']['status'])){
            $parametri['DijeteSearch']['status']=2;
        }
        switch ($tip){
            case 'zdravstvo':
                $view = 'index';
                break;

            default:
                $view = 'ostalidnevnici';
        }
        $dataProvider = $searchModel->search($parametri);

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tip'=>$tip
        ]);
    }

    public function actionIndex2(){

        $searchModel = new DnevnikSearch();
        $parametri = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($parametri);
        return $this->render('index2',[
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel
        ]);
    }

    public function actionView2($id){
        $searchModel = new DnevnikZapisSearch();
        $parametri = Yii::$app->request->queryParams;
        if(!isset($parametri['sort']))
            $parametri['sort']='-created_at';
        $dataProvider = $searchModel->search($parametri);
        return $this->render('dnevnikzapisi',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }
    /**
     * Displays a single Dnevnik model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->request->isPost) {
            // print_r(Yii::$app->request->post());
            // die();
        }

        $model = $this->findModel($id);
        $dnevnikZapis = $model->createDnevnikZapis();
        if ($dnevnikZapis->load(Yii::$app->request->post()) && $dnevnikZapis->save())
        {
            $dnevnikZapis = new DnevnikZapis(); //reset model
        }

        $searchModel = new DnevnikZapisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['dnevnik'=>$id]);
        $attributes = $model->vrstaDnevnika->getDnevnikZapisFormAttribs();

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dnevnikZapis' => $dnevnikZapis,
            'attributes' => $attributes,
        ]);
    }

    /**
     * Creates a new Dnevnik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dnevnik();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Dnevnik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Dnevnik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Dnevnik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dnevnik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dnevnik::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
