<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Objekt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="objekt-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adresa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sifra')->textInput(['maxlength' => true]) ?>

    <?php //$form->field($model, 'mjesto')->textInput() ?>

    <?= $form->field($model, 'mjesto')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(
                \common\models\Mjesto::find()->all(),
                'id',
                'naziv'
            ),
            // 'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
