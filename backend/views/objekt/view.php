<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Objekt */

$this->title = $model->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Objekts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="objekt-view">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<span class="material-icons">print</span>', ['objekt/pdf-print', 'id' => $model->id], ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
    </div>
<div class="card-body detail">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'naziv',
            'adresa',
            'sifra',
            'mjesto',
            [
                'attribute' => 'ped_godina',
                'value' => function($data){
                    return Yii::$app->formatter->asDate($data->pedGodina->od,'php:Y').'./'.Yii::$app->formatter->asDate($data->pedGodina->do,'php:Y').'.';
                }
            ],
        ],
    ]) ?>

</div>
</div></div>