<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ObjektSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Objekts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objekt-index">
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            $gridColumns = [
                // ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                [
                    'value' => 'naziv',
                    'label' => Yii::t('app', 'Naziv'),
                    'attribute' => 'naziv',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Naziv'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'adresa',
                    'label' => Yii::t('app', 'Adresa'),
                    'attribute' => 'adresa',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Adresa'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'sifra',
                    'label' => Yii::t('app', 'Šifra'),
                    'attribute' => 'sifra',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Šifra'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'mjesto0.naziv',
                    'label' => Yii::t('app', 'Mjesto'),
                    'attribute' => 'mjesto_naziv',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Naziv mjesta'),
                        'title' => Yii::t('app', 'Mjesto'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'label' => Yii::t('app', 'Ped. godina'),
                    'attribute' => 'ped_godina',
                    'value' => function ($data) {
                        return $data->pedGodina->getNaziv();
                    },
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filter' => yii\helpers\ArrayHelper::map(\common\models\PedagoskaGodina::find()->all(), 'id', function ($v) {
                        return $v->getNaziv();
                    })
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['objekt/view', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pogledaj'),
                            ]);
                        },

                        'update' => function ($url, $data) {
                            $url2 = \Yii::$app->urlManager->createUrl(['objekt/update', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">create</span>', $url2, [
                                'title' => Yii::t('app', 'Ažuriraj'),
                            ]);
                        },


                    ],
                ],
            ];
            ?>

            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                    ExportMenu::FORMAT_CSV => null,
                ],
                'filename' => 'popisObjekata',
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/dijete/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" .
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                ]); ?>


        </div>
    </div>
</div>