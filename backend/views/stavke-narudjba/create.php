<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StavkeNarudjba */

$this->title = Yii::t('app', 'Create Stavke Narudjba');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Stavke Narudjbas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stavke-narudjba-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
