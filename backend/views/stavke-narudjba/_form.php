<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\StavkeNarudjba */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stavke-narudjba-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ulazni_racun')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\UlazniRacun::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'proizvod')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Proizvod::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'kolicina')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
