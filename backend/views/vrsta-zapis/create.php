<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VrstaZapis */

$this->title = Yii::t('app', 'Create Vrsta Zapis');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vrsta Zapis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrsta-zapis-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
