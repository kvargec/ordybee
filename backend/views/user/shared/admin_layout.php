<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var $content string
 */

?>
<div class="clearfix"></div>

<?= $this->render(
    '/shared/_alert',
    [
        'module' => Yii::$app->getModule('user'),
    ]
) ?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <span class="card-title"><?= Html::encode($this->title) ?></span>
            </div>
            <div class="py-15">
                <div class="panel-body">
                    <style>.grid-view{    max-width: 100%!important;
    margin: 0!important;}.selectize-dropdown-content{background-color:#fff;}</style>
                    <?= $this->render('/shared/_menu') ?>
                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>
</div>