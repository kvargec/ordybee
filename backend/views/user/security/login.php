<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Da\User\Widget\ConnectWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View            $this
 * @var \Da\User\Form\LoginForm $model
 * @var \Da\User\Module         $module
 */

$this->title = Yii::t('usuario', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
$this->context->layout='@backend/views/layouts/main-login';
?>

<?= $this->render('/shared/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<style>
.form-control {
    box-shadow: none;
    padding-left: 10px;
}
.back{min-height:100vh;margin-top: 0;}
.back > div{min-height:calc(100vh - 50px)!important}

@media (max-width:786px){
    .logo{max-width:100vw;height:auto}
    .back{margin-top:0}
    .card-body > *,.field-loginform-rememberme{width:80%;margin: auto;}
    .field-loginform-rememberme{margin-left: 10%!important;}
}
</style>
<div class="col-lg-4 col-lg-offset-4">
<div class="card">
    <div class="card-body login-card-body">
        <h3 class="login-box-msg">Prijava u aplikaciju</h3>

        <?php $form = ActiveForm::begin(['id' => 'login-form']) ?>

        <?= $form->field($model,'login', [
            'options' => ['class' => 'form-group has-feedback'],
          //  'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-envelope"></span></div></div>',
            //'template' => '{beginWrapper}{input}{error}{endWrapper}',
           // 'wrapperOptions' => ['class' => 'input-group mb-3']
        ])
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('login')]) ?>

        <?= $form->field($model, 'password', [
            'options' => ['class' => 'form-group has-feedback'],
           // 'inputTemplate' => '{input}<div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>',
        //    'template' => '{beginWrapper}{input}{error}{endWrapper}',
          //  'wrapperOptions' => ['class' => 'input-group mb-3']
        ])
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-8">
                <?= $form->field($model, 'rememberMe')->checkbox([
                    'template' => '<div class="icheck-primary">{input}{label}</div>',
                    'labelOptions' => [
                        'class' => ''
                    ],
                    'uncheck' => null
                ]) ?>
            </div>
            <div class="col-4">
                <?= Html::submitButton('Prijava', ['class' => 'btn btn-primary btn-block']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <!--     <div class="social-auth-links text-center mb-3">
                 <p>- OR -</p>
                 <a href="#" class="btn btn-block btn-primary">
                     <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
                 </a>
                 <a href="#" class="btn btn-block btn-danger">
                     <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
                 </a>
             </div>
              /.social-auth-links -->

        <p class="mb-1">
            <?php
            echo Yii::t('usuario', 'Password')
            . ($module->allowPasswordRecovery ?
                ' (' . Html::a(
                    Yii::t('usuario', 'Forgot password?'),
                    ['/user/recovery/request'],
                    ['tabindex' => '5']
                )
                . ')' : '')
            ?>
        </p>

    </div>
    <?= ConnectWidget::widget(
        [
            'baseAuthUrl' => ['/user/security/auth'],
        ]
    ) ?>
    <!-- /.login-card-body -->
</div>
</div>
