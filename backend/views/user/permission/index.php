<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

/**
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $this         yii\web\View
 * @var $searchModel  \Da\User\Search\PermissionSearch
 */
use yii\grid\ActionColumn;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\html;

$this->title = Yii::t('usuario', 'Permissions');
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $this->beginContent('@Da/User/resources/views/shared/admin_layout.php') ?>
<div class="table-responsive">
<?= GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}",
        'columns' => [
            [
                'attribute' => 'name',
                'header' => Yii::t('usuario', 'Name'),
                'options' => [
                    'style' => 'width: 20%',
                ],
            ],
            [
                'attribute' => 'description',
                'header' => Yii::t('usuario', 'Description'),
                'options' => [
                    'style' => 'width: 55%',
                ],
            ],
            [
                'attribute' => 'rule_name',
                'header' => Yii::t('usuario', 'Rule name'),
                'options' => [
                    'style' => 'width: 15%',
                ],
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {delete}',
                'urlCreator' => function ($action, $model) {
                    return Url::to(['/user/permission/' . $action, 'name' => $model['name']]);
                },
                'options' => [
                    'style' => 'width: 10%',
                ],
                'buttons' =>[
                    'update' => function ($url, $model, $key) {
                        $url2 = \yii\helpers\Url::to(['/user/permission/update', 'name' => $model['name']]);
                        return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'edit')), [
                            'class' => 'material-icons'
                        ]), $url2);
                    },
                    'delete' => function ($url, $model, $key) {
                        $url2 = \yii\helpers\Url::to(['/user/permission/delete', 'name' => $model['name']]);
                        return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'delete')), [
                            'class' => 'material-icons red'
                        ]), $url2, [
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]);
                    },
                ]
            ],
        ],
    ]
) ?>
</div>
<?php $this->endContent() ?>
