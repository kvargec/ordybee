<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Mjed */

$this->title = Yii::t('app', 'Create Mjed');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mjeds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mjed-create">

<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>




        </div>
        <div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
