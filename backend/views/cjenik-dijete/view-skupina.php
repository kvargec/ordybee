<?php

use common\models\Roditelj;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Skupina */


$temp1 = explode("-", $model->pedGodina->od);
$temp2 = explode("-", $model->pedGodina->do);
$this->title = $model->naziv;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Skupinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="skupina-view">
	<div class="card">
		<div class="card-header card-header-danger">
			<span class="card-title"><?= Html::encode($this->title) ?></span>
		</div>

		<div class="card-body">
			<table class="table table-striped table-bordered">
				<?php $form = ActiveForm::begin(['action' => ['cjenik-dijete/view-skupina', 'id_skupina' => $model->id], 'method' => 'POST']); ?>
				<thead>
					<tr>
						<th><?= Yii::t('app', 'Dijete') ?></th>
						<th class="perc70"><?= Yii::t('app', 'Cijena') ?></th>

					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($cjenik as $index => $cdijete) {
						//print("<pre>".print_r($cdijete->naziv,true)."</pre>");die();		
					?>
						<tr>
							<td><?php echo $cdijete->naziv ?></td>
							<td class="perc70">
								<!-- ,'placeholder' =>Yii::$app->formatter->asCurrency($cdijete->cijena) -->
								<?php
								echo $form->field($cdijete, "[$index]cijena")->textInput(['value' => $cdijete->cijena]); ?>
							</td>
							<td>
								<?php
								echo $form->field($cdijete, "[$index]id_cjenik")->hiddenInput(['value' => $cdijete->id_cjenik])->label(false);
								echo $form->field($cdijete, "[$index]id")->hiddenInput(['value' => $cdijete->id_dijete])->label(false);
								?>
							</td>

						</tr>
					<?php } ?>
					<tr>
						<td colspan="2" style="text-align:right">

							<?= Html::submitButton(Yii::t('app', '<span class="material-icons">check</span>'), ['class' => 'btn btn-success']) ?>

						</td>
					</tr>
				</tbody>

				<?php ActiveForm::end(); ?>
			</table>

		</div>
	</div>
</div>