<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Dijete;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\CjenikDijete */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cjenik-dijete-form">

    <?php $form = ActiveForm::begin(['action' => ['cijenik-dijete/create-cjenik', 'skupina' => $skupina->id], 'method' => 'POST']); 
    foreach ($cjenici as $cjenik) {
        $dijete = Dijete::find()->where(['id' => $cjenik->dijete])->one();
    ?>
        <?= $form->field($cjenik, 'dijete')->hiddenInput(['value' => !empty($cjenik->dijete) ? $cjenik->dijete : ''], ['disabled' => true])->label(false) ?>

        <?= $form->field($cjenik, 'cijena')->textInput(['value' => !empty($cjenik->cijena) ? $cjenik->cijena : 0], ['type' => 'number'])->label($dijete->ime.' '.$dijete->prezime) ?>
    <?php } ?>
    
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
