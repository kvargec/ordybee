<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CjenikDijeteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cjenik Dijetes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cjenik-dijete-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <!--<? //= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) 
                ?>-->

        </div>
        <div class="card-body action-right">

            <?= GridView::widget([
                'dataProvider' => $dataProvider2,
                // 'filterModel' => $searchModel2,
                'columns' => [
                    'naziv',
                    // 'dat_poc',
                    // 'dat_kraj',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}{download}',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data) {
                                $url2 = \Yii::$app->urlManager->createUrl(['cjenik-dijete/view-skupina', 'id_skupina' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Vidi cjenik'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            },
                            'download' => function ($url, $data) {
                                $url2 = \Yii::$app->urlManager->createUrl(['cjenik/create-sve-uplatnice', 'id_skupina' => $data->id]);
                                return Html::a('<span class="material-icons">download</span>', $url2, [
                                    'title' => Yii::t('app', 'Napravi sve uplatnice'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            }
                        ],
                    ],
                ],
            ]); ?>

        </div>
    </div>
</div>