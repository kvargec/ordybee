<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CjenikDijete */

$this->title = Yii::t('app', 'Create Cjenik Dijete');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cjenik Dijetes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cjenik-dijete-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
