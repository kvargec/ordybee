<div class="docs">

    <div class="acc">
        <div class="card-header card-header-primary" id="kartoni">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <span>Objekti</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapseExample">
            <div class="card-body pb-20 instr-text">
                <!-- <div class="d-flex mt-10"><a class="bold p-10" href="#novi-objekt">Unos novog objekta</a> </div> -->
                <div id="novo-dijete" class="p-10">
                    <h2>Unos novog objekta</h2>

                </div>

                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/AnIrnZqxZiU" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            </div>
        </div>
    </div>
    <div class="acc">
        <div class="card-header card-header-primary" id="zaposlenik">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapsezaposlenik" aria-expanded="false" aria-controls="collapsezaposlenik">
                    <span>Zaposlenici</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapsezaposlenik">
            <div class="card-body pb-20 instr-text">
                <!-- <div class="d-flex mt-10"><a class="bold p-10" href="#novi-objekt">Unos novog zaposlenika</a> </div> -->
                <div id="novo-dijete" class="p-10">
                    <h2>Unos novog zaposlenika</h2>

                </div>

                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/bKKVcHiJruI" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            </div>
        </div>
    </div>

</div>