<div class="docs">

    <div class="acc">
        <div class="card-header card-header-primary" id="kartoni">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <span>Kartoni</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapseExample">
            <div class="card-body pb-20 instr-text">
                <!-- <div class="d-flex mt-10"><a class="bold p-10" href="#novi-zapis">Unos novog zapisa</a> </div> -->
                <div id="novi-zapis" class="p-10">
                    <h2>Unos novog zapisa</h2>

                </div>

                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/m6X50u4R80M" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            </div>
        </div>
    </div>
    <div class="acc">
        <div class="card-header card-header-primary" id="kartoni">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapsejelovnik" aria-expanded="false" aria-controls="collapseExample">
                    <span>Jelovnik</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapsejelovnik">
            <div class="card-body pb-20 instr-text">
                <!-- <div class="d-flex mt-10"><a class="bold p-10" href="#jelovnik">Unos jelovnika</a> </div> -->
                <div id="jelovnik" class="p-10">
                    <h2>Unos jelovnika</h2>

                </div>

                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/R08-lQuSNYY" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            </div>
        </div>
    </div>
    <div class="acc">
        <div class="card-header card-header-primary" id="datoteke">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapsedat" aria-expanded="false" aria-controls="collapsedat">
                    <span>Datoteke, slike, video</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapsedat">
            <div class="card-body pb-20 instr-text">
                <!-- <div class="d-flex mt-10"><a class="bold p-10" href="#datoteke">Unos datoteka</a> </div> -->
                <div id="datoteke" class="p-10">
                    <h2>Unos datoteka</h2>

                </div>

                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/qQwvVAv8REg" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            </div>
        </div>
    </div>
    <div class="acc">
        <div class="card-header card-header-primary" id="dnevnik-rada">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapserad" aria-expanded="false" aria-controls="collapserad">
                    <span>Dnevnik rada</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapserad">
            <div class="card-body pb-20 instr-text">
                <!-- <div class="d-flex mt-10"><a class="bold p-10" href="#datoteke">Unos datoteka</a> </div> -->
                <div id="datoteke" class="p-10">
                    <h2>Unos dnevnika rada</h2>

                </div>

                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/ErPSSONpKWg" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            </div>
        </div>
    </div>
    <div class="acc">
        <div class="card-header card-header-primary" id="zahtjevi">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapsezahtj" aria-expanded="false" aria-controls="collapsezahtj">
                    <span>Zahtjevi</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapsezahtj">
            <div class="card-body pb-20 instr-text">
                <!-- <div class="d-flex mt-10"><a class="bold p-10" href="#datoteke">Unos datoteka</a> </div> -->
                <div id="datoteke" class="p-10">
                    <h2>Obrada zahtjeva</h2>

                </div>

                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/Lx_qqzAi66A" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            </div>
        </div>
    </div>
</div>