<div class="docs djeca">
    <div class="acc">
        <div class="card-header card-header-primary" id="polaznici">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <span>Djeca</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapseExample">
            <div class="card-body pb-20 instr-text">
                <div class="d-flex mt-10"><a class="bold p-10" href="#novo-dijete">Unos novog djeteta</a> <a class="bold p-10" href="#izmjena-djeteta">Izmjena podataka djeteta</a>  <a class="bold p-10" href="#prisutnost-djeteta">Unos prisutnosti djece</a></div>
                <div id="novo-dijete" class="p-10">
                    <h2>Unos novog djeteta</h2>
                    <ul>
                    <li class="p-10">Ukoliko roditelj nije ispunio zahtjev, ispunite zahtjev na Roditeljskom portalu.</li>
                        <li class="p-10">Za unos novog djeteta pronađite navigaciju, kliknite na "Zahtjevi", zatim pronađite dijete koje želite upisati i kliknite na žuto oko</li>
                        <li class="p-10">Pregledajte potrebne podatke i na dnu stranice promijenite status u "Upisan"</li>
                        <li class="p-10">Kako bi spremili izmjene, na dnu stranice kliknite plavo dugme s kvačicom</li>
                        <li class="p-10">Zatim u navigaciji pod "Djeca", "Skupine" odaberite skupinu u koju želite dodati novo dijete te ga na vrhu stranice odaberite u padajućem izborniku</li>
                    </ul>
                </div>
                
                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/T86opXT3HgU" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <div id="izmjena-djeteta" class="p-10">
                    <h2>Izmjena podataka djeteta</h2>
                    <ul>
                        <li class="p-10">Za izmjenu podataka djeteta pronađite navigaciju, kliknite na "Djeca", zatim na "Popis djece u sustavu"</li>
                        <li class="p-10">Pronađite dijete koje želite urediti i kliknite kraj njega na ikonu plave olovke</li>
                        <li class="p-10">Kako bi spremili promjene, na dnu stranice kliknite plavo dugme s kvačicom</li>
                    </ul>
                </div>
                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/kCPxklaKX-Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <!-- <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/S7YJ60NLbxA" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                    <div id="prisutnost-djeteta" class="p-10">
                    <h2>Unos prisutnosti djece</h2>
                    <ul>
                        <li class="p-10">Za unos prisutnosti djece pronađite navigaciju, kliknite na "Djeca", zatim na "Skupine", a zatim na plavo dugme "+" pokraj odabrane skupine</li>
                        <li class="p-10">ili na Nadzornoj ploči pronađite žuti grafikon i kliknite na plavo dugme "+" ispod njega</li>
                        <li class="p-10">Klikom na X će se pojaviti ✓ koja će označiti da je dijete prisutno</li>
                        <li class="p-10">Klikom na ✓ će se pojaviti X koje će označiti da dijete nije prisutno</li>
                        <li class="p-10">Za unos razloga izostanka na vrhu stranice kliknite na plavo dugme "Unos razloga izostanka"</li>
                    </ul>
                </div>
                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/Lx6iJwocXVw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                
                </div>
        </div>
    </div>
    <div class="acc">
        <div class="card-header card-header-primary" id="skupina">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                    <span>Skupine</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapse2">
            <div class="card-body">
            <div class="d-flex mt-10"><a class="bold p-10" href="#dijete-u-skupinu">Dodavanje djeteta u skupinu</a> <a class="bold p-10" href="#izmjena-skupine">Izmjena skupine</a> </div>
                <div id="dijete-u-skupinu" class="p-10">
                    <h2>Dodavanje djeteta u skupinu</h2>
                    <ul>
                        <li class="p-10">Za unos novog djeteta pronađite navigaciju, kliknite na "Djeca", zatim na "Skupine". Pronađite skupinu u koju želite dodati dijete, te kraj nje kliknite na ikonu žutog oka</li>
                        <li class="p-10">Ispod žutog dijela u kojem je naziv skupine, kliknite na padajući izbornik u kojem odaberite dijete koje želite dodati</li>
                        <li class="p-10">Kako bi dodali odabrano dijete, kliknite na plavo dugme "Dodaj"</li>
                    </ul>
                </div>
                <!-- <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/S7YJ60NLbxA" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                <div id="izmjena-skupine" class="p-10">
                    <h2>Izmjena podataka djeteta</h2>
                    <ul>
                        <li class="p-10">Za unos novog djeteta pronađite navigaciju, kliknite na "Djeca", zatim na "Skupine"</li>
                        <li class="p-10">Pronađite skupinu koje želite urediti i kliknite kraj nje na ikonu plave olovke</li>
                        <li class="p-10">Kako bi spremili promjene, na dnu stranice kliknite plavo dugme s kvačicom</li>
                    </ul>
                </div>
                <!-- <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/S7YJ60NLbxA" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
            </div>
        </div>
    </div>
</div>