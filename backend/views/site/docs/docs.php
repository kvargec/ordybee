<div class="docs">

    <div class="acc">
        <div class="card-header card-header-primary">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <span>Ikone</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapseExample">
            <div class="card-body">
            <!-- <div class="d-flex mt-10"><a class="bold p-10" href="#ikone">Ikone</a> </div> -->
                <div id="Ikone" class="p-10">
                    <h2>Ikone</h2>
                    <ul>
                        <li class="p-10 f-24"><span class="material-icons yellow">visibility</span> Ikona oka - pregled podataka</li>
                        <li class="p-10 f-24"><span class="material-icons blue">edit</span> Ikona olovke - uređivanje podataka</li>
                        <li class="p-10 f-24"><span class="material-icons red">delete</span> <span>Ikona koša - brisanje podataka</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="acc">
        <div class="card-header card-header-primary" id="polaznici">
            <span class="card-title d-block">
                <a class="d-flex" data-toggle="collapse" href="#collapseporuka" aria-expanded="false" aria-controls="collapseporuka">
                    <span>Poruke</span>
                    <span class="material-icons">expand_more</span>
                </a>
            </span>
        </div>

        <div class="collapse" id="collapseporuka">
            <div class="card-body pb-20 instr-text">
                <!-- <div class="d-flex mt-10"><a class="bold p-10" href="#novo-dijete">Slanje poruka</a> </div> -->
                <div id="novo-dijete" class="p-10">
                    <h2>Slanje poruka</h2>
                   
                </div>
                
                <iframe class="mx-auto" width="80%" height="530" src="https://www.youtube.com/embed/yVdCzlLbW_0" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                
                
                </div>
        </div>
    </div>
   
</div>