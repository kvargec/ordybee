<?php

/* @var $this yii\web\View */

use kartik\bs4dropdown\Dropdown;
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Postavke;


/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZahtjevSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <?php $items = [];
    foreach ($skupine as $skupina) {
        $items[] = ['label' => $skupina->naziv, 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => 0])];
    } ?>



    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">more_time</i>
                </div>
                <div class="card-content">

                    <?php
                    $prisutnost = Postavke::find()->where(['postavka' => 'prisutnostType'])->one();
                    $prisutnostType = $prisutnost->vrijednost;
                    ?>

                    <h3>
                        <?= Html::a('Unesite prisutnost', ['/prisutnost/zaposlenici-' . $prisutnostType], ['class' => '']) ?>
                    </h3>

                    <div class="stats">
                        <?= Html::a('<span class="material-icons">add</span><span class="material-icons">commute</span>', ['prijevoz-zaposlenik/zaposlenici'], ['class' => 'btn btn-success']) ?>

                        <?= Html::a('<span class="material-icons">add</span><span class="material-icons">fastfood</span>', ['zaposlenik-obrok/zaposlenici'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">schedule</i>
                </div>
                <div class="card-content">
                    <h3>
                        <a href="<?= \yii\helpers\Url::to(['/dnevnik-zapis/creatednevnikrada']) ?>" class="">Dodajte dnevnik rada</a>
                    </h3>
                </div>
                <div class="stats">
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">chat</i>
                </div>
                <div class="card-content">
                    <h3>
                        <?= Html::a('Poruke', ['/mailbox/index'], ['class' => '']) ?>
                    </h3>
                </div>
                <div class="stats">
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card front nostats front">
                <div class="card-header " data-background-color="orange">

                    <div>
                        <h4 class="title"><?= Yii::t('app', 'Prisutne djece') ?> </h4>
                        <div class="donut-chart"></div>
                        <p class="text-center"><?= $prisutneDjece . ' / ' . $upisane ?></p>
                    </div>
                </div>
                <div class="card-content">

                    <div class="stats">
                        <?= \yii\bootstrap\ButtonDropdown::widget([
                            'encodeLabel' => false,
                            'label' => '<span class="material-icons">add</span>',
                            'dropdown' => [
                                'items' => $items
                            ]
                        ]) ?>
                    </div>


                    <p class="category"><span class="text-success"></span> </p>
                </div>

            </div>

            <div class="card card-stats">
                <div class="card-header " data-background-color="green">
                    <i class="material-icons">assignment</i>
                </div>
                <div class="card-content">
                    <h3>
                        <a href="<?php echo \yii\helpers\Url::to(['/dnevnik/index', 'tip' => 'logoped']) ?>"><?= Yii::t('app', 'Kartoni') ?></a>
                    </h3>
                </div>
                <div class="stats">
                </div>
            </div>


        </div>
        <div class="col-lg-4">
            <div class="card front nostats front">
                <div class="card-header" data-background-color="orange">
                    <div>
                        <h4 class="title"><?= Yii::t('app', 'Prisutne djece po skupini') ?></h4>

                    </div>
                </div>
                <div class="card-content" id="prisutne">
                    <?php

                    $i = 1;

                    foreach ($skupine as $skup) {
                        if (empty($djecePoSkupini[$skup->id])) {
                            if ($djecePoSkupini[$skup->id] == 0) {
                                $posto = 1;
                            } else {
                                $posto = 1;
                            }
                        } else {
                            $posto = ($prisutneDjecePoSkupini[$skup->id] / $djecePoSkupini[$skup->id]) * 100;
                        } ?>
                        <div id="skupina<?= $i; ?>" class="<?php if ($i > 5) { ?>hidden<?php } ?>">
                            <p class="d-flex justify-content-space align-center mb-0 mt-8"><span><?= $skup->naziv; ?></span><a href="<?= \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skup->id, 'tjedan' => 0]); ?>" class="fs-14"><?= $prisutneDjecePoSkupini[$skup->id] ?> / <?= $djecePoSkupini[$skup->id] ?>&nbsp;&nbsp;&nbsp;<span class="material-icons mb-0">add</span></a></p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width:0%;" aria-valuenow="<?= $posto ?>" aria-valuemin="0" aria-valuemax="<?= $djecePoSkupini[$skup->id] ?>">&nbsp;&nbsp;<?= $prisutneDjecePoSkupini[$skup->id] ?></div>
                            </div>
                        </div>
                    <?php
                        $i++;
                    } ?>
                    <?php if (count($skupine) > 5) { ?>
                        <a id="more" class="btn-group mb-0" onclick="showmore()">Pokaži više</a>
                    <?php } ?>

                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-8 col-sm-12 ">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">today</i>
                </div>
                <div class="card-content">
                    <h3><?= Yii::t('app', 'Kalendar') ?></h3>
                </div>
                <div class="stats px-1">
                    <?php
                    echo \backend\widgets\calendar\CalendarWidget::widget(['model' => $events]);
                    ?>
                </div>
            </div>
        </div>





    </div>


</div>





</div>
<!--  Charts Plugin -->
<!-- <script src="../js/chartist.min.js"></script> -->
<?php

$js = <<< JS
$(".posalji-potvrdu").on("submit", function(e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var data = $(this).serializeArray();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data
    }).done(function(data, textStatus, jqXHR) {
        $.notify(data.message, "success");
    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert( errorThrown );
    });
});
JS;
$this->registerJs($js);
$brojDjece = json_encode($brojDjece);
$js = <<< JS
'<?php echo $prisutneDjece?>'
$(document).ready(function(){
    var obj = $brojDjece;
    
         dataDailySalesChart = {
            labels: ['PON', 'UTO', 'SRI', 'ČET', 'PET',],
            series: [
                [ obj.pon, obj.uto, obj.sri, obj.cet, obj.pet]
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: $upisane+30, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var dailySalesChart = new Chartist.Line('#completedTasksChart', dataDailySalesChart, optionsDailySalesChart);

        md.startAnimationForLineChart(dailySalesChart);

  
});
JS;
$this->registerJs($js);
$brojDjeceProsli = json_encode($brojDjeceProsli);
$js = <<< JS
$(document).ready(function(){
    var obj = $brojDjeceProsli;
    
         dataDailySalesChart = {
            labels: ['PON', 'UTO', 'SRI', 'ČET', 'PET',],
            series: [
                [ obj.pon, obj.uto, obj.sri, obj.cet, obj.pet]
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: $upisane+30, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var dailySalesChart2 = new Chartist.Line('#prosli-tjedan', dataDailySalesChart, optionsDailySalesChart);

        md.startAnimationForLineChart(dailySalesChart2);

  
});
JS;
$this->registerJs($js);

$brojDjecePretprosli = json_encode($brojDjecePretprosli);
$js = <<< JS
$(document).ready(function(){
    var obj = $brojDjecePretprosli;
    
         dataDailySalesChart = {
            labels: ['PON', 'UTO', 'SRI', 'ČET', 'PET',],
            series: [
                [ obj.pon, obj.uto, obj.sri, obj.cet, obj.pet]
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: $upisane+30, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var dailySalesChart3 = new Chartist.Line('#pretprosli-tjedan', dataDailySalesChart, optionsDailySalesChart);

        md.startAnimationForLineChart(dailySalesChart3);
// ----------------------------------------------------------------------------------
// --------------------------------DONUT---------------------------------------------
// ----------------------------------------------------------------------------------
        
var chart = new Chartist.Pie('.donut-chart', {
                series: [$prisutneDjece,$upisane-$prisutneDjece]
            }, {
            donut: true,
            donutWidth: 30,
            showLabel: true,
  startAngle: 270

            });
            var prisutnostZaposlenika = new Chartist.Pie('.donut-chart-zaposlenici', {
                series: [ $prisutniZaposleni,$zaposlenih-$prisutniZaposleni]
            }, {
            donut: true,
            donutWidth: 30,
            showLabel: true,
  startAngle: 270

            });
            var obradenihZahtjeva = new Chartist.Pie('.donut-chart-zahtjevi', {
                series: [$obradeni, $brojZahtjeva-$obradeni]
            }, {
            donut: true,
            donutWidth: 30,
            showLabel: true,
            startAngle: 270

            });

// ----------------------------------------------------------------------------------
// -----------------------------DONUT anima------------------------------------------
// ----------------------------------------------------------------------------------
            chart.on('draw', function(data) {
  if(data.type === 'slice') {
    // Get the total path length in order to use for dash array animation
    var pathLength = data.element._node.getTotalLength();

    // Set a dasharray that matches the path length as prerequisite to animate dashoffset
    data.element.attr({
      'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
    });

    // Create animation definition while also assigning an ID to the animation for later sync usage
    var animationDefinition = {
      'stroke-dashoffset': {
        id: 'anim' + data.index,
        dur: 1000,
        from: -pathLength + 'px',
        to:  '0px',
        easing: Chartist.Svg.Easing.easeOutQuint,
        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
        fill: 'freeze'
      }
    };

    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
    if(data.index !== 0) {
      animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
    }

    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
    data.element.attr({
      'stroke-dashoffset': -pathLength + 'px'
    });

    // We can't use guided mode as the animations need to rely on setting begin manually
    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
    data.element.animate(animationDefinition, false);
  }
});

// For the sake of the example we update the chart every time it's created with a delay of 8 seconds
// --------------------------------------------------------------------------------------------------
prisutnostZaposlenika.on('draw', function(data) {
  if(data.type === 'slice') {
    // Get the total path length in order to use for dash array animation
    var pathLength = data.element._node.getTotalLength();

    // Set a dasharray that matches the path length as prerequisite to animate dashoffset
    data.element.attr({
      'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
    });

    // Create animation definition while also assigning an ID to the animation for later sync usage
    var animationDefinition = {
      'stroke-dashoffset': {
        id: 'anim' + data.index,
        dur: 500,
        from: -pathLength + 'px',
        to:  '0px',
        easing: Chartist.Svg.Easing.easeOutQuint,
        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
        fill: 'freeze'
      }
    };

    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
    if(data.index !== 0) {
      animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
    }

    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
    data.element.attr({
      'stroke-dashoffset': -pathLength + 'px'
    });

    // We can't use guided mode as the animations need to rely on setting begin manually
    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
    data.element.animate(animationDefinition, false);
  }
});
// --------------------------------------------------------------------------------------------------
obradenihZahtjeva.on('draw', function(data) {
  if(data.type === 'slice') {
    // Get the total path length in order to use for dash array animation
    var pathLength = data.element._node.getTotalLength();

    // Set a dasharray that matches the path length as prerequisite to animate dashoffset
    data.element.attr({
      'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
    });

    // Create animation definition while also assigning an ID to the animation for later sync usage
    var animationDefinition = {
      'stroke-dashoffset': {
        id: 'anim' + data.index,
        dur: 1000,
        from: -pathLength + 'px',
        to:  '0px',
        easing: Chartist.Svg.Easing.easeOutQuint,
        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
        fill: 'freeze'
      }
    };

    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
    if(data.index !== 0) {
      animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
    }

    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
    data.element.attr({
      'stroke-dashoffset': -pathLength + 'px'
    });

    // We can't use guided mode as the animations need to rely on setting begin manually
    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
    data.element.animate(animationDefinition, false);
  }
});

     
// ----------------------------------------------------------------------------------
// -------------------------progressbar animation------------------------------------
// ----------------------------------------------------------------------------------
$(function () { 
  $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
});  

// $( window ).scroll(function() {   
 // if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
  $(".progress-bar").each(function(){
    each_bar_width = $(this).attr('aria-valuenow');
    $(this).width(each_bar_width + '%');
  });
       
 //  }  
// });

  
});
JS;
$this->registerJs($js);
?>