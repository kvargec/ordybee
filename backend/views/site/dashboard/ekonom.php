<?php

/* @var $this yii\web\View */

use kartik\bs4dropdown\Dropdown;
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Postavke;


/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZahtjevSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->name;
?>
<div class="site-index">
    <div class="row">
        <div class="col-lg-3 col-md-12 col-sm-12">
            <div class="card card-stats">
                <div class="card-header " data-background-color="green">

                    <i class="material-icons">assignment</i>
                </div>
                <div class="card-content">


                    <p class="title text-left"><a href="<?php echo \yii\helpers\Url::to(['zahtjev/index']) ?>"><?= Yii::t('app', 'Zahtjevi') ?></a></p>
                    <p class="category"><?= $obradeni . ' / ' . $brojZahtjeva ?></p>
                </div>
                <div class="card-footer">
                    <div class="barWrapper">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="<?= $brojZahtjeva > 0 ? $obradeni / $brojZahtjeva * 100 : 0 ?>" aria-valuemin="0" aria-valuemax="100">
                                <span class="popOver" data-toggle="tooltip" data-placement="bottom" title="<?= $brojZahtjeva > 0 ? $obradeni . ' / ' . $brojZahtjeva : 0 ?>"> </span>
                            </div>

                        </div>
                    </div>
                    <!-- <div class="stats">
                            <i class="material-icons text-danger"></i> <a href="<?php echo \yii\helpers\Url::to(['zahtjevi/index']) ?>"><?= Yii::t('app', 'Zahtjevi') ?></a>
                        </div> -->
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">schedule</i>
                </div>
                <div class="card-content">

                    <h3><a href="<?php echo \yii\helpers\Url::to(['dnevnik-zapis/dnevnik-rada-index-skupina']) ?>"><?= Yii::t('app', 'Dnevnici rada') ?></a></h3>
                </div>
                <div class="stats">
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">people</i>
                </div>
                <div class="card-content">
                    <p class="category"><?= Yii::t('app', 'Zaposlenih') ?></p>
                    <h3 class="title"><?php echo $zaposlenih ?></h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-danger"></i> <a href="<?php echo \yii\helpers\Url::to(['zaposlenje/index']) ?>"><?= Yii::t('app', 'Zaposlenici') ?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">notes</i>
                </div>
                <div class="card-content">
                    <p class="category"><?= Yii::t('app', 'Zapisi') ?></p>
                    <h3 class="title">10</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-danger"></i> <a href="<?php echo \yii\helpers\Url::to(['dnevnik-zapis/index']) ?>"><?= Yii::t('app', 'Zapisi') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $items = [];
    foreach ($skupine as $skupina) {
        $items[] = ['label' => $skupina->naziv, 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => 0])];
    } ?>



    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card front nostats front">
                <div class="card-header " data-background-color="orange">

                    <div>
                        <h4 class="title"><?= Yii::t('app', 'Prisutne djece') ?> </h4>
                        <div class="donut-chart"></div>
                        <p class="text-center"><?= $prisutneDjece . ' / ' . $upisane ?></p>
                    </div>
                </div>
                <div class="card-content">

                    <div class="stats">
                        <?= \yii\bootstrap\ButtonDropdown::widget([
                            'encodeLabel' => false,
                            'label' => '<span class="material-icons">add</span>',
                            'dropdown' => [
                                'items' => $items
                            ]
                        ]) ?>
                    </div>


                    <p class="category"><span class="text-success"></span> </p>
                </div>

            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card nostats">
                <div class="card-header " data-background-color="blue">
                    <div>
                        <h4 class="title"><?= Yii::t('app', 'Prisutni zaposlenici:') ?> </h4>
                        <div class="donut-chart-zaposlenici"></div>
                        <p class="text-center"><?= $prisutniZaposleni . ' / ' . $zaposlenih ?></p>
                    </div>
                </div>
                <div class="card-content">
                    <h4 class="title"></h4>
                    <p class="category"></p>
                    <div class="stats">
                        <?php
                        $prisutnost = Postavke::find()->where(['postavka' => 'prisutnostType'])->one();
                        $prisutnostType = $prisutnost->vrijednost;
                        ?>
                        <?= Html::a('<span class="material-icons">add</span>', ['/prisutnost/zaposlenici-' . $prisutnostType], ['class' => 'btn btn-success']) ?>
                        <?= Html::a('<span class="material-icons">add</span><span class="material-icons">commute</span>', ['prijevoz-zaposlenik/zaposlenici'], ['class' => 'btn btn-success']) ?>

                        <?= Html::a('<span class="material-icons">add</span><span class="material-icons">fastfood</span>', ['zaposlenik-obrok/zaposlenici'], ['class' => 'btn btn-success']) ?>
                    </div>
                </div>


            </div>
        </div>
        <!-- <div class="col-md-4">
            <div class="card no-stats">
                <div class="card-header" data-background-color="green">
                    <div>
                        <h4 class="title"><?= Yii::t('app', 'Obrađenih zahtjeva') ?></h4>
                        <div class="donut-chart-zahtjevi"></div>
                        <p class="text-center"><?= $obradeni . ' / ' . $brojZahtjeva ?></p>
                    </div>
                    <h4 class="category"></h4>
                    <div class="donut-chart-zahtjevi"></div>
                </div>
                <div class="card-content">

                    <i class="material-icons text-danger"></i> <a href="<?php echo \yii\helpers\Url::to(['zahtjev/index']) ?>"><?= Yii::t('app', 'Obrada zahtjeva') ?></a>
                </div>

            </div>
        </div> -->

    </div>
    <div class="row">

        <div class="col-lg-4 col-md-12">
            <div class="card nostats">
                <div class="card-header card-chart" data-background-color="green">
                    <div class="ct-chart" id="pretprosli-tjedan"></div>
                </div>

                <div class="card-footer no-border">
                    <p></p>
                    <h4 class="title"><?= Yii::t('app', 'Prisutnost djece pretprošli tjedan') ?></h4>
                    <p></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">

            <div class="card nostats">
                <div class="card-header card-chart " data-background-color="green">
                    <div class="ct-chart" id="prosli-tjedan"></div>
                </div>

                <div class="card-footer no-border">
                    <p></p>
                    <h4 class="title"><?= Yii::t('app', 'Prisutnost djece prošli tjedan') ?></h4>
                    <p></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">


            <div class="card nostats">
                <div class="card-header card-chart blue" data-background-color="blue">
                    <div class="ct-chart" id="completedTasksChart"></div>
                </div>

                <div class="card-footer no-border">
                    <p></p>
                    <h4 class="title"><?= Yii::t('app', 'Prisutnost djece ovaj tjedan') ?></h4>
                    <p></p>
                </div>
            </div>
        </div>


        <!-- <div class="item">
                                    <div class="ct-chart" id="prosli-tjedan"></div>

                                </div>

                                <div class="item">
                                   <div class="ct-chart" id="pretprosli-tjedan"></div>
                                </div> -->


    </div>

</div>





</div>
<!--  Charts Plugin -->
<!-- <script src="../js/chartist.min.js"></script> -->
<?php

$js = <<< JS
$(".posalji-potvrdu").on("submit", function(e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var data = $(this).serializeArray();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data
    }).done(function(data, textStatus, jqXHR) {
        $.notify(data.message, "success");
    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert( errorThrown );
    });
});
JS;
$this->registerJs($js);
$brojDjece = json_encode($brojDjece);
$js = <<< JS
'<?php echo $prisutneDjece?>'
$(document).ready(function(){
    var obj = $brojDjece;
    
         dataDailySalesChart = {
            labels: ['PON', 'UTO', 'SRI', 'ČET', 'PET',],
            series: [
                [ obj.pon, obj.uto, obj.sri, obj.cet, obj.pet]
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: $upisane+30, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var dailySalesChart = new Chartist.Line('#completedTasksChart', dataDailySalesChart, optionsDailySalesChart);

        md.startAnimationForLineChart(dailySalesChart);

  
});
JS;
$this->registerJs($js);
$brojDjeceProsli = json_encode($brojDjeceProsli);
$js = <<< JS
$(document).ready(function(){
    var obj = $brojDjeceProsli;
    
         dataDailySalesChart = {
            labels: ['PON', 'UTO', 'SRI', 'ČET', 'PET',],
            series: [
                [ obj.pon, obj.uto, obj.sri, obj.cet, obj.pet]
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: $upisane+30, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var dailySalesChart2 = new Chartist.Line('#prosli-tjedan', dataDailySalesChart, optionsDailySalesChart);

        md.startAnimationForLineChart(dailySalesChart2);

  
});
JS;
$this->registerJs($js);

$brojDjecePretprosli = json_encode($brojDjecePretprosli);
$js = <<< JS
$(document).ready(function(){
    var obj = $brojDjecePretprosli;
    
         dataDailySalesChart = {
            labels: ['PON', 'UTO', 'SRI', 'ČET', 'PET',],
            series: [
                [ obj.pon, obj.uto, obj.sri, obj.cet, obj.pet]
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: $upisane+30, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var dailySalesChart3 = new Chartist.Line('#pretprosli-tjedan', dataDailySalesChart, optionsDailySalesChart);

        md.startAnimationForLineChart(dailySalesChart3);
// ----------------------------------------------------------------------------------
// --------------------------------DONUT---------------------------------------------
// ----------------------------------------------------------------------------------
        
var chart = new Chartist.Pie('.donut-chart', {
                series: [$prisutneDjece,$upisane-$prisutneDjece]
            }, {
            donut: true,
            donutWidth: 30,
            showLabel: true,
  startAngle: 270

            });
            var prisutnostZaposlenika = new Chartist.Pie('.donut-chart-zaposlenici', {
                series: [ $prisutniZaposleni,$zaposlenih-$prisutniZaposleni]
            }, {
            donut: true,
            donutWidth: 30,
            showLabel: true,
  startAngle: 270

            });
            var obradenihZahtjeva = new Chartist.Pie('.donut-chart-zahtjevi', {
                series: [$obradeni, $brojZahtjeva-$obradeni]
            }, {
            donut: true,
            donutWidth: 30,
            showLabel: true,
            startAngle: 270

            });

// ----------------------------------------------------------------------------------
// -----------------------------DONUT anima------------------------------------------
// ----------------------------------------------------------------------------------
            chart.on('draw', function(data) {
  if(data.type === 'slice') {
    // Get the total path length in order to use for dash array animation
    var pathLength = data.element._node.getTotalLength();

    // Set a dasharray that matches the path length as prerequisite to animate dashoffset
    data.element.attr({
      'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
    });

    // Create animation definition while also assigning an ID to the animation for later sync usage
    var animationDefinition = {
      'stroke-dashoffset': {
        id: 'anim' + data.index,
        dur: 1000,
        from: -pathLength + 'px',
        to:  '0px',
        easing: Chartist.Svg.Easing.easeOutQuint,
        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
        fill: 'freeze'
      }
    };

    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
    if(data.index !== 0) {
      animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
    }

    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
    data.element.attr({
      'stroke-dashoffset': -pathLength + 'px'
    });

    // We can't use guided mode as the animations need to rely on setting begin manually
    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
    data.element.animate(animationDefinition, false);
  }
});

// For the sake of the example we update the chart every time it's created with a delay of 8 seconds
// --------------------------------------------------------------------------------------------------
prisutnostZaposlenika.on('draw', function(data) {
  if(data.type === 'slice') {
    // Get the total path length in order to use for dash array animation
    var pathLength = data.element._node.getTotalLength();

    // Set a dasharray that matches the path length as prerequisite to animate dashoffset
    data.element.attr({
      'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
    });

    // Create animation definition while also assigning an ID to the animation for later sync usage
    var animationDefinition = {
      'stroke-dashoffset': {
        id: 'anim' + data.index,
        dur: 500,
        from: -pathLength + 'px',
        to:  '0px',
        easing: Chartist.Svg.Easing.easeOutQuint,
        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
        fill: 'freeze'
      }
    };

    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
    if(data.index !== 0) {
      animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
    }

    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
    data.element.attr({
      'stroke-dashoffset': -pathLength + 'px'
    });

    // We can't use guided mode as the animations need to rely on setting begin manually
    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
    data.element.animate(animationDefinition, false);
  }
});
// --------------------------------------------------------------------------------------------------
obradenihZahtjeva.on('draw', function(data) {
  if(data.type === 'slice') {
    // Get the total path length in order to use for dash array animation
    var pathLength = data.element._node.getTotalLength();

    // Set a dasharray that matches the path length as prerequisite to animate dashoffset
    data.element.attr({
      'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
    });

    // Create animation definition while also assigning an ID to the animation for later sync usage
    var animationDefinition = {
      'stroke-dashoffset': {
        id: 'anim' + data.index,
        dur: 1000,
        from: -pathLength + 'px',
        to:  '0px',
        easing: Chartist.Svg.Easing.easeOutQuint,
        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
        fill: 'freeze'
      }
    };

    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
    if(data.index !== 0) {
      animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
    }

    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
    data.element.attr({
      'stroke-dashoffset': -pathLength + 'px'
    });

    // We can't use guided mode as the animations need to rely on setting begin manually
    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
    data.element.animate(animationDefinition, false);
  }
});

     
// ----------------------------------------------------------------------------------
// -------------------------progressbar animation------------------------------------
// ----------------------------------------------------------------------------------
$(function () { 
  $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
});  

// $( window ).scroll(function() {   
 // if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
  $(".progress-bar").each(function(){
    each_bar_width = $(this).attr('aria-valuenow');
    $(this).width(each_bar_width + '%');
  });
       
 //  }  
// });

  
});
JS;
$this->registerJs($js);
?>