<?php

/**
 * @var $model \common\models\Mailbox
 */
?>

<div class="msg-content">
    <p><?= Yii::t('app', 'Sadržaj poruke:') ?></p>

    <div class="blue"><?= $model->message0->message ?></div>
    <!-- <?= $model->message0->id ?> -->
    <?php
    if (!empty($model->message0->attachments['attachments'])) {
        echo '<p>' . Yii::t('app', 'Prilozi') . '</p>';
        foreach ($model->message0->attachments as $attachments) {
            $links = '';
            if (!empty($attachments)) {
                $i = 0;
                foreach ($attachments as $attachment) {
                    $array = explode(',', $attachment);
                    $fileName = $array[array_key_last($array)];
                    if (($pos = strpos($attachment, "poruke/")) !== FALSE) { 
                        $attachment = explode('poruke/', $attachment, 2)[1];
                        $attachment = explode('/', $attachment, 2)[1];
                    }
                    $link = Yii::getAlias('@web') . '/poruke/' . $model->message0->id . '/' . $attachment; //explode('backend/web/', $attachment)[1];
                    if (($pos = strpos($fileName, "poruke/")) !== FALSE) { 
                        $fileName = explode('poruke/', $fileName, 2)[1];
                        $fileName = explode('/', $fileName, 2)[1];
                    }
                    if (substr($fileName, -1) === 'g') {
                        
                        $links .= "<a class='desktop' href=#exampleModal" . $model->message0->id . $i . "' data-toggle='modal' data-target='#exampleModal" . $model->message0->id . $i . "'>" . $fileName . "</a>";
                        $links .= "<a class='mobile' href='$link' target='_blank'>" . $fileName . "</a>";
                    } else {
                        $links .= "<a href='$link' target='_blank'>" . $fileName . "</a>";
                    }
    ?>
                    <div class="modal fade" id="exampleModal<?= $model->message0->id . $i ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">

                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo $link; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
            <?php
                    $i++;
                }
            }
            echo $links;
            // echo $fileName;
            ?>


    <?php }
    }

    ?>


</div>