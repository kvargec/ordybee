<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PedagoskaGodina */

$this->title = substr($model->od,0,11).'/'.substr($model->do,0,11);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedagoska Godinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pedagoska-godina-view">

<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="card-body detail">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'od',
            'do',
            'aktivna:boolean',
            [
                'attribute' => 'postavke',
                'format' => 'html',
                'value' => function ($data){
                    $chars=[',','"','}','{','[',']'];
                    $quote=['"',':'];
                
                    $str=str_replace($quote,"\r",json_encode($data->postavke,true));
                    $str=str_replace($chars,"\n",json_encode($data->postavke,true));
                    $str = preg_replace('/\n(\s*\n)+/', '</p><p>', $str);
                    $str = preg_replace('/\n/', '', $str);
                    $str = preg_replace('/\n/', '', $str);
                    $str = str_replace('\u0161','š',$str);
                    $str = str_replace('\u0111','đ',$str);
                    $str = str_replace('\u017e','ž',$str);
                    $str = str_replace('\u010d','č',$str);
                    $str = str_replace('\u0107','ć',$str);
                    $str = '<p>'.$str.'</p>';
                    // $str= json_encode($data->postavke,true);
                    
                    
                    // return json_encode($data->postavke);
                    return $str;
                 
                }
        ],
        ],
    ]) ?>

</div></div></div>
