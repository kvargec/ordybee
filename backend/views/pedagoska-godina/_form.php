<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\PedagoskaGodina */
/* @var $form yii\widgets\ActiveForm */
//print("<pre>".print_r($model->postavke['obavezni_dokumenti'],true)."</pre>");die();
$data = function($model) {
    foreach ($model->postavke as $postavka) {
        $key = key($model->postavke);
        foreach ($postavka as $item) {
            $items[] = $item;
        }
    }
    return $key.":\xA".implode("\xA",$items);
};
if (is_array($model->postavke)) {
    $print = $data($model);
} else {
    $print = $model->postavke;
}

?>

<div class="pedagoska-godina-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="input-field">
		<?= $form->field($model, 'od')->widget(DatePicker::className(), [
			'convertFormat' => true,
			'options' => ['placeholder' => Yii::t('app','Početak godine')],
			'pluginOptions' => [
				'todayHighlight' => true,
				'autoclose'=>true,
			]
		]);
		?>
	</div>
	<div class="input-field">
		<?= $form->field($model, 'do')->widget(DatePicker::className(), [
			'convertFormat' => true,
			'options' => ['placeholder' => Yii::t('app','Kraj godine')],
			'pluginOptions' => [
				'todayHighlight' => true,
				'autoclose'=>true,
			]
		]);
		?>
	</div>
    <?= $form->field($model, 'aktivna')->checkbox() ?>

    <!--<?//= $form->field($model, 'postavke')->textarea([
      //  'rows' => '6',
      //  'value' => $print//json_encode($model->postavke)
    	//]) ?>-->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
