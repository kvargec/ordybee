<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PedagoskaGodina */

$this->title = Yii::t('app', 'Create Pedagoska Godina');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedagoska Godinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedagoska-godina-create">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        
    </div>
    <div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
        //'postavke'=>$postavke
    ]) ?>

</div></div></div>
