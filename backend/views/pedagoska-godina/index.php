<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\PedagoskaGodinaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pedagoške Godine');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedagoska-godina-index">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
</div>
<div class="card-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'od',
            [
                'value' => 'od',
                'label' => Yii::t('app', 'Početak'),
                'attribute' => 'od',
                'format' => ['date', 'php:d.m.Y'],
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Početak'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value' => 'do',
                'label' => Yii::t('app', 'Kraj'),
                'attribute' => 'do',
                'format' => ['date', 'php:d.m.Y'],
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Kraj'),
                    'class' => 'form-control'
                ]
            ],
            // 'do',
            // 'aktivna:boolean',
            [
                'value' => function ($data)
                {
                    return $data->aktivna ? 'Da' : 'Ne';
                },
                'label' => Yii::t('app', 'Aktivna'),
                'attribute' => 'aktivna',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Aktivna'),
                    'class' => 'form-control'
                ]
            ],
            [
                'attribute' => 'postavke',
                'format' => 'html',
                
                'value' => function ($data){
                    $chars=[',','"','}','{','[',']'];
                    $quote=['',':'];
                
                    $str=str_replace($quote,"\r",json_encode($data->postavke,true));
                    $str=str_replace($chars,"\n",json_encode($data->postavke,true));
                    $str=Html::encode($str);
                    $str = preg_replace('/\n(\s*\n)+/', '</p><p>', $str);
                    $str = preg_replace('/\n/', '', $str);
                    $str = str_replace('\u0161','š',$str);
                    $str = str_replace('\u0111','đ',$str);
                    $str = str_replace('\u017e','ž',$str);
                    $str = str_replace('\u010d','č',$str);
                    $str = str_replace('\u0107','ć',$str);
                    $str = '<p>'.$str.'</p>';
                    // $str= json_encode($data->postavke,true);
                    
                    
                    // return json_encode($data->postavke);
                    return $str;
                },
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Postavke'),
                    'class' => 'form-control'
                ]
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}  ',
            // 'visible' => Yii::$app->user->can('admin'),
            'buttons' => [
                'view' => function ($url, $data)  {

                    $url2 = \Yii::$app->urlManager->createUrl(['pedagoska-godina/view', 'id' => $data->id]);
                    return Html::a('<span class="material-icons">visibility</span>', $url2, [
                        'title' => Yii::t('app', 'Pregledaj zapise'),
                        'class' => 'text-left  hgreen',
                    ]);
                },



            ],
        ],
        ],
    ]); ?>


</div></div></div>
