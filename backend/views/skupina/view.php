<?php

use common\models\Roditelj;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Skupina */


$temp1 = explode("-", $model->pedGodina->od);
$temp2 = explode("-", $model->pedGodina->do);
$this->title = $model->naziv . ' (' . $temp1[0] . './.' . $temp2[0] . ')-' . implode(', ', array_map(function ($x) {
    return $x->objekt0->naziv;
}, $model->skupinaObjekts));
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Skupinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="skupina-view">
    <div class="card">
        <div class="card-header card-header-danger d-flex flex-wrap">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <a href="<?= \Yii::$app->urlManager->createUrl(['dnevnik-zapis/pdf-print-dnevnik-rada-svi', 'id' => $model->id]) ?>" class="btn btn-success  px-1" title="preuzimanje svih zapisa za skupinu"><i class="material-icons">download</i></a>
        </div>



        <div class="card-body">
            <?php
            if ($model->slika != null) {
            ?>

                <img class="img-responsive" src="<?php echo $model->getImageUrl(); ?>" style="max-height:300px;object-fit:cover" />
            <?php
            }
            ?>

            <?php $form = ActiveForm::begin(['action' => Url::toRoute('dijete-skupina/create')]); ?>
            <div class="row">
                <div class="col-lg-8">
                    <?= $form->field($model2, 'dijete')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(
                            \common\models\Zahtjev::find()->where(['status' => 7])->orWhere(['status' => 4])->all(),
                            'dijete',
                            function ($v) {
                                if (isset($v->dijete0)) {
                                    return $v->dijete0->ime . ' ' . $v->dijete0->prezime;
                                }
                            }
                        ),
                        'options' => [
                            'placeholder' => 'Odaberite dijete...',
                            'required' => true
                        ],
                    ]) ?>

                    <?= $form->field($model2, 'skupina')->hiddenInput(['value' => $model->id])->label(false) ?>
                </div>
                <div class="col-lg-4">
                    <div class="form-group" style="margin-top:15px">
                        <?= Html::submitButton(Yii::t('app', 'Dodaj'), ['class' => 'btn btn-success']) ?>
                        <!--<?/*= Html::a(Yii::t('app', 'QR Kodovi'), ['qr-codes', 'id' => $model->id], [
                            'class' => 'btn btn-success',
                        ]) */ ?>-->
                    </div>
                </div>
            </div>






            <?php ActiveForm::end(); ?>
            <?php
            $gridColumns = [
                // ['class' => 'yii\grid\SerialColumn'],
                [
                    'value' => function ($dijete) {
                        return $dijete->dijete0->prezime . ' ' . $dijete->dijete0->ime;
                    },
                    'label' => Yii::t('app', 'Ime djeteta'),
                    'attribute' => 'ime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime djeteta'),
                        'title' => Yii::t('app', 'Ime djeteta'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'ime_djeteta'
                ],
                [
                    'value' => 'dijete0.dat_rod',
                    'label' => Yii::t('app', 'Datum rođenja'),
                    'attribute' => 'dat_rod',
                    'format' => ['date', 'php:d.m.Y'],
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Datum rođenja'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'dijete0.oib',
                    'label' => Yii::t('app', 'Oib'),
                    'attribute' => 'oib',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Oib'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'dijete0.dat_upisa',
                    'label' => Yii::t('app', 'Datum upisa'),
                    'attribute' => 'dat_upisa',
                    'format' => ['date', 'php:d.m.Y'],
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Datum upisa'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => function ($dijete) {
                        return $dijete->dijete0->adresa . ' ' . $dijete->dijete0->mjesto;
                    },
                    'label' => Yii::t('app', 'Adresa'),
                    'attribute' => 'adresa',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Adresa'),
                        'title' => Yii::t('app', 'Adresa'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'adresa'
                ],
                [
                    'label' => Yii::t('app', 'Majka'),
                    'value' => function ($dijete) {
                        $mama = $dijete->getRoditeljs()->where(['or', ['spol' => 'F'], ['spol' => 'Ž']])->one();
                        if (isset($mama)) {
                            return $mama->prezime . ' ' . $mama->ime;
                        } else {
                            return '';
                        }
                    },
                    'attribute' => 'i_majke',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime majke'),
                        'title' => Yii::t('app', 'Ime majke'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'ime_majke'
                ],
                [
                    'label' => Yii::t('app', 'Kontakt majke'),
                    'value' => function ($dijete) {
                        $mama = $dijete->getRoditeljs()->where(['or', ['spol' => 'F'], ['spol' => 'Ž']])->one();
                        if (isset($mama)) {
                            return $mama->mobitel;
                        } else {
                            return '';
                        }
                    },
                    'attribute' => 'kont_majke',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Kontakt majke'),
                        'title' => Yii::t('app', 'Kontakt majke'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'kontakt_majke'
                ],
                [
                    'label' => Yii::t('app', 'Zanimanje majke'),
                    'value' => function ($dijete) {
                        $mama = $dijete->getRoditeljs()->where(['or', ['spol' => 'F'], ['spol' => 'Ž']])->one();
                        if (isset($mama)) {
                            return $mama->zanimanje;
                        } else {
                            return '';
                        }
                    },
                    'attribute' => 'zan_majke',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Zanimanje majke'),
                        'title' => Yii::t('app', 'Zanimanje majke'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'zanimanje_majke'
                ],
                [
                    'label' => Yii::t('app', 'Otac'),
                    'value' => function ($dijete) {
                        $tata = $dijete->getRoditeljs()->where(['or', ['spol' => 'M']])->one();
                        if (isset($tata)) {
                            return $tata->prezime . ' ' . $tata->ime;
                        } else {
                            return '';
                        }
                    },
                    'attribute' => 'i_oca',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime oca'),
                        'title' => Yii::t('app', 'Ime oca'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'ime_oca'
                ],
                [
                    'label' => Yii::t('app', 'Kontakt oca'),
                    'value' => function ($dijete) {
                        $tata = $dijete->getRoditeljs()->where(['or', ['spol' => 'M']])->one();
                        if (isset($tata)) {
                            return $tata->mobitel;
                        } else {
                            return '';
                        }
                    },
                    'attribute' => 'kont_oca',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Kontakt oca'),
                        'title' => Yii::t('app', 'Kontakt oca'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'kontakt_oca'
                ],
                [
                    'label' => Yii::t('app', 'Zanimanje oca'),
                    'value' => function ($dijete) {
                        $tata = $dijete->getRoditeljs()->where(['or', ['spol' => 'M']])->one();
                        if (isset($tata)) {
                            return $tata->zanimanje;
                        } else {
                            return '';
                        }
                    },
                    'attribute' => 'zan_oca',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Zanimanje oca'),
                        'title' => Yii::t('app', 'Zanimanje oca'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'zanimanje_oca'
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}{update}{roditelji}',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [


                        'delete' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['dijete-skupina/delete', 'id' => $data->id]);
                            return Html::a('<i class="material-icons">clear</i>', $url2, [
                                'title' => Yii::t('app', 'Izbriši'),
                                'class' => 'text-left  hblue',
                                'data-method' => 'POST'
                            ]);
                        },
                        'update' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['dijete/update', 'id' => $data->dijete, 'skupina' => $data->skupina]);
                            return Html::a('<i class="material-icons">create</i>', $url2, [
                                'title' => Yii::t('app', 'Dijete'),
                                'class' => 'text-left  hblue'
                            ]);
                        },
                        'roditelji' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['dijete/roditelji', 'id' => $data->dijete, 'skupina' => $data->skupina]);
                            return Html::a('<i class="material-icons">people</i>', $url2, [
                                'title' => Yii::t('app', 'Roditelji'),
                                'class' => 'text-left  hblue'
                            ]);
                        },

                    ],
                ],
            ];
            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                ],
                'filename' => 'popisDjece-' . $model->id,
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser,
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/skupina/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" . GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
            ]);
            ?>
        </div>
    </div>
</div>