<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Skupina */

$this->title = Yii::t('app', 'Create Skupina');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Skupinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="skupina-create">
    <div class="card">
        <div class="card-header card-header-danger">
            <span class="card-title"><?= Html::encode($this->title) ?></span>

        </div>

        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>