<?php

use common\models\DijeteSkupina;
use common\models\Roditelj;

echo \moonland\phpexcel\Excel::widget([
    'models' => DijeteSkupina::find()->where(['skupina'=>$id])->all(),
    'columns' => [
        [
            'header'=>Yii::t('app','Ime i prezime'),
            'value'=>function($dijete) {
                return $dijete->dijete0->ime.' '.$dijete->dijete0->prezime;
            }
        ],
        'dijete0.dat_rod:text:Datum rođenja',
        'dijete0.oib:text:OIB',
        [
            'header'=>Yii::t('app','Adresa'),
            'value'=>function($dijete) {
                return $dijete->dijete0->adresa.' '.$dijete->dijete0->mjesto;
            }
        ],
        [
            'header'=>Yii::t('app','Majka'),
            'value'=>function($dijete){
                $mama=Roditelj::find()->where(['dijete'=>$dijete->dijete,'spol'=>'Ž'])->one();
                return $mama->ime.' '.$mama->prezime;
            }
        ],
        [
            'header'=>Yii::t('app','Kontakt majke'),
            'value'=>function($dijete){
                $mama=Roditelj::find()->where(['dijete'=>$dijete->dijete,'spol'=>'Ž'])->one();
                return $mama->mobitel;
            }
        ],
        [
            'header'=>Yii::t('app','Otac'),
            'value'=>function($dijete){
                $mama=Roditelj::find()->where(['dijete'=>$dijete->dijete,'spol'=>'M'])->one();
                return $mama->ime.' '.$mama->prezime;
            }
        ],
        [
            'header'=>Yii::t('app','Kontakt oca'),
            'value'=>function($dijete){
                $mama=Roditelj::find()->where(['dijete'=>$dijete->dijete,'spol'=>'M'])->one();
                return $mama->mobitel;
            }
        ]
    ],
]);