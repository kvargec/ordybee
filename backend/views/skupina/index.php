<?php

use common\models\User;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use common\models\StatusDjeteta;
use common\models\Dijete;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\SkupinaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Skupinas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header card-header-danger">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
    </div>



    <div class="card-body">
        <?php // echo $this->render('_search', ['model' => $searchModel]); 
        $gridColumns = [
            // ['class' => 'yii\grid\SerialColumn'],

            //   'id',
            [
                'value' => 'naziv',
                'label' => Yii::t('app', 'Naziv'),
                'attribute' => 'naziv',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Naziv'),
                    'class' => 'form-control'
                ]
            ],
            [
                'label' => Yii::t('app', 'Broj djece'),
                'attribute' => 'broj_djece',
                'value' => function ($data) {
                    $status_upisan = StatusDjeteta::find()->where(['status' => 'upisan'])->one();
                    $djeca = Dijete::find()->leftJoin('dijete_skupina', 'dijete_skupina.dijete = dijete.id')->where(['dijete_skupina.skupina' => $data->id])->andWhere(['dijete.status' => $status_upisan->id])->orderBy(['prezime' => SORT_ASC])->all();
                    return count($djeca);
                },
                'filterOptions' => [
                    'class' => 'form-group',
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Broj djece'),
                    'title' => Yii::t('app', 'Broj djece'),
                    'class' => 'form-control',
                ],
                'filterAttribute' => 'broj_djece'
            ],
            [
                'value' => function ($data) {

                    $zaposlenici = [];
                    if (!empty($data->zaposleniks)) {
                        foreach ($data->zaposleniks as $item) {
                            $zaposlenici[] = $item->ime . " " . $item->prezime;
                        }
                        return implode(", ", $zaposlenici);
                    }
                },
                'label' => Yii::t('app', 'Zaposlenik'),
                'attribute' => 'zaposlenik_ime',
                'filterOptions' => [
                    'class' => 'form-group',
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Ime i prezime'),
                    'title' => Yii::t('app', 'Ime'),
                    'class' => 'form-control',
                ],
                'filterAttribute' => 'full_name_zaposlenik'
            ],
            // 'slika',
            [
                'value' => function ($data) {
                    $objekti = [];
                    foreach ($data->objekts as $item) {
                        $objekti[] = $item->naziv;
                    }
                    return implode(", ", $objekti);
                },
                'label' => Yii::t('app', 'Objekt'),
                'attribute' => 'objekt_naziv',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Naziv objekta'),
                    'class' => 'form-control'
                ]
            ],
            [
                'label' => Yii::t('app', 'Ped. godina'),
                'attribute' => 'ped_godina',
                'value' => function ($data) {
                    return $data->pedGodina->getNaziv();
                },
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filter' => yii\helpers\ArrayHelper::map(\common\models\PedagoskaGodina::find()->all(), 'id', function ($v) {
                    return $v->getNaziv();
                })
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {prisutnost} ',
                // 'visible' => Yii::$app->user->can('admin'),
                'buttons' => [
                    'view' => function ($url, $data) {

                        $url2 = \Yii::$app->urlManager->createUrl(['skupina/view', 'id' => $data->id]);
                        return Html::a('<span class="material-icons">visibility</span>', $url2, [
                            'title' => Yii::t('app', 'Vidi skupinu i djecu'),
                            'class' => 'text-left  hgreen',
                        ]);
                    },

                    'update' => function ($url, $data) {

                        $url2 = \Yii::$app->urlManager->createUrl(['skupina/update', 'id' => $data->id]);
                        return Html::a('<span class="material-icons">create</span>', $url2, [
                            'title' => Yii::t('app', 'Uredi skupinu'),
                            'class' => 'text-left  hblue',
                        ]);
                    },

                    'prisutnost' => function ($url, $data) {

                        $url2 = \Yii::$app->urlManager->createUrl(['prisutnost/djeca', 'id' => $data->id, 'tjedan' => 0]);
                        return Html::a('<button class="btn btn-info btn-sm"><span class="material-icons">add</span></button>', $url2, [
                            'title' => Yii::t('app', 'Unesi prisutnost djece'),
                            'class' => 'text-left  hgreen',
                        ]);
                    },
                    // 'cjenik'=>function ($url, $data) {
                    //     $url2 = \Yii::$app->urlManager->createUrl(['cjenik-dijete/create', 'id' => $data->id]);
                    //     $rola = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
                    //     $uloge = array();
                    //     foreach ($rola as $tko => $opis) {
                    //         $uloge[] = $tko;
                    //     }
                    //     if ($uloge[0]=='administracija'||$uloge[0]=='superadmin') {
                    //         return Html::a('<span class="material-icons">sell</span>', $url2, [
                    //             'title' => Yii::t('app', 'Unesi cjenik skupine'),
                    //             'class' => 'text-left  hblue',
                    //         ]);
                    //     }



                    // }



                ],
            ],
        ];
        ?>
        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'exportConfig' => [
                ExportMenu::FORMAT_TEXT => null,
                ExportMenu::FORMAT_HTML => null,
                ExportMenu::FORMAT_CSV => null,
            ],
            'filename' => 'popisSkupina',
            'stream' => false,
            'folder' => '@app/web/',
            //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
            //'target' => '_blank',
            'linkPath' => Url::toRoute('@web/web/'),
            'afterSaveView' => '/dijete/zalink',
            'dropdownOptions' => [
                'label' => Yii::t('app', 'Export All'),
                'class' => 'btn btn-outline-secondary'
            ]
        ]) . "<hr>\n" .
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
            ]); ?>

    </div>
</div>