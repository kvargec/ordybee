<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\FileInput;
use \common\models\Zaposlenje;
use \common\models\RadnoMjesto;
use \common\models\Zaposlenik;
use \common\models\ZaposlenikSkupina;
use \common\models\PedagoskaGodina;

/* @var $this yii\web\View */
/* @var $model common\models\Skupina */
/* @var $form yii\widgets\ActiveForm */
$r_mjesto = RadnoMjesto::find()->where(['slug' => 'odgojitelj'])->one();
$r_mjesto2 = RadnoMjesto::find()->where(['slug' => 'rehabilitator_odgojitelj'])->one();
$odgojitelji = [];
$zaposlenici = [];
if (!empty($r_mjesto)) {
    if(!empty($r_mjesto2)){
        $zaposlenici = Zaposlenje::find()->where(['r_mjesto' => $r_mjesto->id])->orWhere(['r_mjesto' => $r_mjesto2->id])->all();
    }else{
        $zaposlenici = Zaposlenje::find()->where(['r_mjesto' => $r_mjesto->id])->all();
    }

    foreach ($zaposlenici as $zaposlenik) {
        $odgojitelji[] = Zaposlenik::find()->where(['id'=> $zaposlenik->zaposlenik])->andWhere(['!=', 'status', 3])->one();
    }
}

//print("<pre>".print_r($zap_skup_model,true)."</pre>");die();
?>

<div class="skupina-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sifra')->textInput(['maxlength' => true]) ?>

    <?php /* $form->field($zap_skup_model, 'zaposlenik[]')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($odgojitelji,'id',function ($data) {
                    return $data['ime'].' '.$data['prezime'];
                }),
                'options' => ['placeholder' => Yii::t('app', 'Odaberi odgojitelja...'),'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
    ]) */
    ?>

    <?='<label class="control-label">'.Yii::t('app', 'Zaposlenici').'</label>';?>
    <?= Select2::widget([
        'name' => 'zaposlenikIds',
        'value' => array_map(function($x) {
            return $x->zaposlenik;
        }, $model->zaposleniciSkupine),
        'data' => ArrayHelper::map($odgojitelji,'id',function ($data) {
            if (!empty($data)) {
                return $data['ime'].' '.$data['prezime'];
            }
        }),
        'options' => [
            'placeholder' => Yii::t('app', 'Odaberi odgojitelja...'),
            'multiple' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]);
    ?>

    <?php //$form->field($model, 'slika')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'imageFile')->label(Yii::t('app','Slika'))->widget(FileInput::classname(), [
        'language' => 'hr',
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'language' => 'hr',
            'initialPreview'=>$model->isNewRecord ? [] : [$model->getImageUrl(),],
            'initialPreviewAsData'=>!$model->isNewRecord,
            'browseLabel' => Yii::t('app', 'Odabir'),
            'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteku ovdje'),
            'msgPlaceholder' => Yii::t('app', 'Odabir'),

        ]
    ]) ?>

    <?php //$form->field($model, 'ped_godina')->textInput() ?>

    <?php
    echo '<label class="control-label">'.Yii::t('app', 'Objekti').'</label>';
    echo Select2::widget([
        'name' => 'objektIds',
        'value' => array_map (function($x) {return $x->objekt;}, $model->skupinaObjekts),
        'data' => ArrayHelper::map(
            \common\models\Objekt::find()->all(),
            'id',
            'naziv'
        ),
        'options' => [
            'placeholder' => Yii::t('app', 'Objekti'),
            'multiple' => true,
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
