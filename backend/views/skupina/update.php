<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Skupina */

$this->title = Yii::t('app', 'Update Skupina: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Skupinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="skupina-update">
<div class="card">
    <div class="card-header card-header-danger">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a(Yii::t('app', 'Create Skupina'), ['create'], ['class' => 'btn btn-success']) ?>
    </div>
   

   
    <div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
