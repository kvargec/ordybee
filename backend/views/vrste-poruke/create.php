<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VrstePoruke */

$this->title = Yii::t('app', 'Create Vrste Poruke');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vrste Porukes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrste-poruke-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
