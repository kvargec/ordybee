<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DocumentZahtjevtSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dokumenti');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-zahtjev-index">
<div class="card">
    <div class="card-header card-header-danger">
        <span class="card-title"><?= Html::encode($this->title) ?></span>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
</div>
<div class="card-body">
    <?php 
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],


        'document0.naziv',
        'document0.vrstaDokumenta.opis',
        'zahtjev0.dijete0.prezime',
        'zahtjev0.dijete0.ime',
        
        [
            'label'=>Yii::t('app','Datoteka'),
            'format'=>'raw',
           
        'contentOptions' => ['class' => 'text-center'],
            'value'=>function($data){
                if($data->document0->vrsta_dokumenta!=1){
                    if(isset($data->document0->filename)){

                        return Html::a('<i class="material-icons">picture_as_pdf</i>'.Yii::t('app','Preuzmi'),Yii::$app->params['frontendUrl'].'/dokumenti/'.$data->zahtjev.'/'.$data->document0->filename);
                    }else{
                        return Yii::t('app','nema');
                    }
                }


            }
        ],
       
    ];
    ?>
    <?= ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumns,
                        'exportConfig' => [
                            ExportMenu::FORMAT_TEXT => null,
                            ExportMenu::FORMAT_HTML => null,
                            ExportMenu::FORMAT_CSV => null,
                        ],
                        'filename' => 'popisDokumenata',
                        'stream' => false,
                        'folder' => '@app/web/',
                        //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                        //'target' => '_blank',
                        'linkPath' => Url::toRoute('@web/web/'),
                        'afterSaveView' => '/dijete/zalink',
                        'dropdownOptions' => [
                            'label' => Yii::t('app','Export All'),
                            'class' => 'btn btn-outline-secondary'
                        ]
    ]) . "<hr>\n" . 
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        
    ]); ?>


</div></div>
</div>
