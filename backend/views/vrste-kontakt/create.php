<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VrsteKontakt */

$this->title = Yii::t('app', 'Create Vrste Kontakt');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vrste Kontakts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrste-kontakt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
