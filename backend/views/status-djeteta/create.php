<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StatusDjeteta */

$this->title = Yii::t('app', 'Create Status Djeteta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Status Djetetas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-djeteta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
