<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Primka */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="primka-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'broj_primke')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ulazni_racun')->textInput() ?>

    <?= $form->field($model, 'datum_primke')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'primio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unos')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\User::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
