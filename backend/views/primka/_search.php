<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\PrimkaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="primka-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'broj_primke') ?>

    <?= $form->field($model, 'ulazni_racun') ?>

    <?= $form->field($model, 'datum_primke') ?>

    <?= $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'primio') ?>

    <?php // echo $form->field($model, 'unos') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
