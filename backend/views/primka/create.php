<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Primka */

$this->title = Yii::t('app', 'Create Primka');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Primkas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="primka-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
