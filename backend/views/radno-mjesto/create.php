<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RadnoMjesto */

$this->title = Yii::t('app', 'Create Radno Mjesto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Radno Mjestos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="radno-mjesto-create">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
    </div>
<div class="card-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
