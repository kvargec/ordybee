<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\RadnoMjestoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Radno Mjestos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="radno-mjesto-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>


            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>

        </div>
        <div class="card-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            $gridColumns = [
                // ['class' => 'yii\grid\SerialColumn'],


                [
                    'value' => 'naziv',
                    'label' => Yii::t('app', 'Naziv'),
                    'attribute' => 'naziv',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Naziv'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'sprema0.naziv',
                    'label' => Yii::t('app', 'Sprema'),
                    'attribute' => 'sprema',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Vrsta spreme'),
                        'title' => Yii::t('app', 'Sprema'),
                        'class' => 'form-control'
                    ]
                ],

                [
                    'value' => 'koeficijent',
                    'label' => Yii::t('app', 'Koeficijent'),
                    'attribute' => 'koeficijent',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Koeficijent'),
                        'class' => 'form-control'
                    ]
                ],


                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['radno-mjesto/view', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pogledaj'),
                            ]);
                        },

                        'update' => function ($url, $data) {
                            $url2 = \Yii::$app->urlManager->createUrl(['radno-mjesto/update', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">create</span>', $url2, [
                                'title' => Yii::t('app', 'Ažuriraj'),
                            ]);
                        },
                        //  'delete' => function ($url, $data) {
                        //     $url2 = \Yii::$app->urlManager->createUrl(['dijete/update', 'id' => $data->id]);
                        //     return Html::a('<span class="material-icons">delete</span>', $url2, [
                        //         'title' => Yii::t('app', 'Obriši'),
                        //     ]);
                        // },

                    ],
                ],
            ];
            ?>

            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                    ExportMenu::FORMAT_CSV => null,
                ],
                'filename' => 'popisRadnihMjesta',
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/dijete/zalink',
                'dropdownOptions' => [
                    'label' => 'Export All',
                    'class' => 'btn btn-info'
                ]
            ]) . "<hr>\n" .
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                ]); ?>


        </div>
    </div>
</div>