<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\RadnoMjesto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="radno-mjesto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sprema')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(
                \common\models\Sprema::find()->all(),
                'id',
                'naziv'
            ),
            // 'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <?= $form->field($model, 'koeficijent')->textInput() ?>

    <?= $form->field($model, 'sifra')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
