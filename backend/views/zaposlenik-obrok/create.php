<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ZaposlenikObrok */

$this->title = Yii::t('app', 'Create Zaposlenik Obrok');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zaposlenik Obroks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zaposlenik-obrok-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
