<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\UserRoditelj */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-roditelj-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\User::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'roditelj')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Roditelj::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
