<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\widgets\FileInput;
use common\models\User;
/* @var $this yii\web\View */
/* @var $model common\models\Poruke */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="poruke-form">

    <?php $form = ActiveForm::begin();

    if ($reply || $reply_all) {
        $keys = array_keys($model->recipients);
        if (isset($model->cc_recipients)) {
            $keys2 = array_keys($model->cc_recipients);
        }
        if (count($keys) == 1) {
            $recipient_id = array_values($model->recipients);
            $recipient = null;

            switch ($keys[0]) {
                case 'skupine':
                    $rec = \common\models\Skupina::find()->where(['id' => $recipient_id[0]])->one();
                    $recipient = $rec->naziv;
                    break;
                case 'zaposlenici':
                    $rec = \common\models\Zaposlenik::find()->where(['id' => $recipient_id[0]])->one();
                    $recipient = $rec->ime . ' ' . $rec->prezime;
                    break;
                case 'roditelji':
                    $rec = \common\models\Roditelj::find()->where(['id' => $recipient_id[0]])->one();
                    $recipient = $rec->ime . ' ' . $rec->prezime;
                    break;
                case 'pojedini':
                    $rec = \common\models\Zaposlenik::find()->where(['id' => $recipient_id[0]])->one();
                    $recipient = $rec->ime . ' ' . $rec->prezime;
                    break;
                case 'admin':
                    $rec = \common\models\User::find()->where(['id' => $recipient_id[0]])->one();
                    $recipient = $rec->username;
                    break;
            };
            echo $form->field($model, 'grupacija')->textInput([
                'value' => $keys[0],
                'class' => 'form-control class-content-title_series',
                'disabled' => true,
            ])->label(Yii::t('app', 'Grupa korisnika'));

            echo $form->field($model, 'recipients')->textInput([
                'value' => $recipient,
                'class' => 'form-control class-content-title_series',
                'disabled' => true,
            ])->label(Yii::t('app', "Primatelji"));
        } else if ($keys > 1) {
            $recipients = [];
            foreach ($keys as $key) {
                $recipient_id = array_values($model->recipients[$key]);
                switch ($key) {
                    case 'skupine':
                        foreach ($recipient_id as $id) {
                            if ($id == 0) {
                                $recipients[] = Yii::t('app', 'Sve skupine');
                            } else {
                                $skupina = \common\models\Skupina::find()->where(['ped_godina' => $pedGodina])->andWhere(['id' => $id])->one();
                                $recipients[] = $skupina->naziv;
                            }
                        }
                    case 'zaposlenici':
                        foreach ($recipient_id as $id) {
                            if ($id == 0) {
                                $recipients[] = Yii::t('app', 'Sva radna mjesta');
                            } else {
                                $radno_mjesto = \common\models\RadnoMjesto::find()->where(['id' => $id])->one();
                                $recipients[] = $radno_mjesto->naziv;
                            }
                        }
                        break;
                    case 'roditelji':
                        foreach ($recipient_id as $id) {
                            if ($id == 0) {
                                $recipients[] = Yii::t('app', 'Svi roditelji');
                            } else {
                                $roditelj = \common\models\Roditelj::find()->where(['id' => $id])->one();
                                $recipients[] = $roditelj->ime . ' ' . $roditelj->prezime;
                            }
                        }
                        break;
                    case 'pojedini':
                        foreach ($recipient_id as $id) {
                            if ($id == 0) {
                                $recipients[] = Yii::t('app', 'Svi zaposlenici');
                            } else {
                                $zaposlenik = \common\models\Zaposlenik::find()->where(['id' => $id])->andWhere(['!=', 'status', 3])->one();
                                if (isset($zaposlenik)) {
                                    $recipients[] = $zaposlenik->ime . ' ' . $zaposlenik->prezime;
                                }
                            }
                        }
                        break;
                    case 'admin':
                        foreach ($recipient_id as $id) {
                            if ($id == 0) {
                                $recipients[] = Yii::t('app', 'Svi administratori');
                            } else {
                                $admin = \common\models\User::find()->where(['id' => $id])->one();
                                $recipients[] =  $admin->username;
                            }
                        }
                        break;
                };
            }
            echo $form->field($model, 'grupacija')->textInput([
                'value' => implode(', ', $keys),
                'class' => 'form-control class-content-title_series',
                'disabled' => true,
            ])->label(Yii::t('app', 'Grupa korisnika'));

            echo $form->field($model, 'recipients')->textInput([
                'value' => implode(', ', $recipients),
                'class' => 'form-control class-content-title_series',
                'disabled' => true,
            ])->label(Yii::t('app', "Primatelji"));
        }
    } else {
        if (!isset($model->recipients)) {
            echo $form->field($model, 'grupacija')->widget(Select2::classname(), [
                'data' => array_merge(['roditelji' => Yii::t('app', 'Roditelji'), 'skupine' => Yii::t('app', 'Skupine'), 'zaposlenici' => Yii::t('app', 'Zaposlenici-radna mjesta'), 'pojedini' => Yii::t('app', 'Pojedini zaposlenici'), 'admin' => Yii::t('app', 'Administrator sistema')]),
                'options' => ['placeholder' => Yii::t('app', 'Odaberi grupu korisnika...'), 'id' => 'grupacija'],
                'pluginOptions' => ['allowClear' => true],
            ])->label(Yii::t('app', 'Grupa korisnika'));

            echo $form->field($model, 'recipients')->hint(
                Yii::t('app', 'Odaberite primatelje (moguće je odabrati više primatelja)')
            )->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => ['id' => 'recipients'],
                'select2Options' => ['pluginOptions' => ['allowClear' => true, 'multiple' => true]],
                'pluginOptions' => [
                    'depends' => ['grupacija'],
                    'placeholder' => Yii::t('app', 'Odaberite...'),
                    'initialize' => true,
                    'loadingText' => '',
                    'url' => Url::to(['/poruke/grupacije']),
                    'allowClear' => true
                ]
            ])->label(Yii::t('app', "Primatelji"));
        } else {
            $grupacija = key($model->recipients);
            $primatelji = $model->recipients[$grupacija];

            echo $form->field($model, 'grupacija')->widget(Select2::classname(), [
                'data' => array_merge(['roditelji' => Yii::t('app', 'Roditelji'), 'skupine' => Yii::t('app', 'Skupine'), 'zaposlenici' => Yii::t('app', 'Zaposlenici-radna mjesta'), 'pojedini' => Yii::t('app', 'Pojedini zaposlenici'), 'admin' => Yii::t('app', 'Administrator sistema')]),
                'options' => ['placeholder' => Yii::t('app', 'Odaberi grupu korisnika...'), 'id' => 'grupacija', 'value' => $grupacija],
                'pluginOptions' => ['allowClear' => true],
            ])->label(Yii::t('app', 'Grupa korisnika'));

            echo $form->field($model, 'recipients')->hint(
                Yii::t('app', 'Odaberite primatelje (moguće je odabrati više primatelja)')
            )->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => ['id' => 'recipients', 'value' => $primatelji],
                'select2Options' => ['pluginOptions' => ['allowClear' => true, 'multiple' => true]],
                'pluginOptions' => [
                    'depends' => ['grupacija'],
                    'placeholder' => Yii::t('app', 'Odaberite...'),
                    'initialize' => true,
                    'loadingText' => '',
                    'url' => Url::to(['/poruke/grupacije']),
                    'allowClear' => true
                ]
            ])->label(Yii::t('app', "Primatelji"));
        }
        //za cc
        if (!isset($model->cc_recipients)) {
            echo $form->field($model, 'ccgrupacija')->widget(Select2::classname(), [
                'data' => array_merge(['roditelji' => Yii::t('app', 'Roditelji'), 'skupine' => Yii::t('app', 'Skupine'), 'zaposlenici' => Yii::t('app', 'Zaposlenici-radna mjesta'), 'pojedini' => Yii::t('app', 'Pojedini zaposlenici'), 'admin' => Yii::t('app', 'Administrator sistema')]),
                'options' => ['placeholder' => Yii::t('app', 'Odaberi grupu korisnika...'), 'id' => 'ccgrupacija'],
                'pluginOptions' => ['allowClear' => true],
            ])->label(Yii::t('app', 'Grupa korisnika za kopiju'));

            echo $form->field($model, 'cc_recipients')->hint(
                Yii::t('app', 'Odaberite primatelje (moguće je odabrati više primatelja)')
            )->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => ['id' => 'cc_recipients'],
                'select2Options' => ['pluginOptions' => ['allowClear' => true, 'multiple' => true]],
                'pluginOptions' => [
                    'depends' => ['ccgrupacija'],
                    'placeholder' => Yii::t('app', 'Odaberite...'),
                    'initialize' => true,
                    'loadingText' => '',
                    'url' => Url::to(['/poruke/grupacije']),
                    'allowClear' => true
                ]
            ])->label(Yii::t('app', "Primatelji kopije"));
        } else {
            $ccgrupacija = key($model->cc_recipients);
            $ccprimatelji = $model->cc_recipients[$ccgrupacija];

            echo $form->field($model, 'ccgrupacija')->widget(Select2::classname(), [
                'data' => array_merge(['roditelji' => Yii::t('app', 'Roditelji'), 'skupine' => Yii::t('app', 'Skupine'), 'zaposlenici' => Yii::t('app', 'Zaposlenici-radna mjesta'), 'pojedini' => Yii::t('app', 'Pojedini zaposlenici'), 'admin' => Yii::t('app', 'Administrator sistema')]),
                'options' => ['placeholder' => Yii::t('app', 'Odaberi grupu korisnika...'), 'id' => 'ccgrupacija', 'value' => $ccgrupacija],
                'pluginOptions' => ['allowClear' => true],
            ])->label(Yii::t('app', 'Grupa korisnika za kopiju'));

            echo $form->field($model, 'cc_recipients')->hint(
                Yii::t('app', 'Odaberite primatelje (moguće je odabrati više primatelja)')
            )->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'options' => ['id' => 'cc_recipients', 'value' => $ccprimatelji],
                'select2Options' => ['pluginOptions' => ['allowClear' => true, 'multiple' => true]],
                'pluginOptions' => [
                    'depends' => ['ccgrupacija'],
                    'placeholder' => Yii::t('app', 'Odaberite...'),
                    'initialize' => true,
                    'loadingText' => '',
                    'url' => Url::to(['/poruke/grupacije']),
                    'allowClear' => true
                ]
            ])->label(Yii::t('app', "Primatelji kopije"));
        }
    }
    //print("<pre>".print_r($recipient,true)."</pre>");die();
    ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'priority')->widget(
                Select2::classname(),
                [
                    'data' => [0 => Yii::t('app', 'Normalno'), 1 => Yii::t('app', 'Važno'), 2 => Yii::t('app', 'Nisko')],
                ]
            )->label(Yii::t('app', "Prioritet")); ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'vrsta_poruke')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\VrstePoruke::find()->all(),
                    'id',
                    'vrsta'
                ),
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\StatusOpce::find()->all(),
                    'id',
                    'naziv'
                ),
            ]) ?>
        </div>
    </div>
    <?= $form->field($model, 'subject')->textInput(['maxlength' => true])->label(Yii::t('app', "Predmet")); ?>

    <?= $form->field($model, 'message')->widget(\dosamigos\tinymce\TinyMce::class, [
        'options' => [
            'clientOptions' => [

                'statusbar' => false,
                'height' => 600,
                'language' => 'hr_HR',
                // 'menubar' => 'file edit view insert format tools table tc help',


                // Ovo je za more info
                //  'valid_elements'=>'h1|h2|p|br|table|td|tr|th|img|b|strong|ul|ol|li',
                'toolbar' => ' bold | numlist bullist alignleft alignright | h1 h2 formatselect| image | table | insertfile |  responsivefilemanager',
                'menubar' => '',
                // 'toolbar' => 'bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
                // 'templates' => [
                //     ['title' => 'New Table', 'description' => 'creates a new table', 'content' => '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'],
                //     ['title' => 'Starting my story', 'description' => 'A cure for writers block', 'content' => 'Once upon a time...'],
                //     ['title' => 'New list with dates', 'description' => 'New List with dates', 'content' => '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>']
                // ],
                'paste_word_valid_elements' => 'h1,h2,p,br,table,td,tr,th,img,b,strong,ul,ol,li',
                'paste_webkit_styles' => 'none',
                'paste_data_images' => true,
                'paste_enable_default_filters' => true,
                'paste_filter_drop' => true,
                'paste_remove_styles' => true,
                'paste_preprocess' => new JsExpression('function(plugin, args) {
    //console.log(args.content);
    var divElement = document.createElement("div");
        divElement.innerHTML = args.content;
        
        // loop through ALL DOM elements insidie the divElement
        var elements = divElement.getElementsByTagName("*");
        for (var i = 0; i < elements.length; i++) {
          
            elements[i].removeAttribute("style");
            elements[i].removeAttribute("color");
            elements[i].removeAttribute("class");
            elements[i].removeAttribute("align");
            elements[i].removeAttribute("href");
          
        }
        console.log(divElement.innerHTML);
        args.content=divElement.innerHTML;
  }'),
                'external_filemanager_path' => '/admin/js/responsive_filemanager/filemanager/',
                'filemanager_title' => "Responsive Filemanager",
                'external_plugins' => ["filemanager" => "/admin/js/responsive_filemanager/filemanager/plugin.min.js"],
                'inline_styles' => false,
                'image_advtab' => true,
                'plugins' => [
                    "advlist -autolink lists -link charmap print preview anchor image  directionality",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste pastetext responsivefilemanager"
                    //     ],
                ],
            ],
        ]

    ])->label(Yii::t('app', "Poruka")); ?>
    <?php $msgid = $model->isNewRecord ? null : $model->id; ?>
    <?= $form->field($model, 'sender')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
    <?= $form->field($model, 'id')->hiddenInput(['value' => $msgid])->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Pošalji'), ['class' => 'btn btn-success', 'id' => 'submit_message_btn']) ?>

        <?php
        $url2 = \yii\helpers\Url::to(['/poruke/save-template']);
        echo Html::a(Yii::t('app', 'Spremi Predložak'), $url2, [
            'class' => 'btn btn-primary',
            'id' => 'submit_template_btn',
            'data' => [
                'method' => 'post',
            ],
        ]);
        $url3 = \yii\helpers\Url::to(['/poruke/save-draft']);
        echo Html::a(Yii::t('app', 'Spremi Skicu'), $url3, [
            'class' => 'btn btn-primary',
            'id' => 'submit_draft_btn',
            'data' => [
                'method' => 'post',
            ],
        ]);
        //if (!$reply && !$reply_all) {
        $url4 = \yii\helpers\Url::to(['/poruke/save-for-upload', 'id' => $replyId, 'reply' => $reply, 'reply_all' => $reply_all]);
        echo Html::a(Yii::t('app', 'Dodaj Datoteke'), $url4, [
            'class' => 'btn btn-primary',
            'id' => 'submit_for_upload_btn',
            'data' => [
                'method' => 'post',
            ],
        ]);
        //}
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<<JS
$(document).ready(function() {
    formSubmitting = false;
    formDirty = false;
    $('#submit_message_btn').on('click', function(e) {
        message = $('#poruke-message_ifr').contents().find('body').text();
        recipients = $('#recipients').val();
        subject = $('#poruke-subject').val();

        if(recipients.length == 0) {
            e.preventDefault();
            //alert('Molimo unesite primatelje');
            $('#recipients').notify('Molimo unesite primatelje');
        } else if (!$.trim(message)) {
            e.preventDefault();
            //alert('Poruka ne može biti prazna');
            $('label[for="poruke-message"]').notify('Poruka ne može biti prazna');
        } else if (!$.trim(subject)) {
            e.preventDefault();
            //alert('Molimo upišite predmet');
            $('#poruke-subject').notify('Molimo upišite predmet');
        } else {
            formSubmitting = true;
        } 
    });
    $('#submit_for_upload_btn').on('click', function(e) {
        message = $('#poruke-message_ifr').contents().find('body').text();
        recipients = $('#recipients').val();
        subject = $('#poruke-subject').val();

        if(recipients.length == 0) {
            e.preventDefault();
            e.stopPropagation();
            //alert('Molimo unesite primatelje.');
            $('#recipients').notify('Molimo unesite primatelje');
        } else if (!$.trim(message)) {
            e.preventDefault();
            e.stopPropagation();
            //alert('Poruka ne može biti prazna.');
            $('label[for="poruke-message"]').notify('Poruka ne može biti prazna');
        } else if (!$.trim(subject)) {
            e.preventDefault();
            e.stopPropagation();
            //alert('Molimo upišite predmet.');
            $('#poruke-subject').notify('Molimo upišite predmet');
        } else {
            formSubmitting = true;
        } 
    });
    $('#submit_template_btn').on('click', function(e) {
        formSubmitting = true;
    });
    $('#submit_draft_btn').on('click', function(e) {
        formSubmitting = true;
    });
    ajaxSubmitted = false;

    $(window).on('beforeunload', function(e) {
        e.preventDefault();
        message = $('#poruke-message_ifr').contents().find('body').text();
        recipients = $('#recipients').val();
        subject = $('#poruke-subject').val();

        if(!$.trim(message) && !$.trim(subject) && recipients.length == 0) {
            formDirty = false;
        } else {
            formDirty = true;
        }
        if (formSubmitting) {
            return undefined;
        } else {
            if (formDirty && !ajaxSubmitted) {
                ajaxSubmitted = true;
                var url = '/ordybee/backend/web/index.php?r=poruke/save-draft';
                var data = $('#w0').serializeArray();
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: data
                }).done(function(data, textStatus, jqXHR) {
                    $.notify(data.message, "success");
                    console.log(data.message);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                });
            } else {
                return undefined;
            }
        }
    });
});
JS;
$this->registerJs($js);
?>