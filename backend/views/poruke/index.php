<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\PorukeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Porukes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poruke-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Poruke'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'subject',
            'message:ntext',
            'sender',
            'recipients',
            //'created_at',
            //'updated_at',
            //'send_at',
            //'status',
            //'attachments',
            //'priority',
            //'vrsta_poruke',
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '50px',

                'value' => function ($model, $key, $index, $column) {
                    return \kartik\grid\GridView::ROW_COLLAPSED;
                },
                // uncomment below and comment detail if you need to render via ajax
                // 'detailUrl' => Url::to(['/poruke/book-details']),
                'detail' => function ($model, $key, $index, $column) {
                    return Yii::$app->controller->renderPartial('_expand-row-details', ['model' => $model]);
                },
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                //'responsiveWrap' =>true,
                'expandOneOnly' => true,
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
