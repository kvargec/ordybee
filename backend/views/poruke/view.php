<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Poruke */

$this->title = Yii::t('app', '') . $model->subject;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Porukes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$count = 0;
?>
<div class="poruke-view">



    <div class="col-md-4 col-lg-2">
        <div>
            <?= Html::a('<span class="material-icons">add</span>', ['poruke/create'], ['class' => 'btn btn-success wide']) ?></div>


        <div id="accordion" role="tablist">
            <div class="card-collapse mt-0">
                <div class="card-header" role="tab" id="headingOne">
                    <h5 class="mb-0 mt-0">
                        <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                            <?php echo Yii::t('app', 'Folders'); ?>
                            <i class="material-icons">keyboard_arrow_down</i>
                        </a>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                    <div class="card-body">
                        <ul>
                            <?php
                            foreach ($folders as $folder) {

                                ?>
                                <li>
                                    <h5 class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/mailbox/index']) ?>" class="">
                                            <span class="material-icons"><?= $folder->icon ?></span><?= ' ' . Yii::t('app', $folder->folder_name) ?> <?= $count > 0 ? '(' . $count . ')' : '' ?>
                                        </a></h5>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>


        </div>

    </div>
    <div class="col-md-8 col-lg-10">
        <div class="card">
            <div class="card-header card-header-primary">
                <span class="card-title"><?= Html::encode($this->title) ?></span>

                <?= Html::a(Yii::t('app', 'Reply'), ['reply', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['trash', 'id' => $model->id], [
                    'class' => 'btn btn-danger btn-sm',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>

            </div>
            <div class="card-body">

                <div class="poruka">
                    <!-- <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [

                            //   'id',
                            'subject',

                            [
                                'label' => Yii::t('app', 'Pošiljatelj'),
                                'attribute' => 'sender_id',
                                'value' => function ($data) {
                                    return isset($data->sender_id) ? $data->sender->username : '';
                                }
                            ],
                            //    'sender',
                            //   'recipients',
                            //   'created_at',
                            //   'updated_at',
                            'send_at',
                            //   'status',
                            //   'attachments',
                            //  'priority',
                            //  'vrsta_poruke0.vrsta',
                            'message:html',
                        ],
                    ]) ?> -->

                    <table id="w0" class="table table-striped table-bordered">
                        <tbody>

                        <tr>
                            <td><?php echo $model->sender; ?></td>
                            <td><?php echo $model->send_at; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo $model->message; ?>
                        </tr>
                        <tr>
                            <td class="attachments"><?php
                                $datoteke = array();
                                if (is_array($model->attachments) && array_key_exists('attachments', $model->attachments)) 
                                $datoteke = $model->attachments['attachments'];
                                foreach ($datoteke as $item) {
                                    $detalji = explode(".", $item);
                                    $ekstenzija = $detalji[sizeof($detalji) - 1];
                                    $path = $detalji[0];
                                    $ime = substr($path, strrpos($path, '/') + 1);
                                    $ekstenzija2 = '';
                                    switch ($ekstenzija) {
                                        case 'jpg':
                                        case 'png':
                                            $ekstenzija2 = '<span class="fa fa-file-image-o"></span>';
                                            break;
                                        case 'pdf':
                                            $ekstenzija2 = '<span class="fa fa-file-pdf-o"></span>';
                                            break;
                                        case 'doc':
                                            $ekstenzija2 = '<span class="fa fa-file-word-o"></span>';
                                            break;
                                        case 'mp4':
                                            $ekstenzija2 = '<span class="fa fa-file-video-o"></span>';
                                            break;
                                        case 'xlsx':
                                            $ekstenzija2 = '<span class="fa fa-file-excel-o"></span>';
                                            break;
                                        case 'docx':
                                            $ekstenzija2 = '<span class="fa fa-file-word-o"></span>';
                                            break;
                                    }
                                    // nedostaje default neka ikona
                                    echo  "<div class='col-sm-3 col-lg-2 text-center'><a class='att' href='".$item."'>" . $ekstenzija2 . " " . $ime . "." . $ekstenzija . "</a></div>";
                                }
                                ?>
                            </td>
                            <td class="download"><a href="#"><span class="fa fa-download "></span></a></td>

                        </tr>
                        <tr>
                            <td><?= Html::a(Yii::t('app', 'Reply'), ['reply', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
                                <?= Html::a(Yii::t('app', 'Delete'), ['trash', 'id' => $model->id], [
                                    'class' => 'btn btn-danger btn-sm',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?></td>
                        <tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>