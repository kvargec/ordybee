<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kdn\yii2\JsonEditor;
/* @var $this yii\web\View */
/* @var $model common\models\Postavke */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="postavke-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'postavka')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vrijednost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dodatno')->widget(
		JsonEditor::class,
		[
			'clientOptions' => ['modes' => ['code', 'tree']],
			'decodedValue' => $model->dodatno, /* if attribute contains already decoded JSON,
        then you should pass it as shown, otherwise omit this line */
		]
	); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
