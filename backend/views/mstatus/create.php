<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Mstatus */

$this->title = Yii::t('app', 'Create Mstatus');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mstatuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mstatus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
