<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
?>


<div class="dijete-zapis-form">
    <?php 
    $form = ActiveForm::begin(); ?>

    <?= $form->field($dnevnikZapis, 'zapis')->textInput()->label(Yii::t('app', 'Zapis'))?>

    <?= $form->field($model2, 'bolest')->textInput()->label(Yii::t('app', 'Kronična bolest'))?>

    <?= $form->field($model2, 'attachments[]')->widget(FileInput::classname(), [
        'language' => 'hr',
        'options' => ['accept' => '*/*', 'id' => 'input', 'multiple' => false,],
        'pluginOptions' => [
            'initialPreview' => isset($modelId) ? $dnevnikZapis->getDijeteFilePaths($modelId) : [],
            'initialPreviewConfig' => isset($modelId) ? $dnevnikZapis->getDijeteFilePathsConfig() : [],
            //'initialPreviewAsData' => true,
            'hideThumbnailContent' => true,
            'showRemove' => false,
            'showUpload' => false,
            'deleteUrl' => yii\helpers\Url::to(['/dijete-zapis/delete-file']),
            'deleteExtraData' => [
                'model_id' => $modelId ?? null,
            ],
            'encodeUrl' => false,
            'language' => substr(\Yii::$app->language, 0, 2),
            'browseLabel' => Yii::t('app', 'Odabir'),
            'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteku ovdje'),
            'placeholder' => Yii::t('app', 'Odabir'),
            'msgPlaceholder' => Yii::t('app', 'Odabir'),
        ],


    ])->label(Yii::t('app', "Datoteka")) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>