<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\DijeteZapis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dijete-zapis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($dnevnikZapis, 'zapis')->textInput()->label(Yii::t('app','Zapis'))?>

    <?= $form->field($model2, 'naziv')->textInput()->label(Yii::t('app','Ime i prezime'))?>

    <?= $form->field($model2, 'djelatnost')->dropDownList(['pedijatar'=>Yii::t('app','Pedijatar'),'zubar'=>Yii::t('app','Zubar')],['prompt'=>Yii::t('app','Odaberite djelatnost...')])->label(Yii::t('app','Djelatnost')) ?>

    <?= $form->field($model2, 'adresa')->textInput()->label(Yii::t('app','Adresa')) ?>

    <?= $form->field($model2, 'telefon')->textInput()->label(Yii::t('app','Telefon')) ?>

    <?= $form->field($model2, 'email')->textInput()->label(Yii::t('app','Email')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
