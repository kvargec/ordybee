<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$polja = ['bcg', 'diteper', 'polio', 'moparu', 'hib', 'hbv'];
$count = 0;
?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($dnevnikZapis, 'zapis')->hiddenInput(['value' => 'Cijepljenje 5u1'])->label(false);
?>
<table>
    <thead>
        <th>I.</th>
        <th>II.</th>
        <th>III.</th>
        <th>IV.</th>
        <th>V.</th>
    </thead>
    <tbody>
        <?php
        foreach ($polja as $polje) {
        ?>
            <tr>
                <?php
                foreach ($model2 as $index => $model) {
                ?>
                    <td>
                        <?php echo  DatePicker::widget([
                            'model' => $model,
                            'attribute' => "[$index]$polja[$index]",
                            'options' => [
                                'id' => "cijepljenje5u1-$count-$polja[$index]"
                            ],
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose' => true,
                            ]
                        ]);
                        ?></td>
                    <!--<td></td>
                    <td></td>
                    <td></td>
                    <td></td>-->

                <?php } ?>
            </tr>
        <?php
            $count++;
        } ?>
    </tbody>
</table>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end();

$js = <<<JS
$("#cijepljenje5u1-0-bcg").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    //console.log(date);
    $("input[id^=cijepljenje5u1-0-").val(date);
});
$("#cijepljenje5u1-1-bcg").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    //console.log(date);
    $("input[id^=cijepljenje5u1-1-").val(date);
});
$("#cijepljenje5u1-2-bcg").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    //console.log(date);
    $("input[id^=cijepljenje5u1-2-").val(date);
});
$("#cijepljenje5u1-3-bcg").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    //console.log(date);
    $("input[id^=cijepljenje5u1-3-").val(date);
});
$("#cijepljenje5u1-4-bcg").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    //console.log(date);
    $("input[id^=cijepljenje5u1-4-").val(date);
});
$("#cijepljenje5u1-5-bcg").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    //console.log(date);
    $("input[id^=cijepljenje5u1-5-").val(date);
});
$("#cijepljenje5u1-6-bcg").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    //console.log(date);
    $("input[id^=cijepljenje5u1-6-").val(date);
});
JS;
$this->registerJs($js);
?>