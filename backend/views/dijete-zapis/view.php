<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use slavkovrn\lightbox\LightBoxWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\DijeteZapis */

$this->title = $model->zapis0->zapis;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dijete Zapis'), 'url' => ['/dijete-zapis/dijete', 'id' => $model->dijete, 'tip' => $model->zapis0->vrsta0->grupa]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="dijete-zapis-view">
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>


            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'tip' => $model->zapis0->vrsta0->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="card-body detail">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    [
                        'attribute' => 'dijete',
                        'value' => function ($data) {
                            return $data->dijete0->fullName;
                        }
                    ],
                    'zapis0.zapis',
                    [
                        'format' => 'html',
                        'label' => Yii::t('app', 'Dodatno'),
                        'value' => function ($data) {
                            $tmp = json_decode($data->dodatno);
                            $txt = '';
                            if (!is_object($tmp)) {
                                $tmp = json_decode($tmp);
                            }
                            foreach ($tmp as $key => $value) {
                                if (!is_array($value)) {
                                    $key = preg_replace('/\_/', ' ', $key);
                                    if (trim($value) != '') {
                                        $txt .= $key . ': ' . $value . '<br>';
                                    }
                                } else {
                                    $txt .= $key . ':<br>';
                                    foreach ($value as $item) {
                                        if ($key == 'attachments') {
                                            $link = Html::a('[link]<span class="material-icons">file_download</span>', Url::base(true) . '/media/dijete/' . $data->dijete . '/' . $item, ['target' => '_blank']);

                                            $txt .=  $link . '<br>'; //podaci koji nisu bolest
                                        }
                                    }
                                }
                            }
                            return $txt;
                        }
                    ],
                ],
            ]);

            ?>

        </div>
        <?php
        $tmp = json_decode($model->dodatno);
        $txt = '';
        if (!is_object($tmp)) {
            $tmp = json_decode($tmp);
        }
        foreach ($tmp as $key => $value) {
            if (!is_array($value)) {
                $txt .= $key . ': ' . $value . '<br>';
            } else {
                $txt .= $key . ':<br>';
                foreach ($value as $item) {
                    if ($key == 'attachments') {
                        $images[] = ['src' => '/upisi/backend/web/media/dijete/' . $model->dijete . '/' . $item, 'url' => '/upisi/backend/web/media/dijete/' . $model->dijete . '/' . $item];
                    }
                }
                if ($key == 'attachments') {
                }
            }
        }
        if (!empty($images)) {
            echo dosamigos\gallery\Gallery::widget(['items' => $images]);
        }
        ?>
    </div>
</div>