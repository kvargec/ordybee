<?php

use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
?>


<div class="dijete-zapis-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($dnevnikZapis, 'zapis')->textInput()->label(Yii::t('app', 'Zapis'))?>

    <?= $form->field($model2, 'pocetak_izostanka')->widget(DatePicker::className(),[
    'name' => 'dp_2',
    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-m-d'

    ]
])->label(Yii::t('app', 'Početak izostanka'))?>

    <?= $form->field($model2, 'kraj_izostanka')->widget(DatePicker::className(),[
        'name' => 'dp_2',
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-m-d'
        ]
    ])->label(Yii::t('app', 'Kraj izostanka')) ?>

<?php
    $datalist = \common\models\Icd10classification::find()->where(['id' =>$model2->bolest])->all();
    $data = ArrayHelper::map($datalist,'id', function($data){
        return $data->code.' - '.$data->name;
    });
?>
    <?= $form->field($model2, 'bolest')->widget(Select2::classname(), [
            'data' =>$data,
        'options' => ['placeholder' => Yii::t('app', 'Pretraživanje bolesti'),],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Čekanje rezultata...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/dijete-zapis/bolesti']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(bolest) { return bolest.name; }'),
            'templateSelection' => new JsExpression('function (bolest) { return bolest.name; }'),
        ],
    ]); ?>

<?= $form->field($model2, 'attachments[]')->widget(FileInput::classname(), [
        'language' => 'hr',
        'options' => ['accept' => '*/*', 'id' => 'input', 'multiple' => false,],
        'pluginOptions' => [
            'initialPreview' => isset($modelId) ? $dnevnikZapis->getDijeteFilePaths($modelId) : [],
            'initialPreviewConfig' => isset($modelId) ? $dnevnikZapis->getDijeteFilePathsConfig() : [],
            //'initialPreviewAsData' => true,
            'hideThumbnailContent' => true,
            'showRemove' => false,
            'showUpload' => false,
            'deleteUrl' => yii\helpers\Url::to(['/dijete-zapis/delete-file']),
            'deleteExtraData' => [
                'model_id' => $modelId ?? null,
            ],
            'encodeUrl' => false,
            'language' => substr(\Yii::$app->language, 0, 2),
            'browseLabel' => Yii::t('app', 'Odabir'),
            'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteku ovdje'),
            'placeholder' => Yii::t('app', 'Odabir'),
            'msgPlaceholder' => Yii::t('app', 'Odabir'),
        ],


    ])->label(Yii::t('app', "Datoteka")) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

