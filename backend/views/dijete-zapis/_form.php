<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\DijeteZapis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dijete-zapis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($dnevnikZapis, 'zapis')->textInput()->label(Yii::t('app','Zapis'))?>

    <?= $form->field($model2, 'tezina')->textInput()->label(Yii::t('app','Težina'))?>

    <?= $form->field($model2, 'percentiletezine')->textInput()->label(Yii::t('app','Percentile težine')) ?>

    <?= $form->field($model2, 'visina')->textInput()->label(Yii::t('app','Visina')) ?>

    <?= $form->field($model2, 'percentilevisine')->textInput()->label(Yii::t('app','Percentile visine')) ?>

    <?= $form->field($model2, 'relativnatezina')->textInput()->label(Yii::t('app','Relativna težina')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
