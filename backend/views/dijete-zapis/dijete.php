<?php

use common\helpers\Utils;
use common\models\PedagoskaGodina;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DijeteZapisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zapisi za dijete') . ' ' . $dijete->getFullName() . ' ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dijete-zapis-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <style>
                .dropdown-toggle {
                    max-width: 97px;
                    max-height: 41px;
                }

                .dijete-zapis-index .dropdown-toggle>span {
                    position: absolute;
                    top: 14px;
                    left: 38%;
                }
            </style>
            <div class="dropdown">
                <a class="btn btn-success btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span><span class="material-icons">add</span> <span class="caret"></span></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <?php
                    foreach ($vrsteZapisa as $vrsta) {
                    ?>
                        <li>
                            <?= Html::a($vrsta->title, ['create', 'tip' => $vrsta->id, 'dijete' => $dijete->id, 'grupa' => $tip]) ?>
                        </li>
                    <?php } ?>
                </ul>

            </div>
            <?= Html::a('<span class="material-icons">print</span>', ['pdf-print', 'id' => $dijete->id, 'tip' => $tip], ['class' => 'btn btn-primary', 'target' => '_blank']) ?>

        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <style>
                th.w30 {
                    max-width: 30%
                }
            </style>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [

                    [
                        'label' => Yii::t('app', 'Zapis'),
                        'headerOptions' => ['class' => 'w30'],
                        'value' => function ($data) {

                            return $data['zapis'];
                        },
                    ],

                    [
                        'format' => 'raw',
                        'label' => Yii::t('app', 'Unos'),
                        'value' => function ($data) {
                            $zapisi = json_decode($data['json_agg']);
                            $return = '';
                            foreach ($zapisi as $item) {
                                $data = \common\models\DijeteZapis::findOne($item);
                                $dnevnik_zapis = \common\models\DnevnikZapis::find()->where(['id' => $data->zapis])->one();
                                $tmp = is_array($data->dodatno) ? $data->dodatno : json_decode($data->dodatno);
                                $text = '';
                                $pocetak = '';
                                $kraj = '';
                                foreach ($tmp as $key => $value) {
                                    if (!is_array($value)) {
                                        if ($key == 'bolest') {
                                            if (!empty($value)) {
                                                if (is_numeric($value)) {
                                                    $bolest = \common\models\Icd10classification::findOne($value);
                                                    if (!empty($bolest)) {
                                                        $text .= $key . ': ' . $bolest->code . ' - ' . $bolest->name . '<br>';
                                                    } else {
                                                        $text .= $key . '; ' . $value . '<br />';
                                                    };
                                                    // $text .= $key . ': ' . $bolest->code . ' - ' . $bolest->name . '<br>';
                                                } else {
                                                    $text .= $key . ': ' . $value . '<br>';
                                                }
                                            } else {
                                                $text .= $key . ': ' . $value . '<br>';
                                            }
                                        } else {
                                            if ($key == 'pocetak_izostanka')  $start_date = new \DateTime(date('Y-m-d', strtotime($value)));
                                            if ($key == 'pocetak_izostanka' or $key == 'kraj_izostanka') {
                                                $text .= Yii::t('app', $key) . ': ' . Yii::$app->formatter->asDate($value) . '<br>';
                                                if ($key == 'kraj_izostanka') {
                                                    $end_date = new \DateTime(date('Y-m-d', strtotime($value)));
                                                    $interval = \DateInterval::createFromDateString('1 day');
                                                    $period = new \DatePeriod($start_date, $interval, $end_date);
                                                    $brojDana = Utils::calculateDaysIspricnica($period, 'dijete', $data->dijete, 0);
                                                    if (isset($brojDana[0])) {
                                                        $text .= Yii::t('app', 'Broj dana: ') . $brojDana[0]->ukupno . '<br />';
                                                    } else {
                                                    }
                                                }
                                            } else {
                                                if ($key == 'attachments') {
                                                    $text .= $key . ': ' . $value . '<br>';
                                                } elseif ($data->zapis0->vrsta0->naziv == 'pracenje_djeteta') {
                                                    $text = 'Obrazac za individualno praćenje djeteta - ' . Yii::$app->formatter->asDate($dnevnik_zapis->updated_at, 'dd.MM.YYYY.');
                                                } else {
                                                    $key = preg_replace('/\_/', ' ', $key);
                                                    if (trim($value) != '') {
                                                        $text .= $key . ': ' . $value . '<br>';
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        if ($key == 'bolest') {
                                            if (!empty($value[array_key_first($value)])) {
                                                if (is_numeric($value[array_key_first($value)])) { // podaci koji imaju id iz baze i spremljeni su kao array
                                                    $bolest = \common\models\Icd10classification::findOne($value[array_key_first($value)]);
                                                    if (!empty($bolest)) {
                                                        $text .= $key . ': ' . $bolest->code . ' - ' . $bolest->name . '<br>';
                                                    } else {
                                                        $text .= $key . '; ' . $value . '<br />';
                                                    };
                                                } else {
                                                    $text .= $key . ': ' . $value[array_key_first($value)] . '<br>'; //podaci koji su array
                                                }
                                            } else {
                                                $text .= $key . ': ' . $value[array_key_first($value)] . '<br>'; // podaci koji su array
                                            }
                                        } else {
                                            //    $key = preg_replace('/\_\d', '', $key);
                                            $text .= $key . ': <br>';
                                            foreach ($value as $item) {
                                                $link = Html::a('[link]<span class="material-icons">file_download</span>', Url::base(true) . '/media/dijete/' . $data->dijete . '/' . $item, ['target' => '_blank']);
                                                if ($key == 'attachments') {
                                                    $text .=  $link . '<br>'; //podaci koji nisu bolest
                                                } else {
                                                    $text .=  $item . '<br>'; //podaci koji nisu bolest
                                                }
                                            }
                                        }
                                    }
                                }
                                $return .= $text;
                            }
                            return $return;
                        }
                    ],


                    [
                        'format' => 'raw',
                        'label' => Yii::t('app', 'Datoteke'),
                        'value' => function ($data) {
                            $data = \common\models\DijeteZapis::findOne([json_decode($data['json_agg'])[0]]);
                            if ($data->zapis0->vrsta0->naziv == 'nalaz')
                                return Html::a('<i class="fa fa-file-pdf-o" style="font-size:24px"></i>', \yii\helpers\Url::to(['/dijete-zapis/zapispdf', 'id' => $data->id]));
                            else
                                return;
                        },

                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}{update}{delete}',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data) {
                                $data = \common\models\DijeteZapis::findOne([json_decode($data['json_agg'])[0]]);
                                // $dnevnik_zapis = \common\models\DnevnikZapis::find()->where(['id' => $data->zapis])->one();
                                if ($data->zapis0->vrsta0->naziv == 'pracenje_djeteta') {
                                    return Html::a('<span class="material-icons hidden">visibility</span>', '', [
                                        'title' => Yii::t('app', 'Pogledaj'),
                                    ]);
                                } else {
                                    $url2 = \Yii::$app->urlManager->createUrl(['dijete-zapis/view', 'id' => $data->id]);
                                    return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                        'title' => Yii::t('app', 'Pogledaj'),
                                    ]);
                                }
                            },

                            'update' => function ($url, $data) {
                                $data = \common\models\DijeteZapis::findOne([json_decode($data['json_agg'])[0]]);
                                // $dnevnik_zapis = \common\models\DnevnikZapis::find()->where(['id' => $data->zapis])->one();
                                if ($data->zapis0->vrsta0->naziv == 'pracenje_djeteta') {
                                    $url2 = \Yii::$app->urlManager->createUrl(['dijete-zapis/update', 'id' => $data->id, 'tip' => $data->zapis0->vrsta0->id]);
                                } else ($url2 = \Yii::$app->urlManager->createUrl(['dijete-zapis/update', 'id' => $data->id]));
                                return Html::a('<span class="material-icons">create</span>', $url2, [
                                    'title' => Yii::t('app', 'Ažuriraj'),
                                ]);
                            },
                            'delete' => function ($url, $data) {
                                $data = \common\models\DijeteZapis::findOne([json_decode($data['json_agg'])[0]]);
                                $url2 = \Yii::$app->urlManager->createUrl(['dijete-zapis/delete', 'id' => $data->id]);
                                return Html::a('<span class="material-icons red">delete</span>', $url2, [
                                    'title' => Yii::t('app', 'Obriši'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },

                        ],
                    ]
                ],
            ]); ?>


        </div>
    </div>
</div>