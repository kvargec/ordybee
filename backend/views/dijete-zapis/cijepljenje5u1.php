<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>
<?php $count = 0; ?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($dnevnikZapis, 'zapis')->hiddenInput(['value' => 'Cijepljenje 5u1'])->label(false);
?>
<table>
    <thead>
        <th>I.</th>
        <th>II.</th>
        <th>III.</th>
        <th>IV.</th>
        <th>V.</th>
    </thead>
    <tbody>
        <tr>
            <?php if (isset($zapisi_vrijednosti['bcg'])) {
                $count = 0;
                foreach ($zapisi_vrijednosti['bcg'] as $value) {
                    echo '<td>' . DatePicker::widget([
                        'name' => 'bcg',
                        'value' => $value,
                        'disabled' => true,
                    ]) . '</td>';
                    $count++;
                }
            }
            ?>



            <?php if ($count < 5) {   //promijeniti u koliko puta se max dijete cijepi sa bcg
            ?>
                <td> <?= $form->field($model2, 'bcg')->widget(DatePicker::classname(), [
                            'name' => 'bcg',
                            'convertFormat' => true,
                            // 'options' => ['placeholder' => 'Enter birth date ...'],
                            'pluginOptions' => [
                                'autoclose' => true
                            ],
                            'pluginEvents' => [
                                "changeDate" => "function(e) { console.log(e);
                        var formattedDate = new Date(e.date);
                        var d = formattedDate.getDate();
                        var m =  formattedDate.getMonth();
                        m += 1;  // JavaScript months are 0-11
                        var y = formattedDate.getFullYear();

                       
                        $('#dynamicmodel-diteper').attr('value',d + \".\" + m + \".\" + y + \".\");
                        $('#dynamicmodel-polio').attr('value',d + \".\" + m + \".\" + y + \".\");
                        $('#dynamicmodel-moparu').attr('value',d + \".\" + m + \".\" + y + \".\");
                        $('#dynamicmodel-hib').attr('value',d + \".\" + m + \".\" + y + \".\");
                         }",
                            ],


                        ]) ?></td>
            <?php } ?>
        </tr>
        <tr>
            <?php if (isset($zapisi_vrijednosti['diteper'])) { ?>
            <?php
                $count = 0;
                foreach ($zapisi_vrijednosti['diteper'] as $value) {
                    echo '<td>' . DatePicker::widget([
                        'name' => 'bcg',
                        'value' => $value,
                        'disabled' => true,
                    ]) . '</td>';
                    $count++;
                }
            }
            ?>
            <?php if ($count < 5) {   //promijeniti u koliko puta se max dijete cijepi sa diteper
            ?>
                <td><?= $form->field($model2, 'diteper')->widget(DatePicker::classname(), [
                        'name' => 'diteper',
                        'convertFormat' => true,
                        // 'options' => ['placeholder' => 'Enter birth date ...'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]) ?>
                </td>
            <?php } ?>
        </tr>
        <tr>
            <?php if (isset($zapisi_vrijednosti['polio'])) { ?>
            <?php
                $count = 0;
                foreach ($zapisi_vrijednosti['polio'] as $value) {
                    echo '<td>' . DatePicker::widget([
                        'name' => 'bcg',
                        'value' => $value,
                        'disabled' => true,
                    ]) . '</td>';
                    $count++;
                }
            }
            ?>
            <?php if ($count < 5) {   //promijeniti u koliko puta se max dijete cijepi sa diteper
            ?>

                <td><?= $form->field($model2, 'polio')->widget(DatePicker::classname(), [
                        'name' => 'polio',
                        'convertFormat' => true,
                        // 'options' => ['placeholder' => 'Enter birth date ...'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]) ?>
                </td>
            <?php } ?>

        </tr>
        <tr>

            <?php if (isset($zapisi_vrijednosti['moparu'])) { ?>
            <?php
                $count = 0;
                foreach ($zapisi_vrijednosti['moparu'] as $value) {
                    echo '<td>' . DatePicker::widget([
                        'name' => 'bcg',
                        'value' => $value,
                        'disabled' => true,
                    ]) . '</td>';
                    $count++;
                }
            }
            ?>
            <?php if ($count < 5) {   //promijeniti u koliko puta se max dijete cijepi sa diteper
            ?>

                <td><?= $form->field($model2, 'moparu')->widget(DatePicker::classname(), [
                        'name' => 'moparu',
                        'convertFormat' => true,
                        // 'options' => ['placeholder' => 'Enter birth date ...'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]) ?>
                </td>
            <?php } ?>

        </tr>
        <tr>
            <?php if (isset($zapisi_vrijednosti['hib'])) { ?>
            <?php
                $count = 0;
                foreach ($zapisi_vrijednosti['hib'] as $value) {
                    echo '<td>' . DatePicker::widget([
                        'name' => 'bcg',
                        'value' => $value,
                        'disabled' => true,
                    ]) . '</td>';
                    $count++;
                }
            }
            ?>
            <?php if ($count < 5) {   //promijeniti u koliko puta se max dijete cijepi sa diteper
            ?>

                <td><?= $form->field($model2, 'hib')->widget(DatePicker::classname(), [
                        'name' => 'hib',
                        'convertFormat' => true,
                        // 'options' => ['placeholder' => 'Enter birth date ...'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]) ?>
                </td>
            <?php } ?>

        </tr>
    </tbody>
</table>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end();
