<?php

use yii\helpers\Html;
?>
<div class="dijete-zapis-index">

   
<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Yii::t('app', 'Mjerenje') ?>: <?= $skupina->naziv ?></span>
       
    </div>
<div class="card-body">
<?php

$listaDjece = '';
$text = '<table class="table table-striped table-bordered"><tr><td>'.Yii::t('app', 'Ime i prezime').'</td><td>'.Yii::t('app', 'Mjerenje').'</td></tr>';
foreach ($djeca as $dijete){

    $forma='';
    $vrstaZapis = \common\models\VrstaZapis::findOne(['naziv' => 'mjerenje', 'grupa' => 'zdravstvo']);
    $mjerenje = \common\models\DijeteZapis::find()->joinWith('zapis0')->where(['dijete'=>$dijete->id,'vrsta'=>$vrstaZapis->id])->one();
    $mjere = isset($mjerenje)?$mjerenje->dodatno:null;

    while (is_string($mjere)){
        $mjere = json_decode($mjere, true);
    }

    $listaDjece.= $dijete->getFullName();
    $forma.= Html::beginForm(['dijete-zapis/unesi-mjerenje'], 'post', ['class' => 'unesi-mjerenje']);
    $forma.= Html::hiddenInput('id',$dijete->id);
    $forma.= Html::textInput('tezina',empty($mjere)?'':$mjere['tezina'],['size'=>5,'placeholder'=>Yii::t('app', 'Težina')]);
    $forma.=Html::textInput('percentiletezine',empty($mjere)?'':$mjere['percentiletezine'],['size'=>15,'placeholder'=>Yii::t('app', 'Percentile težine')]);
    $forma.=Html::textInput('visina',empty($mjere)?'':$mjere['visina'], ['size'=>5,'placeholder'=>Yii::t('app','Visina')]);
    $forma.=Html::textInput('percentilevisine',$mjere==NULL?'':$mjere['percentilevisine'], ['size'=>15,'placeholder'=>Yii::t('app', 'Percentile visine')]);
    $forma.=Html::textInput('relativnatezina',$mjere==NULL?'':$mjere['relativnatezina'] ,['size'=>15,'placeholder'=>Yii::t('app', 'Relativna težina')]);
    $forma.=Html::submitButton(Yii::t('app', 'Spremi'), array('class' => 'btn btn-info btn-sm'));
    $forma.=Html::endForm();
    $text.='<tr><td>'.$dijete->getFullName().'</td><td>'.$forma.'</td></tr>';
}
$text.='</table>';
echo $text;

$js = <<<JS
$(".unesi-mjerenje").on("submit", function(e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var data = $(this).serializeArray();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data
    }).done(function(data, textStatus, jqXHR) {
        $.notify(data.message, "success");
    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert( errorThrown );
    });
});

JS;
$this->registerJs($js);
?>

