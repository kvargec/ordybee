<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DijeteZapisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dijete Zapis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dijete-zapis-index">

   
<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
<div class="card-body">

  

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'value'=>'dijete0.ime',
                'label'=>Yii::t('app','Ime djeteta'),
                'attribute'=>'dijete.ime',
               
            ],
            [
                'value'=>'dijete0.prezime',
                'label'=>Yii::t('app','Prezime djeteta'),
                'attribute'=>'dijete.prezime',
                
            ],
            [
                'value'=>'zapis',
                'label'=>Yii::t('app','Zapis'),
                'attribute'=>'zapis',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
					'placeholder' => Yii::t('app','Zapis'),
                    'title' => Yii::t('app','Zapis'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value'=>'dodatno',
                'label'=>Yii::t('app','Dodatno'),
                'attribute'=>'dodatno',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
					'placeholder' => Yii::t('app','Dodatno'),
                    'title' => Yii::t('app','dodatno'),
                    'class' => 'form-control'
                ]
            ],
            

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}',
                // 'visible' => Yii::$app->user->can('admin'),
                'buttons' => [
                    'view' => function ($url, $data) {

                        $url2 = \Yii::$app->urlManager->createUrl(['dijete-zapis/view', 'id' => $data->id]);
                        return Html::a('<span class="material-icons">visibility</span>', $url2, [
                            'title' => Yii::t('app', 'Pogledaj'),
                        ]);
                    },

                    'update' => function ($url, $data) {
                        $url2 = \Yii::$app->urlManager->createUrl(['dijete-zapis/update', 'id' => $data->id]);
                        return Html::a('<span class="material-icons">create</span>', $url2, [
                            'title' => Yii::t('app', 'Ažuriraj'),
                        ]);
                    },


                ],
            ],
        ],
    ]); ?>


</div>
</div>
</div>
