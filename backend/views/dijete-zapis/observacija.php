<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model common\models\DijeteZapis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dijete-zapis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($dnevnikZapis, 'zapis')->textInput()->label(Yii::t('app','Zapis'))?>

    <?= $form->field($model2, 'datum')->widget(DatePicker::className(), [
                    'convertFormat' => true,
                    // 'options' => ['placeholder' => 'Enter birth date ...'],
                    'pluginOptions' => [
                        'autoclose'=>true
                    ]
                ]); ?>


    <?= $form->field($model2, 'misljenje')->widget(\dosamigos\tinymce\TinyMce::class, ['options' => [
        'clientOptions'=>[

            'statusbar'=>false,
            'height'=>400,
            'language' => 'hr_HR',
            // 'menubar' => 'file edit view insert format tools table tc help',


            // Ovo je za more info
            //  'valid_elements'=>'h1|h2|p|br|table|td|tr|th|img|b|strong|ul|ol|li',
            'toolbar' => ' bold | numlist bullist alignleft alignright | h1 h2 formatselect| image | table | insertfile |  responsivefilemanager',
            'menubar'=>'',
            // 'toolbar' => 'bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
            // 'templates' => [
            //     ['title' => 'New Table', 'description' => 'creates a new table', 'content' => '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'],
            //     ['title' => 'Starting my story', 'description' => 'A cure for writers block', 'content' => 'Once upon a time...'],
            //     ['title' => 'New list with dates', 'description' => 'New List with dates', 'content' => '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>']
            // ],
            'paste_word_valid_elements'=>'h1,h2,p,br,table,td,tr,th,img,b,strong,ul,ol,li',
            'paste_webkit_styles'=>'none',
            'paste_data_images' => true,
            'paste_enable_default_filters' => true,
            'paste_filter_drop' => true,
            'paste_remove_styles'=> true,
            'paste_preprocess'=> new JsExpression('function(plugin, args) {
    //console.log(args.content);
    var divElement = document.createElement("div");
        divElement.innerHTML = args.content;
        
        // loop through ALL DOM elements insidie the divElement
        var elements = divElement.getElementsByTagName("*");
        for (var i = 0; i < elements.length; i++) {
          
            elements[i].removeAttribute("style");
            elements[i].removeAttribute("color");
            elements[i].removeAttribute("class");
            elements[i].removeAttribute("align");
            elements[i].removeAttribute("href");
          
        }
        console.log(divElement.innerHTML);
        args.content=divElement.innerHTML;
  }'),
            'external_filemanager_path'=>'/admin/js/responsive_filemanager/filemanager/',
            'filemanager_title'=>"Responsive Filemanager" ,
            'external_plugins'=> [ "filemanager" => "/admin/js/responsive_filemanager/filemanager/plugin.min.js"],
            'inline_styles'=> false,
            'image_advtab'=> true,
            'plugins' => [
                "advlist -autolink lists -link charmap print preview anchor image  directionality",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste pastetext responsivefilemanager"
                //     ],
            ],],]

    ])->label(Yii::t('app','Mišljenje')) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
