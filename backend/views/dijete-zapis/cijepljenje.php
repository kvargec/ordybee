<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model common\models\DijeteZapis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dijete-zapis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($dnevnikZapis, 'zapis')->textInput()->label(Yii::t('app','Zapis'))?>

    <?= $form->field($model2, 'datum')->widget(DatePicker::className(), [
        'convertFormat' => true,
        // 'options' => ['placeholder' => 'Enter birth date ...'],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]); ?>


    <?= $form->field($model2, 'cjepivo')->textInput()->label(Yii::t('app','Cjepivo')) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
