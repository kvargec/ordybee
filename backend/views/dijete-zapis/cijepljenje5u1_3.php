<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php
$form = ActiveForm::begin(); ?>
<?= $form->field($dnevnikZapis, 'zapis')->hiddenInput(['value' => 'Cijepljenje 5u1'])->label(false); ?>

<table>
    <thead>
        <th>I.</th>
        <th>II.</th>
        <th>III.</th>
    </thead>
    <tbody>
        <tr>
            <td>
                <?= $form->field($model2, 'bcg_1')->widget(DatePicker::classname(), [
                    'name' => 'bcg',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ],
                ]) ?>
            </td>
            <td>
                <?= $form->field($model2, 'bcg_2')->widget(DatePicker::classname(), [
                    'name' => 'bcg',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ],
                ]) ?>
            </td>
            <td>
                <?= $form->field($model2, 'bcg_3')->widget(DatePicker::classname(), [
                    'name' => 'bcg',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ],
                ]) ?>
            </td>
        </tr>
        <tr>
            <td><?= $form->field($model2, 'diteper_1')->widget(DatePicker::classname(), [
                    'name' => 'diteper',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
            <td><?= $form->field($model2, 'diteper_2')->widget(DatePicker::classname(), [
                    'name' => 'diteper',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
            <td><?= $form->field($model2, 'diteper_3')->widget(DatePicker::classname(), [
                    'name' => 'diteper',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
        </tr>
        <tr>
            <td><?= $form->field($model2, 'polio_1')->widget(DatePicker::classname(), [
                    'name' => 'polio',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
            <td><?= $form->field($model2, 'polio_2')->widget(DatePicker::classname(), [
                    'name' => 'polio',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
            <td><?= $form->field($model2, 'polio_3')->widget(DatePicker::classname(), [
                    'name' => 'polio',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
        </tr>
        <tr>
            <td><?= $form->field($model2, 'moparu_1')->widget(DatePicker::classname(), [
                    'name' => 'moparu',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
            <td><?= $form->field($model2, 'moparu_2')->widget(DatePicker::classname(), [
                    'name' => 'moparu',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
            <td><?= $form->field($model2, 'moparu_3')->widget(DatePicker::classname(), [
                    'name' => 'moparu',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
        </tr>
        <tr>
            <td><?= $form->field($model2, 'hib_1')->widget(DatePicker::classname(), [
                    'name' => 'hib',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
            <td><?= $form->field($model2, 'hib_2')->widget(DatePicker::classname(), [
                    'name' => 'hib',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
            <td><?= $form->field($model2, 'hib_3')->widget(DatePicker::classname(), [
                    'name' => 'hib',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]) ?>
            </td>
        </tr>
    </tbody>
</table>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end();

$js = <<<JS
$("#dynamicmodel-bcg_1").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    $("input[id^=dynamicmodel-diteper_1").val(date);
    $("input[id^=dynamicmodel-polio_1").val(date);
    $("input[id^=dynamicmodel-moparu_1").val(date);
    $("input[id^=dynamicmodel-hib_1").val(date);
});
$("#dynamicmodel-bcg_2").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    $("input[id^=dynamicmodel-diteper_2").val(date);
    $("input[id^=dynamicmodel-polio_2").val(date);
    $("input[id^=dynamicmodel-moparu_2").val(date);
    $("input[id^=dynamicmodel-hib_2").val(date);
});
$("#dynamicmodel-bcg_3").on("change", function(e) {
    e.preventDefault();
    var date = $(this).val();
    $("input[id^=dynamicmodel-diteper_3").val(date);
    $("input[id^=dynamicmodel-polio_3").val(date);
    $("input[id^=dynamicmodel-moparu_3").val(date);
    $("input[id^=dynamicmodel-hib_3").val(date);
});
JS;
$this->registerJs($js);
?>