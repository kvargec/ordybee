<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model common\models\DijeteZapis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dijete-zapis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($dnevnikZapis, 'zapis')->textInput()->label(Yii::t('app', 'Zapis')) ?>

    <!-- <?= $form->field($model2, 'datum')->widget(DatePicker::className(), [
                'convertFormat' => true,
                // 'options' => ['placeholder' => 'Enter birth date ...'],
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]); ?> -->
    <style>
        .col-lg-4 .form-group,
        .col-lg-4 .form-group label {
            margin: 5px 0;
        }

        hr {
            margin-top: 30px;
            border-top: 2px solid rgba(0, 61, 78, .5);
            width: 30%;
        }

        h4 {
            text-align: center
        }

        .row,
        .flex {
            display: flex;
        }

        .align-self-center {
            align-self: center;
        }
    </style>
    <hr>
    <h4><?= Yii::t('app', 'SAMOSTALNOST') ?></h4>
    <div class="row">

        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>Samostalnost</h6>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'samostalnost1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'samostalnost2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'samostalnost5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <hr>
    <h4><?= Yii::t('app', 'TJELESNI I PSIHOMOTORNI  RAZVOJ') ?></h4>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>Grube motoričke vještine</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'motoricke1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'motoricke2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'motoricke5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>Fine motoričke vještine</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'motorickef1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'motorickef2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'motorickef5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <hr>
    <h4><?= Yii::t('app', 'SOCIOEMOCIONALNI RAZVOJ') ?></h4>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>DRUŠTVENE VJEŠTINE</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'drustvene1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'drustvene2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'drustvene5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>Rješavanje sukoba</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'sukob1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'sukob2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'sukob5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>Regulacija emocija</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'emocije1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'emocije2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'emocije5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>Slika o sebi <small>(samopouzdanje)</small></h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'slika1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'slika2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'slika5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <hr>
    <h4><?= Yii::t('app', 'SPOZNAJNI RAZVOJ') ?></h4>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>IGRA, INTERESI</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'igra1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'igra2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'igra5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6><small>RAZUMIJEVANJE</small> NOVIH POJMOVA</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'pojmovi1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'pojmovi2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'pojmovi5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6><small>RAZUMIJEVANJE</small> UZROČNO-POSLJEDIČNIH VEZA I ODNOSA</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'veze1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'veze2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'veze5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6><small>KONCENTRACIJA,</small> PAMĆENJE</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'pamcenje1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'pamcenje2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'pamcenje5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <hr>
    <h4><?= Yii::t('app', 'GOVOR, KOMUNIKACIJA, IZRAŽAVANJE, STVARANJE') ?></h4>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>GOVOR</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'govor1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'govor2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'govor5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6><small>KOMUNIKACIJSKE VJEŠTINE –</small> UPORABA JEZIKA</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'jezik1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'jezik2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'jezik5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>LIKOVNE, GLAZBENE, SCENSKE AKTIVNOSTI</h6>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'likovne1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'likovne2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'likovne5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <hr>
    <h4><?= Yii::t('app', 'BOJA') ?></h4>
    <div class="row">

        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>BOJA <small>LEUVENSKA SKALA</small></h6>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'boja1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'boja2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'boja5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <hr>
    <h4><?= Yii::t('app', 'POTICAJ') ?></h4>
    <div class="row">

        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>SPECIFIČNE VJEŠTINE ILI PODRUČJE RAZVOJA KOJE TREBA POTICATI</h6>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'spec1')->textarea()->label(Yii::t('app', 'rujan')) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model2, 'spec2')->textarea()->label(Yii::t('app', 'siječanj')) ?>
        </div>
        <div class="col-lg-4">

            <?= $form->field($model2, 'spec5')->textarea()->label(Yii::t('app', 'svibanj')) ?>
        </div>

    </div>
    <hr>
    <h4><?= Yii::t('app', 'BILJEŠKE') ?></h4>
    <div class="row">

        <div class="col-lg-2 flex h-100 align-self-center">
            <h6>BILJEŠKE I NAPOMENE</h6>
        </div>
        <div class="col-lg-10">

            <?= $form->field($model2, 'biljeske')->textarea()->label(Yii::t('app', 'Bilješke')) ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>