<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DijeteZapis */

$this->title = Yii::t('app', 'Update Dijete Zapis: {name}', [
    'name' => $dnevnikZapis->zapis,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dijete Zapis'), 'url' => ['index']];

$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dijete-zapis-create">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">

            <?= $this->render($view, [
                'model2' => $model2,
                'modelId' => $modelId,
                'dnevnikZapis' => $dnevnikZapis
            ]) ?>
        </div>
    </div>
</div>