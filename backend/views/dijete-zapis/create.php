<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DijeteZapis */

$this->title = Yii::t('app', 'Create Dijete Zapis');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dijete Zapis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dijete-zapis-create">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">

            <?= $this->render($view, [
                'model2' => $model2,
                'dnevnikZapis' => $dnevnikZapis,
                'zapisi_vrijednosti' => isset($zapisi_vrijednosti) ? $zapisi_vrijednosti : ''

            ]) ?>

        </div>
    </div>
</div>