<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\TvrtkaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tvrtka-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'naziv') ?>

    <?= $form->field($model, 'sifra') ?>

    <?= $form->field($model, 'oib') ?>

    <?= $form->field($model, 'adresa') ?>

    <?php // echo $form->field($model, 'mjesto') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'djelatnost') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
