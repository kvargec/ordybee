<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tvrtka */

$this->title = $model->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tvrtkas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tvrtka-view">
<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>

            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>


        </div>
        <div class="card-body detail">
    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'naziv',
            'sifra',
            'oib',
            'adresa',
            'mjesto0.naziv',
            'mjesto0.zupanija0.naziv',
            'djelatnost',
            'status0.naziv',
        ],
    ]) ?>

</div></div></div>
