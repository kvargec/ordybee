<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveField;

/* @var $this yii\web\View */
/* @var $model common\models\Tvrtka */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tvrtka-form">

    <?php $form = ActiveForm::begin(); ?>


            <?= $form->field($model, 'oib',[
                    'addon'=>[
                        'append'=>[
                                'content'=>Html::button(Yii::t('app', 'Pretraži po OIB-u'), [ 'id' => 'oib-button','class' => 'btn btn-primary btn-sm no-break']),

                            ],

                        'asButton' => true
            ]])->textInput()?>



    <div class="row">
    <div class="col-xs-8">
    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-4">
    <?= $form->field($model, 'sifra')->textInput(['maxlength' => true])->label('Šifra (MBS)') ?>
    </div>
    </div>
    <div class="row">
        <div class="col-xs-8">
    <?= $form->field($model, 'adresa')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-4">
    <?= $form->field($model, 'mjesto')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Mjesto::find()->where(['is not','sifra',null])->all(),
                   'id','naziv'
                ),
                'options' => ['placeholder' => Yii::t('app','Odaberite mjesto')]
            ]) ?>
        </div>
    </div>
    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\StatusOpce::find()->all(),
                    'id',
                    'naziv'
                ),
            ]) ?>

    <?= $form->field($model, 'djelatnost')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$url = \yii\helpers\Url::to(["/tvrtka/get-from-oib"]);
$js = <<<JS
$("#oib-button").on('click', function(e) {
    var oib = $('#tvrtka-oib');
    if (oib.val().length === 11){
        $.ajax({
        type: 'POST',
        url: '$url',
        data: {
            oib:oib.val()
        },
        success: function (data) {
         if(data.status == 'success'){
            $("#tvrtka-naziv").attr('value', data.naziv);
            $("#tvrtka-sifra").attr('value', data.sifra);
            $("#tvrtka-djelatnost").attr('value', data.djelatnost);
            $("#tvrtka-adresa").attr('value', data.adresa);
            $("#tvrtka-mjesto").val(data.mjesto).change();
         }else {
             alert(data.message);
         }
          
        }
        });
        
    }else {
        alert('OIB neispravan')
    } 
});
JS;
$this->registerJs($js);
?>