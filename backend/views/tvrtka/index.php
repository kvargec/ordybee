<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\TvrtkaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tvrtkas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrsta-zaposlenja-index">
<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
   


        <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
</div>
<div class="card-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'value' => 'naziv',
                'label' => Yii::t('app', 'Naziv'),
                'attribute' => 'naziv',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Naziv'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value' => 'sifra',
                'label' => Yii::t('app', 'Šifra'),
                'attribute' => 'sifra',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Šifra'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value' => 'oib',
                'label' => Yii::t('app', 'OIB'),
                'attribute' => 'oib',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'OIB'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value' => 'adresa',
                'label' => Yii::t('app', 'Adresa'),
                'attribute' => 'adresa',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Adresa'),
                    'class' => 'form-control'
                ]
            ],
            [
                    'attribute'=>'mjesto',
                    'filter'=>array_merge([''=>'Sva mjesta'],\yii\helpers\ArrayHelper::map(\common\models\Mjesto::find()->all(),'id','naziv')),
                'filterType' => GridView::FILTER_SELECT2,
                    'value'=>'mjesto0.naziv'
            ],
            [
                'attribute'=>'status',
                'filter'=>\yii\helpers\ArrayHelper::map(\common\models\StatusOpce::find()->all(),'id','naziv'),

                'value'=>'status0.naziv', 
                
            ],


            //'djelatnost',

            [
                'class' =>  'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url2 = \yii\helpers\Url::to(['/tvrtka/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'visibility')), [
                            'class' => 'material-icons'
                        ]), $url2);
                    },
                    'update' => function ($url, $model, $key) {
                        $url2 = \yii\helpers\Url::to(['/tvrtka/update', 'id' => $model->id]);
                        return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'edit')), [
                            'class' => 'material-icons'
                        ]), $url2);
                    },
                    'delete' => function ($url, $model, $key) {
                        $url2 = \yii\helpers\Url::to(['/tvrtka/delete', 'id' => $model->id]);
                        return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'delete')), [
                            'class' => 'material-icons red'
                        ]), $url2, [
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div></div></div>
