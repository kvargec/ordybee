<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VrstaPrograma */

$this->title = Yii::t('app', 'Create Vrsta Programa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vrsta Programas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrsta-programa-create">

<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            
        </div>
        <div class="card-body detail">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
