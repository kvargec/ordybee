<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\VrstaPrograma */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vrsta-programa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vrsta')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord) {
        echo $form->field($model, 'parent')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(
                \common\models\VrstaPrograma::find()->where(['=', 'parent', 0])->all(),
                'id',
                function ($v) {
                    return $v->vrsta;
                }
            ),
            'options' => ['placeholder' => 'Odaberi krovni program', 'value' => 0],
            'pluginOptions' => [
                'allowClear' => true,
                'required' => false
            ],
        ])->label('Ukoliko je novi program podvrsta drugog programa:');
    } else {
        echo $form->field($model, 'parent')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(
                \common\models\VrstaPrograma::find()->where(['!=', 'id', $model->id])->andWhere(['=', 'parent', 0])->all(),
                'id',
                function ($v) {
                    return $v->vrsta;
                }
            ),
            'options' => ['placeholder' => 'Odaberi krovni program', 'value' => 0],
            'pluginOptions' => [
                'allowClear' => true,
                'required' => false
            ],
        ])->label('Ukoliko je novi program podvrsta drugog programa:');
    } ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>