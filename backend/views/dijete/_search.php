<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\DijeteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dijete-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ime') ?>

    <?= $form->field($model, 'prezime') ?>

    <?= $form->field($model, 'spol') ?>

    <?= $form->field($model, 'dat_rod') ?>

    <?php // echo $form->field($model, 'oib') ?>

    <?php // echo $form->field($model, 'adresa') ?>

    <?php // echo $form->field($model, 'mjesto') ?>

    <?php // echo $form->field($model, 'prebivaliste') ?>

    <?php // echo $form->field($model, 'sestra') ?>

    <?php // echo $form->field($model, 'cekanje') ?>

    <?php // echo $form->field($model, 'god_cekanja') ?>

    <?php // echo $form->field($model, 'razvoj') ?>

    <?php // echo $form->field($model, 'vrsta_programa') ?>

    <?php // echo $form->field($model, 'teskoce') ?>

    <?php // echo $form->field($model, 'dijagnostika') ?>

    <?php // echo $form->field($model, 'druge_potrebe') ?>

    <?php // echo $form->field($model, 'posebne') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
