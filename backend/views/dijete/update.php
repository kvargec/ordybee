<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dijete */

$this->title = Yii::t('app', 'Update Dijete: {name}', [
    'name' => $model->ime,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dijetes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dijete-update">

    <div class="card">
        <div class="card-header card-header-danger">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
                'podvrsta' => $podvrsta
            ]) ?>

        </div>
    </div>
</div>