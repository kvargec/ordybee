<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="shop-create">
    <div class="card">
        <div class="card-header card-header-danger">

            <span class="card-title"><?= Yii::t('app', 'Podaci o roditeljima/skrbnicima') ?></span>
        </div>

        <div class="col s12 m12 l8">

            <div class="card-body">
                <h3><?= Yii::t('app', 'Podaci o majci djeteta') ?></h3>

                <div class="shop-form">

                    <?php $form = ActiveForm::begin(); ?>

                    <?php // $form->field($model, 'id')->textInput() 
                    ?>
                    <?= $form->field($modelRoditelj[0], '[0]id')->hiddenInput()->label(false) ?>
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]ime')->textInput(['required' => 'true'])->label(Yii::t('app', "Ime majke")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]prezime')->textInput(['required' => 'true'])->label(Yii::t('app', "Prezime majke")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= DatePicker::widget([
                            'model' => $modelRoditelj[0],
                            'attribute' => '[0]dat_rod',
                            'convertFormat' => true,
                            'options' => ['placeholder' => Yii::t('app', 'Datum rođenja majke')],
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose' => true
                            ]
                        ]);
                        ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]oib')->textInput(['required' => 'true'])->label(Yii::t('app', "OIB majke")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]adresa')->textInput(['required' => 'true'])->label(Yii::t('app', "Adresa stanovanja")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]mjesto')->textInput(['required' => 'true'])->label(Yii::t('app', "Grad / općina")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]prebivaliste')->textInput()->label(Yii::t('app', "Prijavljeno prebivalište")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]poslodavaca')->textInput(['required' => 'true'])->label(Yii::t('app', 'Naziv i adresa poslodavca')); ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]mobitel')->textInput(['required' => 'true'])->label(Yii::t('app', "Broj mobitela")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]email')->textInput()->label(Yii::t('app', "E-mail")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]zanimanje')->textInput(['required' => 'true'])->label(Yii::t('app', "Zanimanje i stručna sprema")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[0], '[0]radno')->textInput()->label(Yii::t('app', "Radno vrijeme")) ?>
                    </div>
                    <br />
                    <hr>
                    <h3><?= Yii::t('app', 'Podaci o ocu djeteta') ?></h3>

                    <?= $form->field($modelRoditelj[1], '[1]id')->hiddenInput()->label(false) ?>
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]ime')->textInput(['required' => 'true'])->label(Yii::t('app', "Ime oca")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]prezime')->textInput(['required' => 'true'])->label(Yii::t('app', "Prezime oca")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= DatePicker::widget([
                            'model' => $modelRoditelj[1],
                            'attribute' => '[1]dat_rod',
                            'convertFormat' => true,
                            'options' => ['placeholder' => Yii::t('app', 'Datum rođenja oca')],
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose' => true
                            ]
                        ]);
                        ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]oib')->textInput(['required' => 'true'])->label(Yii::t('app', "OIB oca")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]adresa')->textInput(['required' => 'true'])->label(Yii::t('app', "Adresa stanovanja")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]mjesto')->textInput(['required' => 'true'])->label(Yii::t('app', "Grad / općina")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]prebivaliste')->textInput()->label(Yii::t('app', "Prijavljeno prebivalište")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]poslodavaca')->textInput(['required' => 'true'])->label(Yii::t('app', 'Naziv i adresa poslodavca')); ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]mobitel')->textInput(['required' => 'true'])->label(Yii::t('app', "Broj mobitela")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]email')->textInput()->label(Yii::t('app', "E-mail")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]zanimanje')->textInput(['required' => 'true'])->label(Yii::t('app', "Zanimanje i stručna sprema")) ?>
                    </div>
                    <br />
                    <div class="input-field">
                        <?= $form->field($modelRoditelj[1], '[1]radno')->textInput()->label(Yii::t('app', "Radno vrijeme")) ?>
                    </div>
                    <br />
                    <br />
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Dalje'), ['class' => $modelRoditelj[0]->isNewRecord ? 'btn btn-success right' : 'btn btn-primary right']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>


            </div>
        </div>
    </div>

</div>
</div>
</div>