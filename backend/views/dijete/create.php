<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dijete */

$this->title = Yii::t('app', 'Create Dijete');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dijetes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dijete-create">
    <div class="card">
        <div class="card-header card-header-danger">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('<span class="material-icons">print</span>', ['dijete/pdf-print', 'id' => $model->id], ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
        </div>
        <div class="card-body detail">


            <?= $this->render('_form', [
                'model' => $model,
                'podvrsta' => $podvrsta
            ]) ?>

        </div>
    </div>
</div>