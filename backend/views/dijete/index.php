<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
use yii\helpers\Url;
use common\models\Skupina;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DijeteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dijetes');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dijete-index">
    <div class="card">
        <div class="card-header card-header-danger">
            <span class="card-title"><?= Html::encode($this->title) ?></span class="card-title">
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>

        <div class="card-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                // ['class' => 'yii\grid\SerialColumn'],
                //'id',
                [
                    'value' => 'ime',
                    'label' => Yii::t('app', 'Ime djeteta'),
                    'attribute' => 'ime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime djeteta'),
                        'title' => Yii::t('app', 'Ime djeteta'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'prezime',
                    'label' => Yii::t('app', 'Prezime djeteta'),
                    'attribute' => 'prezime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Prezime djeteta'),
                        'title' => Yii::t('app', 'Prezime djeteta'),
                        'class' => 'form-control'
                    ]

                ],
                [
                    'value' => 'spol',
                    'label' => Yii::t('app', 'Spol djeteta'),
                    'attribute' => 'spol',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Spol djeteta'),
                        'title' => Yii::t('app', 'Spol djeteta'),
                        'class' => 'form-control'
                    ]

                ],
                [
                    'value' => 'dat_rod',
                    'label' => Yii::t('app', 'Datum rođenja'),
                    'attribute' => 'dat_rod',
                    'format' => ['date', 'php:d.m.Y'],
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Datum rođenja'),
                        'title' => Yii::t('app', 'Datum rođenja'),
                        'class' => 'form-control'
                    ]

                ],
                [
                    'value' => 'oib',
                    'label' => Yii::t('app', 'OIB'),
                    'attribute' => 'oib',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'OIB'),
                        'title' => Yii::t('app', 'OIB'),
                        'class' => 'form-control'
                    ]

                ],
                [
                    'value' => 'adresa',
                    'label' => Yii::t('app', 'Adresa'),
                    'attribute' => 'adresa',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Adresa'),
                        'title' => Yii::t('app', 'Adresa'),
                        'class' => 'form-control'
                    ]

                ],
                [
                    'value' => 'mjesto',
                    'label' => Yii::t('app', 'Mjesto'),
                    'attribute' => 'mjesto',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Mjesto'),
                        'title' => Yii::t('app', 'Mjesto'),
                        'class' => 'form-control'
                    ]

                ],
                [
                    'value' => 'skupina0.naziv',
                    'label' => Yii::t('app', 'Skupina'),
                    'attribute' => 'skupina',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Naziv skupine'),
                        'title' => Yii::t('app', 'Skupina'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => function ($data) {
                        return $data->covidPotvrda;
                    },
                    'attribute' => 'covid_potvrda',
                    'label' => Yii::t('app', 'COVID Potvrda'),
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'COVID Potvrda'),
                        'title' => Yii::t('app', 'COVID Potvrda'),
                        'class' => 'form-control'
                    ],
                ],
                [
                    'label' => Yii::t('app', 'Status'),
                    'attribute' => 'status',
                    'value' => 'status0.status',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Status'),
                        'title' => Yii::t('app', 'Status'),
                        'class' => 'form-control'
                    ],
                    'filter' => Arrayhelper::map(\common\models\StatusDjeteta::find()->asArray()->all(), 'id', 'status'),
                ],
                //'prebivaliste',
                //'sestra',
                //'cekanje',
                //'god_cekanja',
                //'razvoj',
                //'vrsta_programa',
                //'teskoce',
                //'dijagnostika',
                //'druge_potrebe',
                //'posebne',
                //'create_at',
                //'update_at',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}{roditelji}',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['dijete/view', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pogledaj'),
                            ]);
                        },
                        'update' => function ($url, $data) {
                            $url2 = \Yii::$app->urlManager->createUrl(['dijete/update', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">create</span>', $url2, [
                                'title' => Yii::t('app', 'Ažuriraj'),
                            ]);
                        },
                        'roditelji' => function ($url, $data) use ($godina) {
                            $skupina = Skupina::find()->where(['naziv' => $data->skupina0->naziv])->andWhere(['ped_godina' => $godina->id])->one();
                            $url2 = \Yii::$app->urlManager->createUrl(['dijete/roditelji', 'id' => $data->id, 'skupina' => $skupina->id]);
                            return Html::a('<i class="material-icons">people</i>', $url2, [
                                'title' => Yii::t('app', 'Roditelji'),
                                'class' => 'text-left  hblue'
                            ]);
                        },
                        //  'delete' => function ($url, $data) {
                        //     $url2 = \Yii::$app->urlManager->createUrl(['dijete/update', 'id' => $data->id]);
                        //     return Html::a('<span class="material-icons">delete</span>', $url2, [
                        //         'title' => Yii::t('app', 'Obriši'),
                        //     ]);
                        // },

                    ],
                ],
            ];
            ?>
            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                    ExportMenu::FORMAT_CSV => null,
                ],
                'filename' => 'popisDjece',
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/dijete/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" . GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
            ]); ?>

        </div>
    </div>
</div>