<?php

use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Dijete */

$this->title = $model->ime . ' ' . $model->prezime;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dijetes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="dijete-view">

    <div class="card">
        <div class="card-header card-header-danger">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('<span class="material-icons">print</span>', ['dijete/pdf-print', 'id' => $model->id], ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
        </div>
        <div class="card-body detail">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'ime',
                    'prezime',
                    'spol',
                    'dat_rod',
                    'oib',
                    'adresa',
                    'mjesto',
                    'prebivaliste',
                    'sestra',
                    'cekanje',
                    'god_cekanja',
                    'razvoj',
                    'vrsta_programa',
                    [
                        'label' => Yii::t('app', 'Skupina'),
                        'attribute' => 'skupina0.naziv'
                    ],
                    'teskoce',
                    'dijagnostika',
                    'druge_potrebe',
                    'posebne',
                    'create_at',
                    'update_at',
                ],
            ]) ?>
            <h3><?= Yii::t('app', 'Roditelji') ?></h3>
            <?= \kartik\grid\GridView::widget([
                'dataProvider' => $roditelji,
                'showPageSummary' => false,
                'columns' => [
                    'ime',
                    'prezime',
                    'spol',
                    'dat_rod',
                    'oib',
                    'adresa',
                    'mjesto',
                    'prebivaliste',
                    'radno',
                    'zanimanje',
                    'poslodavca',
                    'mobitel',
                    'email'
                ]
            ]) ?>

            <h3><?= Yii::t('app', 'Kartoni zdravstvene službe') ?></h3>
            <?= GridView::widget([
                'dataProvider' => $zdravstvena,
                //'filterModel' => $searchModel,
                'columns' => [


                    'zapis0.zapis',
                    [
                        'header' => 'Nalaz',
                        'format' => 'html',
                        'value' => function ($data) {
                            $text = '';
                            if (is_string($data->dodatno)) {
                                $tmp =  json_decode($data->dodatno);
                                foreach ($tmp as $key => $value) {
                                    if (is_array($value)) {
                                        foreach ($value as $v) {
                                            $text .= $key . ': ' .  $v . '<br>';
                                        }
                                    } else {
                                        $text .= $key . ': ' .  $value . '<br>';
                                    }
                                }
                            } else {
                                $tmp = $data->dodatno;
                                foreach ($tmp as $key => $value) {
                                    if (is_array($value)) {
                                        foreach ($value as $v) {
                                            $text .= $key . ': ' .  $v . '<br>';
                                        }
                                    } else {
                                        $text .= $key . ': ' .  $value . '<br>';
                                    }
                                }
                            }
                            return $text;
                        }
                    ],

                    [
                        'class' =>  'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/view', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('visibility'), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'update' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/update', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('create'), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'delete' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/delete', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('delete'), [
                                    'class' => 'material-icons red'
                                ]), $url2, [
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ],
                    [
                        'format' => 'raw',
                        'header' => 'Nalaz',
                        'value' => function ($data) {
                            if ($data->zapis0->vrsta0->naziv == 'nalaz')
                                return Html::a('<i class="fa fa-file-pdf-o" style="font-size:24px"></i>', \yii\helpers\Url::to(['/dijete-zapis/zapispdf', 'id' => $data->id]));
                            else
                                return;
                        },

                    ]
                ],
            ]); ?>

            <h3><?= Yii::t('app', 'Kartoni logopeda/rehabilitatora') ?></h3>
            <?= GridView::widget([
                'dataProvider' => $logoped,
                //'filterModel' => $searchModel,
                'columns' => [


                    'zapis0.zapis',
                    [
                        'format' => 'html',
                        'value' => function ($data) {
                            $tmp =  json_decode($data->dodatno);
                            $text = '';
                            foreach ($tmp as $key => $value) {
                                if (is_array($value)) {
                                    foreach ($value as $v) {
                                        $text .= $key . ': ' .  $v . '<br>';
                                    }
                                } else {
                                    $text .= $key . ': ' .  $value . '<br>';
                                }
                            }
                            return $text;
                        }
                    ],

                    [
                        'class' =>  'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/view', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('visibility'), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'update' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/update', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('create'), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'delete' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/delete', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('delete'), [
                                    'class' => 'material-icons red'
                                ]), $url2, [
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ],
                    [
                        'format' => 'raw',
                        'header' => 'Nalaz',
                        'value' => function ($data) {
                            if ($data->zapis0->vrsta0->naziv == 'nalaz')
                                return Html::a('<i class="fa fa-file-pdf-o" style="font-size:24px"></i>', \yii\helpers\Url::to(['/dijete-zapis/zapispdf', 'id' => $data->id]));
                            else
                                return;
                        },

                    ]
                ],
            ]); ?>

            <h3><?= Yii::t('app', 'Kartoni psihologa') ?></h3>
            <?= GridView::widget([
                'dataProvider' => $psiholog,
                //'filterModel' => $searchModel,
                'columns' => [


                    'zapis0.zapis',
                    [
                        'format' => 'html',
                        'value' => function ($data) {
                            $tmp =  json_decode($data->dodatno);
                            $text = '';
                            foreach ($tmp as $key => $value) {
                                if (is_array($value)) {
                                    foreach ($value as $v) {
                                        $text .= $key . ': ' .  $v . '<br>';
                                    }
                                } else {
                                    $text .= $key . ': ' .  $value . '<br>';
                                }
                            }
                            return $text;
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/view', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('visibility'), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'update' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/update', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('create'), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'delete' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/delete', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('delete'), [
                                    'class' => 'material-icons red'
                                ]), $url2, [
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ],
                    [
                        'format' => 'raw',
                        'header' => 'Nalaz',
                        'value' => function ($data) {
                            if ($data->zapis0->vrsta0->naziv == 'nalaz')
                                return Html::a('<i class="fa fa-file-pdf-o" style="font-size:24px"></i>', \yii\helpers\Url::to(['/dijete-zapis/zapispdf', 'id' => $data->id]));
                            else
                                return;
                        },

                    ]
                ],
            ]); ?>

            <h3><?= Yii::t('app', 'Kartoni pedagoga') ?></h3>
            <?= GridView::widget([
                'dataProvider' => $pedagog,
                //'filterModel' => $searchModel,
                'columns' => [


                    'zapis0.zapis',
                    [
                        'format' => 'html',
                        'value' => function ($data) {
                            $tmp =  json_decode($data->dodatno);
                            $text = '';
                            foreach ($tmp as $key => $value) {
                                if (is_array($value)) {
                                    foreach ($value as $v) {
                                        $text .= $key . ': ' .  $v . '<br>';
                                    }
                                } else {
                                    $text .= $key . ': ' .  $value . '<br>';
                                }
                            }
                            return $text;
                        }
                    ],

                    [
                        'class' =>  'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/view', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('visibility'), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'update' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/update', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('create'), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'delete' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/dijete-zapis/delete', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode('delete'), [
                                    'class' => 'material-icons red'
                                ]), $url2, [
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ],
                    [
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->zapis0->vrsta0->naziv == 'nalaz')
                                return Html::a('<i class="fa fa-file-pdf-o" style="font-size:24px"></i>', \yii\helpers\Url::to(['/dijete-zapis/zapispdf', 'id' => $data->id]));
                            else
                                return;
                        },

                    ]
                ],
            ]); ?>

        </div>
    </div>
</div>