<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use kartik\widgets\DepDrop;
/* @var $this yii\web\View */
/* @var $model common\models\Dijete */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dijete-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prezime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'spol')->radioList([
        'M' => Yii::t('app', 'muški'),
        'Ž' => Yii::t('app', 'ženski')
    ])->label(Yii::t('app', 'Spol')); ?>

    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'dat_rod',
        'convertFormat' => true,
        'options' => ['placeholder' => Yii::t('app', 'Datum rođenja djeteta')],
        'pluginOptions' => [
            'todayHighlight' => true,
            'autoclose' => true
        ]
    ]);
    ?>
    <?= Yii::t('app', 'Datum upisa'); ?>
    <br />
    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'dat_upisa',
        'convertFormat' => true,
        'options' => ['placeholder' => Yii::t('app', 'Datum upisa')],
        'value' => empty($model->dat_upisa) ? null : $model->dat_upisa,
        'pluginOptions' => [
            'todayHighlight' => true,
            'autoclose' => true
        ]
    ])
    ?>
    <?= Yii::t('app', 'Datum ispisa') ?><br />
    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'dat_ispisa',
        'convertFormat' => true,

        'options' => ['placeholder' => Yii::t('app', 'Datum ispisa')],
        //'value'=> isset($model->dat_ispisa) ? '' : $model->dat_ispisa,
        'pluginOptions' => [
            'todayHighlight' => true,

            'autoclose' => true
        ]
    ]);
    ?>
    <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map(\common\models\StatusDjeteta::find()->all(), 'id', 'status')) ?>

    <?php
    if (!$podvrsta) {
        echo $form->field($model, 'vrsta_programa')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(\common\models\VrstaPrograma::find()->where(['or', ['parent' => null], ['parent' => 0]])->all(), 'id', 'vrsta'),
            'options' => ['placeholder' => Yii::t('app', 'Odaberi vrstu programa...'), 'id' => 'vrsta_programa'],
        ])->label(Yii::t('app', 'Vrsta programa'));;
    } else {
        echo $form->field($model, 'vrstadepdrop')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(\common\models\VrstaPrograma::find()->where(['or', ['parent' => null], ['parent' => 0]])->all(), 'id', 'vrsta'),
            'options' => ['placeholder' => Yii::t('app', 'Odaberi vrstu krovnog programa...'), 'id' => 'vrstadepdrop'],
        ])->label(Yii::t('app', 'Krovni program'));

        echo $form->field($model, 'vrsta_programa')->widget(DepDrop::classname(), [
            'type' => DepDrop::TYPE_SELECT2,
            'options' => ['id' => 'vrsta_programa'],
            'select2Options' => [
                'pluginOptions' => [
                    'placeholder' => Yii::t('app', 'Odaberite...'),
                    'allowClear' => true,
                    'multiple' => false,
                ]
            ],
            'pluginOptions' => [
                'depends' => ['vrstadepdrop'],
                'placeholder' => Yii::t('app', 'Odaberite...'),
                'initialize' => true,
                'loadingText' => '',
                'url' => Url::to(['/zahtjev/vrsta-potprograma'])
            ]
        ])->label(Yii::t('app', "Potprogram"));
    }
    ?>

    <?= $form->field($model, 'oib')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adresa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mjesto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prebivaliste')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sestra')->radioList([
        'D' => Yii::t('app', 'Da'),
        'N' => Yii::t('app', 'Ne')
    ])->label(Yii::t('app', 'Ima li brata/sestru već upisane u dječji vrtić')); ?>

    <?= $form->field($model, 'cekanje')->radioList([
        'D' => Yii::t('app', 'Da'),
        'N' => Yii::t('app', 'Ne')
    ])->label(Yii::t('app', 'Je li dijete bilo na listi čekanja u dječjem vrtiću')); ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>