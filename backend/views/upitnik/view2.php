<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Upitnik */

$this->title = $model->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upitniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="upitnik-view">
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>


        </div>
        <div class="card-body  detail">

            <div class="row">
                <div class="col-lg-6">



                    <?php

                    //print("<pre>".print_r($odgovori,true)."</pre>");die();

                    echo Html::beginForm(['spremi'], 'post');
                    foreach ($svaPitanja as $pitanje) { ?>
                        <?php
                        echo '<div class="form-group">';
                        $odgovori = json_decode($pitanje->odgovori);

                        echo '<label class="control-label h4">' . $pitanje->pitanje . '</label>';
                        switch ($pitanje->vrsta_pitanja) {
                            case 1:

                                echo '<div role="radiogroup">';
                                foreach ($odgovori as $index => $odgovor) {

                                    echo '<label>';
                                    echo '<input type="radio"  name="pitanje_' . $pitanje->id . '" value="' . $index . '"> ' . $odgovor;
                                    echo '</label>';
                                }
                                echo '</div>';
                                break;
                            case 2:

                                echo '<div class="form-group">';
                                $i = 1;

                                foreach ($odgovori as $index => $odgovor) {
                                    echo '<label class="control-label mr-1">';
                                    echo '<input type="checkbox" name="pitanje_' . $pitanje->id . '[]" value="' . $index . '" > ' . $odgovor;
                                    echo '</label>';
                                    //if($i%3==0){
                                    echo '<br>';
                                    // }
                                    $i++;
                                }

                                echo '</div>';
                                break;
                            case 3:

                                echo '<input type="text" class="form-control" name="pitanje_' . $pitanje->id . '" > ';
                                break;
                            case 4:

                                echo '<textarea name="pitanje_' . $pitanje->id . '" class="form-control"></textarea>';
                                break;
                            case 5:
                                echo '<input type="text" class="form-control dates" name="pitanje_' . $pitanje->id . '" > ';
                                break;
                        }
                        echo '</div> <br />';
                        ?>
                    <?php }
                    echo '<button type="submit" class="btn btn-success right"><span  class="material-icons">check</span></button>';
                    echo Html::endForm();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>