<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\UpitnikSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Upitniks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="upitnik-index">
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>

        </div>
        <div class="card-body">
            <h3>Upitnici koje trebate ispuniti:</h3>
            <ul class="text-left">
                <?php if (!empty($upitnici)) {
                    foreach ($upitnici as $key => $value) { ?>
                        <li>
                            <a href="<?php echo \yii\helpers\Url::to(['upitnik/view2']) ?>&id=<?php echo $key; ?>">
                            <?php echo $value; ?> 
                        </a>
                        </li>
                <?php }
                } else {
                    echo '<h4> nema upitnika </h4>';
                } ?>
            </ul>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    [
                        'value' => 'naziv',
                        'label' => Yii::t('app', 'Naziv'),
                        'attribute' => 'naziv',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Naziv'),
                            'class' => 'form-control'
                        ]
                    ],
                    'opis:html',
                    [
                        'label' => Yii::t('app', 'Ped. godina'),
                        'attribute' => 'ped_godina',
                        'value' => function ($data) {
                            return $data->pedGodina->getNaziv();
                        },
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filter' => ArrayHelper::map(\common\models\PedagoskaGodina::find()->all(), 'id', function ($v) {
                            return $v->getNaziv();
                        })
                    ],
                    [
                        'value' => 'date_publish',
                        'label' => Yii::t('app', 'Datum početka'),
                        'attribute' => 'date_publish',
                        'format' => ['date', 'php:d.m.Y'],
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Datum početka'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'value' => 'date_end',
                        'label' => Yii::t('app', 'Datum završetka'),
                        'attribute' => 'date_end',
                        'format' => ['date', 'php:d.m.Y'],
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Datum završetka'),
                            'class' => 'form-control'
                        ]
                    ],
                    //'created_at',
                    //'creator',
                    //'status',
                    //'upitnik_for',
                    //'is_required:boolean',

                    [
                        'class' => 'yii\grid\ActionColumn',

                        // 'visible' => Yii::$app->user->can('admin'),
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/upitnik/view', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'visibility')), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'update' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/upitnik/update', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'edit')), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'delete' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/upitnik/delete', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'delete')), [
                                    'class' => 'material-icons red'
                                ]), $url2, [
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>