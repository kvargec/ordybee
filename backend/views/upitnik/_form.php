<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model common\models\Upitnik */
/* @var $form yii\widgets\ActiveForm */
$pedGodina=\common\models\PedagoskaGodina::find()->where(['aktivna'=>true])->one();
$pedGodina=$pedGodina->id;
$userID=Yii::$app->user->id;
?>

<div class="upitnik-form">


    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-lg-8">

    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-1">
        <label class="control-label"></label>
    <?= $form->field($model, 'is_required')->checkbox(['label'=>Yii::t('app','Obavezna'),'class'=>'form-check-input'])?>
</div>
    <div class="col-lg-3">
    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(
            \common\models\StatusOpce::find()->all(),
            'id',
            'naziv'
        ),
    ]) ?>
    </div>
</div>
    <div class="row">
    <div class="col-lg-12">
        <?php
    echo $form->field($model, 'upitnik_for')->widget(Select2::classname(), [
        'data' => array_merge(['roditelji' => Yii::t('app','Roditelji'),'djeca' => Yii::t('app','Pojedino dijete'),'skupine'=>ArrayHelper::map(\common\models\Skupina::find()->where(['ped_godina'=>$pedGodina])->all(),'id','naziv'), 'zaposlenici' => Yii::t('app','Zaposlenici'),'pojedini'=>Yii::t('app','Pojedini zaposlenici')]),
        'options' => ['multiple'=>'true','placeholder' => Yii::t('app', 'Odaberi sve kojima će se anketa prikazivati ...')],
    ])->label(Yii::t('app', 'Grupa korisnika')); ?>

    </div>
    </div>
    <div class="row">
    <div class="col-lg-6">
        <?= DatePicker::widget([
            'model' => $model,
            'attribute' => 'date_publish',
            'convertFormat' => false,
            'options' => ['placeholder' => Yii::t('app','Datum objave')],
            'pluginOptions' => [
                'todayHighlight' => true,
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);?>
    </div>
    <div class="col-lg-6">
        <?= DatePicker::widget([
            'model' => $model,
            'attribute' => 'date_end',
            'convertFormat' => false,
            'options' => ['placeholder' => Yii::t('app','Datum završetka')],
            'pluginOptions' => [
                'todayHighlight' => true,
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);?>
    </div>
    </div>
    <?= $form->field($model, 'opis')->widget(\dosamigos\tinymce\TinyMce::class, ['options' => [
        'clientOptions'=>[

            'statusbar'=>false,
            'height'=>600,
            'language' => 'hr_HR',
            // 'menubar' => 'file edit view insert format tools table tc help',


            // Ovo je za more info
            //  'valid_elements'=>'h1|h2|p|br|table|td|tr|th|img|b|strong|ul|ol|li',
            'toolbar' => ' bold | numlist bullist alignleft alignright | h1 h2 formatselect| image | table | insertfile |  responsivefilemanager',
            'menubar'=>'',
            // 'toolbar' => 'bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
            // 'templates' => [
            //     ['title' => 'New Table', 'description' => 'creates a new table', 'content' => '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'],
            //     ['title' => 'Starting my story', 'description' => 'A cure for writers block', 'content' => 'Once upon a time...'],
            //     ['title' => 'New list with dates', 'description' => 'New List with dates', 'content' => '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>']
            // ],
            'paste_word_valid_elements'=>'h1,h2,p,br,table,td,tr,th,img,b,strong,ul,ol,li',
            'paste_webkit_styles'=>'none',
            'paste_data_images' => true,
            'paste_enable_default_filters' => true,
            'paste_filter_drop' => true,
            'paste_remove_styles'=> true,
            'paste_preprocess'=> new JsExpression('function(plugin, args) {
    //console.log(args.content);
    var divElement = document.createElement("div");
        divElement.innerHTML = args.content;
        
        // loop through ALL DOM elements insidie the divElement
        var elements = divElement.getElementsByTagName("*");
        for (var i = 0; i < elements.length; i++) {
          
            elements[i].removeAttribute("style");
            elements[i].removeAttribute("color");
            elements[i].removeAttribute("class");
            elements[i].removeAttribute("align");
            elements[i].removeAttribute("href");
          
        }
        console.log(divElement.innerHTML);
        args.content=divElement.innerHTML;
  }'),
            'external_filemanager_path'=>'/admin/js/responsive_filemanager/filemanager/',
            'filemanager_title'=>"Responsive Filemanager" ,
            'external_plugins'=> [ "filemanager" => "/admin/js/responsive_filemanager/filemanager/plugin.min.js"],
            'inline_styles'=> false,
            'image_advtab'=> true,
            'plugins' => [
                "advlist -autolink lists -link charmap print preview anchor image  directionality",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste pastetext responsivefilemanager"
                //     ],
            ],],]

    ])?>

    <?= $form->field($model, 'ped_godina')->hiddenInput(['value'=>$pedGodina])->label(false) ?>



    <?= $form->field($model, 'creator')->hiddenInput(['value'=>$userID])->label(false) ?>






    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


