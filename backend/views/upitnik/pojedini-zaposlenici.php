<?php

use common\models\UserRoditelj;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Upitnik */

$this->title = $zap->ime . ' ' . $zap->prezime . ' - ' . $upitnik->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upitnici'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="upitnik-view">
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>

        </div>
        <div class="card-body detail">
            <?= GridView::widget([
                'dataProvider' => $odgovori,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'pitanje0.pitanje',

                    [
                        'label' => 'Odgovori',
                        'format' => 'raw',
                        'value' => function ($data) {
                            switch ($data->pitanje0->vrsta_pitanja) {
                                case 1:
                                    $tt = json_decode($data->pitanje0->odgovori, true);
                                    return '<b>' . $tt[json_decode($data->value)->odgovori] . '</b>';
                                    break;
                                case 2:
                                    $sviOdg = array();
                                    foreach (json_decode($data->value)->odgovori as $odg) {
                                        $tt = json_decode($data->pitanje0->odgovori, true);
                                        $sviOdg[] = $tt[$odg];
                                    };
                                    return '<b>' . implode("<br />", $sviOdg) . '</b>';
                                    break;
                                case 3:
                                case 4:
                                    return '<b>' . json_decode($data->value)->odgovori . '</b>';
                                    break;
                                case 5:
                                    return '<b>' . json_decode($data->value)->odgovori . '</b>';
                                    break;
                            }
                        }
                    ],

                ],
            ]); ?>
        </div>
    </div>
</div>