<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Upitnik */

$this->title = Yii::t('app', 'Create Upitnik');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upitniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.field-upitnik-ped_godina,.field-upitnik-creator{
    margin-top:0;padding:0
    }
    @media (min-width:767px){
.field-upitnik-is_required{margin-top: 30px;}}</style>
<div class="upitnik-create">
    <div class="card">
        <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
        </div>
</div>
</div>
