<?php

use common\models\UserRoditelj;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use common\models\Zaposlenik;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Upitnik */

$this->title = $model->naziv;
$upitnik = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upitniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="upitnik-view">
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span><span class="material-icons">question_mark</span>', ['pitanje/create'], [
                'class' => 'btn btn-primary',

            ]) ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a(Yii::t('app', 'Duplicate'), ['duplicate', 'id' => $model->id], [
                'class' => 'btn btn-primary',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to duplicate this item?'),
                    'method' => 'post',
                ],
            ]) ?>

            <!--<? //= Html::a('<span class="material-icons">print</span>', ['upitnik/pdf-print', 'id' => $model->id, 'dijete'=>$dijete], ['class' => 'btn btn-primary', 'target' => '_blank']) //
                ?>-->
        </div>
        <div class="card-body  detail">


            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'naziv',
                    [
                        'attribute' => 'opis',
                        'format' => 'html'
                    ],
                    [
                        'label' => Yii::t('app', 'Datum početka'),
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->date_publish, 'php:j.n.Y.');
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Datum završetka'),
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->date_end, 'php:j.n.Y.');
                        }
                    ],

                    //    'creator',
                    //     'status0.naziv',
                    //    'upitnik_for',
                    //    'is_required:boolean',
                ],
            ]) ?>
            <span class="h3" style="color:#003d4e">Pitanja</span>
            <?= GridView::widget([
                'dataProvider' => $pitanjaUpitnika,
                'layout' => '{items}',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'pitanje',
                    'opis'
                ],
            ]) ?>
        </div>
    </div>
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Yii::t('app', 'Upitnik') ?></span>

        </div>
        <div class="card-body  detail">
            <?= GridView::widget([
                'dataProvider' => $upitnici,

                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => $label,
                        'value' => function ($data) {
                            $temp = UserRoditelj::find()->where(['user' => $data->user])->one();
                            $text = "";

                            $zaposlenik = Zaposlenik::find()->where(['user' => $data->user])->one();
                            if (isset($temp)) {
                                $text = $temp->roditelj0->dijete0->getFullName();
                            } else {
                                $text = $zaposlenik->ime . ' ' . $zaposlenik->prezime;
                            }
                            return $text;
                        },


                    ],
                    //'created_at',
                    //'creator',
                    //'status',
                    //'upitnik_for',
                    //'is_required:boolean',

                    [
                        'class' => 'yii\grid\ActionColumn',

                        // 'visible' => Yii::$app->user->can('admin'),
                        'template' => '{view}{print}',
                        'buttons' => [
                            'view' => function ($url, $model2, $key) use ($model) {
                                $temp = UserRoditelj::find()->where(['user' => $model2->user])->one();
                                $dijete = null;
                                $user = User::find()->where(['id' => $model2->user])->one();
                                if (isset($temp)) {
                                    $dijete = $temp->roditelj0->dijete;
                                    $url2 = \yii\helpers\Url::to(['/upitnik/pojedini', 'id' => $model->id, 'dijete' => $dijete]);
                                } else {
                                    $url2 = \yii\helpers\Url::to(['/upitnik/pojedini-zaposlenici', 'id' => $model->id, 'user' => $model2->user]);
                                }

                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'visibility')), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'print' => function ($url, $model2, $key) use ($model) {
                                $temp = UserRoditelj::find()->where(['user' => $model2->user])->one();
                                $dijete = null;
                                if (isset($temp)) {
                                    $dijete = $temp->roditelj0->dijete;
                                }
                                $url2 = \yii\helpers\Url::to(['/upitnik/pdf-print', 'id' => $model->id, 'dijete' => $dijete]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'download')), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>