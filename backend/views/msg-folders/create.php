<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MsgFolders */

$this->title = Yii::t('app', 'Create Msg Folders');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Msg Folders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="msg-folders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
