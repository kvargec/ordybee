<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\MjestoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mjestos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mjesto-index">
<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>

           

        </div>
        <div class="card-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            [
                'value' => 'id',
                'label' => Yii::t('app', 'Id'),
                'attribute' => 'id',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Id'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value' => 'naziv',
                'label' => Yii::t('app', 'Naziv'),
                'attribute' => 'naziv',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Naziv'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value' => 'zip',
                'label' => Yii::t('app', 'Poštanski broj'),
                'attribute' => 'zip',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Poštanski broj'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value' => 'zupanija0.naziv',
                'label' => Yii::t('app', 'Županija'),
                'attribute' => 'zupanija.naziv',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Županija'),
                    'class' => 'form-control'
                ]
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div></div></div>
