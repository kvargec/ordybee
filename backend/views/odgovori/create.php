<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Odgovori */

$this->title = Yii::t('app', 'Create Odgovori');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Odgovoris'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="odgovori-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
