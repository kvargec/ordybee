<?php

use yii\helpers\Html;
use kartik\builder\Form;

$this->title = Yii::t('app', 'Boduj Zahtjev: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zahtjevs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Boduj');
?>

<div class="zahtjev-boduj">

<h1><?= Html::encode($this->title) ?></h1>

<?php
echo Html::beginForm('', '', ['class'=>'form-horizontal']);
echo Form::widget([
    // formName is mandatory for non active forms
    // you can get all attributes in your controller 
    // using $_POST['bodovi']
    'formName'=>'bodovi',
    
    // default grid columns
    'columns'=>2,
    
    'attributes'=>$attributes,
]);
echo Html::endForm();
?>

</div>