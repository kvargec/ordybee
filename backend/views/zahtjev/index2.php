<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZahtjevSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zahtjevs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zahtjev-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'urbroj',
            'create_at',
            // 'update_at',
            // 'status0.status',

            // 'getDijeteDatRod',

            // [
            //     'attribute' => 'dijete0.dat_rod',
            //     'label'=>'Datum rođenja dijeteta',
            //     'value' => function ($model) {
            //         return $model->getDijeteDatRod();
            //     },
            // ],

            'dijete_dat_rod',
            // 'bodovi',
            // 'izravan_upis',
            // 'bodovi_ili_izravan_upis',
            [
                'attribute' => 'bodovi_izravan_upis',
                'label' => Yii::t('app','Bodovi/Izravan Upis'),
            ],

            // [
            //     'attribute' => 'bodovi',
            //     'value' => function ($model) {
            //         if($model->getIzravanUpis()) {
            //             return 'izravan upis';
            //         }

            //         return $model->getBodovi();
            //     },
            // ],
            // [
            //     'attribute' => 'izravan_upis',
            //     'value' => function ($model) {
            //         return $model->getIzravanUpis() ? 'Da' : 'Ne';
            //     },
            // ],

            // ['class' => 'yii\grid\ActionColumn',
            //     'template' => '{view}{update}',
            //     // 'visible' => Yii::$app->user->can('admin'),
            //     'buttons' => [
            //         'view' => function ($url, $data) {

            //             $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/view', 'id' => $data->id]);
            //             return Html::a('<span class="glyphicon glyphicon-search"></span>&nbsp;', $url2, [
            //                 'title' => Yii::t('app', 'Pregledaj zahtjev'),
            //             ]);
            //         },

            //         'update' => function ($url, $data) {

            //             $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/update', 'id' => $data->id]);
            //             return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url2, [
            //                 'title' => Yii::t('app', 'Uredi zahtjev'),
            //             ]);
            //         },

            //     ],
            // ],
        ],
    ]); ?>


</div>
