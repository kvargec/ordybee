<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Zahtjev */

$this->title = Yii::t('app', 'Update Zahtjev: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zahtjevs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="zahtjev-update">
    <div class="card">
        <div class="card-header card-header-danger">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-content">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>