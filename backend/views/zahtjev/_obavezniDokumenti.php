<?php

use yii\helpers\Html;
use kartik\builder\Form;
?>

<div class="zahtjev-obiljezi-predane-obavezne-dokumente" id="zahtjev-obiljezi-predane-obavezne-dokumente">

    <h2><?= Yii::t('app', 'Pribilježite predane dokumente') ?> </h2>

    <?php
        echo Html::beginForm(['obiljezi-predane-obavezne-dokumente', 'id' => $zahtjev->id], 'post', ['class'=>'form-horizontal']);
        echo Form::widget([
            // formName is mandatory for non active forms
            // you can get all attributes in your controller 
            // using $_POST['bodovi']
            'formName'=>'predaniObavezniDokumenti',
            
            // default grid columns
            // 'columns'=>1,
            
            'attributes'=>$obavezniDokumentiAttributes,
        ]);
        echo Html::endForm();
    ?>
</div>