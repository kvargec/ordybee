<?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\web\View;
use kartik\export\ExportMenu;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZahtjevSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zahtjevs');
$this->params['breadcrumbs'][] = $this->title;

$script = <<<JS

jQuery(document).ready(function($) {
    $(".send-email-form").submit(function(event) {
        event.preventDefault();
        var data = $(this).serializeArray();
        var url = $(this).attr('action');
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: data
        })
        .done(function(response) {
            if (response.data.success == true) {
                $.notify({
                icon: 'glyphicon glyphicon-ok-sign',
                message: response.data.message,
            },{
                    // settings
                    // type: 'danger',
                    delay: 1000,
                timer: 2,
                allow_dismiss: false,
                animate: {
                        enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
            });
            } else {
                alert(response.data.message);
            }
        })
        .fail(function() {
        });
    
    });
});

JS;
$this->registerJs($script, View::POS_END);
?>

<div class="zahtjev-index">
    <div class="card">
        <div class="card-header card-header-danger">
            <span class="card-title"><?= Html::encode($this->title) ?></span>


            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            $gridColumns = [
                // ['class' => 'yii\grid\SerialColumn'],
                [
                    'value' => 'dijete0.ime',
                    'label' => Yii::t('app', 'Ime djeteta'),
                    'attribute' => 'dijete_ime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime djeteta'),
                        'title' => Yii::t('app', 'Ime djeteta'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'dijete_ime'
                ],
                [
                    'value' => 'dijete0.prezime',
                    'label' => Yii::t('app', 'Prezime djeteta'),
                    'attribute' => 'dijete_prezime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Prezime djeteta'),
                        'title' => Yii::t('app', 'Prezime djeteta'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'dijete_prezime'

                ],
                [
                    'value' => 'urbroj',
                    'label' => Yii::t('app', 'Šifra djeteta'),
                    'attribute' => 'urbroj',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Šifra djeteta'),
                        'title' => Yii::t('app', 'Šifra djeteta'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'create_at',
                    'label' => Yii::t('app', 'Izrađen'),
                    'attribute' => 'create_at',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Izrađen'),
                        'title' => Yii::t('app', 'Izrađen'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'create_at'
                ],
                [
                    'label' => Yii::t('app', 'Ped. godina'),
                    'attribute' => 'pd_godina',
                    'value' => function ($data) {
                        return $data->pdGodina->getNaziv();
                    },
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filter' => ArrayHelper::map(\common\models\PedagoskaGodina::find()->all(), 'id', function ($v) {
                        return $v->getNaziv();
                    })
                ],
                /*[
            'header' => "Datum izrade i<br/>zadnjeg ažuriranja",
            'format' => 'raw',
            'value' => function ($data) {
                $create_at = $data->create_at;
                $update_at = $data->update_at;
                return "$create_at<br/>$update_at";
            }
        ],*/
                [
                    'label' => Yii::t('app', 'Status'),
                    'attribute' => 'status',
                    'value' => function ($data) {
                        return $data->status0->status;
                    },
                    //'format'=>'raw',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filter' => Arrayhelper::map(\common\models\Status::find()->asArray()->all(), 'id', 'status'),

                ],



                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['class' => 'text-center'],
                    'template' => '{view} {update} {download}{print}{send}',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/view', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pregledaj zahtjev'),
                                'class' => 'text-left  ',
                            ]);
                        },

                        'update' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/update', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">edit</span>', $url2, [
                                'title' => Yii::t('app', 'Uredi zahtjev'),
                                'class' => 'text-left  ',
                            ]);
                        },

                        'download' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/mojapotvrda', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">get_app</span>', $url2, [
                                'title' => Yii::t('app', 'Skini potvrdu'),
                                'class' => 'text-left  ',
                                'target' => '_blank',
                            ]);
                        },
                        'print' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/pdfzahtjev', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">print</span>', $url2, [
                                'title' => Yii::t('app', 'Skini potvrdu'),
                                'class' => 'text-left  ',
                                'target' => '_blank',
                            ]);
                        },
                        'send' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/mojapotvrda', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">email</span>', "#", [
                                'title' => Yii::t('app', 'Pošalji potvrdu'),
                                'class' => 'text-left  ',
                                'target' => '_blank',
                            ]);
                        },
                    ],
                ],
            ];
            ?>

            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                    ExportMenu::FORMAT_CSV => null,
                ],
                'filename' => 'popisZahtjeva',
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/dijete/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" .
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                ]); ?>


        </div>
    </div>
</div>