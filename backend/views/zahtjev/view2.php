<?php

use backend\models\OdlukaForm;
use backend\models\RjesenjeForm;
use backend\models\UgovorForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model common\models\Zahtjev */

$this->title = $zahtjev->dijete0->ime . ', ' . $zahtjev->dijete0->prezime;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zahtjevs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="card">
    <div class="card-header card-header-danger">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $zahtjev->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="material-icons">print</span>', ['zahtjev/pdfzahtjev', 'id' => $zahtjev->id], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="card-content p0">


        <div class="zahtjev-view detail">

            <?= Tabs::widget([
                'items' => [
                    [
                        'label' => Yii::t('app','Osnovno'),
                        'content' => $this->render('_osnovno.php', [
                            'zahtjev' => $zahtjev,
                            'dijete' => $dijete,
                            'roditelji' => $roditelji,
                            'siblings' => $siblings,
                            'documents' => $documents
                        ]),
                        'active' => true
                    ],
                    [
                        'label' => Yii::t('app','Obavezni dokumenti'),
                        'content' => $this->render('_obavezniDokumenti.php', [
                            'zahtjev' => $zahtjev,
                            'obavezniDokumentiAttributes' => $obavezniDokumentiAttributes,
                        ]),
                    ],
                    [
                        'label' => Yii::t('app','Bodovi'),
                        'content' => $this->render('_bodovi.php', [
                            'zahtjev' => $zahtjev,
                            'attributes' => $attributes,
                        ]),
                    ],
                    [
                        'label' => Yii::t('app','Dokumenti vrtića'),
                        'content' => $this->render('_dokumenti.php', [
                            'zahtjev' => $zahtjev,
                            'documents' => $documents,
                            'rjesenjeForm' => new RjesenjeForm(),
                            'odlukaForm' => new OdlukaForm(),
                            'ugovorForm' => new UgovorForm(),
                        ]),
                    ],
                ],
            ]);
            ?>

        </div>
    </div>
</div>