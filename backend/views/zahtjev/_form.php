<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Zahtjev */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zahtjev-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'urbroj')->textInput() ?>
    <?= $form->field($model, 'klasa')->textInput() ?>
    <?= $form->field($model, 'napomena')->widget(\dosamigos\tinymce\TinyMce::className(), ['options' => [
        'rows' => 26],

        'clientOptions'=>[
            'statusbar'=>false,
            'height'=>300,

            'menubar'=>'',
            'toolbar' => 'bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | removeformat | fullscreen  | responsivefilemanager link anchor  ',
            'external_filemanager_path'=>'/admin/js/responsive_filemanager/filemanager/',
            'filemanager_title'=>"Responsive Filemanager" ,
            'external_plugins'=> [ "filemanager" => "/admin/js/responsive_filemanager/filemanager/plugin.min.js"],
            'inline_styles'=> false,
            'image_advtab'=> true,
            'plugins' => [
                "advlist -autolink lists link charmap print preview anchor image  directionality ",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste pastetext responsivefilemanager"

            ],]

    ]) ?>



   
    <?= $form->field($model, 'status')->widget(Select2::className(), [
            'data' => ArrayHelper::map(\common\models\Status::find()->all(), 'id','status'),
            'options' => [
                    'placeholder' => Yii::t('app','Status'),
                    'multiple' => false,
            ],
            'pluginOptions' => [
                'allowClear' => true,
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
