<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\GridView;
$teskoceData = ['a' => Yii::t('app','oštećenja vida'),
    'b' => Yii::t('app','oštećenja sluha'),
    'c' => Yii::t('app','poremećaji govorno –glasovne komunikacije i specifične teškoće u učenju'),
    'd' => Yii::t('app','tjelesni invaliditet'),
    'e' => Yii::t('app','intelektualne teškoće (sindromi…)'),
    'f' => Yii::t('app','poremećaji u ponašanju uvjetovani organskim faktorima, ADHD'),
    'g' => Yii::t('app','poremećaj socijalne komunikacije; poremećaj iz autističnog spektra; autizam'),
    'h' => Yii::t('app','postojanje više vrsta i stupnjeva teškoća u psihofizičkom razvoju')];

$dijagnostikaData = [
    'a' => Yii::t('app','vještačenjem stručnog povjerenstva socijalne skrbi'),
    'b' => Yii::t('app','nalazom i mišljenjem specijalizirane ustanove'),
    'c' => Yii::t('app','nalazom i mišljenjem ostalih stručnjaka')
];
?>

<div class="">

    <?= DetailView::widget([
        'model' => $zahtjev,
        'attributes' => [
        // 'id',
            'urbroj',
            'create_at',
            'update_at',
            'dijete0.ime',
            'dijete0.prezime',
            'zeljeni_datum:date',
            [
                    'label'=>'Željena lokacija',
                    'value'=>function($data){
                        $temp="Sve lokacije";
                        if($data->lokacija>0){
                            $objekt=\common\models\Objekt::find()->where(['id'=>$data->lokacija])->one();
                            $temp=$objekt->fullPodaci();
                        }
                        return $temp;
                    }
            ],
            [
                'attribute' => 'ocijenjen',
                'value' => function ($model) {
                    return $model->ocijenjen ? Yii::t('app','Da') : Yii::t('app','Ne');
                },
            ],
            [
                // 'attribute' => 'owner',
                'label' => Yii::t('app','Bodovi'),
                'value' => function ($model) {
                    if(!$model->ocijenjen) {
                        return null;
                    }

                    if($model->getIzravanUpis()) {
                        return Yii::t('app','Izravan upis');
                    }

                    return $model->getBodovi();
                },
            ],
            [
                // 'attribute' => 'owner',
                'label' => Yii::t('app','Izravan upis'),
                'value' => function ($model) {
                    if(!$model->ocijenjen) {
                        return null;
                    }

                    return $model->getIzravanUpis() ? Yii::t('app','Da') : Yii::t('app','Ne');
                },
            ],
        ],
    ]) ?>

    <div class="col s12 m12 l8">
        <h4 class="pl-1"><?= Yii::t('app', 'Pregled unešenih podataka') ?></h4>
        <div class="panel panel-success">
            <div class="panel-heading">
                <?php echo $zahtjev->dijete0->ime.', '.$zahtjev->dijete0->prezime; ?>
            </div>
            <div class="panel-body">
            <!-- <a href="<?php /*echo \yii\helpers\Url::to(['dijete/update', 'id' => "$dijete->id"])*/?>" class="btn btn-warning">
                    Uredi podatke o djetetu
                </a><br/><br/>-->
                <?= DetailView::widget([
                    'model' => $dijete,
                    'attributes' => [
                        'ime',
                        'prezime',
                        'spol',
                        'dat_rod',
                        'oib',
                        'drzavljanstvo',
                        [
                            'label'=>'Boravište/stanovanje',
                            'value'=>function($data){
                                    return $data->adresa.',  '.$data->mjesto;
                            }
                        ],
                        [
                                'label'=>'Prebivalište',
                                'value'=>function($data){
                                    if($data->prebivaliste_jednako_boraviste==='N'){
                                        return $data->adresa_prebivalista.', '.$data->prebivaliste;
                                    }else{
                                        return $data->adresa.',  '.$data->mjesto;
                                    }
                                }
                        ],
                        [
                                'label'=>'Brat/sestra u vrtiću',
                                'value'=>function($data){
                                    return $data->sestra=='D'?'Da':'Ne';
                                }
                        ],
                        [
                            'label'=>'Lista čekanja',
                            'value'=>function($data){
                                return $data->cekanje=='D'?'Da, godina:'.$data->god_cekanja:'Ne';
                            }
                        ],
                        'razvoj',
                        'vrstaPrograma.vrsta',
                        [
                                'label'=>'Teškoće u razvoju',
                                'value'=>function($data) use ($teskoceData){
                                    $podaci=json_decode($data->teskoce);
                                    $tekst=array();
                                    if(is_array($podaci)) {
                                        foreach ($podaci as $pod) {
                                            $tekst[] = $teskoceData[$pod];

                                        }
                                    }
                                    return implode(", ",$tekst);
                                }
                        ],
                        [
                            'label'=>'Dijagnostički postupak za utvrđivanje teškoća',
                            'value'=>function($data) use ($dijagnostikaData){
                                $podaci=json_decode($data->dijagnostika);

                                $tekst=array();
                                if(is_array($podaci)){
                                    foreach($podaci as $pod){
                                        $tekst[]=$dijagnostikaData[$pod];

                                    }
                                }
                                return implode(", ",$tekst);
                            }
                        ],
                        'druge_potrebe',
                        'posebne',
                    ],
                ]) ?>
            </div>
        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <?= Yii::t('app', 'Roditelji') ?>
            </div>
            <div class="panel-body">
                <?= kartik\grid\GridView::widget([
                    'dataProvider' => $roditelji,
                    'columns' => [
                        [
                          'label'=>'Osnovni podaci',
                            'format'=>'html',
                            'value'=>function($data){
                                return $data->osnovniPodaci("N");
                            }
                        ],
                        [
                            'label'=>'Boravište/stanovanje',
                            'format'=>'html',
                            'value'=>function($data){
                                return $data->adresa.',  '.$data->mjesto;
                            }
                        ],
                        [
                            'label'=>'Prebivalište',
                            'format'=>'html',
                            'value'=>function($data){
                                if($data->prebivaliste_jednako_boraviste==='N'){
                                    return $data->adresa_prebivalista.', '.$data->prebivaliste;
                                }else{
                                    return $data->adresa.',  '.$data->mjesto;
                                }
                            }
                        ],
                        [
                            'label'=>'Podaci o poslodavcu/zanimanje',
                            'format'=>'html',
                            'value'=>function($data){
                                $tekst='Zanimanje: '.$data->zanimanje.'<br />';
                                $tekst.='Radno vrijeme: '.$data->radno.'<br />';
                                $tekst.=''.$data->poslodavaca.'<br />';
                                $tekst.=''.$data->adresa_poslodavca.'<br />';
                                return $tekst;
                            }
                        ],
                        [
                                'label'=>'Kontakt',
                            'format'=>'html',
                                'value'=>function($data){
                                    return $data->mobitel.'<br />'.$data->email;
                                }
                        ],


                    /* ['class' => 'yii\grid\ActionColumn',
                            'template' => '{update}',
                            // 'visible' => Yii::$app->user->can('admin'),
                            'buttons' => [

                                'update' => function ($url, $data) {

                                    $url2 = \Yii::$app->urlManager->createUrl(['roditelj/update', 'id' => $data->id]);
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url2, [
                                        'title' => Yii::t('app', 'Uredi roditelja'),
                                        'class' => 'text-center block'
                                    ]);
                                },

                            ],
                        ],*/
                    ],
                    'showHeader' => true,
                    'summary'=>'',
                ])
                ?>
            </div>
        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <?= Yii::t('app', 'Sestre i braća') ?>
            </div>
            <div class="panel-body">
                <?= kartik\grid\GridView::widget([
                    'dataProvider' => $siblings,
                    'columns' => [
                        'ime',
                        'prezime',
                        'spol',
                        'dat_rod',
                        'oib',
                        'drzavljanstvo',
                        [
                            'label'=>'Boravište/stanovanje',
                            'value'=>function($data){
                                    return $data->adresa.',  '.$data->mjesto;
                            }
                        ],
                        [
                                'label'=>'Prebivalište',
                                'value'=>function($data){
                                    if($data->prebivaliste_jednako_boraviste==='N'){
                                        return $data->adresa_prebivalista.', '.$data->prebivaliste;
                                    }else{
                                        return $data->adresa.',  '.$data->mjesto;
                                    }
                                }
                        ],

                    ],
                    'showHeader' => true,
                    'summary'=>'',
                ])
                ?>
            </div>
        </div>

        <div class="panel panel-success">
            <div class="panel-heading">
                <?= Yii::t('app', 'Dodani dokumenti') ?>
            </div>
            <div class="panel-body">
                <?= yii\grid\GridView::widget([
                    'dataProvider' => $documents,
                    'columns' => [
                        'naziv',
                        [
                            'label'=>'Datoteka',
                            'format'=>'raw',
                            'value'=>function($data){
                                if($data->vrsta_dokumenta==1){
                                    if(isset($data->filename)){
                                        $dz=\common\models\DocumentZahtjev::find()->where(['document'=>$data->id])->one();
                                        return Html::a('<i class="glyphicon glyphicon-file"></i>'.Yii::t('app', 'Preuzmi'),Yii::$app->params['frontendUrl'].'/upload/zahtjevi/'.$dz->zahtjev.'/'.$data->filename);
                                    }else{
                                        return Yii::t('app','nema');
                                    }
                                }


                            }
                        ],
                    ],
                    'showHeader' => true,
                    'summary'=>'',
                ])
                ?>
            </div>
        </div>
    </div>
</div>