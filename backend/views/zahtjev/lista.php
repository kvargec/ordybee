<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\models\RjesenjeForm;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZahtjevSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rang lista');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zahtjev-index rang-lista">
    <div class="card">
        <div class="card-header card-header-danger">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?php
            $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/ispis-rang-liste']);
            echo Html::a('<i class="material-icons">print</i> ' . Yii::t('app', 'Ispis liste'), $url2, [
                'title' => Yii::t('app', 'Ispis'),
                'class' => 'btn btn-success',
                'target' => '_blank',
            ]);

            ?>

            <?php
            $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/ispis-rang-liste2']);
            echo Html::a('<i class="material-icons">print</i> ' . Yii::t('app', 'Ispis liste (interno)'), $url2, [
                'title' => Yii::t('app', 'Ispis'),
                'class' => 'btn btn-success',
                'target' => '_blank',
            ]);

            ?>

        </div>
        <div class="card-body">
            <?php
            $tempTekst = '';

            $formatter = \Yii::$app->formatter;
            foreach ($rezultati as $index => $rezultat) {
                foreach ($upisneGrupe as $grupa){
                    if($grupa->id==$index){
                        $nazivGrupe = $grupa;
                        break;
                    }

                }

                $tempTekst .= '<h4 class="summary text-uppercase">' . $nazivGrupe->naziv . ' (' . $formatter->asDate($nazivGrupe->date_poc, 'php:j.n.Y.') . '-' . $formatter->asDate($nazivGrupe->date_kraj, 'php:j.n.Y.') . ')</h4>';
                $tempTekst .= '<h4 class="summary">' . Yii::t('app', 'Lista primljene djece') . '</h4>';
                $tempTekst .= '<table id="w0" class="table table-striped table-bordered" >
        <tr>
            <th>#</th>
            <th>' . Yii::t('app', 'Šifra') . '</th>

            <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
            <th>' . Yii::t('app', 'Bodovi') . '</th>
            <th></th>


        </tr>';
                $i = 1;
                $j = 1;
                foreach ($rezultat as $rt) {
                    $klasa = 'style="text-align:center"';
                    $klasa2 = 'style="text-align:left"';

                    $tempTekst .= '<tr>';
                    $tempTekst .= '<td>' . $j . '.</td>';
                    $temp = isset($rt['urbroj']) ? $rt['urbroj'] : '';
                    $temp2 = isset($rt['dijete_dat_rod']) ? $formatter->asDate($rt['dijete_dat_rod'], 'php:j.n.Y.') : '';
                    $temp3 = isset($rt['bodovi']) ? $rt['bodovi'] : '';
                    $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/view', 'id' => $rt['id']]);
                    $link = Html::a('<span class="glyphicon glyphicon-search"></span>&nbsp;', $url2, [
                        'title' => Yii::t('app', 'Pregledaj zahtjev'),
                    ]);

                    $zahtjevId = $rt['id'];

                    $button1 = Html::button(Yii::t('app', 'Rješenje'), ['onclick' => "(function (e) { showForm($zahtjevId, '#form1-$zahtjevId'); })();"]);
                    $button2 = Html::button(Yii::t('app', 'Odluka'), ['onclick' => "(function (e) { showForm($zahtjevId, '#form2-$zahtjevId'); })();"]);
                    $button3 = Html::button(Yii::t('app', 'Ugovor'), ['onclick' => "(function (e) { showForm($zahtjevId, '#form3-$zahtjevId'); })();"]);
                    $tempTekst .= '<td ' . $klasa . '>' . $temp . '</td>';
                    $tempTekst .= '<td ' . $klasa . '>' . $temp2 . '</td>';
                    $tempTekst .= '<td ' . $klasa . '>' . $temp3 . '</td>';
                    $tempTekst .= '<td ' . $klasa . '>' . $link;
                    $tempTekst .= '<div class="dropdown"><a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span><span class="material-icons">add</span> <span class="caret"></span></span><div class="ripple-container"></div></a>';
                    $tempTekst .= '<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">';
                    $tempTekst .= '<li>' . $button1 . '</li>';
                    $tempTekst .= '<li>' . $button2 . '</li>';
                    $tempTekst .= '<li>' . $button3 . '</li>';
                    $tempTekst .= '</ul></div>';

                    $form1 = Html::beginForm(['zahtjev/send-rjesenje', 'id' => $rt['id']], 'post', ['class' => 'send-dokument']);
                    $form1 .= Html::activeLabel($rjesenjeForm, 'klasa');
                    $form1 .= '<div>';
                    $form1 .= Html::activeTextInput($rjesenjeForm, 'klasa',['value'=>$rt['klasa']]);
                    $form1 .= '</div>';
                    $form1 .= Html::activeLabel($rjesenjeForm, 'urbroj');
                    $form1 .= '<div>';
                    $form1 .= Html::activeTextInput($rjesenjeForm, 'urbroj',['value'=>$rt['klasa']]);
                    $form1 .= '</div>';
                    $form1 .= Html::activeLabel($rjesenjeForm, 'datumUpisaDjeteta');
                    $form1 .= DatePicker::widget([
                        'model' => $rjesenjeForm,
                        'attribute' => 'datumUpisaDjeteta',
                        'convertFormat' => true
                    ]);
                    $form1 .= Html::activeLabel($rjesenjeForm, 'datumDonosenjaRjesenja');
                    $form1 .= DatePicker::widget([
                        'model' => $rjesenjeForm,
                        'attribute' => 'datumDonosenjaRjesenja',
                        'convertFormat' => true
                    ]);
                    $form1 .= Html::submitButton(Yii::t('app', 'Pošalji rješenje'), ['class' => 'btn btn-success']);
                    $form1 .= Html::endForm();

                    $form2 = Html::beginForm(['zahtjev/send-odluka', 'id' => $rt['id']], 'post', ['class' => 'send-dokument']);
                    $form2 .= Html::activeLabel($odlukaForm, 'klasa');
                    $form2 .= '<div>';
                    $form2 .= Html::activeTextInput($odlukaForm, 'klasa',['value'=>$rt['klasa']]);
                    $form2 .= '</div>';
                    $form2 .= Html::activeLabel($odlukaForm, 'urbroj');
                    $form2 .= '<div>';
                    $form2 .= Html::activeTextInput($odlukaForm, 'urbroj',['value'=>$rt['klasa']]);
                    $form2 .= '</div>';
                    $form2 .= Html::activeLabel($odlukaForm, 'datumUpisaDjeteta');
                    $form2 .= DatePicker::widget([
                        'model' => $odlukaForm,
                        'attribute' => 'datumUpisaDjeteta',
                        'convertFormat' => true
                    ]);
                    $form2 .= Html::submitButton(Yii::t('app', 'Pošalji odluku'), ['class' => 'btn btn-success']);
                    $form2 .= Html::endForm();

                    $form3 = Html::beginForm(['zahtjev/send-ugovor', 'id' => $rt['id']], 'post', ['class' => 'send-dokument']);
                    $form3 .= Html::activeLabel($ugovorForm, 'klasa');
                    $form3 .= '<div>';
                    $form3 .= Html::activeTextInput($ugovorForm, 'klasa',['value'=>$rt['klasa']]);
                    $form3 .= '</div>';
                    $form3 .= Html::activeLabel($ugovorForm, 'urbroj');
                    $form3 .= '<div>';
                    $form3 .= Html::activeTextInput($ugovorForm, 'urbroj',['value'=>$rt['klasa']]);
                    $form3 .= '</div>';
                    $form3 .= Html::submitButton(Yii::t('app', 'Pošalji ugovor'), ['class' => 'btn btn-success']);
                    $form3 .= Html::endForm();

                    $hideElement = '"display:none"';
                    // $hideElement = '';
                    $rjesenje = Yii::t('app', 'Rješenje');
                    $odluka = Yii::t('app', 'Odluka');
                    $ugovor = Yii::t('app', 'Ugovor');
                    $html = <<<HTML
<table>
            <tr style=$hideElement id="form1-$zahtjevId">
                <td>
                    <h3>$rjesenje</h3>
                    $form1
                </td>
            </tr>
            <tr style=$hideElement id="form2-$zahtjevId">
                <td>
                    <h3>$odluka</h3>
                    $form2
                </td>
            </tr>
            <tr style=$hideElement id="form3-$zahtjevId">
                <td>
                    <h3>$ugovor</h3>
                    $form3
                </td>
            </tr>
</table>
HTML;

                    $js = <<<JS
            function showForm(zahtjevId, formId) {
                zahtjevId = zahtjevId.toString();
                $("#form1-"+zahtjevId).hide();
                $("#form2-"+zahtjevId).hide();
                $("#form3-"+zahtjevId).hide();

                $(formId).show("slow");
            }
JS;

                    $tempTekst .= $html;


                    $this->registerJs($js, VIEW::POS_END);

                    $js = <<<JS
            $(".send-dokument").on("submit", function(e) {
                console.log(e);
                e.preventDefault();
                var url = $(this).attr('action');
                var data = $(this).serializeArray();
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: data
                }).done(function(data, textStatus, jqXHR) {
                    if(data.ok) {
                        $.notify(data.message, "success");
                    } else {
                        $.notify(data.message, "info");
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    alert( errorThrown );
                });
            });
JS;

                    $this->registerJs($js);

                    if ($nazivGrupe->broj == $i) {

                        $tempTekst .= '</table>';
                        $tempTekst .= '<h4>' . Yii::t('app', 'Lista čekanja') . '</h4>';
                        $tempTekst .= '<table id="w0" class="table table-striped table-bordered">
                    <tr>
                        <th>#</th>
                        <th>' . Yii::t('app', 'Šifra') . '</th>

                        <th>' . Yii::t('app', 'Dat.rođenja') . '</th>
                        <th>' . Yii::t('app', 'Bodovi') . '</th>
                        <th></th>

                    </tr>';
                        $j = 0;
                    } else {
                        $klasa = 'style="text-align:center"';
                    }
                    $i++;
                    $j++;
                    $tempTekst .= '</td>';
                    $tempTekst .= '</tr>';
                }
                $tempTekst .= '</table>';
                $tempTekst .= '<hr />';
            }
            echo $tempTekst;

            ?>

        </div>

    </div>