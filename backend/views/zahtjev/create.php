<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Zahtjev */

$this->title = Yii::t('app', 'Create Zahtjev');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zahtjevs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zahtjev-create">

<div class="skupina-create">
    <div class="card">
        <div class="card-header card-header-danger">
            <span class="card-title"><?= Html::encode($this->title) ?></span>

        </div>
<div class="card-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div></div>
