<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\date\DatePicker;
use yii\web\View;
?>
<div class="">
    <div class="panel panel-success">
        <div class="panel-heading">
            <?= Yii::t('app', 'Dokumenti vrtića') ?>
        </div>
        <div class="panel-body">
            <?= yii\grid\GridView::widget([
                'dataProvider' => $documents,
                'columns' => [
                    'naziv',
                    'vrstaDokumenta.opis',
                    [
                        'label' => Yii::t('app', 'Datoteka'),
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->vrsta_dokumenta != 1) {
                                if (isset($data->filename)) {
                                    $dz = \common\models\DocumentZahtjev::find()->where(['document' => $data->id])->one();
                                    return Html::a('<i class="material-icons">picture_as_pdf</i>' . Yii::t('app', 'Preuzmi'), Yii::$app->params['frontendUrl'] . '/dokumenti/' . $dz->zahtjev . '/' . $data->filename);
                                } else {
                                    return Yii::t('app', 'nema');
                                }
                            }
                        }
                    ],
                ],
                'showHeader' => true,
                'summary' => '',
            ])
            ?>
        </div>
        <div class="panel-footer">
            <?php
            $zahtjevId = $zahtjev->id;
            $klasa = 'style="text-align:center"';
            $klasa2 = 'style="text-align:left"';

            $button1 = Html::button(Yii::t('app', 'Rješenje'), ['onclick' => "(function (e) { showForm($zahtjevId, '#form1-$zahtjevId'); })();"]);
            $button2 = Html::button(Yii::t('app', 'Odluka'), ['onclick' => "(function (e) { showForm($zahtjevId, '#form2-$zahtjevId'); })();"]);
            $button3 = Html::button(Yii::t('app', 'Ugovor'), ['onclick' => "(function (e) { showForm($zahtjevId, '#form3-$zahtjevId'); })();"]);

            $tempTekst = '<div class="dropdown">
  <a class="btn btn-success btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>'.Yii::t('app', 'Dokumenti').' <span class="caret"></span></span>
  </a>';
            $tempTekst .= '<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">';

            $tempTekst .= '<li>' . $button1 . '</li>';
            $tempTekst .= '<li>' . $button2 . '</li>';
            $tempTekst .= '<li>' . $button3 . '</li>';
            $tempTekst .= '</ul></div>';

            $form1 = Html::beginForm(['zahtjev/get-rjesenje', 'id' => $zahtjevId], 'post', ['class' => 'send-dokument']);
            $form1 .= Html::activeLabel($rjesenjeForm, 'klasa');
            $form1 .= '<div>';
            $form1 .= Html::activeTextInput($rjesenjeForm, 'klasa', ['value' => $zahtjev->klasa]);
            $form1 .= '</div>';
            $form1 .= Html::activeLabel($rjesenjeForm, 'urbroj');
            $form1 .= '<div>';
            $form1 .= Html::activeTextInput($rjesenjeForm, 'urbroj', ['value' => $zahtjev->klasa]);
            $form1 .= '</div>';
            $form1 .= Html::activeLabel($rjesenjeForm, 'datumUpisaDjeteta');
            $form1 .= DatePicker::widget([
                'model' => $rjesenjeForm,
                'attribute' => 'datumUpisaDjeteta',
                'convertFormat' => true,
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]);
            $form1 .= Html::activeLabel($rjesenjeForm, 'datumDonosenjaRjesenja');
            $form1 .= DatePicker::widget([
                'model' => $rjesenjeForm,
                'attribute' => 'datumDonosenjaRjesenja',
                'convertFormat' => true,
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]);
            $form1 .= Html::submitButton(Yii::t('app', 'Spremi rješenje'), ['class' => 'btn btn-success']);
            $form1 .= Html::endForm();

            $form2 = Html::beginForm(['zahtjev/get-odluka', 'id' => $zahtjevId], 'post', ['class' => 'send-dokument']);
            $form2 .= Html::activeLabel($odlukaForm, 'klasa');
            $form2 .= '<div>';
            $form2 .= Html::activeTextInput($odlukaForm, 'klasa', ['value' => $zahtjev->klasa]);
            $form2 .= '</div>';
            $form2 .= Html::activeLabel($odlukaForm, 'urbroj');
            $form2 .= '<div>';
            $form2 .= Html::activeTextInput($odlukaForm, 'urbroj', ['value' => $zahtjev->klasa]);
            $form2 .= '</div>';
            $form2 .= Html::activeLabel($odlukaForm, 'datumUpisaDjeteta');
            $form2 .= DatePicker::widget([
                'model' => $odlukaForm,
                'attribute' => 'datumUpisaDjeteta',
                'convertFormat' => true,
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]);
            $form2 .= Html::submitButton(Yii::t('app', 'Spremi odluku'), ['class' => 'btn btn-success']);
            $form2 .= Html::endForm();

            $form3 = Html::beginForm(['zahtjev/get-ugovor', 'id' => $zahtjevId], 'post', ['class' => 'send-dokument']);
            $form3 .= Html::activeLabel($ugovorForm, 'klasa');
            $form3 .= '<div>';
            $form3 .= Html::activeTextInput($ugovorForm, 'klasa', ['value' => $zahtjev->klasa]);
            $form3 .= '</div>';
            $form3 .= Html::activeLabel($ugovorForm, 'urbroj');
            $form3 .= '<div>';
            $form3 .= Html::activeTextInput($ugovorForm, 'urbroj', ['value' => $zahtjev->klasa]);
            $form3 .= '</div>';
            $form3 .= Html::submitButton(Yii::t('app', 'Spremi ugovor'), ['class' => 'btn btn-success']);
            $form3 .= Html::endForm();

            $hideElement = '"display:none"';
            // $hideElement = '';
            $rjesenje = Yii::t('app', 'Rješenje');
            $odluka = Yii::t('app', 'Odluka');
            $ugovor = Yii::t('app', 'Ugovor');
            $html = <<<HTML
<table>
            <tr style=$hideElement id="form1-$zahtjevId">
                <td>
                    <h3>$rjesenje</h3>
                    $form1
                </td>
            </tr>
            <tr style=$hideElement id="form2-$zahtjevId">
                <td>
                    <h3>$odluka</h3>
                    $form2
                </td>
            </tr>
            <tr style=$hideElement id="form3-$zahtjevId">
                <td>
                    <h3>$ugovor</h3>
                    $form3
                </td>
            </tr>
</table>
HTML;

            $js = <<<JS
            function showForm(zahtjevId, formId) {
                zahtjevId = zahtjevId.toString();
                $("#form1-"+zahtjevId).hide();
                $("#form2-"+zahtjevId).hide();
                $("#form3-"+zahtjevId).hide();

                $(formId).show("slow");
            }
JS;

            $tempTekst .= $html;


            $this->registerJs($js, VIEW::POS_END);

            $js = <<<JS
         /*   $(".send-dokument").on("submit", function(e) {
                console.log(e);
                e.preventDefault();
                var url = $(this).attr('action');
                var data = $(this).serializeArray();
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: data
                }).done(function(data, textStatus, jqXHR) {
                    if(data.ok) {
                        $.notify(data.message, "success");
                    } else {
                        $.notify(data.message, "info");
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    alert( errorThrown );
                });
            });*/
JS;

            $this->registerJs($js);
            echo $tempTekst;
            ?>
        </div>
    </div>
</div>