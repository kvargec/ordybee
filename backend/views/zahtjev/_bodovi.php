<?php

use yii\helpers\Html;
use kartik\builder\Form;
?>

<div class="zahtjev-boduj" id="zahtjev-boduj">
<h2><?= Yii::t('app', 'Pribilježite dodatne bodove') ?> </h2>
    <style>#zahtjev-boduj .col-sm-12{display:flex;justify-content: space-between;}</style>
    <?php
        echo Html::beginForm(['boduj', 'id' => $zahtjev->id], 'post', ['class'=>'form-horizontal']);
        echo Form::widget([
            // formName is mandatory for non active forms
            // you can get all attributes in your controller 
            // using $_POST['bodovi']
            'formName'=>'bodovi',
            
            // default grid columns
            'columns'=>1,
            
            'attributes'=>$attributes,
        ]);
        echo Html::endForm();
    ?>
</div>