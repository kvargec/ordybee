<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model common\models\Zahtjev */

$this->title = $zahtjev->dijete0->ime . ', ' . $zahtjev->dijete0->prezime;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zahtjevs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="card">
    <div class="card-header card-header-danger">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $zahtjev->urbroj], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $zahtjev->urbroj], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="card-content">

        <div class="zahtjev-view row">


            <div class="col-sm-12 col-lg-3">
                <p>
                    <?php //Html::button(Yii::t('app', 'Boduj'), ['class' => 'btn btn-primary', 'onclick' => 'document.getElementById("zahtjev-boduj").style.display = "block";']) 
                    ?>
                </p>

                <div class="zahtjev-boduj" id="zahtjev-boduj" style='display: block;'>
                    <?php
                    echo Html::beginForm(['boduj', 'id' => $zahtjev->id], 'post', ['class' => 'form-horizontal']);
                    echo Form::widget([
                        // formName is mandatory for non active forms
                        // you can get all attributes in your controller 
                        // using $_POST['bodovi']
                        'formName' => 'bodovi',

                        // default grid columns
                        'columns' => 1,

                        'attributes' => $attributes,
                    ]);
                    echo Html::endForm();
                    ?>
                </div>
            </div>

            <div class="col-sm-12 col-lg-8">

                <?= DetailView::widget([
                    'model' => $zahtjev,
                    'attributes' => [
                        // 'id',
                        'urbroj',
                        'create_at',
                        'update_at',
                        'dijete0.ime',
                        'dijete0.prezime',
                        'zeljeni_datum',
                        [
                            'attribute' => 'ocijenjen',
                            'value' => function ($model) {
                                return $model->ocijenjen ? Yii::t('app','Da') : Yii::t('app','Ne');
                            },
                        ],
                        [
                            // 'attribute' => 'owner',
                            'label' => Yii::t('app','Bodovi'),
                            'value' => function ($model) {
                                if (!$model->ocijenjen) {
                                    return null;
                                }

                                return $model->getBodovi();
                            },
                        ],
                        [
                            // 'attribute' => 'owner',
                            'label' => Yii::t('app','Izravan upis'),
                            'value' => function ($model) {
                                if (!$model->ocijenjen) {
                                    return null;
                                }

                                return $model->getIzravanUpis() ? Yii::t('app','Da') : Yii::t('app','Ne');
                            },
                        ],
                    ],
                ]) ?>

                <div class="col s12 m12 l8">
                    <h4><?= Yii::t('app', 'Pregled unešenih podataka') ?></h4>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <?php echo $zahtjev->dijete0->ime . ', ' . $zahtjev->dijete0->prezime; ?>
                        </div>
                        <div class="panel-body">
                            <!-- <a href="<?php /*echo \yii\helpers\Url::to(['dijete/update', 'id' => "$dijete->id"])*/ ?>" class="btn btn-warning">
                        Uredi podatke o djetetu
                    </a><br/><br/>-->
                            <?= DetailView::widget([
                                'model' => $dijete,
                                'attributes' => [
                                    'ime',
                                    'prezime',
                                    'spol',
                                    'dat_rod',
                                    'oib',
                                    'adresa',
                                    'mjesto',
                                    'prebivaliste',
                                    'sestra',
                                    'cekanje',
                                    'god_cekanja',
                                    'razvoj',
                                    'vrsta_programa',
                                    'teskoce',
                                    'dijagnostika',
                                    'druge_potrebe',
                                    'posebne',
                                ],
                            ]) ?>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <?= Yii::t('app', 'Roditelji') ?>
                        </div>
                        <div class="panel-body">
                            <?= yii\grid\GridView::widget([
                                'dataProvider' => $roditelji,
                                'columns' => [
                                    'ime',
                                    'prezime',
                                    'dat_rod',
                                    'oib',
                                    'adresa',
                                    'mjesto',
                                    'prebivaliste',
                                    'mobitel',
                                    'email',
                                    'poslodavaca',
                                    'radno',
                                    /* ['class' => 'yii\grid\ActionColumn',
                                'template' => '{update}',
                                // 'visible' => Yii::$app->user->can('admin'),
                                'buttons' => [

                                    'update' => function ($url, $data) {

                                        $url2 = \Yii::$app->urlManager->createUrl(['roditelj/update', 'id' => $data->id]);
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url2, [
                                            'title' => Yii::t('app', 'Uredi roditelja'),
                                            'class' => 'text-center block'
                                        ]);
                                    },

                                ],
                            ],*/
                                ],
                                'showHeader' => true,
                                'summary' => '',
                            ])
                            ?>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <?= Yii::t('app', 'Sestre i braća') ?>
                        </div>
                        <div class="panel-body">
                            <?= yii\grid\GridView::widget([
                                'dataProvider' => $siblings,
                                'columns' => [
                                    'ime',
                                    'prezime',
                                    'spol',
                                    'dat_rod',
                                    'oib',
                                    'prebivaliste',
                                    /*['class' => 'yii\grid\ActionColumn',
                                'template' => '{update}',
                                'buttons' => [

                                    'update' => function ($url, $data) {
                                        $url2 = \Yii::$app->urlManager->createUrl(['dijete/updatesibling', 'id' => "$data->id" ]);
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url2, [
                                            'title' => Yii::t('app', 'Uredi'),
                                            'class' => 'text-center block'
                                        ]);
                                    },

                                ],
                            ],*/
                                ],
                                'showHeader' => true,
                                'summary' => '',
                            ])
                            ?>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <?= Yii::t('app', 'Dodani dokumenti') ?>
                        </div>
                        <div class="panel-body">
                            <?= yii\grid\GridView::widget([
                                'dataProvider' => $documents,
                                'columns' => [
                                    'naziv',
                                    [
                                        'label' => 'Datoteka',
                                        'format' => 'raw',
                                        'value' => function ($data) {
                                            if (isset($data->filename)) {
                                                $dz = \common\models\DocumentZahtjev::find()->where(['document' => $data->id])->one();
                                                return Html::a('<i class="glyphicon glyphicon-file"></i>'.Yii::t('app','Preuzmi'), Yii::$app->params['frontendUrl'] . '/upload/zahtjevi/' . $dz->zahtjev . '/' . $data->filename);
                                            } else {
                                                return 'nema';
                                            }
                                        }
                                    ],
                                ],
                                'showHeader' => true,
                                'summary' => '',
                            ])
                            ?>
                        </div>
                    </div>

                    <?= Yii::t('app', 'Popis obaveznih dokumenta') ?><br />
                    <?php
                    $dzs = \common\models\DocumentZahtjev::find()->where(['zahtjev' => $zahtjev->id])->all();
                    $condition = [
                        'id' => array_map(function ($x) {
                            return $x->id;
                        }, $dzs),
                        'naziv' => \common\models\Document::OBAVEZNI_DOKUMENTI,
                    ];
                    $ds = \common\models\Document::find()->select('naziv')->distinct()->where($condition)->all();
                    $dnames = array_map(function ($x) {
                        return $x->naziv;
                    }, $ds);
                    foreach (\common\models\Document::OBAVEZNI_DOKUMENTI as $d) {
                        if (in_array($d, $dnames)) {
                            $class = '"glyphicon glyphicon-check"';
                        } else {
                            $class = '"glyphicon glyphicon-unchecked"';
                        }
                        echo "<span class=$class></span> $d<br/>";
                    }
                    // foreach($ds as $d) {
                    //     echo "<li>$d->naziv</li>";
                    // }
                    ?>

                </div>

            </div>

        </div>
    </div>
</div>