<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Proizvod */

$this->title = Yii::t('app', 'Update Proizvod: {name}', [
    'name' => $model->naziv,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Proizvods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="proizvod-update">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
   


      
</div>
<div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
