<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Proizvod */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proizvod-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sifra')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grupa')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\GrupaProizvod::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'opis')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'mjed')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Mjed::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
