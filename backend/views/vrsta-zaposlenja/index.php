<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\VrstaZaposlenjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Vrsta Zaposlenjas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrsta-zaposlenja-index">
<div class="card">
    <div class="card-header card-header-primary"> 
        <span class="card-title"><?= Html::encode($this->title) ?></span>
   
        <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
</div>
<div class="card-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        //'id',
        'naziv',
        ['class' => 'yii\grid\ActionColumn',
                 'template' => '{view}{update}',
                 // 'visible' => Yii::$app->user->can('admin'),
                 'buttons' => [
                     'view' => function ($url, $data) {

                         $url2 = \Yii::$app->urlManager->createUrl(['vrsta-zaposlenja/view', 'id' => $data->id]);
                         return Html::a('<span class="material-icons">visibility</span>', $url2, [
                             'title' => Yii::t('app', 'Pogledaj'),
                         ]);
                     },

                     'update' => function ($url, $data) {
                         $url2 = \Yii::$app->urlManager->createUrl(['vrsta-zaposlenja/update', 'id' => $data->id]);
                         return Html::a('<span class="material-icons">create</span>', $url2, [
                             'title' => Yii::t('app', 'Ažuriraj'),
                         ]);
                     },
                     

                 ],
             ],
    ];
    ?>

    <?= ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumns,
                        'exportConfig' => [
                            ExportMenu::FORMAT_TEXT => null,
                            ExportMenu::FORMAT_HTML => null,
                            ExportMenu::FORMAT_CSV => null,
                        ],
                        'filename' => 'popisVrstaZaposlenja',
                        'stream' => false,
                        'folder' => '@app/web/',
                        //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                        //'target' => '_blank',
                        'linkPath' => Url::toRoute('@web/web/'),
                        'afterSaveView' => '/dijete/zalink',
                        'dropdownOptions' => [
                            'label' => Yii::t('app','Export All'),
                            'class' => 'btn btn-outline-secondary'
                        ]
    ]) . "<hr>\n" . 
    GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>


</div></div>
</div>
