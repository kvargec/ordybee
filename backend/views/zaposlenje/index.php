<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZaposlenjeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zaposlenjes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zaposlenje-index">
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>

        </div>
        <div class="card-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            $gridColumns = [
                // ['class' => 'yii\grid\SerialColumn'],

                [
                    'value' => function ($data) {
                        return $data->zaposlenik0->ime . ' ' . $data->zaposlenik0->prezime;
                    },
                    'label' => Yii::t('app', 'Zaposlenik'),
                    'attribute' => 'ime',
                    'filterOptions' => [
                        'class' => 'form-group',
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime prezime'),
                        'title' => Yii::t('app', 'Ime'),
                        'class' => 'form-control',
                    ],
                    'filterAttribute' => 'full_name_zaposlenik'
                ],
                [
                    'value' => 'rMjesto.naziv',
                    'label' => Yii::t('app', 'Radno mjesto'),
                    'attribute' => 'r_mjesto',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Naziv radnog mjesta'),
                        'title' => Yii::t('app', 'Radno mjesto'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'dat_pocetka',
                    'label' => Yii::t('app', 'Datum početka'),
                    'attribute' => 'dat_pocetka',
                    'format' => ['date', 'php:d.m.Y'],
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Datum početka'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'date_kraj',
                    'label' => Yii::t('app', 'Datum kraja'),
                    'attribute' => 'date_kraj',
                    'format' => ['date', 'php:d.m.Y'],
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Datum kraja'),
                        'class' => 'form-control'
                    ]
                ],

                [
                    'value' => 'koeficijent',
                    'label' => Yii::t('app', 'Koeficijent'),
                    'attribute' => 'koeficijent',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Koeficijent'),
                        'class' => 'form-control'
                    ]
                ],

                //'vrsta',
                //'r_mjesto',
                //'created_at',
                //'updated_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['zaposlenje/view', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pogledaj'),
                            ]);
                        },

                        'update' => function ($url, $data) {
                            $url2 = \Yii::$app->urlManager->createUrl(['zaposlenje/update', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">create</span>', $url2, [
                                'title' => Yii::t('app', 'Ažuriraj'),
                            ]);
                        },


                    ],
                ],
            ]; ?>

            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                    ExportMenu::FORMAT_CSV => null,
                ],
                'filename' => 'popisZaposlenja',
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/dijete/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" .
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                ]); ?>


        </div>
    </div>
</div>