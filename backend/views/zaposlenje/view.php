<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Zaposlenje */

$this->title = $model->rMjesto->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zaposlenjes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="zaposlenje-view">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<span class="material-icons">print</span>', ['zaposlenje/pdf-print', 'id' => $model->id], ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
    </div>
    <div class="card-body detail">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [   'attribute' =>'dat_pocetka',
                'value' => function ($data){
                    return Yii::$app->formatter->asDate($data->dat_pocetka, 'php:j.n.Y');
                },
            ],
            [   'attribute' =>'date_kraj',
                'value' => function ($data){
                    return Yii::$app->formatter->asDate($data->date_kraj, 'php:j.n.Y');
                },
            ],
            'koeficijent',
            'zaposlenik0.ime',
            'zaposlenik0.prezime',
            'vrsta0.naziv',
            'rMjesto.naziv',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div></div></div>
