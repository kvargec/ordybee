<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Zaposlenje */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zaposlenje-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dat_pocetka')->widget(DatePicker::classname(), [
        'convertFormat' => true,
        // 'options' => ['placeholder' => 'Enter birth date ...'],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]) ?>

    <?= $form->field($model, 'date_kraj')->widget(DatePicker::classname(), [
        'convertFormat' => true,
        // 'options' => ['placeholder' => 'Enter birth date ...'],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]) ?>

    <?= $form->field($model, 'koeficijent')->textInput() ?>

    <?= $form->field($model, 'zaposlenik')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(
                \common\models\Zaposlenik::find()->where(['!=', 'status', 3])->all(),
                'id',
                function ($v) {
                    return $v->getFullName();
                }
            ),
            // 'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <?= $form->field($model, 'vrsta')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(
                \common\models\VrstaZaposlenja::find()->all(),
                'id',
                'naziv'
            ),
            // 'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <?= $form->field($model, 'r_mjesto')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(
                \common\models\RadnoMjesto::find()->all(),
                'id',
                'naziv'
            ),
            // 'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
