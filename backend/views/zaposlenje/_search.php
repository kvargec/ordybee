<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\ZaposlenjeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zaposlenje-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'dat_pocetka') ?>

    <?= $form->field($model, 'date_kraj') ?>

    <?= $form->field($model, 'koeficijent') ?>

    <?= $form->field($model, 'zaposlenik') ?>

    <?php // echo $form->field($model, 'vrsta') ?>

    <?php // echo $form->field($model, 'r_mjesto') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
