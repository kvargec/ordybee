<?php

use kartik\widgets\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Calendar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calendar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dan')->widget(\kartik\widgets\DateTimePicker::classname(), [
                'type' => DateTimePicker::TYPE_INPUT,
                'value' => '23-Feb-1982 10:10',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy hh:ii'
                ]
]);
             ?>

    <?= $form->field($model, 'vrsta')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\DanType::find()->orderBy('id')->asArray()->all(),
                    'id',
                    function ($data) {
                        return $data['vrsta'];
                    }),
                'options' => ['placeholder' => Yii::t('app','Odaberi vrstu dana...')],
            ]) ?>

    <?= $form->field($model, 'napomena')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
