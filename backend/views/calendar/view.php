<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Calendar */

$this->title = $model->napomena;
$this->params['breadcrumbs'][] = ['label' => 'Calendar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="calendar-view">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
</div>
<div class="card-body  detail">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dan:date',
            'vrsta',
            'napomena:ntext',
        ],
    ]) ?>

</div>
</div>
</div>
