<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\web\JsExpression;
?>
<div class="card">
    <div class="card-header card-header-primary calendar">
        <span class="card-title"> <?= Yii::t('app', 'Kalendar') ?></span>
        <?= \yii\bootstrap\ButtonDropdown::widget([
            'encodeLabel' => false,
            'label' => Yii::t('app', '<span class="material-icons">add</span>'),
            'dropdown' => [
                'items' => [
                    ['label' => Yii::t('app', 'Praznici'), 'url' => \yii\helpers\Url::to(['/calendar/create'])],
                    ['label' => Yii::t('app', 'Aktivnosti'), 'url' => \yii\helpers\Url::to(['/aktivnosti/create'])],
                ]
            ]
        ]) ?>
    </div>
    <div class="card-body" style="padding-bottom:20px">
        <?php
        echo \backend\widgets\calendar\CalendarWidget::widget(['model' => $events]);
        ?>
    </div>
</div>
</div>