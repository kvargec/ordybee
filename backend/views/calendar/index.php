<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Calendar');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calendar-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>

        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            $gridColumns = [
                // ['class' => 'yii\grid\SerialColumn'],

                [
                    'label' => 'Datum',
                    'attribute' => 'dan',
                    'value' => function ($data) {
                        return Yii::$app->formatter->asDate($data->dan);
                    },
                    'filter' => '<div class="drp-container input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' .
                        DateRangePicker::widget([
                            'name'  => 'ItemOrderSearch[created_at]',
                            'pluginOptions' => [
                                'locale' => [
                                    'separator' => 'to',
                                ],
                                'opens' => 'right'
                            ]
                        ]) . '</div>',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    /*
                    [
                    'label'=>'Rok isporuke projekta',
                    'format' => 'raw',
                    'contentOptions'=>['style'=>'min-width:150px'],
                    'attribute'=>'date_rok',
                    'enableSorting'=>true,
                        'filter'=>DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_rok',
                        'language' => 'hr',
                        // 'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'readonly'=>true,
                        'convertFormat'=>true,
                        // 'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'timePicker' => false,
                            'format' => 'd.m.Y.',
                            'todayBtn' => true,

                        ]
                    ]),
                    'value'=>function ($dataProvider) {
                            if($dataProvider->date_rok==null)return"N/D";
                            $date = new \DateTime($dataProvider->date_rok);
                            $date=getdate($date->getTimestamp());
                            $trenutno = new \DateTime();
                            $trenutno=getdate($trenutno->getTimestamp());
                            $ostalo = $date[0]-$trenutno[0];
                            $ostalo=round($ostalo/60/60/24);
                            $a=$ostalo == 1?'':'a';
                            $nastavak=$ostalo <0? ' (istekao)':' ('.$ostalo.' dan'.$a.')';
                            return date("d.m.Y. G:i", strtotime($dataProvider->date_rok));
                        },
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Datum od'),
                        'class' => 'form-control'
                    ],*/
                ],
                [
                    'value' => 'vrsta0.vrsta',
                    'label' => Yii::t('app', 'Vrsta'),
                    'attribute' => 'vrsta',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Broj'),
                        'class' => 'form-control'
                    ],
                    'filter' => yii\helpers\Arrayhelper::map(\common\models\DanType::find()->asArray()->all(), 'id', 'vrsta'),
                ],
                'napomena:ntext',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    //'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['calendar/view', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pogledaj'),
                            ]);
                        },

                        'update' => function ($url, $data) {
                            $url2 = \Yii::$app->urlManager->createUrl(['calendar/update', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">create</span>', $url2, [
                                'title' => Yii::t('app', 'Ažuriraj'),
                            ]);
                        },


                    ],
                ],
            ] ?>
            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                    ExportMenu::FORMAT_CSV => null,
                ],
                'filename' => 'popisBlagdana',
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/dijete/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" . GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
            ]); ?>


        </div>
    </div>
</div>