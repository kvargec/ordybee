<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Calendar */

$this->title = Yii::t('app','Create Calendar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Calendars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calendar-create">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        
</div>
<div class="card-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
