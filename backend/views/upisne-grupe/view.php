<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UpisneGrupe */

$this->title = $model->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upisne Grupes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="upisne-grupe-view">
<div class="card">
    <div class="card-header card-header-danger">
    <span class="card-title"><?= Html::encode($this->title) ?></span>

   
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="card-body detail">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'naziv',
            'date_poc',
            'date_kraj',
            'broj',
            [
                'attribute' => 'ped_godina',
                'value' => function($data){
                    return Yii::$app->formatter->asDate($data->pedGodina->od,'php:Y').'./'.Yii::$app->formatter->asDate($data->pedGodina->do,'php:Y').'.';
                }
            ],
        ],
    ]) ?>

</div>
</div>
</div>
