<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\UpisneGrupe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="upisne-grupe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>

    <!--<?//= $form->field($model, 'date_poc')->textInput() ?>-->
    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'date_poc',
        'convertFormat' => false,
        'options' => ['placeholder' => Yii::t('app','Datum početka')],
        'pluginOptions' => [
            'todayHighlight' => true,
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]);
    ?> 

    <!--<?//= $form->field($model, 'date_kraj')->textInput() ?>-->
    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'date_kraj',
        'convertFormat' => false,
        'options' => ['placeholder' => Yii::t('app','Datum kraja')],
        'pluginOptions' => [
            'todayHighlight' => true,
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]);
    ?> 

    <?= $form->field($model, 'broj')->textInput() ?>

    <?= $form->field($model, 'ped_godina')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\PedagoskaGodina::find()->all(),'id',function($v){return $v->getNaziv();})) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
