<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\UpisneGrupeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="upisne-grupe-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'naziv') ?>

    <?= $form->field($model, 'date_poc') ?>

    <?= $form->field($model, 'date_kraj') ?>

    <?= $form->field($model, 'broj') ?>

    <?php // echo $form->field($model, 'ped_godina') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
