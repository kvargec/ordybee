<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use common\models\Postavke;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use \ramosisw\CImaterial\widgets\Menu;
use common\models\PedagoskaGodina;
use MyCademy\BootstrapTour\Tour;


if (class_exists('ramosisw\CImaterial\web\MaterialAsset')) {
    ramosisw\CImaterial\web\MaterialAsset::register($this);
}
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="<?php echo Yii::getAlias('@web') . '/'; ?>/favicon.png" type="image/png" sizes="16x16">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons+Round" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.11.0/css/bootstrap-tour.css" rel="stylesheet">
    <link href="//cdn.jsdelivr.net/npm/featherlight@1.7.14/release/featherlight.min.css" type="text/css" rel="stylesheet" />
    <link href="//cdn.jsdelivr.net/npm/featherlight@1.7.14/release/featherlight.gallery.min.css" type="text/css" rel="stylesheet" />


    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrapper ">
        <?php
        if (!Yii::$app->user->isGuest) {
        ?>
            <div class="sidebar" id="sidebar" data-color="purple" data-background-color="white">
                <!--
       Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
       Tip 2: you can also add an image using data-image tag
   -->
                <div class="logo" id="demo"><a href="<?php echo Yii::getAlias('@web'); ?>" class="simple-text logo-normal">
                        <img src="<?php echo Yii::getAlias('@web') . '/logo.svg'; ?>" class="img-fluid"></img>
                        <!-- <?php echo Yii::$app->params['aplikName']; ?> -->
                    </a>
                    <button id="minimizeSidebar" class="btn btn-round btn-primary">
                        <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                    </button>
                </div>

                <div class="sidebar-wrapper">
                    <?php
                    $rola = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
                    $uloge = array();
                    foreach ($rola as $tko => $opis) {
                        $uloge[] = $tko;
                    }
                    if (empty($uloge[0])) {
                        echo $this->render('menu/prazno');
                    } else {
                        echo $this->render('menu/' . $uloge[0]);
                    }

                    //        die();
                    ?>
                    <?= Html::a('<span class="material-icons">info</span> Pomoć', ['/site/docs'], ['class' => 'btn btn-success mx-auto px-35', 'id' => 'demo2']) ?>
                    <!-- <?= Html::a('<span class="material-icons">autorenew</span> Nova pedagoška godina', ['/pedagoska-godina/create'], ['class' => 'btn btn-danger mx-auto mt-15', 'id' => 'new_year', 'style' => 'visibility:hidden']) ?>
                    <a href="#" id="startTourBtn" class="mx-auto px-35 text-center"><span class="material-icons">help</span></a> -->
                    <!-- <?= Html::a('Upute', ['/site/docs'], ['class' => 'mx-auto px-35 text-center']) ?> -->
                </div>

            </div>
        <?php } ?>

        <div class="main-panel ">

            <?php
            if (!Yii::$app->user->isGuest) {
                // setlocale(LC_ALL,'hr-HR','croatian');
                // $hrformat = "%A, %e.%m.%Y  %H:%M";
                // $brandLabel = strftime($hrformat);
                // date("d.m.Y. H:i, l");
                $brandLabel = '';
                $brandUrl = Yii::$app->homeUrl;
            } else {
                $brandLabel = Html::img('@web/light.svg', ['alt' => Yii::$app->name]);
                $brandUrl = '';
            }

            NavBar::begin([
                'brandLabel' => $brandLabel,
                'brandUrl' => $brandUrl,
                'innerContainerOptions' => ['class' => 'container-fluid'],
                'options' => [
                    'class' => 'navbar navbar-transparent navbar-absolute',
                ],
            ]);
            ?>



            <?php
            $menuItems = [];
            if (!Yii::$app->user->isGuest) {
                $menuItems[] = '<li class="searchbar "><form action="/action_page.php">
                <input type="text" placeholder="' . Yii::t('app', 'Search') . '" name="search">
                <button type="submit" class="btn btn-sm btn-primary search-button"><span class="material-icons">search</span></button>
              </form></li><li style="display:none">'
                    . Html::beginForm(['/site/godina'], 'post')
                    . \kartik\select2\Select2::widget([
                        'name' => 'ped_godina',
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\PedagoskaGodina::find()->orderBy(['id' => SORT_DESC])->all(), 'id', function ($data) {
                            return date('Y', strtotime($data->od)) . '/' . date('Y', strtotime($data->do));
                        }),
                        'value' => isset($_SESSION['ped_godina']) ? $_SESSION['ped_godina'] : '',
                        'pluginEvents' => [
                            'select2:select' => 'function(){
                                $(this).closest(\'form\').submit();
                            }'
                        ]
                    ])
                    . Html::endForm()
                    . '</li>';
            }
            // $menuItems[] =
            //     ['label' => 'Home', 'url' => ['/site/index'], 'options' => ['class' => 'desktop']];

            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/user/security/login']];
            } else {

                $notifikacije = \backend\controllers\NotifikacijeController::getNotifications(Yii::$app->user->id);
                $notifikacijeLista = [];
                foreach ($notifikacije as $item) {
                    $notifikacijeLista[] = ['label' => strlen($item->subject) > 30 ? substr($item->subject, 0, 30) . ' ...' : $item->subject, 'url' => ['/notifikacije/view', 'id' => $item->id, 'tip' => 'inbox']];
                }
                // $menuItems[] = ['label' => '<i class="material-icons">notifications</i>( ' . count($notifikacije) . ' )', 'url' => '#', 'items' => $notifikacijeLista];
                $menuItems[] = ['label' => '<i class="material-icons">list</i>( 0 )', 'url' => ['/zahtjev/rang-lista'], 'options' => ['class' => 'desktop']];
                $menuItems[] = ['label' => '<i class="material-icons">calendar_today</i>', 'url' => ['/calendar/index2'], 'options' => ['class' => 'desktop']];
                $menuItems[] = '<li class="desktop">'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        Yii::$app->user->identity->username . ' ' . '<i class="material-icons">logout</i> ',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>';
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'encodeLabels' => false,
                'items' => $menuItems,
            ]);
            NavBar::end();

            ?>

            <?php $back = Yii::$app->user->isGuest ? 'back' : ''; ?>
            <div class="content <?= $back ?>">

                <?= Alert::widget() ?>
                <div class="container-fluid">
                    <?php  /* Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) */ ?>
                    <?php if (Yii::$app->user->isGuest) {
                        echo '<div style="text-align: center"><img src="logo.svg" style="height:150px;width: auto"/></div>';
                    } ?>
                    <?= $content ?>
                </div>
                <footer class="footer">
                    <div class="content">
                        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?>
                            <?php
                            $verzija = Postavke::find()->where(['postavka' => 'verzijaOrdyBee'])->one();
                            if (isset($verzija)) {
                                echo " ver. " . $verzija->vrijednost;
                            }
                            ?>

                            <?= date('Y') ?></p>

                        <p class="pull-right"><?= Yii::t('app', 'Powered by dikobraz') ?></p>
                    </div>
                </footer>
            </div>


        </div>
    </div>

    <?php $this->endBody() ?>
    <?php
    $year = PedagoskaGodina::find()->where(['>', 'od', date("Y-08-25")])->one();
    $temp = null;
    if (!is_null($year)) {
        $temp = 1;
    } else {
        $temp = 0;
    }

    ?>
    <script>
        $("#minimizeSidebar").click(function() {
            $("#sidebar").toggleClass("small");
            $(".sidebar-wrapper").toggleClass("small");
            $(".main-panel").toggleClass("big");
        });
        var weekday = new Array(7);
        weekday[0] = "Nedjelja";
        weekday[1] = "Ponedjeljak";
        weekday[2] = "Utorak";
        weekday[3] = "Srijeda";
        weekday[4] = "Četvrtak";
        weekday[5] = "Petak";
        weekday[6] = "Subota";

        setInterval(function() {
            var today = new Date();
            var minutes = today.getMinutes();
            minutes = minutes > 9 ? minutes : '0' + minutes;
            var date = weekday[today.getDay()] + ', ' + today.getDate() + '.' + (today.getMonth() + 1) + '.' + today.getFullYear() + '.';
            var time = today.getHours() + ":" + minutes;
            var dateTime = date + ' ' + time;
            if (<?= Yii::$app->user->isGuest ? 0 : 1 ?>) {
                var x = document.getElementsByClassName("navbar-brand");
                x[0].innerHTML = dateTime;

            }
        }, 1000);
    </script>
    <?php
    $js = <<<JS
$("[name='ped_godina']").on('change', function() {
  $(this).form().submit;
})
JS;

    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var itemsMainDiv = ('.MultiCarousel');
            var itemsDiv = ('.MultiCarousel-inner');
            var itemWidth = "";

            $('.leftLst, .rightLst').click(function() {
                var condition = $(this).hasClass("leftLst");
                if (condition)
                    click(0, this);
                else
                    click(1, this)
            });

            ResCarouselSize();




            $(window).resize(function() {
                ResCarouselSize();
            });

            //this function define the size of the items
            function ResCarouselSize() {
                var incno = 0;
                var dataItems = ("data-items");
                var itemClass = ('.item');
                var id = 0;
                var btnParentSb = '';
                var itemsSplit = '';
                var sampwidth = $(itemsMainDiv).width();
                var bodyWidth = $('body').width();
                $(itemsDiv).each(function() {
                    id = id + 1;
                    var itemNumbers = $(this).find(itemClass).length;
                    btnParentSb = $(this).parent().attr(dataItems);
                    itemsSplit = btnParentSb.split(',');
                    $(this).parent().attr("id", "MultiCarousel" + id);


                    if (bodyWidth >= 1200) {
                        incno = itemsSplit[3];
                        itemWidth = sampwidth / incno * 1.3;
                    } else if (bodyWidth >= 992) {
                        incno = itemsSplit[2];
                        itemWidth = sampwidth / incno * 1.3;
                    } else if (bodyWidth >= 768) {
                        incno = itemsSplit[1];
                        itemWidth = sampwidth / incno;
                    } else {
                        incno = itemsSplit[0];
                        itemWidth = sampwidth / incno;
                    }
                    $(this).css({
                        'transform': 'translateX(0px)',
                        'width': itemWidth * itemNumbers
                    });
                    $(this).find(itemClass).each(function() {
                        $(this).outerWidth(itemWidth);
                    });

                    $(".leftLst").addClass("over");
                    $(".rightLst").removeClass("over");

                });
            }


            //this function used to move the items
            function ResCarousel(e, el, s) {
                var leftBtn = ('.leftLst');
                var rightBtn = ('.rightLst');
                var translateXval = '';
                var divStyle = $(el + ' ' + itemsDiv).css('transform');
                var values = divStyle.match(/-?[\d\.]+/g);
                var xds = Math.abs(values[4]);
                if (e == 0) {
                    translateXval = parseInt(xds) - parseInt(itemWidth * s);
                    $(el + ' ' + rightBtn).removeClass("over");

                    if (translateXval <= itemWidth / 2) {
                        translateXval = 0;
                        $(el + ' ' + leftBtn).addClass("over");
                    }
                } else if (e == 1) {
                    var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                    translateXval = parseInt(xds) + parseInt(itemWidth * s);
                    $(el + ' ' + leftBtn).removeClass("over");

                    if (translateXval >= itemsCondition - itemWidth / 2) {
                        translateXval = itemsCondition;
                        $(el + ' ' + rightBtn).addClass("over");
                    }
                }
                $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
            }

            //It is used to get some elements from btn
            function click(ell, ee) {
                var Parent = "#" + $(ee).parent().attr("id");
                var slide = $(Parent).attr("data-slide");
                ResCarousel(ell, Parent, slide);
            }

        });
    </script>
    <script>
        var LHC_API = LHC_API || {};
        LHC_API.args = {
            mode: 'widget',
            lhc_base_url: '//pomoc.ordybee.com/',
            wheight: 450,
            wwidth: 350,
            pheight: 520,
            pwidth: 500,
            domain: '<?php echo $_SERVER["HTTP_HOST"]; ?>',
            leaveamessage: true,
            check_messages: false,
            identifier: '<?php echo $_SERVER["HTTP_HOST"]; ?>',
            lang: 'hrv/'
        };
        (function() {
            var po = document.createElement('script');
            po.type = 'text/javascript';
            po.setAttribute('crossorigin', 'anonymous');
            po.async = true;
            var date = new Date();
            po.src = '//pomoc.ordybee.com/design/defaulttheme/js/widgetv2/index.js?' + ("" + date.getFullYear() + date.getMonth() + date.getDate());
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();
        $('tr.filters td').filter(function() {
            return $(this).html().indexOf('&nbsp') !== -1;

        }).addClass('mob-hide');
    </script>
    <script>
        $("td:empty")
            .text("-")
    </script>
    <script type="text/javascript">
        // making button for new school year visible depending on the date
        var string = <?php echo $temp; ?>;
        var bool = null;
        if (string == 0) {
            bool = false;
        } else {
            bool = true;
        }

        console.log(string);
        console.log(bool);

        function show_now() {
            var my_time = new Date(); //current date
            var month = my_time.getUTCMonth() + 1; //months from 1-12
            var day = my_time.getUTCDate(); //day of the month
            // console.log(month, day);
            if (((month = 8 && day >= 25) || (month = 9 && day <= 10)) && bool == false) {
                // console.log('true');
                document.getElementById('new_year').style.visibility = "visible";
            } else {
                // console.log('false');
                document.getElementById('new_year').style.visibility = "hidden";
            }
        }
        show_now();
    </script>
    <script type="text/javascript">
        function dnevnikRada(a) {
            var x = document.getElementById(a);
            //var z = document.getElementById('upload');
            var y = document.getElementById('buttons');
            document.querySelector("input[value=" + a + "]").checked = true;

            if (x.classList.contains("hidden")) {
                Array.from(document.getElementsByClassName("smallform")).forEach(
                    function(element) {
                        element.classList.remove("slidedown");
                    }
                );
                x.classList.add("slidedown");
                //if (a == 'neposredni' || a == 'posredni' || a == 'vanustanove') {
                //    z.classList.add("slidedown");
                //} else {
                //    z.classList.remove("slidedown");
                //}
                // y.style.display = "none";
            } else {
                x.classList.remove("slidedown");
                //z.classList.remove("slidedown");
            }
            // y.style.display = "block";
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tour/0.11.0/js/bootstrap-tour.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <!-- <script type="text/javascript">
        var whitelist = $.fn.tooltip.Constructor.DEFAULTS.whiteList;
        whitelist.button = ['data-role', 'style'];
        whitelist.img = ['style'];
        whitelist.div = ['style'];
        var tour = new Tour({
            steps: [{
                    element: "#demo",
                    title: "Logo",
                    content: "Možete ovdje staviti svoj logo u Postavkama",
                    placement: "bottom"
                },
                {
                    element: "#demo2",
                    title: "Prisutnost",
                    content: "Unesite svoju prisutnost",
                    placement: "top"
                }
            ],
            storage: false
        });

        // tour.init();
        // tour.start();
        tour.init();
        // tour.start();
        $("#startTourBtn").click(function() {
            tour.restart();
        });
    </script> -->
    <script>
        $(function() {
            $('input.dates').daterangepicker({
                opens: 'center',
                locale: {
                    format: 'DD.MM.YYYY.',
                    applyLabel: '  <span  class="material-icons">check</span>  ',
                    cancelLabel: '<span  class="material-icons danger">close</span>',
                },
                cancelButtonClasses: 'cancelBtn btn btn-sm btn-danger'
            }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#simplebtn").click(function() {
                $("#simple").show();
                $("#complex").hide();
            });
            $("#complexbtn").click(function() {
                $("#complex").show();
                $("#simple").hide();
            });
        });
    </script>
    <script>
        $(".alert").animate({
            opacity: 1
        }, 3000).fadeOut();
    </script>
    <script>
        function showmore() {
            $("#prisutne .hidden").fadeIn(1000).removeClass('hidden');
            $("#more").fadeOut(500);
        }
    </script>
    <script src="//cdn.jsdelivr.net/npm/featherlight@1.7.14/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/featherlight@1.7.14/release/featherlight.gallery.min.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>


<?php $this->endPage() ?>