<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use \ramosisw\CImaterial\widgets\Menu;


if (class_exists('ramosisw\CImaterial\web\MaterialAsset')) {
    ramosisw\CImaterial\web\MaterialAsset::register($this);
}
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="<?php echo Yii::getAlias('@web') . '/'; ?>/favicon.png" type="image/png" sizes="16x16">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap" rel="stylesheet">
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrapper ">





            <?php $back=Yii::$app->user->isGuest?'back':'';?>
            <div class="content <?= $back ?>">

                <?= Alert::widget() ?>
                <div class="container-fluid">
                    <?php  /* Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) */ ?>
                    <?php if (Yii::$app->user->isGuest) {
                       echo '<div style="text-align: center"><img src="logo.svg" class="logo" /></div><style>.logo{height:150px;width:auto}</style>';
                    }?>
                    <?= $content ?>
                </div>
                <footer class="footer">
    <div class="content">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::t('app','Powered by dikobraz')?></p>
    </div>
</footer>
            </div>

    </div>

    <?php $this->endBody() ?>

    <script>
        $("#minimizeSidebar").click(function() {
            $("#sidebar").toggleClass("small");
            $(".sidebar-wrapper").toggleClass("small");
            $(".main-panel").toggleClass("big");
        });
        var weekday = new Array(7);
        weekday[0] = "Nedjelja";
        weekday[1] = "Ponedjeljak";
        weekday[2] = "Utorak";
        weekday[3] = "Srijeda";
        weekday[4] = "Četvrtak";
        weekday[5] = "Petak";
        weekday[6] = "Subota";

        setInterval(function() {
            var today = new Date();
            var minutes = today.getMinutes();
            minutes = minutes > 9 ? minutes : '0' + minutes;
            var date = weekday[today.getDay()] + ', ' + today.getDate() + '.' + (today.getMonth() + 1) + '.' + today.getFullYear() + '.';
            var time = today.getHours() + ":" + minutes;
            var dateTime = date + ' ' + time;
            if (<?= Yii::$app->user->isGuest?0:1 ?>) {
                var x = document.getElementsByClassName("navbar-brand");
                x[0].innerHTML = dateTime;
                console.log(x[0].innerHTML = dateTime);
            }



        }, 1000);
    </script>
    <?php
    $js = <<<JS
$("[name='ped_godina']").on('change', function() {
  $(this).form().submit;
})
JS;

    ?>
    <script type="text/javascript">
    $(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

   ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno*1.3;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno*1.3;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});
</script>
</body>

</html>


<?php $this->endPage() ?>