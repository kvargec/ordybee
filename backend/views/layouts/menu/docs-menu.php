<?php
use \ramosisw\CImaterial\widgets\Menu;
use yii\helpers\Html;
echo Menu::widget([
        'itemOptions' => ['class' => 'nav-item'],
        'options' => ['class' => 'nav'],
        //'labelTemplate'=>'<i class="material-icons">{icon}</i><p>{label}</p>',
        //  'linkTemplate'=>'<a href="{url}" class="nav-link"><i class="material-icons">{icon}</i><p>{label}</p></a>',
        'items' => [
            // Important: you need to specify url as 'controller/action',
            // not just as 'controller' even if default action is used.
            ['label' => yii::t("app", "Nadzorna ploča"), 'icon' => 'dashboard', 'url' => ['/site/index']],
            ['label' => yii::t("app", "Osnovno"), 'icon' => 'account_tree', 'url' => ['/site/docs']],
             ['label' => yii::t("app", "Modul djece"), 'icon' => 'family_restroom', 'url' => ['/site/docsdjeca'], 'active' => $this->context->route == 'site/docsdjeca'],
             ['label' => yii::t("app", "Modul službe"), 'icon' => 'folder_shared', 'url' => ['/site/docssluzbe'], 'active' => $this->context->route == 'site/docssluzbe'],
             ['label' => yii::t("app", "Modul administracije"), 'icon' => 'meeting_room', 'url' => ['/site/docsadmini'], 'active' => $this->context->route == 'site/docsadmini']
            //  ['label' => yii::t("app", "Službe"), 'icon' => 'folder_shared', 'url' => ['/site/docssluzbe'], 'active' => $this->context->route == 'site/docssluzbe' ]
           

        ],


    ]).'<li class="mob">'
    . Html::beginForm(['/site/logout'], 'post')
    . Html::submitButton(
        Yii::$app->user->identity->username . ' '. '<i class="material-icons">logout</i> ' ,
        ['class' => 'btn btn-link logout']
    )
    . Html::endForm()
    . '</li>';;

