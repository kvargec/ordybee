<?php

use \ramosisw\CImaterial\widgets\Menu;
use yii\helpers\Html;
use common\models\Zaposlenik;

$user = Yii::$app->user->id;
$zaposlenik = Zaposlenik::find()->where(['user' => $user])->one();
// echo $zaposlenik->id;
echo Menu::widget([
    'itemOptions' => ['class' => 'nav-item'],
    'options' => ['class' => 'nav'],
    //'labelTemplate'=>'<i class="material-icons">{icon}</i><p>{label}</p>',
    //  'linkTemplate'=>'<a href="{url}" class="nav-link"><i class="material-icons">{icon}</i><p>{label}</p></a>',
    'items' => [
        // Important: you need to specify url as 'controller/action',
        // not just as 'controller' even if default action is used.
        ['label' => yii::t("app", "Nadzorna ploča"), 'icon' => 'dashboard', 'url' => ['/site/index']],
        ['label' => yii::t("app", "Djeca"), 'icon' => 'face', 'url' => ['#'], 'items' => [
            ['label' => yii::t("app", "Skupine"), 'icon' => 'groups', 'url' => ['/skupina/index'], 'active' => (Yii::$app->controller->id == 'skupina' || $this->context->route == 'prisutnost/djeca' || $this->context->route == 'prisutnost/razlog-izostanka')],
            ['label' => yii::t("app", "Popis djece u sustavu"), 'icon' => 'child_care', 'url' => ['/dijete/index'], 'active' => Yii::$app->controller->id == 'dijete'],
        ]],
        // 'Products' menu item will be selected as long as the route is 'product/index'
        /*   ['label' => yii::t("app", "Djeca"), 'icon' => 'face', 'url' => ['#'], 'items' => [
                   ['label' => yii::t("app", "Skupine"), 'icon' => 'groups', 'url' => ['/skupina/index'], 'active' => (Yii::$app->controller->id == 'skupina' || $this->context->route == 'prisutnost/djeca' || $this->context->route == 'prisutnost/razlog-izostanka')],
                   ['label' => yii::t("app", "Zahtjevi"), 'icon' => 'assignment', 'url' => ['/zahtjev/index'], 'active' => Yii::$app->controller->id == 'zahtjev'],
                   ['label' => yii::t("app", "Popis djece u sustavu"), 'icon' => 'child_care', 'url' => ['/dijete/index'], 'active' => Yii::$app->controller->id == 'dijete'],
                   ['label' => yii::t("app", "Upisne grupe"), 'icon' => 'how_to_reg', 'url' => ['/upisne-grupe/index'], 'active' => (Yii::$app->controller->id == 'upisne-grupe')],
                   ['label' => yii::t("app", "Rang liste"), 'icon' => 'list', 'url' => ['/zahtjev/rang-lista'], 'active' => ($this->context->route == 'zahtjev/rang-lista')],
                   ['label' => yii::t("app", "Intervjui"), 'icon' => 'forum', 'url' => ['/intervju/index'], 'active' => ($this->context->route == 'intervju/index')],
                   ['label' => yii::t("app", "Dokumenti"), 'icon' => 'description', 'url' => ['/document-zahtjev/index']],
               ]],
               ['label' => yii::t("app", "Organizacija"), 'icon' => 'business_center', 'url' => ['#'], 'items' => [
                   ['label' => yii::t("app", "Radna mjesta"), 'icon' => 'person_pin', 'url' => ['/radno-mjesto/index'], 'active' => (Yii::$app->controller->id == 'radno-mjesto')],
                   ['label' => yii::t("app", "Vrste zaposlenja"), 'icon' => 'person_pin_circle', 'url' => ['/vrsta-zaposlenja/index'], 'active' => (Yii::$app->controller->id == 'vrsta-zaposlenja')],
                   ['label' => yii::t("app", "Zaposlenici"), 'icon' => 'supervisor_account', 'url' => ['/zaposlenik/index'], 'active' => (Yii::$app->controller->id == 'zaposlenik') || $this->context->route == 'prisutnost/zaposlenici'],
                   ['label' => yii::t("app", "Zaposlenja"), 'icon' => 'manage_accounts', 'url' => ['/zaposlenje/index'], 'active' => (Yii::$app->controller->id == 'zaposlenje')],
                   ['label' => yii::t("app", "Objekti"), 'icon' => 'home', 'url' => ['/objekt/index'], 'active' => Yii::$app->controller->id == 'objekt'],

                   ['label' => yii::t("app", "Dnevnici"), 'icon' => 'book', 'url' => ['/dnevnik/index2']],
                   ['label' => yii::t("app", "Ugovori"), 'icon' => 'description', 'url' => ['#']],
               ]],*/
        //['label' => yii::t("app", "Službe"), 'icon' => 'folder_shared', 'url' => ['#'], 'items' => [
        /*['label' => yii::t("app", "Zdravstvena"), 'icon' => 'local_hospital', 'url' => ['#'], 'items' => [
                ['label' => yii::t("app", "Kartoni"), 'icon' => 'library_books', 'url' => ['/dnevnik/index', 'tip' => 'zdravstvo'], 'active' => (isset($_GET['tip'])?$_GET['tip'] == 'zdravstvo'?true:false:false)],
                ['label' => yii::t("app", "Jelovnik"), 'icon' => 'menu_book', 'url' => ['/jelovnik/view'], 'active' => (Yii::$app->controller->id == 'jelovnik')],
                ['label' => yii::t("app", "Jela"), 'icon' => 'fastfood', 'url' => ['/jelo/index'], 'active' => (Yii::$app->controller->id == 'jelo')],

            ]],*/
        /* ['label' => yii::t("app", "Logoped / rehabilitator"), 'icon' => 'hearing', 'url' => ['#'], 'items' => [
                 ['label' => yii::t("app", "Kartoni"), 'icon' => 'library_books', 'url' => ['/dnevnik/index', 'tip' => 'logoped'],'active' => (isset($_GET['tip'])?$_GET['tip'] == 'logoped'?true:false:false)],
             ]],
            ['label' => yii::t("app", "Psiholog"), 'icon' => 'recent_actors', 'url' => ['#'], 'items' => [
                ['label' => yii::t("app", "Kartoni"), 'icon' => 'library_books', 'url' => ['/dnevnik/index', 'tip' => 'psiholog'],'active' => (isset($_GET['tip'])?$_GET['tip'] == 'psiholog'?true:false:false)],
            ]],
                   ['label' => yii::t("app", "Pedagog"), 'icon' => 'escalator_warning', 'url' => ['#'], 'items' => [
                    ['label' => yii::t("app", "Kartoni"), 'icon' => 'library_books', 'url' => ['/dnevnik/index', 'tip' => 'pedagog'],'active' => (isset($_GET['tip'])?$_GET['tip'] == 'pedagog'?true:false:false)],
                ]],*/
        //['label' => yii::t("app", "Odgajatelj"), 'icon' => 'escalator_warning', 'url' => ['#'], 'items' => [
        ['label' => yii::t("app", "Dnevnici rada"), 'icon' => 'schedule', 'url' => ['/dnevnik-zapis/dnevnik-rada-index-zap', 'id' => $zaposlenik->id]],
        ['label' => yii::t("app", "Kartoni"), 'icon' => 'library_books', 'url' => ['/dnevnik/index', 'tip' => 'odgajatelj'], 'active' => (isset($_GET['tip']) ? $_GET['tip'] == 'odgajatelj' ? true : false : false)],
        //]],
        //  ]],

        /*  ['label' => yii::t("app", "Poslovno"), 'icon' => 'meeting_room', 'url' => ['#'], 'items' => [
                 ['label' => yii::t("app", "Ravnateljstvo"), 'icon' => 'supervisor_account', 'url' => ['#'],'items'=>[
                     ['label' => yii::t("app", "Izvještaji"), 'icon' => 'book', 'url' => ['/site/report']],
                     ['label' => yii::t("app", "Zaposlenici"), 'icon' => 'book', 'url' => ['/zaposlenik/index2']],
                 ]
                 ],
                 ['label' => yii::t("app", "Tajništvo"), 'icon' => 'work', 'url' => ['#'],
                     'items'=>[
                         ['label' => yii::t("app", "Zaposlenici"), 'icon' => 'book', 'url' => ['/zaposlenik/index2']],
                         ['label' => yii::t("app", "Dokumenti"), 'icon' => 'book', 'url' => ['/document/index']],
                     ]
                 ],
                 ['label' => yii::t("app", "Rač."), 'icon' => 'home', 'url' => ['#'],
                     'items'=>[
                         ['label' => yii::t("app", "Zaposlenici"), 'icon' => 'book', 'url' => ['/zaposlenik/index3']],
                         ['label' => yii::t("app", "Djeca"), 'icon' => 'book', 'url' => ['/skupina/index']],
                     ],
                 ],
                 ['label' => yii::t("app", "Kalendari"), 'icon' => 'calendar_today', 'url' => ['#'],'items'=>[
                     ['label' => yii::t("app", "Praznici"), 'icon' => 'book', 'url' => ['/calendar/index']],
                     ['label' => yii::t("app", "Aktivnosti"), 'icon' => 'book', 'url' => ['/aktivnosti/index']],
                 ]

                 ],
             ]],*/
        /*['label' => yii::t("app", "Postavke"), 'icon' => 'settings', 'url' => ['#'], 'items' => [
                // ['label' => yii::t("app", "Ped. godina"), 'icon' => 'timer', 'url' => ['/'], ],
                //  ['label' => yii::t("app", "Kalendar"), 'icon' => 'calendar_today', 'url' => ['/calendar'],],
                ['label' => yii::t("app", "Korisnici"), 'icon' => 'supervised_user_circle', 'url' => ['/user/admin'], 'active' => in_array(Yii::$app->controller->id, ['admin', 'role', 'permission', 'rule'])],
                ['label' => yii::t("app", "Predlošci dokumenata"), 'icon' => 'settings_brightness', 'url' => ['/dokument-template']],
                ['label' => yii::t("app", "Postavke"), 'icon' => 'settings', 'url' => ['/postavke']],
            ]],*/

        ['label' => yii::t("app", "Upitnici"), 'icon' => 'poll', 'url' => ['/upitnik/index'], 'active' => Yii::$app->controller->id == 'upitnik'],
        //['label' => yii::t("app", "Notifikacije"), 'icon' => 'notifications', 'url' => ['/notifikacije/index', 'inbox' => Yii::$app->user->id], 'active' => Yii::$app->controller->id == 'notifikacije'],
        ['label' => yii::t("app", "Poruke"), 'icon' => 'email', 'url' => ['/mailbox/index'], 'active' => Yii::$app->controller->id == 'poruke'],
        ['label' => yii::t("app", "Media"), 'icon' => 'perm_media', 'url' => ['/media/index'], 'active' => Yii::$app->controller->id == 'media'],

        //['label' => yii::t("app", "Tvrtke"), 'icon' => 'business', 'url' => ['/tvrtka/index'], 'active' => Yii::$app->controller->id == 'tvrtka'],
        ['label' => Yii::t('app', 'Login'), 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest, 'template' => '<a href="{url}" class="mob"><span>{label}</span></a>'],
        ['label' => '( 0 )', 'icon' => 'list', 'url' => ['/zahtjev/rang-lista'], 'template' => '<a href="{url}" class="mob">{icon}{label}</a>'],
        ['label' => '', 'icon' => 'calendar_today', 'url' => ['/calendar/index2'], 'template' => '<a href="{url}" class="mob">{icon}{label}</a>'],
        ['label' => yii::t("app", "Moj račun"), 'icon' => 'person', 'url' => ['/user/settings/account', 'id' => Yii::$app->user->identity->id], 'active' => in_array(Yii::$app->controller->id, ['settings'])],



    ],


]) . '<li class="mob">'
    . Html::beginForm(['/site/logout'], 'post')
    . Html::submitButton(
        Yii::$app->user->identity->username . ' ' . '<i class="material-icons">logout</i> ',
        ['class' => 'btn btn-link logout']
    )
    . Html::endForm()
    . '</li>';;
