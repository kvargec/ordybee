<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Dnevnik */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dnevniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="dnevnik-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sifra',
            'zaposlenik',
            'ped_godina',
            'vrsta_dnevnika',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <h3><?= Yii::t('app','Zapisi dnevnika') ?></h3>

    <?php Pjax::begin(['id' => 'zapises']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'zapis',
            'vrsta0.naziv',
            'status0.naziv',
            'created_at',
            'updated_at',

            ['class' => 'yii\grid\ActionColumn',
            'controller' => 'dnevnik-zapis' ],
        ],
    ]); ?>

    <?php Pjax::end() ?>

    <h3><?= Yii::t('app','Stvaranje novog zapisa') ?></h3>

    <!-- Render create form -->    
    <?= $this->render('createDnevnikZapis', [
        'model' => $dnevnikZapis,
        'attributes' => $attributes,
    ])  ?>

</div>
