<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dnevnik */

$this->title = Yii::t('app', 'Create Dnevnik');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dnevniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dnevnik-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
