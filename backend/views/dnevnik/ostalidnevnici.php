<?php

use kartik\widgets\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\export\ExportMenu;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DnevnikSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dnevnik') . " - " . $tip;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="dnevnik-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            $gridColumns = [
                //['class' => 'yii\grid\SerialColumn'],

                //    'id',
                [
                    'value' => function ($data) {
                        return $data->ime . ' ' . $data->prezime . ' (' . $data->spol . ')'; // $data['name'] for array data, e.g. using SqlDataProvider.
                    },
                    'label' => Yii::t('app', 'Ime Prezime (spol)'),
                    'attribute' => 'ime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime prezime'),
                        'title' => Yii::t('app', 'Ime'),
                        'class' => 'form-control'
                    ],
                    'filterAttribute' => 'full_name'
                ],
                'dat_rod',
                [
                    'label' => Yii::t('app', 'Skupina'),
                    'value' => 'skupina0.naziv',
                    'attribute' => 'skupina',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Naziv skupine'),
                        'title' => Yii::t('app', 'Skupina'),
                        'class' => 'form-control'
                    ],
                ],
                [
                    'label' => Yii::t('app', 'Zapisa'),
                    'value' => function ($data) use ($tip) {
                        return count($data->brojZapisa($tip));
                    }
                ],
                [
                    'label' => Yii::t('app', 'Status'),
                    'attribute' => 'status',
                    'value' => 'status0.status',
                    'filter' => Arrayhelper::map(\common\models\StatusDjeteta::find()->asArray()->all(), 'id', 'status')
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}  ',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) use ($tip) {

                            $url2 = \Yii::$app->urlManager->createUrl(['dijete-zapis/dijete', 'id' => $data->id, 'tip' => $tip]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pregledaj zapise'),
                                'class' => 'text-left  hgreen',
                            ]);
                        },



                    ],
                ],

            ];
            ?>
            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                    ExportMenu::FORMAT_CSV => null,
                ],
                'filename' => 'popisDnevnika' . '-' . $tip,
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/dijete/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" .
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                ]); ?>


        </div>
    </div>
</div>