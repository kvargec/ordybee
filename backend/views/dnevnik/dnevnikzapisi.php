<?php

use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Zapisi dnevnika');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dnevnik-index">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
</div>
<div class="card-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //    'id',

            [

                    'label'=>Yii::t('app','Dijete'),
                    'value'=>function($data){
                        return $data->dijeteZapis->dijete0->ime.' '.$data->dijeteZapis->dijete0->prezime;
                    }
            ],
            'zapis',
            [
                    'format'=>'html',
                    'label'=>Yii::t('app','Vrijednosti'),
                    'value'=>function($data){
                        $tmp = json_decode($data->vrijednosti_atributa);
                        $txt='';
                        if (is_iterable($tmp)) {
                            foreach ($tmp as $key => $value) { 
                                $txt .= $key . ': ' . $value . '<br>';
                            }
                            return $txt;
                        }
                    }
            ],
            'created_at',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}  ',
                // 'visible' => Yii::$app->user->can('admin'),
                'buttons' => [
                    'view' => function ($url, $data) {


                            $url2 = \Yii::$app->urlManager->createUrl(['dijete-zapis/view', 'id'=>$data->dijeteZapis->id]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pregledaj zapise'),
                                'class' => 'text-left  hgreen',
                            ]);


                    },



                ],
            ],

        ],
    ]); ?>

    </div>
</div>
</div>