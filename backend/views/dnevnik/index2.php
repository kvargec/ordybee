<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Dnevnici');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dnevnik-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <a href="<?= \yii\helpers\Url::to(['/dnevnik-zapis/creatednevnikrada']) ?>" class="btn btn-success"><span class="material-icons">add</span> Dnevnik rada</a>
        </div>
        <div class="card-body">
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn'],

                //    'id',
                [
                    'value' => 'sifra',
                    'label' => Yii::t('app', 'Šifra'),
                    'attribute' => 'sifra',
                    'filterOptions' => [
                        'class' => 'form-group',
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Šifra'),
                        'title' => Yii::t('app', 'Šifra'),
                        'class' => 'form-control',
                    ],
                ],
                [
                    'value' => function ($data) {
                        return $data->zaposlenik0->ime . ' ' . $data->zaposlenik0->prezime;
                    },
                    'label' => Yii::t('app', 'Zaposlenik'),
                    'attribute' => 'ime',
                    'filterOptions' => [
                        'class' => 'form-group',
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime prezime'),
                        'title' => Yii::t('app', 'Ime'),
                        'class' => 'form-control',
                    ],
                    'filterAttribute' => 'full_name_zaposlenik'
                ],
                [
                    'value' => function ($data) {
                        if (!empty($data->dnevnikZapis)) {
                            return $data->dnevnikZapis[0]->vrsta0->title;
                        } else return;
                    },
                    'label' => Yii::t('app', 'Tip zapisa'),
                ],
                /*
        [
            'value' => function($data){
                    if(!empty($data->vrstaDnevnika)){
                        return $data->vrstaDnevnika->naziv;
                    }else return;
            },
            'label' => Yii::t('app','Tip dnevnika'),
        ],*/
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}  ',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) {
                            $zapis = $data->dnevnikZapis;
                            if (!empty($zapis)) {
                                $url2 = \Yii::$app->urlManager->createUrl(['dnevnik/index', 'tip' => $zapis[0]->vrsta0->grupa]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pregledaj zapise dnevnika'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            }
                            return;
                        },



                    ],
                ],

            ];
            ?>
            <?= ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                    ExportMenu::FORMAT_CSV => null,
                ],
                'filename' => 'popisDnevnika',
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/dijete/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" .
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                ]); ?>
        </div>
    </div>
</div>