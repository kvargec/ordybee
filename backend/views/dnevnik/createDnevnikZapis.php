<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model frontend\models\DnevnikZapis */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$this->registerJs(
   '$("document").ready(function(){ 
		$("#new_zapis").on("pjax:end", function() {
			$.pjax.reload({container:"#zapises"});  //Reload GridView
		});
    });',
    \yii\web\View::POS_READY
);
?>

<div class="zapis-form">

<?php //Pjax::begin(['id' => 'new_zapis']); ?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>
<div class="row">
    <div class="col-sm-3">
        <?= $form->field($model, "zapis")->textArea() ?>
    </div>
    <div class="col-sm-3">
        <?php
        $vrstaArray = ArrayHelper::map(
            \common\models\VrstaZapis::find()->all(),
            'id',
            'naziv'
        );
        echo $form->field($model, "vrsta")->dropDownList($vrstaArray, ['prompt' => Yii::t('app','---- Odaberi vrstu zapisa ----')]) ?>
    </div>
    <div class="col-sm-3">
        <?php
        $statusArray = ArrayHelper::map(
            \common\models\StatusOpce::find()->all(),
            'id',
            'naziv'
        );
        echo $form->field($model, "status")->dropDownList($statusArray, ['prompt' => Yii::t('app','---- Odaberi status zapisa ----')]) ?>
    </div>
</div>

<div class="override-material">
    <?php
    $formName = $model->formName();
    echo Form::widget([
        'formName'=>$formName . '[vrijednosti_atributa]',
        'columns'=>1,
        'attributes'=>$attributes,
    ]);
    ?>
</div>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
<?php //Pjax::end(); ?>

</div>