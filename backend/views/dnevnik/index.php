<?php

use kartik\widgets\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\export\ExportMenu;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DijeteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dnevnik') . " - " . $tip;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="card-body">
        <div class="row  m15 ml-0">
            <div class="col-lg-5 pl-0">
                <h3 class="lh-2"><?= Yii::t('app', 'Unos mjerenja za skupinu') ?></h3>
            </div>

            <?php echo Html::beginForm(['/dijete-zapis/mjerenje-skupina'], 'get', ['class' => 'posalji-potvrdu']);
            echo '<div class="col-lg-5">';
            echo  Select2::widget([
                'name' => 'skupina',
                'data' => ArrayHelper::map(
                    \common\models\Skupina::find()->all(),
                    'id',
                    function ($v) {
                        return $v->naziv;
                    }
                ),
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite skupinu...'),
                ],
            ]);
            echo '</div>';
            echo '<div class="col-lg-2">';
            echo '<div class="form-group submit">';
            echo Html::submitButton(Yii::t('app', '<span class="material-icons">done</span>'), array('class' => 'btn btn-info btn-sm send-form'));
            echo '</div>';
            echo '</div>';
            echo Html::endForm(); ?>





        </div>


        <?php // echo $this->render('_search', ['model' => $searchModel]); 
        $gridColumns = [
            // ['class' => 'yii\grid\SerialColumn'],

            //    'id',
            [
                'value' => function ($data) {
                    return $data->ime . ' ' . $data->prezime . ' (' . $data->spol . ')'; // $data['name'] for array data, e.g. using SqlDataProvider.
                },
                'label' => Yii::t('app', 'Ime Prezime (spol)'),
                'attribute' => 'ime',
                'filterOptions' => [
                    'class' => 'form-group',
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Ime prezime'),
                    'title' => Yii::t('app', 'Ime'),
                    'class' => 'form-control',
                ],
                'filterAttribute' => 'full_name'
            ],
            [
                'label' => Yii::t('app', 'Skupina'),
                'value' => 'skupina0.naziv',
                'attribute' => 'skupina',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Naziv skupine'),
                    'title' => Yii::t('app', 'Skupina'),
                    'class' => 'form-control'
                ],
            ],
            [
                'value' => function ($data) {
                    return $data->covidPotvrda;
                },
                'attribute' => 'covid_potvrda',
                'label' => Yii::t('app', 'COVID Potvrda'),
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'COVID Potvrda'),
                    'title' => Yii::t('app', 'COVID Potvrda'),
                    'class' => 'form-control'
                ],
            ],
            [
                'label' => '<span class="material-icons small-x">event</span><span class="material-icons red small-x">close</span>',
                'encodeLabel' => false,
                'value' => function ($data) {
                    return $data->getBrojMjIzostanaka() . '<span class="material-icons red small-x">close</span>';
                },
                'format' => 'html'
            ],
            [
                'label' => Yii::t('app', 'Periodi izostanka'),
                'contentOptions' => ['class' => 'text-left'],
                'format' => 'html',
                'value' => function ($data) {
                    $return = '';
                    foreach ($data->getIntervaliIzostanaka() as $interval) {
                        if ($interval->pocetak === $interval->kraj) {
                            $return .= Yii::$app->formatter->asDate($interval->pocetak) . ' ' . $interval->ukupno . Yii::t('app', ' dan') . '<br>';
                        } else {
                            $return .= Yii::$app->formatter->asDate($interval->pocetak) . ' - ' . Yii::$app->formatter->asDate($interval->kraj) . Yii::t('app', ' dana : ') . $interval->ukupno . '<br>';
                        }
                    }
                    return $return;
                }
            ],
            [
                'label' => Yii::t('app', 'Status'),
                'attribute' => 'status',
                'value' => 'status0.status',
                'filter' => Arrayhelper::map(\common\models\StatusDjeteta::find()->asArray()->all(), 'id', 'status'),
                'filterOptions' => [
                    'class' => 'form-group'
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}  ',
                // 'visible' => Yii::$app->user->can('admin'),
                'buttons' => [
                    'view' => function ($url, $data) use ($tip) {

                        $url2 = \Yii::$app->urlManager->createUrl(['dijete-zapis/dijete', 'id' => $data->id, 'tip' => $tip]);
                        return Html::a('<span class="material-icons">visibility</span>', $url2, [
                            'title' => Yii::t('app', 'Pregledaj zapise'),
                            'class' => 'text-left  hgreen',
                        ]);
                    },



                ],
            ],

        ];
        ?>

        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'exportConfig' => [
                ExportMenu::FORMAT_TEXT => null,
                ExportMenu::FORMAT_HTML => null,
                ExportMenu::FORMAT_CSV => null,
            ],
            'filename' => 'popisDnevnikaZdravstva',
            'stream' => false,
            'folder' => '@app/web/',
            //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
            //'target' => '_blank',
            'linkPath' => Url::toRoute('@web/web/'),
            'afterSaveView' => '/dijete/zalink',
            'dropdownOptions' => [
                'label' => Yii::t('app', 'Export All'),
                'class' => 'btn btn-outline-secondary'
            ]
        ]) . "<hr>\n" .
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
            ]); ?>


    </div>
</div>
</div>