<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Dnevnik */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dnevnik-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sifra')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zaposlenik')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Zaposlenik::find()->where(['!=', 'status', 3])->all(),
                    'id',
                    function ($v) {
                        return $v->getFullName();
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'vrsta_dnevnika')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\VrstaDnevnik::find()->all(),
                    'id',
                    'naziv'
                ),
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
