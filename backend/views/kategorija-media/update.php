<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KategorijaMedia */

$this->title = Yii::t('app', 'Update Kategorija Media: {name}', [
    'name' => $model->kategorija,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kategorija Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="objekt-create">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>