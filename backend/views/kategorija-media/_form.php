<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\KategorijaMedia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kategorija-media-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kategorija')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent')->textInput()->hiddenInput(['value' => '0'])->label(false) ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(
            \common\models\StatusOpce::find()->all(),
            'id',
            function ($status) {
                return $status->naziv;
            }
        ),
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>