<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Kontakt */

$this->title = Yii::t('app', 'Create Kontakt');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kontakts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kontakt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
