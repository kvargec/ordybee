<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ZaposlenikSkupina */

$this->title = Yii::t('app', 'Create Zaposlenik Skupina');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zaposlenik Skupinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zaposlenik-skupina-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
