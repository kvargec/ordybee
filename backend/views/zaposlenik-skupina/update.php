<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ZaposlenikSkupina */

$this->title = Yii::t('app', 'Update Zaposlenik Skupina: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zaposlenik Skupinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="zaposlenik-skupina-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
