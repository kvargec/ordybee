<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\AktivnostMediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Aktivnost Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aktivnost-media-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>



            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    // ['class' => 'yii\grid\SerialColumn'],

                    [
                        'value' => 'id',
                        'label' => Yii::t('app', 'Id'),
                        'attribute' => 'id',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Id'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'value' => 'aktivnost',
                        'label' => Yii::t('app', 'Aktivnost'),
                        'attribute' => 'aktivnost',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Aktivnost'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'value' => 'media',
                        'label' => Yii::t('app', 'Media'),
                        'attribute' => 'media',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Media'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'value' => 'redoslijed',
                        'label' => Yii::t('app', 'Redoslijed'),
                        'attribute' => 'redoslijed',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Redoslijed'),
                            'class' => 'form-control'
                        ]
                    ],
                    
                    'status',
                    //'extra_settings',

                    [
                        'class' =>  'yii\grid\ActionColumn',


                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/aktivnost-media/view', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'visibility')), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'update' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/aktivnost-media/update', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'edit')), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'delete' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/aktivnost-media/delete', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'delete')), [
                                    'class' => 'material-icons red'
                                ]), $url2, [
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>