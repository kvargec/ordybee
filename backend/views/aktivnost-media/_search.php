<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\AktivnostMediaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aktivnost-media-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'aktivnost') ?>

    <?= $form->field($model, 'media') ?>

    <?= $form->field($model, 'redoslijed') ?>

    <?= $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'extra_settings') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
