<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AktivnostMedia */

$this->title = Yii::t('app', 'Create Aktivnost Media');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aktivnost Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aktivnost-media-create">

<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>

        </div>
        <div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
