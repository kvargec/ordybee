<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\AktivnostMedia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aktivnost-media-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'aktivnost')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Aktivnosti::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'media')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Media::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'redoslijed')->textInput() ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\StatusOpce::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'extra_settings')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
