<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZahtjevSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rang lista');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zahtjev-index">
<div class="card">
    <div class="card-header card-header-danger">
    <span class="card-title"><?= Html::encode($this->title) ?></span>
    <?php
    $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/ispis-rang-liste-intervju']);
    echo Html::a('<i class="material-icons">print</i> '.Yii::t('app','Ispis liste'), $url2, [
        'title' => Yii::t('app', 'Ispis'),
        'class' => 'btn btn-success',
        'target' => '_blank',
    ]);

    ?>

    <?php
    $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/ispis-rang-liste-intervju2']);
    echo Html::a('<i class="material-icons">print</i> '.Yii::t('app','Ispis liste (interno)'), $url2, [
        'title' => Yii::t('app', 'Ispis'),
        'class' => 'btn btn-success',
        'target' => '_blank',
    ]);

    ?>
    <?php
    echo '<br>';
    $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/ispis-rang-liste-intervju','status'=>8]);
    echo Html::a('<i class="material-icons">print</i> '.Yii::t('app','Ispis liste s nep. dokumentacijom'), $url2, [
        'title' => Yii::t('app', 'Ispis'),
        'class' => 'btn btn-grey',
        'target' => '_blank',
    ]);

    ?>

    <?php
    $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/ispis-rang-liste-intervju2','status'=>8]);
    echo Html::a('<i class="material-icons">print</i> '. Yii::t('app','Ispis liste (interno) s nep. dokumentacijom'), $url2, [
        'title' => Yii::t('app', 'Ispis'),
        'class' => 'btn btn-grey',
        'target' => '_blank',
    ]);

    ?>
</div>
<div class="card-body">
    <?php
    $tempTekst='';

    $formatter = \Yii::$app->formatter;
    foreach($rezultati as $index=>$rezultat){
        $nazivGrupe=$upisneGrupe[$index-1];
        $tempTekst.='<h4 class="summary text-uppercase">'.$nazivGrupe->naziv.'  '.$formatter->asDate($nazivGrupe->date_poc,'php:j.n.Y.').'-'.$formatter->asDate($nazivGrupe->date_kraj,'php:j.n.Y.').'</h4>';
        $tempTekst.='<h4 class="summary">'.Yii::t('app','Lista primljene djece').'</h4>';
        $tempTekst.='<table id="w0" class="table table-striped table-bordered">
        <tr>
            <th>#</th>
            <th>'.Yii::t('app','Šifra').'</th>
          
            <th style="white-space:break-spaces;word-break:break-word;">'.Yii::t('app','Dat. rođenja').'</th>
            <th>'.Yii::t('app','Bodovi').'</th>
            <th>'.Yii::t('app','Datum i vrijeme').'</th>

         
        </tr>';
        $i=1;
        $j=1;
        foreach ($rezultat as $rt){
            $klasa='style="text-align:center"';
            $klasa2='style="text-align:left"';

            $tempTekst.= '<tr>';
            $tempTekst.='<td>'.$j.'.</td>';
            $temp=isset($rt['urbroj'])?$rt['urbroj']:'';
            $temp2=isset($rt['dijete_dat_rod'])?$formatter->asDate($rt['dijete_dat_rod'],'php:j. n. Y.'):'';
            $temp3=isset($rt['bodovi'])?$rt['bodovi']:'';
            $url2 = \Yii::$app->urlManager->createUrl(['zahtjev/view', 'id' => $rt['id']]);
            $forma = '';
            $forma .= Html::beginForm(['intervju/snimi', 'id' => $rt['id']], 'post', ['class' => 'posalji-potvrdu']);
            $forma.='<label class="control-label">'.Yii::t('app','Datum i vrijeme intervjua').'</label>';
            $intervju=\common\models\Intervju::find()->where(['zahtjev'=>$rt['id']])->one();
            $datum=isset($intervju)?$intervju->vrijeme:'';
            $forma.=DateTimePicker::widget( [
                'options' => ['placeholder' => Yii::t('app','Odaberite datum i vrijeme...')],
                'value'=>$datum,
                'name'=>'vrijeme',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii'
                ]
            ]);

           /* $forma .= '<br/>Lokacija:';
            $forma .= Html::input('lokacija', 'lokacija', 'Glavni objekt', ['required'=>true]); */
            $forma .= '<br/>';
            $forma .= Html::submitButton(Yii::t('app','Spremi'), array('class' => 'btn btn-info btn-sm'));
            $forma .= Html::endForm();

            $tempTekst.= '<td '.$klasa.'>'.$temp.'</td>';
            $tempTekst.= '<td >'.$temp2.'</td>';
            $tempTekst.= '<td '.$klasa.'>'.$temp3.'</td>';
            $tempTekst.= '<td ' . $klasa . '>' . $forma . '</td>';
            $tempTekst.= '</tr>';

            if($nazivGrupe->broj==$i){
                break;
                $tempTekst.= '</table>';
                $tempTekst.='<h4>'.Yii::t('app','Lista čekanja').'</h4>';
                $tempTekst.='<table id="w0" class="table table-striped table-bordered" >
                    <tr>
                        <th>#</th>
                        <th>'.Yii::t('app','Šifra').'</th>
                 
                        <th>'.Yii::t('app','Dat.rođenja').'</th>
                        <th>'.Yii::t('app','Bodovi').'</th>
                        <th></th>
                     
                    </tr>';
                $j=0;
            }else{
                $klasa='style="text-align:center"';

            }
            $i++;
            $j++;
        }
        $tempTekst.='</table>';
        $tempTekst.='<hr />';
    }
    echo $tempTekst;

    $js = <<< JS
$(".posalji-potvrdu").on("submit", function(e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var data = $(this).serializeArray();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data
    }).done(function(data, textStatus, jqXHR) {
        $.notify(data.message, "success");
    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert( errorThrown );
    });
});
JS;

    $this->registerJs($js); 
    ?>

</div></div>
</div>