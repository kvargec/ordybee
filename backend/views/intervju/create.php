<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Intervju */

$this->title = Yii::t('app', 'Create Intervju');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Intervjus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intervju-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
