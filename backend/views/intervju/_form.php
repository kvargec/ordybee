<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Intervju */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intervju-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'zahtjev')->textInput() ?>

    <?= $form->field($model, 'vrijeme')->textInput() ?>

    <?= $form->field($model, 'lokacija')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'napomena')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'odrzan')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
