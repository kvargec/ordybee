<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\IntervjuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intervju-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'zahtjev') ?>

    <?= $form->field($model, 'vrijeme') ?>

    <?= $form->field($model, 'lokacija') ?>

    <?= $form->field($model, 'napomena') ?>

    <?php // echo $form->field($model, 'odrzan')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
