<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DnevnikZapis */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dnevnik Zapis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="dnevnik-zapis-view">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="card-body detail">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'dnevnik',
                    'zapis:ntext',
                    'vrsta',
                    'status',
                    // 'vrijednosti_atributa',
                    [
                        'attribute' => 'vrijednosti_atributa',
                        'value' => function ($model) {
                            return json_encode($model->vrijednosti_atributa, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
                        },
                    ],
                    'created_at',
                    'updated_at',
                ],
            ]) ?>

        </div>
    </div>
</div>