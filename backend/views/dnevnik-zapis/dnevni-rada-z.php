<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DnevnikZapisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$podaci = $dataProvider->getModels();
if (isset($podaci[0])) {
    $this->params['breadcrumbs'][] = Yii::t('app', 'Dnevnici rada') . '-' . $podaci[0]->dnevnik0->zaposlenik0->fullName;
    $this->title = Yii::t('app', 'Dnevnici rada') . '-' . $podaci[0]->dnevnik0->zaposlenik0->fullName;
} else {
    $this->title = Yii::t('app', 'Dnevnici rada');
}

?>

<div class="dnevnik-zapis-index">

    <div class="card">

        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['creatednevnikrada'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]);

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //	'zapis:ntext',
                    // 'id',
                    [
                        'label' => Yii::t('app', 'Vrijeme'),
                        'format' => 'html',
                        'value' => function ($model, $widget) {
                            foreach ($model->vrijednosti_atributa as $key => $vrijednosti) {
                                if (isset($vrijednosti['start']) && isset($vrijednosti['end'])) {
                                    return 'od ' . $vrijednosti['start'] . '<br>do ' . $vrijednosti['end'];
                                }
                            }
                            /*function ($model) {
                                    return json_encode($model->vrijednosti_atributa, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
                                },*/
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Vrijeme (sati)'),
                        'format' => 'html',
                        'value' => function ($model, $widget) {
                            foreach ($model->vrijednosti_atributa as $key => $vrijednosti) {
                                if (isset($vrijednosti['start']) && isset($vrijednosti['end'])) {
                                    $start = strtotime($vrijednosti['start']);
                                    $end = strtotime($vrijednosti['end']);
                                    $trajanje = $end - $start;
                                    $trajanje_sati = $trajanje / 60 / 60;
                                    $trajanje_minute = round($trajanje / 60 / 60, 2) - floor($trajanje_sati);
                                    return floor($trajanje_sati) . ':' . round($trajanje_minute * 60);
                                }
                            }
                            /*function ($model) {
                                    return json_encode($model->vrijednosti_atributa, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
                                },*/
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Opis'),
                        'format' => 'html',
                        'value' => function ($model, $widget) { //Kod ispod je zbog različitih unosa u bazi - negdje s tekstom, negdje s id-em skupine/aktivnosti
                            foreach ($model->vrijednosti_atributa as $key => $vrijednosti) {
                                $grupacija = '';
                                if (isset($vrijednosti['grupacija'])) {
                                    $grupacija = $vrijednosti['grupacija'];
                                }
                                switch ($key) {
                                    case 'posredni':
                                        return implode('', array_map(
                                            function ($v, $k) use ($grupacija) { //proslijeđujemo grupaciju u callbacku
                                                if (is_array($v)) {
                                                    switch ($k) {
                                                        case 'grupacija':
                                                            return 'Grupacija: <ul>' . $v . '</li></ul>';
                                                            break;
                                                        case 'skupina':
                                                            $nazivi = [];
                                                            foreach ($v as $i) {
                                                                $skupine[] = \common\models\Skupina::find()->where(['id' => $i])->one();
                                                                foreach ($skupine as $skupina) {
                                                                    $nazivi[] = $skupina->naziv;
                                                                }
                                                            }
                                                            return 'Skupine: <ul>' . implode('<li>', $nazivi) . '</li></ul>';
                                                            break;
                                                        case 'aktivnost':
                                                            $v_array = [];
                                                            foreach ($v as $i) {
                                                                switch ($i) {
                                                                    case $i === 0:
                                                                        $v_array[] = 'Vođenje knjige pedagoške dokumentacije odgojno-obrazovne skupine';
                                                                        break;
                                                                    case 1:
                                                                        $v_array[] = 'Poslovi za ostvarivanje tjednog plana i programa (izrada sredstava za rad s djecom, uređenje prostora za realizaciju odgojno-obrazovnog rada)';
                                                                        break;
                                                                    case 2:
                                                                        $v_array[] = 'Dokumentiranje aktivnosti djece (razvojne mape, uradci djece, samorefleksije djece, narativni oblici, opservacije i postignuća djece)';
                                                                        break;
                                                                    case 3:
                                                                        $v_array[] = 'Dokumentiranje aktivnosti odgojitelja (samorefeksija i zajedničke refleksije odgojitelja ipedagoga)';
                                                                        break;
                                                                    case 4:
                                                                        $v_array[] = 'Suradnja s roditeljima (individualni razgovori/savjetodavni rad) (Ime i prezime roditelja, tema razgovora, na čiju inicijativu)';
                                                                        break;
                                                                    case 5:
                                                                        $v_array[] = 'Suradnja s pedagogom (uvid u praćenje i dokumentaciju o aktivnostima te razgovori o tekućoj problematici i situacijama)';
                                                                        break;
                                                                    case 6:
                                                                        $v_array[] = 'Stručni aktiv (navesti temu i tko je voditelj)';
                                                                        break;
                                                                    case 7:
                                                                        $v_array[] = 'Odgojiteljsko vijeće (navesti dnevni red)';
                                                                        break;
                                                                    case 8:
                                                                        $v_array[] = 'Roditeljski sastanci/edukativne i kreativne radionice/druženja s roditeljima (navesti temu te prisutne roditelje)';
                                                                        break;
                                                                    case 9:
                                                                        $v_array[] = 'Nešto drugo';
                                                                        break;
                                                                    default:
                                                                        $v_array[] = $i;
                                                                        break;
                                                                }
                                                            }
                                                            //print("<pre>".print_r($v_array,true)."</pre>");die();
                                                            return 'Aktivnosti: <ul>' . implode('<li>', $v_array) . '</li></ul>';
                                                            break;
                                                        case 'pojedinci':
                                                            $vrsta_pojedinca = '';
                                                            if ($grupacija == 'zaposlenici') {
                                                                $pojedinci[] = \common\models\Zaposlenik::find()->where(['id' => $v])->asArray()->all();
                                                                $vrsta_pojedinca = 'Zaposlenici: ';
                                                            } elseif ($grupacija == 'djeca') {
                                                                $pojedinci[] = \common\models\Dijete::find()->where(['id' => $v])->asArray()->all();
                                                                $vrsta_pojedinca = 'Djeca';
                                                            } elseif ($grupacija == 'roditelji') {
                                                                $pojedinci[] = \common\models\Roditelj::find()->where(['id' => $v])->asArray()->all();
                                                                $vrsta_pojedinca = 'Roditelji: ';
                                                            }
                                                            $pojedinci = array_merge(...$pojedinci);
                                                            $temp = [];
                                                            foreach ($pojedinci as $pojedinac) {
                                                                $temp[] = $pojedinac['ime'] . ' ' . $pojedinac['prezime'];
                                                            }
                                                            return $vrsta_pojedinca . '<ul>' . implode('<li>', $temp) . '</li></ul>';
                                                            break;
                                                        default:
                                                            return $k . '[]=' . implode('&' . $k . '[]=', $v) . '<br/>';
                                                    }
                                                } else {
                                                    switch ($k) {
                                                        case 'grupacija':
                                                            return 'Grupacija: <ul>' . $v . '</li></ul>';
                                                            break;
                                                        case 'skupina':
                                                            if (!isset($v)) {
                                                                $skupina = \common\models\Skupina::find()->where(['id' => $v])->one();
                                                                return 'Skupine: <ul><li>' . $skupina->naziv . '</li></ul>';
                                                            }
                                                            break;
                                                        case 'aktivnost':
                                                            $i = '';
                                                            switch ($v) {
                                                                case $v === 0:
                                                                    $i = 'Vođenje knjige pedagoške dokumentacije odgojno-obrazovne skupine';
                                                                    break;
                                                                case 1:
                                                                    $i = 'Poslovi za ostvarivanje tjednog plana i programa (izrada sredstava za rad s djecom, uređenje prostora za realizaciju odgojno-obrazovnog rada)';
                                                                    break;
                                                                case 2:
                                                                    $i = 'Dokumentiranje aktivnosti djece (razvojne mape, uradci djece, samorefleksije djece, narativni oblici, opservacije i postignuća djece)';
                                                                    break;
                                                                case 3:
                                                                    $i = 'Dokumentiranje aktivnosti odgojitelja (samorefeksija i zajedničke refleksije odgojitelja ipedagoga)';
                                                                    break;
                                                                case 4:
                                                                    $i = 'Suradnja s roditeljima (individualni razgovori/savjetodavni rad) (Ime i prezime roditelja, tema razgovora, na čiju inicijativu)';
                                                                    break;
                                                                case 5:
                                                                    $i = 'Suradnja s pedagogom (uvid u praćenje i dokumentaciju o aktivnostima te razgovori o tekućoj problematici i situacijama)';
                                                                    break;
                                                                case 6:
                                                                    $i = 'Stručni aktiv (navesti temu i tko je voditelj)';
                                                                    break;
                                                                case 7:
                                                                    $i = 'Odgojiteljsko vijeće (navesti dnevni red)';
                                                                    break;
                                                                case 8:
                                                                    $i = 'Roditeljski sastanci/edukativne i kreativne radionice/druženja s roditeljima (navesti temu te prisutne roditelje)';
                                                                    break;
                                                                case 9:
                                                                    $i = 'Nešto drugo';
                                                                    break;
                                                                default:
                                                                    $i = $v;
                                                            }
                                                            return 'Aktivnosti: <ul><li>' . $i . '</li></ul>';
                                                            break;
                                                        case 'end':
                                                            // return 'Kraj' . ': ' . Yii::$app->formatter->asDate($v, 'php:j.n.Y. H:i').'<br>';
                                                            return ' ';
                                                            break;
                                                        case 'start':
                                                            // return 'Početak' . ': ' .  Yii::$app->formatter->asDate($v, 'php:j.n.Y. H:i').'<br/>';
                                                            return ' ';
                                                            break;
                                                        case 'vrsta':
                                                            return 'Vrsta' . ': ' . $v . '<br/>';
                                                            break;
                                                        case 'zapis_custom':
                                                            return null;
                                                            break;
                                                        default:
                                                            return $k . ': ' . $v . '<br/>';
                                                    }
                                                }
                                            },
                                            $vrijednosti,
                                            array_keys($vrijednosti)
                                        ));
                                        break;
                                    case 'neposredni':
                                        return implode('', array_map(
                                            function ($v, $k) use ($grupacija) {
                                                if (is_array($v)) {
                                                    switch ($k) {
                                                        case 'skupina':
                                                            $nazivi = [];
                                                            foreach ($v as $i) {
                                                                $skupine[] = \common\models\Skupina::find()->where(['id' => $i])->one();
                                                                foreach ($skupine as $skupina) {
                                                                    $nazivi[] = $skupina->naziv;
                                                                }
                                                            }
                                                            return 'Skupine: <ul>' . implode('<li>', $nazivi) . '</li></ul>';
                                                            break;
                                                        case 'aktivnost':
                                                            $v_array = [];
                                                            foreach ($v as $i) {
                                                                switch ($i) {
                                                                    case $i === 0:
                                                                        $v_array[] = 'Životno praktične i radne';
                                                                        break;
                                                                    case 1:
                                                                        $v_array[] = 'Raznovrsne igre';
                                                                        break;
                                                                    case 2:
                                                                        $v_array[] = 'Društvene i društvene zabavne';
                                                                        break;
                                                                    case 3:
                                                                        $v_array[] = 'Umjetničke';
                                                                        break;
                                                                    case 4:
                                                                        $v_array[] = 'Raznovrsno izražavanje i stvaranje';
                                                                        break;
                                                                    case 5:
                                                                        $v_array[] = 'Istraživačko spoznajne';
                                                                        break;
                                                                    case 6:
                                                                        $v_array[] = 'Specifične aktivnosti s kretanjem';
                                                                        break;
                                                                    default:
                                                                        $v_array[] = $i;
                                                                        break;
                                                                }
                                                            }
                                                            return 'Aktivnosti: <ul>' . implode('<li>', $v_array) . '</li></ul>';
                                                            break;
                                                        case 'pojedinci':
                                                            $vrsta_pojedinca = '';
                                                            if ($grupacija == 'zaposlenici') {
                                                                $pojedinci[] = \common\models\Zaposlenik::find()->where(['id' => $v])->asArray()->all();
                                                                $vrsta_pojedinca = 'Zaposlenici: ';
                                                            } elseif ($grupacija == 'djeca') {
                                                                $pojedinci[] = \common\models\Dijete::find()->where(['id' => $v])->asArray()->all();
                                                                $vrsta_pojedinca = 'Djeca';
                                                            } elseif ($grupacija == 'roditelji') {
                                                                $pojedinci[] = \common\models\Roditelj::find()->where(['id' => $v])->asArray()->all();
                                                                $vrsta_pojedinca = 'Roditelji: ';
                                                            }
                                                            $pojedinci = array_merge(...$pojedinci);
                                                            $temp = [];
                                                            foreach ($pojedinci as $pojedinac) {
                                                                $temp[] = $pojedinac['ime'] . ' ' . $pojedinac['prezime'];
                                                            }
                                                            return $vrsta_pojedinca . '<ul>' . implode('<li>', $temp) . '</li></ul>';
                                                            break;
                                                        default:
                                                            return $k . '[]=' . implode('&' . $k . '[]=', $v) . '<br/>';
                                                    }
                                                } else {
                                                    switch ($k) {
                                                        case 'grupacija':
                                                            return 'Grupa: <ul>' . $v . '</li></ul>';
                                                            break;
                                                        case 'skupina':
                                                            if (!isset($v)) {
                                                                $skupina = \common\models\Skupina::find()->where(['id' => $v])->one();
                                                                return 'Skupine: <ul><li>' . $skupina->naziv . '</li></ul>';
                                                            }
                                                            break;
                                                        case 'aktivnost':
                                                            $i = '';
                                                            switch ($v) {
                                                                case $v === 0:
                                                                    $i = 'Životno praktične i radne';
                                                                    break;
                                                                case 1:
                                                                    $i = 'Raznovrsne igre';
                                                                    break;
                                                                case 2:
                                                                    $i = 'Društvene i društvene zabavne';
                                                                    break;
                                                                case 3:
                                                                    $i = 'Umjetničke';
                                                                    break;
                                                                case 4:
                                                                    $i = 'Raznovrsno izražavanje i stvaranje';
                                                                    break;
                                                                case 5:
                                                                    $i = 'Istraživačko spoznajne';
                                                                    break;
                                                                case 6:
                                                                    $i = 'Specifične aktivnosti s kretanjem';
                                                                    break;
                                                                default:
                                                                    $i = $v;
                                                                    break;
                                                            }
                                                            return 'Aktivnosti: <ul><li>' . $i . '</li></ul>';
                                                            break;
                                                        case 'end':
                                                            // return 'Kraj' . ': ' . Yii::$app->formatter->asDate($v, 'php:j.n.Y. H:i').'<br>';
                                                            return ' ';
                                                            break;
                                                        case 'start':
                                                            // return 'Početak' . ': ' .  Yii::$app->formatter->asDate($v, 'php:j.n.Y. H:i').'<br/>';
                                                            return ' ';
                                                            break;
                                                        case 'vrsta':
                                                            return 'Vrsta' . ': ' . $v . '<br/>';
                                                            break;
                                                        case 'zapis_custom':
                                                            return '';
                                                            break;
                                                        default:
                                                            return $k . ': ' . $v . '<br/>';
                                                    }
                                                }
                                            },
                                            $vrijednosti,
                                            array_keys($vrijednosti)
                                        ));
                                        break;
                                    case 'vanustanove':
                                        return implode('', array_map(
                                            function ($v, $k) {
                                                if (is_array($v)) {
                                                    switch ($k) {
                                                        case 'skupina':
                                                            $nazivi = [];
                                                            foreach ($v as $i) {
                                                                $skupine[] = \common\models\Skupina::find()->where(['id' => $i])->one();
                                                                foreach ($skupine as $skupina) {
                                                                    $nazivi[] = $skupina->naziv;
                                                                }
                                                            }
                                                            return 'Skupine: <ul>' . implode('<li>', $nazivi) . '</li></ul>';
                                                            break;
                                                        case 'aktivnost':
                                                            $v_array = [];
                                                            foreach ($v as $i) {
                                                                switch ($i) {
                                                                    case $i === 0:
                                                                        $v_array[] = 'Rad na literaturi';
                                                                        break;
                                                                    case 1:
                                                                        $v_array[] = 'Poslovi za ostvarivanje tjednog plana i programa (priprema poticaja za rad s djecom)';
                                                                        break;
                                                                    case 2:
                                                                        $v_array[] = 'Stručno usavršavanje (online/uživo) navesti naziv edukacije i mjesto održavanja';
                                                                        break;
                                                                    case 3:
                                                                        $v_array[] = 'Nešto drugo';
                                                                        break;
                                                                    default:
                                                                        $v_array[] = $i;
                                                                        break;
                                                                }
                                                            }
                                                            return 'Aktivnosti: <ul>' . implode('<li>', $v_array) . '</li></ul>';
                                                            break;
                                                        default:
                                                            return $k . '[]=' . implode('&' . $k . '[]=', $v) . '<br/>';
                                                    }
                                                } else {
                                                    switch ($k) {
                                                        case 'skupina':
                                                            if (!isset($v)) {
                                                                $skupina = \common\models\Skupina::find()->where(['id' => $v])->one();
                                                                return 'Skupine: <ul><li>' . $skupina->naziv . '</li></ul>';
                                                            }
                                                            break;
                                                        case 'aktivnost':
                                                            $i = '';
                                                            switch ($v) {
                                                                case $v === 0:
                                                                    $i = 'Rad na literaturi';
                                                                    break;
                                                                case 1:
                                                                    $i = 'Poslovi za ostvarivanje tjednog plana i programa (priprema poticaja za rad s djecom)';
                                                                    break;
                                                                case 2:
                                                                    $i = 'Stručno usavršavanje (online/uživo) navesti naziv edukacije i mjesto održavanja';
                                                                    break;
                                                                case 3:
                                                                    $i = 'Nešto drugo';
                                                                    break;
                                                                default:
                                                                    $i = $v;
                                                                    break;
                                                            }
                                                            return 'Aktivnosti: <ul><li>' . $i . '</li></ul>';
                                                            break;
                                                        case 'end':
                                                            // return 'Kraj' . ': ' . Yii::$app->formatter->asDate($v, 'php:j.n.Y. H:i').'<br>';
                                                            return ' ';
                                                            break;
                                                        case 'start':
                                                            // return 'Početak' . ': ' .  Yii::$app->formatter->asDate($v, 'php:j.n.Y. H:i').'<br/>';
                                                            return ' ';
                                                            break;
                                                        case 'vrsta':
                                                            return 'Vrsta' . ': ' . $v . '<br/>';
                                                            break;
                                                        case 'zapis_custom':
                                                            return '';
                                                            break;
                                                        default:
                                                            return $k . ': ' . $v . '<br/>';
                                                    }
                                                }
                                            },
                                            $vrijednosti,
                                            array_keys($vrijednosti)
                                        ));
                                        break;
                                    default:
                                        return implode('', array_map(
                                            function ($v, $k) {
                                                if (is_array($v)) {
                                                    switch ($k) {
                                                        case 'skupina':
                                                            $nazivi = [];
                                                            foreach ($v as $i) {
                                                                $skupine[] = \common\models\Skupina::find()->where(['id' => $i])->one();
                                                                foreach ($skupine as $skupina) {
                                                                    $nazivi[] = $skupina->naziv;
                                                                }
                                                            }
                                                            return 'Skupine: <ul>' . implode('<li>', $nazivi) . '</li></ul>';
                                                            break;
                                                        case 'aktivnost':
                                                            return 'Aktivnosti: <ul>' . implode('<li>', $v) . '</li></ul>';
                                                            break;
                                                        default:
                                                            return $k . '[]=' . implode('&' . $k . '[]=', $v) . '<br/>';
                                                    }
                                                } else {
                                                    switch ($k) {
                                                        case 'skupina':
                                                            if (!isset($v)) {
                                                                $skupina = \common\models\Skupina::find()->where(['id' => $v])->one();
                                                                return 'Skupine: <ul><li>' . $skupina->naziv . '</li></ul>';
                                                            }
                                                            break;
                                                        case 'aktivnost':
                                                            return 'Aktivnosti: <ul><li>' . $v . '</li></ul>';
                                                            break;
                                                        case 'end':
                                                            // return 'Kraj' . ': ' . Yii::$app->formatter->asDate($v, 'php:j.n.Y. H:i').'<br>';
                                                            return ' ';
                                                            break;
                                                        case 'start':
                                                            // return 'Početak' . ': ' .  Yii::$app->formatter->asDate($v, 'php:j.n.Y. H:i').'<br/>';
                                                            return ' ';
                                                            break;
                                                        case 'vrsta':
                                                            return 'Vrsta' . ': ' . $v . '<br/>';
                                                            break;
                                                        case 'zapis_custom':
                                                            return '';
                                                            break;
                                                        default:
                                                            return $k . ': ' . $v . '<br/>';
                                                    }
                                                }
                                            },
                                            $vrijednosti,
                                            array_keys($vrijednosti)
                                        ));
                                        break;
                                }
                            }
                        }
                        /*function ($model) {
                                return json_encode($model->vrijednosti_atributa, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
                            },*/
                    ],

                    [
                        'label' => Yii::t('app', 'Slike'),
                        'format' => 'html',
                        'contentOptions' => ['class' => 'id', 'data-featherlight-gallery' => '', 'data-featherlight-filter' => 'a'],
                        'value' => function ($data) {
                            $links = '';
                            if (!empty($data->attachments)) {
                                $attach = $data->attachments['attachments'];
                                foreach ($attach as $file) {
                                    $fileLink = 'dnevnik/' . $data->id . '/' . $file;
                                    $urlDetails = \Yii::$app->urlManager->createUrl(['dnevnik-zapis/viewdnevnikrada', 'id' => $data->id]);
                                    $links .= Html::a(Html::img($fileLink), $fileLink, ['target' => '_blank', 'class' => 'me-2']);
                                }
                            } else {
                                $links = "Nema datoteka";
                            }

                            return $links;
                        }
                    ],
                    [
                        'label' => Yii::t('app', 'Napravljeno'),
                        'value' => function ($data) {
                            return Yii::$app->formatter->asDate($data->created_at, 'php:j.n.Y. H:i');
                        },
                    ],
                    // [
                    // 	'label'=>Yii::t('app','Ažurirano'),
                    // 	'value'=>function($data){
                    // 		return Yii::$app->formatter->asDate($data->updated_at, 'php:j.n.Y. H:i');
                    // 	},
                    // ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}  ',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data) {

                                $url2 = \Yii::$app->urlManager->createUrl(['dnevnik-zapis/viewdnevnikrada', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pregledaj zapise'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            },
                        ],
                    ],
                ],
            ]);

            ?>
        </div>
    </div>
</div>