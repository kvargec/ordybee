<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Add files');

?>
<div class="media-create">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">
            <?php //$form = ActiveForm::begin([
            //      'options' => [
            //          'enctype' => 'multipart/form-data',
            //      ],
            //      'action' => [
            //          'dnevnik-zapis/upload',
            //      ],
            //      'method' => 'POST',
            //  ]);
            ?>
            <?= FileInput::widget([
                'model' => $model,
                'attribute' => 'attachments[]',
                'name' => 'attachments[]',
                'language' => 'hr',
                'options' => [
                    'accept' => 'image/*,video/*',
                    'id' => 'attachments_id',
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'allowedFileTypes' => ['image', 'video'],
                    'initialPreview' => $model->getFilePaths($model->id),
                    'initialPreviewConfig' => $model->getFilePathsConfig($model->id),
                    'initialPreviewAsData' => true,
                    'uploadUrl' => Url::to(['/dnevnik-zapis/upload-file']),
                    'uploadExtraData' => [
                        'model_id' => $model->id,
                    ],
                    'deleteUrl' => Url::to(['/dnevnik-zapis/delete-file']),
                    'deleteExtraData' => [
                        'model_id' => $model->id,
                    ],
                    'previewFileType' => 'any',
                    'showUpload' => false,
                    'showRemove' => false,
                    'overwriteInitial' => false,
                    'uploadAsync' => false,
                    'browseOnZoneClick' => true,
                    'encodeUrl' => false, //double encodes otherwise 
                    'language' => substr(\Yii::$app->language, 0, 2),
                    'browseLabel' => Yii::t('app', 'Odabir'),
                    'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteke ovdje'),
                    'dropZoneClickTitle' => Yii::t('app', ' (ili pritisnite za odabir)'),
                    'placeholder' => Yii::t('app', 'Odabir'),
                    'msgPlaceholder' => Yii::t('app', 'Odabir'),
                    'msgUploadBegin' => Yii::t('app', 'Pričekajte dok se slike učitaju'),
                    'msgUploadThreshold' => Yii::t('app', 'Pričekajte, slike se učitavaju'),
                    'msgUploadEnd' => Yii::t('app', 'Završeno'),
                    //'maxFileCount' => 15,
                    //'maxFileSize' => 10000
                ],
            ]); ?>
            <div class="form-group">
                <a class="btn btn-success" href="<?= \yii\helpers\Url::to(['/dnevnik-zapis/creatednevnikrada']) ?>"><?= Yii::t('app', 'Save') ?></a>
            </div>
            <? //php ActiveForm::end(); 
            ?>

        </div>
    </div>
</div>
<?php
$js = <<<JS
$('input[name^="DnevnikZapis"]').on('filebatchselected', function(event) {
    $(this).fileinput('upload');
});
JS;
$this->registerJs($js);
?>