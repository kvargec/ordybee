<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DnevnikZapisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dnevnici rada po skupinama');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dnevnik-zapis-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">person</span> Po zaposlenicima', ['dnevnik-rada-index-zaposlenika'], ['class' => 'btn btn-primary']) ?>
            <!-- <?= Html::a('<span class="material-icons">person</span> Po zaposlenicima', ['dnevnik-rada-index-zaposlenika'], ['class' => 'btn-simple right mt-15']) ?> -->
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'value' => 'naziv',
                        'label' => Yii::t('app', 'Naziv grupe'),
                        'attribute' => 'naziv',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Naziv'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'label' => Yii::t('app', 'Objekt'),
                        'value' => function ($data) {
                            $objekti = [];
                            foreach ($data->objekts as $item) {
                                $objekti[] = $item->naziv;
                            }
                            return implode(", ", $objekti);
                        },
                        'attribute' => 'objekt',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Naziv objekta'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'label' => Yii::t('app', 'Ped. godina'),
                        'attribute' => 'ped_godina',
                        'value' => function ($data) {
                            return $data->pedGodina->getNaziv();
                        },
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filter' => yii\helpers\ArrayHelper::map(\common\models\PedagoskaGodina::find()->all(), 'id', function ($v) {
                            return $v->getNaziv();
                        })
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data) {

                                $url2 = \Yii::$app->urlManager->createUrl(['dnevnik-zapis/dnevnik-rada-index-skup', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pregledaj zapise'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            },



                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>