<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DnevnikZapis */

$this->title = Yii::t('app', 'Create Dnevnik Zapis');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dnevnik Zapis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dnevnik-zapis-create">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>

        </div>
        <div class="card-body">
            <div class="row pb-20 pt-20">
                <?php
                $rmjesto = \common\models\RadnoMjesto::find()->where(['slug' => 'odgojitelj'])->one();
                $rola = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
                $uloge = [];
                $form = null;
                foreach ($rola as $tko => $opis) {
                    $uloge[] = $tko;
                }
                if (($uloge[0]) == 'superadmin' || $uloge[0] == 'administracija') { ?>
                    <div class="col-lg-4 col-lg-push-8">

                    <?php } else { ?>
                        <div class="col-lg-7 col-lg-push-5">
                        <?php } ?>
                        <?php
                        //$godina = \common\models\PedagoskaGodina::find()->where(['aktivna' => true])->one();

                        if (($uloge[0]) == 'pedagog') {
                            $form = ActiveForm::begin(['action' => ['dnevnik-zapis/creatednevnikradapedagog'], 'method' => 'POST']);
                        } elseif (($uloge[0]) != 'pedagog') {
                            $form = ActiveForm::begin(['action' => ['dnevnik-zapis/creatednevnikrada'], 'method' => 'POST']);
                        }
                        ?>

                        <h3 class="mt-0">Novi dnevnik rada</h3>
                        <?php
                        if (($uloge[0]) == 'superadmin' || $uloge[0] == 'administracija') {
                        ?>
                            <label class="control-label"><?= Yii::t('app', 'Dnevnik zaposlenika') ?></label>
                            <div class="zaposlenik_id_div">
                                <?=
                                Select2::widget([
                                    'name' => 'zaposlenik',
                                    'data' => ArrayHelper::map(
                                        \common\models\Zaposlenje::find()->with([
                                            'zaposlenik0' => function ($query) {
                                                $query->andWhere(['!=', 'status', 3])->orderBy('prezime,ime DESC');
                                            },
                                        ])->where(['r_mjesto' => $rmjesto->id])->all(),
                                        'zaposlenik',
                                        function ($zaposlenje) {
                                            $zaposlenici[] = \common\models\Zaposlenik::find()->where(['id' => $zaposlenje->zaposlenik])->one();
                                            foreach ($zaposlenici as $zaposlenik) {
                                                if (isset($zaposlenik)) {
                                                    return $zaposlenik->ime . ' ' . $zaposlenik->prezime;
                                                }
                                            }
                                        }
                                    ),
                                    'options' => [
                                        'placeholder' => Yii::t('app', 'Odaberite odgojitelja'),
                                        'multiple' => false,
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ]
                                ])
                                ?>
                            </div>
                        <?php
                        }
                        ?>
                            <div class="objekt_id_div">
                                <?php 
                                $data = [0 => Yii::t('app', 'Nema objekta')];
                                $data = $data + ArrayHelper::map(\common\models\Objekt::find()->all(),'id','naziv');
                                ?>
                                <?=
                                    Select2::widget([
                                        'name' => 'objekt',
                                        'data' => $data,
                                        'options' => [
                                            'placeholder' => Yii::t('app', 'Odaberite objekt'),
                                            'multiple' => false,
                                            'required' => true,
                                            'class' => 'form-control'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                        ],
                                    ]) 
                                ?>
                            </div>
                        <?= $form->field($model, 'status')->hiddenInput(['value' => null])->label(false); ?>
                        <?= Html::radioList('roles', [], ['neposredni' => 'Neposredni rad', 'posredni' => 'Posredni rad', 'vanustanove' => 'Rad van ustanove', 'godisnji' => 'Godišnji odmor', 'bolovanje' => 'Bolovanje', 'slobodno' => 'Slobodni dani', 'porodiljni' => 'Porodiljni', 'dopust' => 'Dopust'], ['style' => 'display:none', 'id' => 'zapis_type_radio']); ?>
                        <div id="buttons">

                            <div class="dropdown">
                                <a class="btn btn-success btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span><span class="material-icons">add</span> Dodaj dnevnik rada <span class="caret"></span></span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <?php
                                    $vrsta = ['neposredni' => 'Neposredni rad', 'posredni' => 'Posredni rad', 'vanustanove' => 'Rad van ustanove', 'godisnji' => 'Godišnji odmor', 'bolovanje' => 'Bolovanje', 'slobodno' => 'Slobodni dani', 'porodiljni' => 'Porodiljni', 'dopust' => 'Dopust'];
                                    foreach ($vrsta as $vrsta => $vrstaRada) {
                                    ?>
                                        <li>
                                            <a onclick="dnevnikRada('<?= $vrsta; ?>')"><?= $vrstaRada; ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div id="neposredni" class="row smallform hidden">
                            <h3 class="">Neposredni rad</h3>
                            <?php
                            if (($uloge[0]) == 'pedagog') {
                                echo $this->render('_form-ped-nep', [
                                    'model' => $model,
                                    'form' => $form
                                ]);
                            } else {
                                echo $this->render('_form2', [
                                    'model' => $model,
                                    'form' => $form
                                ]);
                            } ?>
                        </div>
                        <div id="posredni" class="row smallform hidden">
                            <h3 class="">Posredni rad</h3>
                            <?php
                            if (($uloge[0]) == 'pedagog') {
                                echo $this->render('_form-ped-pos', [
                                    'model' => $model,
                                    'form' => $form
                                ]);
                            } else {
                                echo $this->render('_form3', [
                                    'model' => $model,
                                    'form' => $form
                                ]);
                            } ?>
                        </div>
                        <div id="vanustanove" class="row smallform hidden">
                            <h3 class="">Rad van ustanove</h3>
                            <?= $this->render('_form4', [
                                'model' => $model,
                                'form' => $form
                            ]) ?>
                        </div>
                        <!--
                <style>
                    .file-caption.icon-visible .file-caption-name {
                        padding-left: 2.5rem;
                    }

                    .form-group .form-control {
                        margin-bottom: 0
                    }

                    .file-drop-zone-title {
                        white-space: break-spaces !important
                    }

                    .dnevnik-zapis-create h3 {
                        padding: 0 5px;
                    }
                </style>
                <div id="upload" class="row smallform hidden" style="padding: 0px 7px 8px 15px; width:100%">
                    <div class="dnevnik-zapis-form form-group">
                        <? //= $form->field($model, 'attachments[]', [])->widget(FileInput::classname(), [
                        //  'language' => 'hr',
                        //  'options' => [
                        //      'accept' => '*/*',
                        //      'id' => 'input',
                        //      'multiple' => true,
                        //  ],
                        //  'pluginOptions' => [
                        //      //'initialPreview' => $model->isNewRecord ? [] : [$model->getFilePaths()],
                        //      'initialPreviewAsData' => !$model->isNewRecord,
                        //      'showUpload' => false,
                        //      'language' => substr(\Yii::$app->language, 0, 2),
                        //      'browseLabel' => Yii::t('app', 'Odabir'),
                        //      'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteke ovdje'),
                        //      'placeholder' => Yii::t('app', 'Odabir'),
                        //      'msgPlaceholder' => Yii::t('app', 'Odabir'),
                        //      'overwriteInitial' => false,
                        //      'maxFileCount' => 10
                        //  ]
                        //])->label(Yii::t('app', "Prilozi")) 
                        ?>
                    </div>
                </div>-->
                        <div id="bolovanje" class="row smallform hidden">
                            <h3 class="">Bolovanje</h3>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Početak bolovanja</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'bolovanje_start',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Od')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Kraj bolovanja</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'bolovanje_end',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Do')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right submit-button']) ?>
                            </div>
                        </div>
                        <div id="slobodno" class="row smallform hidden">
                            <h3 class="">Slobodni dani</h3>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Početak slobodnih dana</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'slobodno_start',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Od')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Kraj slobodnih dana</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'slobodno_end',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Do')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right submit-button']) ?>
                            </div>
                        </div>

                        <div id="godisnji" class="row smallform hidden">
                            <h3 class="">Godišnji odmor</h3>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Početak godišnjeg odmora</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'godisnji_start',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Od')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Kraj godišnjeg odmora</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'godisnji_end',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Do')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right submit-button']) ?>
                            </div>
                        </div>

                        <div id="porodiljni" class="row smallform hidden">
                            <h3 class="">Porodiljni</h3>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Početak porodiljnog</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'porodiljni_start',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Od')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Kraj porodiljnog</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'porodiljni_end',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Do')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right submit-button']) ?>
                            </div>
                        </div>

                        <div id="dopust" class="row smallform hidden">
                            <h3 class="">Dopust</h3>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Početak dopusta</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'dopust_start',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Od')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="col-lg-6"> <?php echo '<label class="control-label">Kraj dopusta</label>';
                                                    echo DatePicker::widget([
                                                        'name' => 'dopust_end',
                                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                                        'readonly' => true,
                                                        //'value' => '23-Feb-2021',
                                                        'options' => ['placeholder' => Yii::t('app', 'Do')],
                                                        'pluginOptions' => [
                                                            'autoclose' => true,
                                                            'format' => 'dd.mm.yyyy'
                                                        ]
                                                    ]); ?>
                            </div>
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right submit-button']) ?>
                            </div>
                        </div>


                        <?php ActiveForm::end(); ?>
                        </div>
                        <?php if (($uloge[0]) == 'superadmin' || $uloge[0] == 'administracija') { ?>
                            <div class="col-lg-8 col-lg-pull-4">

                            <?php } else { ?>
                                <div class="col-lg-5 col-lg-pull-7">
                                <?php } ?>
                                <style>
                                    .fc .fc-timegrid-col.fc-day-today {
                                        background-color: rgba(202, 219, 230, .1);
                                    }

                                    .fc .fc-cell-shaded,
                                    .fc .fc-day-disabled {
                                        background-color: #fff
                                    }

                                    .fc .fc-daygrid-day.fc-day-today {
                                        background-color: rgba(157, 186, 204, 0.3);
                                    }

                                    .fc-theme-standard td,
                                    .fc-theme-standard th,
                                    .fc-scrollgrid,
                                    .fc-theme-standard .fc-scrollgrid {
                                        border: 1px solid #d6e3f3;
                                    }
                                </style>

                                <?php
                                //print("<pre>".print_r($events,true)."</pre>");die();

                                echo \backend\widgets\calendar2\CalendarWidget::widget(['model' => $events, 'options' => ['view' => 'timeGridDay', 'toolbar' => 'day']]);
                                ?>


                                </div>

                            </div>
                    </div>
            </div>
        </div>

    </div>
    <?php
    $js = <<<JS
    $('.input-group.date > input').css('cursor', 'pointer');
    $('.input-group.date > div > input').css('cursor', 'pointer');
    $(document).on('click', '.submit-button', function(e){
        if($('.zaposlenik_id_div .form-group').hasClass('is-empty')){
            e.preventDefault();
            $.notify("Molimo odaberite dnevnik zaposlenika");
        }   
    })
    $(document).on('click', '.submit-button', function(e){
        if($('.objekt_id_div .form-group').hasClass('is-empty')){
            e.preventDefault();
            $.notify("Molimo odaberite objekt");
        }   
    })
    $(document).submit(function (e) {
        var delivered = true;
        if ($("input[value='neposredni']:checked").length > 0) {
            //if (!$('input[name="neposredni[zapis_custom]"]').val()) {
            //    e.preventDefault();
            //    $('input[name="neposredni[zapis_custom]"]').addClass('warning');
            //    alert("Molimo unesite naziv aktivnosti");
            //} else {
            //    $('select[name="neposredni[zapis_custom]"]').removeClass('warning');
            //}
            if(!$('select[name="neposredni[aktivnost][]"]').val()){
                e.preventDefault();
                $('select[name="neposredni[aktivnost][]"]').addClass('warning');
                $.notify("Molimo unesite vrstu aktivnosti");
                delivered = false;
            } else {
                $('select[name="neposredni[aktivnost][]"]').removeClass('warning');
            }
            if (!$('input[name="neposredni[start]"]').val()){
                e.preventDefault();
                $('input[name="neposredni[start]"]').addClass('warning');
                $.notify("Molimo unesite vrijeme početka aktivnosti");
                delivered = false;
            } else {
                $('input[name="neposredni[start]"]').removeClass('warning');
            }
            if (!$('input[name="neposredni[end]"]').val()){
                e.preventDefault();
                $('input[name="neposredni[end]"]').addClass('warning');
                $.notify("Molimo unesite vrijeme kraja aktivnosti");
                delivered = false;
            } else {
                $('input[name="neposredni[end]"]').removeClass('warning');
            }
        }
        if ($("input[value='posredni']:checked").length > 0) {
            //if (!$('input[name="posredni[zapis_custom]"]').val()) {
            //    e.preventDefault();
            //    $('input[name="posredni[zapis_custom]"]').addClass('warning');
            //    alert("Molimo unesite naziv aktivnosti");
            //} else {
            //    $('input[name="posredni[zapis_custom]"]').removeClass('warning');
            //}
            if(!$('select[name="posredni[aktivnost][]"]').val()){
                e.preventDefault();
                $('select[name="posredni[aktivnost][]"]').addClass('warning');
                $.notify("Molimo unesite vrstu aktivnosti");
                delivered = false;
            } else {
                $('select[name="posredni[aktivnost][]"]').removeClass('warning');
            }
            if (!$('input[name="posredni[start]"]').val()){
                e.preventDefault();
                $('input[name="posredni[start]"]').addClass('warning');
                $.notify("Molimo unesite vrijeme početka aktivnosti");
                delivered = false;
            } else {
                $('input[name="posredni[start]"]').removeClass('warning');
            }
            if (!$('input[name="posredni[end]"]').val()){
                e.preventDefault();
                $('input[name="posredni[end]"]').addClass('warning');
                $.notify("Molimo unesite vrijeme kraja aktivnosti");
                delivered = false;
            } else {
                $('input[name="posredni[end]"]').removeClass('warning');
            }
        }
        if ($("input[value='vanustanove']:checked").length > 0) {
            //if (!$('input[name="vanustanove[zapis_custom]"]').val()){
            //    e.preventDefault();
            //    $('input[name="vanustanove[zapis_custom]"]').addClass('warning');
            //    alert("Molimo unesite naziv aktivnosti");
            //} else {
            //    $('input[name="vanustanove[zapis_custom]"]').removeClass('warning');
            //}
            if(!$('select[name="vanustanove[aktivnost][]"]').val()){
                e.preventDefault();
                $('select[name="vanustanove[aktivnost][]"]').addClass('warning');
                $.notify("Molimo unesite vrstu aktivnosti");
                delivered = false;
            } else {
                $('select[name="vanustanove[aktivnost][]"]').removeClass('warning');
            }
            if (!$('input[name="vanustanove[start]"]').val()){
                e.preventDefault();
                $('input[name="vanustanove[start]"]').addClass('warning');
                $.notify("Molimo unesite vrijeme početka aktivnosti");
                delivered = false;
            } else {
                $('input[name="vanustanove[start]"]').removeClass('warning');
            }
            if (!$('input[name="vanustanove[end]"]').val()){
                e.preventDefault();
                $('input[name="vanustanove[end]"]').addClass('warning');
                $.notify("Molimo unesite vrijeme kraja aktivnosti");
                delivered = false;
            } else {
                $('input[name="vanustanove[end]"]').removeClass('warning');
            }
        }
        if ($("input[value='godisnji']:checked").length > 0) {
            if (!$('input[name="godisnji_start"]').val()){
                e.preventDefault();
                $('input[name="godisnji_start"]').addClass('warning');
                $.notify("Molimo unesite vrijeme početka godišnjeg odmora");
                delivered = false;
            } else {
                $('input[name="godisnji_start"]').removeClass('warning');
            }
            if (!$('input[name="godisnji_end"]').val()){
                e.preventDefault();
                $('input[name="godisnji_end"]').addClass('warning');
                $.notify("Molimo unesite vrijeme završetka godišnjeg odmora");
                delivered = false;
            } else {
                $('input[name="godisnji_end"]').removeClass('warning');
            }
        } 
        if ($("input[value='slobodno']:checked").length > 0) {
            if (!$('input[name="slobodno_start"]').val()){
                e.preventDefault();
                $('input[name="slobodno_start"]').addClass('warning');
                $.notify("Molimo unesite vrijeme početka slobodnog vremena");
                delivered = false;
            } else {
                $('input[name="slobodno_start"]').removeClass('warning');
            }
            if (!$('input[name="slobodno_end"]').val()){
                e.preventDefault();
                $('input[name="slobodno_end"]').addClass('warning');
                $.notify("Molimo unesite vrijeme završetka slobodnog vremena");
                delivered = false;
            } else {
                $('input[name="slobodno_end"]').aremovelass('warning');
            }
        } 
        if ($("input[value='bolovanje']:checked").length > 0) {
            if (!$('input[name="bolovanje_start"]').val()){
                e.preventDefault();
                $('input[name="bolovanje_start"]').addClass('warning');
                $.notify("Molimo unesite vrijeme početka bolovanja");
                delivered = false;
            } else {
                $('input[name="bolovanje_start"]').removeClass('warning');
            }
            if (!$('input[name="bolovanje_end"]').val()){
                e.preventDefault();
                $('input[name="bolovanje_end"]').addClass('warning');
                $.notify("Molimo unesite vrijeme završetka bolovanja");
                delivered = false;
            } else {
                $('input[name="bolovanje_end"]').removeClass('warning');
            }
        }
        if ($("input[value='porodiljni']:checked").length > 0) {
            if (!$('input[name="porodiljni_start"]').val()){
                e.preventDefault();
                $('input[name="porodiljni_start"]').addClass('warning');
                $.notify("Molimo unesite vrijeme početka porodiljnog");
                delivered = false;
            } else {
                $('input[name="porodiljni_start"]').removeClass('warning');
            }
            if (!$('input[name="porodiljni_end"]').val()){
                e.preventDefault();
                $('input[name="porodiljni_end"]').addClass('warning');
                $.notify("Molimo unesite vrijeme završetka porodiljnog");
                delivered = false;
            } else {
                $('input[name="porodiljni_end"]').removeClass('warning');
            }
        }
        if ($("input[value='dopust']:checked").length > 0) {
            if (!$('input[name="dopust_start"]').val()){
                e.preventDefault();
                $('input[name="dopust_start"]').addClass('warning');
                $.notify("Molimo unesite vrijeme početka dopusta");
                delivered = false;
            } else {
                $('input[name="dopust_start"]').removeClass('warning');
            }
            if (!$('input[name="dopust_end"]').val()){
                e.preventDefault();
                $('input[name="dopust_end"]').addClass('warning');
                $.notify("Molimo unesite vrijeme završetka dopusta");
                delivered = false;
            } else {
                $('input[name="dopust_end"]').removeClass('warning');
            }
        }  
        if (delivered) {
            $.notify("Aktivnost spremljena");
        } 
    });

JS;
    $this->registerJs($js);
    ?>