<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\FileInput;
use common\models\PedagoskaGodina;
/* @var $this yii\web\View */
/* @var $model common\models\DnevnikZapis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dnevnik-zapis-form  col-lg-12">
    <?php $title = $model->zapis ? $model->zapis : null;
    $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();

    $v_array4 = [];
    if (!$model->isNewRecord) {
        foreach ($model->vrijednosti_atributa as $key => $vrijednosti) {
            if ($key == 'vanustanove') {
                foreach ($vrijednosti as $k => $v) {
                    if ($k == 'aktivnost') {
                        if (is_array($v)) {
                            foreach ($v as $i) {
                                switch ($i) {
                                    case 'Rad na literaturi':
                                        $v_array4[] = '0';
                                        break;
                                    case 'Poslovi za ostvarivanje tjednog plana i programa (priprema poticaja za rad s djecom)':
                                        $v_array4[] = '1';
                                        break;
                                    case 'Stručno usavršavanje (online/uživo) navesti naziv edukacije i mjesto održavanja':
                                        $v_array4[] = '2';
                                        break;
                                    case 'Nešto drugo':
                                        $v_array4[] = '3';
                                        break;
                                    default:
                                        $v_array4[] = $i;
                                        break;
                                }
                            }
                        } else {
                            switch ($v) {
                                case 'Rad na literaturi':
                                    $v_array4[] = '0';
                                    break;
                                case 'Poslovi za ostvarivanje tjednog plana i programa (priprema poticaja za rad s djecom)':
                                    $v_array4[] = '1';
                                    break;
                                case 'Stručno usavršavanje (online/uživo) navesti naziv edukacije i mjesto održavanja':
                                    $v_array4[] = '2';
                                    break;
                                case 'Nešto drugo':
                                    $v_array4[] = '3';
                                    break;
                                default:
                                    $v_array4[] = $v;
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    ?>

    <div class="form-group">
        <!-- <label class="control-label"><?= Yii::t('app', 'Naziv aktivnosti') ?></label> -->
        <?= Html::textInput('vanustanove[zapis_custom]', $title, ['class' => 'form-control hidden', 'style' => 'width:350px', 'placeholder' => Yii::t('app', 'Upišite naziv aktivnosti...')]) ?>
        <div class="form-group"><label class="control-label"><?= Yii::t('app', 'Skupina') ?></label>
            <?= Select2::widget([
                'name' => 'vanustanove[skupina]',
                'data' => ArrayHelper::map(
                    \common\models\Skupina::find()->where(['ped_godina' => $godina->id])->all(),
                    'id',
                    function ($v) {
                        return $v->naziv;
                    }
                ),
                'value' => (!$model->isNewRecord && !empty($model->vrijednosti_atributa['vanustanove']) && isset($model->vrijednosti_atributa['vanustanove']['skupina'])) ? $model->vrijednosti_atributa['vanustanove']['skupina'] : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite skupinu...'),
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
        <div class="form-group"><label class="control-label"><?= Yii::t('app', 'Aktivnost') ?></label>
            <?= Select2::widget([
                'name' => 'vanustanove[aktivnost]',
                'data' => [
                    'Rad na literaturi' => 'Rad na literaturi',
                    'Poslovi za ostvarivanje tjednog plana i programa (priprema poticaja za rad s djecom)' => 'Poslovi za ostvarivanje tjednog plana i programa (priprema poticaja za rad s djecom)',
                    'Stručno usavršavanje (online/uživo) navesti naziv edukacije i mjesto održavanja' => 'Stručno usavršavanje (online/uživo) navesti naziv edukacije i mjesto održavanja',
                    'Nešto drugo' => 'Nešto drugo'
                ],
                'value' => (!$model->isNewRecord && array_key_exists('vanustanove', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['vanustanove']['aktivnost'])) ? $v_array4 : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite vrstu aktivnost...'),
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
        <div class="row mt-15">
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'vanustanove[start]',
                    'value' => (!$model->isNewRecord && array_key_exists('vanustanove', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['vanustanove']['start'] : date('d.m.Y. 08:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Od')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?> </div>
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'vanustanove[end]',
                    'value' => (!$model->isNewRecord && array_key_exists('vanustanove', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['vanustanove']['end'] : date('d.m.Y. 13:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Do')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right submit-button', 'style' => 'z-index:100;']) ?>
                </div>
            </div>
        </div>
    </div>
</div>