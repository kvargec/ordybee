<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DnevnikZapisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dnevnik Zapis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dnevnik-zapis-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'dnevnik',
                    'zapis:ntext',
                    'vrsta',
                    'status',
                    //'created_at',
                    //'updated_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}  ',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data)  {

                                $url2 = \Yii::$app->urlManager->createUrl(['dnevnik-zapis/view', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pregledaj zapise'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            },



                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>