<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use kartik\widgets\DatePicker;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\FileInput;
use common\models\PedagoskaGodina;
/* @var $this yii\web\View */
/* @var $model common\models\DnevnikZapis */
/* @var $form yii\widgets\ActiveForm */
?>
<?php

$v_array2 = [];
if (!$model->isNewRecord) {
    foreach ($model->vrijednosti_atributa as $key => $vrijednosti) {
        if ($key == 'neposredni') {
            foreach ($vrijednosti as $k => $v) {
                if ($k == 'aktivnost') {
                    if (is_array($v)) {
                        foreach ($v as $i) {
                            switch ($i) {
                                case 'Vođenje knjige pedagoške dokumentacije odgojno-obrazovne skupine':
                                    $v_array2[] = '0';
                                    break;
                                case 'Poslovi za ostvarivanje tjednog plana i programa (izrada sredstava za rad s djecom, uređenje prostora za realizaciju odgojno-obrazovnog rada)':
                                    $v_array2[] = '1';
                                    break;
                                case 'Dokumentiranje aktivnosti djece (razvojne mape, uradci djece, samorefleksije djece, narativni oblici, opservacije i postignuća djece)':
                                    $v_array2[] = '2';
                                    break;
                                case 'Dokumentiranje aktivnosti odgojitelja (samorefeksija i zajedničke refleksije odgojitelja ipedagoga)':
                                    $v_array2[] = '3';
                                    break;
                                case 'Suradnja s roditeljima (individualni razgovori/savjetodavni rad) (Ime i prezime roditelja, tema razgovora, na čiju inicijativu)':
                                    $v_array2[] = '4';
                                    break;
                                case 'Suradnja s pedagogom (uvid u praćenje i dokumentaciju o aktivnostima te razgovori o tekućoj problematici i situacijama)':
                                    $v_array2[] = '5';
                                    break;
                                case 'Stručni aktiv (navesti temu i tko je voditelj)':
                                    $v_array2[] = '6';
                                    break;
                                case 'Odgojiteljsko vijeće (navesti dnevni red)':
                                    $v_array2[] = '7';
                                    break;
                                case 'Roditeljski sastanci/edukativne i kreativne radionice/druženja s roditeljima (navesti temu te prisutne roditelje)':
                                    $v_array2[] = '8';
                                    break;
                                case 'Nešto drugo':
                                    $v_array2[] = '9';
                                    break;
                                default:
                                    $v_array2[] = $i;
                                    break;
                            }
                        }
                    } else {
                        switch ($v) {
                            case 'Vođenje knjige pedagoške dokumentacije odgojno-obrazovne skupine':
                                $v_array2[] = '0';
                                break;
                            case 'Poslovi za ostvarivanje tjednog plana i programa (izrada sredstava za rad s djecom, uređenje prostora za realizaciju odgojno-obrazovnog rada)':
                                $v_array2[] = '1';
                                break;
                            case 'Dokumentiranje aktivnosti djece (razvojne mape, uradci djece, samorefleksije djece, narativni oblici, opservacije i postignuća djece)':
                                $v_array2[] = '2';
                                break;
                            case 'Dokumentiranje aktivnosti odgojitelja (samorefeksija i zajedničke refleksije odgojitelja ipedagoga)':
                                $v_array2[] = '3';
                                break;
                            case 'Suradnja s roditeljima (individualni razgovori/savjetodavni rad) (Ime i prezime roditelja, tema razgovora, na čiju inicijativu)':
                                $v_array2[] = '4';
                                break;
                            case 'Suradnja s pedagogom (uvid u praćenje i dokumentaciju o aktivnostima te razgovori o tekućoj problematici i situacijama)':
                                $v_array2[] = '5';
                                break;
                            case 'Stručni aktiv (navesti temu i tko je voditelj)':
                                $v_array2[] = '6';
                                break;
                            case 'Odgojiteljsko vijeće (navesti dnevni red)':
                                $v_array2[] = '7';
                                break;
                            case 'Roditeljski sastanci/edukativne i kreativne radionice/druženja s roditeljima (navesti temu te prisutne roditelje)':
                                $v_array2[] = '8';
                                break;
                            case 'Nešto drugo':
                                $v_array2[] = '9';
                                break;
                            default:
                                $v_array2[] = $v;
                                break;
                        }
                    }
                }
            }
        }
    }
}


?>

<div class="dnevnik-zapis-form col-lg-12">
    <?php $title = $model->zapis ? $model->zapis : null;
    $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one(); ?>

    <div class="form-group">
        <!-- <label class="control-label"><?= Yii::t('app', 'Naziv aktivnosti') ?></label> -->

        <?= Html::textInput('neposredni[zapis_custom]', $title, ['class' => 'form-control hidden', 'style' => 'width:350px', 'placeholder' => Yii::t('app', 'Upišite naziv aktivnosti...')]) ?>
        <div class="form-group field-mentorstvo">

            <input type="hidden" name="neposredni[mentor]" value="0"><label><input type="checkbox" id="mentorstvo" name="neposredni[mentor]" value="1"> Mentor</label>

            <div class="help-block"></div>
        </div>

        <div class="form-group"><label class="control-label"><?= Yii::t('app', 'Skupina') ?></label>
            <?= Select2::widget([
                'name' => 'neposredni[skupina]',
                'data' => ArrayHelper::map(
                    \common\models\Skupina::find()->where(['ped_godina' => $godina->id])->all(),
                    'id',
                    function ($v) {
                        return $v->naziv;
                    }
                ),
                'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['neposredni']['skupina'])) ? $model->vrijednosti_atributa['neposredni']['skupina'] : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite skupinu...'),
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
        <div class="form-group"><label class="control-label"><?= Yii::t('app', 'Aktivnost') ?></label>
            <?= Select2::widget([
                'name' => 'neposredni[aktivnost]',
                'data' => [
                    'Životno praktične i radne' => 'Životno praktične i radne',
                    'Raznovrsne igre' => 'Raznovrsne igre',
                    'Društvene i društvene zabavne' => 'Društvene i društvene zabavne',
                    'Umjetničke' => 'Umjetničke',
                    'Raznovrsno izražavanje i stvaranje' => 'Raznovrsno izražavanje i stvaranje',
                    'Istraživačko spoznajne' => 'Istraživačko spoznajne',
                    'Specifične aktivnosti s kretanjem' => 'Specifične aktivnosti s kretanjem'
                ],
                'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['neposredni']['aktivnost'])) ? $v_array2 : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite vrstu aktivnost...'),
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
        <div class="row mt-15">
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'neposredni[start]',
                    'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['neposredni']['start'] : date('d.m.Y. 08:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Od')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?> </div>
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'neposredni[end]',
                    'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['neposredni']['end'] : date('d.m.Y. 13:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Do')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right submit-button', 'style' => 'z-index:100;']) ?>
                </div>
            </div>
        </div>
    </div>
</div>