<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Icd10classification;
/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DnevnikZapisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dnevnik Zapis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dnevnik-zapis-index">
    <style>
        body,
        table,
        td {
            font-family: sans-serif !important;
        }

        .card-body>.table tr td:first-of-type,
        .card-body>.table tr th:first-of-type {
            padding-left: inherit;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
            border: 1px 0 solid gray
        }
    </style>
    <div class="card">

        <div class="card-body">

            <table class="table table-bordered table-striped">
                <tr style="background-color:#cae6da">
                    <td colspan="2">R.B.</td>

                    <td rowspan="2">Ime i prezime</td>
                    <td colspan="5" style="text-align:center;font-weight:bold">DIJAGNOZA ILI ŠIFRA BOLESTI</td>
                    <td rowspan="2">Ostalo</td>
                    <td rowspan="2">Cijepljenost</td>

                    <td rowspan="2" style="text-align:center;">Broj dana izostanaka</td>
                    <td rowspan="2">Skupina</td>
                </tr>
                <tr style="background-color:#cae6da">
                    <td colspan="2">mjesec</td>
                    <td>Zarazne bolesti <br>A 00 - B 99</td>
                    <td>Bolesti živčanog sustava<br>G 00 - G 99</td>
                    <td>Bolesti dišnog sustava<br>J 00 - J 99</td>
                    <td>Bolesti oka i uha<br>H 00 - H 95</td>
                    <td>Bolesti probavnog sustava<br>K 00 - K 93</td>
                </tr>
                <?php
                $i = 1;
                $ukupnoIzostanaka = 0;
                foreach ($data as $dijeteId => $groupedRecords) {
                    // Display information for each dijete_id
                    echo '<tr><td style="background-color:#cae6da">' . $i . '</td>';
                    if (!empty($groupedRecords)) {
                        $dijeteZapis = reset($groupedRecords); // Get the first record
                        echo '<td style="background-color:#cae6da">' . $mjesec . '</td>';
                    }
                    if (!empty($groupedRecords)) {
                        $dijeteZapis = reset($groupedRecords); // Get the first record
                        echo '<td>' . $dijeteZapis->dijete0->ime;
                        $skupina = '';
                        if ($dijeteZapis->dijete0->skupina0) {
                            $skupina = $dijeteZapis->dijete0->skupina0->naziv;
                        }
                        echo ' ' . $dijeteZapis->dijete0->prezime . '</td>';
                    }
                    $difference = 0;
                    // Iterate over the grouped records for the current dijete_id

                    foreach ($groupedRecords as $dijeteZapis) {

                        // echo 'Dijete Zapis ID: ' . $dijeteZapis->id . '<br>';
                        $dodatno = json_decode($dijeteZapis->dodatno, true);
                        $pocetakIzostanka = new DateTime($dodatno['pocetak_izostanka']);
                        $krajIzostanka = new DateTime($dodatno['kraj_izostanka']);
                        $pocetakIzostanka->setDate($pocetakIzostanka->format('Y'), $pocetakIzostanka->format('m'), $pocetakIzostanka->format('d'));
                        $krajIzostanka->setDate($pocetakIzostanka->format('Y'), $krajIzostanka->format('m'), $krajIzostanka->format('d'));
                        if ($pocetakIzostanka->format('m') < $mjesec) {
                            $pocetakIzostanka->modify('first day of $mjesecIme ' . $pocetakIzostanka->format('Y'));
                            var_dump($pocetakIzostanka->format('m'));
                        }

                        // Set the end date to the last day of February if it's after February
                        if ($krajIzostanka->format('m') > $mjesec) {
                            $krajIzostanka->modify('last day of $mjesecIme ' . $krajIzostanka->format('Y'));
                        }
                        // Calculate the difference between the two dates
                        $weekdays = 0;
                        $currentDate = clone $pocetakIzostanka;

                        while ($currentDate <= $krajIzostanka) {
                            // Check if the current day is not a Saturday (6) or Sunday (0)
                            if ($currentDate->format('N') < 6) {
                                $weekdays++;
                            }

                            // Move to the next day
                            $currentDate->modify('+1 day');
                        }
                        $difference += $weekdays;
                        $ukupnoIzostanaka += $weekdays;
                        // echo '<td colspan="4">' .  $dodatno['pocetak_izostanka'] . ' - ' . $dodatno['kraj_izostanka'] . '</td>';
                        // Check if the expected keys are present in the decoded array
                        $categories = [
                            'A00-B99' => 'Zarazne bolesti',
                            'G00-G99' => 'Bolesti živčanog sustava',
                            'J00-J99' => 'Bolesti dišnog sustava',
                            'H00-H95' => 'Bolesti oka i uha',
                            'K00-K93' => 'Bolesti probavnog sustava',
                            'Ostalo' => 'Ostalo'
                        ];
                        $categoryArrays = array_fill_keys(array_keys($categories), []);
                        if ($dodatno && isset($dodatno['bolest'])) {
                            if (is_array($dodatno['bolest'])) {
                                $flatBolest = implode('', $dodatno['bolest']); // Convert array to string
                            } else {
                                $flatBolest = $dodatno['bolest']; // Use the string directly
                            }
                            $bolestId = intval($flatBolest);

                            // Check if $bolestId is a valid integer
                            if ($bolestId > 0) {
                                // Find the corresponding Icd10Classification record
                                $icd10Record = Icd10classification::find()->where(['id' => $bolestId])->one();
                                $selectedCategory = null;
                                foreach ($categories as $ranges => $category) {
                                    $range = explode('-', $ranges);
                                    if (count($range) === 2) {
                                        list($start, $end) =  $range;

                                        if (strcmp($icd10Record->code, $start) >= 0 && strcmp($icd10Record->code, $end) <= 0) {
                                            $selectedCategory  = $range;
                                            // var_dump($range);
                                        }
                                    }
                                }
                                if ($selectedCategory !== null) {
                                    // Use a temporary variable for the array key
                                    $tempKey = implode('-', $selectedCategory);

                                    // Check if the key already exists in $categoryArrays
                                    if (!isset($categoryArrays[$tempKey])) {
                                        $categoryArrays[$tempKey] = [];
                                    }

                                    // Append the bolest code to the array
                                    $categoryArrays[$tempKey][] = $icd10Record->code;
                                } else {
                                    // If the bolest code doesn't fall into any category, use a default category
                                    $categoryArrays['Ostalo'][] = $icd10Record->code;
                                }
                            } else {
                                // echo 'Invalid Bolest';
                            }
                        }
                    }

                    foreach ($categories as $range => $category) : ?>


                        <td style="text-align:center">
                            <?php
                            // Output bolest codes for the current category
                            $bolestCodes = $categoryArrays[$range];
                            // var_dump($bolestCodes);
                            $bolestCodes = $categoryArrays[$range] ?? [];

                            if (!empty($bolestCodes)) {
                                echo implode(', ', $bolestCodes);
                            } else {
                                echo '';
                            }
                            ?>
                        </td>

                <?php endforeach;

                    echo '<td></td>';
                    echo '<td style="text-align:center">' . $difference . '</td>';

                    echo '<td style="text-align:center">' . $skupina . '</td>';
                    echo '</tr>'; // Add a line break between each dijete_id
                    $i++;;
                }
                echo '<tr style="background-color:#cae6da;font-weight:bold">';
                echo '<td colspan="10" style="background-color:#cae6da;font-weight:bold;font-size:24px">Ukupno</td>';
                echo '<td style="text-align:center;font-weight:bold;font-size:24px">' . $ukupnoIzostanaka . '</td>';
                echo '<td></td>';
                echo '</tr>'
                ?>
            </table>
            <?php

            // echo GridView::widget([
            //     'dataProvider' => $dataProvider,

            //     'columns' => [


            //         'id',
            //         'dnevnik',
            //         'zapis:ntext',
            //         'vrsta',
            //         'status',
            //         //'created_at',
            //         //'updated_at',

            //         [
            //             'class' => 'yii\grid\ActionColumn',
            //             'template' => '{view}  ',
            //             // 'visible' => Yii::$app->user->can('admin'),
            //             'buttons' => [
            //                 'view' => function ($url, $data) {

            //                     $url2 = \Yii::$app->urlManager->createUrl(['dnevnik-zapis/view', 'id' => $data->id]);
            //                     return Html::a('<span class="material-icons">visibility</span>', $url2, [
            //                         'title' => Yii::t('app', 'Pregledaj zapise'),
            //                         'class' => 'text-left  hgreen',
            //                     ]);
            //                 },



            //             ],
            //         ],
            //     ],
            // ]); 
            ?>


        </div>
    </div>
</div>