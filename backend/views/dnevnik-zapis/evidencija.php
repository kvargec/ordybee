<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Zaposlenik;

$aktivnaGodina = \common\models\PedagoskaGodina::find()->where(['aktivna' => 1])->one();
$zaposlenik = Zaposlenik::find()->all();
foreach ($zaposlenik as &$zap) {
    $zap->ime = $zap->ime . ' ' . $zap->prezime;
}
$zaposlenici = ArrayHelper::map($zaposlenik, 'id', 'ime');
$mjeseci = \common\helpers\Utils::mjeseciPedGod();
$this->title = Yii::t('app', 'Evidencije');

?>
<div class="card">
    <div class="card-header card-header-primary izvjestaji">
        <span class="card-title"><?= Yii::t('app',  'Evidencije') ?></span>
        <?= Html::a('<span class="material-icons">add</span><span class="material-icons">commute</span>', ['prijevoz-zaposlenik/zaposlenici'], ['class' => 'btn btn-success']) ?>

        <?= Html::a('<span class="material-icons">add</span><span class="material-icons">fastfood</span>', ['zaposlenik-obrok/zaposlenici'], ['class' => 'btn btn-success']) ?>
    </div>


    <div class="card-body ">
        <div class="row evidencija">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="blue">
                        <i class="material-icons">summarize</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Evidencija zaposlenika') ?> </h4>
                        <h4 class="title">&nbsp;</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('prisutnost/zaposlenici-pdf-mjesecni')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">


                            <?= Select2::widget([
                                'name' => 'mjesec',
                                'data' => $mjeseci,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                                    'multiple' => false,
                                    'class' => 'form-control',
                                    'required' => true
                                ],

                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>

                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> Evidencija dolazaka</button>


                        </div>
                        <!-- <?= Html::a('<span class="material-icons">file_download</span> PDF', ['/prisutnost/zaposlenici-pdf-mjesecni'], ['class' => 'btn btn-success']) ?> -->
                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">summarize</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Efektiva zaposlenika') ?> </h4>
                        <h4 class="title">&nbsp;</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('prisutnost/zaposlenici-efektiva-pdf-mjesecni')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">

                            <?= Select2::widget([
                                'name' => 'zaposlenici',
                                'data' => $zaposlenici,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite zaposlenika'),
                                    'multiple' => false,
                                    'class' => 'form-control',
                                    'required' => true

                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>
                        </div>
                        <div class="d-flex justify-content-end izvjestaji">

                            <?= Select2::widget([
                                'name' => 'mjesec',
                                'data' => $mjeseci,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                                    'multiple' => false,
                                    'class' => 'form-control',
                                    'required' => true
                                ],

                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>

                        </div>
                        <div class="d-flex justify-content-end">

                            <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> Efektiva</button>
                        </div>
                        <!-- <?= Html::a('<span class="material-icons">file_download</span> PDF', ['/prisutnost/zaposlenici-pdf-mjesecni'], ['class' => 'btn btn-success']) ?> -->
                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="blue">
                        <i class="material-icons">summarize</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Dodaci na plaću') ?> </h4>
                        <h4 class="title">&nbsp;</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('dnevnik-zapis/zaposlenici-dodaci')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">

                            <?= Select2::widget([
                                'name' => 'zaposlenici',
                                'data' => $zaposlenici,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite zaposlenika'),
                                    'multiple' => false,
                                    'class' => 'form-control',
                                    'required' => true

                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>
                        </div>
                        <div class="d-flex justify-content-end izvjestaji">

                            <?= Select2::widget([
                                'name' => 'mjesec',
                                'data' => $mjeseci,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                                    'multiple' => false,
                                    'class' => 'form-control',
                                    'required' => true
                                ],

                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>

                        </div>
                        <div class="d-flex justify-content-end">

                            <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> Dodaci</button>
                        </div>
                        <!-- <?= Html::a('<span class="material-icons">file_download</span> PDF', ['/prisutnost/zaposlenici-pdf-mjesecni'], ['class' => 'btn btn-success']) ?> -->
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="blue">
                        <i class="material-icons">face</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Prisutnost 1 zaposlenika') ?> </h4>
                        <h4 class="title">&nbsp;</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('prisutnost/zaposlenici-pojedini-pdf-mjesecni')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">

                            <?= Select2::widget([
                                'name' => 'zaposlenici',
                                'data' => $zaposlenici,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite zaposlenika'),
                                    'multiple' => false,
                                    'class' => 'form-control',
                                    'required' => true

                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>
                        </div>

                        <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> PDF</button>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">commute</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Prijevoz') ?> </h4>
                        <h4 class="title">&nbsp;</h4>

                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('prijevoz-zaposlenik/zaposlenici-prijevoz-pdf-mjesecni')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">
                            <?= Select2::widget([
                                'name' => 'mjesec',
                                'data' => $mjeseci,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                                    'multiple' => false,
                                    'required' => true,
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>
                        </div>
                        <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> PDF</button>
                        <?php ActiveForm::end(); ?>
                    </div>



                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="blue">
                        <i class="material-icons">fastfood</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Obroci') ?> </h4>
                        <h4 class="title">&nbsp;</h4>

                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('zaposlenik-obrok/zaposlenici-obroci-pdf-mjesecni')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">
                            <?= Select2::widget([
                                'name' => 'mjesec',
                                'data' => $mjeseci,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                                    'multiple' => false,
                                    'required' => true,
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>
                        </div>
                        <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> PDF</button>
                        <?php ActiveForm::end(); ?>
                    </div>



                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">event_available</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Sati i godišnji') ?> </h4>
                        <h4 class="title">&nbsp;</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('prisutnost/zaposlenici-pdf-godisnji')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">
                            <?= Select2::widget([
                                'name' => 'mjesec',
                                'data' => $mjeseci,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                                    'multiple' => false,
                                    'required' => true,
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>
                        </div>
                        <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> PDF</button>
                        <?php ActiveForm::end(); ?>



                    </div>



                </div>
            </div>
        </div>
    </div>
</div>
</div>