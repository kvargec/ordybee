<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use kartik\widgets\DatePicker;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\widgets\FileInput;
use common\models\PedagoskaGodina;
/* @var $this yii\web\View */
/* @var $model common\models\DnevnikZapis */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="dnevnik-zapis-form col-lg-12">
    <?php $title = $model->zapis ? $model->zapis : null;
    $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one(); 
    ?>

    <div class="form-group">
        <!-- <label class="control-label"><?= Yii::t('app', 'Naziv aktivnosti') ?></label> -->

        <?= Html::textInput('neposredni[zapis_custom]', $title, ['class' => 'form-control hidden', 'style' => 'width:350px', 'placeholder' => Yii::t('app', 'Upišite naziv aktivnosti...')]) ?>
        <label class="control-label"><?= Yii::t('app', 'Grupa') ?></label>
        <?= Select2::widget([
                'name'=>'neposredni[grupacija]',
                'data' => [
                    'roditelji' => Yii::t('app', 'Roditelji'), 
                    'zaposlenici' => Yii::t('app', 'Zaposlenici'), 
                    'djeca' => Yii::t('app', 'Djeca'), 
                ],
                'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['neposredni']['grupacija'])) ? $model->vrijednosti_atributa['neposredni']['grupacija'] : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberi grupu...'), 
                    'id' => 'neposredni-grupacija'
                ]
            ]); ?>
            <label class="control-label"><?= Yii::t('app', 'Pojedinci') ?></label>
            <?= DepDrop::widget([
                'name'=>'neposredni[pojedinci]',
                'type' => DepDrop::TYPE_SELECT2,
                'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['neposredni']['pojedinci'])) ? $model->vrijednosti_atributa['neposredni']['pojedinci'] : null,
                'options' => [
                    'id' => 'neposredni-pojedinci'
                ],
                'select2Options' => [
                    'pluginOptions' => [
                        'placeholder' => Yii::t('app', 'Odaberite...'),
                        'allowClear' => true, 
                        'multiple' => true
                        ]
                    ],
                'pluginOptions' => [
                    'depends' => ['neposredni-grupacija'],
                    'placeholder' => Yii::t('app', 'Odaberite...'),
                    'initialize' => true,
                    'loadingText' => '',
                    'url' => Url::to(['/dnevnik-zapis/grupacije'])
                ]
            ]);
        ?>
        <!--<div class="form-group"><label class="control-label"><?= Yii::t('app', 'Skupina') ?></label>
            <?/*= Select2::widget([
                'name' => 'neposredni[vrsta]',
                'data' => ArrayHelper::map(
                    \common\models\Skupina::find()->where(['ped_godina' => $godina->id])->all(),
                    'id',
                    function ($v) {
                        return $v->naziv;
                    }
                ),
                'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['neposredni']['skupina'])) ? $model->vrijednosti_atributa['neposredni']['skupina'] : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite skupinu...'),
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ]) */?>
        </div>-->
        <div class="form-group"><label class="control-label"><?= Yii::t('app', 'Aktivnost') ?></label>
            <?= Select2::widget([
                'name' => 'neposredni[aktivnost]',
                'data' => [
                    'Promatranje u skupini' => 'Promatranje u skupini',
                    'Individualni rad s djetetom' => 'Individualni rad s djetetom',
                    'Radionica s djecom u skupini' => 'Radionica s djecom u skupini',
                    'Informativni suradnja s odgojiteljima' => 'Informativni suradnja s odgojiteljima',
                    'Savjetodavna suradnja s odgojiteljima' => 'Savjetodavna suradnja s odgojiteljima',
                    'Radni dogovori' => 'Radni dogovori',
                    'Savjetodavna suradnja s roditeljima' => 'Savjetodavna suradnja s roditeljima',
                    'Roditeljski sastanak' => 'Roditeljski sastanak',
                    'Edukativna radionica' => 'Edukativna radionica',
                    'Nešto drugo' => 'Nešto drugo'
                ],
                'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['neposredni']['aktivnost'])) ? $model->vrijednosti_atributa['neposredni']['aktivnost'] : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite vrstu aktivnost...'),
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
        <div class="row mt-15">
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'neposredni[start]',
                    'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['neposredni']['start'] : date('d.m.Y. 08:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Od')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?> </div>
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'neposredni[end]',
                    'value' => (!$model->isNewRecord && array_key_exists('neposredni', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['neposredni']['end'] : date('d.m.Y. 13:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Do')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right', 'style'=>'z-index:100;']) ?>
                </div>
            </div>
        </div>
    </div>
</div>