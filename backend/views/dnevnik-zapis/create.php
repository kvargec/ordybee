<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DnevnikZapis */

$this->title = Yii::t('app', 'Create Dnevnik Zapis');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dnevnik Zapis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dnevnik-zapis-create">

<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>

        </div>
        <div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
