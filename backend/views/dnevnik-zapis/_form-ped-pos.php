<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use kartik\widgets\DatePicker;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\FileInput;
use common\models\PedagoskaGodina;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\DnevnikZapis */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="dnevnik-zapis-form col-lg-12">
    <?php $title = $model->zapis ? $model->zapis : null;
    $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one(); ?>

    <div class="form-group">
        <!-- <label class="control-label"><?= Yii::t('app', 'Naziv aktivnosti') ?></label> -->

        <?= Html::textInput('posredni[zapis_custom]', $title, ['class' => 'form-control hidden', 'style' => 'width:350px', 'placeholder' => Yii::t('app', 'Upišite naziv aktivnosti...')]) ?>
        <label class="control-label"><?= Yii::t('app', 'Grupa') ?></label>
        <?= Select2::widget([
                'name'=>'posredni[grupacija]',
                'data' => [
                    'roditelji' => Yii::t('app', 'Roditelji'), 
                    'zaposlenici' => Yii::t('app', 'Zaposlenici'), 
                    'djeca' => Yii::t('app', 'Djeca'), 
                ],
                'value' => (!$model->isNewRecord && array_key_exists('posredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['posredni']['grupacija'])) ? $model->vrijednosti_atributa['posredni']['grupacija'] : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberi grupu...'), 
                    'id' => 'posredni-grupacija'
                ]
            ]); ?>
            <label class="control-label"><?= Yii::t('app', 'Pojedinci') ?></label>
            <?= DepDrop::widget([
                'name'=>'posredni[pojedinci]',
                'type' => DepDrop::TYPE_SELECT2,
                'value' => (!$model->isNewRecord && array_key_exists('posredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['posredni']['pojedinci'])) ? $model->vrijednosti_atributa['posredni']['pojedinci'] : null,
                'options' => [
                    'id' => 'posredni-pojedinci'
                ],
                'select2Options' => [
                    'pluginOptions' => [
                        'placeholder' => Yii::t('app', 'Odaberite...'),
                        'allowClear' => true, 
                        'multiple' => true
                        ]
                    ],
                'pluginOptions' => [
                    'depends' => ['posredni-grupacija'],
                    'placeholder' => Yii::t('app', 'Odaberite...'),
                    'initialize' => true,
                    'loadingText' => '',
                    'url' => Url::to(['/dnevnik-zapis/grupacije'])
                ]
            ]);
        ?>
        <div class="form-group"><label class="control-label"><?= Yii::t('app', 'Aktivnost') ?></label>
            <?= Select2::widget([
                'name' => 'posredni[aktivnost]',
                'data' => [
                    'Dokumentiranje' => 'Dokumentiranje',
                    'Priprema oblika profesionalnog učenja i rada' => 'Priprema oblika profesionalnog učenja i rada',
                    'Stručno usavršavanje' => 'Stručno usavršavanje',
                    'Ostali poslovi' => 'Ostali poslovi'
                ],
                'value' => (!$model->isNewRecord && array_key_exists('posredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['posredni']['aktivnost'])) ? $model->vrijednosti_atributa['posredni']['aktivnost'] : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite vrstu aktivnost...'),
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
        <div class="row mt-15">
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'posredni[start]',
                    'value' => (!$model->isNewRecord && array_key_exists('posredni', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['posredni']['start'] : date('d.m.Y. 08:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Od')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?> </div>
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'posredni[end]',
                    'value' => (!$model->isNewRecord && array_key_exists('posredni', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['posredni']['end'] : date('d.m.Y. 13:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Do')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right', 'style'=>'z-index:100;']) ?>
                </div>
            </div>
        </div>
    </div>
</div>