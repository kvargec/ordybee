<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DnevnikZapisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dnevnici rada po zaposlenicima');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dnevnik-zapis-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">groups</span>&nbsp;Po skupinama', ['dnevnik-rada-index-skupina'], ['class' => 'btn btn-primary']) ?>
            <!-- <?= Html::a('<span class="material-icons">groups</span>&nbsp;Po skupinama', ['dnevnik-rada-index-skupina'], ['class' => 'btn-simple right mt-15']) ?> -->
        </div>
        <div class="card-body">
        
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

        <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'value' => 'ime',
                        'label' => Yii::t('app', 'Ime'),
                        'attribute' => 'ime',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Ime'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'value' => 'prezime',
                        'label' => Yii::t('app', 'Prezime'),
                        'attribute' => 'prezime',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Prezime'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data)  {

                                $url2 = \Yii::$app->urlManager->createUrl(['dnevnik-zapis/dnevnik-rada-index-zap', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pregledaj zapise'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>

        </div>
    </div>
</div>