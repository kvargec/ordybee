<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DnevnikZapis */

$this->title = Yii::t('app', 'Update Dnevnik Zapis: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dnevnik Zapis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dnevnik-zapis-update">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        
    </div>
<div class="card-body detail">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
