<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\FileInput;
use common\models\PedagoskaGodina;
/* @var $this yii\web\View */
/* @var $model common\models\DnevnikZapis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dnevnik-zapis-form  col-lg-12">
    <?php $title = $model->zapis ? $model->zapis : null;
    $godina = PedagoskaGodina::find()->where(['aktivna' => true])->one();

    $v_array3 = [];
    if (!$model->isNewRecord) {
        foreach ($model->vrijednosti_atributa as $key => $vrijednosti) {
            if ($key == 'posredni') {
                foreach ($vrijednosti as $k => $v) {
                    if ($k == 'aktivnost') {
                        if (is_array($v)) {
                            foreach ($v as $i) {
                                switch ($i) {
                                    case 'Životno praktične i radne':
                                        $v_array3[] = '0';
                                        break;
                                    case 'Raznovrsne igre':
                                        $v_array3[] = '1';
                                        break;
                                    case 'Društvene i društvene zabavne':
                                        $v_array3[] = '2';
                                        break;
                                    case 'Umjetničke':
                                        $v_array3[] = '3';
                                        break;
                                    case 'Raznovrsno izražavanje i stvaranje':
                                        $v_array3[] = '4';
                                        break;
                                    case 'Istraživačko spoznajne':
                                        $v_array3[] = '5';
                                        break;
                                    case 'Specifične aktivnosti s kretanjem':
                                        $v_array3[] = '6';
                                        break;
                                    default:
                                        $v_array3[] = $i;
                                        break;
                                }
                            }
                        } else {
                            switch ($v) {
                                case 'Životno praktične i radne':
                                    $v_array3[] = '0';
                                    break;
                                case 'Raznovrsne igre':
                                    $v_array3[] = '1';
                                    break;
                                case 'Društvene i društvene zabavne':
                                    $v_array3[] = '2';
                                    break;
                                case 'Umjetničke':
                                    $v_array3[] = '3';
                                    break;
                                case 'Raznovrsno izražavanje i stvaranje':
                                    $v_array3[] = '4';
                                    break;
                                case 'Istraživačko spoznajne':
                                    $v_array3[] = '5';
                                    break;
                                case 'Specifične aktivnosti s kretanjem':
                                    $v_array3[] = '6';
                                    break;
                                default:
                                    $v_array3[] = $v;
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    ?>


    <div class="form-group">
        <!-- <label class="control-label"><?= Yii::t('app', 'Naziv aktivnosti') ?></label> -->
        <?= Html::textInput('posredni[zapis_custom]', $title, ['class' => 'form-control hidden', 'style' => 'width:350px', 'placeholder' => Yii::t('app', 'Upišite naziv aktivnosti...')]) ?>
        <div class="form-group"><label class="control-label"><?= Yii::t('app', 'Skupina') ?></label>
            <?= Select2::widget([
                'name' => 'posredni[skupina]',
                'data' => ArrayHelper::map(
                    \common\models\Skupina::find()->where(['ped_godina' => $godina->id])->all(),
                    'id',
                    function ($v) {
                        return $v->naziv;
                    }
                ),
                'value' => (!$model->isNewRecord && array_key_exists('posredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['posredni']['skupina'])) ? $model->vrijednosti_atributa['posredni']['skupina'] : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite skupinu...'),
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
        <div class="form-group"><label class="control-label"><?= Yii::t('app', 'Aktivnost') ?></label>
            <?= Select2::widget([
                'name' => 'posredni[aktivnost]',
                'data' => [
                    'Vođenje knjige pedagoške dokumentacije odgojno-obrazovne skupine' => 'Vođenje knjige pedagoške dokumentacije odgojno-obrazovne skupine',
                    'Poslovi za ostvarivanje tjednog plana i programa (izrada sredstava za rad s djecom, uređenje prostora za realizaciju odgojno-obrazovnog rada)' => 'Poslovi za ostvarivanje tjednog plana i programa (izrada sredstava za rad s djecom, uređenje prostora za realizaciju odgojno-obrazovnog rada)',
                    'Dokumentiranje aktivnosti djece (razvojne mape, uradci djece, samorefleksije djece, narativni oblici, opservacije i postignuća djece)' => 'Dokumentiranje aktivnosti djece (razvojne mape, uradci djece, samorefleksije djece, narativni oblici, opservacije i postignuća djece)',
                    'Dokumentiranje aktivnosti odgojitelja (samorefeksija i zajedničke refleksije odgojitelja ipedagoga)' => 'Dokumentiranje aktivnosti odgojitelja (samorefeksija i zajedničke refleksije odgojitelja ipedagoga)',
                    'Suradnja s roditeljima (individualni razgovori/savjetodavni rad) (Ime i prezime roditelja, tema razgovora, na čiju inicijativu)' => 'Suradnja s roditeljima (individualni razgovori/savjetodavni rad) (Ime i prezime roditelja, tema razgovora, na čiju inicijativu)',
                    'Suradnja s pedagogom (uvid u praćenje i dokumentaciju o aktivnostima te razgovori o tekućoj problematici i situacijama)' => 'Suradnja s pedagogom (uvid u praćenje i dokumentaciju o aktivnostima te razgovori o tekućoj problematici i situacijama)',
                    'Stručni aktiv (navesti temu i tko je voditelj)' => 'Stručni aktiv (navesti temu i tko je voditelj)',
                    'Odgojiteljsko vijeće (navesti dnevni red)' => 'Odgojiteljsko vijeće (navesti dnevni red)',
                    'Roditeljski sastanci/edukativne i kreativne radionice/druženja s roditeljima (navesti temu te prisutne roditelje)' => 'Roditeljski sastanci/edukativne i kreativne radionice/druženja s roditeljima (navesti temu te prisutne roditelje)',
                    'Nešto drugo' => 'Nešto drugo'
                ],
                'value' => (!$model->isNewRecord && array_key_exists('posredni', $model->vrijednosti_atributa) && isset($model->vrijednosti_atributa['posredni']['aktivnost'])) ? $v_array3 : null,
                'options' => [
                    'placeholder' => Yii::t('app', 'Odaberite vrstu aktivnost...'),
                    'multiple' => true,
                ],
                'pluginOptions' => ['allowClear' => true],
            ]) ?>
        </div>
        <div class="row mt-15">
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'posredni[start]',
                    'value' => (!$model->isNewRecord && array_key_exists('posredni', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['posredni']['start'] : date('d.m.Y. 08:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Od')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?> </div>
            <div class="col-lg-6">
                <?= DateTimePicker::widget([
                    'model' => $model,
                    'name' => 'posredni[end]',
                    'value' => (!$model->isNewRecord && array_key_exists('posredni', $model->vrijednosti_atributa)) ? $model->vrijednosti_atributa['posredni']['end'] : date('d.m.Y. 13:00'),
                    'convertFormat' => true,
                    'readonly' => true,
                    'options' => ['placeholder' => Yii::t('app', 'Do')],
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'autoclose' => true
                    ]
                ]);
                ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success float-right submit-button', 'style' => 'z-index:100;']) ?>
                </div>
            </div>
        </div>
    </div>
</div>