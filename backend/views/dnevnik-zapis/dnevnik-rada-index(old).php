<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DnevnikZapisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dnevnici rada');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dnevnik-zapis-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['creatednevnikrada'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'zapis:ntext',
                    [
                        'label'=>Yii::t('app','Zaposlenik'),
                        'value'=>'dnevnik0.zaposlenik0.fullName',
                    ],
                    [
                        'label'=>Yii::t('app','Napravljeno'),
                        'value'=>function($data){ 
                            return Yii::$app->formatter->asDate($data->created_at, 'php:j.n.Y. H:i'); 
                        },
                    ],
                    [
                        'label'=>Yii::t('app','Ažurirano'),
                        'value'=>function($data){ 
                            return Yii::$app->formatter->asDate($data->updated_at, 'php:j.n.Y. H:i'); 
                        },
                    ],
                    [
                        'format'=>'html',
                        'label'=>Yii::t('app','Vrijednosti'),
                        'value'=>function($data){
                            $tmp = $data->vrijednosti_atributa;
                            foreach(array_reverse($tmp) as $tmp){
                            $txt='';

                                foreach ($tmp as $key => $value) {

										if($key=='posredni'){
											$koji=['Životno praktične i radne', 'Raznovrsne igre', 'Društvene i društvene zabavne', 'Umjetničke', 'Raznovrsno izražavanje i stvaranje', 'Istraživačko spoznajne', 'Specifične aktivnosti s kretanjem'];
										}elseif (($key=='neposredni')){
											$koji=['Vođenje knjige pedagoške dokumentacije odgojno-obrazovne skupine','Poslovi za ostvarivanje tjednog plana i programa (izrada sredstava za rad s djecom, 
                uređenje prostora za realizaciju odgojno-obrazovnog rada) ','Dokumentiranje aktivnosti djece (razvojne mape, uradci djece, samorefleksije djece, 
                narativni oblici, opservacije i postignuća djece) ','Dokumentiranje aktivnosti odgojitelja (samorefeksija i zajedničke refleksije odgojitelja i 
                pedagoga) ','Suradnja s roditeljima (individualni razgovori/savjetodavni rad) (Ime i prezime roditelja, 
                tema razgovora, na čiju inicijativu)','Suradnja s pedagogom (uvid u praćenje i dokumentaciju o aktivnostima te razgovori o 
                tekućoj problematici i situacijama)','Stručni aktiv (navesti temu i tko je voditelj)','Odgojiteljsko vijeće (navesti dnevni red)','Roditeljski sastanci/edukativne i kreativne radionice/druženja s roditeljima (navesti temu 
                te prisutne roditelje)','Nešto drugo'];
										}else{
											$koji=['Rad na literaturi','Poslovi za ostvarivanje tjednog plana i programa (priprema poticaja za rad s djecom )','Stručno usavršavanje (online/uživo) navesti naziv edukacije i mjesto održavanja','Nešto drugo'];
										}
										/*if(isset($value) and is_array($value)){
											foreach ($value as $key2=>$value2){
											if($key2=='skupina'){
												$skupine=\common\models\Skupina::find()->where(['in','id',$value])->all();
												$arr=array_map(function($obj){return $obj->naziv;},$skupine);
												$tempSk=implode(', ',$arr);

												$txt.= 'Skupine: '.$tempSk;
											}
											if($key2=='aktivnost'){

												$tempAk='';
												foreach($value2 as $item){
													$tempAk.=$koji[$item].' ';
												}

												$txt.= 'Aktivnosti: '.$tempAk;
											}
										}
										}*/
										if(is_array($value)){
											$value=implode(', ',$value);
										}
										$txt .= $key . ': ' . $value . '<br>';


                                }
                                return $txt;
                            }
                        }

                ],
                    // 'vrsta',
                    // 'status',
                    //'created_at',
                    //'updated_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}  ',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data)  {

                                $url2 = \Yii::$app->urlManager->createUrl(['dnevnik-zapis/viewdnevnikrada', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pregledaj zapise'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            },



                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>