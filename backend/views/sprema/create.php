<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sprema */

$this->title = Yii::t('app', 'Create Sprema');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Spremas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sprema-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
