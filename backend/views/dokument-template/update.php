<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DokumentTemplate */

$this->title = Yii::t('app', 'Ažuriranje predložak dokumenta: {name}', [
    'name' => $model->naziv,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Predlošci dokumenata'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->slug]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dokument-template-update">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        
    </div>
<div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
