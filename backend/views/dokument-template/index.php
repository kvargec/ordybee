<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DokumentTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Predlošci dokumenata');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dokument-template-index">


    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>

        </div>
        <div class="card-body">


            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'value' => 'naziv',
                        'label' => Yii::t('app', 'Naziv'),
                        'attribute' => 'naziv',
                        //'filterOptions' => [
                        //    'class' => 'form-group'
                        //],
                        //'filterInputOptions' => [
                        //    'placeholder' => Yii::t('app', 'Naziv'),
                        //    'class' => 'form-control'
                        //]
                    ],
                    [
                        'value' => 'template',
                        'label' => Yii::t('app', 'Template'),
                        'attribute' => 'template',
                        //'filterOptions' => [
                        //    'class' => 'form-group'
                        //],
                        //'filterInputOptions' => [
                        //    'placeholder' => Yii::t('app', 'Template'),
                        //    'class' => 'form-control'
                        //]
                    ],
                    [
                        'value' => 'slug',
                        'label' => Yii::t('app', 'Šifra'),
                        'attribute' => 'slug',
                        //'filterOptions' => [
                        //    'class' => 'form-group'
                        //],
                        //'filterInputOptions' => [
                        //    'placeholder' => Yii::t('app', 'Šifra'),
                        //    'class' => 'form-control'
                        //]
                    ],

                    // 'sadrzaj:ntext',
                    //'ped_godina',
                    [
                        'label' => Yii::t('app', 'Ped. godina'),
                        'attribute' => 'ped_godina',
                        'value' => function ($data) {
                            return $data->pedGodina->getNaziv();
                        },
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filter' => yii\helpers\ArrayHelper::map(\common\models\PedagoskaGodina::find()->all(), 'id', function ($v) {
                            return $v->getNaziv();
                        })
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}{update}',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data) {

                                $url2 = \Yii::$app->urlManager->createUrl(['dokument-template/view', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pogledaj'),
                                ]);
                            },

                            'update' => function ($url, $data) {
                                $url2 = \Yii::$app->urlManager->createUrl(['dokument-template/update', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">create</span>', $url2, [
                                    'title' => Yii::t('app', 'Ažuriraj'),
                                ]);
                            },


                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>