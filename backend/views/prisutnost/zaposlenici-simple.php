<?php

use kartik\widgets\TimePicker;
use yii\helpers\Html;
use kartik\select2\Select2;

$pon = date('j.n.Y', strtotime('monday this week'));
$pet = date('j.n.Y', strtotime('friday this week'));
$tjedan = $_GET['tjedan'] ?? 0;
?>

<div class="card">
    <div class="card-header card-header-primary ">
        <span class="card-title"><?= Yii::t('app',  'Unos prisutnosti za zaposlenike') ?></span>
        <span class="card-subtitle"> <?= Yii::t('app', 'Period: ') ?> <?php echo date('j.n.', strtotime($datumi['pon'])) . ' - ' . date('j.n.', strtotime($datumi['pet'])) ?></span>
        <?= \yii\bootstrap\ButtonDropdown::widget([
            'encodeLabel' => false,
            'label' => Yii::t('app', 'Odabir tjedna'),
            'dropdown' => [
                'items' => [
                    ['label' => date('j.n.', strtotime('-6 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-6 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/zaposlenici', 'tjedan' => '-6'])],
                    ['label' => date('j.n.', strtotime('-5 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-5 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/zaposlenici', 'tjedan' => '-5'])],
                    ['label' => date('j.n.', strtotime('-4 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-4 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/zaposlenici', 'tjedan' => '-4'])],
                    ['label' => date('j.n.', strtotime('-3 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-3 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/zaposlenici', 'tjedan' => '-3'])],
                    ['label' => date('j.n.', strtotime('-2 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-2 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/zaposlenici',  'tjedan' => '-2'])],
                    ['label' => date('j.n.', strtotime('-1 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-1 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/zaposlenici',  'tjedan' => '-1'])],
                    ['label' => date('j.n.', strtotime($pon)) . ' - ' . date('j.n', strtotime($pet)), 'url' => \yii\helpers\Url::to(['/prisutnost/zaposlenici', 'tjedan' => '0'])],
                ]
            ]
        ]) ?>
        <?= Html::a('<span class="material-icons">add</span><span class="material-icons">commute</span>', ['prijevoz-zaposlenik/zaposlenici'], ['class' => 'btn btn-success']) ?>

        <?= Html::a('<span class="material-icons">add</span><span class="material-icons">fastfood</span>', ['zaposlenik-obrok/zaposlenici'], ['class' => 'btn btn-success']) ?>
        <span class="btn-group">
            <!-- <?= Html::a('<span class="material-icons">file_download</span> PDF', ['/prisutnost/zaposlenici-pdf'], ['class' => 'btn btn-success']) ?> -->
            <!-- <?= Html::a('<span class="material-icons">file_download</span> EXCEL', ['/prisutnost/zaposlenici-excel'], ['class' => 'btn btn-success']) ?> -->
            <?php
            $rola = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
            $uloge = array();
            foreach ($rola as $tko => $opis) {
                $uloge[] = $tko;
            }
            if ($uloge[0] == 'superadmin' || $uloge[0] == 'ravnateljstvo' || $uloge[0] == 'administracija') {




                //        die();
            ?>

                <?= Html::a('<span class="material-icons">file_download</span> Izvještaji', ['/prisutnost/izvjesce'], ['class' => 'btn btn-success']); ?>
            <?php } ?>
        </span>
    </div>
    <div class="card-body zaposlenici">
        <table id="w0" class="table table-bordered table-striped desktop">
            <thead>
                <?php

                switch ($tjedan) {
                    case 0:
                ?>
                        <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                        <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week'))) ?>&nbsp;</th>
                    <?php
                        break;
                    case -1:
                    ?>
                        <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                        <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('last week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('last week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('last week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('last week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('last week'))) ?>&nbsp;</th>
                    <?php
                        break;
                    case -2:
                    ?>
                        <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                        <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                    <?php
                        break;
                    case -3:
                    ?>
                        <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                        <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                    <?php
                        break;
                    case -4:
                    ?>
                        <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                        <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                    <?php
                        break;
                    case -5:
                    ?>
                        <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                        <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                    <?php
                        break;
                    case -6:
                    ?>
                        <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                        <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                        <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                <?php
                        break;
                }
                ?>
            </thead>
            <tbody class="" id="simple">
                <?php
                foreach ($zaposlenici as $zaposlenik) {
                ?>
                    <?php if (Yii::$app->user->getId() == $zaposlenik->user) {   ?>
                        <tr>
                            <td><?php echo $zaposlenik->ime . " " . $zaposlenik->prezime;  ?></td>
                            <?php foreach ($datumi  as $key => $datum) {
                                $htmlRespnse = '' ?>
                                <td style="text-align: center">
                                    <?php
                                    $prisutnost = $zaposlenik->getPrisutnost()->where(['datum' => $datum])->one();
                                    if (!isset($prisutnost)) : ?>
                                        <form action="" method="post" id="prisutnost-zaposlenika<?= $key . '-' . $zaposlenik->id ?>">

                                            <div class="form-group">
                                                <?php
                                                echo Html::textInput('prisutnost_type', 'simple', ['class' => 'form-control hidden']);

                                                echo Html::dropDownList(
                                                    'razlog',
                                                    'rad_8h',
                                                    [
                                                        'rad_8h' => 'Rad 8h',
                                                        'rad_4h' => 'Rad 4h',
                                                        'godisnji' => 'Godišnji',
                                                        'placeni_dopust' => 'Plaćeni dopust',
                                                        'bolovanje' => 'Bolovanje',
                                                        'slobodni_dan' => 'Slobodni dan',
                                                        'porodiljni' => 'Porodiljni',
                                                        'neradno' => 'Blagdan/Praznik',
                                                        'ostalo' => 'Ostalo'
                                                    ]
                                                )
                                                ?>
                                            </div>
                                            <input hidden name="id" value="<?php echo $zaposlenik->id ?>">
                                            <input hidden name="datum" value="<?php echo $datum ?> ">
                                            <button type="submit" class="btn btn-primary btn-sm mini"><span class="material-icons">check</span></button>
                                        </form>
                                    <?php else : ?>
                                        <?php
                                        echo Yii::$app->controller->renderPartial('zaposlenici-prisutnost-summary', ['prisutnost' => $prisutnost]);
                                        ?>
                                    <?php endif; ?>
                                </td>
                            <?php }  ?>
                        </tr>
                    <?php } elseif (Yii::$app->user->identity->username == 'root' || Yii::$app->user->can('administracija') || Yii::$app->user->can('prisutnost')) { ?>
                        <tr>

                            <td><?php echo $zaposlenik->ime . " " . $zaposlenik->prezime;  ?></td>
                            <?php foreach ($datumi  as $key => $datum) {
                                $htmlRespnse = '' ?>
                                <td style="text-align: center">
                                    <?php
                                    $prisutnost = $zaposlenik->getPrisutnost()->where(['datum' => $datum])->one();
                                    if (!isset($prisutnost)) : ?>
                                        <form action="" method="post" id="prisutnost-zaposlenika<?= $key . '-' . $zaposlenik->id ?>">

                                            <div class="form-group">
                                                <?php
                                                echo Html::textInput('prisutnost_type', 'simple', ['class' => 'form-control hidden']);

                                                echo Html::dropDownList(
                                                    'razlog',
                                                    'rad_8h',
                                                    [
                                                        'rad_8h' => 'Rad 8h',
                                                        'rad_4h' => 'Rad 4h',
                                                        'godisnji' => 'Godišnji',
                                                        'placeni_dopust' => 'Plaćeni dopust',
                                                        'bolovanje' => 'Bolovanje',
                                                        'slobodni_dan' => 'Slobodni dan',
                                                        'porodiljni' => 'Porodiljni',
                                                        'neradno' => 'Blagdan/Praznik',
                                                        'ostalo' => 'Ostalo'
                                                    ]
                                                )
                                                ?>
                                            </div>
                                            <input hidden name="id" value="<?php echo $zaposlenik->id ?>">
                                            <input hidden name="datum" value="<?php echo $datum ?> ">
                                            <button type="submit" class="btn btn-primary btn-sm mini"><span class="material-icons">check</span></button>
                                        </form>
                                    <?php else : ?>
                                        <?php
                                        echo Yii::$app->controller->renderPartial('zaposlenici-prisutnost-summary', ['prisutnost' => $prisutnost]);
                                        ?>
                                    <?php endif; ?>
                                </td>
                            <?php }  ?>
                        </tr>
                <?php
                    }
                } ?>

            </tbody>
        </table>
        <table class="table table-striped table-bordered mob zaposlenici">

            <?php

            foreach ($zaposlenici as $zaposlenik) { ?>
                <?php if (Yii::$app->user->getId() == $zaposlenik->user) {   ?>
                    <thead>
                        <th colspan="5" class="text-center"><?= Yii::t('app', 'Zaposlenik') ?>: <?php echo $zaposlenik->ime . " " . $zaposlenik->prezime;  ?></th>

                    </thead>
                    <tbody>
                        <?php

                        switch ($tjedan) {
                            case 0:
                        ?>
                                <tr>
                                    <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week'))) ?>&nbsp;</th>
                                </tr>
                            <?php
                                break;
                            case -1:
                            ?>
                                <tr>
                                    <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('last week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('last week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('last week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('last week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('last week'))) ?>&nbsp;</th>
                                </tr>
                            <?php
                                break;
                            case -2:
                            ?>
                                <tr>
                                    <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                </tr>
                            <?php
                                break;
                            case -3:
                            ?>
                                <tr>
                                    <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                </tr>
                            <?php
                                break;
                            case -4:
                            ?>
                                <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                            <?php
                                break;
                            case -5:
                            ?>
                                <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                            <?php
                                break;
                            case -6:
                            ?>
                                <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                        <?php
                                break;
                        }
                        ?>
                        <tr>

                            <?php foreach ($datumi  as $key => $datum) {
                                $htmlRespnse = '' ?>
                                <td style="text-align: center">
                                    <?php
                                    $prisutnost = $zaposlenik->getPrisutnost()->where(['datum' => $datum])->one();
                                    if (!isset($prisutnost)) : ?>
                                        <form action="" method="post" id="prisutnost-zaposlenika<?= $key . '-' . $zaposlenik->id ?>">

                                            <div class="form-group">
                                                <?php
                                                echo Html::textInput('prisutnost_type', 'simple', ['class' => 'form-control hidden']);

                                                echo Html::dropDownList(
                                                    'razlog',
                                                    'rad_8h',
                                                    [
                                                        'rad_8h' => 'Rad 8h',
                                                        'rad_4h' => 'Rad 4h',
                                                        'godisnji' => 'Godišnji',
                                                        'placeni_dopust' => 'Plaćeni dopust',
                                                        'bolovanje' => 'Bolovanje',
                                                        'slobodni_dan' => 'Slobodni dan',
                                                        'porodiljni' => 'Porodiljni',
                                                        'neradno' => 'Blagdan/Praznik',
                                                        'ostalo' => 'Ostalo'
                                                    ]
                                                )
                                                ?>
                                            </div>
                                            <input hidden name="id" value="<?php echo $zaposlenik->id ?>">
                                            <input hidden name="datum" value="<?php echo $datum ?> ">
                                            <button type="submit" class="btn btn-primary btn-sm mini"><span class="material-icons">check</span></button>
                                        </form>
                                    <?php else : ?>
                                        <?php
                                        echo Yii::$app->controller->renderPartial('zaposlenici-prisutnost-summary', ['prisutnost' => $prisutnost]);
                                        ?>
                                    <?php endif; ?>
                                </td>
                            <?php }  ?>
                        </tr>
                    <?php } elseif (Yii::$app->user->identity->username == 'root' || Yii::$app->user->identity->username == 'administracija') { ?>
                        <thead>
                            <th colspan="5" class="text-center"><?= Yii::t('app', 'Zaposlenik') ?>: <?php echo $zaposlenik->ime . " " . $zaposlenik->prezime;  ?></th>

                        </thead>
                    <tbody>
                        <?php

                        switch ($tjedan) {
                            case 0:
                        ?>
                                <tr>
                                    <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week'))) ?>&nbsp;</th>
                                </tr>
                            <?php
                                break;
                            case -1:
                            ?>
                                <tr>
                                    <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('last week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('last week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('last week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('last week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('last week'))) ?>&nbsp;</th>
                                </tr>
                            <?php
                                break;
                            case -2:
                            ?>
                                <tr>
                                    <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -2 week'))) ?>&nbsp;</th>
                                </tr>
                            <?php
                                break;
                            case -3:
                            ?>
                                <tr>
                                    <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                    <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -3 week'))) ?>&nbsp;</th>
                                </tr>
                            <?php
                                break;
                            case -4:
                            ?>
                                <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -4 week'))) ?>&nbsp;</th>
                            <?php
                                break;
                            case -5:
                            ?>
                                <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -5 week'))) ?>&nbsp;</th>
                            <?php
                                break;
                            case -6:
                            ?>
                                <th><?= Yii::t('app',  'Zaposlenik') ?></th>
                                <th style="text-align: center"><?= Yii::t('app',  'PON') ?><br> <?php echo date('j.n.Y', strtotime('monday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'UTO') ?><br> <?php echo date('j.n.Y', strtotime('tuesday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'SRI') ?><br> <?php echo date('j.n.Y', strtotime('wednesday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'ČET') ?><br> <?php echo date('j.n.Y', strtotime('thursday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                                <th style="text-align: center"><?= Yii::t('app',  'PET') ?><br> <?php echo date('j.n.Y', strtotime('friday', strtotime('this week -6 week'))) ?>&nbsp;</th>
                        <?php
                                break;
                        }
                        ?>
                        <tr>

                            <?php foreach ($datumi  as $key => $datum) {
                                $htmlRespnse = '' ?>
                                <td style="text-align: center">
                                    <?php
                                    $prisutnost = $zaposlenik->getPrisutnost()->where(['datum' => $datum])->one();
                                    if (!isset($prisutnost)) : ?>
                                        <form action="" method="post" id="prisutnost-zaposlenika<?= $key . '-' . $zaposlenik->id ?>">
                                            <div class="form-group">
                                                <?php
                                                echo Html::textInput('prisutnost_type', 'simple', ['class' => 'form-control hidden']);

                                                echo Html::dropDownList(
                                                    'razlog',
                                                    'rad_8h',
                                                    [
                                                        'rad_8h' => 'Rad 8h',
                                                        'rad_4h' => 'Rad 4h',
                                                        'godisnji' => 'Godišnji',
                                                        'placeni_dopust' => 'Plaćeni dopust',
                                                        'bolovanje' => 'Bolovanje',
                                                        'slobodni_dan' => 'Slobodni dan',
                                                        'porodiljni' => 'Porodiljni',
                                                        'neradno' => 'Blagdan/Praznik',
                                                        'ostalo' => 'Ostalo'
                                                    ]
                                                )
                                                ?>
                                            </div>
                                            <input hidden name="id" value="<?php echo $zaposlenik->id ?>">
                                            <input hidden name="datum" value="<?php echo $datum ?> ">
                                            <button type="submit" class="btn btn-primary btn-sm mini"><span class="material-icons">check</span></button>
                                        </form>
                                    <?php else : ?>
                                        <?php
                                        echo Yii::$app->controller->renderPartial('zaposlenici-prisutnost-summary', ['prisutnost' => $prisutnost]);
                                        ?>
                                    <?php endif; ?>
                                </td>
                            <?php }  ?>
                        </tr>
                <?php }
            } ?>
        </table>
        <?php
        $url = \yii\helpers\Url::to(['/prisutnost/dodaj-zaposlenik']);
        $js = <<<JS
$('body').on('submit', '[id^="prisutnost-zaposlenika"]', function(e) {
   var resultTr = $(this).closest('td');
    console.log(resultTr);
  e.preventDefault();
  $.ajax({
    type: 'POST',
    url: '$url',
    data: $(this).serialize(),
  }).done(function(data, textStatus, jqXHR) {
      if (data.status === 'success'){
       $.notify(data.message, "success");   
      }else{
          $.notify(data.message, "error");
      }
       resultTr.html(data.value);
    
  })
})
JS;
        $this->registerJs($js);
        ?>

        <?php
        $url2 = \yii\helpers\Url::to(['/prisutnost/update-zaposlenik-simple']);
        $js = <<<JS
$('body').on('submit', '[id^="update-prisutnost-zaposlenik-"]', function(e) {
   e.preventDefault();
    var resultRow = $(this).closest('td');
    $.ajax({
        type: 'post',
        url: '$url2',
        data: $(this).serialize(),
    }).done(function(data, textStatus, jqXHR) {
      resultRow.html(data.value);
})
})

JS;
        $this->registerJs($js);
        ?>
    </div>
</div>
</div>