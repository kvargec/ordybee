<?php

use yii\helpers\Html;


$pon = date('j.n.Y', strtotime('monday this week'));
$pet = date('j.n.Y', strtotime('friday this week'));
?>
<div class="card razlog-izostanka">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Yii::t('app', 'Unos razloga izostanka za skupinu:') ?> <?php echo $skupina->naziv ?></span>
        <?= Html::a(Yii::t('app', 'Povratak na unos prisutnosti'), ['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => $tjedan], ['class' => 'btn btn-success']) ?><br>

        <span class="card-title"><?= Yii::t('app', ' Period: ') ?><?php echo date('j.n.', strtotime($datumi['pon'])) . ' - ' . date('j.n.', strtotime($datumi['pet'])) ?></span>
        <?= \yii\bootstrap\ButtonDropdown::widget([
            'encodeLabel' => false,
            'label' => Yii::t('app', 'Odabir tjedna'),
            'dropdown' => [
                'items' => [
                    ['label' => date('j.n.', strtotime('-6 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-6 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '-6'])],
                    ['label' => date('j.n.', strtotime('-5 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-5 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '-5'])],
                    ['label' => date('j.n.', strtotime('-4 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-4 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '-4'])],
                    ['label' => date('j.n.', strtotime('-3 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-3 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/razlog-izostanka', 'id' => $skupina->id, 'tjedan' => '-3'])],
                    ['label' => date('j.n.', strtotime('-2 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-2 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/razlog-izostanka', 'id' => $skupina->id, 'tjedan' => '-2'])],
                    ['label' => date('j.n.', strtotime('-1 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-1 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/razlog-izostanka', 'id' => $skupina->id, 'tjedan' => '-1'])],
                    ['label' => date('j.n.', strtotime($pon)) . ' - ' . date('j.n.', strtotime($pet)), 'url' => \yii\helpers\Url::to(['/prisutnost/razlog-izostanka', 'id' => $skupina->id, 'tjedan' => '0'])],
                ]
            ]
        ]) ?>
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered">
            <thead>
                <th><?= Yii::t('app', 'Dijete') ?></th>
                <th style="text-align: center"><?= Yii::t('app', 'PON') ?><br> <?php echo date('j.n.Y', strtotime($datumi['pon'])) ?>&nbsp;</th>
                <th style="text-align: center"><?= Yii::t('app', 'UTO') ?><br> <?php echo date('j.n.Y', strtotime($datumi['uto'])) ?>&nbsp;</th>
                <th style="text-align: center"><?= Yii::t('app', 'SRI') ?><br> <?php echo date('j.n.Y', strtotime($datumi['sri'])) ?>&nbsp;</th>
                <th style="text-align: center"><?= Yii::t('app', 'ČET') ?><br> <?php echo date('j.n.Y', strtotime($datumi['cet'])) ?>&nbsp;</th>
                <th style="text-align: center"><?= Yii::t('app', 'PET') ?><br> <?php echo date('j.n.Y', strtotime($datumi['pet'])) ?>&nbsp;</th>
            </thead>
            <tbody>
                <?php

                foreach ($djeca as $one) { ?>
                    <tr id="<?= 'dijete' . $one->id ?>">

                        <td><?php echo $one->ime . " " . $one->prezime;  ?></td>
                        <?php foreach ($datumi as $key => $value) {
                            $dodatak = '' ?>

                            <td style="text-align: center; overflow-x:hidden;"><?php $datum = date('Y-m-d', strtotime($value));
                                                                                $prisutnost = $one->getPrisutnost()->where(['datum' => $datum])->one();
                                                                                if (isset($prisutnost)) {
                                                                                    if ($prisutnost->status == 0) {
                                                                                        if ($prisutnost->razlog_izostanka == null) {
                                                                                            $dodatak = '<div class="' . $one->id . $key . '"> <form action="' . \yii\helpers\Url::to(['/prisutnost/opravdaj-dijete']) . '" method="post">
                    <select onchange="this.form.submit()" name="razlog">
                        <option disabled selected>' . Yii::t('app', 'Odaberite razlog izostanka...') . '</option> 
                        <option value="osobni">' . Yii::t('app', 'Osobni razlog') . '</option>
                        <option value="zdravstveni">' . Yii::t('app', 'Zdravstveni razlog') . '</option>
                        <option value="godisnji">' . Yii::t('app', 'Godišnji odmor') . '</option>
                        <option value="blagdan">' . Yii::t('app', 'Blagdan / praznik') . '</option>
                    </select>
                    <input hidden name="id" value="' . $one->id . '">
                    <input hidden name="datum" value="' . $datum . '">
                    <input hidden name="tjedan" value="' . $tjedan . '">
                    
                </form>';
                                                                                        }
                                                                                        if ($prisutnost->razlog_izostanka != null) {
                                                                                            $dodatak = Yii::t('app', 'Razlog izostanka: ') . $prisutnost->razlog_izostanka;
                                                                                        }
                                                                                    }
                                                                                    $ikona = $prisutnost->status == 1 ? '<button class="btn btn-primary btn-sm"><span class="material-icons" >check</span></button><br><br>' : '<button class="btn btn-danger btn-sm"><span class="material-icons" >close</span></button><br>';
                                                                                    $akcija = $prisutnost->status == 1 ? 'obrisi' : 'dodaj';
                                                                                } else {
                                                                                    $ikona = '<button class="btn btn-danger btn-sm"><span class="material-icons" ">close</span></button><br>';
                                                                                    $akcija = 'dodaj';
                                                                                    $dodatak = '<div class="' . $one->id . $key . '"> <form action="' . \yii\helpers\Url::to(['/prisutnost/opravdaj-dijete']) . '" method="post">
                    <select onchange="this.form.submit()" name="razlog">
                        <option disabled selected>' . Yii::t('app', 'Odaberite razlog izostanka...') . '</option> 
                        <option value="osobni">' . Yii::t('app', 'Osobni razlog') . '</option>
                        <option value="zdravstveni">' . Yii::t('app', 'Zdravstveni razlog') . '</option>
                        <option value="godisnji">' . Yii::t('app', 'Godišnji odmor') . '</option>
                        <option value="blagdan">' . Yii::t('app', 'Blagdan / praznik') . '</option>
                    </select>
                    <input hidden name="id" value="' . $one->id . '">
                    <input hidden name="datum" value="' . $datum . '">
                    <input hidden name="tjedan" value="' . $tjedan . '">
                    
                </form>';
                                                                                }
                                                                                $url = \Yii::$app->urlManager->createUrl(['prisutnost/' . $akcija . '-dijete', 'id' => $one->id, 'datum' => $datum, 'tjedan' => $tjedan]);
                                                                                echo Html::a($ikona, $url, [
                                                                                    'title' => Yii::t('app', 'Unesi prisutnost'),
                                                                                ]);
                                                                                echo '<br>' . $dodatak;


                                                                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>