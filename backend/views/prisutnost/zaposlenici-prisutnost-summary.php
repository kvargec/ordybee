<?php if ($prisutnost->razlog_izostanka == null) { ?>
        <?= Yii::t('app','Vrijeme početka: ')?> <?= $prisutnost->pocetak . "<br>"; ?>
        <?= Yii::t('app','Vrijeme kraja: ')?> <?= $prisutnost->kraj . "<br>"; ?>
        <?php $start = new DateTime($prisutnost->pocetak);
                $kraj = new DateTime($prisutnost->kraj);
                $razlika = $kraj->diff($start);
        ?>
        <?= Yii::t('app','Ukupno: ')?> <?= $razlika->format('%hh %imin')?>
        <form id='update-prisutnost-zaposlenik-<?=$prisutnost->entitet_id."-".$prisutnost->datum?>'>
        <input hidden name='id' value='<?=$prisutnost->entitet_id?>'>
        <input hidden name='datum' value='<?= $prisutnost->datum?>'>
        <button type="submit" class="btn btn-danger btn-sm"><span class="material-icons">close</span></button>
        </form>
        <?php 
} else { 
        $razlog = '';
        switch ($prisutnost->razlog_izostanka){
                case 'godisnji':
                        $razlog = 'Godišnji';
                        break;
                case 'placeni_dopust':
                        $razlog ='Plaćeni dopust'; 
                        break;
                case 'bolovanje':
                        $razlog ='Bolovanje'; 
                        break;
                case 'slobodni_dan':
                        $razlog ='Slobodni dan';
                        break;
                case 'porodiljni':
                        $razlog ='Porodiljni'; 
                        break;
                case 'neradno':
                        $razlog ='Blagdan/Praznik'; 
                        break;
                case 'ostalo':
                        $razlog ='Ostalo';
                        break;
                }
        ?>
        <!-- <?= Yii::t('app','Izostanak')?> -->
        <br><?= Yii::t('app','Razlog izostanka: ')?><br> <?= $razlog . "<br>"; ?>
        <form id='update-prisutnost-zaposlenik-<?=$prisutnost->entitet_id."-".$prisutnost->datum?>'>
        <input hidden name='id' value='<?=$prisutnost->entitet_id?>'>
        <input hidden name='datum' value='<?= $prisutnost->datum?>'>
        <button type="submit" class="btn btn-danger btn-sm"><span class="material-icons">close</span></button>
        </form>
<?php 
} 
?>

