<?php

use yii\helpers\Html;

$pon = date('j.n.Y', strtotime('monday this week'));
$pet = date('j.n.Y', strtotime('friday this week'));
?>
<?php if (Yii::$app->session->hasFlash('error')) {
    Yii::$app->session->getFlash("error");
} ?>
<div class="card">

    <div class="card-header card-header-primary">
        <span class="card-subtitle"> <?= Yii::t('app', 'Unos prisutnosti za skupinu:') ?></span>
        <span class="card-title"><?php echo $skupina->naziv ?></span><br>





        <span class="card-subtitle"> <?= Yii::t('app', 'Period: ') ?> <?php echo date('j.n.', strtotime($datumi['pon'])) . ' - ' . date('j.n.', strtotime($datumi['pet'])) ?></span>
        <?= \yii\bootstrap\ButtonDropdown::widget([
            'encodeLabel' => false,
            'label' => Yii::t('app', 'Odabir tjedna'),
            'dropdown' => [
                'items' => [
                    ['label' => date('j.n.', strtotime('-6 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-6 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '-6'])],
                    ['label' => date('j.n.', strtotime('-5 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-5 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '-5'])],
                    ['label' => date('j.n.', strtotime('-4 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-4 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '-4'])],
                    ['label' => date('j.n.', strtotime('-3 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-3 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '-3'])],
                    ['label' => date('j.n.', strtotime('-2 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-2 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '-2'])],
                    ['label' => date('j.n.', strtotime('-1 week', strtotime($pon))) . ' - ' . date('j.n.', strtotime('-1 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '-1'])],
                    ['label' => date('j.n.', strtotime($pon)) . ' - ' . date('j.n', strtotime($pet)), 'url' => \yii\helpers\Url::to(['/prisutnost/djeca', 'id' => $skupina->id, 'tjedan' => '0'])],
                ]
            ]
        ]) ?>
        <?= Html::a(Yii::t('app', 'Unos razloga izostanka'), ['/prisutnost/razlog-izostanka', 'id' => $skupina->id, 'tjedan' => $tjedan], ['class' => 'btn btn-success']) ?>
        <span class="btn-group">
            <?= Html::a('<span class="material-icons">file_download</span> PDF', ['/prisutnost/djeca-pdf', 'skupina' => $skupina->id, 'tjedan' => $tjedan], ['class' => 'btn btn-success']) ?>
            <?= Html::a('<span class="material-icons">file_download</span> EXCEL', ['/prisutnost/djeca-excel', 'skupina' => $skupina->id, 'tjedan' => $tjedan], ['class' => 'btn btn-success']) ?>
        </span>
        <?= Html::a(Yii::t('app', 'Mjesečni izvještaj'), ['/prisutnost/izvjesce'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="card-body" id="cardDjeca">

        <table class="table table-striped table-bordered" id="djeca">
            <thead>
                <th colspan="6" class="text-center bold"><?= Yii::t('app', 'Broj djece u skupini:') ?> <?= count($djeca) ?></th>
            </thead>
            <thead>
                <th><?= Yii::t('app', 'Dijete') ?></th>
                <th style="text-align: center"><?= Yii::t('app', 'PON') ?><br> <?php echo date('j.n.Y', strtotime($datumi['pon'])) ?>&nbsp;</th>
                <th style="text-align: center"><?= Yii::t('app', 'UTO') ?><br> <?php echo date('j.n.Y', strtotime($datumi['uto'])) ?>&nbsp;</th>
                <th style="text-align: center"><?= Yii::t('app', 'SRI') ?><br> <?php echo date('j.n.Y', strtotime($datumi['sri'])) ?>&nbsp;</th>
                <th style="text-align: center"><?= Yii::t('app', 'ČET') ?><br> <?php echo date('j.n.Y', strtotime($datumi['cet'])) ?>&nbsp;</th>
                <th style="text-align: center"><?= Yii::t('app', 'PET') ?><br> <?php echo date('j.n.Y', strtotime($datumi['pet'])) ?>&nbsp;</th>
            </thead>
            <tbody>
                <?php

                foreach ($djeca as $one) { ?>
                    <tr>

                        <td><?php echo $one->ime . " " . $one->prezime;  ?></td>
                        <?php foreach ($datumi as $key => $value) {
                            $dodatak = '' ?>

                            <td style="text-align: center"><?php
                                                            $now = date('Y-m-d', time());
                                                            $datum = date('Y-m-d', strtotime($value));
                                                            if ($datum <= $now) {
                                                                $prisutnost = $one->getPrisutnost2($one->id, $datum);

                                                                if (isset($prisutnost)) {
                                                                    if ($prisutnost->status == 0) {
                                                                        if ($prisutnost->razlog_izostanka == null) {
                                                                            $dodatak = Yii::t('app', 'Razlog izostanka: -');
                                                                        }
                                                                        if ($prisutnost->razlog_izostanka != null) {
                                                                            $dodatak = Yii::t('app', 'Razlog izostanka: ') . $prisutnost->razlog_izostanka;
                                                                        }
                                                                    }
                                                                    $ikona = $prisutnost->status == 1 ? '<span id="pris' . $one->id . $datum . '" class="material-icons">check</span>' : '<span id="pris' . $one->id . $datum . '" class="material-icons">close</span><br>';
                                                                    $akcija = $prisutnost->status == 1 ? 'obrisi' : 'dodaj';
                                                                } else {
                                                                    $dodatak = Yii::t('app', 'Razlog izostanka: -');
                                                                    $ikona = '<span id="pris' . $one->id . $datum . '" class="material-icons">close</span><br>';
                                                                    $akcija = 'dodaj';
                                                                }
                                                            } else {
                                                                $dodatak = Yii::t('app', 'Razlog izostanka: -');
                                                                $ikona = '<span id="pris' . $one->id . $datum . '" class="material-icons">close</span><br>';
                                                                $akcija = 'dodaj';
                                                            }


                                                            $forma = '';
                                                            $forma .= Html::beginForm(['prisutnost/' . $akcija . '-dijete', 'id' => $one->id, 'datum' => $datum], 'post', ['class' => 'unesi-prisutnost', 'id' => 'form' . $one->id . $datum]);
                                                            if (strpos($ikona, 'close') !== false) {
                                                                $forma .= Html::submitButton($ikona, array('class' => 'btn btn-danger btn-sm mini mini'));
                                                            } else {
                                                                $forma .= Html::submitButton($ikona, array('class' => 'btn btn-primary btn-sm mini mini'));
                                                            }
                                                            $forma .= Html::endForm();
                                                            //        $url=\Yii::$app->urlManager->createUrl(['prisutnost/'.$akcija.'-dijete', 'id' => $one->id, 'datum'=>$datum]);
                                                            //            echo Html::a($ikona, $url, [
                                                            //                'title' => Yii::t('app', 'Unesi prisutnost'),]);
                                                            //            echo '<br>'.$dodatak;
                                                            echo '<div>' . $forma . '</div>';
                                                            echo '<br><div id="dodatak' . $one->id . $datum . '">' . $dodatak . '</div>';

                                                            ?>
                                <style>
                                    table .btn.btn-primary {
                                        background: linear-gradient(60deg, #cadbe6, #9dbacc);
                                        color: #003d4e !important;
                                    }

                                    table .btn.btn-primary>span.material-icons {
                                        font-weight: 900 !important;
                                    }
                                </style>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>


</div>
</div>
<?php

$js = <<< JS
$(".unesi-prisutnost").on("submit", function(e) {
e.preventDefault();
var url = $(this).attr('action');
var data = $(this).serializeArray();
$.ajax({
url: url,
type: 'post',
dataType: 'json',
data: data
}).done(function(data, textStatus, jqXHR) {
    console.log( $('#pris'+data.id).html());
    if ($('#pris'+data.id).html() === 'close') {
       
        $('#pris'+data.id).html('check');
         $('#pris'+data.id).closest('button').removeClass().addClass('btn btn-primary btn-sm mini');
    } else {
        $('#pris'+data.id).html('close');
         $('#pris'+data.id).closest('button').removeClass().addClass('btn btn-danger btn-sm mini');
        
    }
    if ($('#form'+data.id).attr('action').includes('dodaj')) {
        formNewAction = $('#form'+data.id).attr('action').replace('dodaj', 'obrisi');
    } else {
        formNewAction = $('#form'+data.id).attr('action').replace('obrisi', 'dodaj');
    }
    $('#form'+data.id).attr('action', formNewAction);
    $('#dodatak'+data.id).html(data.dodatak);
    //////////////////////////////////////////////// mobile inputs and form
    if ($('#mobpris'+data.id).html() === 'close') {
       
        $('#mobpris'+data.id).html('check');
         $('#mobpris'+data.id).closest('button').removeClass().addClass('btn btn-primary btn-sm mini');
    } else {
        $('#mobpris'+data.id).html('close');
         $('#mobpris'+data.id).closest('button').removeClass().addClass('btn btn-danger btn-sm mini');
        
    }
    if ($('#mobform'+data.id).attr('action').includes('dodaj')) {
        formNewAction = $('#mobform'+data.id).attr('action').replace('dodaj', 'obrisi');
    } else {
        formNewAction = $('#mobform'+data.id).attr('action').replace('obrisi', 'dodaj');
    }
    $('#mobform'+data.id).attr('action', formNewAction);
    $('#mobdodatak'+data.id).html(data.dodatak);
    //$.notify(data.message, "success");
}).fail(function(jqXHR, textStatus, errorThrown) {
alert( errorThrown );
});
});
JS;

$this->registerJs($js);


?>
</div>