<?php

use kartik\widgets\TimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
?>
<style>
    .pt-20 {
        padding-top: 20px;
        padding-bottom: 15px;
    }
</style>
<form action="" method="post" class="edit-zaposlenici" id="prisutnost-zaposlenika<?= $prisutnost->entitet_id . '-' . $prisutnost->datum ?>">
    <div class="form-group">
    <?php
        echo Html::textInput('prisutnost_type', 'simple', ['class' => 'form-control hidden']);

        echo Html::dropDownList(
            'razlog',
            'rad_8h',
            ['rad_8h'=>'Rad 8h',
            'rad_4h'=>'Rad 4h',
            'godisnji'=>'Godišnji', 
            'placeni_dopust'=>'Plaćeni dopust', 
            'bolovanje'=>'Bolovanje', 
            'slobodni_dan'=>'Slobodni dan',
            'porodiljni'=>'Porodiljni', 
            'neradno'=>'Blagdan/Praznik', 
            'ostalo'=>'Ostalo']
        )
    ?>
    </div>
    <input hidden name="id" value="<?php echo $prisutnost->entitet_id ?>">
    <input hidden name="datum" value="<?php echo $prisutnost->datum ?> ">
    <button type="submit" class="btn btn-primary  btn-sm"><span class="material-icons">check</span></button>
</form>