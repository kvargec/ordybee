<?php

use kartik\widgets\TimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
?>
<style>
    .pt-20 {
        padding-top: 20px;
        padding-bottom: 15px;
    }
</style>
<form action="" method="post" class="edit-zaposlenici" id="prisutnost-zaposlenika<?= $prisutnost->entitet_id . '-' . $prisutnost->datum ?>">
    <!-- <label class="control-label"><?= Yii::t('app', 'Vrijeme početka') ?></label><br> -->
    <div class="d-flex editform" style="font-size:14px">

        <span class="material-icons">login</span>

        <?php echo TimePicker::widget([
            'name' => 'begin_time',
            'size' => 'sm',
            'value' => $prisutnost->pocetak,
            // 'addon'=>'<span class="material-icons">logout</span>',
            'pluginOptions' => [
                'showSeconds' => false,
                'showMeridian' => false,
                'minuteStep' => 5,
                'template' => false,
                'timeFormat' => 'hh:mm',
            ]
        ]); ?>
        <!-- <label class="control-label"><?= Yii::t('app', 'Vrijeme odlaska') ?></label><br> -->
        <span class="material-icons">logout</span>
        <?php echo TimePicker::widget([
            'name' => 'end_time',
            'size' => 'sm',
            'value' => $prisutnost->kraj,
            // 'addon'=>'<span class="material-icons">logout</span>',
            'pluginOptions' => [
                'showSeconds' => false,
                'showMeridian' => false,
                'minuteStep' => 5,
                'template' => false,
                'timeFormat' => 'hh:mm',
            ]
        ]); ?>
    </div>
    <div class="form-group">

        <?php
        echo Html::textInput('prisutnost_type', 'advanced', ['class' => 'form-control hidden']);

        echo Html::dropDownList(
            'razlog',
            'rad',
            [
                'rad' => 'Rad',
                'godisnji' => 'Godišnji',
                'placeni_dopust' => 'Plaćeni dopust',
                'bolovanje' => 'Bolovanje',
                'slobodni_dan' => 'Slobodni dan',
                'neradno' => 'Blagdan/Praznik',
                'ostalo' => 'Ostalo'
            ]
        )
        ?>

    </div>
    <input hidden name="id" value="<?php echo $prisutnost->entitet_id ?>">
    <input hidden name="datum" value="<?php echo $prisutnost->datum ?> ">
    <button type="submit" class="btn btn-primary  btn-sm"><span class="material-icons">check</span></button>
</form>