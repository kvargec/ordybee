<?php

use kartik\widgets\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;

$aktivnaGodina = \common\models\PedagoskaGodina::find()->where(['aktivna' => 1])->one();
$skupine = ArrayHelper::map(\common\models\Skupina::find()->where(['ped_godina' => $aktivnaGodina->id])->all(), 'id', 'naziv');
$zaSve[0] = Yii::t('app', 'Sve skupine');
$skupine = $zaSve + $skupine;
$mjeseci = \common\helpers\Utils::mjeseciPedGod();
$this->title = Yii::t('app', 'Izvještaji');

?>
<div class="card">
    <div class="card-header card-header-primary izvjestaji">
        <span class="card-title"><?= Yii::t('app',  'Izvještaji') ?></span>
        <!-- <span class="btn-group">
            <?= Html::a('<span class="material-icons">file_download</span> PDF', ['/prisutnost/zaposlenici-pdf'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('<span class="material-icons">file_download</span> EXCEL', ['/prisutnost/zaposlenici-excel'], ['class' => 'btn btn-success']) ?>
        </span> -->

    </div>


    <div class="card-body ">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">face</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Prisutnost djece') ?> </h4>
                        <h4 class="title">&nbsp;</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('prisutnost/djeca-pdf-mjesecni')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">

                            <?= Select2::widget([
                                'name' => 'skupine',
                                'data' => $skupine,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite skupinu'),
                                    'multiple' => false,
                                    'class' => 'form-control',
                                    'required' => true

                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>

                            <?= Select2::widget([
                                'name' => 'mjesec',
                                'data' => $mjeseci,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                                    'multiple' => false,
                                    'class' => 'form-control',
                                    'required' => true
                                ],

                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>

                        </div>

                        <p class="mt-15 mb-0"><?= Yii::t('app', 'Izaberite vrstu:') ?> </p>
                        <div class="row">
                            <div class="col-lg-8">
                                <input type="radio" id="simple" name="view_type" value="simple" checked>
                                <label for="simple"><?= Yii::t('app', 'Ukupan broj prisutnih dana za mjesec') ?> </label>
                            </div>

                            <div class="col-lg-4 text-right">
                                <input type="radio" id="advanced" name="view_type" value="advanced">
                                <label for="advanced"><?= Yii::t('app', 'Prisutnih po danu') ?> </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> PDF</button>
                        <!-- <?= Html::a('<span class="material-icons">file_download</span> PDF', ['/prisutnost/zaposlenici-pdf-mjesecni'], ['class' => 'btn btn-success']) ?> -->
                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="blue">
                        <i class="material-icons">people</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Prisutnost zaposlenika') ?> </h4>
                        <h4 class="title">&nbsp;</h4>

                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('prisutnost/zaposlenici-pdf-mjesecni')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">
                            <?= Select2::widget([
                                'name' => 'mjesec',
                                'data' => $mjeseci,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                                    'multiple' => false,
                                    'required' => true,
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>
                        </div>
                        <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> PDF</button>
                        <?php ActiveForm::end(); ?>
                    </div>



                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">sick</i>
                    </div>
                    <div class="card-content">
                        <h4 class="title"><?= Yii::t('app', 'Izostanci djece') ?> </h4>
                        <h4 class="title">&nbsp;</h4>
                        <?php $form = ActiveForm::begin(['action' => Url::toRoute('dnevnik-zapis/izostanci-djece')]); ?>
                        <div class="d-flex justify-content-end izvjestaji">



                            <?= Select2::widget([
                                'name' => 'mjesec',
                                'data' => $mjeseci,
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                                    'multiple' => false,
                                    'class' => 'form-control',
                                    'required' => true
                                ],

                                'pluginOptions' => [
                                    'allowClear' => true,

                                ],
                            ]) ?>

                        </div>


                        <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> PDF</button>
                        <!-- <?= Html::a('<span class="material-icons">file_download</span> PDF', ['/prisutnost/zaposlenici-pdf-mjesecni'], ['class' => 'btn btn-success']) ?> -->
                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>