<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Documents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-index">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>

    </div>
<div class="card-body">

  

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'value' => 'naziv',
                'label' => Yii::t('app', 'Naziv'),
                'attribute' => 'naziv',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Naziv'),
                    'class' => 'form-control'
                ]
            ],
            [
                'value' => 'filename',
                'label' => Yii::t('app', 'Ime dokumenta'),
                'attribute' => 'filename',
                'filterOptions' => [
                    'class' => 'form-group'
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Ime dokumenta'),
                    'class' => 'form-control'
                ]
            ],

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} ',
                // 'visible' => Yii::$app->user->can('admin'),
                'buttons' => [
                    'view' => function ($url, $data) {

                        $url2 = \Yii::$app->urlManager->createUrl(['document/view', 'id' => $data->id]);
                        return Html::a('<span class="material-icons">visibility</span>', $url2, [
                            'title' => Yii::t('app', 'Pregledaj zapise'),
                            'class' => 'text-left  hgreen',
                        ]);
                    }, 
                    'update' => function ($url, $data){
                        $url2 = \Yii::$app->urlManager->createUrl(['document/update', 'id' => $data->id]);
                        return Html::a('<span class="material-icons">create</span>', $url2, [
                            'title' => Yii::t('app', 'Ažuriraj'),
                        ]);
                    },
                    'delete' => function ($url, $data){
                      
                        $url2 = \Yii::$app->urlManager->createUrl(['document/delete', 'id' => $data->id]);
                        return Html::a('<span class="material-icons red">delete</span>', $url2, [
                            'title' => Yii::t('user', 'Obriši'),
                            'data-confirm' => Yii::t('user', 'Are you sure you want to delete this document?'),
                            'data-method' => 'POST',
                        ]);
                        
                    }, 



                ],
        ],
        ],
    ]); ?>


</div></div>
</div>

