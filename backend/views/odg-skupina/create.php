<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OdgSkupina */

$this->title = Yii::t('app', 'Create Odg Skupina');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Odg Skupinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="odg-skupina-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
