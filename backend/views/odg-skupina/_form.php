<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\OdgSkupina */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="odg-skupina-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'odgajatelj')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Zaposlenik::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'skupina')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Skupina::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'date_from')->textInput() ?>

    <?= $form->field($model, 'date_to')->textInput() ?>

    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\StatusOpce::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
