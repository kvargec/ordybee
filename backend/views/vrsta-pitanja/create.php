<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VrstaPitanja */

$this->title = Yii::t('app', 'Create Vrsta Pitanja');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vrsta Pitanjas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrsta-pitanja-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
