<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VrstaDnevnik */

$this->title = Yii::t('app', 'Update Vrsta Dnevnik: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vrsta Dnevniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="vrsta-dnevnik-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
