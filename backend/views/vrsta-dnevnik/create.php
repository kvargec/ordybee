<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VrstaDnevnik */

$this->title = Yii::t('app', 'Create Vrsta Dnevnik');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vrsta Dnevniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrsta-dnevnik-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
