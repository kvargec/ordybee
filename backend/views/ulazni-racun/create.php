<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UlazniRacun */

$this->title = Yii::t('app', 'Create Ulazni Racun');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ulazni Racuns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ulazni-racun-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
