<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\UlazniRacunSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ulazni Racuns');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ulazni-racun-index">
<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
   


        <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
</div>
<div class="card-body">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'dobavljac',
            'naslov',
            'napomena:ntext',
            'datum_racuna',
            //'created_at',
            //'preuzeo',
            //'iznos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div></div></div>
