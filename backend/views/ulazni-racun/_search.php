<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Search\UlazniRacunSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ulazni-racun-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'dobavljac') ?>

    <?= $form->field($model, 'naslov') ?>

    <?= $form->field($model, 'napomena') ?>

    <?= $form->field($model, 'datum_racuna') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'preuzeo') ?>

    <?php // echo $form->field($model, 'iznos') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
