<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\UlazniRacun */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ulazni-racun-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dobavljac')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Tvrtka::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'naslov')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'napomena')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'datum_racuna')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'preuzeo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iznos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
