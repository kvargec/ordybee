<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\Kontakt */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$this->registerJs(
   '$("document").ready(function(){ 
		$("#new_kontakt").on("pjax:end", function() {
			$.pjax.reload({container:"#kontakts"});  //Reload GridView
		});
    });',
    \yii\web\View::POS_READY
);
?>

<div class="kontakt-form">

<?php Pjax::begin(['id' => 'new_kontakt']); ?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>
<div class="container">
<div class="row">
    <div class="col-sm-3">
        <?php
        $vrstaArray = ArrayHelper::map(
            \common\models\VrsteKontakt::find()->all(),
            'id',
            'vrsta'
        );
        echo $form->field($model, "vrsta")->dropDownList($vrstaArray, ['prompt' => Yii::t('app','---- Odaberi vrstu kontakta ----')]) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, "vrijednost") ?>
    </div>
    <div class="col-sm-3 pull-right">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
</div>
    </div>
<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>

</div>