<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use common\helpers\Utils;
/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZaposlenikSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zaposleniks');
$this->params['breadcrumbs'][] = $this->title;

$start_date = new \DateTime(date('Y-m-d', strtotime('first day of this month', time())));
$end_date = new \DateTime(date('Y-m-d', strtotime('last day of this month', time())));
$interval = \DateInterval::createFromDateString('1 day');
$period = new \DatePeriod($start_date, $interval, $end_date);

?>
<div class="zaposlenik-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>


            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Unesi prisutnost'), ['/prisutnost/zaposlenici'], ['class' => 'btn btn-info']) ?>
        </div>

        <?php // echo $this->render('_search', ['model' => $searchModel]); 
        ?>
        <div class="card-body">
            <?php $gridColumns = [
                // 'titula',
                [
                    'value' => 'ime',
                    'label' => Yii::t('app', 'Ime'),
                    'attribute' => 'ime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'prezime',
                    'label' => Yii::t('app', 'Prezime'),
                    'attribute' => 'prezime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Prezime'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'oib',
                    'label' => Yii::t('app', 'OIB'),
                    'attribute' => 'oib',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'OIB'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'format' => 'html',
                    'value' => function ($data) {
                        $return  = '';
                        foreach ($data->kontakts as $item) {
                            $return .= $item->kontakt0->vrsta0->vrsta . ': ' . $item->kontakt0->vrijednost . '<br>';
                        }
                        return $return;
                    },
                    'label' => Yii::t('app', 'Kontakti'),
                    'attribute' => 'kontakt',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Kontakti'),
                        'title' => Yii::t('app', 'Kontakti'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'label' => Yii::t('app', 'Prisutnost zaposlenici'),
                    'format' => 'html',
                    'value' => function ($data) use ($period) {
                        $izostao = Utils::calculateDays($period, 'zaposlenik', $data->id, 0);
                        if (empty($izostao)) {
                            $temp = Yii::t('app', 'Izostao dana: ') . '0<br />';
                        } else {
                            if (is_array($izostao)) {
                                $temp = Yii::t('app', 'Izostao dana: ') . $izostao[0]->ukupno . '<br />';
                            } else {
                                $temp = Yii::t('app', 'Izostao dana: ') . $izostao->ukupno . '<br />';
                            }
                        }
                        return $temp;
                    }

                ],





                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['zaposlenik/view', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pogledaj'),
                            ]);
                        },

                        'update' => function ($url, $data) {
                            $url2 = \Yii::$app->urlManager->createUrl(['zaposlenik/update', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">create</span>', $url2, [
                                'title' => Yii::t('app', 'Ažuriraj'),
                            ]);
                        },


                    ],
                ],
            ];


            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_PDF => null,
                    ExportMenu::FORMAT_HTML => null,

                ],
                'filename' => 'popisZaposlenika',
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser,
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/zaposlenik/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" . GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'columns' => $gridColumns,

            ]); ?>





        </div>
    </div>
</div>