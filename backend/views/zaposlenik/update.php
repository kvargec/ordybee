<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Zaposlenik */

$this->title = Yii::t('app', 'Update Zaposlenik: {name}', [
    'name' => $model->prezime,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zaposleniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="zaposlenik-update">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        
    </div>
    <div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
