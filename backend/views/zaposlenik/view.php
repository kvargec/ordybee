<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\FileInput;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Zaposlenik */

$this->title = $model->ime . ' ' . $model->prezime;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zaposleniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="zaposlenik-view">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('<span class="material-icons">print</span>', ['zaposlenik/pdf-print', 'id' => $model->id], ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
        </div>
        <div class="card-body">


            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'ime',
                    'prezime',
                    'adresa',
                    'mjesto0.naziv',
                    [
                        'label' => 'OIB',
                        'attribute' => 'oib',
                        
                    ],
                    // 'dat_rodjenja',
                    [
                        'label' => 'Datum rođenja',
                        'attribute' => 'dat_rodjenja',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDateTime($model->dat_rodjenja, 'php:d.m.Y');
                        },
                    ],
                    [
                        'label' => 'Sprema',
                        'attribute' => 'sprema0.naziv',
                        
                    ],
                    [
                        'label' => 'Spol',
                        'attribute' => 'spol',
                        'value' => function ($model) {
                            if ($model->spol == 1) {
                                return 'Ž';
                            } elseif ($model->spol == 0) {
                                return 'M';
                            } else {
                                return 'Nije definirano';
                            }
                        },                   
                    ],
                    // 'created_at',
                    // 'updated_at',
                ],
            ]) ?>

            <h3 class="px-5">Kontakti zaposlenika</h3>

            <?php Pjax::begin(['id' => 'kontakts']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    // ['class' => 'yii\grid\SerialColumn'],

                    'kontakt0.vrsta0.vrsta',
                    'kontakt0.vrijednost',

                    // ['class' => 'yii\grid\ActionColumn',
                    // ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'zaposlenik-kontakt',
                        'template' => '{view}{update}',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data) {

                                $url2 = \Yii::$app->urlManager->createUrl(['zaposlenik-kontakt/view', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pogledaj'),
                                ]);
                            },

                            'update' => function ($url, $data) {
                                $url2 = \Yii::$app->urlManager->createUrl(['zaposlenik-kontakt/update', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">create</span>', $url2, [
                                    'title' => Yii::t('app', 'Ažuriraj'),
                                ]);
                            },


                        ],
                    ],
                ],
            ]); ?>

            <?php Pjax::end() ?>

            <h3 class="px-5">Dodavanje kontakta</h3>

            <!-- Render create form -->
            <?= $this->render('createKontakt', [
                'model' => $kontakt,
                'zaposlenikKontakt' => $zaposlenikKontakt,
            ])  ?>

            <h3 class="px-5"><?= Yii::t('app', 'Files') ?></h3>
            <div class="container">
            <div class="row pb-20">
                <div class="col-lg-12">
                    <?php 
                    if(!empty($model->attachments) && is_array($model->attachments)){
                        $i=0;
                        foreach ($model->attachments as $dokument) { 
                            if($i>count($dokument)){
                                break;
                            }
                            foreach ($dokument as $file) { ?>
                                <div class="col-lg-3 col-xs-6 text-center ">
                                    <a href="<?php echo Yii::getAlias('@web') . '/zaposlenici/' . $model->id . '/' . $file; ?>" target="_blank">
                                        <?php
                                        $ekst = explode(".", $file);
                                        switch ($ekst[count($ekst) - 1]) {
                                            case 'docx':
                                            case 'rtf':
                                            case 'doc':
                                                $icon = '<i class="fa fa-file-word-o" style="font-size:40px"></i>';
                                                break;
                                            case 'xls':
                                            case 'xlsx':
                                                $icon = '<i class="fa fa-file-excel-o" style="font-size:40px"></i>';
                                                break;
                                            case 'pdf':
                                                $icon = '<i class="fa fa-file-pdf-o" style="font-size:40px"></i>';
                                                break;
                                            default:
                                                $icon = '<i class="fa fa-file" style="font-size:40px"></i>';
                                                break;
                                        } ?>
                                        <?php echo $icon; ?>
                                        <p class="text-center"><?php echo $file; ?></p>
                                    </a>
                                </div>

                    <?php
                            }
                        }
                        $i++;
                    } else {
                        echo Yii::t('app', 'No files') . '&nbsp;&nbsp;' . Html::a('<span class="material-icons">add</span>', ['update', 'id' => $model->id, '#' => 'zaposlenik-file'], ['class' => 'btn btn-primary']);
                    } ?>
                </div>

            </div>
            </div>



        </div>
    </div>
</div>