<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZaposlenikSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Zaposleniks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zaposlenik-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>


            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Unesi prisutnost'), ['/prisutnost/zaposlenici'], ['class' => 'btn btn-info']) ?>


        </div>

        <?php // echo $this->render('_search', ['model' => $searchModel]); 
        ?>
        <div class="card-body">
            <?php $gridColumns = [
                // ['class' => 'yii\grid\SerialColumn'],

                [
                    'value' => 'ime',
                    'label' => Yii::t('app', 'Ime'),
                    'attribute' => 'ime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Ime'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'prezime',
                    'label' => Yii::t('app', 'Prezime'),
                    'attribute' => 'prezime',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Prezime'),
                        'class' => 'form-control'
                    ]
                ],

                [
                    'value' => 'adresa',
                    'label' => Yii::t('app', 'Adresa'),
                    'attribute' => 'adresa',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Adresa'),
                        'class' => 'form-control'
                    ]
                ],
                [
                    'value' => 'mjesto0.naziv',
                    'label' => Yii::t('app', 'Mjesto'),
                    'attribute' => 'mjesto',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Mjesto'),
                        'title' => Yii::t('app', 'Mjesto'),
                        'class' => 'form-control'
                    ]
                ],
                // 'titula',
                [
                    'format' => 'html',
                    'value' => function ($data) {
                        $return  = '';
                        if (!empty($data->kontakts)) {
                            foreach ($data->kontakts as $item) {
                                if (!empty($item->kontakt0) && !empty($item->kontakt0->vrsta0)) {
                                    $return .= $item->kontakt0->vrsta0->vrsta . ': ' . $item->kontakt0->vrijednost . '<br>';
                                }
                                return $return;
                            }
                        }
                    },
                    'label' => Yii::t('app', 'Kontakti'),
                    'attribute' => 'kontakt',
                    'filterOptions' => [
                        'class' => 'form-group'
                    ],
                    'filterInputOptions' => [
                        'placeholder' => Yii::t('app', 'Kontakti'),
                        'title' => Yii::t('app', 'Kontakti'),
                        'class' => 'form-control'
                    ]
                ],

                //'oib',
                //'dat_rodjenja',
                //'sprema',
                //'spol',
                //'created_at',
                //'updated_at',


                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                    // 'visible' => Yii::$app->user->can('admin'),
                    'buttons' => [
                        'view' => function ($url, $data) {

                            $url2 = \Yii::$app->urlManager->createUrl(['zaposlenik/view', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                'title' => Yii::t('app', 'Pogledaj'),
                            ]);
                        },

                        'update' => function ($url, $data) {
                            $url2 = \Yii::$app->urlManager->createUrl(['zaposlenik/update', 'id' => $data->id]);
                            return Html::a('<span class="material-icons">create</span>', $url2, [
                                'title' => Yii::t('app', 'Ažuriraj'),
                            ]);
                        },


                    ],
                ],
            ];


            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => null,
                    ExportMenu::FORMAT_HTML => null,
                ],
                'filename' => 'popisZaposlenika',
                'stream' => false,
                'folder' => '@app/web/',
                //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser,
                //'target' => '_blank',
                'linkPath' => Url::toRoute('@web/web/'),
                'afterSaveView' => '/zaposlenik/zalink',
                'dropdownOptions' => [
                    'label' => Yii::t('app', 'Export All'),
                    'class' => 'btn btn-outline-secondary'
                ]
            ]) . "<hr>\n" . GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'columns' => $gridColumns,

            ]); ?>





        </div>
    </div>
</div>