<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Zaposlenik */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zaposlenik-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'] // important
    ]); ?>
    <div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'ime')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'prezime')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'adresa')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'mjesto')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(
                \common\models\Mjesto::find()->all(),
                'id',
                'naziv'
            ),
            // 'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-6">
    <?= $form->field($model, 'oib')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
    <?= $form->field($model, 'dat_rodjenja')->widget(DatePicker::classname(), [
        // 'convertFormat' => true,
        // 'options' => ['placeholder' => 'Enter birth date ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy'
        ]
    ]) ?>
    </div>
    </div>
    <div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'spol')->widget(Select2::classname(), [
            'data' => ['M', 'Ž'],
            'options' => ['placeholder' => 'Odaberite spol'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'sprema')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(
                \common\models\Sprema::find()->all(),
                'id',
                'naziv'
            ),
            // 'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>
    
    <div class="col-lg-4">
        <?= $form->field($model, 'user')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'),
            'options' => ['placeholder' => 'Odaberite korisnika'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    </div>
    <div id="zaposlenik-file">
        <?= $form->field($model, 'attachments[]')->widget(FileInput::classname(), [
            'language' => 'hr',
            'options' => [
                'accept' => '*/*',
                'id' => 'input',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'initialPreview' => $model->isNewRecord ? [] : $model->getFilePathsBackend($model->id),
                'initialPreviewAsData' => true,
                'initialPreviewShowDelete' => true,
                'deleteUrl' => \yii\helpers\Url::to(['/zaposlenik/delete-sgl-attachment', 'id' => $model->id, 'title' => '{caption}']),
                'showUpload' => false,
                'showRemove' => false,
                /*
           'initialPreviewConfig' => [
                'key' => '{caption}',
                'url' => \yii\helpers\Url::to(['/zaposlenik/delete-sgl-attachments', 'id' => $model->id, 'title' => 'key'])
            ],*/

                'language' => substr(\Yii::$app->language, 0, 2),
                'browseLabel' => Yii::t('app', 'Odabir'),
                'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteke ovdje'),
                'placeholder' => Yii::t('app', 'Odabir')
            ]
        ])->label(Yii::t('app', "Prilozi")) ?>
        <!-- <?= Html::a('<span class="material-icons">delete</span>', ['zaposlenik/delete-attachments', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm', 'target' => '_self']) ?> -->
    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>