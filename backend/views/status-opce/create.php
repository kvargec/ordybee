<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StatusOpce */

$this->title = Yii::t('app', 'Create Status Opce');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Status Opces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-opce-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
