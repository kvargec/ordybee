<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\PrijevozZaposlenikSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Prijevoz Zaposleniks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prijevoz-zaposlenik-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Prijevoz Zaposlenik'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'prijevoz_id',
            'zaposlenik_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
