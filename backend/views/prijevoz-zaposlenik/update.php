<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PrijevozZaposlenik */

$this->title = Yii::t('app', 'Update Prijevoz Zaposlenik: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Prijevoz Zaposleniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="prijevoz-zaposlenik-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
