<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\PrijevozZaposlenik */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prijevoz-zaposlenik-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'prijevoz_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Prijevoz::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'zaposlenik_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Zaposlenik::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
