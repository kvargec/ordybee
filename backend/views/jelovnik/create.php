<?php

use kartik\widgets\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Calendar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calendar-form jelovnik-create">
<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Yii::t('app','Izrada jelovnika')?></span>
        </div>
        <div class="card-body">
    <?php $form = ActiveForm::begin(['id'=>'jelovnik-form']); ?>

    <table class="table table-striped table-bordered">
        <thead>
        <th></th>
        <th><?= Yii::t('app','Ponedjeljak') ?><br><?php echo date('j.n.Y',strtotime($datum['pon']));?></th>
        <th><?= Yii::t('app','Utorak') ?><br><?php echo date('j.n.Y',strtotime($datum['uto']));?></th>
        <th><?= Yii::t('app','Srijeda') ?><br><?php echo date('j.n.Y',strtotime($datum['sri']));?></th>
        <th><?= Yii::t('app','Četvrtak') ?><br><?php echo date('j.n.Y',strtotime($datum['cet']));?></th>
        <th><?= Yii::t('app','Petak') ?><br><?php echo date('j.n.Y',strtotime($datum['pet']));?></th>
        </thead>
        <tr>
            <td><?= Yii::t('app','Doručak') ?></td>
            <?php foreach ($dorucak as $key=>$value){?>
            <td> <?= $form->field($dorucak, '[dorucak]'.$key)->widget(Select2::classname(), [
                    'data' =>ArrayHelper::map(\common\models\Jelo::find()->all(), 'naziv', function($data){
                        return $data->naziv;
                    }),
                    'options' => ['placeholder' => Yii::t('app','Odaberite jelo ...'), 'multiple' => true],
                    'maintainOrder' => true,
                    'pluginOptions' => [
                        'tags'=>true,
                        'tokenSeparators' => [','],
                        'maximumInputLength' => 100
                    ],
                ])->label(false); ?></td>
            <?php }?>

        </tr>
        <tr>
            <td><?= Yii::t('app','Ručak') ?></td>
            <?php foreach ($rucak as $key=>$value){?>
                <td> <?= $form->field($rucak, '[rucak]'.$key)->widget(Select2::classname(), [
                        'data' =>ArrayHelper::map(\common\models\Jelo::find()->all(), 'naziv', function($data){
                            return $data->naziv;
                        }),
                        'options' => ['placeholder' => Yii::t('app', 'Odaberite jelo ...'), 'multiple' => true],
                        'maintainOrder' => true,
                        'pluginOptions' => [
                            'tags'=>true,
                            'tokenSeparators' => [','],
                            'maximumInputLength' => 100
                        ],
                    ])->label(false); ?></td>
            <?php }?>

        </tr>
        <tr>
            <td><?= Yii::t('app','Užina') ?></td>
            <?php foreach ($uzina as $key=>$value){?>
                <td> <?= $form->field($uzina, '[uzina]'.$key)->widget(Select2::classname(), [
                        'data' =>ArrayHelper::map(\common\models\Jelo::find()->all(), 'naziv', function($data){
                            return $data->naziv;
                        }),
                        'options' => ['placeholder' => Yii::t('app','Odaberite jelo ...'), 'multiple' => true],
                        'maintainOrder' => true,
                        'pluginOptions' => [
                            'tags'=>true,
                            'tokenSeparators' => [','],
                            'maximumInputLength' => 100
                        ],
                    ])->label(false); ?></td>
            <?php }?>

        </tr>



    </table>


            <?php echo $form->field($dodatak, 'dodatak')->widget(FileInput::classname(), [
                'options' => ['accept' => '*'], 'pluginOptions' => ['showCaption' => false,
                    'initialPreview' => [

                        Yii::getAlias('@frontendUrl')."/admin/media/". $dodatak->dodatak
                    ],
                    'initialPreviewAsData' => true,
                    'overwriteInitial' => true,
                    'showRemove' => false,
                    'showUpload' => false,
                    'browseLabel' => Yii::t('app', 'Odabir'),
            'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteku ovdje'),
            'placeholder' => Yii::t('app', 'Odabir'),
            'msgPlaceholder' => Yii::t('app', 'Odabir'),

                    'browseClass' => 'btn btn-primary btn-block'],
            ])->label(Yii::t('app','Naslovna slika')); ?>





    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','<span class="material-icons">done</span>'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div></div></div>
