<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Calendar */
/* @var $form yii\widgets\ActiveForm */


$pon = date('j.n.Y', strtotime('monday this week'));
$pet = date('j.n.Y', strtotime('friday this week'));
?>

<div class="calendar-form jelovnik-view">
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Yii::t('app', 'Jelovnik za:') ?> <?= date('j.n.Y', strtotime($datum['pon'])) ?> - <?= date('j.n.Y', strtotime($datum['pet'])) ?></span>
            <br>


            <?= \yii\bootstrap\ButtonDropdown::widget([
                'encodeLabel' => false,
                'label' => Yii::t('app', '<span class="material-icons">event</span>'),
                'dropdown' => [
                    'items' => [
                        ['label' => date('j.n.', strtotime('-1 week', strtotime($pon))) . ' - ' . date('j.n', strtotime('-1 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/jelovnik/view', 'tjedan' => '-1'])],
                        ['label' => date('j.n.', strtotime($pon)) . ' - ' . date('j.n', strtotime($pet)), 'url' => \yii\helpers\Url::to(['/jelovnik/view', 'tjedan' => '0'])],
                        ['label' => date('j.n.', strtotime('+1 week', strtotime($pon))) . ' - ' . date('j.n', strtotime('+1 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/jelovnik/view', 'tjedan' => '+1'])],
                        ['label' => date('j.n.', strtotime('+2 week', strtotime($pon))) . ' - ' . date('j.n', strtotime('+2 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/jelovnik/view', 'tjedan' => '+2'])],
                        ['label' => date('j.n.', strtotime('+3 week', strtotime($pon))) . ' - ' . date('j.n', strtotime('+3 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/jelovnik/view', 'tjedan' => '+3'])],
                        ['label' => date('j.n.', strtotime('+4 week', strtotime($pon))) . ' - ' . date('j.n', strtotime('+4 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/jelovnik/view', 'tjedan' => '+4'])],
                        ['label' => date('j.n.', strtotime('+5 week', strtotime($pon))) . ' - ' . date('j.n', strtotime('+5 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/jelovnik/view', 'tjedan' => '+5'])],
                        ['label' => date('j.n.', strtotime('+6 week', strtotime($pon))) . ' - ' . date('j.n', strtotime('+6 week', strtotime($pet))), 'url' => \yii\helpers\Url::to(['/jelovnik/view', 'tjedan' => '+6'])],
                    ]
                ]
            ]) ?>
            <?= Html::a(Yii::t('app', '<span class="material-icons">add</span>'), ['create', 'tjedan' => $tjedan], ['class' => 'btn btn-success']) ?>

            <?= Html::a(Yii::t('app', '<span class="material-icons">edit</span>'), ['update', 'tjedan' => $tjedan], ['class' => 'btn btn-success']) ?>

            <?= Html::a(Yii::t('app', '<span class="material-icons">download</span>'), ['pdf', 'tjedan' => $tjedan], ['class' => 'btn btn-success']) ?>

        </div>
        <div class="card-body">



            <table class="table table-striped table-bordered text-center desktop">
                <thead>
                    <th></th>
                    <th><?= Yii::t('app', 'Ponedjeljak') ?><br><?php echo date('j.n.Y', strtotime($datum['pon'])); ?></th>
                    <th><?= Yii::t('app', 'Utorak') ?><br><?php echo date('j.n.Y', strtotime($datum['uto'])); ?></th>
                    <th><?= Yii::t('app', 'Srijeda') ?><br><?php echo date('j.n.Y', strtotime($datum['sri'])); ?></th>
                    <th><?= Yii::t('app', 'Četvrtak') ?><br><?php echo date('j.n.Y', strtotime($datum['cet'])); ?></th>
                    <th><?= Yii::t('app', 'Petak') ?><br><?php echo date('j.n.Y', strtotime($datum['pet'])); ?></th>
                </thead>
                <tr>
                    <td><?= Yii::t('app', 'Doručak') ?></td>
                    <td> <?= $model->dorucak->pon; ?></td>
                    <td> <?= $model->dorucak->uto; ?></td>
                    <td> <?= $model->dorucak->sri; ?></td>
                    <td> <?= $model->dorucak->cet; ?></td>
                    <td> <?= $model->dorucak->pet; ?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('app', 'Ručak') ?></td>
                    <td> <?= $model->rucak->pon; ?></td>
                    <td> <?= $model->rucak->uto; ?></td>
                    <td> <?= $model->rucak->sri; ?></td>
                    <td> <?= $model->rucak->cet; ?></td>
                    <td> <?= $model->rucak->pet; ?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('app', 'Užina') ?></td>
                    <td> <?= $model->uzina->pon ?></td>
                    <td> <?= $model->uzina->uto ?></td>
                    <td> <?= $model->uzina->sri ?></td>
                    <td> <?= $model->uzina->cet ?></td>
                    <td> <?= $model->uzina->pet ?></td>
                </tr>
            </table>
            <style>
                .mob {
                    display: table
                }

                .mob tbody tr td:nth-of-type(2) {
                    white-space: normal;
                    word-break: break-word;
                }

                tbody tr td:nth-of-type(1) {
                    word-break: keep-all;
                }
                @media (min-width: 786px){
                    .mob{display:none}
                }
            </style>
            <table class="table table-striped table-bordered mob">
                <thead>

                    <th colspan="2" class="text-center"><?= Yii::t('app', 'Ponedjeljak') ?><br><?php echo date('j.n.Y', strtotime($datum['pon'])); ?></th>

                </thead>
                <tbody>
                    <tr>
                        <td><?= Yii::t('app', 'Doručak') ?></td>
                        <td> <?= $model->dorucak->pon; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Ručak') ?></td>
                        <td> <?= $model->rucak->pon; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Užina') ?></td>
                        <td> <?= $model->uzina->pon ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered mob">
                <thead>

                    <th colspan="2" class="text-center"><?= Yii::t('app', 'Utorak') ?><br><?php echo date('j.n.Y', strtotime($datum['uto'])); ?></th>

                </thead>
                <tbody>
                    <tr>
                        <td><?= Yii::t('app', 'Doručak') ?></td>
                        <td> <?= $model->dorucak->uto; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Ručak') ?></td>
                        <td> <?= $model->rucak->uto; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Užina') ?></td>
                        <td> <?= $model->uzina->uto ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered mob">
                <thead>

                    <th colspan="2" class="text-center"><?= Yii::t('app', 'Srijeda') ?><br><?php echo date('j.n.Y', strtotime($datum['sri'])); ?></th>

                </thead>
                <tbody>
                    <tr>
                        <td><?= Yii::t('app', 'Doručak') ?></td>
                        <td> <?= $model->dorucak->sri; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Ručak') ?></td>
                        <td> <?= $model->rucak->sri; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Užina') ?></td>
                        <td> <?= $model->uzina->sri ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered mob">
                <thead>

                    <th colspan="2" class="text-center"><?= Yii::t('app', 'Četvrtak') ?><br><?php echo date('j.n.Y', strtotime($datum['cet'])); ?></th>

                </thead>
                <tbody>
                    <tr>
                        <td><?= Yii::t('app', 'Doručak') ?></td>
                        <td> <?= $model->dorucak->cet; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Ručak') ?></td>
                        <td> <?= $model->rucak->cet; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Užina') ?></td>
                        <td> <?= $model->uzina->cet ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered mob">
                <thead>

                    <th colspan="2" class="text-center"><?= Yii::t('app', 'Petak') ?><br><?php echo date('j.n.Y', strtotime($datum['pet'])); ?></th>

                </thead>
                <tbody>
                    <tr>
                        <td><?= Yii::t('app', 'Doručak') ?></td>
                        <td> <?= $model->dorucak->pet; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Ručak') ?></td>
                        <td> <?= $model->rucak->pet; ?></td>
                    </tr>
                    <tr>
                        <td><?= Yii::t('app', 'Užina') ?></td>
                        <td> <?= $model->uzina->pet ?></td>
                    </tr>
                </tbody>
            </table>


        </div>
    </div>
</div>