<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Jelovnik */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jelovnik-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'datum')->widget(DatePicker::classname(), [
                'convertFormat' => true,
            ]) ?>

    <?= $form->field($model, 'obrok')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Obrok::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'jelo')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Jelo::find()->all(),
                    'id',
                    function ($v) {
                        return 'PROMJENI!!!';
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'redoslijed')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
