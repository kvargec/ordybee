<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Notifikacije */

$this->title = Yii::t('app', 'Create Notifikacije');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notifikacijes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifikacije-create">
<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
       
        
    </div>
<div class="card-body">
<div class="col-lg-3">
                <div>
                    <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success wide']) ?></div>


                <div id="accordion" role="tablist">
                    <div class="card-collapse mt-0">
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="mb-0 mt-0">
                                <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                    <?= Yii::t('app','Folders')?>
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </a>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                            <div class="card-body">
                                <ul>
                                    <li>
                                        <h5 class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/notifikacije/index', 'inbox' => Yii::$app->user->id]) ?>"  ><span class="material-icons">inbox</span><?= ' '.Yii::t('app' ,'Inbox')?> </a></h5>
                                    </li>
                                    <li>
                                        <h5 class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/notifikacije/index', 'sent' => Yii::$app->user->id]) ?>" ><span class="material-icons">outbox</span> <?=' '.Yii::t('app' ,'Sent') ?></a></h5>
                                    </li>
                                    <li>
                                        <h5 class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/notifikacije/index', 'trash' => Yii::$app->user->id]) ?>" ><span class="material-icons red">delete</span><?= ' '.Yii::t('app' ,'Trash') ?></a></h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card-collapse">
                        <div class="card-header" role="tab" id="headingTwo">
                            <h5 class="mb-0 mt-0">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Labels
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <ul>
                                <li class="inbox">
                                        <h5 class="mt-0 mb-0"><a href="#"><span class="material-icons teal">panorama_fish_eye</span> Label1</a></h5>
                                    </li>
                                    <li class="sent">
                                        <h5 class="mt-0 mb-0"><a href="#"><span class="material-icons blue">panorama_fish_eye</span> Label2</a></h5>
                                    </li>
                                    <li class="trash">
                                        <h5 class="mt-0 mb-0"><a href="#"><span class="material-icons yellow">panorama_fish_eye</span> Label3</a></h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->

                </div>

            </div>
            <div class="col-lg-9">
    <?= $this->render('_form', [
        'model' => $model,
        'mediaProvider' => $mediaProvider
    ]) ?>

</div></div></div></div>
