
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\ZaposlenjeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Notifikacija');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zaposlenje-index">
<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>


        
        
</div>
<div class="card-content"><?php echo $notifikacija->sadrzaj;
        echo '<br>';
        echo Yii::t('app','Poslano: ').date('Y.m.d H:i:s', strtotime($notifikacija->created_at))
?>
</div>
