<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Notifikacije */

$this->title = isset($model->sender->username)?Yii::t('app','Poruka od ').$model->sender->username:Yii::t('app','Notifikacija ').$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Notifikacijes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="notifikacije-view">
<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>


        </div>
<div class="card-body">

            <div class="col-md-4 col-lg-2">
                <div class="text-center">
                    <?= Html::a('<span class="material-icons btn btn-default btn-round">add</span>', ['create'], ['class' => 'new-message']) ?></div>


                <div id="accordion" role="tablist">
                    <div class="card-collapse mt-0">
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="mb-0 mt-0">
                                <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                    <?= Yii::t('app','Folders')?>
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </a>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                            <div class="card-body">
                                <ul>
                                    <li>
                                        <p class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/notifikacije/index', 'inbox' => Yii::$app->user->id])  ?>" <?= $tip == 'inbox'?'class="active"':'' ?>  ><span class="material-icons">inbox</span><?= ' '.Yii::t('app' ,'Inbox')?></a></p>
                                    </li>
                                    <li>
                                        <p class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/notifikacije/index', 'sent' => Yii::$app->user->id]) ?>" <?= $tip == 'sent'?'class="active"':'' ?>><span class="material-icons">outbox</span> <?=' '.Yii::t('app' ,'Sent') ?></a></p>
                                    </li>
                                    <li>
                                        <p class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/notifikacije/index', 'trash' => Yii::$app->user->id]) ?>" <?= $tip == 'trash'?'class="active"':'' ?> ><span class="material-icons">delete</span><?= ' '.Yii::t('app' ,'Trash') ?></a></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card-collapse">
                        <div class="card-header" role="tab" id="headingTwo">
                            <h5 class="mb-0 mt-0">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Labels
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <ul>
                                <li class="inbox">
                                        <h5 class="mt-0 mb-0"><a href="#"><span class="material-icons teal">panorama_fish_eye</span> Label1</a></h5>
                                    </li>
                                    <li class="sent">
                                        <h5 class="mt-0 mb-0"><a href="#"><span class="material-icons blue">panorama_fish_eye</span> Label2</a></h5>
                                    </li>
                                    <li class="trash">
                                        <h5 class="mt-0 mb-0"><a href="#"><span class="material-icons yellow">panorama_fish_eye</span> Label3</a></h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->

                </div>

            </div>
            <div class="col-md-8 col-lg-10">


    <div class="text-right buttons-notification"><a href="#" class="btn btn-primary btn-sm"><span class="material-icons">reply</span></a><a href="#"  class="btn btn-primary btn-sm"><span class="material-icons">print</span></a><a href="#" class="btn btn-danger btn-sm"><span class="material-icons" >delete</span></a></div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                    'label' => Yii::t('app','Primatelj'),
                'attribute' => 'user',
                'value' => function ($data){
                    return isset($data->user)?$data->user0->username:'';
                }
],
            [
                'label' => Yii::t('app','Pošiljatelj'),
                'attribute' => 'sender_id',
                'value' => function ($data){
                    return isset($data->sender_id)?$data->sender->username:'';
                }
            ],
            // 'akcija',
            // 'akcija_id',
            // 'radnja',
            'created_at',
            'read_at',
            'subject:ntext',
            'sadrzaj:ntext',
            [
                'label' => Yii::t('app','Dodaci poruci'),
                'format'=>'raw',
                'value' => function ($model) {
                    $linkovi = "";
                    if (!is_null($model->attachments)) {
                        foreach ($model->attachments["attachments"] as $link){
                            $linkovi .= "
                            <a href='/admin$link' target='_blank' style='font-size:32px'>
                                <span class='fa fa-file-pdf-o'></span>
                                <span class='fa fa-file-word-o'></span>
                                <span class='fa fa-file-text-o'></span>
                                <span class='fa fa-file-image-o '></span>
                                <span class='fa fa-file-video-o '></span>
                                <span class='fa fa-file-excel-o'></span>
                            </a>";
                        }
                    }
                    return $linkovi;
                }
            ],
            //  
        ],
    ]) ?>

</div>
        </div>
    </div>
</div>
