<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\NotifikacijeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
switch ($select_tab){
    case 'inbox':
        $this->title = Yii::t('app', 'Pristigle notifikacije');
        break;
    case 'trash':
        $this->title = Yii::t('app', 'Obrisane notifikacije');
        break;
    case 'sent':
        $this->title = Yii::t('app', 'Poslane notifikacije');
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notifikacije-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>


        </div>
        <div class="card-body">

            <div class="col-md-4 col-lg-2">
                <div class="text-center">
                    <?= Html::a('<span class="material-icons btn btn-default btn-round">add</span>', ['create'], ['class' => 'new-message']) ?></div>


                <div id="accordion" role="tablist">
                    <div class="card-collapse mt-0">
                        <div class="card-header" role="tab" id="headingOne">
                            <h5 class="mb-0 mt-0">
                                <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                    <?= Yii::t('app','Folders')?>
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </a>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                            <div class="card-body">
                                <ul>
                                    <li>
                                        <p class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/notifikacije/index', 'inbox' => Yii::$app->user->id]) ?>" <?= $select_tab=='inbox'?'class="active"':'' ?>><span class="material-icons">inbox</span><?= ' '.Yii::t('app' ,'Inbox') ?> <?= $count>0?'('.$count.')':''?></a></p>
                                    </li>
                                    <li>
                                        <p class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/notifikacije/index', 'sent' => Yii::$app->user->id]) ?>" <?= $select_tab=='sent'?'class="active"':'' ?>><span class="material-icons">outbox</span> <?=' '.Yii::t('app' ,'Sent') ?></a></p>
                                    </li>
                                    <li>
                                        <p class="mt-0 mb-0"><a href="<?= \yii\helpers\Url::to(['/notifikacije/index', 'trash' => Yii::$app->user->id]) ?>" <?= $select_tab=='trash'?'class="active"':'' ?>><span class="material-icons">delete</span><?= ' '.Yii::t('app' ,'Trash') ?></a></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card-collapse">
                        <div class="card-header" role="tab" id="headingTwo">
                            <h5 class="mb-0 mt-0">
                                <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Labels
                                    <i class="material-icons">keyboard_arrow_down</i>
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <ul>
                                <li class="inbox">
                                        <h5 class="mt-0 mb-0"><a href="#"><span class="material-icons teal">panorama_fish_eye</span> Label1</a></h5>
                                    </li>
                                    <li class="sent">
                                        <h5 class="mt-0 mb-0"><a href="#"><span class="material-icons blue">panorama_fish_eye</span> Label2</a></h5>
                                    </li>
                                    <li class="trash">
                                        <h5 class="mt-0 mb-0"><a href="#"><span class="material-icons yellow">panorama_fish_eye</span> Label3</a></h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->

                </div>

            </div>
            <div class="col-md-8 col-lg-10">

                <?php // echo $this->render('_search', ['model' => $searchModel]); 
                ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout'=> "{summary}\n{items}{pager}\n",
                    'rowOptions' => function ($data) use ($select_tab){
                        if (!$data->read_at && ($data->sender_id != Yii::$app->user->id || $data->sender_id == $data->user)){
                            return ['class' => 'info', 'onClick' => 'window.location = "'. \yii\helpers\Url::to(['/notifikacije/view', 'id' => $data->id, 'tip' => $select_tab]).'"'];
                        }
                        return ['onClick' => 'window.location = "'.\yii\helpers\Url::to(['/notifikacije/view', 'id' => $data->id, 'tip' => $select_tab]).'"'];

                    },
                    'columns' => [

                        [
                            'label' => Yii::t('app', 'user'),
                            'value' => function ($data) {

                                return isset($data->user0) ? $data->user0->username : '';
                            }
                        ],



                        //'created_at',

                        'subject',
                        [
                            'attribute' => 'sadrzaj',
                            'value' => function ($data) {
                                if (strlen($data->sadrzaj) > 65) {
                                    return substr($data->sadrzaj, 0, 65) . '...';
                                } else {

                                    return $data->sadrzaj;
                                }
                            }
                        ],

                        [
                         'label' => Yii::t('app', 'Vrijeme dolaska'),
                            'value' => function ($data){
                                $minute_diff = time()/60 - strtotime($data->created_at)/60 ;
                                if ($minute_diff <= 60){
                                    return Yii::t('app', 'Prije {n, plural, one{# minute} few{# minute} other{# minuta}}',['n' => ceil($minute_diff)]);
                                }else{
                                    return Yii::$app->formatter->asDate($data->created_at,'php:j.n.Y. H:i');
                                }

                                /*elseif ($minute_diff/60 < 24){
                                    return Yii::t('app', 'Prije {n, plural, one{# sat} few{# sata} other{# sati}}',['n' => floor($minute_diff/60)]);
                                }else{
                                    return Yii::t('app', 'Prije {n, plural, one{# dan} few{# dana} other{# dana}}', ['n' => floor($minute_diff/60/24)]);
                                }*/
                            }
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}{delete}',
                            // 'visible' => Yii::$app->user->can('admin'),
                            'buttons' => [
                                'view' => function ($url, $data) use ($select_tab){

                                    $url2 = \Yii::$app->urlManager->createUrl(['notifikacije/view', 'id' => $data->id, 'tip' => $select_tab]);
                                    return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                        'title' => Yii::t('app', 'Pogledaj'),
                                    ]);
                                },
                                'delete' => function ($url, $data){
                                    if (($data->sender_id == Yii::$app->user->id && $data->sender_trash) || ($data->user == Yii::$app->user->id && $data->user_trash)){
                                        $url2 = \yii\helpers\Url::to(['notifikacije/empty-trash', 'id' => $data->id]);
                                    }else{
                                        $url2 = \yii\helpers\Url::to(['notifikacije/trash', 'id' => $data->id]);
                                    }
                                    return Html::a('<span class="material-icons red">delete</span>', $url2, [
                                        'title' => Yii::t('app', 'Obriši'), 'data-method' => 'POST'
                                    ]);
                                },

                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>