<?php

use dektrium\user\models\User;
use dosamigos\fileupload\FileUpload;
use dosamigos\fileupload\FileUploadUI;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model common\models\Notifikacije */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="notifikacije-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user[user]')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(User::find()->all(),'id','username'),
        'options' => ['placeholder' => Yii::t('app','Odaberi korisnika...'),'multiple' => true],
    ]) ?>

    <?= $form->field($model, 'user[grupa]')->widget(Select2::classname(), [
        'data' => array_merge(['roditelji'=>'roditelji'],ArrayHelper::map(\common\models\RadnoMjesto::find()->all(),'naziv',function ($data){
            return $data->naziv;
        })),
        'options' => ['placeholder' => Yii::t('app','Odaberi grupu korisnika...'),'multiple' => true],
    ])->label(Yii::t('app','Grupa korisnika')) ?>

    <?= $form->field($model,'subject')->textInput() ?>
    <?= $form->field($model, 'sender_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
    <?= $form->field($model, 'sadrzaj')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'status')->hiddenInput(['value' => 1])->label(false) ?>
    <?= $form->field($model, 'attachments[]')->widget(FileInput::classname(), [
        'options' => ['accept' => '*/*', 'id' => 'input', 'multiple' => true],
        'pluginOptions' => [
            'initialPreview'=>$model->isNewRecord ? [] : [$model->getImgSrc(),],
            'initialPreviewAsData'=>!$model->isNewRecord,
            'msgPlaceholder' => Yii::t('app', 'Odabir'),
            // 'browseLabel' => '',
            'removeLabel' => '',
            'uploadLabel' => '',
            'browseClass' => 'btn btn-primary',
            'uploadClass' => 'btn btn-primary',
            'removeClass' => 'btn btn-danger',
            'browseLabel' => Yii::t('app', 'Odabir'),
            'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteku ovdje'),
            'placeholder' => Yii::t('app', 'Odabir'),
        ]
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
