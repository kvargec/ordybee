<?php

use yii\helpers\Html;
?>
<div class=" col-lg-3">
    <div class="panel panel-info">
        <div class="panel-heading">
            <span class="username"><?php echo $model->naziv ?></span>
        </div>
        <div class="panel-body">
            <?php
            $image = '';
            $ekst=explode(".",$model->filename);
            switch ($ekst[count($ekst)-1]){
                case 'docx':
                case 'rtf':
                case 'doc':
                    $image=Html::img(Yii::getAlias("@frontendUrl").'/img'. DIRECTORY_SEPARATOR.'word.png', ['alt'=>$model->naziv, 'class' => 'img-responsive pad back-center-img ']);
                    break;
                case 'xls':
                case 'xlsx':
                    $image=Html::img(Yii::getAlias("@frontendUrl").'/img'. DIRECTORY_SEPARATOR.'excel.png', ['alt'=>$model->naziv, 'class' => 'img-responsive pad back-center-img ']);
                    break;
                case 'pdf':
                    $image=Html::img(Yii::getAlias("@frontendUrl").'/img'. DIRECTORY_SEPARATOR.'pdf.png', ['alt'=>$model->naziv, 'class' => 'img-responsive pad back-center-img ']);
                    break;
                default:
                    $image = Html::img(Yii::getAlias("@frontendUrl").'/media'. DIRECTORY_SEPARATOR.$model->filename, ['alt'=>$model->naziv, 'class' => 'img-responsive pad back-center-img ']);
                    break;
            }
            echo $image;
            ?>

        </div>
        <div class="panel-footer">
            <?= Html::checkbox("mediaIds[$model->id]", $checked) ?>
        </div>
    </div>
</div>