<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\MediaTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Media Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-type-index">
<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>

           

        </div>
        <div class="card-body">
  

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'naziv',
            'icon',

            [
                'class' =>  'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $url2 = \yii\helpers\Url::to(['/media-type/view', 'id' => $model->id]);
                        return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'visibility')), [
                            'class' => 'material-icons'
                        ]), $url2);
                    },
                    'update' => function ($url, $model, $key) {
                        $url2 = \yii\helpers\Url::to(['/media-type/update', 'id' => $model->id]);
                        return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'edit')), [
                            'class' => 'material-icons'
                        ]), $url2);
                    },
                    'delete' => function ($url, $model, $key) {
                        $url2 = \yii\helpers\Url::to(['/media-type/delete', 'id' => $model->id]);
                        return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'delete')), [
                            'class' => 'material-icons red'
                        ]), $url2, [
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div></div></div>
