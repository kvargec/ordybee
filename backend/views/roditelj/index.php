<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\RoditeljSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Roditeljs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="roditelj-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ime',
            'prezime',
            'spol',
            'dat_rod',
            //'oib',
            //'adresa',
            //'mjesto',
            //'prebivaliste',
            //'radno',
            //'zanimanje',
            //'poslodavaca',
            //'mobitel',
            //'email:email',
            //'dijete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
