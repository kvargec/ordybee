<?php

use kartik\widgets\DepDrop;
use kartik\widgets\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Media */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="media-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    echo $form->field($model, 'grupacija')->widget(Select2::classname(), [
        'data' => array_merge(['roditelji' => Yii::t('app', 'Roditelji'), 'skupine' => Yii::t('app', 'Skupine'), 'zaposlenici' => Yii::t('app', 'Zaposlenici-radna mjesta'), 'pojedini' => Yii::t('app', 'Pojedini zaposlenici')]),
        'options' => ['placeholder' => Yii::t('app', 'Odaberi grupu korisnika...'), 'id' => 'grupacija'],
    ])->label(Yii::t('app', 'Grupa korisnika'));
    if (!$model->isNewRecord) {
        $model->recipients = $model->getRecipients();
    }
    echo $form->field($model, 'recipients')->hint(
        Yii::t('app', 'Odaberite primatelje (moguće je odabrati više primatelja)')
    )->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'options' => ['id' => 'recipients'],
        'select2Options' => ['pluginOptions' => ['allowClear' => true, 'multiple' => true]],
        'pluginOptions' => [
            'depends' => ['grupacija'],
            'placeholder' => Yii::t('app', 'Odaberite...'),
            'initialize' => true,
            'loadingText' => '',
            'url' => Url::to(['/media/grupacije'])
        ]
    ])->label(Yii::t('app', "Primatelji"));
    ?>
    <?php
    echo $form->field($model, 'ccgrupacija')->widget(Select2::classname(), [
        'data' => array_merge(['roditelji' => Yii::t('app', 'Roditelji'), 'skupine' => Yii::t('app', 'Skupine'), 'zaposlenici' => Yii::t('app', 'Zaposlenici-radna mjesta'), 'pojedini' => Yii::t('app', 'Pojedini zaposlenici')]),
        'options' => ['placeholder' => Yii::t('app', 'Odaberi grupu korisnika...'), 'id' => 'ccgrupacija'],
        'pluginOptions' => ['allowClear' => true]
    ])->label(Yii::t('app', 'Grupa korisnika za kopiju'));
    if (!$model->isNewRecord) {
        $model->cc_recipients = $model->getCcRecipients();
    }
    echo $form->field($model, 'cc_recipients')->hint(
        Yii::t('app', 'Odaberite primatelje (moguće je odabrati više primatelja)')
    )->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'options' => ['id' => 'cc_recipients'],
        'select2Options' => ['pluginOptions' => ['allowClear' => true, 'multiple' => true]],
        'pluginOptions' => [
            'depends' => ['ccgrupacija'],
            'placeholder' => Yii::t('app', 'Odaberite...'),
            'initialize' => true,
            'loadingText' => '',
            'url' => Url::to(['/media/grupacije'])
        ]
    ])->label(Yii::t('app', "Primatelji kopije"));

    ?>

    <?= $form->field($model, 'naziv')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opis')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'media_type')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(
            \common\models\MediaType::find()->all(),
            'id',
            function ($v) {
                return $v->naziv;
            }
        ),
        'options' => ['placeholder' => Yii::t('app', 'Odaberite vrstu')]
    ])->label(Yii::t('app', "Vrsta medije")) ?>

    <?=
    $form->field($model, 'kategorija')->widget(Select2::class, [
        'data' => ArrayHelper::map(\common\models\KategorijaMedia::find()->where(['status' => 2])->all(), 'id', 'kategorija'),
        'options' => ['placeholder' => Yii::t('app', 'Odaberite kategoriju')]
    ]);
    ?>
    <!-- <? //= $form->field($model, 'attachments[]')->widget(FileInput::classname(), [
            //
            //    'language' => 'hr',
            //    'options' => ['accept' => '*/*', 'id' => 'input', 'multiple' => true,],
            //    'pluginOptions' => [
            //        'initialPreview' => $model->isNewRecord ? [] : $model->getFilePaths(),
            //        'initialPreviewAsData' => true,
            //        'showRemove' => false,
            //        'showUpload' => false,
            //        'msgPlaceholder' => Yii::t('app', 'Odabir'),
            //        'language' => substr(\Yii::$app->language, 0, 2),
            //        'browseLabel' => Yii::t('app', 'Odabir'),
            //        'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteku ovdje'),
            //        'placeholder' => Yii::t('app', 'Odabir'),
            //    ],
            //
            //
            //])->label(Yii::t('app', "Datoteka")) 
            ?>-->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>