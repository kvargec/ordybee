<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use evgeniyrru\yii2slick\Slick;
use wbraganca\videojs\VideoJsWidget;
use yii\helpers\Url;
use common\models\Media;

use common\models\User;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\MediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-index">
    <!-- <div class="card"> -->
    <!-- <div class="card-header card-header-primary netflix">
            <span class="card-title"> <?= Yii::t('app', 'Media'); ?></span>
        </div>
        <div class="card-body" style="overflow: unset;">
            <div class="container">
                <div class="row">

                    <?php
                    if (!empty($items_strip) && !empty($items_view)) {
                        echo Slick::widget([
                            // HTML tag for container. Div is default.
                            'itemContainer' => 'div',
                            // HTML attributes for widget container
                            'containerOptions' => ['class' => 'MultiCarousel center'],
                            // Position for inclusion js-code
                            // see more here: http://www.yiiframework.com/doc-2.0/yii-web-view.html#registerJs()-detail
                            'jsPosition' => yii\web\View::POS_READY,
                            // It possible to use Slick.js events
                            // see more: http://kenwheeler.github.io/slick/#events
                            /*
                            'events' => [
                                
                                'edge' => 'function(event, slick, direction) {
                                            console.log(direction);
                                            // left
                                        }'
                            ],*/
                            // Items for carousel. Empty array not allowed, exception will be throw, if empty 
                            'items' => $items_strip,
                            // HTML attribute for every carousel item
                            'itemOptions' => ['class' => 'slick-item-preview'],
                            // settings for js plugin
                            // @see http://kenwheeler.github.io/slick/#settings
                            'clientOptions' => [
                                // note, that for params passing function you should use JsExpression object
                                // but pay atention, In slick 1.4, callback methods have been deprecated and replaced with events.
                                //'onAfterChange' => new JsExpression('function() {console.log("The cat has shown")}'),
                                'centerMode' => true,
                                'centerPadding' => '60px',
                                'focusOnSelect' => true,
                                'asNavFor' => '.media-display',
                                //'variableWidth' => true,
                                'dots'     => true,
                                'arrows' => true,
                                'autoplay' => false,
                                //'appendArrows' => '',
                                'speed'    => 300,
                                'infinite' => true,
                                'slidesToShow' => 3,
                                'slidesToScroll' => 1,
                                'responsive' => [
                                    [
                                        'breakpoint' => 1200,
                                        'settings' => [
                                            'slidesToShow' => 3,
                                            'slidesToScroll' => 1,
                                            'dots' => true,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 992,
                                        'settings' => [
                                            'slidesToShow' => 2,
                                            'slidesToScroll' => 1,
                                            'dots' => true,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 768,
                                        'settings' => [
                                            'slidesToShow' => 2,
                                            'slidesToScroll' => 1,
                                            'dots' => true,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 480,
                                        // 'settings' => 'unslick', // Destroy carousel, if screen width less than 480px
                                        'settings' => [
                                            'slidesToShow' => 1,
                                            'dots' => true,
                                        ],
                                    ],

                                ],
                            ],
                        ]);
                    }
                    ?>
                </div>
                <div class="slick-display">
                    <?php
                    if (!empty($items_strip) && !empty($items_view)) {
                        echo Slick::widget([
                            // HTML tag for container. Div is default.
                            'itemContainer' => 'div',
                            // HTML attributes for widget container
                            'containerOptions' => ['class' => 'media-display'],
                            // Position for inclusion js-code
                            // see more here: http://www.yiiframework.com/doc-2.0/yii-web-view.html#registerJs()-detail
                            'jsPosition' => yii\web\View::POS_READY,
                            // It possible to use Slick.js events
                            // see more: http://kenwheeler.github.io/slick/#events
                            /*
                            'events' => [
                                'edge' => 'function(event, slick, direction) {
                                            console.log(direction);
                                            // left
                                        }'
                            ],*/
                            // Items for carousel. Empty array not allowed, exception will be throw, if empty 
                            'items' => $items_view,
                            // HTML attribute for every carousel item
                            'itemOptions' => ['class' => 'slick-item'],
                            // settings for js plugin
                            // @see http://kenwheeler.github.io/slick/#settings
                            'clientOptions' => [
                                // note, that for params passing function you should use JsExpression object
                                // but pay atention, In slick 1.4, callback methods have been deprecated and replaced with events.
                                //'onAfterChange' => new JsExpression('function() {console.log("The cat has shown")}'),
                                'asNavFor' => '.MultiCarousel',
                                'arrows' => false,
                                'fade' => true,
                                'autoplay' => false,
                                'infinite' => false,
                                'slidesToShow' => 1,
                                'slidesToScroll' => 1,
                                'responsive' => [
                                    [
                                        'breakpoint' => 1200,
                                        'settings' => [
                                            'slidesToShow' => 1,
                                            'slidesToScroll' => 1,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 992,
                                        'settings' => [
                                            'slidesToShow' => 1,
                                            'slidesToScroll' => 1,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 768,
                                        'settings' => [
                                            'slidesToShow' => 1,
                                            'slidesToScroll' => 1,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 480,
                                        // 'settings' => 'unslick', // Destroy carousel, if screen width less than 480px
                                        'settings' => [
                                            'slidesToShow' => 1,
                                        ],
                                    ],

                                ],
                            ],
                        ]);
                    } ?>
                </div>
            </div>
            <div class="card-footer"></div>
        </div> -->

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]);
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'value' => 'user.username',
                        'label' => Yii::t('app', 'Pošiljatelj'),
                        'attribute' => 'user_username',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Pošiljatelj'),
                            'title' => Yii::t('app', 'Pošiljatelj'),
                            'class' => 'form-control'
                        ],
                        'filterAttribute' => 'user_username'
                    ],
                    [
                        'value' => 'naziv',
                        'label' => Yii::t('app', 'Naziv'),
                        'attribute' => 'naziv',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Naziv'),
                            'title' => Yii::t('app', 'Naziv'),
                            'class' => 'form-control'
                        ],
                    ],
                    [
                        'value' => 'opis',
                        'format' => 'ntext',
                        'label' => Yii::t('app', 'Opis'),
                        'attribute' => 'opis',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Opis'),
                            'title' => Yii::t('app', 'Opis'),
                            'class' => 'form-control'
                        ],
                    ],
                    [
                        'value' => 'created_at',
                        'label' => Yii::t('app', 'Poslano'),
                        'attribute' => 'created_at',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Poslano'),
                            'title' => Yii::t('app', 'Poslano'),
                            'class' => 'form-control'
                        ],
                        'filterAttribute' => 'created_at'
                    ],
                    // [
                    //     'value' => 'mediaType.naziv',
                    //     'label' => Yii::t('app', 'Tip'),
                    //     'attribute' => 'mediaType',
                    //     'filterOptions' => [
                    //         'class' => 'form-group'
                    //     ],
                    //     'filterInputOptions' => [
                    //         'placeholder' => Yii::t('app', 'Tip'),
                    //         'title' => Yii::t('app', 'Tip'),
                    //         'class' => 'form-control'
                    //     ]
                    // ],
                    [
                        'value' => function ($data) {
                            $return  = '';
                            if (!empty($data->size)) {
                                $return = $data->size . ' MB';
                                return $return;
                            }
                        },
                        'label' => Yii::t('app', 'Veličina'),
                        'attribute' => 'size',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Veličina'),
                            'title' => Yii::t('app', 'Veličina'),
                            'class' => 'form-control'
                        ],
                        'filterAttribute' => 'size'
                    ],
                    [
                        'format' => 'raw',
                        'value' => function ($data) {
                            $dat = '';
                            if (file_exists('media/' . $data->id)) {
                                if (isset($data->attachments)) {
                                    foreach ($data->attachments as $key => $val) {
                                        foreach ($val as $k => $v) {
                                            $dat .= Html::a(
                                                $v . '<br/>',
                                                Yii::getAlias('@web') . '/media/' . $data->id . '/' . $v,
                                                ['target' => '_blank']

                                            );
                                        }
                                    }
                                }
                            } else {
                                if (isset($data->attachments)) {
                                    foreach ($data->attachments as $key => $val) {
                                        foreach ($val as $k => $v) {
                                            $dat .= Html::a(
                                                $v . '<br/>',
                                                Yii::getAlias('@web') . '/media/' . '/' . $v,
                                                ['target' => '_blank']

                                            );
                                        }
                                    }
                                }
                            }
                            // return LightBoxWidget::widget([
                            //     'id'     =>'lightbox',  // id of plugin should be unique at page
                            //     'class'  =>'galary',    // class of plugin to define style
                            //     'height' =>'50px',     // height of image visible in widget
                            //     'width' =>'50px',      // width of image visible in widget
                            //     'images' => $val,
                            // ]);
                            return $dat;
                        },
                        'label' => Yii::t('app', 'Media'),
                        'attribute' => 'attachments',

                    ],
                    [
                        'class' =>  'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/media/view', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'visibility')), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'update' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/media/update', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'edit')), [
                                    'class' => 'material-icons'
                                ]), $url2);
                            },
                            'delete' => function ($url, $model, $key) {
                                $url2 = \yii\helpers\Url::to(['/media/delete', 'id' => $model->id]);
                                return Html::a(Html::tag('span', Html::encode(Yii::t('app', 'delete')), [
                                    'class' => 'material-icons red'
                                ]), $url2, [
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>