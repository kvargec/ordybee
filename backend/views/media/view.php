<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use evgeniyrru\yii2slick\Slick;
use wbraganca\videojs\VideoJsWidget;
use yii\helpers\Url;
use common\models\Media;
use common\models\Postavke;
/* @var $this yii\web\View */
/* @var $model common\models\Media */

$this->title = $model->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$media = Media::find()->indexBy('id')->all();

$image_paths = [];
$video_paths = [];


$type = '';
if (isset($model->media_type)) {
    $type = $model->media_type;
    $user_id = $model->user_id;
}
if ($type == '5') {
    if (file_exists('media/' . $model->id)) {
        $path = $model->getFilePaths($model->id);
    } else {
        $path = $model->getMediaPaths();
    }
    $image_paths[] = $path;
} elseif ($type == '1') {
    if (file_exists('media/' . $model->id)) {
        $path = $model->getFilePaths($model->id);
    } else {
        $path = $model->getMediaPaths();
    }
    $video_paths[] = $path;
}

// print_r($image_paths);
$items_view = [];
$items_strip = [];

foreach ($image_paths as $images) {
    foreach ($images as $image) {
        $image_string = $image;
        $items_strip[] = Html::img($image_string);
        $items_view[] = Html::img($image_string);
    }
}
foreach ($video_paths as $videos) {
    foreach ($videos as $video) {
        $video_string = $video;
        $items_strip[] = VideoJsWidget::widget([
            'options' => [
                'class' => 'video-js vjs-default-skin vjs-big-play-centered',
                //'poster' => "http://vjs.zencdn.net/v/oceans.mp4#t=10",
                'height' => 200,
                'controls' => false,
                'preload' => 'auto',
                'data' => [
                    'setup' => [
                        'autoplay' => false
                    ],
                ],
            ],
            'tags' => [
                'source' => [
                    ['src' => $video_string . '#t=10', 'type' => 'video/mp4'],
                    ['src' => $video_string . '#t=10', 'type' => 'video/webm']
                ],
                'track' => [
                    ['kind' => 'captions', 'src' => '', 'srclang' => 'en', 'label' => 'English']
                ]
            ]
        ]);
        $items_view[] = VideoJsWidget::widget([
            'options' => [
                'class' => 'video-js vjs-default-skin vjs-big-play-centered',
                //'poster' => "http://vjs.zencdn.net/v/oceans.mp4#t=10",
                'controls' => true,
                'height' => 500,
                'width' => 1083,
                'preload' => 'auto',
                'data' => [
                    'setup' => [
                        'autoplay' => false,
                    ],
                ],
            ],
            'tags' => [
                'source' => [
                    ['src' => $video_string, 'type' => 'video/mp4'],
                    ['src' => $video_string, 'type' => 'video/webm']
                ],
                'track' => [
                    ['kind' => 'captions', 'src' => '', 'srclang' => 'en', 'label' => 'English']
                ]
            ]
        ]);
    }
}

$this->title = Yii::t('app', 'Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-index">
    <div class="card">
        <div class="card-header card-header-primary netflix">
            <span class="card-title"> <?= Yii::t('app', 'Media'); ?></span>
        </div>
        <div class="card-body" style="overflow: unset;">
            <div class="container">
                <div class="row">

                    <?php
                    if (!empty($items_strip) && !empty($items_view)) {
                        echo Slick::widget([
                            // HTML tag for container. Div is default.
                            'itemContainer' => 'div',
                            // HTML attributes for widget container
                            'containerOptions' => ['class' => 'MultiCarousel center'],
                            // Position for inclusion js-code
                            // see more here: http://www.yiiframework.com/doc-2.0/yii-web-view.html#registerJs()-detail
                            'jsPosition' => yii\web\View::POS_READY,
                            // It possible to use Slick.js events
                            // see more: http://kenwheeler.github.io/slick/#events
                            /*
                            'events' => [
                                
                                'edge' => 'function(event, slick, direction) {
                                            console.log(direction);
                                            // left
                                        }'
                            ],*/
                            // Items for carousel. Empty array not allowed, exception will be throw, if empty 
                            'items' => $items_strip,
                            // HTML attribute for every carousel item
                            'itemOptions' => ['class' => 'slick-item-preview'],
                            // settings for js plugin
                            // @see http://kenwheeler.github.io/slick/#settings
                            'clientOptions' => [
                                // note, that for params passing function you should use JsExpression object
                                // but pay atention, In slick 1.4, callback methods have been deprecated and replaced with events.
                                //'onAfterChange' => new JsExpression('function() {console.log("The cat has shown")}'),
                                'centerMode' => true,
                                'centerPadding' => '60px',
                                'focusOnSelect' => true,
                                'asNavFor' => '.media-display',
                                //'variableWidth' => true,
                                'dots'     => true,
                                'arrows' => true,
                                'autoplay' => false,
                                //'appendArrows' => '',
                                'speed'    => 300,
                                'infinite' => true,
                                'slidesToShow' => 3,
                                'slidesToScroll' => 1,
                                'responsive' => [
                                    [
                                        'breakpoint' => 1200,
                                        'settings' => [
                                            'slidesToShow' => 3,
                                            'slidesToScroll' => 1,
                                            'dots' => true,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 992,
                                        'settings' => [
                                            'slidesToShow' => 2,
                                            'slidesToScroll' => 1,
                                            'dots' => true,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 768,
                                        'settings' => [
                                            'slidesToShow' => 2,
                                            'slidesToScroll' => 1,
                                            'dots' => true,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 480,
                                        // 'settings' => 'unslick', // Destroy carousel, if screen width less than 480px
                                        'settings' => [
                                            'slidesToShow' => 1,
                                            'dots' => true,
                                        ],
                                    ],

                                ],
                            ],
                        ]);
                    }
                    ?>
                </div>
                <div class="slick-display">
                    <?php
                    if (!empty($items_strip) && !empty($items_view)) {
                        echo Slick::widget([
                            // HTML tag for container. Div is default.
                            'itemContainer' => 'div',
                            // HTML attributes for widget container
                            'containerOptions' => ['class' => 'media-display'],
                            // Position for inclusion js-code
                            // see more here: http://www.yiiframework.com/doc-2.0/yii-web-view.html#registerJs()-detail
                            'jsPosition' => yii\web\View::POS_READY,
                            // It possible to use Slick.js events
                            // see more: http://kenwheeler.github.io/slick/#events
                            /*
                            'events' => [
                                'edge' => 'function(event, slick, direction) {
                                            console.log(direction);
                                            // left
                                        }'
                            ],*/
                            // Items for carousel. Empty array not allowed, exception will be throw, if empty 
                            'items' => $items_view,
                            // HTML attribute for every carousel item
                            'itemOptions' => ['class' => 'slick-item'],
                            // settings for js plugin
                            // @see http://kenwheeler.github.io/slick/#settings
                            'clientOptions' => [
                                // note, that for params passing function you should use JsExpression object
                                // but pay atention, In slick 1.4, callback methods have been deprecated and replaced with events.
                                //'onAfterChange' => new JsExpression('function() {console.log("The cat has shown")}'),
                                'asNavFor' => '.MultiCarousel',
                                'arrows' => false,
                                'fade' => true,
                                'autoplay' => false,
                                'infinite' => false,
                                'slidesToShow' => 1,
                                'slidesToScroll' => 1,
                                'responsive' => [
                                    [
                                        'breakpoint' => 1200,
                                        'settings' => [
                                            'slidesToShow' => 1,
                                            'slidesToScroll' => 1,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 992,
                                        'settings' => [
                                            'slidesToShow' => 1,
                                            'slidesToScroll' => 1,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 768,
                                        'settings' => [
                                            'slidesToShow' => 1,
                                            'slidesToScroll' => 1,
                                        ],
                                    ],
                                    [
                                        'breakpoint' => 480,
                                        // 'settings' => 'unslick', // Destroy carousel, if screen width less than 480px
                                        'settings' => [
                                            'slidesToShow' => 1,
                                        ],
                                    ],

                                ],
                            ],
                        ]);
                    } ?>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
</div>
<div class="media-view">


    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>


            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>

        </div>
        <div class="card-body detail">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'naziv',
                    [
                        'value' => $model->user->username,
                        'label' => Yii::t('app', 'Pošiljatelj'),
                        'attribute' => 'user_id'
                    ],
                    'opis:ntext',
                    [
                        'value' => $model->mediaType->naziv,
                        'label' => Yii::t('app', 'Tip medije'),
                        'attribute' => 'media_type',
                    ],
                    [
                        'value' => function ($data) {
                            $return  = '';
                            if (!empty($data->size)) {
                                $return = $data->size . ' MB';
                                return $return;
                            }
                        },
                        'label' => Yii::t('app', 'Veličina'),
                        'attribute' => 'size',
                    ],
                    //'filename',
                    //'created_at',
                    //'author',
                    //'user_id',
                    [
                        'format' => 'raw',

                        'value' => function ($data) {
                            $dat = '';
                            // $weburl = Postavke::find()->where(['postavka' => 'backendWeb'])->one();
                            // if ($weburl->vrijednost !== null) {
                            //     $admin = $weburl->vrijednost;
                            // } else {
                            //     $admin = '/admin';
                            // }
                            if (file_exists('media/' . $data->id)) {
                                if (isset($data->attachments)) {
                                    foreach ($data->attachments as $key => $val) {
                                        foreach ($val as $k => $v) {
                                            $dat .= Html::a(
                                                $v  . '<br/>',
                                                Yii::getAlias('@web') . '/media/' . $data->id . '/' . $v,
                                                ['target' => '_blank']

                                            );
                                        }
                                    }
                                }
                            } else {
                                if (isset($data->attachments)) {
                                    foreach ($data->attachments as $key => $val) {
                                        foreach ($val as $k => $v) {
                                            $dat .= Html::a(
                                                $v . 'nope<br/>',
                                                Yii::getAlias('@web') . '/media/' .  $v,
                                                ['target' => '_blank']

                                            );
                                        }
                                    }
                                }
                            }
                            clearstatcache();
                            // echo $_SERVER['DOCUMENT_ROOT'];
                            // return LightBoxWidget::widget([
                            //     'id'     =>'lightbox',  // id of plugin should be unique at page
                            //     'class'  =>'galary',    // class of plugin to define style
                            //     'height' =>'50px',     // height of image visible in widget
                            //     'width' =>'50px',      // width of image visible in widget
                            //     'images' => $val,
                            // ]);
                            return $dat;
                        },
                        'label' => Yii::t('app', 'Media'),
                        'attribute' => 'attachments',
                    ]
                ],
            ]) ?>

        </div>
    </div>
</div>