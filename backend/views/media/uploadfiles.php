<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Media */

$this->title = Yii::t('app', 'Add files');

?>
<div class="media-create">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">
            <?= FileInput::widget([
                'model' => $model,
                'attribute' => 'attachments[]',
                'name' => 'attachments[]',
                'language' => 'hr',
                'options' => [
                    'accept' => '*/*',
                    'id' => 'attachments_id',
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    //'allowedFileTypes' => ['*/*'], default null (all)
                    'initialPreview' => $model->getFilePaths($model->id),
                    'initialPreviewConfig' => $model->getFilePathsConfig($model->id),
                    'initialPreviewAsData' => true,
                    'uploadUrl' => Url::to(['/media/upload-file']),
                    'uploadExtraData' => [
                        'model_id' => $model->id,
                    ],
                    'deleteUrl' => Url::to(['/media/delete-file']),
                    'deleteExtraData' => [
                        'model_id' => $model->id,
                    ],
                    'previewFileType' => 'any',
                    'showUpload' => false,
                    'showRemove' => false,
                    'overwriteInitial' => false,
                    'uploadAsync' => false,
                    'browseOnZoneClick' => true,
                    'encodeUrl' => false, //double encodes otherwise 
                    'language' => substr(\Yii::$app->language, 0, 2),
                    'browseLabel' => Yii::t('app', 'Odabir'),
                    'dropZoneTitle' => Yii::t('app', 'Povucite i ispustite datoteke ovdje'),
                    'dropZoneClickTitle' => Yii::t('app', ' (ili pritisnite za odabir)'),
                    'placeholder' => Yii::t('app', 'Odabir'),
                    'msgPlaceholder' => Yii::t('app', 'Odabir'),
                    'msgUploadBegin' => Yii::t('app', 'Pričekajte dok se slike učitaju'),
                    'msgUploadThreshold' => Yii::t('app', 'Pričekajte, slike se učitavaju'),
                    'msgUploadEnd' => Yii::t('app', 'Završeno'),
                ],
            ]); ?>
            <div class="form-group">
                <a class="btn btn-success" href="<?= \yii\helpers\Url::to(['/media/index']) ?>"><?= Yii::t('app', 'Save') ?></a>
            </div>
        </div>
    </div>
</div>
<?php
$js = <<<JS
$('input[name^="Media"]').on('filebatchselected', function(event) {
    $(this).fileinput('upload');
});
JS;
$this->registerJs($js);
?>