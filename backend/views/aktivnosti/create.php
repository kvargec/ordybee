<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Aktivnosti */

$this->title = Yii::t('app', 'Create Aktivnosti');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aktivnostis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aktivnosti-create">

<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>

   
       
    
    </div>
    <div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
