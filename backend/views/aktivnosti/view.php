<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Aktivnosti */

$this->title = $model->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aktivnostis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="aktivnosti-view">

<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>

            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<span class="material-icons">print</span>', ['aktivnosti/pdf-print', 'id' => $model->id], ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
        

        </div>
        <div class="card-body detail">

    <style>.card .table tr td:last-of-type{text-align:left;}</style>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'naziv',
            [
                'attribute' => 'datum_pocetka',
                //'format' => ['dateTime', 'php:d.m.Y H:i'],
                'value' => function ($data) {
                    return Yii::$app->formatter->asDate($data->datum_pocetka, 'php:j.n.Y. H:i');
                },
            ],
            [
                'attribute' => 'datum_kraja',
                'value' => function ($data) {
                    return Yii::$app->formatter->asDate($data->datum_kraja, 'php:j.n.Y. H:i');
                },
                //'format' => ['dateTime', 'php:d.m.Y H:i'],
            ],
            'trajanje',
            [

                'attribute' => 'opis',
    
                'format' => 'html',

    
            ],
            [
                    'attribute' => 'skupine',
                    'value' => function ($data){
                        if ($data->skupine) {
                            foreach ($data->skupine as $skupina) {
                                return $skupina;
                            }
                        }
                    }
            ],
            'status',
            'created_at',
            'user_id',
        ],
    ]) ?>

</div>
</div></div>