<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ZaposlenikKontakt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zaposlenik-kontakt-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'zaposlenik')->textInput() ?>

    <?= $form->field($model, 'kontakt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
