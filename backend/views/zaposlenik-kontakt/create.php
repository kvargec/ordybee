<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ZaposlenikKontakt */

$this->title = Yii::t('app', 'Create Zaposlenik Kontakt');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zaposlenik Kontakts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zaposlenik-kontakt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
