<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\PitanjeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pitanja');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pitanje-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    // ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'pitanje',
                    'opis:ntext',
                    'upitnik0.naziv',
                    'is_required:boolean',

                    //'vrsta_pitanja',
                    //'odgovori',


                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}{update}',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data) {

                                $url2 = \Yii::$app->urlManager->createUrl(['pitanje/view', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pogledaj'),
                                ]);
                            },

                            'update' => function ($url, $data) {
                                $url2 = \Yii::$app->urlManager->createUrl(['pitanje/update', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">create</span>', $url2, [
                                    'title' => Yii::t('app', 'Ažuriraj'),
                                ]);
                            },


                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>