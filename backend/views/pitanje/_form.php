<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Pitanje */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pitanje-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pitanje')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opis')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'upitnik')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(
            \common\models\Upitnik::find()->all(),
            'id',
            'naziv'
        ),
    ]) ?>

    <?= $form->field($model, 'is_required', ['template' => '{input}{label}{error}'])->checkbox(['label' => 'obavezno']) ?>

    <?= $form->field($model, 'vrsta_pitanja')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(
            \common\models\VrstaPitanja::find()->all(),
            'id',
            'vrsta_pitanje'

        ),
    ]) ?>

    <?= $form->field($model, 'odgovori')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>