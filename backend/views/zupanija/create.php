<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Zupanija */

$this->title = Yii::t('app', 'Create Zupanija');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zupanijas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zupanija-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
