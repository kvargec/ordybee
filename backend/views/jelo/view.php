<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Jelo */

$this->title = $model->naziv;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Jelos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="jelo-view">
<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>


        <?= Html::a('<span class="material-icons">create</span>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="material-icons red">delete</span>', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="card-body  detail">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'naziv',
            'podaci',
        ],
    ]) ?>

</div>
</div>
</div>
