<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Jelo */

$this->title = Yii::t('app','Create Jelo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Jelos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jelo-create">
<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
    </div>
    <div class="card-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
