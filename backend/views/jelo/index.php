<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\JeloSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Jela');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jelo-index">
<div class="card">
    <div class="card-header card-header-primary">
        <span class="card-title"><?= Html::encode($this->title) ?></span>
        <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
</div>

<div class="card-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    $gridColumns = [
            
        [
            'value' => 'naziv',
            'label' => Yii::t('app', 'Naziv'),
            'attribute' => 'naziv',
            'filterOptions' => [
                'class' => 'form-group'
            ],
            'filterInputOptions' => [
                'placeholder' => Yii::t('app', 'Naziv'),
                'class' => 'form-control'
            ]
        ],
        [
            'value' => 'podaci',
            'label' => Yii::t('app', 'Podaci'),
            'attribute' => 'podaci',
            'filterOptions' => [
                'class' => 'form-group'
            ],
            'filterInputOptions' => [
                'placeholder' => Yii::t('app', 'Podaci'),
                'class' => 'form-control'
            ]
        ],
        ['class' => 'yii\grid\ActionColumn',
        'template' => '{view} {update} {delete} ',
            // 'visible' => Yii::$app->user->can('admin'),
            'buttons' => [
                'view' => function ($url, $data) {

                    $url2 = \Yii::$app->urlManager->createUrl(['jelo/view', 'id' => $data->id]);
                    return Html::a('<span class="material-icons">visibility</span>', $url2, [
                        'title' => Yii::t('app', 'Pregledaj zapise'),
                        'class' => 'text-left  hgreen',
                    ]);
                }, 
                'update' => function ($url, $data){
                    $url2 = \Yii::$app->urlManager->createUrl(['jelo/update', 'id' => $data->id]);
                    return Html::a('<span class="material-icons">create</span>', $url2, [
                        'title' => Yii::t('app', 'Ažuriraj'),
                    ]);
                },
                'delete' => function ($url, $data){
                  
                    $url2 = \Yii::$app->urlManager->createUrl(['jelo/delete', 'id' => $data->id]);
                    return Html::a('<span class="material-icons red">delete</span>', $url2, [
                        'title' => Yii::t('user', 'Obriši'),
                        'data-confirm' => Yii::t('user', 'Are you sure you want to delete this user?'),
                        'data-method' => 'POST',
                    ]);
                    
                }, 



            ],
        ],  
    ];
    ?>

    <?= ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumns,
                        'exportConfig' => [
                            ExportMenu::FORMAT_TEXT => null,
                            ExportMenu::FORMAT_HTML => null,
                            ExportMenu::FORMAT_CSV => null,
                        ],
                        'filename' => 'popisJela',
                        'stream' => false,
                        'folder' => '@app/web/',
                        //'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser, needs stream => true
                        //'target' => '_blank',
                        'linkPath' => Url::toRoute('@web/web/'),
                        'afterSaveView' => '/dijete/zalink',
                        'dropdownOptions' => [
                            'label' => Yii::t('app','Export All'),
                            'class' => 'btn btn-outline-secondary'
                        ]
                    ]) . "<hr>\n" . 
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>


</div></div></div>
