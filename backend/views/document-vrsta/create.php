<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DocumentVrsta */

$this->title = Yii::t('app', 'Create Document Vrsta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Document Vrstas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-vrsta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
