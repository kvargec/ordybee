<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
$mjeseci = \common\helpers\Utils::mjeseciPedGod();

/* @var $this yii\web\View */
/* @var $model common\models\Cjenik */

$this->title = Yii::t('app', 'Napravi Uplatnicu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cjeniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cjenik-create">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">
            <div class="cjenik-form">
                <?php ActiveForm::begin(['action'=> ['cjenik/uplatnica', 'dijete_id' => $_GET['id']], 'method' => 'POST']); ?>
                <?= Select2::widget([
                    'name' => 'mjesec',
                    'data' => $mjeseci,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                        'multiple' => false,
                        'required' => true,
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,

                    ],
                ]) ?>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary "><span class="material-icons">download</span> PDF</button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="cjenik-form">
                <?php ActiveForm::begin(['action'=> ['cjenik/uplatnica-send', 'dijete_id' => $_GET['id']], 'method' => 'POST']); ?>
                <?= Select2::widget([
                    'name' => 'mjesec',
                    'data' => $mjeseci,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Odaberite mjesec'),
                        'multiple' => false,
                        'required' => true,
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,

                    ],
                ]) ?>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary "><span class="material-icons">mail</span> PDF</button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
