<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\CjenikSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cjeniks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cjenik-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('<span class="material-icons" style="font-size:12px">add</span><span class="material-icons" >groups</span>', ['cjenik-dijete/index'], ['class' => 'btn btn-success', 'title' => 'Izradi cjenik za skupinu']) ?>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsive' => true,
                'columns' => [
                    // ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    [
                        'value' => 'program0.vrsta',
                        'label' => Yii::t('app', 'Program'),
                        'attribute' => 'program',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Program'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'value' => 'cijena',
                        'label' => Yii::t('app', 'Cijena'),
                        'attribute' => 'cijena',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Cijena'),
                            'class' => 'form-control'
                        ]
                    ],
                    // 'povlastena',
                    [
                        'value' => 'povlastena',
                        'label' => Yii::t('app', 'Povlaštena'),
                        'attribute' => 'povlastena',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Povlaštena'),
                            'class' => 'form-control'
                        ]
                    ],
                    // 'ped_godina',

                    [
                        // vidi ovo Jo :)
                        'value' => function ($data) {
                            return $data->pedGodina->getNaziv();
                        },
                        'label' => Yii::t('app', 'Pedagoška godina'),
                        'attribute' => 'ped_godina',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Pedagoška godina'),
                            'class' => 'form-control'
                        ]
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}  ',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            'view' => function ($url, $data) {

                                $url2 = \Yii::$app->urlManager->createUrl(['cjenik/view', 'id' => $data->id]);
                                return Html::a('<span class="material-icons">visibility</span>', $url2, [
                                    'title' => Yii::t('app', 'Pregledaj zapise'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            },



                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>