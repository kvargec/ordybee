<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\CjenikSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cjeniks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cjenik-index">

    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            <?= Html::a('<span class="material-icons">add</span>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="card-body">

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <!-- <img src="<?php echo Yii::getAlias('@web') . '/uplatnica.jpg'; ?>"> -->
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsive' => true,
                'columns' => [
                    [
                        'value' => 'ime',
                        'label' => Yii::t('app', 'Ime djeteta'),
                        'attribute' => 'ime',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Ime djeteta'),
                            'title' => Yii::t('app', 'Ime djeteta'),
                            'class' => 'form-control'
                        ]
                    ],
                    [
                        'value' => 'prezime',
                        'label' => Yii::t('app', 'Prezime djeteta'),
                        'attribute' => 'prezime',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Prezime djeteta'),
                            'title' => Yii::t('app', 'Prezime djeteta'),
                            'class' => 'form-control'
                        ]

                    ],

                    [
                        'value' => 'oib',
                        'label' => Yii::t('app', 'OIB'),
                        'attribute' => 'oib',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'OIB'),
                            'title' => Yii::t('app', 'OIB'),
                            'class' => 'form-control'
                        ]

                    ],
                    // [
                    //     'value'=>'adresa',
                    //     'label'=>Yii::t('app','Adresa'),
                    //     'attribute'=>'adresa',
                    //     'filterOptions' => [
                    //         'class' => 'form-group'
                    //     ],
                    //     'filterInputOptions' => [
                    //         'placeholder' => Yii::t('app','Adresa'),
                    //         'title' => Yii::t('app','Adresa'),
                    //         'class' => 'form-control'
                    //     ]

                    // ],
                    // [
                    //     'value'=>'mjesto',
                    //     'label'=>Yii::t('app','Mjesto'),
                    //     'attribute'=>'mjesto',
                    //     'filterOptions' => [
                    //         'class' => 'form-group'
                    //     ],
                    //     'filterInputOptions' => [
                    //         'placeholder' => Yii::t('app','Mjesto'),
                    //         'title' => Yii::t('app','Mjesto'),
                    //         'class' => 'form-control'
                    //     ]

                    // ],
                    [
                        'value' => 'skupina0.naziv',
                        'label' => Yii::t('app', 'Skupina'),
                        'attribute' => 'skupina',
                        'filterOptions' => [
                            'class' => 'form-group'
                        ],
                        'filterInputOptions' => [
                            'placeholder' => Yii::t('app', 'Naziv skupine'),
                            'title' => Yii::t('app', 'Skupina'),
                            'class' => 'form-control'
                        ]
                    ],
                    //'prebivaliste',
                    //'sestra',
                    //'cekanje',
                    //'god_cekanja',
                    //'razvoj',
                    //'vrsta_programa',
                    //'teskoce',
                    //'dijagnostika',
                    //'druge_potrebe',
                    //'posebne',
                    //'create_at',
                    //'update_at',
                    // [
                    //         'label'=>Yii::t('app','Status'),
                    //         'attribute'=>'status',
                    //         'value'=>'status0.status',
                    //         'filter' => Arrayhelper::map(\common\models\StatusDjeteta::find()->asArray()->all(), 'id', 'status'),
                    // ],
                    // [
                    //         // vidi ovo Jo :)
                    //     'value' => function($data){
                    //         return $data->pedGodina->getNaziv();
                    //     },
                    //     'label' => Yii::t('app', 'Pedagoška godina'),
                    //     'attribute' => 'ped_godina',
                    //     'filterOptions' => [
                    //         'class' => 'form-group'
                    //     ],
                    //     'filterInputOptions' => [
                    //         'placeholder' => Yii::t('app', 'Pedagoška godina'),
                    //         'class' => 'form-control'
                    //     ]
                    // ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{create} ',
                        // 'visible' => Yii::$app->user->can('admin'),
                        'buttons' => [
                            //'view' => function ($url, $data) {
                            //    $url2 = \Yii::$app->urlManager->createUrl(['cjenik/view-uplatnica', 'id' => $data->id]);
                            //    return Html::a('<span class="material-icons ">visibility</span>', $url2, [
                            //        'title' => Yii::t('app', 'Pregledaj uplatnice'),
                            //        'class' => 'text-left  hgreen',
                            //    ]);
                            //},
                            'create' => function ($url, $data) {

                                $url2 = \Yii::$app->urlManager->createUrl(['cjenik/create-uplatnica', 'id' => $data->id]);
                                return Html::a('<span class="material-icons ">edit</span>', $url2, [
                                    'title' => Yii::t('app', 'Izradi uplatnicu'),
                                    'class' => 'text-left  hgreen',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>