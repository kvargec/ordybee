<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cjenik */

$this->title = Yii::t('app', 'Update Cjenik: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cjeniks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cjenik-update">

<div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
            
        </div>
        <div class="card-body detail">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div></div>
