<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DijeteSkupina */

$this->title = Yii::t('app', 'Create Dijete Skupina');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dijete Skupinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dijete-skupina-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
