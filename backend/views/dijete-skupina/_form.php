<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\DijeteSkupina */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dijete-skupina-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dijete')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Dijete::find()->all(),
                    'id',
                    function ($v) {
                        return Yii::t('app','PROMJENI!!!');
                    }
                ),
            ]) ?>

    <?= $form->field($model, 'skupina')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    \common\models\Skupina::find()->all(),
                    'id',
                    function ($v) {
                        return Yii::t('app','PROMJENI!!!');
                    }
                ),
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
