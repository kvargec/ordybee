<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StatusObjekt */

$this->title = Yii::t('app', 'Create Status Objekt');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Status Objekts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-objekt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
