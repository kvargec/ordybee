<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\VrsteObroka */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vrste-obroka-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vrsta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cijena')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
