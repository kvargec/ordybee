<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VrsteObroka */

$this->title = Yii::t('app', 'Create Vrste Obroka');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vrste Obrokas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vrste-obroka-create">
    <div class="card">
        <div class="card-header card-header-primary">
            <span class="card-title"><?= Html::encode($this->title) ?></span>
        </div>
        <div class="card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>