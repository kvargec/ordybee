<?php
namespace backend\widgets\calendar;

use yii\base\Widget;
use yii\helpers\Html;

class CalendarWidget extends Widget
{
    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        // Register AssetBundle
        CalendarWidgetAsset::register($this->getView());

        return $this->render('_calendar', ['events' => $this->model]);
    }
}