<?php

use yii\helpers\Html;
?>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {

            initialView: 'dayGridMonth',
            timeZone: 'UTC',
            locale: 'hr',
            headerToolbar: {
                left: 'prev,next',
                center: 'title',
                right: 'dayGridDay,dayGridWeek,dayGridMonth'
            },
            events: [<?php
                        foreach ($events as $event) {
                            echo $event . ',';
                        }
                        ?>]
        });
        calendar.render();
    });
</script>
<div>
    <style>
        @media (max-width: 786px) {
            .fc-view-harness {
                min-height: 380px;
            }

            .fc .fc-button .fc-icon {
                font-size: 1em
            }

            .fc .fc-toolbar-title {
                font-size: 1.3em;
                padding: 0 5px;
            }
            .fc-toolbar-chunk + .fc-toolbar-chunk .fc-button-group button:first-of-type,.fc-toolbar-chunk + .fc-toolbar-chunk .fc-button-group button:nth-of-type(2){
                display: none;
            }
            tbody tr td:nth-of-type(2){
                background-color:rgba(0,0,0,0);
                width:unset;

            }
            
        }
        .fc .fc-button-primary{
            background: linear-gradient(60deg, #095367, #003d4e) !important;
        }
        .fc .fc-button-primary:hover,.fc .fc-button-primary:focus,.fc .fc-button-primary:active{
            box-shadow:0 12px 20px -10px rgb(0 188 212 / 28%), 0 4px 20px 0px rgb(0 0 0 / 12%), 0 7px 8px -5px rgb(0 188 212 / 20%)
        }
    </style>

    <div id='calendar'></div>
</div