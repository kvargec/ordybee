<?php
namespace backend\widgets\calendar;

use yii\web\AssetBundle;

class CalendarWidgetAsset extends AssetBundle
{
    public $js = [
        'main.js',
        'locales-all.js'
    ];

    public $css = [
        'main.css'
    ];

    public $depends = [
        //'yii\web\JqueryAsset'
    ];

    public function init()
    {
        // Tell AssetBundle where the assets files are
        $this->sourcePath = __DIR__ . "/assets";
        parent::init();
    }
}

