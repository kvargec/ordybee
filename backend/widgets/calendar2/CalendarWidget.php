<?php
namespace backend\widgets\calendar2;

use yii\base\Widget;
use yii\helpers\Html;

class CalendarWidget extends Widget
{
    public $model;
    public $options;
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        // Register AssetBundle
        CalendarWidgetAsset::register($this->getView());

        return $this->render('_calendar2', ['events' => $this->model,'options'=>$this->options]);
    }
}