<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class UgovorForm extends Model
{
    public $klasa;
    public $urbroj;

    public function rules()
    {
        return [
            [['klasa', 'urbroj'], 'required'],
            [['klasa'], 'string', 'max' => 255],
        ];
    }
    public function attributeLabels()
    {
        return [

            'klasa' => Yii::t('app', 'Klasa'),
            'urbroj' => Yii::t('app', 'Urudžbeni broj'),
            'datumUgovora' => Yii::t('app', 'Datum ugovora')

        ];
    }
}