<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class OdlukaForm extends Model
{
    public $klasa;
    public $urbroj;
    public $datumUpisaDjeteta;

    public function rules()
    {
        return [
            [['klasa', 'urbroj', 'datumUpisaDjeteta'], 'required'],
            // [['datumUpisaDjeteta'], 'required'],
            [['klasa', 'urbroj'], 'string', 'max' => 255],
            [['datumUpisaDjeteta'], 'date', 'format' => Yii::$app->formatter->dateFormat],
        ];

    }
    public function attributeLabels()
    {
        return [

            'klasa' => Yii::t('app', 'Klasa'),
            'urbroj' => Yii::t('app', 'Urudžbeni broj'),

            'datumUpisaDjeteta' => Yii::t('app', 'Datum upisa'),

        ];
    }
}