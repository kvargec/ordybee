<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tvrtka".
 *
 * @property int $id
 * @property string|null $naziv
 * @property string|null $sifra
 * @property string|null $oib
 * @property string|null $adresa
 * @property int|null $mjesto
 * @property int|null $status
 * @property string|null $djelatnost
 *
 * @property Mjesto $mjesto0
 * @property StatusOpce $status0
 */
class Tvrtka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tvrtka';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mjesto', 'status'], 'default', 'value' => null],
            [['mjesto', 'status'], 'integer'],
            [['naziv', 'djelatnost'], 'string', 'max' => 250],
            [['sifra'], 'string', 'max' => 30],
            [['oib'], 'string', 'max' => 13],
            [['adresa'], 'string', 'max' => 150],
            [['mjesto'], 'exist', 'skipOnError' => true, 'targetClass' => Mjesto::className(), 'targetAttribute' => ['mjesto' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusOpce::className(), 'targetAttribute' => ['status' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'naziv' => Yii::t('app', 'Naziv'),
            'sifra' => Yii::t('app', 'Sifra'),
            'oib' => Yii::t('app', 'Oib'),
            'adresa' => Yii::t('app', 'Adresa'),
            'mjesto' => Yii::t('app', 'Mjesto'),
            'status' => Yii::t('app', 'Status'),
            'djelatnost' => Yii::t('app', 'Djelatnost'),
        ];
    }

    /**
     * Gets query for [[Mjesto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMjesto0()
    {
        return $this->hasOne(Mjesto::className(), ['id' => 'mjesto']);
    }

    /**
     * Gets query for [[Status0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusOpce::className(), ['id' => 'status']);
    }
}
