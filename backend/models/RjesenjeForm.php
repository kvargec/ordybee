<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class RjesenjeForm extends Model
{
    public $klasa;
    public $urbroj;
    public $datumUpisaDjeteta;
    public $datumDonosenjaRjesenja;

    public function rules()
    {
        return [
            [['klasa', 'urbroj', 'datumUpisaDjeteta', 'datumDonosenjaRjesenja'], 'required'],
            // [['datumUpisaDjeteta', 'datumDonosenjaRjesenja'], 'required'],
            [['klasa', 'urbroj'], 'string', 'max' => 255],
            [['datumUpisaDjeteta', 'datumDonosenjaRjesenja'], 'date', 'format' => Yii::$app->formatter->dateFormat],
        ];
    }
    public function attributeLabels()
    {
        return [

            'klasa' => Yii::t('app', 'Klasa'),
            'urbroj' => Yii::t('app', 'Urudžbeni broj'),
            'datumDonosenjaRjesenja' => Yii::t('app', 'Datum rješenja'),
            'datumUpisaDjeteta' => Yii::t('app', 'Datum upisa'),

        ];
    }
}