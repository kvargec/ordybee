php init<br />
composer install<br />
podesiti bazu<br /> 
 php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations<br />
 php yii migrate/up --migrationPath=@yii/rbac/migrations<br />
 Ubaciti u bazu INSERT INTO "public"."user"("id", "username", "email", "password_hash", "auth_key", "confirmed_at", "unconfirmed_email", "blocked_at", "registration_ip", "created_at", "updated_at", "flags", "last_login_at") VALUES (2, 'root', 'zwone@hotmail.com', '$2y$12$c2vsDMrHtVlfvOpPwBW8WOefD/vRblA8loO/hsS5H8AjwEXS7ut66', 'xRD7PVQLCcodV79MHXgBbeiSOlvnTY4r', 1511374993, NULL, NULL, '::1', 1511374993, 1511374993, 0, 1571314290);
<br />
Sada tek migracije pokrenuti
<br />
<hr />
DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
